/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.test.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;

/**
 * Utils methods for tests
 */
public class FraSCAtiTestUtils
{
    
    public static String getBindingUriBase()
    {
        return System.getProperty(AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingProcessor.BINDING_URI_BASE_DEFAULT_VALUE);
    }
    
    public static String completeBindingURI(String uri)
    {
      String completedUri=uri;
      if(completedUri != null && completedUri.startsWith("/"))
      {
          completedUri = System.getProperty(AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingProcessor.BINDING_URI_BASE_DEFAULT_VALUE) + uri;
      }
      return completedUri;
    }
    
    public static void assertWSDLExist(boolean assertion, String wsdlLocation)
    {
        String url=wsdlLocation;
        if(!wsdlLocation.endsWith("?wsdl"))
        {
            url+="?wsdl";
        }
        assertURLExist(assertion, url);
    }
    
    public static void assertWADLExist(boolean assertion, String wadlLocation)
    {
        String url=wadlLocation;
        if(!wadlLocation.endsWith("?_wadl"))
        {
            url+="?_wadl";
        }
        assertURLExist(assertion, url);
    }
    
    public static void assertURLExist(boolean assertion, String urlString)
    {
    	String completeStringURL = completeBindingURI(urlString);
        URL url = null;
        try
        {
            url = new URL(completeStringURL);
        } catch (MalformedURLException e)
        {
           fail("assertWSDLExist the provided wsdlLocation : "+urlString+" is not a valid URL");
        }

        int responseCode = 404;
        HttpURLConnection urlConnection = null;
        try
        {
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("GET"); 
            urlConnection.connect ();
            responseCode=urlConnection.getResponseCode();
        } catch (Exception e)
        {
            fail("assertWSDLExist unable to connect to "+url.getPath());
        } 
        finally
        {
            if(urlConnection!=null)
            {
                urlConnection.disconnect();   
            }
        }
        
        Logger.getAnonymousLogger().info("GET "+urlString+" HTTP status "+responseCode);
        boolean isResponseCodeOK=HttpURLConnection.HTTP_OK==responseCode;
        assertEquals(assertion,isResponseCodeOK);
    }
    
}
