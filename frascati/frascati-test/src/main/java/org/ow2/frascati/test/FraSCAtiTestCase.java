/**
 * OW2 FraSCAti: Testing module
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle, Gwenael Cattez
 */

package org.ow2.frascati.test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.After;
import org.junit.Before;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.util.context.ContextualProperties;

/**
 * Abstract class for testing SCA applications with FraSCAti
 * 
 */
public abstract class FraSCAtiTestCase
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // ---------------------------------------------------------------------------

    /**
     * The FraSCAti instance.
     */
    protected FraSCAti frascati;

    protected Map<String, Component> deployedSCAComposites;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // ---------------------------------------------------------------------------

    /**
     * Return the name of the composite to test.
     * 
     * @return the name of the composite to test.
     */
    private final String getMainCompositeName()
    {
        return System.getProperty("org.ow2.frascati.test.composite");
    }

    /**
     * Get the deployed component named compositeName
     * 
     * @param compositeName the name of the deployed component
     * @return the deployed component named compositeName, null if no component
     *         found
     */
    protected final Component getComposite(String compositeName)
    {
        return this.deployedSCAComposites.get(compositeName);
    }

    /**
     * Get the main composite defined by org.ow2.frascati.test.composite system property
     * 
     * @return The main composite or null
     */
    protected final Component getMainComposite()
    {
        return this.getComposite(this.getMainCompositeName());
    }

    /**
     * Initialize a ContextualProperties instance.
     * 
     * @param contextualProperties The contextual properties instance to
     *        initialize.
     */
    protected void initializeContextualProperties(ContextualProperties contextualProperties)
    {
        // Nothing to do by default.
    }

    /**
     * Get a service on a deployed SCA composite
     * 
     * @param <T> the class type for the return
     * @param composite the deployed SCA composite
     * @param serviceName the name of the searched service
     * @return the service named <code>serviceName</code> or null
     */
    protected final <T> T getService(Class<T> cl, Component composite, String serviceName)
    {
        System.out.println("Getting SCA service '" + serviceName + "'...");

        try
        {
            return frascati.getService(composite, serviceName, cl);
        } catch (Exception e)
        {
            System.err.println("No such SCA service: " + serviceName);
            return null;
        }
    }

    /**
     * Get a service on a deployed SCA composite
     * 
     * @param <T> the class type for the return
     * @param compositeName the name of the deployed SCA composite
     * @param serviceName the name of the searched service
     * @return the service named <code>serviceName</code> or null
     */
    protected final <T> T getService(Class<T> cl, String compositeName, String serviceName)
    {
        Component composite = this.getComposite(compositeName);
        if (composite == null)
        {
            System.err.println("No such SCA composite: " + compositeName);
            return null;
        }
        return this.getService(cl, composite, serviceName);
    }

    /**
     * Get a service on the main SCA composite to test
     * 
     * @param <T> the class type for the return
     * @param serviceName the name of the service
     * @return the service named <code>serviceName</code> or null
     */
    protected final <T> T getService(Class<T> cl, String serviceName)
    {
        System.out.println("Getting SCA service '" + serviceName + "'...");
        String mainCompositeName = this.getMainCompositeName();
        return this.getService(cl, mainCompositeName, serviceName);
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // ---------------------------------------------------------------------------

    /**
     * Load a composite
     * 
     * @param name the composite name
     * @throws FrascatiException
     * @throws NoSuchInterfaceException
     * @throws MalformedURLException
     */
    @Before
    public void loadComposite() throws Exception // FrascatiException,
                                                 // NoSuchInterfaceException,
                                                 // MalformedURLException
    {
        this.deployedSCAComposites = new HashMap<String, Component>();
        System.out.println("Instantiating FraSCAti...");
        this.frascati = FraSCAti.newFraSCAti();

        // Create and initialize a processing context.
        ProcessingContext processingContext = frascati.newProcessingContext();
        initializeContextualProperties(processingContext);

        // deploy contributions
        List<File> toDeployContributions = this.getContributions();
        for (File contributionFile : toDeployContributions)
        {
            this.deployContribution(contributionFile);
        }

        // deploy composites
        Map<String, File> toDeployComposites = this.getComposites();
        String toDeployCompositeName;
        File toDeployCompositeFile;
        for (Entry<String, File> toDeployCompositeEntry : toDeployComposites.entrySet())
        {
            toDeployCompositeName = toDeployCompositeEntry.getKey();
            toDeployCompositeFile = toDeployCompositeEntry.getValue();
            this.deployComposite(toDeployCompositeName, toDeployCompositeFile);
        }

        String mainCompositeName = getMainCompositeName();

        if (mainCompositeName != null && !mainCompositeName.equals(""))
        {
            System.out.println("Loading SCA composite '" + mainCompositeName + "'...");
            Component mainComposite = frascati.processComposite(mainCompositeName, processingContext);
            this.deployedSCAComposites.put(mainCompositeName, mainComposite);
        }
    }

    /**
     * Get all the composite files to deploy before starting tests
     */
    protected Map<String, File> getComposites()
    {
        return new HashMap<String, File>();
    }

    /**
     * Get all the contribution files to deploy before starting tests
     */
    protected List<File> getContributions()
    {
        return new ArrayList<File>();
    }

    /**
     * Close the SCA domain, undeploy all component
     * 
     * @throws IllegalLifeCycleException if the domain cannot be closed
     * @throws NoSuchInterfaceException if the lifecycle controller of the
     *         component is not found
     */
    @After
    public final void close() throws FrascatiException
    {
        for (Component deployedComponent : this.deployedSCAComposites.values())
        {
            frascati.close(deployedComponent);
        }
        frascati.close();
    }

    /**
     * Deploy a composite in the current FraSCAti instance
     * 
     * @param compositeName name of the composite to deploy
     * @param compositeFile the composite file
     * @throws Exception deployment fail
     */
    protected void deployComposite(String compositeName, File compositeFile) throws FrascatiException, MalformedURLException, NoSuchInterfaceException
    {
        CompositeManager compositeManager = frascati.getCompositeManager();
        ProcessingContext processingContext = compositeManager.newProcessingContext(compositeFile.toURI().toURL());
        Component deployedComposite = compositeManager.getComposite(compositeName, processingContext.getClassLoader());
        String deployedCompositeName = Fractal.getNameController(deployedComposite).getFcName();
        System.out.println("SCA composite '" + deployedCompositeName + "' deployed");
        this.deployedSCAComposites.put(deployedCompositeName, deployedComposite);
    }

    /**
     * Deploy a contribution in the current FraSCAti instance
     * 
     * @param contributionFile the contribution file to deploy
     * @throws Exception deployment fail
     */
    protected void deployContribution(File contributionFile) throws FrascatiException, NoSuchInterfaceException
    {
        CompositeManager compositeManager = frascati.getCompositeManager();
        ProcessingContext processingContext = compositeManager.newProcessingContext();
        Component[] deployedComposites = compositeManager.processContribution(contributionFile.getAbsolutePath(), processingContext);
        String deployedCompositeName;
        for (Component deployedComposite : deployedComposites)
        {
            deployedCompositeName = Fractal.getNameController(deployedComposite).getFcName();
            System.out.println("SCA composite '" + deployedCompositeName + "' deployed");
            this.deployedSCAComposites.put(deployedCompositeName, deployedComposite);
        }
    }

}
