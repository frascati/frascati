<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">
<section id="debug-intent">
  <title>The debug intent</title>

  <para>Debugging an SCA application can be done with FraSCAti thanks to the
  debug intent.</para>

  <para>This intent traces when entering and exiting implementation code of an
  SCA component. It can be set onto a component, then the debug traces will
  appear both when calling a service of the component and when a reference of
  the component uses another component's service. Finer grain debugging can be
  set by setting the debug intent only on a given service or reference of a
  component.</para>

  <para>Let's see how to use it on the helloworld-pojo example.</para>

  <para>The SCA composite file of this example is:</para>

  <programlisting>&lt;composite xmlns="http://www.osoa.org/xmlns/sca/1.0" targetNamespace="http://helloworld"
  xmlns:hw="http://helloworld" name="helloworld"&gt;

  &lt;service name="r" promote="client"/&gt;

  &lt;component name="client"&gt;
    &lt;service name="r"&gt;
      &lt;interface.java interface="java.lang.Runnable" /&gt;
    &lt;/service&gt;
    
    &lt;reference name="printService" target="server" /&gt;
    
    &lt;implementation.java class="org.ow2.frascati.examples.helloworld.pojo.Client" /&gt;
  &lt;/component&gt;
  
  &lt;component name="server"&gt;
    &lt;service name="printService"&gt;
      &lt;interface.java interface="org.ow2.frascati.examples.helloworld.pojo.PrintService"/&gt;
    &lt;/service&gt;

    &lt;implementation.java class="org.ow2.frascati.examples.helloworld.pojo.Server" /&gt;
  &lt;/component&gt;

&lt;/composite&gt;</programlisting>

  <para>The standard output when running this example is:</para>

  <programlisting>CLIENT created.
SERVER created.
SERVER: setting header to '---&gt;&gt; '.
SERVER: begin printing...
---&gt;&gt; Hello World!
SERVER: print done.</programlisting>

  <para>Now, let's put the debug intent on the service named <code>r</code> of
  the client component, and on the server component:</para>

  <programlisting>&lt;composite xmlns="http://www.osoa.org/xmlns/sca/1.0" targetNamespace="http://helloworld"
  xmlns:hw="http://helloworld" name="helloworld"&gt;

  &lt;service name="r" promote="client"/&gt;

  &lt;component name="client"&gt;
    &lt;service name="r" <emphasis role="bold">requires="frascati-debug"</emphasis>&gt;
      &lt;interface.java interface="java.lang.Runnable" /&gt;
    &lt;/service&gt;
    
    &lt;reference name="printService" target="server" /&gt;
    
    &lt;implementation.java class="org.ow2.frascati.examples.helloworld.pojo.Client" /&gt;
  &lt;/component&gt;
  
  &lt;component name="server"&gt;
    &lt;service name="printService" <emphasis role="bold">requires="frascati-debug"</emphasis>&gt;
      &lt;interface.java interface="org.ow2.frascati.examples.helloworld.pojo.PrintService"/&gt;
    &lt;/service&gt;

    &lt;implementation.java class="org.ow2.frascati.examples.helloworld.pojo.Server" /&gt;
  &lt;/component&gt;

&lt;/composite&gt;</programlisting>

  <para>As a result, the <code>run</code> method of the service is logged,
  like the <code>print</code> method executed by the server component:</para>

  <programlisting>CLIENT created.<emphasis role="bold">
[FRASCATI-DEBUG] Entering 'java.lang.Runnable.run' in component 'client'</emphasis>
SERVER created.
SERVER: setting header to '---&gt;&gt; '<emphasis role="bold">
[FRASCATI-DEBUG] Entering 'org.ow2.frascati.examples.helloworld.pojo.PrintService.print' in component 'server'</emphasis>
SERVER: begin printing...
---&gt;&gt; Hello World!
SERVER: print done.<emphasis role="bold">
[FRASCATI-DEBUG] Exiting 'org.ow2.frascati.examples.helloworld.pojo.PrintService.print' from component 'server'
[FRASCATI-DEBUG] Exiting 'java.lang.Runnable.run' from component 'client'</emphasis></programlisting>
</section>
