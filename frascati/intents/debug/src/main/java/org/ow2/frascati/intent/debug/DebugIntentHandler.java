/**
 * OW2 FraSCAti: SCA Intent Debug
 * Copyright (C) 2008-2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.intent.debug;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * {@link IntentHandler} implementation for a simple debug.
 * 
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public class DebugIntentHandler
  implements IntentHandler {

  /**
   * A configurable header to add to debug traces.
   */
  @Property(name= "header")
  protected String header = "[FRASCATI-DEBUG]";

  // --------------------------------------------------------------------------
  // Implementation of the IntentHandler interface
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.tinfi.control.intent.IntentHandler#invoke(IntentJoinPoint)
   */
  public Object invoke(IntentJoinPoint ijp) throws Throwable {

    //
    // Obtain reflective information on the current invocation.
    //
    // The invoked component.
    Component c = ijp.getComponent();
    // Its component name.
    String componentName = Fractal.getNameController(c).getFcName();
    // The invoked method.
    String methodDesc = ijp.getMethod().toString();
    // The name of the invoked SCA service or reference.
    String interfaceName = ijp.getInterface().getFcItfName();
    // Is an SCA service or reference?
    String serviceOrReference = ((InterfaceType)ijp.getInterface().getFcItfType())
                                .isFcClientItf() ? "reference" : "service";

    //
    // Before the current invocation.
    //
    System.err.println(header + " Before calling:");
    System.err.println(header + "\t component: " + componentName);
    System.err.println(header + "\t " + serviceOrReference + ": " + interfaceName);
    System.err.println(header + "\t method: " + methodDesc);

    //
    // Proceed the current invocation.
    //
    Object ret = ijp.proceed();

    //
    // After the current invocation.
    //
    System.err.println(header + " After calling:");
    System.err.println(header + "\t component: " + componentName);
    System.err.println(header + "\t " + serviceOrReference + ": " + interfaceName);
    System.err.println(header + "\t method: " + methodDesc);

    return ret;
  }

}
