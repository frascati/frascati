/***
 * OW2 FraSCAti Bench intent
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.intent.bench.api;

import org.objectweb.fractal.api.Component;

/**
 * This interface provides report services for FraSCAti benchmarks
 * 
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version $Revision$
 *
 */
public interface Report {

  /**
   * Report benchmark statistics of a component
   * @param c the component benchmarked
   * @param executionTime the elapsed execution time for c
   */
  // TODO: report execution time for a given interface and a given method
  public void report(Component c, long executionTime);

}
