/***
 * OW2 FraSCAti Bench intent
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.intent.bench;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.intent.bench.api.Report;
import org.ow2.frascati.intent.bench.api.Statistics;

/**
 * Simple bench implementation component that provides {@link Report}
 * and {@link Statistics} services.
 * 
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version $Revision$
 *
 */
@Scope("COMPOSITE")
public class BenchStatistics
  implements Report,
             Statistics {
  
  /**
   * Benchmarked components
   */
  protected Map<Component,Long> components = new HashMap<Component,Long>();
  
  // --------------------------------------------------------------------------
  // Implementation of the Report interface
  // --------------------------------------------------------------------------
  
  /**
   * @see org.ow2.frascati.intent.bench.api.Report#report(Component, long)
   */
  public void report(Component c, long executionTime) {
    if(components.get(c) == null) {
      components.put(c, new Long(0));
    }
    Long newBench = components.get(c) + Long.valueOf(executionTime);
    components.put(c, newBench);
  }

  // --------------------------------------------------------------------------
  // Implementation of the Statistics interface
  // --------------------------------------------------------------------------
  
  /**
   * @see  org.ow2.frascati.intent.bench.api.Statistics#getStats()
   */
  public String getStats() {
    String ret = "";
    for(Component c : components.keySet()) {
      Long bench = components.get(c);
      String name;
      try {
        name = Fractal.getNameController(c).getFcName();
      } catch (NoSuchInterfaceException e) {
        name = " ## WARNING # can't get component name ##";
      }
      ret += "#=======\n";
      ret += "| component: " + name + "\n";
      ret += "|    has run \t" + bench + " milliseconds.\n";
      ret += "#=======\n";
    }
    return ret;
  }
}
