/***
 * OW2 FraSCAti Bench intent
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.intent.bench;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.intent.bench.api.Statistics;


/**
 * Graphical component providing a button for getting benchmark reports.
 * 
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version $Revision$
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class StatisticsGUI
     extends JPanel
  implements ActionListener {

  /**
   * {@link javax.swing.JPanel} is serializable
   */
  private static final long serialVersionUID = 1L;
  
  /**
   * SCA {@link Reference} that provides statistics
   */
  @Reference
  protected Statistics statistics;
  
  /**
   * Text area that display statistics
   */
  protected JTextArea statArea;

  /**
   * Default constructor that builds the {@link JFrame} with its {@link JButton}
   * and {@link JTextArea} that shows statistics
   */
  public StatisticsGUI() {
    super(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.gridwidth = GridBagConstraints.REMAINDER;
    JButton button = new JButton("Show bench");
    button.addActionListener(this);
    button.setToolTipText("Click this button to display the FraSCAti benchmark");
    statArea = new JTextArea(15, 55);
    statArea.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(statArea);
    c.fill = GridBagConstraints.HORIZONTAL;
    add(button, c);
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 1.0;
    c.weighty = 1.0;
    add(scrollPane, c);
    JFrame frame = new JFrame("Bench");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(this);
    frame.pack();
    frame.setVisible(true);
  }
  
  /**
   * Print statistics
   * @see java.awt.event.ActionListener#actionPerformed(ActionEvent)s
   */
  public void actionPerformed(ActionEvent e) {
    statArea.append("#====================================================\n");
    statArea.append("#                       BENCHMARK REPORT\n");
    statArea.append(statistics.getStats());
  }
}
