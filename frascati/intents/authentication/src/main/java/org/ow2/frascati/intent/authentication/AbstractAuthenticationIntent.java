/**
 * OW2 FraSCAti : Abstract Authentication Intent
 * 
 * Copyright (c) 2009-2010 INRIA, University of Lille 1, Orange Labs
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Rémi Mélisson
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.intent.authentication;

import javax.naming.AuthenticationException;
import javax.security.auth.Subject;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.annotation.Context;
import org.oasisopen.sca.annotation.Scope;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * 
 * Abstract class for authentication intent definition
 * 
 * @author Remi Melisson
 *
 */
@Scope("COMPOSITE")
public abstract class AbstractAuthenticationIntent implements IntentHandler {
	
	@Context
	ComponentContext context;
	
	public Object invoke(IntentJoinPoint ijp) throws Throwable {

		// we retrieve the subject from the request context 
		Subject subject = context.getRequestContext().getSecuritySubject();
		
		// and check its consistency
		if (subject!=null && isAuthenticated(subject)) {
			
			// that's correct, so can we proceed the request
			return ijp.proceed();
		}
		 
		throw new AuthenticationException();
	}
	
	/**
	 * This method should check if the subject is authorized to process
	 * the request. 
	 * 
	 * @param subject The subject associated to a request
	 * @return
	 */
	public abstract boolean isAuthenticated(Subject subject);
}
