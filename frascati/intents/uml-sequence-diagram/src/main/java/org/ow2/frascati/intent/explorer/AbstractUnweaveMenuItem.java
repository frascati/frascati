/**
 * OW2 FraSCAti Intents
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.explorer;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.intent.api.WeaverException;

/**
 * Is the FraSCAti Explorer {@link MenuItem} to call {@link Weaver#unweave(Component)}.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public abstract class AbstractUnweaveMenuItem
              extends AbstractWeaverMenuItem
{
  /**
   * @see AbstractAlwaysEnabledMenuItem#execute()
   */
  @Override
  protected final void execute(Component component)
    throws WeaverException
  {
    getWeaver().unweave(component);
  }
}
