package org.ow2.frascati.intent.umlsequencediagram.explorer;

import org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem;
import org.ow2.frascati.intent.trace.api.Trace;

public class ZoomOut extends AbstractAlwaysEnabledMenuItem<Trace> {
	private static final double MIN_ZOOM = 0.5;

	@Override
	protected void execute(Trace selected) throws Exception {
		double zoomvalue = TracePanel.zoom.get() * .9;
		if (zoomvalue > MIN_ZOOM)
			TracePanel.zoom.set(zoomvalue);
	}
}
