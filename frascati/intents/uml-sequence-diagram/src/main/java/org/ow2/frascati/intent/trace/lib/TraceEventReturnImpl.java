/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import org.ow2.frascati.intent.trace.api.TraceEventReturn;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * OW2 FraSCAti Trace Intent - TraceEventReturn implementation.
 *
 * @author Philippe Merle
 * @version 1.4
 */
public class TraceEventReturnImpl
     extends TraceEventCallImpl
  implements TraceEventReturn
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The value returned.
     */
    private Object returnValue;

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    protected TraceEventReturnImpl(IntentJoinPoint ijp, Object returnValue)
    {
      super(ijp);
      this.returnValue = returnValue;
    }

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Get the returned value.
     */
    public final Object getReturnValue()
    {
      return this.returnValue;
    }

    /**
     * Get the returned value as string.
     */
    public final String getReturnValueAsString()
    {
      if(this.returnValue != null) {
    	if(this.returnValue instanceof String) {
    	  return '\"' + (String)this.returnValue + '\"';
    	}
        return this.returnValue.toString();
      }
      if(getMethod().getReturnType() == void.class) {
        return "void";
      }
      return "null";
    }
}
