/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.explorer;

import java.util.List;

import org.objectweb.util.explorer.api.Entry;

import org.ow2.frascati.intent.trace.api.TraceEventCall;

/**
 * OW2 FraSCAti Explorer plugin for displaying the sub elements
 * of a TraceEventCall instance.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceEventCallContext<TEC extends TraceEventCall>
     extends TraceEventContext<TEC>
{
  // ==================================================================
  // Internal state.
  // ==================================================================

  // TODO: set serial version UID.
  private static final long serialVersionUID = 0L;

  // ==================================================================
  // Internal methods.
  // ==================================================================

  /**
   * Add the sub elements of the selected TraceEventCall instance.
   */
  @Override
  protected void addEntries(TEC traceEventCall, List<Entry> entries)
  {
    // add entries for a TraceEvent instance.
	super.addEntries(traceEventCall, entries);

	// add the trace event call interface as a new entry.
    entries.add( newEntry(traceEventCall.getInterfaceName(), traceEventCall.getInterface()) );

	// add the trace event call method as a new entry.
    entries.add( newEntry(traceEventCall.getMethodName(), traceEventCall.getMethod()) );

	// add the trace event call arguments as a new entry.
    // TODO?
  }

  // ==================================================================
  // Public methods.
  // ==================================================================

}
