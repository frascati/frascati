/**
 * OW2 FraSCAti Intents
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.lib;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.util.Fractal;

import org.ow2.frascati.intent.api.WeaverException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * Weaver of an SCA Intent.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class IntentHandlerWeaver
     extends AbstractWeaver
{
    static interface Applier
    {
      void apply(Component component) throws WeaverException;
    }

    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * Exclude EasyBPEL components.
     */
    @Property(name = "excludes-implementation-bpel")
    private boolean excludesImplementationBpel = true;

    /**
     * Reference to the owning Fractal component.
     */
    @Controller(name = "component")
    private Component component;

    /**
     * SCA reference name for the SCA intent handler to weave.
     */
    private static final String INTENT_HANDLER = "intent-handler";

    /**
     * The SCA intent handler to weave.
     */
    private IntentHandler intentHandler;

    //---------------------------------------------------------------------------
    // SCA methods.
    // --------------------------------------------------------------------------

    @Reference(name = INTENT_HANDLER)
    public void setIntentHandler(IntentHandler ih)
    {
      intentHandler = ih;
    }

    @Init
    public void init()
      throws NoSuchInterfaceException
    {
      // Here we need the real IntentHandler bound to this component.
      intentHandler = (IntentHandler)Fractal.getBindingController(component).lookupFc(INTENT_HANDLER);
    }

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Get the SCA basic intent controller.
     */
    protected final SCABasicIntentController getSCABasicIntentController(Component component)
        throws NoSuchInterfaceException
    {
      return (SCABasicIntentController)component.getFcInterface(SCABasicIntentController.NAME);
    }

    /**
     * Add the SCA intent to an SCA component.
     */
    protected final boolean addIntentHandler(Component component)
        throws WeaverException
    {
      try {
        getSCABasicIntentController(component).addFcIntentHandler(this.intentHandler);
        return true;
      } catch(NoSuchInterfaceException nsie) {
        log.warning("A component has no SCA intent controller");
      } catch(IllegalLifeCycleException ilce) {
        severe(new WeaverException("Can not add intent handler", ilce));
      }
      return false;
    }

    /**
     * Remove the SCA intent from an SCA component.
     */
    protected final void removeIntentHandler(Component component)
        throws WeaverException
    {
      try {
        getSCABasicIntentController(component).removeFcIntentHandler(this.intentHandler);
      } catch(NoSuchInterfaceException nsie) {
        log.warning("A component has no SCA intent controller");
      } catch(IllegalLifeCycleException ilce) {
        severe(new WeaverException("Can not remove intent handler", ilce));
      }
    }

    /**
     * Traverse the SCA component to apply the SCA intent.
     */
    protected final void traverse(Component component, Applier applier)
        throws WeaverException
    {
      if(excludesImplementationBpel && "implementation.bpel".equals(getFractalComponentName(component))) {
        log.warning("Could not traverse a BPEL implementation");
        return;
      }

      // Apply on the component.
      applier.apply(component);

      // Try to get the Fractal content controller.
      ContentController contentController = null;
      try {
        contentController = Fractal.getContentController(component);
      } catch(NoSuchInterfaceException nsie) {
        // Return as component is not a composite.
    	return;
      }

      // Traverse the sub components of the component.
      for(Component c: getFractalSubComponents(contentController)) {
        traverse(c, applier);
      }
    }

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    static long nbApplies;

    /**
     * @see org.ow2.frascati.intent.api.Weaver#weave(Component)
     */
    public final void weave(Component scaComposite) throws WeaverException
    {
      nbApplies = 0;
      long startTime = System.nanoTime();

      // Begin the reconfiguration.
      stopFractalComponent(scaComposite);

      // Add the intent handler on the SCA composite recursively.
      traverse(scaComposite,
               new Applier() {
                 public final void apply(Component component) throws WeaverException
                 {
                   if(addIntentHandler(component)) {
                     nbApplies++;
                   }
                 }
               }
      );

      // End the reconfiguration.
      startFractalComponent(scaComposite);

      long endTime = System.nanoTime();
      log.info("Weare the intent on " + nbApplies + " components in " + (endTime - startTime) + "ns.");
    }

    /**
     * @see org.ow2.frascati.intent.api.Weaver#unweave(Component)
     */
    public final void unweave(Component scaComposite) throws WeaverException
    {
      // Begin the reconfiguration.
      stopFractalComponent(scaComposite);

      // Remove the intent handler on the SCA composite recursively.
      traverse(scaComposite,
              new Applier() {
                public final void apply(Component component) throws WeaverException
                {
                  removeIntentHandler(component);
                }
              }
      );

      // End the reconfiguration.
      startFractalComponent(scaComposite);
	}
}
