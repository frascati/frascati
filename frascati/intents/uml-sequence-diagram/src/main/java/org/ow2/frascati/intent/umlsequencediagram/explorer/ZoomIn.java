package org.ow2.frascati.intent.umlsequencediagram.explorer;

import org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem;
import org.ow2.frascati.intent.trace.api.Trace;

public class ZoomIn extends AbstractAlwaysEnabledMenuItem<Trace> {
	private static final double MAX_ZOOM = 2;

	@Override
	protected void execute(Trace selected) throws Exception {
		double zoomvalue = TracePanel.zoom.get() * 1.1;
		if (zoomvalue < MAX_ZOOM)
			TracePanel.zoom.set(zoomvalue * 1.1);
	}
}
