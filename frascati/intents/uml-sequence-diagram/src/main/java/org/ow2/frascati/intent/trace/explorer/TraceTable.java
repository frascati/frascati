/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.explorer;

import java.util.List;

import org.ow2.frascati.explorer.plugin.AbstractTablePlugin;

import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceEvent;
import org.ow2.frascati.intent.trace.api.TraceEventCall;
import org.ow2.frascati.intent.trace.api.TraceEventReturn;
import org.ow2.frascati.intent.trace.api.TraceEventThrow;

/**
 * OW2 FraSCAti Explorer Table for displaying the a Trace instance.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceTable
     extends AbstractTablePlugin<Trace>
{
    //==================================================================
    // Internal state.
    //==================================================================

    private static final int NB_COLUNMS = 8;

    // ==================================================================
    // Internal methods.
    // ==================================================================

    /**
     * Add the colunm headers.
     */
    @Override
    protected final String[] getHeaders()
    {
      return new String[] {"Id", "Event", "Thread", "Component", "Interface", "Method", "Arguments", "Result/Throw"};
    }

    /**
     * Add the rows associated to the selected instance.
     */
    @Override
    protected final void addRows(Trace trace, List<Object[]> rows)
    {
      int id = 1;
      for(TraceEvent traceEvent : trace.getTraceEvents()) {
        Object[] row = new Object[NB_COLUNMS];
        rows.add(row);

        // colunm 0
        row[0] = Integer.toString(id);
        id++;

        // colunm 1
        row[1] = newEntry("", traceEvent);

        // colunm 2
        Thread thread = traceEvent.getThread();
        row[2] = newEntry(thread.getName(), thread);

        if(traceEvent instanceof TraceEventCall) {
          TraceEventCall traceEventCall = (TraceEventCall)traceEvent;

          // colunm 3
          row[3] = newEntry(traceEventCall.getComponentName(), traceEventCall.getComponent());

          // colunm 4
          row[4] = newEntry(traceEventCall.getInterfaceName(), traceEventCall.getInterface());

          // colunm 5
          row[5] = newEntry(traceEventCall.getMethodName(), traceEventCall.getMethod());

          // colunm 6
          row[6] = traceEventCall.getArgumentsAsString();

          // colunm 7
          String result = "";
          if(traceEventCall instanceof TraceEventReturn) {
            result = ((TraceEventReturn)traceEventCall).getReturnValueAsString();
          } else if(traceEventCall instanceof TraceEventThrow) {
            result = ((TraceEventThrow)traceEventCall).getThrowableAsString();
          }
          row[7] = result;
        } else {
          row[7] = traceEvent.toString();
        }
      }
    }

    //==================================================================
    // Public methods.
    //==================================================================

}
