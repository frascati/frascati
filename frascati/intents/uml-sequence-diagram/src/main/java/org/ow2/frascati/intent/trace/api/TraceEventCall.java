/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.api;

import java.lang.reflect.Method;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * OW2 FraSCAti Trace Intent - TraceEventCall interface.
 *
 * @author Philippe Merle
 * @version 1.4
 */
public interface TraceEventCall
         extends TraceEvent
{
    /**
     * Get the IntentJoinPoint instance of this trace event call.
     */
    IntentJoinPoint getIntentJoinPoint();

    /**
     * Get the Interface instance of this trace event call.
     */
    Interface getInterface();

    /**
     * Get the interface name of this trace event call.
     */
    String getInterfaceName();

    /**
     * Is getInterface() returns an SCA service.
     */
    boolean isScaService();

    /**
     * Is getInterface() returns an SCA reference.
     */
    boolean isScaReference();

    /**
     * Get the InterfaceType instance of this trace event call.
     */
    InterfaceType getInterfaceType();

    /**
     * Get the interface type signature of this trace event call.
     */
    String getInterfaceSignature();

    /**
     * Get the method of this trace event call.
     */
    Method getMethod();

    /**
     * Get the method name of this trace event call.
     */
    String getMethodName();

    /**
     * Get the method arguments of this trace event call.
     */
    Object[] getArguments();

    /**
     * Get the arguments of this trace event call as a string.
     */
    String getArgumentsAsString();
}
