/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.explorer;

import java.util.List;

import org.objectweb.util.explorer.api.Entry;
import org.ow2.frascati.explorer.plugin.AbstractContextPlugin;

import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceManager;

/**
 * The FraSCAti Explorer Context for displaying the list of traces of a TraceManager instance.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceManagerContext
     extends AbstractContextPlugin<TraceManager>
{
  // ==================================================================
  // Internal state.
  // ==================================================================

  // TODO: set serial version UID.
  private static final long serialVersionUID = 0L;

  // ==================================================================
  // Constructors.
  // ==================================================================

  // ==================================================================
  // Internal methods.
  // ==================================================================

  /**
   * Add the traces of the selected trace manager.
   */
  @Override
  protected final void addEntries(TraceManager traceManager, List<Entry> entries)
  {
    for (Trace trace : traceManager.getTraces()) {
      entries.add( newEntry(trace.getName(), trace) );
    }
  }

  // ==================================================================
  // Public methods.
  // ==================================================================

}
