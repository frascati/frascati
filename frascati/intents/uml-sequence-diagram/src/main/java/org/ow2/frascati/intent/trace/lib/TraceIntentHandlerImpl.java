/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import java.lang.reflect.Method;
import javax.jws.Oneway;

import org.osoa.sca.annotations.OneWay;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.intent.lib.AbstractIntentHandler;
import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceManager;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * OW2 FraSCAti Trace Intent - IntentHandler implementation.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceIntentHandlerImpl
     extends AbstractIntentHandler
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * Reference to the trace manager.
     */
    @Reference(name = "trace-manager")
    private TraceManager traceManager;

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.tinfi.control.intent.IntentHandler#invoke(IntentJoinPoint)
     */
    public final Object invoke(final IntentJoinPoint ijp)
      throws Throwable
    {
       // Get the trace associated to the current thread.
       Trace trace = Trace.TRACE.get();
       boolean isNewTrace = (trace == null);
       if(isNewTrace) {
         // Create a new trace.
         trace = traceManager.newTrace();
         // Attach the trace to the current thread.
         Trace.TRACE.set(trace);
       }

       Method invokedMethod = ijp.getMethod();
       boolean asyncCall = (invokedMethod.getAnnotation(Oneway.class) != null)  // WS oneway
                        || (invokedMethod.getAnnotation(OneWay.class) != null); // SCA oneway

       if(asyncCall) {
         // Add a new trace event async call to the trace.
         trace.addTraceEvent(new TraceEventAsyncCallImpl(ijp));

         try {
           // Process the invocation.
           ijp.proceed();
         } finally {
           if(isNewTrace) {
             // Deattach the trace from the current thread.
             Trace.TRACE.set(null);
           }
         }

         // Add a new trace event async end to the trace.
         trace.addTraceEvent(new TraceEventAsyncEndImpl(ijp));

         return null;

       } else {
         // Add a new trace event call to the trace.
         trace.addTraceEvent(new TraceEventCallImpl(ijp));

         Object result = null;
         try {
           // Process the invocation.
           result = ijp.proceed();
         } catch(Throwable throwable) {
           // Add a new trace event throw to the trace.
           trace.addTraceEvent(new TraceEventThrowImpl(ijp, throwable));
           throw throwable;
         } finally {
           if(isNewTrace) {
             // Deattach the trace from the current thread.
             Trace.TRACE.set(null);
           }
         }
         // Add a new trace event return to the trace.
         trace.addTraceEvent(new TraceEventReturnImpl(ijp, result));

         return result;
       }
    }
}
