/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import org.ow2.frascati.intent.trace.api.TraceEventThrow;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * OW2 FraSCAti Trace Intent - TraceEventThrow implementation.
 *
 * @author Philippe Merle
 * @version 1.4
 */
public class TraceEventThrowImpl
     extends TraceEventCallImpl
  implements TraceEventThrow
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The throwable instance.
     */
    private Throwable throwable;

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    protected TraceEventThrowImpl(IntentJoinPoint ijp, Throwable throwable)
    {
      super(ijp);
      this.throwable = throwable;
    }

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Get the throwable instance.
     */
    public final Throwable getThrowable()
    {
      return this.throwable;
    }

    /**
     * Get the throwable instance as a string.
     */
    public final String getThrowableAsString()
    {
      return this.throwable.toString();    
    }
}
