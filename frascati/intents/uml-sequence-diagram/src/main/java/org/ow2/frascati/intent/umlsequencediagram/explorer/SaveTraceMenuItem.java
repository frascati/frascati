/**
 * OW2 FraSCAti UML Sequence Diagram Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.umlsequencediagram.explorer;

import java.awt.Image;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import net.sf.sdedit.config.Configuration;
import net.sf.sdedit.config.ConfigurationManager;
import net.sf.sdedit.diagram.Diagram;
import net.sf.sdedit.text.TextHandler;
import net.sf.sdedit.ui.ImagePaintDevice;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem;
import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.umlsequencediagram.api.Constants;
import org.ow2.frascati.intent.umlsequencediagram.lib.TraceDiagramGenerator;

/**
 * Save a trace into a file.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class SaveTraceMenuItem extends AbstractAlwaysEnabledMenuItem<Trace> {
	// --------------------------------------------------------------------------
	// Internal state
	// --------------------------------------------------------------------------

	/**
	 * The {@link JFileChooser} instance.
	 */
	private static JFileChooser fileChooser = null;

	// --------------------------------------------------------------------------
	// Internal methods
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Constructor
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// AbstractAlwaysEnabledMenuItem
	// --------------------------------------------------------------------------

	/**
	 * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute()
	 */
	protected final void execute(Trace trace) throws Exception {
		// At the first call, creates the file chooser.
		if (fileChooser == null) {
			fileChooser = new JFileChooser();
			// Sets to the user's current directory.
			fileChooser.setCurrentDirectory(new File(System
					.getProperty("user.dir")));
			fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			// Filters all files.
			fileChooser.setFileFilter(new FileFilter() {
				/** Whether the given file is accepted by this filter. */
				public final boolean accept(File f) {
					return f.isDirectory() || f.getName().endsWith(".png");
				}

				/** The description of this filter. */
				public final String getDescription() {
					return "PNG image";
				}
			});
		}

		// Opens the file chooser to select a file.
		int returnVal = fileChooser.showSaveDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			// Get the OW2 FraSCAti UML Sequence Diagram Intent TraceGenerator
			// service.
			TraceDiagramGenerator traceGenerator = (TraceDiagramGenerator) getFraSCAtiExplorerService(
					CompositeManager.class).getComposite(
					Constants.SCA_COMPOSITE_NAME).getFcInterface(
					Constants.TRACE_GENERATOR_SERVICE_NAME);

			ImagePaintDevice ipd = new ImagePaintDevice();

			Configuration dataObject = (Configuration) ConfigurationManager
					.createNewDefaultConfiguration().getDataObject();

			try {
				new Diagram(dataObject,
						traceGenerator.getDiagramDataProvider(trace), ipd)
						.generate();
			} catch (Exception ex) {
				ex.printStackTrace();
				return;
			}

			ipd.saveImage(fileChooser.getSelectedFile());
		}
	}

	// --------------------------------------------------------------------------
	// Public methods.
	// --------------------------------------------------------------------------

}
