/**
 * OW2 FraSCAti UML Sequence Diagram Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * Nicolas Petitprez
 */

package org.ow2.frascati.intent.umlsequencediagram.explorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import net.sf.sdedit.config.Configuration;
import net.sf.sdedit.config.ConfigurationManager;
import net.sf.sdedit.diagram.Diagram;
import net.sf.sdedit.ui.PanelPaintDevice;
import net.sf.sdedit.ui.components.ZoomPane;

import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.explorer.api.FraSCAtiExplorer;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;
import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.umlsequencediagram.api.Constants;
import org.ow2.frascati.intent.umlsequencediagram.lib.TraceDiagramGenerator;

/**
 * Is the FraSCAti Explorer plugin to show {@link Trace} instances.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TracePanel extends AbstractSelectionPanel<Trace> implements
		PropertyChangeListener {
	public static final Property<Double> zoom = new PropertyImpl();

	ZoomPane zoomPanel = new ZoomPane();

	public void propertyChange(PropertyChangeEvent evt) {
		zoomPanel.setScale(zoom.get());
	}

	/**
	 * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
	 */
	public void selected(TreeView treeView) {
		zoom.addPropertyChangeListener(this);
		setMinimumSize(new Dimension(0, 0));
		setPreferredSize(new Dimension(800, 600));

		Trace trace = (Trace) treeView.getSelectedObject();

		TraceDiagramGenerator diagramProvider;

		try {
			diagramProvider = (TraceDiagramGenerator) FraSCAtiExplorer.SINGLETON
					.get().getFraSCAtiExplorerService(CompositeManager.class)
					.getComposite(Constants.SCA_COMPOSITE_NAME)
					.getFcInterface(Constants.TRACE_GENERATOR_SERVICE_NAME);
		} catch (Exception e1) {
			e1.printStackTrace();
			return;
		}

		PanelPaintDevice paintDevice = new PanelPaintDevice(true);
		Configuration dataObject = (Configuration) ConfigurationManager
				.createNewDefaultConfiguration().getDataObject();

		Diagram diagram = new Diagram(dataObject,
				diagramProvider.getDiagramDataProvider(trace), paintDevice);
		try {
			diagram.generate();
		} catch (Exception e) {
			e.printStackTrace();
		}

		zoomPanel.setViewportView(paintDevice.getPanel());
		if (zoom.get() == null)
			zoom.set(1d);
		zoomPanel.setScale(zoom.get());
		setLayout(new BorderLayout());
		add(BorderLayout.CENTER, zoomPanel);

		// Refresh the Swing Panel.
		validate();
	}

	/**
	 * @see org.objectweb.util.explorer.api.Panel#unselected(org.objectweb.util.explorer.api.TreeView)
	 */
	public void unselected(TreeView treeView) {
		zoom.removePropertyChangeListener(this);
	}

}
