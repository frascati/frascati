/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import java.util.ArrayList;
import java.util.List;

import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceManager;

/**
 * OW2 FraSCAti Trace Intent - TraceManager implementation.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
@Scope("COMPOSITE")
public class TraceManagerImpl
  implements TraceManager
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * List of all traces.
     */
    private List<Trace> traceList = new ArrayList<Trace>();

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Create a new trace.
     */
    public final Trace newTrace()
    {
      Trace trace = new TraceImpl();
      this.traceList.add(trace);
      return trace;
    }

    /**
     * Remove a trace.
     */
    public final void removeTrace(Trace trace)
    {
      this.traceList.remove(trace);
    }

    /**
     * Get all traces.
     */
    public final Trace[] getTraces()
    {
      return this.traceList.toArray(new Trace[this.traceList.size()]);
    }
}
