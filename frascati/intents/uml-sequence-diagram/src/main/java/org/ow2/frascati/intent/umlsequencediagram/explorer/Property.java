package org.ow2.frascati.intent.umlsequencediagram.explorer;

import java.beans.PropertyChangeListener;

public interface Property<TYPE> {

	public void addPropertyChangeListener(PropertyChangeListener listener);

	public TYPE get();

	public void removePropertyChangeListener(PropertyChangeListener listener);

	public void set(TYPE value);

}
