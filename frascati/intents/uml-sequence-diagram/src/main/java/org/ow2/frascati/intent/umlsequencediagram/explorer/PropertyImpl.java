package org.ow2.frascati.intent.umlsequencediagram.explorer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class PropertyImpl<TYPE> implements Property<TYPE> {

	private final PropertyChangeSupport support = new PropertyChangeSupport(
			this);

	private TYPE value;

	@org.osoa.sca.annotations.Property
	protected String name;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		support.addPropertyChangeListener(listener);
	}

	public TYPE get() {
		return value;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		support.removePropertyChangeListener(listener);
	}

	@Override
	public String toString() {
		return "Property [name=" + name + ", value=" + value + "]";
	}

	public void set(TYPE value) {
		if (this.value != value) {
			TYPE old = this.value;
			this.value = value;
			support.firePropertyChange(name, old, value);
		}
	}

}
