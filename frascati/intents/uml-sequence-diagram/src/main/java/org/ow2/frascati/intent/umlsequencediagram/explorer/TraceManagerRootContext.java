/**
 * OW2 FraSCAti UML Sequence Diagram Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.umlsequencediagram.explorer;

import org.objectweb.util.explorer.api.RootContext;
import org.objectweb.util.explorer.api.RootEntry;
import org.objectweb.util.explorer.core.root.lib.DefaultRootEntry;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.explorer.plugin.AbstractPlugin;
import org.ow2.frascati.intent.umlsequencediagram.api.Constants;

/**
 * OW2 FraSCAti UML Sequence Diagram root context plugin.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceManagerRootContext 
     extends AbstractPlugin
  implements RootContext
{
  // ==================================================================
  // Internal state.
  // ==================================================================

  // TODO: set serial version UID.
  private static final long serialVersionUID = 0L;

  // ==================================================================
  // Constructors.
  // ==================================================================

  // ==================================================================
  // Internal methods.
  // ==================================================================

  // ==================================================================
  // Public methods.
  // ==================================================================

  public RootEntry[] getEntries()
  {
	try {
      Object traceManager = getFraSCAtiExplorerService(CompositeManager.class)
                            .getComposite(Constants.SCA_COMPOSITE_NAME)
                            .getFcInterface(Constants.TRACE_MANAGER_SERVICE_NAME);
      return new RootEntry[] {
        new DefaultRootEntry("UML Sequence Diagrams", traceManager, 0)
      };
    } catch(Exception exc) {
      exc.printStackTrace();
      return new RootEntry[0];
    }
  }
}
