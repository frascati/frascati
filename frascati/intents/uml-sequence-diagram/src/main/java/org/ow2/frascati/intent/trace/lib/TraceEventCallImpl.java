/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import java.lang.reflect.Method;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;

import org.ow2.frascati.intent.trace.api.TraceEventCall;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * OW2 FraSCAti Trace Intent - TraceEventCall implementation.
 *
 * @author Philippe Merle
 * @version 1.4
 */
public class TraceEventCallImpl
     extends TraceEventImpl
  implements TraceEventCall
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The IntentJoinPoint of this trace event call.
     */
    private IntentJoinPoint ijp;
	
    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    protected TraceEventCallImpl(IntentJoinPoint ijp)
    {
      super();
      this.ijp = ijp;
    }

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Get the IntentJoinPoint instance of this trace event call.
     */
    public final IntentJoinPoint getIntentJoinPoint()
    {
      return this.ijp;
    }

    /**
     * Get the Component instance of this trace event call.
     */
    public final Component getComponent()
    {
      return this.ijp.getComponent();
    }

    /**
     * Get the Interface instance of this trace event call.
     */
    public final Interface getInterface()
    {
      return this.ijp.getInterface();
    }

    /**
     * Get the interface name of this trace event call.
     */
    public final String getInterfaceName()
    {
      return getInterface().getFcItfName();
    }

    /**
     * Is an SCA service.
     */
    public final boolean isScaService()
    {
      return getInterfaceType().isFcClientItf() ? false : true;
    }

    /**
     * Is an SCA reference.
     */
    public final boolean isScaReference()
    {
      return getInterfaceType().isFcClientItf();
    }

    /**
     * Get the InterfaceType instance of this trace event call.
     */
    public final InterfaceType getInterfaceType()
    {
      return (InterfaceType)this.ijp.getInterface().getFcItfType();
    }

    /**
     * Get the interface type signature of this trace event call.
     */
    public final String getInterfaceSignature()
    {
      return getInterfaceType().getFcItfSignature();
    }

    /**
     * Get the method of this trace event call.
     */
    public final Method getMethod()
    {
      return this.ijp.getMethod();
    }
    
    /**
     * Get the method name of this trace event call.
     */
    public final String getMethodName()
    {
      return getMethod().getName();
    }

    /**
     * Get the arguments of this trace event call.
     */
    public final Object[] getArguments()
    {
      return this.ijp.getArguments();
    }

    /**
     * Get the arguments of this trace event call as a string.
     */
    public final String getArgumentsAsString()
    {
      StringBuilder sb = new StringBuilder();
      boolean notFirst = false;
      for(Object argument : getArguments()) {
        if(notFirst) {
          sb.append(", ");
        }
        if(argument == null) {
          sb.append("null");
        } else if(argument instanceof String) {
          sb.append('\"');
          sb.append(argument.toString());
          sb.append('\"');
        } else {
          sb.append(argument.toString());
        }
        notFirst = true;
      }
      return sb.toString();
    }
}
