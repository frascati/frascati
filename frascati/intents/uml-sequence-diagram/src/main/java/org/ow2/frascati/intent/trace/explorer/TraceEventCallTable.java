/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.explorer;

import java.util.List;

import org.ow2.frascati.explorer.plugin.AbstractTablePlugin;

import org.ow2.frascati.intent.trace.api.TraceEventCall;

/**
 * OW2 FraSCAti Explorer Table for displaying a TraceEventCall instance.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceEventCallTable<TEC extends TraceEventCall>
     extends AbstractTablePlugin<TEC>
{
    //==================================================================
    // Internal state.
    //==================================================================

    protected static final int NB_COLUNMS = 2;

    // ==================================================================
    // Internal methods.
    // ==================================================================
    
    /**
     * Add the column headers.
     */
    @Override
    protected final String[] getHeaders()
    {
      return new String[] {"Attribute", "Value"};
    }

    /**
     * Add the rows associated to the selected instance.
     */
    @Override
    protected void addRows(TEC traceEventCall, List<Object[]> rows)
    {
      Object[] line;

      // line 1
      Thread thread = traceEventCall.getThread();
      line = new Object[NB_COLUNMS];
      line[0] = "Thread";
      line[1] = newEntry(thread.getName(), thread);
      rows.add(line);

      // line 2
      line = new Object[NB_COLUNMS];
      line[0] = "Component";
      line[1] = newEntry(traceEventCall.getComponentName(), traceEventCall.getComponent());
      rows.add(line);

      // line 3
      line = new Object[NB_COLUNMS];
      line[0] = "Interface";
      line[1] = newEntry(traceEventCall.getInterfaceName(), traceEventCall.getInterface());
      rows.add(line);

      // line 4
      line = new Object[NB_COLUNMS];
      line[0] = "Method";
      line[1] = newEntry(traceEventCall.getMethodName(), traceEventCall.getMethod());
      rows.add(line);

      // line 5
      line = new Object[NB_COLUNMS];
      line[0] = "Arguments";
      line[1] = traceEventCall.getArgumentsAsString();
      rows.add(line);
    }

    //==================================================================
    // Public methods.
    //==================================================================

}
