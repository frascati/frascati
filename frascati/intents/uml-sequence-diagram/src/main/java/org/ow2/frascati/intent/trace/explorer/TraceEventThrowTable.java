/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.explorer;

import java.util.List;

import org.ow2.frascati.intent.trace.api.TraceEventThrow;

/**
 * OW2 FraSCAti Explorer Table for displaying a TraceEventThrow instance.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class TraceEventThrowTable
     extends TraceEventCallTable<TraceEventThrow>
{
    //==================================================================
    // Internal state.
    //==================================================================

    // ==================================================================
    // Internal methods.
    // ==================================================================
    
    /**
     * Add the rows associated to the selected instance.
     */
    @Override
    protected final void addRows(TraceEventThrow traceEventThrow, List<Object[]> rows)
    {
      super.addRows(traceEventThrow, rows);

      Object[] line = new Object[NB_COLUNMS];
      line[0] = "Throw";
      line[1] = traceEventThrow.getThrowableAsString();
      rows.add(line);
    }

    //==================================================================
    // Public methods.
    //==================================================================

}
