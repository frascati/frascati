/**
 * OW2 FraSCAti UML Sequence Diagram Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Petitprez
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.intent.umlsequencediagram.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import net.sf.sdedit.diagram.DiagramDataProvider;
import net.sf.sdedit.text.TextHandler;

import org.oasisopen.sca.annotation.Property;
import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceEvent;
import org.ow2.frascati.intent.trace.api.TraceEventAsyncCall;
import org.ow2.frascati.intent.trace.api.TraceEventAsyncEnd;
import org.ow2.frascati.intent.trace.api.TraceEventCall;
import org.ow2.frascati.intent.trace.api.TraceEventReturn;
import org.ow2.frascati.intent.trace.api.TraceEventThrow;

public class TextDiagramGenerator implements TraceDiagramGenerator {

	@Property
	protected int messageLength = 20;

	@Property
	protected boolean showCompositeAsFragment = false;

	@Property
	protected boolean showReference = false;

	private class Actor {
		private String label;
		private final String name;
		private String type;

		public Actor(String name, String type, String label) {
			super();
			this.name = name;
			this.type = type;
			this.label = label;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Actor other = (Actor) obj;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		public String getLabel() {
			return label;
		}

		public String getName() {
			return name;
		}

		public String getType() {
			return type;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public String toString() {
			return name + ":" + type
					+ (label != null ? (" \"" + label + "\"") : "");
		}
	}

	private class Calling {
		TraceEventCall call;
		boolean newThread = false;
		TraceEventReturn returned;
		Actor src;
		Actor target;
		TraceEventThrow thro;
		String noteId;

		/**
		 * @param noteId
		 *            the noteId to set
		 */
		public void setNoteId(String noteId) {
			this.noteId = noteId;
		}

		@Override
		public String toString() {
			StringBuffer buffer = new StringBuffer();
			if (noteId != null) {
				buffer.append("(").append(noteId).append(")");
			}

			// src part
			if (src != null)
				buffer.append(src.getName()).append(":");

			if (newThread) {
				buffer.append(">");
			} else {
				if (returned != null)
					buffer.append(cleanStr(returned.getReturnValueAsString()))
							.append("=");
				else if (thro != null)
					buffer.append(cleanStr(thro.getThrowableAsString()))
							.append("=");
			}

			if (target != null) {
				buffer.append(target.getName());
			}
			buffer.append(".").append(call.getMethodName()).append("(")
					.append(cleanStr(call.getArgumentsAsString())).append(")");

			return buffer.toString();
		}
	}

	private String cleanStr(String toclean) {
		toclean = toclean.trim();
		toclean = toclean.replaceAll("\n", " ");
		toclean = toclean.replaceAll(":", "\\\\:");
		// toclean = toclean.replaceAll("\\.", "\\\\.");

		if (toclean.length() > messageLength)
			toclean = toclean.substring(0, messageLength) + "...";
		return toclean;
	}

	List<Actor> actors = new ArrayList<Actor>();
	List<Object> diagramEvent = new ArrayList<Object>();

	protected Actor addActor(String name, String type, String label) {
		for (Actor actor : actors) {
			if (actor.getName().equals(name))
				return actor;
		}
		Actor actor = new Actor(name, type, label);
		actors.add(actor);
		return actor;
	}

	private String getType(TraceEventCall call) {
		if (call.isScaComposite())
			return "Composite";
		if (call.isScaService())
			return "Service";
		if (call.isScaReference())
			return "Reference";
		return call.getComponentName();
	}

	public DiagramDataProvider getDiagramDataProvider(Trace trace) {
		try {

			Stack<Calling> currentStack = new Stack<Calling>();

			for (TraceEvent event : trace.getTraceEvents()) {
				// add a section to show composites
				if (showCompositeAsFragment && event.isScaComposite()) {
					if (event instanceof TraceEventReturn
							|| event instanceof TraceEventThrow) {
						if (diagramEvent.get(diagramEvent.size() - 1)
								.toString().startsWith("[c:"))
							diagramEvent.remove(diagramEvent.size() - 1);
						else
							diagramEvent.add("[/c]");
					} else {
						diagramEvent
								.add("[c:" + event.getComponentName() + "]");
					}
				} else {
					TraceEventCall tracecall = (TraceEventCall) event;
					if (!showReference && tracecall.isScaReference())
						continue;
					if (event instanceof TraceEventReturn) {
						Calling pop = currentStack.pop();
						TraceEventReturn ret = (TraceEventReturn) tracecall;
						pop.returned = ret;
					} else if (event instanceof TraceEventThrow) {
						Calling pop = currentStack.pop();
						TraceEventThrow ret = (TraceEventThrow) event;
						pop.thro = ret;
					} else {
						Actor prev = currentStack.isEmpty() ? addActor(
								trace.getName(), trace.getName(),
								trace.getName()) : currentStack.peek().target;
						Actor actor = addActor(tracecall.getComponentName(),
								getType(tracecall), null);
						Calling call = new Calling();
						call.call = tracecall;
						call.src = prev;
						call.target = actor;
						call.newThread = tracecall instanceof TraceEventAsyncCall
								|| tracecall instanceof TraceEventAsyncEnd;
						currentStack.push(call);
						diagramEvent.add(call);
					}
				}
			}
			if (!currentStack.isEmpty()) {
				String noteId = "1";
				diagramEvent.add("*" + noteId + " "
						+ currentStack.peek().src.getName()
						+ "\nEnd of execution\n*1");
				currentStack.peek().setNoteId("1");
			}

			StringBuffer buffer = new StringBuffer();

			for (Actor actor : actors) {
				buffer.append(actor).append("\n");
			}
			buffer.append("\n");
			for (Object object : diagramEvent) {
				buffer.append(object.toString()).append("\n");
			}
			return new TextHandler(buffer.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new TextHandler("");
	}
}
