/**
 * OW2 FraSCAti Intents
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.api;

import org.ow2.frascati.util.FrascatiException;

/**
 * OW2 FraSCAti Intent Weaver exception.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class WeaverException
     extends FrascatiException
{
  // TODO: set serial version UID
  private static final long serialVersionUID = 0L;

  /**
   * Constructs a weaver exception instance.
   */
  public WeaverException()
  {
    super();
  }

  /**
   * Constructs a weaver exception instance.
   *
   * @param message the message of this exception.
   */
  public WeaverException(String message)
  {
    super(message);
  }

  /**
   * Constructs a weaver exception instance.
   *
   * @param message the message of this exception.
   * @param cause the cause of this exception.
   */
  public WeaverException(String message, Throwable cause)
  {
    super(message, cause);
  }

  /**
   * Constructs a weaver exception instance.
   *
   * @param cause the cause of this exception.
   */
  public WeaverException(Throwable cause)
  {
    super(cause);
  }
}
