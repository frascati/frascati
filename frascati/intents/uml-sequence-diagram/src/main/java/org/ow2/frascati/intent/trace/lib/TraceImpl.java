/**
 * OW2 FraSCAti Trace Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.trace.lib;

import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.intent.trace.api.Trace;
import org.ow2.frascati.intent.trace.api.TraceEvent;

/**
 * OW2 FraSCAti Trace Intent - Trace implementation.
 *
 * @author Philippe Merle
 * @version 1.4
 */
public class TraceImpl
  implements Trace
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The trace's name.
     */
    private String name;

    /**
     * The list of trace events.
     */
    private List<TraceEvent> traceEventList = new ArrayList<TraceEvent>();

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    protected TraceImpl()
    {
      // The name of the trace is by default the name of the current thread.
      this.name = Thread.currentThread().getName();
    }

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Get the trace name.
     */
    public final String getName()
    {
      return this.name;
    }

    /**
     * Add a trace event.
     *
     * This is a synchronized method as several children threads could call it concurrently.
     */
    public final synchronized void addTraceEvent(TraceEvent traceEvent)
    {
      this.traceEventList.add(traceEvent);
    }

    /**
     * Get all trace events.
     */
    public final TraceEvent[] getTraceEvents()
    {
      return this.traceEventList.toArray(new TraceEvent[this.traceEventList.size()]);
    }
}
