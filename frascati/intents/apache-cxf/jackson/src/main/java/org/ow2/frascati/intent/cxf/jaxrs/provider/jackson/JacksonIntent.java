/**
 * OW2 FraSCAti SCA Intents for Apache CXF JAX-RS with Jackson
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.jaxrs.provider.jackson;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.provider.ProviderFactory;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import org.ow2.frascati.intent.cxf.AbstractBindingIntentHandler;

/**
 * Intent for using Jackson to (un)marshall JSON data.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class JacksonIntent
      extends AbstractBindingIntentHandler
{
	@Override
    public final void apply(Object proxy)
    {
	  // Get the endpoint associated to an Apache CXF JAX-RS client.
      Endpoint endpoint = WebClient.getConfig(proxy).getConduitSelector().getEndpoint();
      // Get the provider factory attached to this endpoint.
      ProviderFactory providerFactory = (ProviderFactory)endpoint.get(ProviderFactory.class.getName());
      // Add a Jackson-based JSON provider instance.
      providerFactory.registerUserProvider(new JacksonJsonProvider());

      super.apply(proxy);
    }
}
