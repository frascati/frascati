/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.feature;

import org.apache.cxf.BusFactory;
import org.apache.cxf.feature.Feature;
import org.apache.cxf.interceptor.InterceptorProvider;

import org.ow2.frascati.intent.cxf.AbstractInterceptorProviderIntent;

/**
 * Abstract FraSCAti binding intent for configuring Apache CXF feature.
 *
 * @author Philippe Merle - Inria
 */
public abstract class AbstractFeatureBasedIntent<FeatureType extends Feature>
              extends AbstractInterceptorProviderIntent
           implements FeatureFactory<FeatureType>
{
    // ----------------------------------------------------------------------
    // Internal state.
    // ----------------------------------------------------------------------

    /**
     * Apache CXF feature.
     */
    private FeatureType feature = newFeature();

    // ----------------------------------------------------------------------
    // Internal method.
    // ----------------------------------------------------------------------

    /**
     * Get the Apache CXF feature.
     */
    protected final FeatureType getFeature()
    {
      return this.feature;
    }

    // ----------------------------------------------------------------------
    // Related to class AbstractInterceptorProviderIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure Apache CXF interceptors.
     *
     * @param interceptorProvider the Apache CXF interceptor provider to configure.
     *
     * @see AbstractInterceptorProviderIntent#configure(InterceptorProvider)
     */
    protected final void configure(InterceptorProvider interceptorProvider)
    {
      this.feature.initialize(interceptorProvider, BusFactory.getDefaultBus());
    }

}
