/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.logging;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import org.ow2.frascati.intent.cxf.AbstractEndpointIntent;

/**
 * Intent to logging Apache CXF messages.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class MessageLoggingIntent
     extends AbstractEndpointIntent
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
    // Related to class AbstractEndpointIntent.
    // ----------------------------------------------------------------------

	/**
	 * @see AbstractEndpointIntent#configure(Endpoint)
	 */
    protected void configure(Endpoint endpoint)
    {
      // Add an interceptor on incoming and outgoing requests for the endpoint.
      endpoint.getInInterceptors().add(new MyInterceptor());
      endpoint.getInInterceptors().add(new MyInterceptor());
    }

    // ----------------------------------------------------------------------
    // Internal classes.
    // ----------------------------------------------------------------------

    class MyInterceptor extends AbstractPhaseInterceptor<Message>
    {
      public MyInterceptor()
      {
        super(Phase.USER_LOGICAL);
      }

      public final void handleMessage(Message message)
      {
        for(java.util.Map.Entry<String, Object> entry : message.entrySet()) {
          System.out.println("- " + entry.getKey() + '=' + entry.getValue());
        }
      }
    }
}
