/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.endpoint.property;

import org.apache.cxf.endpoint.Endpoint;

import org.ow2.frascati.intent.cxf.AbstractEndpointIntent;

/**
 * Abstract binding intent class for configuring an Apache CXF endpoint property.
 *
 * @author Philippe Merle - Inria
 * @since 1.5
 */
public abstract class AbstractEndpointPropertyIntent<PropertyType extends Object>
              extends AbstractEndpointIntent
{
    // ----------------------------------------------------------------------
    // Related to interface AbstractEndpointIntent.
    // ----------------------------------------------------------------------

    /**
     * @see AbstractEndpointIntent#configure(Endpoint)
     */
    protected final void configure(Endpoint endpoint)
    {
      String propertyName = getPropertyName();
      PropertyType propertyValue = getPropertyValue();
      info("Set an endpoint property name " + propertyName + " with value " + propertyValue);
      endpoint.put(propertyName, propertyValue);
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Get the name of the endpoint property to configure.
     *
     * @return the name of the endpoint property to configure.
     */
    protected abstract String getPropertyName();

    /**
     * Get the value of the endpoint property to configure.
     *
     * @return the value of the endpoint property to configure.
     */
    protected abstract PropertyType getPropertyValue();
}
