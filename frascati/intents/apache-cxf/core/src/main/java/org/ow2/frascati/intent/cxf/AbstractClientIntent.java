/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf;

import org.apache.cxf.endpoint.Client;

/**
 * Abstract binding intent class for configuring Apache CXF Client.
 *
 * @author Philippe Merle - Inria
 */
public abstract class AbstractClientIntent
              extends AbstractBindingIntentHandler
{
    // ----------------------------------------------------------------------
    // Related to interface BindingIntentHandler.
    // ----------------------------------------------------------------------

    /**
     * Apply this binding intent on a proxy instance.
     *
     * @param proxy the proxy instance to configure.
     *
     * @see BindingIntentHandler#apply(Object)
     */
    @Override
    public final void apply(Object proxy)
    {
      // Obtain the client associated to the proxy.
      Client client = null;
      // Is the proxy a Apache CXF Web Service client?
//  if(proxy instanceof org.apache.cxf.jaxws.JaxWsClientProxy) {
      client = org.apache.cxf.frontend.ClientProxy.getClient(proxy);
      if(client == null) {
        throw new IllegalArgumentException(proxy + " is not an Apache CXF Web Service client!");
      }

      // Configure the client.
      configure(client);

      super.apply(proxy);
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF client instance.
     *
     * @param conduit the Apache CXF client to configure.
     */
    protected abstract void configure(Client client);
}
