/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.jaxrs;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.intent.cxf.AbstractBindingIntentHandler;

/**
 * Intent to add an implicit query parameter to REST requests.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class ImplicitQueryParamIntent
     extends AbstractBindingIntentHandler
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

	/**
	 * Name of the implicit query param.
	 */
	@Property(name="name")
	private String implicitQueryParamName;

	/**
	 * Value of the implicit query param.
	 */
	@Property(name="value")
	private String implicitQueryParamValue;

	// ----------------------------------------------------------------------
    // Related to interface BindingIntentHandler.
    // ----------------------------------------------------------------------

    /**
     * Apply this binding intent on a proxy instance.
     *
     * @param proxy the proxy instance to configure.
     *
     * @see BindingIntentHandler#apply(Object)
     */
    @Override
    public final void apply(Object proxy)
    {
      // Obtain the endpoint associated to the proxy instance.
      Endpoint endpoint = null;

      // Is the proxy a Apache CXF REST client?
      if(proxy instanceof Client) {
        endpoint = WebClient.getConfig(proxy).getConduitSelector().getEndpoint();
      }
      
      if(endpoint == null) {
        throw new IllegalArgumentException(proxy + " is not an Apache CXF REST client!");
      }

      // Add an interceptor on outgoing requests for the endpoint.
      endpoint.getOutInterceptors().add(new MyInterceptor());

      super.apply(proxy);
    }

    // ----------------------------------------------------------------------
    // Internal classes.
    // ----------------------------------------------------------------------

    class MyInterceptor extends AbstractPhaseInterceptor<Message>
    {
      public MyInterceptor()
      {
        super(Phase.USER_LOGICAL);
      }

      public final void handleMessage(Message message)
      {
    	// Get the target address of this message.
        String address = (String)message.get(Message.ENDPOINT_ADDRESS);
        if(address != null) {
          StringBuilder sb = new StringBuilder(address);
          if(address.indexOf('?') == -1) {
            // add '?' if the address does not contain it already.
            sb.append('?');
          } else {
            // else add '&'
        	sb.append('&');
          }
          // Add both implicit query parameter name and value.
          sb.append(implicitQueryParamName);
          sb.append('=');
          sb.append(implicitQueryParamValue);
          // Change the address for this message.
          message.put(Message.ENDPOINT_ADDRESS, sb.toString());
        }
      }
    }
}
