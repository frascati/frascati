/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.jaxws;

import java.util.List;
import javax.xml.ws.Binding;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.MessageContext;

/**
 * Abstract binding intent class for configuring a JAX-WS Handler.
 *
 * @author Philippe Merle - Inria
 */
public abstract class AbstractHandlerIntent
              extends AbstractWSBindingIntent
{
    // ----------------------------------------------------------------------
    // Related to class AbstractWSBindingIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF WS binding instance.
     *
     * @param binding the Apache CXF WS binding to configure.
     */
    protected final void configure(Binding binding)
    {
      // Add this handler to the proxy instance.
      List<Handler> list = binding.getHandlerChain();
      list.add(this.getHandler());
      binding.setHandlerChain(list);
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Get the JAX-WS Handler to add to the proxy instance.
     *
     * @return the JAX-WS Handler to add to the proxy instance.
     */
    protected abstract Handler<MessageContext> getHandler();
}
