/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.logging;

import org.apache.cxf.interceptor.InterceptorProvider;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.intent.cxf.AbstractInterceptorProviderIntent;

/**
 * Binding Intent class for configuring Apache CXF Logging interceptors.
 *
 * @author Philippe Merle - Inria
 */
public class LoggingInterceptorIntent
     extends AbstractInterceptorProviderIntent
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    @Property(name="PrettyLogging")
    public final void setPrettyLogging(boolean flag)
    {
      this.logInInterceptor.setPrettyLogging(flag);
      this.logOutInterceptor.setPrettyLogging(flag);
    }

    // ----------------------------------------------------------------------
    // Internal state.
    // ----------------------------------------------------------------------

    /**
     * Apache CXF logging interceptor for incoming messages.
    */
    private LoggingInInterceptor logInInterceptor = new LoggingInInterceptor();

    /**
     * Apache CXF logging interceptor for outcoming messages.
     */
    private LoggingOutInterceptor logOutInterceptor = new LoggingOutInterceptor();
    
    // ----------------------------------------------------------------------
    // Related to class AbstractInterceptorProviderIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure Apache CXF interceptors.
     *
     * @param interceptorProvider the Apache CXF interceptor provider to configure.
     *
     * @see AbstractInterceptorProviderIntent#configure(InterceptorProvider)
     */
    protected final void configure(InterceptorProvider interceptorProvider)
    {
      interceptorProvider.getInInterceptors().add(this.logInInterceptor);
      interceptorProvider.getOutInterceptors().add(this.logOutInterceptor);
      interceptorProvider.getOutFaultInterceptors().add(this.logOutInterceptor);
    }
}
