/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.service.Service;

/**
 * Abstract binding intent class for configuring Apache CXF service.
 *
 * @author Philippe Merle - Inria
 * @since 1.5
 */
public abstract class AbstractServiceIntent
              extends AbstractEndpointIntent
{
    // ----------------------------------------------------------------------
    // Related to class AbstractEndpointIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF endpoint instance.
     *
     * @param endpoint the Apache CXF endpoint to configure.
     *
     * @see AbstractEndpointIntent#configure(T)
     */
    @Override
    public final void configure(Endpoint endpoint)
    {
      configure(endpoint.getService());
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF service instance.
     *
     * @param service the Apache CXF service to configure.
     */
    protected abstract void configure(Service service);
}
