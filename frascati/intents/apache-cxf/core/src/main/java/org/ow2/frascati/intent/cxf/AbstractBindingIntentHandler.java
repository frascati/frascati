/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf;

import org.objectweb.fractal.bf.connectors.common.BindingIntentHandler;

import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;

/**
 * Abstract class for implementing FraSCAti binding intent components.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
@org.oasisopen.sca.annotation.Service(BindingIntentHandler.class)
public abstract class AbstractBindingIntentHandler
              extends AbstractScopeCompositeLoggeable
           implements BindingIntentHandler
{
    // ----------------------------------------------------------------------
    // Related to interface BindingIntentHandler.
    // ----------------------------------------------------------------------

    /**
     * Apply this binding intent on a proxy instance.
     *
     * @param proxy the proxy instance to configure.
     *
     * @see BindingIntentHandler#apply(Object)
     */
    public void apply(Object proxy)
    {
      info("applied on " + proxy.toString());
    }
}
