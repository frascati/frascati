/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.jaxws;

import java.util.Map;
import javax.xml.ws.BindingProvider;

/**
 * Abstract intent class for configuring Web Service request contexts.
 *
 * @author Philippe Merle - Inria
 */
public abstract class AbstractRequestContextIntent
              extends AbstractBindingProviderIntent
{
    // ----------------------------------------------------------------------
    // Related to class AbstractBindingProviderIntent.
    // ----------------------------------------------------------------------

    /**
     * @see AbstractBindingProviderIntent#configure(BindingProvider)
     */
    @Override
    protected final void configure(BindingProvider bindingProvider)
    {
      Map<String, Object> requestContext = bindingProvider.getRequestContext();
      configure(requestContext);
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Configure a request context.
     *
     * @param requestContext the request context to configure.
     */
    protected abstract void configure(Map<String, Object> requestContext);
}
