/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.gzip;

import org.apache.cxf.interceptor.InterceptorProvider;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.intent.cxf.AbstractInterceptorProviderIntent;

/**
 * FraSCAti binding intent for configuring Apache CXF GZIP interceptors.
 *
 * @author Philippe Merle - Inria
 */
public class GZIPIntent
     extends AbstractInterceptorProviderIntent
{
    // ----------------------------------------------------------------------
    // Internal state.
    // ----------------------------------------------------------------------

    /**
     * Apache CXF GZIP IN interceptor.
     */
    private GZIPInInterceptor in = new GZIPInInterceptor();

    /**
     * Apache CXF GZIP OUT interceptor.
     */
    private GZIPOutInterceptor out = new GZIPOutInterceptor();

    // ----------------------------------------------------------------------
    // Related to class AbstractInterceptorProviderIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure Apache CXF interceptors.
     *
     * @param interceptorProvider the Apache CXF interceptor provider to configure.
     *
     * @see AbstractInterceptorProviderIntent#configure(InterceptorProvider)
     */
    protected final void configure(InterceptorProvider interceptorProvider)
    {
      interceptorProvider.getInInterceptors().add(in);
      interceptorProvider.getInFaultInterceptors().add(in);
      interceptorProvider.getOutInterceptors().add(out);
      interceptorProvider.getOutFaultInterceptors().add(out);
    }

    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    @Property(name="Threshold")
    public final void setThreshold(int threshold)
    {
      this.out.setThreshold(threshold);
    }
}
