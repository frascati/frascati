/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf;

import org.apache.cxf.interceptor.InterceptorProvider;
import org.apache.cxf.endpoint.Server;

/**
 * Abstract Binding Intent class for configuring Apache CXF interceptors.
 *
 * @author Philippe Merle - Inria
 */
public abstract class AbstractInterceptorProviderIntent
              extends AbstractBindingIntentHandler
{
    // ----------------------------------------------------------------------
    // Related to interface BindingIntentHandler.
    // ----------------------------------------------------------------------

    /**
     * Apply this binding intent on a proxy instance.
     *
     * @param proxy the proxy instance to configure.
     *
     * @see BindingIntentHandler#apply(Object)
     */
    @Override
    public final void apply(Object proxy)
    {
      // Obtain the interceptor provider associated to the proxy instance.
      InterceptorProvider interceptorProvider = null;
      // Is the proxy a Apache CXF REST client?
      if(proxy instanceof org.apache.cxf.jaxrs.client.Client) {
        interceptorProvider = org.apache.cxf.jaxrs.client.WebClient.getConfig(proxy);
      // Is the proxy a Apache CXF Web Service/REST server?
      } else if(proxy instanceof Server) {
        interceptorProvider = ((Server)proxy).getEndpoint();
      } else {
      // Is the proxy a Apache CXF Web Service client?
//  if(proxy instanceof org.apache.cxf.jaxws.JaxWsClientProxy) {
        interceptorProvider = org.apache.cxf.frontend.ClientProxy.getClient(proxy);
      }
      if(interceptorProvider == null) {
        throw new IllegalArgumentException(proxy + " is not an Apache CXF Web Service/REST client/server!");
      }

      // Configure the interceptor provider.
      configure(interceptorProvider);

      super.apply(proxy);
    }

    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF interceptor provider instance.
     *
     * @param interceptorProvider the Apache CXF interceptor provider to configure.
     */
    protected abstract void configure(InterceptorProvider interceptorProvider);
}
