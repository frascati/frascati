/**
 * OW2 FraSCAti SCA Intents for Apache CXF WS-Discovery
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.ws.discovery;

import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;

import org.apache.cxf.ws.discovery.WSDiscoveryClient;
import org.apache.cxf.ws.discovery.wsdl.ProbeType;
import org.apache.cxf.ws.discovery.wsdl.ProbeMatchType;
import org.apache.cxf.ws.discovery.wsdl.ProbeMatchesType;

/**
 * Helper class to use the Apache CXF WS Discovery service.
 *
 * @author Philippe Merle - Inria
 * @since 1.5
 */
public class WsDiscoveryHelper
{
	/**
	 * Probe all service adresses matching to a given type.
	 *
	 * @param type the QName type to probe.
	 * @return all the service addresses matching the type.
	 */
    static List<String> probe(QName type)
    {
      // Create an Apache CXF WS-Discovery client.
      WSDiscoveryClient wsdclient = new WSDiscoveryClient();
      // or: new WSDiscoveryClient("soap.udp://proxyhost:3702");

      // Create the probe query.
      ProbeType p = new ProbeType();
      p.getTypes().add(type);

      // Execute the probe query.
      ProbeMatchesType pmt = wsdclient.probe(p);

      // Extract all the service addresses matching the probe query.
      List<String> result = new ArrayList<String>();
      for (ProbeMatchType pm : pmt.getProbeMatch()) {
        for (String add : pm.getXAddrs()) {
          result.add(add);
        }
      }

      // Close the Apache CXF WS-Discovery client.
      try {
        wsdclient.close();
      } catch(java.io.IOException ioe) {
        ioe.printStackTrace();
      }

      return result;
    }
}
