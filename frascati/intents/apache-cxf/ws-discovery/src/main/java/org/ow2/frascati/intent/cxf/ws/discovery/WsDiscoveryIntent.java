/**
 * OW2 FraSCAti SCA Intents for Apache CXF WS-Discovery
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.ws.discovery;

import java.util.List;
import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.message.Message;

import org.ow2.frascati.intent.cxf.AbstractBindingIntentHandler;
import org.ow2.frascati.intent.cxf.endpoint.EndpointHelper;

/**
 * Intent for Apache CXF WS-Discovery.
 *
 * @author Philippe Merle - Inria
 * @since 1.5
 */
public class WsDiscoveryIntent
     extends AbstractBindingIntentHandler
{
    // ----------------------------------------------------------------------
    // Related to interface BindingIntentHandler.
    // ----------------------------------------------------------------------

    /**
     * Apply this binding intent on a proxy instance.
     *
     * @param proxy the proxy instance to configure.
     *
     * @see BindingIntentHandler#apply(Object)
     */
    @Override
    public final void apply(Object proxy)
    {
      // Obtain the client associated to the proxy instance.
      Client client = null;

      // Is the proxy a Apache CXF Web Service client?
      client = ClientProxy.getClient(proxy);
      
      if(client == null) {
        throw new IllegalArgumentException(proxy + " is not an Apache CXF Web Service client!");
      }

      // Get the type of the endpoint associated to the client.
      QName type = EndpointHelper.getInterfaceQName(client.getEndpoint());

      // Probe this type with WS-Discovery.
      List<String> serviceAddresses = WsDiscoveryHelper.probe(type);

      // Just keep the first service address found.
      String firstAddress = serviceAddresses.get(0);

      // Setup the client endpoint address to the first service address found. 
      client.getRequestContext().put(Message.ENDPOINT_ADDRESS, firstAddress);        	

      super.apply(proxy);
    }
}
