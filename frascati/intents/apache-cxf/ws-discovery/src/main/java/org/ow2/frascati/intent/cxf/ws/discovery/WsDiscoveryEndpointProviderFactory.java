/**
 * OW2 FraSCAti SCA Intents for Apache CXF WS-Discovery
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.ws.discovery;

import java.util.List;
import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Endpoint;

import org.ow2.frascati.intent.cxf.clustering.endpoint.EndpointProvider;
import org.ow2.frascati.intent.cxf.clustering.endpoint.AbstractEndpointProviderFactory;
import org.ow2.frascati.intent.cxf.endpoint.EndpointHelper;

/**
 * Factory of WS-Discovery clustering endpoint providers.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class WsDiscoveryEndpointProviderFactory
     extends AbstractEndpointProviderFactory
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // For interface EndpointProviderFactory.
    // ----------------------------------------------------------------------

    /**
     * @see EndpointProviderFactory#newEndpointProvider(Endpoint)
     */
    public final EndpointProvider newEndpointProvider(Endpoint endpoint)
    {
      // the type of the endpoint used to query endpoint addresses with ws-discovery.
      final QName type = EndpointHelper.getInterfaceQName(endpoint);

      return new EndpointProvider() {
        /**
         * @see EndpointProvider#getEndpointAddresses()
         */
        public final List<String> getEndpointAddresses()
        {
          info("WS-Discovery probing for " + type);
          return WsDiscoveryHelper.probe(type);
        }
      };
    }
}
