/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

------------
Introduction
------------

This module provides a set of intents for both Web Service and REST bindings.
These intents allow you to configure technical properties of both Web Service
and REST bindings at a fine grain level like
- logging messages,
- setup connection and receive timeouts,
- use a proxy server,
- setup authorization policy,
- setup TLS/SSL,
- compress messages with GZIP,
- use FastInfoset,
- use XML streaming,
- setup data binding to use (Aegis, JAXB, JiBX, XmlBeans),
- setup endpoint properties (ExceptionMessageCauseEnabled, FaultStackTraceEnabled,
  MtomEnabled, SchemaValidationEnabled, PrivateEndpoint),
- use WS-Addressing,
- use WS-RM,
- load balancing requests between several services.

Let's note that these intents are specific to Apache CXF and can't be used
with other forms of FraSCAti bindings.

----------------
Maven dependency
----------------

To use these Web Service and REST binding intents, add the following Maven
dependency to your pom.xml files:

    <dependency>
      <groupId>org.ow2.frascati.intent</groupId>
      <artifactId>frascati-intent-apache-cxf</artifactId>
      <version>${frascati.version}</version>
    </dependency>

----------------
Logging messages
----------------

The logging intent allows you to log all requests and responses transported
via both Web Service and REST bindings. 

Following samples illustrate how to use the Logging intent:

  <sca:service ...>
    <sca:binding.ws ... requires="Logging"/>
  </sca:service>

  <sca:service ...>
    <frascati:binding.rest ... requires="Logging"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="Logging"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="Logging"/>
  </sca:reference>

The logging intent has some configurable properties:
- PrettyLogging is a boolean property for configuring how XML messages are displayed.
  If this property is equals to true then XML messages are pretty printed.
  By default, this property is equals to false.

  Add the following component to your composite to pretty print XML messages:

  <sca:component name="Logging">
    <sca:implementation.composite name="LoggingIntent"/>
    <sca:property name="PrettyLogging">true</property>
  </sca:component>

Some reusable configurations of the logging intent are already pre-defined:
- Logging       : no pretty print of XML messages.
- PrettyLogging : pretty print of XML messages.

To use them, your bindings must require one of the previous intent names, e.g.:

    <sca:binding.ws ... requires="PrettyLogging"/>

    <frascati:binding.rest ... requires="Logging"/>

------------------
Connection timeout
------------------

The ConnectionTimeout intent allows you to setup the connection timeout of
both Web Service and REST client-side bindings, i.e., bindings associated
to SCA references. The connection timeout is the maximal timeout expected
to establish the connection to a remote service. When the connection
timeout is exceeded then a connection timeout exception is thrown.

The ConnectionTimeout intent has some configurable properties:
- ConnectionTimeout is an integer property to setup the duration in milliseconds
  before a connection timeout exception is thrown.

  Add the following component to your composite to define a connection timeout of 15 seconds:

  <sca:component name="aConnectionTimeout">
    <sca:implementation.composite name="ConnectionTimeoutIntent"/>
    <sca:property name="ConnectionTimeout">15000</sca:property>
  </sca:component>

Following samples illustrate how to use this ConnectionTimeout intent:

  <sca:reference ...>
    <sca:binding.ws ... requires="aConnectionTimeout"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="aConnectionTimeout"/>
  </sca:reference>

Some reusable configurations of the ConnectionTimeout intent are already pre-defined:
- ConnectionTimeout-1s  : connection timeout of 1 second.
- ConnectionTimeout-2s  : connection timeout of 2 seconds.
- ConnectionTimeout-3s  : connection timeout of 3 seconds.
- ConnectionTimeout-4s  : connection timeout of 4 seconds.
- ConnectionTimeout-5s  : connection timeout of 5 seconds.
- ConnectionTimeout-6s  : connection timeout of 6 seconds.
- ConnectionTimeout-7s  : connection timeout of 7 seconds.
- ConnectionTimeout-8s  : connection timeout of 8 seconds.
- ConnectionTimeout-9s  : connection timeout of 9 seconds.
- ConnectionTimeout-10s : connection timeout of 10 seconds.
- ConnectionTimeout-20s : connection timeout of 20 seconds.
- ConnectionTimeout-30s : connection timeout of 30 seconds.

To use them, your bindings must require one of the previous intent names, e.g.:

    <sca:binding.ws ... requires="ConnectionTimeout-10s"/>

    <frascati:binding.rest ... requires="ConnectionTimeout-5s"/>

To define a new reusable configuration of the ConnectionTimeout intent,
you must define a new composite. Following sample defines the reusable
ConnectionTimeout-1minute intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="ConnectionTimeout-1minute">
    <include name="ConnectionTimeoutIntent"/>
    <property name="ConnectionTimeout">60000</property>
  </composite>

---------------
Receive timeout
---------------

The ReceiveTimeout intent allows you to setup the receive timeout of
both Web Service and REST client-side bindings, i.e., bindings associated
to SCA references. The receive timeout is the maximal timeout expected
to receive responses from a remote service. When the receive timeout 
is exceeded then a receive timeout exception is thrown.

The ReceiveTimeout intent has some configurable properties:
- ReceiveTimeout is an integer property to setup the duration in milliseconds
  before a receive timeout exception is thrown.

  Add the following component to your composite to define a receive timeout of 15 seconds:

  <sca:component name="aReceiveTimeout">
    <sca:implementation.composite name="ReceiveTimeoutIntent"/>
    <sca:property name="ReceiveTimeout">15000</sca:property>
  </sca:component>

Following samples illustrate how to use this ReceiveTimeout intent:

  <sca:reference ...>
    <sca:binding.ws ... requires="aReceiveTimeout"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="aReceiveTimeout"/>
  </sca:reference>

Some reusable configurations of the ReceiveTimeout intent are already pre-defined:
- ReceiveTimeout-1s  : receive timeout of 1 second.
- ReceiveTimeout-2s  : receive timeout of 2 seconds.
- ReceiveTimeout-3s  : receive timeout of 3 seconds.
- ReceiveTimeout-4s  : receive timeout of 4 seconds.
- ReceiveTimeout-5s  : receive timeout of 5 seconds.
- ReceiveTimeout-6s  : receive timeout of 6 seconds.
- ReceiveTimeout-7s  : receive timeout of 7 seconds.
- ReceiveTimeout-8s  : receive timeout of 8 seconds.
- ReceiveTimeout-9s  : receive timeout of 9 seconds.
- ReceiveTimeout-10s : receive timeout of 10 seconds.
- ReceiveTimeout-20s : receive timeout of 20 seconds.
- ReceiveTimeout-30s : receive timeout of 30 seconds.

To use them, your bindings must require one of the previous intent names, e.g.:

    <sca:binding.ws ... requires="ReceiveTimeout-5s"/>

    <frascati:binding.rest ... requires="ReceiveTimeout-10s"/>

To define a new reusable configuration of the ReceiveTimeout intent,
you must define a new composite. Following sample defines the reusable
ReceiveTimeout-1minute intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="ReceiveTimeout-1minute">
    <include name="ReceiveTimeoutIntent"/>
    <property name="ReceiveTimeout">60000</property>
  </composite>

--------------------
Using a proxy server
--------------------

The ProxyServer intent allows you to setup the proxy server to use with
both Web Service and REST client-side bindings, i.e., bindings associated
to SCA references. This is useful when your application runs inside a
private secure network and needs to call remote services beyond the firewall.

The ProxyServer intent has some configurable properties:
- ProxyServer is a string property to setup the IP address of the proxy server.
- ProxyServerPort is an integer property to setup the IP port of the proxy server.

  Add the following component to your composite to define a proxy server:

  <sca:component name="aProxyServer">
    <sca:implementation.composite name="ProxyServerIntent"/>
    <sca:property name="ProxyServer">www.proxy.net</sca:property>
    <sca:property name="ProxyServerPort">80</sca:property>
  </sca:component>

Following samples illustrate how to use this ProxyServer intent:

  <sca:reference ...>
    <sca:binding.ws ... requires="aProxyServer"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="aProxyServer"/>
  </sca:reference>

To define a reusable configuration of the ProxyServer intent,
you must define a new composite. Following sample defines the reusable
MyCompanyProxyServer intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="MyCompanyProxyServer">
    <include name="ProxyServerIntent"/>
    <property name="ProxyServer">proxy.mycompany.com</property>
    <property name="ProxyServerPort">80</property>
  </composite>

--------------------
Authorization policy
--------------------

The AuthorizationPolicy intent allows you to setup the authorization policy to use
with both Web Service and REST client-side bindings, i.e., bindings associated
to SCA references. This is useful when your application calls remote services
requiring authentication.

The AuthorizationPolicy intent has some configurable properties:
- AuthorizationType is a string property to setup the authorization type
  required by the remote service. Expected types are Basic, Digest,
  Negotiate.
- Authorization is a string property to setup the authorization
  required by the remote service.
- UserName is a string property to setup the user name.
- Password is a string property to setup the password.

  Add the following component to your composite to define an authorization policy:

  <sca:component name="anAuthorizationPolicy">
    <sca:implementation.composite name="AuthorizationPolicyIntent"/>
    <sca:property name="AuthorizationType">Basic</sca:property>
    <sca:property name="Authorization"></sca:property>
    <sca:property name="UserName">yourUserName</sca:property>
    <sca:property name="Password">yourPassword</sca:property>
  </sca:component>

Following samples illustrate how to use the AuthorizationPolicy intent:

  <sca:reference ...>
    <sca:binding.ws ... requires="anAuthorizationPolicy"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="anAuthorizationPolicy"/>
  </sca:reference>

To define a reusable configuration of the AuthorizationPolicy intent,
you must define a new composite. Following sample defines the reusable
MyAuthorizationPolicy intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="MyAuthorizationPolicy">
    <include name="AuthorizationPolicyIntent"/>
    <property name="AuthorizationType">Basic</property>
    <property name="Authorization"></property>
    <property name="UserName">yourUserName</property>
    <property name="Password">yourPassword</property>
  </composite>

As the Basic authorization type is often used, MyAuthorizationPolicy can be
simply written as follow:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="MyAuthorizationPolicy">
    <include name="BasicAuthorizationPolicyIntent"/>
    <property name="UserName">yourUserName</property>
    <property name="Password">yourPassword</property>
  </composite>

Here BasicAuthorizationPolicyIntent setups both AuthorizationType and Authorization
properties automatically.

------------------
Configuring TLS/SSL
------------------

The TLS/SSL intent allows you to setup TLS/SSL client parameters to use
with both Web Service and REST client-side bindings, i.e., bindings associated
to SCA references. This is useful when your application calls secure remote services.

The TLS/SSL intent has some configurable properties:
- TrustManagerKeyStorePassword is the password to read the trust manager key store.
- TrustManagerKeyStoreFile is the location of the trust manager key store.
- KeyManagerKeyStorePassword is the password to read the key manager key store.
- KeyManagerKeyStoreFile is the location of the key manager key store.

  Add the following component to your composite to define TLS/SSL parameters:

  <sca:component name="anSSLConfiguration">
    <sca:implementation.composite name="TLSClientParametersIntent"/>
    <sca:property name="TrustManagerKeyStorePassword">password</sca:property>
    <sca:property name="TrustManagerKeyStoreFile">truststore.jks</sca:property>
    <sca:property name="KeyManagerKeyStorePassword">password</sca:property>
    <sca:property name="KeyManagerKeyStoreFile">keystore.jks</sca:property>
  </sca:component>

Following samples illustrate how to use this TLS/SSL intent:

  <sca:reference ...>
    <sca:binding.ws ... requires="anSSLConfiguration"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="anSSLConfiguration"/>
  </sca:reference>

To define a reusable configuration of the TLS/SSL intent,
you must define a new composite. Following sample defines the reusable
MySSLConfiguration intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="MySSLConfiguration">
    <include name="TLSClientParametersIntent"/>
    <property name="TrustManagerKeyStorePassword">password</property>
    <property name="TrustManagerKeyStoreFile">truststore.jks</property>
    <property name="KeyManagerKeyStorePassword">password</property>
    <property name="KeyManagerKeyStoreFile">keystore.jks</property>
  </composite>


TBC: How to setup the server-side...

An example could be found at /trunk/example/helloworld-rest-https/.

------------------------------
Compressing messages with GZIP
------------------------------

The compression intent allows you to compress messages transported via both
Web Service and REST bindings using gzip.

Following samples illustrate how to use the compression intent:

  <sca:service ...>
    <sca:binding.ws ... requires="GZIP"/>
  </sca:service>

  <sca:service ...>
    <frascati:binding.rest ... requires="GZIP"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="GZIP"/>
  </sca:reference>

  <sca:reference ...>
    <frascati:binding.rest ... requires="GZIP"/>
  </sca:reference>

The compression intent has some configurable properties:
- Threshold is an integer property for defining the compression threshold.
  Messages smaller than this threshold will not be compressed.
  To force compression of all messages, set the threshold to 0.
  By default, threshold is equals to 1kB.

  Add the following component to your composite to compress messages bigger than 10kB:

  <sca:component name="GZIP">
    <sca:implementation.composite name="GZIPIntent"/>
    <sca:property name="Threshold">10240</sca:property>
  </sca:component>

To define a reusable configuration of the compression intent,
you must define a new composite. Following sample defines the reusable
GZIP-2kB intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="GZIP-2kB">
    <implementation.composite name="GZIPIntent"/>
    <property name="Threshold">2048</property>
  </composite>

An example could be found at /trunk/example/helloworld-ws-compression/.

-----------------
Using FastInfoset
-----------------

The FastInfoset intent allows you to compress Web Services messages using FastInfoset.

Following samples illustrate how to use the FastInfoset intent:

  <sca:service ...>
    <sca:binding.ws ... requires="FastInfoset"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="FastInfoset"/>
  </sca:reference>

The FastInfoset intent has some configurable properties:
- Force is a boolean property to force to use FastInfoset without negotiation.
  To force using FastInfoset without negotiating, set force to true.
  By default, force is equals to false.

  Add the following component to your composite to force using FastInfoset without negotiation:

  <sca:component name="FastInfoset">
    <sca:implementation.composite name="FastInfosetIntent"/>
    <sca:property name="Force">true</sca:property>
  </sca:component>

To define a reusable configuration of the FastInfoset intent,
you must define a new composite. Following sample defines the 
reusable AlwaysFastInfoset intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="AlwaysFastInfoset">
    <implementation.composite name="FastInfosetIntent"/>
    <property name="Force">true</property>
  </composite>

An example could be found at /trunk/example/helloworld-ws-compression/.

-------------------
Using XML streaming
-------------------

The STAX intent performs data binding using XML streaming.

Following samples illustrate how to use the STAX intent:

  <sca:service ...>
    <sca:binding.ws ... requires="STAX"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="STAX"/>
  </sca:reference>

The STAX intent has no configurable properties.

-------------------------
Setup data binding to use
-------------------------

Apache CXF supports various data bindings implementing the mapping between Java and XML. 
In FraSCAti, these data bindings are reified by intents, namely Aegis, JAXB, JiBX, and XmlBeans intents.

Following samples illustrate how to use these data binding intents:

  <sca:service ...>
    <sca:binding.ws ... requires="Aegis"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="Aegis"/>
  </sca:reference>

  <sca:service ...>
    <sca:binding.ws ... requires="JiBX"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="JiBX"/>
  </sca:reference>

Currently, these data binding intents have no configurable properties.

-------------------------
Setup endpoint properties
-------------------------

To be written.

- ExceptionMessageCauseEnabled

  Enable exception message cause.
  Useful for debugging.

- FaultStackTraceEnabled

  Enable fault stack trace.
  Useful for debugging.

- MtomEnabled

  Enable MOTM.

- SchemaValidationEnabled

  Enable schema validation.

- PrivateEndpoint

  Make an endpoint private.

-------------------
Using WS-Addressing
-------------------

The WS-Addressing intent is dedicated to Web Service bindings
requiring to use the WS-Addressing specification.

Following samples illustrate how to use the WS-Addressing intent:

  <sca:service ...>
    <sca:binding.ws ... requires="WSAddressing"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="WSAddressing"/>
  </sca:reference>

The WS-Addressing intent has some configurable properties:
- AddressingRequired is a boolean property to indicate if
  WS-Addresing is required or not. By default, it is equals to true.
- AllowDuplicates is a boolean property to determine if 
  duplicate MessageIDs are tolerated or not. By default, it is equals to true.
- UsingAddressingAdvisory is a boolean property to indicate if
  the presence of the <UsingAddressing> element in the wsdl is purely advisory, i.e.
  its absence doesn't prevent the encoding of WS-A headers. This is especially useful 
  to enable WS-Addressing in the java-first case, where you have no wsdl.
  By default, it is equals to true.

  Add the following component to your composite to avoid duplicate MessageIDs:

  <sca:component name="WSAddressing">
    <sca:implementation.composite name="WSAddressing"/>
    <sca:property name="AllowDuplicates">false</sca:property>
  </sca:component>

To define a reusable configuration of the WS-Addressing intent,
you must define a new composite. Following sample defines the reusable
MyWS-Addressing intent:

  <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="MyWS-Addressing">
    <include name="WSAddressingIntent"/>
    <property name="AddressingRequired">true</property>
    <property name="AllowDuplicates">false</property>
    <property name="UsingAddressingAdvisory">true</property>
  </composite>

An example could be found at /trunk/example/helloworld-wsa/.

-----------
Using WS-RM
-----------

The WS-RM intent is dedicated to Web Service bindings requiring
to use the Web Services Reliable Messaging (WS-RM) specification.

Following samples illustrate how to use the WS-RM intent:

  <sca:service ...>
    <sca:binding.ws ... requires="WS-RM"/>
  </sca:service>

  <sca:reference ...>
    <sca:binding.ws ... requires="WS-RM"/>
  </sca:reference>

The WS-RM intent has no configurable properties.

An example could be found at /trunk/example/helloworld-ws-rm/.

-----------------------
Load balancing requests
-----------------------

The load balancing intent allows to load balance client requests
to several Web services.

The load balancing intent has some configurable properties:
- AlternateAddresses is a string property given the list of the
  targeted service URIs. URIs are separated by a space.
- DelayBetweenRetries is an integer property that setups
  the delay between retries.

  Add the following component to your composite:

  <sca:component name="LoadBalancingBetweenSeveralServices">
    <sca:implementation.composite name="LoadBalancing" />
    <sca:property name="AlternateAddresses">uri-service1 ... uri-serviceN</sca:property>
  </sca:component>

Following samples illustrate how to use this load balancing intent:

  <sca:reference ...>
    <sca:binding.ws uri="" ... requires="LoadBalancingBetweenSeveralServices"/>
  </sca:reference>

Let's note that the previous uri attribute is empty. The binding will load 
balance requests between alternate addresses defined by the intent.

An example could be found at /trunk/example/helloworld-wslb/.

------------------
Concluding remarks
------------------

As shown this module provides a set of useful intents for both Web Service and REST bindings.
These intents encapsulate configurable mechanisms provided by Apache CXF.

Unfortunately not all Apache CXF configurable mechanisms are encapsulated into FraSCAti intents.
Moreover Apache CXF is extensible to support application specific configurable mechanisms.
However this is easy to add new FraSCAti intents to support other Apache CXF configurable mechanisms.

