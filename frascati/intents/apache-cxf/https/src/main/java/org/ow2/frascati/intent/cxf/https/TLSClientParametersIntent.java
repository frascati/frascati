/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.https;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.transport.http.HTTPConduit;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.intent.cxf.AbstractConduitIntent;

/**
 * Concrete Binding Intent class for configuring Apache CXF TLSClientParameters.
 *
 * @author Philippe Merle - Inria
 */
public class TLSClientParametersIntent
     extends AbstractConduitIntent<HTTPConduit>
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    @Property(name="TrustManagerKeyStoreType")
    private String trustManagerKeyStoreType = "JKS";

    @Property(name="TrustManagerKeyStorePassword")
    private String trustManagerKeyStorePassword;

    @Property(name="TrustManagerKeyStoreFile")
    private String trustManagerKeyStoreFile;

    @Property(name="KeyManagerKeyStoreType")
    private String keyManagerKeyStoreType = "JKS";

    @Property(name="KeyManagerKeyStorePassword")
    private String keyManagerKeyStorePassword;

    @Property(name="KeyManagerKeyStoreFile")
    private String keyManagerKeyStoreFile;

    // ----------------------------------------------------------------------
    // Related to class AbstractConduitIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF HTTP conduit instance.
     *
     * @param conduit the Apache CXF HTTP conduit to configure.
     *
     * @see AbstractConduitIntent#configure(T)
     */
    public final void configure(HTTPConduit httpConduit)
    {
      // Create a new TLSClientParameters.
      TLSClientParameters tlsCP = new TLSClientParameters();

      try {
        // Init TrustManagers.
        KeyStore trustStore = KeyStore.getInstance(this.trustManagerKeyStoreType);
        trustStore.load(new FileInputStream(this.trustManagerKeyStoreFile), this.trustManagerKeyStorePassword.toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);
        tlsCP.setTrustManagers(tmf.getTrustManagers());

        // Init KeyManagers.
        KeyStore keyStore = KeyStore.getInstance(this.keyManagerKeyStoreType);
        keyStore.load(new FileInputStream(this.keyManagerKeyStoreFile), this.keyManagerKeyStorePassword.toCharArray());
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, this.keyManagerKeyStorePassword.toCharArray());
        tlsCP.setKeyManagers(kmf.getKeyManagers());

        // TODO: The following is not recommended and would not be done in a prodcution environment.
        tlsCP.setDisableCNCheck(true);
      
        // Setup TLSClientParameters of the httpConduit.
        httpConduit.setTlsClientParameters(tlsCP);

      } catch(KeyStoreException kse) {
        kse.printStackTrace();
      } catch(NoSuchAlgorithmException nsae) {
        nsae.printStackTrace();
      } catch(CertificateException ce) {
        ce.printStackTrace();
      } catch(UnrecoverableKeyException uke) {
        uke.printStackTrace();
      } catch(FileNotFoundException fnfe) {
        fnfe.printStackTrace();
      } catch(IOException ioe) {
        ioe.printStackTrace();
      }
    }
}
