/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.https;

import java.io.FileInputStream;
import java.util.logging.Level;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.cxf.BusFactory;
import org.apache.cxf.configuration.jsse.TLSServerParameters;
import org.apache.cxf.configuration.security.ClientAuthentication;
import org.apache.cxf.configuration.security.FiltersType;
import org.apache.cxf.transport.http_jetty.JettyHTTPServerEngineFactory;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;

/**
 * This component allows to configure Apache CXF TLS/SSL server parameters.
 *
 * @author Philippe Merle - Inria
 * @since 1.5
 */
@org.oasisopen.sca.annotation.EagerInit
public class TLSServerConfiguration
     extends AbstractScopeCompositeLoggeable
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    @Property(name="Port")
    private int port;

    @Property(name="TrustManagerKeyStoreType")
    private String trustManagerKeyStoreType = "JKS";

    @Property(name="TrustManagerKeyStorePassword")
    private String trustManagerKeyStorePassword;

    @Property(name="TrustManagerKeyStoreFile")
    private String trustManagerKeyStoreFile;

    @Property(name="KeyManagerKeyStoreType")
    private String keyManagerKeyStoreType = "JKS";

    @Property(name="KeyManagerKeyStorePassword")
    private String keyManagerKeyStorePassword;

    @Property(name="KeyManagerKeyStoreFile")
    private String keyManagerKeyStoreFile;

    @Property(name="CipherSuitesFilterIncludes")
    private String cipherSuitesFilterIncludes;

    @Property(name="ClientAuthenticationRequired")
    private boolean clientAuthenticationRequired;

    @Property(name="ClientAuthenticationWant")
    private boolean clientAuthenticationWant;

    /**
     * Configure an Apache CXF TLS/SSL server parameter.
     */
    @org.oasisopen.sca.annotation.Init
    public final void configure()
    {
      try {
        // Create a new TLSServerParameters.
        TLSServerParameters tlsParams = new TLSServerParameters();

        // Init TrustManagers.
        KeyStore trustStore = KeyStore.getInstance(this.trustManagerKeyStoreType);
        trustStore.load(new FileInputStream(this.trustManagerKeyStoreFile), this.trustManagerKeyStorePassword.toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);
        tlsParams.setTrustManagers(tmf.getTrustManagers());

        // Init KeyManagers.
        KeyStore keyStore = KeyStore.getInstance(this.keyManagerKeyStoreType);
        keyStore.load(new FileInputStream(this.keyManagerKeyStoreFile), this.keyManagerKeyStorePassword.toCharArray());
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore, this.keyManagerKeyStorePassword.toCharArray());
        tlsParams.setKeyManagers(kmf.getKeyManagers());

        // Configure CipherSuitesFilter.
        FiltersType filter = new FiltersType();
        for(String include : this.cipherSuitesFilterIncludes.split(" ")) {
          filter.getInclude().add(include);
        }
        tlsParams.setCipherSuitesFilter(filter);
        
        // Configure ClientAuthentication.
        ClientAuthentication ca = new ClientAuthentication();
        ca.setRequired(this.clientAuthenticationRequired);
        ca.setWant(this.clientAuthenticationWant);
        tlsParams.setClientAuthentication(ca);

        // Obtain the Apache CXF Jetty server factory.
        JettyHTTPServerEngineFactory jettyFactory =
            BusFactory.getDefaultBus().getExtension(JettyHTTPServerEngineFactory.class);

        // Configure the Apache CXF JettyHTTPServerEngineFactory.
        jettyFactory.setTLSServerParametersForPort(this.port, tlsParams);

      } catch (Exception exc) {
          log.log(Level.SEVERE, "Security configuration failed", exc);
      }
    }
}
