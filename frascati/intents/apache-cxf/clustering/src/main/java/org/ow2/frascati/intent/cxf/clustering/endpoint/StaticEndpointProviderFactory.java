/**
 * OW2 FraSCAti SCA Intents for Apache CXF Clustering
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.clustering.endpoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.cxf.endpoint.Endpoint;

import org.oasisopen.sca.annotation.Property;

/**
 * Factory of static clustering endpoint providers.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class StaticEndpointProviderFactory
     extends AbstractEndpointProviderFactory
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    /**
     * List of alternate addresses.
     */
    @Property(name="AlternateAddresses")
    private String alternateAddresses = "";

    // ----------------------------------------------------------------------
    // For interface EndpointProviderFactory.
    // ----------------------------------------------------------------------

    /**
     * @see EndpointProviderFactory#newEndpointProvider(Endpoint)
     */
    public final EndpointProvider newEndpointProvider(Endpoint endpoint)
    {
      return new EndpointProvider() {
        /**
         * @see EndpointProvider#getEndpointAddresses()
         */
        public final List<String> getEndpointAddresses()
        {
          info("getEndpointAddresses returns " + alternateAddresses);
          return new ArrayList(Arrays.asList(alternateAddresses.split(" ")));
        }
      };
    }
}
