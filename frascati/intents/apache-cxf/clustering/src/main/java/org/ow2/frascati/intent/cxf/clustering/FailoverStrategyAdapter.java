/**
 * OW2 FraSCAti SCA Intents for Apache CXF Clustering
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.clustering;

import java.util.List;

import org.apache.cxf.clustering.FailoverStrategy;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.message.Exchange;

import org.ow2.frascati.intent.cxf.clustering.endpoint.EndpointProvider;

/**
 * Apache CXF clustering strategy adapter.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class FailoverStrategyAdapter implements FailoverStrategy
{
    // ----------------------------------------------------------------------
    // Internal state.
    // ----------------------------------------------------------------------

    /**
     * The endpoint provider.
     */
    private EndpointProvider endpointProvider;

    /**
     * The delegate clustering strategy.
     */
    private FailoverStrategy strategy;

    // ----------------------------------------------------------------------
    // Public methods.
    // ----------------------------------------------------------------------

    /**
     * Get the delegate endpoint provider.
     *
     * @return the delegate endpoint provider.
     */
    public final EndpointProvider getEndpointProvider()
    {
      return this.endpointProvider;
    }

    /**
     * Set the delegate endpoint provider.
     *
     * @param strategy the delegate endpoint provider.
     */
    public final void setEndpointProvider(EndpointProvider endpointProvider)
    {
      this.endpointProvider = endpointProvider;
    }

    /**
     * Get the delegate clustering strategy.
     *
     * @return the delegate clustering strategy.
     */
    public final FailoverStrategy getStrategy()
    {
      return this.strategy;
    }

    /**
     * Set the delegate clustering strategy.
     *
     * @param strategy the delegate clustering strategy.
     */
    public final void setStrategy(FailoverStrategy strategy)
    {
      this.strategy = strategy;
    }

    // ----------------------------------------------------------------------
    // Related to interface FailoverStrategy.
    // ----------------------------------------------------------------------

    /**
     * @see FailoverStrategy#getAlternateAddresses(Exchange)
     */
    public final List<String> getAlternateAddresses(Exchange exchange)
    {
      // delegate to the endpoint provider.
      return this.endpointProvider.getEndpointAddresses();
    }

    /**
     * @see FailoverStrategy#getAlternateEndpoints(Exchange)
     */
    public final List<Endpoint> getAlternateEndpoints(Exchange exchange)
    {
  	  // delegate to the strategy.
      return this.strategy.getAlternateEndpoints(exchange);
    }

    /**
     * @see FailoverStrategy#selectAlternateAddress(List<String>)
     */
    public final String selectAlternateAddress(List<String> addresses)
    {
      // delegate to the strategy.
      return this.strategy.selectAlternateAddress(addresses);
    }

    /**
     * @see FailoverStrategy#selectAlternateEndpoint(List<Endpoint>)
     */
    public final Endpoint selectAlternateEndpoint(List<Endpoint> alternates)
    {
      // delegate to the strategy.
      return this.strategy.selectAlternateEndpoint(alternates);
    }
}
