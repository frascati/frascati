/**
 * OW2 FraSCAti SCA Intents for Apache CXF Clustering
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.clustering;

import org.apache.cxf.endpoint.Client;

import org.apache.cxf.clustering.FailoverFeature;
import org.apache.cxf.clustering.FailoverStrategy;

import org.oasisopen.sca.annotation.Reference;

import org.ow2.frascati.intent.cxf.AbstractClientIntent;
import org.ow2.frascati.intent.cxf.clustering.endpoint.EndpointProvider;
import org.ow2.frascati.intent.cxf.clustering.endpoint.EndpointProviderFactory;
import org.ow2.frascati.intent.cxf.clustering.strategy.ClusteringStrategyFactory;
import org.ow2.frascati.intent.cxf.feature.FeatureFactory;

/**
 * FraSCAti binding intent for configuring Apache CXF clustering features and strategies.
 *
 * @author Philippe Merle - Inria
 */
public class ClusteringIntent
     extends AbstractClientIntent
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    /**
     * Used Apache CXF clustering feature factory.
     */
    @Reference(name="clustering-feature-factory")
    private FeatureFactory<FailoverFeature> clusteringFeatureFactory;

    /**
     * Used Apache CXF clustering strategy factory.
     */
    @Reference(name="clustering-strategy-factory")
    private ClusteringStrategyFactory<FailoverStrategy> clusteringStrategyFactory;

    /**
     * Used endpoint provider factory.
     */
    @Reference(name="endpoint-provider-factory")
    private EndpointProviderFactory endpointProviderFactory;

    // ----------------------------------------------------------------------
    // Related to class AbstractClientIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF client instance.
     *
     * @param client the Apache CXF client to configure.
     */
    @Override
    protected final void configure(Client client)
    {
      // Create an Apache CXF clustering feature.
      FailoverFeature clusteringFeature = this.clusteringFeatureFactory.newFeature();

      // Create an Apache CXF clustering strategy.
      FailoverStrategy clusteringStrategy = this.clusteringStrategyFactory.newStrategy();

      // Create an endpoint provider.
      EndpointProvider endpointProvider = this.endpointProviderFactory.newEndpointProvider(client.getEndpoint());

      // Create the failover strategy adapter.
      FailoverStrategyAdapter failoverStrategy = new FailoverStrategyAdapter();
      failoverStrategy.setEndpointProvider(endpointProvider);
      failoverStrategy.setStrategy(clusteringStrategy);

      // Connect the clustering feature to the failover strategy.
      clusteringFeature.setStrategy(failoverStrategy);

      // Configure the client instance with the clustering feature.
      clusteringFeature.initialize(client, null);
    }
}
