/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.http;

import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;

import org.oasisopen.sca.annotation.Property;

/**
 * Binding intent class for configuring Apache CXF ConnectionTimeout.
 *
 * @author Philippe Merle - Inria
 */
public class ConnectionTimeoutIntent
     extends AbstractHTTPClientPolicyIntent
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    @Property(name="ConnectionTimeout")
    private int connectionTimeout;

    // ----------------------------------------------------------------------
    // Related to class AbstractHTTPClientPolicyIntent.
    // ----------------------------------------------------------------------

    /**
     * Configure an Apache CXF HTTP client policy.
     *
     * @param httpClientPolicy the Apache CXF HTTP client policy to configure.
     *
     * @see AbstractHTTPClientPolicyIntent#configure(HTTPClientPolicy)
     */
    protected final void configure(HTTPClientPolicy httpClientPolicy)
    {
      httpClientPolicy.setConnectionTimeout(this.connectionTimeout);
    }
}
