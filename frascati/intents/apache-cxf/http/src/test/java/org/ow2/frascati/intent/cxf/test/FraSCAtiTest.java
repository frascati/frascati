/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.test;

import org.junit.Test;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.examples.twitterweather.api.TwitterWeather;

/**
 * JUnit test case for OW2 FraSCAti SCA Intents for Apache CXF. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest
{
    @Test
    public void test() throws Exception {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component component = frascati.getComposite("twitter-weather-with-intents");
      TwitterWeather tw = frascati.getService(component, "tw", TwitterWeather.class);
      String response = tw.getWeatherForUser("vschiavoni");
      System.out.println(response);
    }

    @Test
    public void testAutoRedirectIntent() throws Exception {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component component = frascati.getComposite("twitter-weather-auto-redirect-intent");
      TwitterWeather tw = frascati.getService(component, "tw", TwitterWeather.class);
      String response = tw.getWeatherForUser("vschiavoni");
      System.out.println(response);
    }
}
