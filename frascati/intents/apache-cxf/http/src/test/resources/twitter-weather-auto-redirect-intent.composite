<?xml version="1.0" encoding="ISO-8859-15"?>
<!--  OW2 FraSCAti Examples: Twitter and Weather orchestration                      -->
<!--  Copyright (C) 2008-2013 Inria, University of Lille 1                          -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307          -->
<!--  USA                                                                           -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Philippe Merle                                                        -->
<!--                                                                                -->
<!--  Contributors:                                                                 -->
<!--                                                                                -->

<composite name="twitter-weather-auto-redirect-intent"
  xmlns="http://www.osoa.org/xmlns/sca/1.0"
  xmlns:wsdli="http://www.w3.org/2004/08/wsdl-instance"
  xmlns:frascati="http://frascati.ow2.org/xmlns/sca/1.1"
  xmlns:http="org/ow2/frascati/intent/cxf/http"
  xmlns:logging="org/ow2/frascati/intent/cxf/logging"
  targetNamespace="http://frascati.ow2.org/weather">

  <service name="tw" promote="orchestration/TwitterWeather"/>

  <component name="orchestration">
    <implementation.java class="org.ow2.frascati.examples.twitterweather.lib.Orchestration" />
    <reference name="twitter">
      <!-- Here the uri is incorrect as should be https://twitter.com but everything is ok as auto redirection is activated by the AutoRedirect intent. -->
      <frascati:binding.rest uri="http://twitter.com"
                             requires="http:AutoRedirect logging:Logging"
      />
    </reference>
    <reference name="decoder" target="decoder"/> 
    <reference name="weather">
      <binding.ws wsdli:wsdlLocation="http://www.webservicex.net/globalweather.asmx?wsdl"
                  wsdlElement="http://www.webservicex.net#wsdl.port(GlobalWeather/GlobalWeatherSoap)"/>
    </reference>
  </component>

  <component name="decoder">
   <implementation.java class="org.ow2.frascati.examples.twitterweather.lib.DecoderImpl" />
  </component>

</composite>
