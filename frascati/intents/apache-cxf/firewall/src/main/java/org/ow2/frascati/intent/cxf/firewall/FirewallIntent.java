/**
 * OW2 FraSCAti SCA Intents for Apache CXF
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.intent.cxf.firewall;

import java.util.HashSet;
import java.util.Set;
import javax.servlet.ServletRequest;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.intent.cxf.AbstractEndpointIntent;

/**
 * Intent to firewall requests.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class FirewallIntent
     extends AbstractEndpointIntent
{
    // ----------------------------------------------------------------------
    // Internal state.
    // ----------------------------------------------------------------------

    // TODO: Here it is required to find an efficient data structure to store and test authorizatez hosts.
    // Firstly it is needed to identify all authorization forms we would like to accept, e.g.:
    // - Precise IP host and port (e.g. 127.0.0.1:64000)
    // - A precise IP host and range of ports (e.g. 127.0.0.1:[64000..65000])
    // - A range of IP host and range of ports (e.g. 127.0.0.*:[1000..1500])
    // Secondly, we should find an efficient algorithm to test the authorization of a given remote host/port.

	/**
     * Data structure to store authorized hosts.
     */
    private Set<String> authorizedAddresses;

    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    /**
     * Configure authorized addresses.
     */
    @Property(name="AuthorizedAddresses")
    private void setAuthorizedAddresses(String addresses)
    {
      this.authorizedAddresses = new HashSet<String>();
      for(String address : addresses.split(" ")) {
        this.authorizedAddresses.add(address);
      }
    }

    // ----------------------------------------------------------------------
    // Related to class AbstractEndpointIntent.
    // ----------------------------------------------------------------------

    /**
     * @see AbstractEndpointIntent#configure(Endpoint)
     */
    protected final void configure(Endpoint endpoint)
    {
      // Add an interceptor on incoming requests for the endpoint.
      endpoint.getInInterceptors().add(new MyInterceptor());
    }

    // ----------------------------------------------------------------------
    // Internal methods and classes.
    // ----------------------------------------------------------------------

    protected final boolean checkAuthorization(String remoteHost, int remotePort)
    {
      // Here is a very simple authorization test.
      if(! authorizedAddresses.contains(remoteHost)) {
        // Reject this incoming request.
        log.severe("REJECT incoming request from " + remoteHost + ":" + remotePort);
        return false;
      }

      // Accept this incoming request.
      log.fine("ACCEPT incoming request from " + remoteHost + ":" + remotePort);
      return true;
    }

    class MyInterceptor extends AbstractPhaseInterceptor<Message>
    {
      public MyInterceptor()
      {
        super(Phase.RECEIVE);
      }

      public final void handleMessage(Message message) throws Fault
      {
        // Get remote IP host and port of this incoming message.
        ServletRequest httpRequest = (ServletRequest)message.get("HTTP.REQUEST");
        String remoteHost = httpRequest.getRemoteHost();
        int remotePort = httpRequest.getRemotePort();

        // Check if this remote host/port is authorized to invoke the endpoint.
        if(! checkAuthorization(remoteHost, remotePort)) {
          throw new Fault("Address " + remoteHost + ":" + remotePort + " not authorized!", log);
        }
      }
    }
}
