/***
 * OW2 FraSCAti Assembly Factory Tools
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Dolet
 * 
 * Contributor(s): Christophe Demarey
 */

package org.ow2.frascati.factory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.cxf.tools.common.ToolContext;
import org.apache.cxf.tools.wsdlto.WSDLToJava;

/**
 * Command line for WSDL2Java transformations.
 * 
 * @author Nicolas Dolet.
 */
public class WebServiceCommandLine {

  /** Definitions of option names */
  private static final String HELP_OPT = "help";
  private static final String WSDL2JAVA_OPT = "wsdl2java";
  private static final String WSDL_FILE_OPT = "wsdl-file";
  private static final String WSDL_URL_OPT = "wsdl-url";
  private static final String OUTPUTDIR_OPT = "output-directory";
  private static final String DEFAULT_OUTPUTDIR_OPT = "target/generated/src/main/java";

  /** Create Options object. */
  Options options;
  /** The name of the executable. */
  String commandName;
  /** The command line */
  CommandLine cmd;
  
  /**
   * Default constructor.
   */
  public WebServiceCommandLine(String commandName) {
    OptionGroup factoryGroup = new OptionGroup();
    options = new Options();
    this.commandName = commandName;
    
    // add options
    options.addOption("h", HELP_OPT, false, "print this message.");
    options.addOption("f", WSDL_FILE_OPT, true, "the WSDL file to parse.");
    options.addOption("u", WSDL_URL_OPT, true, "the WSDL URL to parse.");
    options.addOption("o", OUTPUTDIR_OPT, true,
        "the output directory (default: " + DEFAULT_OUTPUTDIR_OPT + ").");
    
    options.addOptionGroup(factoryGroup);
  }
  
  /**
   * Parse the wsdl2java command line.
   * 
   * @param args - Command arguments.
   */
  public void parse(String[] args) {
    CommandLineParser parser = new PosixParser();
    
    try {
      cmd = parser.parse(options, args);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    if ( (args.length == 0) || (cmd.hasOption(HELP_OPT)) ) {
      usage();
      System.exit(-1);
    }
  }
  
  /**
   * Print command usage
   */
  public void usage() {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("frascati " + commandName + " [OPTIONS]", options);
  }

  /**
   * Get the WSDL file name.
   * 
   * @return the WSDL file handler.
   */
  public File getWsdlFile() {
    String fileName = cmd.getOptionValue(WSDL_FILE_OPT);
    File wsdlFile = null;
    
    if (fileName != null) {
      wsdlFile = new File(fileName);
      if ( ! wsdlFile.exists() ) {
        System.err.println("Unable to find WSDL file: " + wsdlFile);
        System.err.println("Exiting...");
        System.exit(-1);
      }
      System.out.println("  WSDL file: " + wsdlFile.getAbsolutePath());
    }
    return wsdlFile; 
  }
  
  /**
   * Get the WSDL url.
   * 
   * @return the WSDL url.
   */
  public URL getWsdlUrl() {
    String url = cmd.getOptionValue(WSDL_URL_OPT);
    URL wsdlUrl =  null;
    
    if (url != null) {
      try {
        wsdlUrl = new URL(url);
      } catch (MalformedURLException e) {
        System.err.println("Malformed URL : " + url);
        System.err.println("Exiting...");
        System.exit(-1);
      }
      System.out.println("  WSDL URL: " + wsdlUrl.getPath());
    }
    return wsdlUrl;
  }
  
  /**
   * Get the output directory name.
   * 
   * @return the output directory name.
   */
  public String getOutputDir() {
    String value = cmd.getOptionValue(OUTPUTDIR_OPT);
    return (value == null) ? DEFAULT_OUTPUTDIR_OPT : value;
  }
  
  /**
   * Main method
   * 
   * @param args - Command line arguments
   */
  public static void main(String[] args) {
    
    System.out.println("FraSCAti: generating Java from WSDL...");
    // => Parse arguments   
    WebServiceCommandLine cmd = new WebServiceCommandLine(WSDL2JAVA_OPT);
    cmd.parse(args);
    
    String outputDir = cmd.getOutputDir();
    File wsdlFile = cmd.getWsdlFile();
    URL wsdlUrl = cmd.getWsdlUrl();
    
    if ( (wsdlFile == null) && (wsdlUrl == null) ) {
      System.err.println("Please set the WSDL file/URL to parse");
      cmd.usage();
      System.exit(-1);
    }
    if ( (wsdlFile != null) && (wsdlUrl != null) ) {
      System.err.println("Please choose either a WSDL file OR an URL to parse (not both!).");
      cmd.usage();
      System.exit(-1);
    }
        
    System.out.println("  output directory : " + outputDir);
    String wsdl = (wsdlFile == null ? wsdlUrl.toString() : wsdlFile.getAbsolutePath() );
    String[] params = new String[]{"-d", outputDir, wsdl};
    try {
      new WSDLToJava(params).run(new ToolContext());
      System.out.println("Java code successfully generated!\n");
    } catch (Exception e) {
      System.err.println("Unable to generate Java code from WSDL: " + e.getMessage());
    }
  }
}

