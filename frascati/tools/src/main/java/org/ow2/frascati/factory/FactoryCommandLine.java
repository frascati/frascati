/***
 * OW2 FraSCAti Assembly Factory Tools
 * Copyright (C) 2008-2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Christophe Demarey
 * 
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.factory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.Launcher;
import org.ow2.frascati.bootstrap.FraSCAtiFractal;
import org.ow2.frascati.util.FrascatiException;

/**
 * Command line entry point to SCA factories.
 * 
 * @author Christophe Demarey.
 */
public class FactoryCommandLine {
  /** Definitions of option names */
  private static final String HELP_OPT = "help";
  private static final String PATH_OPT = "libpath";
  private static final String SERVICE_NAME_OPT = "service-name";
  private static final String METHOD_NAME_OPT = "method-name";
  private static final String PARAMS_OPT = "p";
  private static final String GF_OUTPUT_OPT = "output-directory";
  private static final String FSCRIPT_OPT  = "fscript";
  /** Create Options object. */
  Options options;
  /** The name of the executable. */
  String commandName = "frascati";
  /** The factory command line */
  CommandLine cmd;
  
  /**
   * Default constructor.
   */
  public FactoryCommandLine() {
    options = new Options();
    
    // add options
    options.addOption("h", HELP_OPT, false, "print this message.");
    
    OptionBuilder.withArgName("lib.jar"+File.pathSeparator+"path/");
    OptionBuilder.hasArgs();
    OptionBuilder.withValueSeparator(File.pathSeparatorChar);
    OptionBuilder.withDescription("a separated list of jars/pathes used by the factory.");
    options.addOption(OptionBuilder.create(PATH_OPT));
    
    options.addOption("t", FSCRIPT_OPT, false, "Enable FraSCAti Script.");
    
    options.addOption("s", SERVICE_NAME_OPT, true, "the name of the service to use."
                      + " If not specified, the composite is run in a (standalone) server mode.");
    options.addOption("m", METHOD_NAME_OPT, true, 
        "the name of the method to invoke. Can be only used if the -s option is specified.");
    
    OptionBuilder.withArgName("param1 param2...");
    OptionBuilder.hasArgs();
    OptionBuilder.withValueSeparator(' ');
    OptionBuilder.withDescription("a list of parameters for the method. Can be only used if the -m option is specified.");
    options.addOption(OptionBuilder.create(PARAMS_OPT));
  }
  
  /**
   * Parse the factory command line.
   * 
   * @param args - Command arguments.
   */
  public void parse(String[] args) {
    CommandLineParser parser = new PosixParser();
    
    try {
      cmd = parser.parse(options, args);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    if ( (args.length == 0) || (cmd.hasOption(HELP_OPT)) ) {
      usage();
      System.exit(-1);
    }
  }
  
  /**
   * Print command usage
   */
  public void usage() {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp(commandName + " <compositeName> [OPTION] ...\n"
        + "\t<compositeName> is a name of a SCA composite file.", options);      
  }
  
  /**
   * Get the Service name to use.
   * 
   * @return the Service name to use.
   */
  public String getServiceName() {
    return cmd.getOptionValue(SERVICE_NAME_OPT);
  }
  
  /**
   * Get the Method name to invoke.
   * 
   * @return the Method name to invoke.
   */
  public String getMethodName() {
    return cmd.getOptionValue(METHOD_NAME_OPT);
  }
  
  /**
   * Get the parameters of the method to invoke.
   * 
   * @return the parameters of the method to invoke.
   */
  public String[] getParams() {
    String[] params = cmd.getOptionValues(PARAMS_OPT);
    if(params != null)
      return params;
    else
      return new String[0];
  }
  
  /**
   * Get the Classpath used by the factory.
   * 
   * @return the Classpath used by the factory.
   */
  public String[] getLibPath() {
    String[] libpath = cmd.getOptionValues(PATH_OPT);
    if (libpath != null)
      return libpath;
    else 
      return new String[0];
  }
  
  /**
   * Get the path of the base output directory.
   * 
   * @return the path of the base output directory.
   */
  public String getBaseDir() {
    String value = cmd.getOptionValue(GF_OUTPUT_OPT);
    return (value == null) ? "." : value;
  }
  
  /**
   * Query to see if Fscript is enabled.
   * 
   * @return true if the option is present (i.e. fscript plugin is activated).
   */
  public boolean isFscriptEnabled() {
      return cmd.hasOption("t");
  }
  
  /**
   * Main method
   * 
   * @param args - Command line arguments
   */
  public static void main(String[] args) throws FrascatiException {
    
    // The factory command line used to retrieve options value.
    FactoryCommandLine cmd = null;
    // List of URLs used to define the classpath
    URL[] urls = null;

    System.out.println("\nOW2 FraSCAti Standalone Runtime");

    // => Parse arguments   
    cmd = new FactoryCommandLine(); 
    cmd.parse(args);
    
    String compositeName = args[0];
    String[] paths = cmd.getLibPath();
    
    urls = new URL[paths.length];
    for (int i = 0; i < urls.length; i++) {
      try {
        urls[i] = new File(paths[i]).toURI().toURL();
      } catch (MalformedURLException e) {
        System.err.println("Error while getting URL for : " + urls[i]);
        e.printStackTrace();
      }
    }

    Launcher launcher = null;

    try {
        // TODO must be configurable from pom.xml files.
        if(cmd.isFscriptEnabled()) {
          System.setProperty("fscript-factory", "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory");
        }
		if (System.getProperty(FraSCAti.FRASCATI_BOOTSTRAP_PROPERTY_NAME) == null) {
          System.setProperty(FraSCAti.FRASCATI_BOOTSTRAP_PROPERTY_NAME, FraSCAtiFractal.class.getCanonicalName());
		}

// TODO
// TODO Reactive this feature.
//        f.getService(f.getFactory(), "juliac", JuliacConfiguration.class).setOutputDir(new File(cmd.getBaseDir()));

        FraSCAti frascati = FraSCAti.newFraSCAti();
        launcher = new Launcher(compositeName, frascati, urls);
    } catch (FrascatiException e) {
    	e.printStackTrace();
        System.err.println("Cannot instantiate the FraSCAti factory!");
        System.err.println("Exiting ...");
        System.exit(-1);
    }
    
    String service = cmd.getServiceName();
    if(service == null) { // Run in a server mode
      System.out.println("FraSCAti is running in a server mode...");
      System.out.println("Press Ctrl+C to quit...");
      Thread.yield();
    } else {
      Object result = launcher.call(service, cmd.getMethodName(), Object.class, (Object[]) cmd.getParams());
      System.out.println("Call done!");
      if(result != null) {
        System.out.println("Service response:");
        System.out.println(result);
      }
    }
  }
}
