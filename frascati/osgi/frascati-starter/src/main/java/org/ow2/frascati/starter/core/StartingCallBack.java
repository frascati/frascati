/**
 * 
 */
package org.ow2.frascati.starter.core;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.logging.Level;

import juliac.generated.SCALifeCycleControllerImpl;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.ow2.frascati.starter.api.InitializableItf;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.reference.ServiceReferenceUtil;

/**
 * Wait for a Component starting to set off a registered action
 */
public class StartingCallBack extends AbstractLoggeable
{   
    /**
     * ServiceReference of the Component which starting
     * is waited
     */
    private ServiceReference<?> waited;
    
    /**
     * Callback's result
     */
    private Object result;

    /**
     * the Object concerned by the Starting event 
     */
    private Object callbackObject;

    /**
     * the array of Objects used as argument to set off
     * the callback 
     */
    private Object[] callbackArguments;

    /**
     * the invoked callback method when the starting event has
     * been received
     */
    private Method callbackMethod;
    
    /**
     * Defines whether the current StartingCallBack instance
     * can be set off more than once
     */
    private boolean reusable;

    private TinfiComponentInterceptor<?> interceptor;
    
    private IntentHandler handler;
    
    private Method method;

    private SCALifeCycleControllerImpl lifecylceController;
    
    protected class StartingIntent implements IntentHandler
    {
        /** 
         * {@inheritDoc}
         * 
         * @see org.ow2.frascati.tinfi.api.IntentHandler#i
         * nvoke(org.ow2.frascati.tinfi.api.IntentJoinPoint)
         */
        public Object invoke(IntentJoinPoint intentJoinPoint) throws Throwable
        {
            Object result = intentJoinPoint.proceed();
            String targetedMethodName = intentJoinPoint.getMethod().getName();
            Class<?> targetedClass = intentJoinPoint.getMethod().getDeclaringClass();
            boolean toSetOff = false;
            
            if(InitializableItf.class.isAssignableFrom(targetedClass)
                    && "initialize".equals(targetedMethodName))
            {
                    toSetOff = true;
                    
            }else if(!InitializableItf.class.isAssignableFrom(targetedClass)
                    && "setFcStarted".equals(targetedMethodName))
            {
                    toSetOff = ((Boolean) result).booleanValue();
            }
            if(toSetOff)
            {
                setOff();
            }
            return result;
        }
    }
    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    /**
     * Weave the IntentHandler passed on as a parameter with the 
     * {@see org.objectweb.fractal.api.control.LifeCycleController} of the SCA object to spy
     * 
     * @param spied
     *          the SCA object to spy
     * @param handler
     *          the IntentHandler to weave
     * @return
     *          true if the IntentHandler has successfully been woven
     *          false otherwise
     */
    protected boolean weaveIntent()
    {   
        try
        {  
            if(interceptor != null)
            {
                interceptor.addIntentHandler(handler, method);  
            
            } else
            {
                Class<?> targetedClass = waited.getClass();
                interceptor = ServiceReferenceUtil.getInterceptor(waited);
                
                if(InitializableItf.class.isAssignableFrom(targetedClass) && 
                        waited != callbackObject)
                {
                    method = InitializableItf.class.getDeclaredMethod("initialize");
                    
                } else if(this.lifecylceController != null)
                {
                    //method call to listen : setFcStarted
                    method = LifeCycleCoordinator.class.getMethod("setFcStarted", 
                            (Class<?>[]) null);
                    //Weave
                    interceptor = (TinfiComponentInterceptor<?>)
                    ((ComponentInterface) this.lifecylceController.getFcCoordinator()
                            ).getFcItfImpl();
                }
                if(method != null)
                {
                    interceptor.addIntentHandler(handler, method);
                    return true;
                }
                return false;
            }
        } catch (Exception e)
        { 
            if(log.isLoggable(Level.SEVERE))
            {
                log.log(Level.SEVERE,e.getMessage(),e);
            }                
        }
        return false;
    }

    /**
     * Retrieve and return the LifecycleController of the ServiceReference object
     * passed on as a parameter
     * 
     * @param spied
     *          ServiceReference Object
     */
    protected SCALifeCycleControllerImpl getLifecycleController(
            ServiceReference<?> spied)
    {
        Field lifecycleField = null;        
        TinfiComponentInterceptor<?> tci = ServiceReferenceUtil.getInterceptor(spied);
        if(tci!= null)
        {
            try
            {   //retrieve the private field referencing the life cycle controller of
                //the spied object
                lifecycleField = tci.getClass().getDeclaredField("_lc");
                lifecycleField.setAccessible(true);
                
                SCALifeCycleControllerImpl lifeCycleController = 
                    (SCALifeCycleControllerImpl)lifecycleField.get(tci);
                
                return lifeCycleController;
                
            } catch (Throwable throwable)
            {
                if(log.isLoggable(Level.SEVERE))
                {
                    log.log(Level.SEVERE,throwable.getMessage(),throwable);
                }                
            }
        }
        return null;
    }     
    
    /**
     * Starting event has been received. Set off the callback method
     */
    protected synchronized void setOff()
    {
        try
        {
            result = callbackMethod.invoke(callbackObject, callbackArguments);
            
        } catch (Throwable throwable)
        {
            log.log(Level.WARNING,throwable.getMessage(),throwable);
            
        } finally 
        {
            if(!reusable && interceptor != null)
            {
                try
                {
                    interceptor.removeIntentHandler(handler,method);
                    
                } catch (NoSuchMethodException e)
                {
                    log.log(Level.WARNING,e.getMessage(),e);
                }
            }
        }
        result = null;
    }
    
    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param waited
     *          ServiceReference of the Component which starting is waited
     */
    public StartingCallBack(ServiceReference<?> waited)
    {
        this(waited,false);
    }

    /**
     * Constructor
     * 
     * @param waited
     *          ServiceReference of the Component which starting is waited
     * @param reusable
     *          define whether the waiter is set off each time the starting event is
     *          received
     */
    public StartingCallBack(ServiceReference<?> waited, boolean reusable)
    {
        this.waited = waited;
        this.lifecylceController = getLifecycleController(waited);
        this.reusable = reusable;
        this.handler = new StartingIntent();
    }
    
    /**
     * @param callbackObject
     * @param callbackMethod
     * @param callbackArguments
     */
    public void schedule(Object callbackObject,Method callbackMethod,
            Object... callbackArguments)
    {
        this.callbackObject = callbackObject;
        this.callbackArguments = callbackArguments;
        this.callbackMethod = callbackMethod;
        int state = -1;
        
        if(this.lifecylceController == null)
        {
            setOff();
            
        } else
        {
            synchronized(this.lifecylceController)
            {
                Class<?> waitedClass = waited.getClass();
                  
                if((state = this.lifecylceController.fcState) == 2 && 
                     (callbackObject == waited 
                       || !InitializableItf.class.isAssignableFrom(waitedClass)
                         || (InitializableItf.class.isAssignableFrom(waitedClass) 
                             && ((InitializableItf)waited).isInitialized())))
                {
                    setOff();
                }
                if((reusable && (state != -1 
                    || InitializableItf.class.isAssignableFrom(waitedClass)))
                        || state == 0)
                {
                    weaveIntent();
                }
            }
        }
    }    

    
    /**
     * Return the result of the callback
     */
    public Object getResult()
    {
        return result;
    }
}
