/**
 * OW2 FraSCAti : Starter
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.api;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

import org.oasisopen.sca.ServiceReference;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.starter.core.StartingCallBack;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Abstract implementation of the StarterItf interface
 */
@EagerInit
@Scope("COMPOSITE")
public abstract class AbstractStarter extends AbstractLoggeable 
implements StarterItf
{
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The total number of components waiting the 'setFcStarted' method call 
     */
    private AtomicInteger totalStarterCount = new AtomicInteger(0);

    /**
     * The number of started components
     */
    private AtomicInteger currentStarterCount = new AtomicInteger(0);    
    
    /**
     * Is the rearmed mechanism available ?
     */
    @Property(name="rearmed-available", required=false)
    protected boolean rearmedAvailable;    

    /**
     * The required CompositeManager
     */
    @Reference(name = "service-reference", required=false)
    protected Object serviceReference;
    
    /**
     * The required Starter Container
     */
    @Reference(name = "reference-container", required=false)
    private Iterator referenceContainer;
    
    /**
     * The required Starter Container
     */
    @Reference(name = "starter-container")
    protected StarterContainerItf starterContainer;    
    
    private boolean referenceRegistered;
    
    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.ReferenceContainerItf#incrementAndGet()
     */
    private boolean incrementAndGet()
    {
        int inc = currentStarterCount.incrementAndGet();
        return inc == totalStarterCount.get();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.ReferenceContainerItf#plus()
     */
    private int plus()
    {
        return totalStarterCount.incrementAndGet();
    }
    
    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    protected AbstractStarter()
    {
        referenceRegistered = false;
    }
    
    /**
     * Initialization
     */
    @Init
    public void init()
    {        
        if(serviceReference != null)
        {
            referenceRegistered = true;
            StartingCallBack callback = new StartingCallBack((ServiceReference<?>)
                serviceReference, rearmedAvailable);
            plus();
            try
            {
                callback.schedule(this, StarterItf.class.getDeclaredMethod(
                        "start"), (Object[])null);
                
            } catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }
        } else if(referenceContainer != null)
        {
            StartingCallBack callback = new StartingCallBack((ServiceReference<?>)
                    referenceContainer, false);
            try
            {
                callback.schedule(this, AbstractStarter.class.getDeclaredMethod(
                        "registerServiceReferences"), (Object[])null);
                
            }catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }
        } 
    }
    
    /**
     * referenceContainer has been started. A callback is registered for 
     * each ServiceReference starting
     */
    public void registerServiceReferences()
    {
        if(referenceRegistered)
        {
            return;
        }
        referenceRegistered = true;
        List<StartingCallBack> callbacks = new ArrayList<StartingCallBack>();
        while(referenceContainer.hasNext())
        {
                ServiceReference<?> sref = (ServiceReference<?>) referenceContainer.next();
                callbacks.add(new StartingCallBack(sref, rearmedAvailable));
                plus();              
        }
        for(StartingCallBack callback : callbacks)
        {
            try
            {
                callback.schedule(this, StarterItf.class.getDeclaredMethod("start"),
                        (Object[])null);
            
            }catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }
        }    
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.StarterItf#start()
     */
    public void start()
    {   
        if(incrementAndGet())
        {
            try
            {   
                this.currentStarterCount = new AtomicInteger(0);
                new StartingCallBack( (ServiceReference<?>) starterContainer ,
                        false ).schedule( this, AbstractStarter.class.getDeclaredMethod(
                                "doStart"), (Object[])null);
                
            }catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }
         }
    }
    
    /**
     * 
     */
    public void doStart()
    {        
        while (starterContainer.hasNext())
        {
            InitializableItf initializable = starterContainer.next();
            try                    
            {
                new StartingCallBack((ServiceReference<?>)
                    initializable, false).schedule(initializable, 
                          InitializableItf.class.getDeclaredMethod("initialize"),
                           (Object[])null);
                
            }  catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }       
        }
        starterContainer.rearmed();
    }
    
}
