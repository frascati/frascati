/**
 * OW2 FraSCAti : Starter
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.core;

import java.util.List;
import java.util.logging.Level;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.starter.api.InitializableItf;
import org.ow2.frascati.starter.api.StarterContainerItf;
import org.ow2.frascati.util.AbstractLoggeable;

@Scope("COMPOSITE")
@Service(StarterContainerItf.class)
public class StarterContainer extends AbstractLoggeable implements
        StarterContainerItf
{

    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * Is the rearmed mechanism available ?
     */
    @Property(name="rearmed-available", required=false)
    boolean rearmedAvailable;
    
    /** The list of InitializableItf Component */
    @Reference(name = "initializables")
    List<InitializableItf> initializables;
    
    /** the InitializableItf Component enumeration index */
    private int index = 0;

    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     * 
     * @see java.util.Iterator#hasNext()
     */
    public boolean hasNext()
    {
        boolean hasNext = index < initializables.size();
        return  hasNext;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.util.Iterator#next()
     */
    public InitializableItf next()
    {
       InitializableItf next = null;
       try
       {
           next = initializables.get(index++);
           
       } catch(IndexOutOfBoundsException ioobe)
       {
           if(log.isLoggable(Level.CONFIG))
           {
               log.log(Level.CONFIG,ioobe.getMessage());
           }
       }
       return next;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.util.Iterator#remove()
     */
    public void remove()
    {
        if(log.isLoggable(Level.CONFIG))
        {
            log.log(Level.CONFIG,"InitilizableItf components cannot be " +
        		"removed from the container");
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.StarterContainerItf#size()
     */
    public int size()
    {
        return initializables.size();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.StarterContainerItf#rearmed()
     */
    public void rearmed()
    { 
        if(rearmedAvailable && index > 0)
        {
            index = 0;
            for(InitializableItf initializable :initializables)
            {
                initializable.setInitializable(true);
            }
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,"StarterContainer["+this+"] rearmed");
            }
        }
    }
}
