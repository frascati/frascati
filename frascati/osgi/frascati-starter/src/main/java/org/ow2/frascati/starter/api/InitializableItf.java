/**
 * OW2 FraSCAti : Starter
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.api;

import org.osoa.sca.annotations.Service;

/** 
 * Allow to link the initialization process of an SCA component to another one
 * The initialize method of an InitializableItf object will be called after the call 
 * of the startFc method of the LifeCycleController of the linked SCA Component
 * */
@Service
public interface InitializableItf
{
    /**
     * Return true if the IntializableItf object has already been initialized,
     * false otherwise.
     */
    boolean isInitialized();
    
    /**
     * Initialization of the InitializableItf implementation object at the 
     * right time
     */
    void initialize();

    /**
     * Define whether the 'initialize' method of the current InitializableItf implementation 
     * instance has to be called or not
     *
     * @param initializable
     *          Is the current InitializableItf instance "initializable" ? 
     */
    void setInitializable(boolean initializable);
    
    /**
     * Return the next InitializableItf object to initialize
     * 
     * @return 
     *          the next InitializableItf object
     */
    InitializableItf getNextInitializable();
}
