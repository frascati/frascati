/**
 * OW2 FraSCAti : Starter
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.api;

import java.util.logging.Level;

import org.oasisopen.sca.ServiceReference;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.starter.core.StartingCallBack;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * AbstractInitializable allow to implement InitializableItf without 
 * being concerned about the initialization chain 
 */
@Scope("COMPOSITE")
public abstract class AbstractInitializable
    extends AbstractLoggeable implements InitializableItf
{
    
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * Defines whether the current {@link InitializableIf} instance 
     * can be rearmed
     */
    @Property(name="rearmed-available", required=false)
    boolean rearmedAvailable;
    
    /** 
     * Defines the next {@link InitializableItf} instance 
     * to initialize 
     * */
    @Reference(name = "next-initializable", required = false)
    protected InitializableItf nextInitializable;
    
    /**
     *  define whether the initialization method has already 
     *  been called or not 
     *  */
    protected boolean initialized;

    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    /**
     * The {@link InitializableItf} instance is effectively
     * initialized   
     */
    protected abstract void doInitialize();

    /**
     * Constructor
     */
    protected AbstractInitializable()
    {
        initialized = false;
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.InitializableItf#isInitialized()
     */
    public boolean isInitialized()
    {
        return initialized;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.InitializableItf
     * #initialize()
     */
    public final void initialize()
    {
        // if the method has already been called ...
        if (initialized)
        { // ..end
            super.log.log(Level.CONFIG, "InitializableIf instance ["+this
                    +"] has already been initialized");
            return;
        }
        try
        {
            // .. else call the doInitialize method
            doInitialize();
            
        } catch (Exception throwable)
        {
            log.log(Level.WARNING," InitializableIf instance ["+this
                    +"]: An error occured while initializing ",throwable);
        }
        // ..if the nextInitializable attribute is not null
        if (nextInitializable != null)
        { 
            // next intialize the next Initializable object
            try                    
            {
                new StartingCallBack((ServiceReference<?>)
                        nextInitializable, false).schedule(nextInitializable, 
                          InitializableItf.class.getDeclaredMethod("initialize"),
                           (Object[])null);
                
            }  catch (Throwable throwable)
            {
                log.log(Level.WARNING,throwable.getMessage(),throwable);
            }
        }
        initialized = true;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.InitializableItf#setInitializable(boolean)
     */
    public void setInitializable(boolean initializable)
    {
        if(!initializable || rearmedAvailable)
        {
            initialized = !initializable;
            
            super.log.log(Level.CONFIG, "InitializableItf ["+ this 
                    +"] setInitializable {" + initialized + "}");
        } 
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.InitializableItf
     * #getNextInitializable()
     */
    public InitializableItf getNextInitializable()
    {
        return nextInitializable;
    }
}
