/**
 * OW2 FraSCAti : Starter
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.api;

import org.osoa.sca.annotations.Service;

/** 
 * The starter service is woven to the LifeCycleController of the component 
 * witch starting up is expected   
 * */
@Service
public interface StarterItf
{
    /**
     * Tell the StarterItf implementation object that the listened Component has 
     * been started. So, InitializableItf registered objects initialization
     * can be executed
     */
    void start();
//    
//    /**
//     * Tell the StarterItf implementation object that the StarterContainerItf
//     * instance has been started (or re-started).
//     */
//    void startContainer();
//    
//    /**
//     * Tell the StarterItf implementation object that the listened Component has 
//     * been stopped
//     */
//    void stop();
}
