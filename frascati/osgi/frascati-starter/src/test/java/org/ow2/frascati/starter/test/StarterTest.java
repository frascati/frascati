/**
 * OW2 FraSCAti : Starter Test
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

/**
 * Starter Test Case
 */
public class StarterTest
{
    private static FraSCAti frascati = null;
    private static Logger log = Logger.getLogger(
            StarterTest.class.getCanonicalName());
    
    private Component root;
    
    static
    {
         try
        {
            frascati = FraSCAti.newFraSCAti();
            
        } catch (FrascatiException e)
        {
            e.printStackTrace();
            log.log(Level.SEVERE,e.getMessage(),e);
        } 
    }
    
    /**
     * Test if the FraSCAti static instance exists and find
     * its 'org.ow2.frascati.FraSCAti' root component 
     */
    @Before
    public void setUp()
    {
        assertNotNull(frascati);

        Component scaContainer;
        try
        {
            scaContainer = frascati.getCompositeManager().getTopLevelDomainComposite();

            ContentController contentController = (ContentController) scaContainer.getFcInterface(
                    "content-controller");
            Component[] components = contentController.getFcSubComponents();
            root = components[0];
            
        } catch (FrascatiException e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
            
        } catch (NoSuchInterfaceException e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
        }        
        assertNotNull(root);
    }
    
    /**
     * Test if InitilizableItf Components have been loaded and if 
     * their 'initialize' method has been called
     * @throws Exception 
     */
    @Test 
    public void testInitilizable() throws Exception
    {
        Class<?> initializableClass =frascati.getClassLoader().loadClass(
        "org.ow2.frascati.starter.api.InitializableItf");
        
        Class<?> toStartClass =frascati.getClassLoader().loadClass(
        "org.ow2.frascati.starter.test.ToStart");
        
        Method getName = toStartClass.getDeclaredMethod("getName");
        Method getInitializationCount = toStartClass.getDeclaredMethod("getInitializationCount");
        Method initialize = initializableClass.getDeclaredMethod("initialize");
        Method setInitializable = initializableClass.getDeclaredMethod("setInitializable", 
                new Class<?>[]{boolean.class});

        Object started_1 = frascati.getService(root, "started-one", toStartClass);
        Object started_2 = frascati.getService(root, "started-two", toStartClass);
        Object started_3 = frascati.getService(root, "started-three", toStartClass);
        Object started_4 = frascati.getService(root, "started-four", toStartClass);
        Object started_5 = frascati.getService(root, "started-five", toStartClass);
        Object started_6 = frascati.getService(root, "started-six", toStartClass);

        assertNotNull(started_1);
        log.log(Level.INFO,(String)getName.invoke(started_1) + " loaded" );
        assertNotNull(started_2);
        log.log(Level.INFO,(String)getName.invoke(started_2) + " loaded" );
        assertNotNull(started_3);
        log.log(Level.INFO,(String)getName.invoke(started_3) + " loaded" );
        assertNotNull(started_4);
        log.log(Level.INFO,(String)getName.invoke(started_4) + " loaded" );
        assertNotNull(started_5);
        log.log(Level.INFO,(String)getName.invoke(started_5) + " loaded" );
        assertNotNull(started_6);
        log.log(Level.INFO,(String)getName.invoke(started_6) + " loaded" );

        setInitializable.invoke(started_1, new Object[]{true});
        initialize.invoke(started_1);
        setInitializable.invoke(started_2, new Object[]{true});
        initialize.invoke(started_2);
        
        int initializationCount=((Integer)getInitializationCount.invoke(started_1)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_1) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(2==initializationCount);

        initializationCount=((Integer)getInitializationCount.invoke(started_2)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_2) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(2==initializationCount);

        initializationCount=((Integer)getInitializationCount.invoke(started_3)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_3) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(1==initializationCount);
        
        initializationCount=((Integer)getInitializationCount.invoke(started_4)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_4) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(1==initializationCount);
        
        initializationCount=((Integer)getInitializationCount.invoke(started_5)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_5) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(2==initializationCount);
        
        initializationCount=((Integer)getInitializationCount.invoke(started_6)).intValue();
        log.log(Level.INFO,(String)getName.invoke(started_6) + 
                " has been initialized " + initializationCount + " time(s)");
        assertTrue(2==initializationCount);
    }
}
