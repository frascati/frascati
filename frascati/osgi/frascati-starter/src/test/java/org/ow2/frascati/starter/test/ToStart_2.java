/**
 * OW2 FraSCAti : Starter Test
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.starter.test;

import java.util.logging.Level;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.starter.api.AbstractInitializable;

/**
 * Test the Starter
 */
@Service(ToStart.class)
public class ToStart_2 extends AbstractInitializable implements ToStart
{

    @Property
    private String name;

    protected int initialization = 0;

    /**
     * The MembraneGeneration which starting up is expected
     * */
    @Reference(name="membrane-generation")
    private MembraneGeneration membraneGeneration;
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.AbstractInitializable#doInitialize()
     */
    @Override
    protected void doInitialize()
    {
        log.log(Level.INFO,"doInitialize method call for " + name); 
        log.log(Level.INFO,"The required membrane generation has been found : " + 
                membraneGeneration.toString());
        initialization++;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.test.ToStart#getName()
     */
    public String getName()
    {
        return this.name;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.test.ToStart#getInitializationCount()
     */
    public int getInitializationCount()
    {
        return initialization;
    }

}
