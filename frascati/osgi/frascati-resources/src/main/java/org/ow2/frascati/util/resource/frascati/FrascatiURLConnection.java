/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.frascati;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 *
 */
public class FrascatiURLConnection extends URLConnection
{   
    /**
     * Logger
     */
    protected static final Logger LOGGER = Logger.getLogger(
            FrascatiURLConnection.class.getCanonicalName());
    
    private long bundleId = -1;
    
    private String embeddedResource = "";
    
    /**
     * Constructor
     * 
     * @param url
     *          the URL to build the FrascatiURLConnection with
     * @throws IOException 
     *          if the url's protocol is not 'frascati'
     */
    protected FrascatiURLConnection(URL url) throws IOException
    {
        super(url);
        String protocol = url.getProtocol();
        
        if("frascati".equals(protocol))
        {  
            String[] hosts = url.getHost().split("#");
            bundleId = Long.parseLong(hosts[0]);
            if(hosts.length == 2)
            {
                embeddedResource = hosts[1];
            }
        } else
        {
            if(LOGGER.isLoggable(Level.CONFIG))
            {
                LOGGER.log(Level.CONFIG,"Unmanaged protocol '" + protocol + "'");
            }
            throw new IOException("Unmanaged protocol '" + protocol + "'");
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLConnection#connect()
     */
    @Override 
    public void connect() throws IOException
    {
        if(LOGGER.isLoggable(Level.CONFIG))
        {
            LOGGER.log(Level.CONFIG,"FrascatiURLConnection["+this+"] connected");
        }
    }
    
    /**
     * {@inheritDoc}
     *
     * @see java.net.URLConnection#getContent()
     */
    @Override 
    public Object getContent() throws IOException
    {
        return translate(true).getContent();
    }
    
    /**
     * {@inheritDoc}
     *
     * @see java.net.URLConnection#getInputStream()
     */
    @Override 
    public InputStream getInputStream() throws IOException
    {
        URL translatedUrl =  translate(false);
        if(translatedUrl == null)
        {
            System.out.println("0000000000000000000000000000000000");
            System.out.println("'" + url + "' TRANSLATION IS NULL");
            System.out.println("0000000000000000000000000000000000");
            return null;
        }
        return translatedUrl.openStream();
    }
    
    /**
     * @return
     * @throws MalformedURLException
     */
    public URL translate(boolean finalize) throws MalformedURLException
    {
        String filePart = url.getFile();
        String pathPart = null;
        String jarPart = null;
        int index = -1;
        
        if(filePart.endsWith("/"))
        {
            filePart.substring(0,filePart.length()-1);
        }
        if(filePart.startsWith("/"))
        {
            filePart = filePart.substring(1);
        }
        if(filePart.endsWith(".jar"))
        {
            index = filePart.lastIndexOf('/');
            jarPart = filePart.substring(index==-1?0:index);
            pathPart = "";
        
        } else
        {   
            jarPart =  embeddedResource;
            pathPart = filePart;
        }
        //retrieve the BundleContext    
        BundleContextRevisionItf<?,?> bundleContext = 
            FrascatiURLConnectionFactory.getInstance().getBundleContext();
        
        //build the searched entry path 
        StringBuilder entryBuilder = new StringBuilder("");
        StringBuilder filePartBuilder = new StringBuilder("");
        
        if(jarPart.length()==0)
        {
            entryBuilder.append(pathPart.length()==0?"META-INF":pathPart);
            
        } else
        {
            entryBuilder.append(jarPart);
            
            if(FrascatiURLConnectionFactory.JAR_PROTOCOL.length() == 0
                    && pathPart.length() > 0)
            {
                entryBuilder.append(FrascatiURLConnectionFactory.JAR_SEPARATOR_CHAR);
                entryBuilder.append(pathPart);
                
            } else if((FrascatiURLConnectionFactory.JAR_PROTOCOL.length() > 0 
                    && finalize) || pathPart.length() > 0)
            {
                filePartBuilder.append(FrascatiURLConnectionFactory.JAR_SEPARATOR_CHAR);
                filePartBuilder.append(pathPart);
            }
        }
        //retrieve the searched resource in the Bundle which identifier is defined as host in the URL 
        URL realURL = bundleContext.getBundle(bundleId).getEntry(entryBuilder.toString());
        
//        System.out.println("\n\tJARPART : " + jarPart + "\n\tPATHPART : " +
//                pathPart + "\n\tentryBuilder : " + entryBuilder.toString()
//                + "\n\tfilePartBuilder : " + filePartBuilder.toString() + "\n\tREALURL : " + realURL);
        
        if(filePartBuilder.length()>0)
        {
                filePartBuilder.insert(0,realURL.toExternalForm());
                realURL = new URL(FrascatiURLConnectionFactory.JAR_PROTOCOL,
                        "",filePartBuilder.toString());
                
        } else if(jarPart.length() == 0 && pathPart.length() == 0)
        {  
            //deal with a bundle's url
            index = realURL.toExternalForm().length()- realURL.getFile().length()+1;
            realURL = new URL(realURL.toExternalForm().substring(0,index));
        }
        return realURL;
    }
    
    
}
