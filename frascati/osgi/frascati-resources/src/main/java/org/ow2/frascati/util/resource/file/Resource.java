/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.file;

import java.io.File;
import java.net.URL;
import java.util.Enumeration;

import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;

/**
 * Directory resource handler
 */
public class Resource extends AbstractResource
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // ---------------------------------------------------------------------------
    
    private URL directoryUrl;
    private File directory;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // ---------------------------------------------------------------------------
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.util.resource.AbstractResource#getBaseURL()
     */
    @Override 
    public URL getBaseURL()
    {
        return directoryUrl;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceEntries()
     */
    @Override
    protected Enumeration<String> getResourceEntries()
    {
        return IOUtils.getFileEntries(directory, filter);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getComparable()
     */
    @Override
    protected String getComparable()
    {
        String dirResourcePath = directory.getAbsolutePath();
        String dirResourceName = IOUtils.separatorTrim(dirResourcePath);
        return dirResourceName;
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param resourceParent
     */
    public Resource(AbstractResource resourceParent)
    {
        super(resourceParent);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#manage(java.lang.Object)
     */
    @Override
    public boolean manage(Object resourceObject)
    {
        if (resourceObject instanceof URL)
        {
            URL resourceUrl = (URL) resourceObject;
            if (!"file".equals(resourceUrl.getProtocol()))
            {
                return false;
            }
            this.directoryUrl = resourceUrl;
            String dirPath = resourceUrl.getPath();
            directory = new File(dirPath);
            if (directory == null || !directory.exists())
            {
                return false;
            }
        } else
        {
            return false;
        }
        resourceURL = directoryUrl;
        return true;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceObject()
     */
    @Override
    public Object getResourceObject()
    {
        return directory;
    }
}