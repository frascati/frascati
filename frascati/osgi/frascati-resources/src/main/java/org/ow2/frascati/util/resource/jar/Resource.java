/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.jar;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.jar.JarFile;

import java.util.logging.Level;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.frascati.FrascatiURLConnection;
import org.ow2.frascati.util.resource.frascati.Handler;

/**
 * Jar file resource handler
 */
public class Resource extends AbstractResource
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    protected String jarName;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    
    protected JarFile getJarFile() throws IOException
    {
        URLConnection connection  = resourceURL.openConnection();
        if(FrascatiURLConnection.class.isAssignableFrom(connection.getClass()))
        {
            return (JarFile)connection.getContent();
        }
        return ((JarURLConnection)new URL("jar","",
                resourceURL.toExternalForm()+"!/").openConnection()).getJarFile();
    }
    
    /**
     * {@inheritDoc}
     * @throws IOException 
     * @throws MalformedURLException 
     * 
     * @see org.ow2.frascati.util.resource.AbstractResource#getBaseURL()
     */
    @Override 
    public URL getBaseURL()
    {
        try
        {
            if(!FrascatiURLConnection.class.isAssignableFrom(
                    resourceURL.openConnection().getClass()))
            {
                return new URL("jar","",resourceURL.toExternalForm()+"!/");
            }
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return resourceURL;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceEntries()
     */
    @Override
    protected Enumeration<String> getResourceEntries()
    {
        try
        {
            JarFile jarFile = getJarFile();
            return IOUtils.getJarEntries(jarFile , filter);
            
        } catch (Exception e)
        {
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getComparable()
     */
    @Override
    protected String getComparable()
    {
        return jarName;
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param resourceParent
     */
    public Resource(AbstractResource resourceParent)
    {
        super(resourceParent);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#manage(java.lang.Object)
     */
    @Override
    public boolean manage(Object resourceObject)
    {
        boolean managed = false;
        
        if (resourceObject instanceof URL)
        {
            URL resourceUrl = (URL) resourceObject;
            try
            {
                URLConnection connection = resourceUrl.openConnection();
                if(FrascatiURLConnection.class.isAssignableFrom(
                        connection.getClass()))
                {
                    Object content = connection.getContent();
                    if(JarFile.class.isAssignableFrom(content.getClass()))
                    {    
                        managed = true;
                        String jarResourcePath = resourceUrl.getFile();
                        jarName = IOUtils.separatorTrim(jarResourcePath);
                        jarName = IOUtils.pathLastPart(jarName);
                        resourceURL = new URL("frascati",resourceUrl.getHost()+"#"+jarName,
                                -1,"", new Handler());
                    }
                } else
                {
                    URL jarResourceURL = new URL("jar","",resourceUrl.toExternalForm()+"!/");
                    URLConnection jarConnection = jarResourceURL.openConnection();
                    
                    if(JarURLConnection.class.isAssignableFrom(jarConnection.getClass()))
                    {
                        managed = true;
                        resourceURL = resourceUrl;
                        String jarResourcePath = resourceUrl.getFile();
                        jarName = IOUtils.separatorTrim(jarResourcePath);
                        jarName = IOUtils.pathLastPart(jarName);
                    }
                } 
                if(managed)
                {
                }
            } catch(IOException ioe)
            {
                log.log(Level.WARNING,ioe.getMessage(),ioe);
            }
        }
        return managed;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getEmbeddedJarResource(java.lang.String)
     */
    @Override
    public AbstractResource getEmbeddedResource(String embeddedResourceName)
    {
        try
        {
            URL resURL = new URL(getBaseURL(),embeddedResourceName);
            File tmpDir = IOUtils.getTmpDir();
            String simpleResourceName = IOUtils
                    .pathLastPart(embeddedResourceName);
            File resourceCopy = new File(new StringBuilder(
                    tmpDir.getAbsolutePath()).append(File.separatorChar)
                    .append(simpleResourceName).toString());
            String resourceCopyPath = resourceCopy.getAbsolutePath();
            JarFile jarFile = getJarFile();
            InputStream is = jarFile.getInputStream(
                    jarFile.getEntry(embeddedResourceName));
            
            IOUtils.copyFromStream(is, resourceCopyPath);
            
            resURL = new URL("jar", "", new StringBuilder("file:")
                    .append(resourceCopyPath).append("!/").toString());

            return newResource(this,resURL);
            
        } catch (Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceObject()
     */
    @Override
    public Object getResourceObject()
    {
        return resourceURL;
    }
}
