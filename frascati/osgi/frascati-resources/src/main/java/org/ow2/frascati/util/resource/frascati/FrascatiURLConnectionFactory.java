/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.frascati;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 *
 */
public class FrascatiURLConnectionFactory
{    

    public static String JAR_SEPARATOR_CHAR = "!/";
    
    public static String JAR_PROTOCOL = "jar";
    
    /**
     * 
     */
    public static final String OSGi_HELPER_CLASSNAME = 
        "org.objectweb.fractal.juliac.osgi.OSGiHelper";

    /**
     * 
     */
    public static final String PLATFORM_ITF_CLASSNAME = 
        "org.objectweb.fractal.juliac.osgi.PlatformItf";
    
    /**
     * 
     */
    private static FrascatiURLConnectionFactory instance;
    
    /**
     * 
     */
    protected static final Logger LOGGER = Logger.getLogger(
            FrascatiURLConnectionFactory.class.getCanonicalName());
    
    /**
     * @return
     */
    public static FrascatiURLConnectionFactory getInstance()
    {
        if(instance == null)
        {
           new FrascatiURLConnectionFactory();
        }
        return instance;
    }
    
    /**
     * 
     */
    protected BundleContextRevisionItf<?,?> bundleContext;
    
    /**
     * Constructor
     */
    protected FrascatiURLConnectionFactory()
    {
        if(instance == null)
        {
            instance = this;
        }
    }
    
    /**
     * @param bundleContext
     */
    public void setBundleContext(BundleContextRevisionItf<?,?> bundleContext)
    {
        this.bundleContext = bundleContext;
    }
    
    /**
     * @return
     */
    public BundleContextRevisionItf<?,?> getBundleContext()
    {
        if(bundleContext == null)
        {
            try
            {
                Class<?> osgiHelperClass = Thread.currentThread().getContextClassLoader(
                ).loadClass(OSGi_HELPER_CLASSNAME);
                
                Class<?> platformItfClass = Thread.currentThread().getContextClassLoader(
                ).loadClass(PLATFORM_ITF_CLASSNAME);
                
                Object platform = osgiHelperClass.getDeclaredMethod(
                        "getPlatform").invoke(null);
                
                bundleContext = (BundleContextRevisionItf<?, ?>) platformItfClass.getDeclaredMethod(
                        "getBundleContext").invoke(platform);
                
            } catch (Exception e)
            {
                LOGGER.log(Level.WARNING,"");
            }
        }
        return bundleContext;
    }
}
