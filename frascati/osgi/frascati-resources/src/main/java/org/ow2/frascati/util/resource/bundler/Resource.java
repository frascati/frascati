/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.bundler;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.logging.Level;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.frascati.Handler;

/**
 * Bundle resource handler
 */
public class Resource extends AbstractResource
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // ---------------------------------------------------------------------------
    
    private BundleRevisionItf<?,?> bundleResource;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceEntries()
     */
    protected Enumeration<String> getResourceEntries()
    {
        Enumeration<String> resourceEntries = OSGiIOUtils.getBundleEntries(
                bundleResource, filter);
        return resourceEntries;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getComparable()
     */
    protected String getComparable()
    {
        return bundleResource.getSymbolicName();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.util.resource.AbstractResource#getBaseURL()
     */
    @Override 
    public URL getBaseURL()
    {
        try
        {
            return new URL("frascati",""+bundleResource.getBundleId(),-1,"", 
                    new Handler(bundleResource));
            
        } catch (IOException e)
        {
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        return null;
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param resourceParent
     */
    public Resource(AbstractResource resourceParent)
    {
        super(resourceParent);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#manage(java.lang.Object)
     */
    public boolean manage(Object resourceObject)
    {
        try
        {
            bundleResource = (BundleRevisionItf<?,?>) resourceObject;
            
        } catch(ClassCastException e)
        {
            return false;
        }         
        resourceURL = getBaseURL();
        if(resourceURL == null)
        {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#isSameResourceFile(java.lang.String)
     */
    public boolean isSameResourceFile(String resourceName)
    {
        if (resourceName.equals(getComparable()))
        {
            return true;
        }
        return false;
    }

    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.util.resource.AbstractResource#getResourceObject()
     */
    public Object getResourceObject()
    {
        return bundleResource;
    }
}
