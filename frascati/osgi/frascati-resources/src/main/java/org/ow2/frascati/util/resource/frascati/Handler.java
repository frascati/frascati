/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.frascati;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 *
 */
public class Handler extends URLStreamHandler
{
    /**
     * 
     */
    public Handler()
    {
        super();
    }
    
    /**
     * @param bundleResource
     */
    public Handler(BundleRevisionItf<?, ?> bundleResource)
    {
        super();
        if(FrascatiURLConnectionFactory.getInstance().bundleContext == null)
        {
            FrascatiURLConnectionFactory.getInstance().bundleContext = 
                bundleResource.getBundleContext();
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see java.net.URLStreamHandler#openConnection(java.net.URL)
     */
    @Override 
    protected URLConnection openConnection(URL url) throws IOException
    {
        return new FrascatiURLConnection(url);
    }
}
