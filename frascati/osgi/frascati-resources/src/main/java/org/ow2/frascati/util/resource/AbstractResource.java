/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.resource;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.frascati.Handler;

/**
 * Generic resource that can be a file, a jar file, an OSGi bundle, or any type 
 * registered using an {@link org.ow2.frascati.util.resource.plugin.Plugin}  
 */
public abstract class AbstractResource
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * System property name to define packages containing AbstractResource inherited
     * classes
     */
    public static final String RESOURCE_PROP = 
        "org.ow2.frascati.util.resource";
    
    /**
     * The default value of the RESOURCE_PROP property
     */
    public static final String RESOURCE_DEFAULT = 
        "bundle|bundler|jar|file";
    
    /**
     * The ResourceFilter class name to use
     */
    public static final String RESOURCE_FILTER_PROP = 
        "org.ow2.frascati.util.resource.filter";
    
    /**
     * The default value of the RESOURCES_FILTER_PROP property
     */
    protected static final String RESOURCE_FILTER_DEFAULT = 
        "org.ow2.frascati.util.resource.ResourceFilterImpl";
    
    /**
     * System property name to extend defined packages containing AbstractResource
     * inherited classes
     */
    public static final String PLUGIN_CLASS = 
        "org.ow2.frascati.util.resource.plugin.Plugin";
    
    /**
     * Fetch RESOURCES_FILTER_PROP property value use the RESOURCES_FILTER_DEFAULT value
     * if it's null
     */
    protected static String resourceFilterProp = null;
    
    /**
     * Logger
     */
    protected static final Logger log = Logger.getLogger(AbstractResource.class.getCanonicalName());
    
    
    // Identify packages of AbstractResource-inherited classes
    static
    {
        try
        { 
            // Is there a defined plugin Class ... ?
            Class<?> pluginClass = AbstractResource.class.getClassLoader().loadClass(PLUGIN_CLASS);
            // If there is, it is instantiated...
            AbstractPlugin plugin = (AbstractPlugin) pluginClass.newInstance();
            // ...and called
            plugin.plug();
            
        } catch (Exception e)
        {
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        
        // finalize the AbstractResource system property definition
        String resourceProp = System.getProperty(RESOURCE_PROP, null);
        resourceProp = new StringBuilder(
                (resourceProp != null) ? resourceProp : "").append(
                (resourceProp != null) ? "|" : "").append(RESOURCE_DEFAULT).toString();
        
        System.setProperty(RESOURCE_PROP, resourceProp);
        
        resourceFilterProp = System.getProperty(RESOURCE_FILTER_PROP,RESOURCE_FILTER_DEFAULT);
        log.log(Level.INFO,"\n"+
                "Define resources property [" + RESOURCE_PROP + " = "  + resourceProp + "]" +
                "\nDefine resources filter property [" +
                RESOURCE_FILTER_PROP +" = " + resourceFilterProp + "]");

        String frascatiProtocolHandlerPackage = "org.ow2.frascati.util.resource";
        String protocolPackageHandlers = System.getProperty("java.protocol.handler.pkgs");
        
        if(protocolPackageHandlers == null)
        {
            protocolPackageHandlers = frascatiProtocolHandlerPackage;
            
        } else if(!protocolPackageHandlers.contains(frascatiProtocolHandlerPackage))
        {
            protocolPackageHandlers = new StringBuilder(protocolPackageHandlers).append(
                    "|").append(frascatiProtocolHandlerPackage).toString();
        }
        log.log(Level.INFO,"register 'frascati' protocol package Handler: " + 
                protocolPackageHandlers);
        System.setProperty("java.protocol.handler.pkgs",protocolPackageHandlers);
    }

    /**
     * Create an AbstractResource instance for the object passed on as a parameter
     * 
     * @param resourceObject
     *            the object to encapsulate in a resource instance
     * @return a resource object
     */
    public static AbstractResource newResource(AbstractResource parentResource, 
            Object resourceObject)
    {
        int i = 0;
        AbstractResource resource = null;
        String registeredResourcesStr = System.getProperty(RESOURCE_PROP);
        String[] registeredResources = null;
        if (registeredResourcesStr != null)
        {
            registeredResources = registeredResourcesStr.split("\\|");
            if (registeredResources.length == 0)
            {
                return null;
            }
        } else
        {
            return null;
        }
        for (; i < registeredResources.length; i++)
        {
            try
            {
                String resourceClassName = new StringBuilder(
                        "org.ow2.frascati.util.resource.").append(
                        registeredResources[i]).append(".Resource").toString();
                
                @SuppressWarnings("unchecked")
                Class<? extends AbstractResource> resourceClass = 
                    (Class<? extends AbstractResource>) Class.forName(resourceClassName);
                
                Constructor<? extends AbstractResource> constructor = resourceClass.getConstructor(
                        new Class<?>[]{AbstractResource.class});
                
                resource = constructor.newInstance(new Object[]{parentResource});                
                if (resource.manage(resourceObject))
                {
                    return resource;
                    
                }else
                {
                    log.log(Level.CONFIG,"cannot manage resource '" + 
                            resourceObject + "' using " + resourceClass);  
                }
            } catch (Exception e)
            {
                log.log(Level.CONFIG,e.getMessage(),e);   
            }
        }
        log.log(Level.WARNING, "No resource type found for " + resourceObject);
        return null;
    }

    protected URL resourceURL;
    protected ResourceFilter filter = null;
    protected boolean built;
    
    final protected Map<String, List<String>> entries;
    final protected List<String> packages;
    final protected List<String> embeddedJars;
    final protected AbstractResource parentResource;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     */
    protected AbstractResource(AbstractResource resourceParent)
    {
        built = false;
        entries = new HashMap<String, List<String>>();
        packages = new ArrayList<String>();
        embeddedJars = new ArrayList<String>();
        parentResource = resourceParent;
        try
        {
            @SuppressWarnings("unchecked")
            Class<FilenameFilter> filterClass = (Class<FilenameFilter>) 
            Class.forName(resourceFilterProp);
            filter = (ResourceFilter) filterClass.newInstance();
            filter.setEmbeddedjars(embeddedJars);
            
        } catch (Exception e)
        {
            if(log.isLoggable(Level.INFO))
            {
                log.log(Level.INFO, e.getMessage(), e);
            }
        } finally
        {
            if (filter == null)
            {
                Class<?> filterClass;
                try
                {
                    filterClass = Class.forName(RESOURCE_FILTER_DEFAULT);
                    filter = (ResourceFilter) filterClass.newInstance();
                    filter.setEmbeddedjars(embeddedJars);
                    
                } catch (Exception e)
                {
                    if(log.isLoggable(Level.INFO))
                    {
                        log.log(Level.INFO, e.getMessage(), e);
                    }
                }
            }
        }
    }
    
    /**
     * Each AbstractResource inherited class has to define the way to retrieve an entry's
     * URL
     * 
     * @param entryPath
     *            The searched entry
     * @return The URL of the entry
     */
    public abstract URL getBaseURL();

    /**
     * Each AbstractResource inherited class has to define the way to enumerate entries
     * list
     * 
     * @return The entries list enumeration
     */
    protected abstract Enumeration<String> getResourceEntries();

    /**
     * Each AbstractResource inherited class has to define the comparable string used to
     * identify same handled resource objects
     * 
     * @return The comparable string identifier
     */
    protected abstract String getComparable();

    /**
     * @param entry
     * @return
     */
    private String formatEntry(String entry)
    {
        String formatedEntry = (entry.endsWith("/") || entry.endsWith("\\"))?
                entry.substring(0,entry.length()-1):entry;
                
        formatedEntry = (formatedEntry.startsWith("/") || formatedEntry.startsWith("\\"))?
                formatedEntry.substring(1):formatedEntry;
                
        return  formatedEntry;
    }
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    public abstract Object getResourceObject();

    public abstract boolean manage(Object resourceObject);

    /**
     * Return the list of packages name that can be found in this AbstractResource
     */
    public List<String> packages()
    {
        return Collections.unmodifiableList(packages);
    }
    
    /**
     * Define the list of embedded resources which have not to be use to create
     * AbstractResource objects
     * 
     * @param unmanagedResources
     *            List of embedded files
     */
    public void setUnmanagedResourcesList(List<String> unmanagedResources)
    {
        if (filter != null)
        {
            filter.setUnmanagedResources(unmanagedResources);
        }
    }

    /**
     * Build the list of relative paths to files contained in the resource object
     */
    public synchronized void buildEntries()
    {
        Enumeration<String> resourceEntries = getResourceEntries();
        if (resourceEntries == null)
        {
            if(log.isLoggable(Level.WARNING))
            {
                log.log(Level.WARNING,
                    "call to the resource entries search has returned a null Enumeration");
            }
            return;
        }
        try
        {
            while (resourceEntries.hasMoreElements())
            {
                String entry = resourceEntries.nextElement();
                entry = formatEntry(entry);
                int index = entry.lastIndexOf('/');
                if(index == -1 && File.separatorChar!='/')
                {
                    index = entry.lastIndexOf('\\');
                } 
                String packageName = index==-1?
                        "_root_":entry.substring(0,index).replace('/','.').replace('\\','.');
                String resourceName = index==-1?entry:entry.substring(index+1);
                
                List<String> list = entries.get(packageName);
                if (list == null)
                {
                    list = new ArrayList<String>();
                    entries.put(packageName, list);
                }
                list.add(resourceName);
                if(!packages.contains(packageName))
                {
                    packages.add(packageName);
                }
            }
        } catch (Exception e)
        {
            if(log.isLoggable(Level.WARNING))
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }
        built = true;
    }

    /**
     * Test whether the entries list has been build
     * 
     * @return True if the BuildEntries method has been called False otherwise
     */
    public boolean isBuilt()
    {
        return built;
    }

    /**
     * Return the URL of the searched parameter if it has been found in the resource
     * object
     * 
     * @param searched
     *            the resource name to search
     * @param exact
     *            true if the path has to be exactly the same (for class file search)
     *            false if the path has only to end by the searched parameter
     * @return the resource URL
     */
    public URL getResource(String packageName,String resourceName,
            String searched, boolean exact)
    {
        String entry = formatEntry(searched);
        if("_root_".equals(packageName))
        {
            for(String packageNameItem : packages)
            {
                List<String> list = entries.get(packageNameItem);
                if (list == null)
                {
                    continue;
                }
                for (int i = 0; i < list.size(); i++)
                {
                    String resourceStr = list.get(i);
                    
                    if(resourceName.equals(resourceStr))
                    {
                        String entryComplete = "_root_".equals(packageNameItem)?
                                resourceName:new StringBuilder(
                                        packageNameItem.replace('.','/')).append(
                                        '/').append(resourceName).toString();
                        try
                        {
                            URL result = null;
                            URL baseURL = getBaseURL();
                            if("frascati".equals(baseURL.getProtocol()))
                            {
                                result = new URL(baseURL,entryComplete,new Handler());
                            } else
                            {
                                result = new URL(baseURL,entryComplete);
                            }
                            return result;
                        
                        } catch(MalformedURLException e)
                        {
                            if(log.isLoggable(Level.WARNING))
                            {
                                log.log(Level.WARNING,e.getMessage(),e);
                            }
                        }
                    }
                }
            }
        } else
        {
            List<String> list = entries.get(packageName);
            if (list != null)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    if(resourceName.equals(list.get(i)))
                    {
                        try
                        {
                            URL result = null;
                            URL baseURL = getBaseURL();
                            if("frascati".equals(baseURL.getProtocol()))
                            {
                                result = new URL(baseURL,entry,new Handler());
                            } else
                            {
                                result = new URL(baseURL,entry);
                            }
                            return result;
                        
                        } catch(Exception e)
                        {
                            if(log.isLoggable(Level.WARNING))
                            {
                                log.log(Level.WARNING,e.getMessage(),e);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Add the contained resources' URLs which name is passed on as parameter to the list
     * passed on as parameter
     * 
     * @param searched
     *            the searched resources name
     * @param results
     *            the list to complete
     */
    public void getResources(String packageName,String resourceName,
            String searched, List<URL> results)
    {
        String entry = formatEntry(searched);
        if("_root_".equals(packageName))
        {
            for(String packageNameItem : packages)
            {
                List<String> list = entries.get(packageNameItem);
                if (list == null)
                {
                    continue;
                }
                for (int i = 0; i < list.size(); i++)
                {
                    String resourceStr = list.get(i);
                    
                    if(resourceName.equals(resourceStr))
                    {
                        String entryComplete = "_root_".equals(packageNameItem)?
                                resourceName:new StringBuilder(
                                        packageNameItem.replace('.','/')).append(
                                        '/').append(resourceName).toString();
                        try
                        {
                            URL result = null;
                            URL baseURL = getBaseURL();
                            if("frascati".equals(baseURL.getProtocol()))
                            {
                                result = new URL(baseURL,entryComplete,new Handler());
                            } else
                            {
                                result = new URL(baseURL,entryComplete);
                            }
                            results.add(result);
                        
                        } catch(MalformedURLException e)
                        {
                            if(log.isLoggable(Level.WARNING))
                            {
                                log.log(Level.WARNING,e.getMessage(),e);
                            }
                        }
                    }
                }
            }
        } else
        {
            List<String> list = entries.get(packageName);
            if (list != null)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    if(resourceName.equals(list.get(i)))
                    {
                        try
                        {
                            URL result = null;
                            URL baseURL = getBaseURL();
                            if("frascati".equals(baseURL.getProtocol()))
                            {
                                result = new URL(baseURL,entry,new Handler());
                            } else
                            {
                                result = new URL(baseURL,entry);
                            }
                            results.add(result);
                        
                        } catch(MalformedURLException e)
                        {
                            if(log.isLoggable(Level.WARNING))
                            {
                                log.log(Level.WARNING,e.getMessage(),e);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Add the contained resources' URLs which relative path matches the regular
     * expression passed on as parameter to the list passed on as parameter
     * 
     * @param regex
     *            the regular expression to match
     * @param results
     *            the list to complete
     */
    public void getRegExpResources(String regex, List<URL> results)
    {
        Pattern pattern = null;
        try
        {
            pattern = Pattern.compile(regex);
            
        } catch (PatternSyntaxException e)
        {
            log.log(Level.INFO, 
                    "The syntax of the regular expression is not valid :"
                       + e.getMessage());
            return;
        }
        for(String packageNameItem : entries.keySet())
        {
           List<String> list = entries.get(packageNameItem);
           if (list == null)
           {
               continue;
           }
           String packageResource = new String(packageNameItem);
           packageResource = packageResource.replace('.', '/');
           
           for (int i = 0; i < list.size(); i++)
           {
              String resource = packageResource + '/' + list.get(i);
              Matcher matcher = pattern.matcher(resource);
              if (matcher.find())
              {
                  try
                  {
                      URL result = null;
                      URL baseURL = getBaseURL();
                      if("frascati".equals(baseURL.getProtocol()))
                      {
                          result = new URL(baseURL,resource,new Handler());
                      } else
                      {
                          result = new URL(baseURL,resource);
                      }
                      results.add(result);
                  
                  } catch(MalformedURLException e)
                  {
                      if(log.isLoggable(Level.CONFIG))
                      {
                          log.log(Level.CONFIG,e.getMessage(),e);
                      }
                  }
              }
           }                
        }
    }

    /**
     * Try to build an AbstractResource instance from a contained resource
     * 
     * @param embeddedResourceName
     *            the contained resource name
     * @return an AbstractResource object
     */
    public AbstractResource getEmbeddedResource(String embeddedResourceName)
    {
        try
        {
            URL result = new URL(getBaseURL(),embeddedResourceName);
            AbstractResource newResource = newResource(this,result);
            return newResource;
        
        } catch(Exception e)
        {
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        return null;
    }

    /**
     * Try to build an AbstractResource instance from a contained resource that has been
     * cached first
     * 
     * @param embeddedResourceName
     *            the contained resource's name
     * @param cacheDirPath
     *            the directory where to cache the resource
     * @return an AbstractResource object
     */
    public AbstractResource cacheEmbeddedResource(String embeddedResourceName,
            String cacheDirPath)
    {
        URL result = null;
        try
        {
            result = new URL(getBaseURL(),embeddedResourceName);
        
        } catch(Exception e)
        {
            if(log.isLoggable(Level.CONFIG))
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        if (result != null)
        {
            if (cacheDirPath == null)
            {
                cacheDirPath = IOUtils.getTmpDir().getAbsolutePath();
            }
            try
            {
                int index = embeddedResourceName.lastIndexOf('/');
                if(index == -1 && File.separatorChar!='/')
                {
                    index = embeddedResourceName.lastIndexOf('\\');
                } 
                String resourceCopyName = embeddedResourceName.substring(index + 1);
                String resourceCopyPath = new StringBuilder(cacheDirPath)
                        .append(File.separatorChar).append(resourceCopyName)
                        .toString();
                IOUtils.copyFromStream(result.openStream(), resourceCopyPath);
                AbstractResource newResource = newResource(null,
                        new File(resourceCopyPath).toURI().toURL());
                return newResource;
                
            } catch (IOException e)
            {
                e.printStackTrace();
                log.warning("Unable to cache embedded resource :"
                        + embeddedResourceName);
            }
            return newResource(this,result);
        } else
        {
            log.warning("Unable to find embedded resource :"
                    + embeddedResourceName);
        }
        return null;
    }

    /**
     * Return the handled resource's URL
     * 
     * @return the resource's URL
     */
    public URL getResourceURL()
    {
        return resourceURL;
    }
    
    /**
     * Return the list of the resource's embedded jar files
     * 
     * @return the list of embedded jar files
     */
    public final List<String> getEmbeddedJars()
    {
        return embeddedJars;
    }

    /**
     * Test whether the current handled resource has the same name as the one passed on as
     * parameter
     * 
     * @param resourceName
     *            the resource name to test
     * @return true if the current handled resource has the same name as the one passed on
     *         as parameter false otherwise
     */
    public boolean isSameResourceFile(String resourceName)
    {
        if (getComparable().equals(resourceName))
        {
            return true;
        }
        return false;
    }

    public boolean equals(Object object)
    {
        if (object instanceof AbstractResource)
        {
            return getComparable().equals(((AbstractResource) object).getComparable());
        
        } else if (object instanceof String)
        {
            return getComparable().equals((String) object);
        }
        return object==this;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return getComparable();
    }
}
