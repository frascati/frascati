/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.resource;

import java.io.File;
import java.util.List;

public class ResourceFilterImpl implements ResourceFilter
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private List<String> unmanagedResources;
    private List<String> embeddedJars;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.FileFilter#setEmbeddedAsResource(java.util.List)
     */
    public void setUnmanagedResources(List<String> unmanagedResources)
    {
        this.unmanagedResources = unmanagedResources;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.FileFilter#setEmbeddedjars(java.util.List)
     */
    public void setEmbeddedjars(List<String> embeddedJars)
    {
        this.embeddedJars = embeddedJars;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(File f, String name)
    {
        if (unmanagedResources != null)
        {
            int n = 0;
            while (n < unmanagedResources.size())
            {
                String unmanagedResource = unmanagedResources.get(n++);
                if (name.endsWith(unmanagedResource))
                {
                    return true;
                }
            }
        }
        if (name.endsWith(".jar"))
        {
            embeddedJars.add(name);
            return false;
        }
        return true;
    }
};
