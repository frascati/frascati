/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.explorer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;

import java.util.logging.Logger;
import java.util.logging.Level;


@SuppressWarnings("serial")
public class BundleGraphicEcho extends JPanel implements BundleListenerRevisionItf,
        ActionListener, IntrospectPanelListener
{
    private JTextPane nameArea;
    private JTextPane stateArea;
    private JButton bundleButton;
    private BundleEchoMenu menu;
    private Dictionary<?, ?> headers;
    private long bundleId;
    private boolean activator;
    private String location;
    private BundleEchoListener listener;
    private String state = null;

    private static final Logger LOGGER = Logger.getLogger(
            BundleGraphicEcho.class.getCanonicalName());
    
    /**
     * @param bundle
     */
    public BundleGraphicEcho(BundleRevisionItf<?,?> bundle)
    {
        super();
        MouseListener ml = new BundleEchoMouseListener();
        setLayout(null);
        setPreferredSize(new Dimension(150, 150));
        setBackground(Color.white);
        addMouseListener(ml);
        bundleButton = new JButton();
        bundleButton.setContentAreaFilled(false);
        bundleButton.setBounds(40, 5, 70, 70);
        bundleButton.setActionCommand("display");
        bundleButton.addActionListener(this);
        nameArea = new JTextPane();
        nameArea.setBounds(1, 75, 148, 50);
        nameArea.setEditorKit(new HTMLEditorKit());
        nameArea.setEditable(false);
        nameArea.addMouseListener(ml);
        stateArea = new JTextPane();
        stateArea.setEditorKit(new HTMLEditorKit());
        stateArea.setBounds(1, 125, 148, 20);
        stateArea.setEditable(false);
        stateArea.addMouseListener(ml);
        HTMLDocument nameDoc = (HTMLDocument) nameArea.getEditorKit().createDefaultDocument();
        nameArea.setDocument(nameDoc);

        String name = ((name = bundle.getHeaders().get("Bundle-Name")) == null) ? 
                bundle.getHeaders().get("Bundle-SymbolicName"):name;
        location = (((location = bundle.getHeaders().get("Bundle-Location")) == null) ? 
                bundle.getLocation() : location);
        activator = bundle.getHeaders().get("Bundle-Activator") != null || 
        bundle.getHeaders().get("Service-Component") != null;
        try
        {
            nameDoc.insertAfterStart(nameDoc.getDefaultRootElement(),
                    "<div style='text-align:center;font-size:9pt;font-family:Courrier'>"
                            + name + "</div>");
            
        } catch (BadLocationException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        menu = new BundleEchoMenu(this);
        bundleButton.setComponentPopupMenu(menu);
        nameArea.setComponentPopupMenu(menu);
        stateArea.setComponentPopupMenu(menu);
        setComponentPopupMenu(menu);
        add(bundleButton);
        add(nameArea);
        add(stateArea);
        bundleId = bundle.getBundleId();
        bundleChanged(bundle,0);
    }

    /**
     * @param listener
     */
    public void setBundleEchoListener(BundleEchoListener listener)
    {
        this.listener = listener;
    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e)
    {
        if (listener != null)
        {
            listener.bundleEchoSelected(bundleId);
            String commande = e.getActionCommand();
            if ("display".equals(commande))
            {
                // always done
            } else if ("start".equals(commande))
            {
                listener.bundleEchoAsksForStart(bundleId);
            } else if ("stop".equals(commande))
            {
                listener.bundleEchoAsksForStop(bundleId);
            } else if ("update".equals(commande))
            {
                listener.bundleEchoAsksForUpdate(bundleId);
            } else if ("install".equals(commande))
            {
                listener.bundleEchoAsksForInstall(location);
            } else if ("uninstall".equals(commande))
            {
                listener.bundleEchoAsksForUninstall(bundleId);
            }
        }
    }

    /**
     * @param install
     * @param start
     * @param stop
     * @param uninstall
     * @param update
     */
    private void displayBundleMenu(boolean install, boolean start,
            boolean stop, boolean uninstall, boolean update)
    {
        menu.install.setVisible(install);
        menu.start.setVisible(start);
        menu.stop.setVisible(stop);
        menu.uninstall.setVisible(uninstall);
        menu.update.setVisible(update);
    }

    /**
     *  {@inheritDoc}
     *  
     * @see org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf#
     * bundleChanged(org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf, int)
     */
    public void bundleChanged(BundleRevisionItf bundle, int type)
    {       
        if (bundle.getBundleId() == bundleId)
        {
            String state = bundle.getBundleContext().mapBundleState(
                    bundle.getState());
            
            if("ACTIVE".equals(state))
            {
                bundleButton.setIcon(activator ? ResourceHelper
                        .getIcon("activeIcon") : ResourceHelper
                        .getIcon("activeServiceIcon"));
                state = "Active";
                displayBundleMenu(false, false, true, true, true);
             
            } else if("INSTALLED".equals(state))
            {
                bundleButton.setIcon(activator ? ResourceHelper
                        .getIcon("installedIcon") : ResourceHelper
                        .getIcon("inactiveServiceIcon"));
                state = "Installed";
                displayBundleMenu(false, true, false, true, true);
                
            }else if("RESOLVED".equals(state))
            {
                bundleButton.setIcon(activator ? ResourceHelper
                        .getIcon("stoppedIcon") : ResourceHelper
                        .getIcon("activeServiceIcon"));
                state = "Resolved";
                displayBundleMenu(false, true, false, true, true);
                
            }else if("STARTING".equals(state))
            {
                bundleButton.setIcon(ResourceHelper
                        .getIcon("inactiveServiceIcon"));
                state = "Starting";
                displayBundleMenu(false, false, false, false, false);
                
            }else if("STOPPING".equals(state))
            {
                bundleButton.setIcon(ResourceHelper
                        .getIcon("inactiveServiceIcon"));
                state = "Stopping";
                displayBundleMenu(false, false, false, false, false);
                    
            }else if("UNINSTALLED".equals(state))
            {
                bundleButton.setIcon(ResourceHelper
                        .getIcon("uninstallIcon"));
                state = "Uninstalled";
                displayBundleMenu(false, false, false, false, false);
            }
            headers = bundle.getHeaders();
            HTMLDocument stateDoc = (HTMLDocument) stateArea.getEditorKit()
                    .createDefaultDocument();
            stateArea.setDocument(stateDoc);
            try
            {
                stateDoc.insertAfterStart(stateDoc.getDefaultRootElement(),
                        "<div style='text-align:center;font-size:9pt;font-weight:bold;'>"
                                + state + "</div>");
            } catch (BadLocationException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IOException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
        }
        
    }
    
    /* (non-Javadoc)
     * @see java.awt.Component#toString()
     */
    public String toString()
    {
        String str = null;
        if (headers != null)
        {
            StringBuilder builder = new StringBuilder();
            Enumeration<?> keys = headers.keys();
            while (keys.hasMoreElements())
            {
                String key = (String) keys.nextElement();
                String value = (String) headers.get(key);
                builder.append(key);
                builder.append(" : ");
                builder.append(value);
                builder.append("\n");
            }
            str = builder.toString();
        }
        return str;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.osgi.introspect.explorer.IntrospectPanelListener#getState()
     */
    public String getState()
    {
        return state;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.osgi.introspect.explorer.IntrospectPanelListener#alreadyKnown(long)
     */
    public boolean alreadyKnown(long bundleId)
    {
        return bundleId == this.bundleId;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.osgi.introspect.explorer.IntrospectPanelListener#newSelected(long)
     */
    public void newSelected(long bundleId)
    {
        setBorder(bundleId == this.bundleId ? BorderFactory
                .createLineBorder(Color.blue) : null);
        if (bundleId == this.bundleId)
        {
            listener.bundleEchoAsksForDisplay(toString());
        }
    }

    /**
     * @author munilla
     *
     */
    private class BundleEchoMenu extends JPopupMenu
    {
        JMenuItem start;
        JMenuItem stop;
        JMenuItem uninstall;
        JMenuItem install;
        JMenuItem update;

        public BundleEchoMenu(BundleGraphicEcho parent)
        {
            super();
            addComponentListener(new ComponentListener()
            {
                public void componentHidden(ComponentEvent arg0)
                {
                }

                public void componentMoved(ComponentEvent arg0)
                {
                }

                public void componentResized(ComponentEvent arg0)
                {
                }

                public void componentShown(ComponentEvent arg0)
                {
                    actionPerformed(new ActionEvent(this, -1, "display"));
                }
            });
            start = new JMenuItem("start");
            start.setIcon(ResourceHelper.getIcon("startIcon"));
            start.setMnemonic(KeyEvent.VK_S);
            start.setActionCommand("start");
            start.addActionListener(BundleGraphicEcho.this);
            stop = new JMenuItem("stop");
            stop.setIcon(ResourceHelper.getIcon("stopIcon"));
            stop.setMnemonic(KeyEvent.VK_E);
            stop.setActionCommand("stop");
            stop.addActionListener(parent);
            update = new JMenuItem("update");
            update.setIcon(ResourceHelper.getIcon("updateIcon"));
            update.setMnemonic(KeyEvent.VK_U);
            update.setActionCommand("update");
            update.addActionListener(parent);
            install = new JMenuItem("install");
            install.setIcon(ResourceHelper.getIcon("installIcon"));
            install.setMnemonic(KeyEvent.VK_I);
            install.setActionCommand("install");
            install.addActionListener(parent);
            uninstall = new JMenuItem("uninstall");
            uninstall.setIcon(ResourceHelper.getIcon("uninstallIcon"));
            uninstall.setMnemonic(KeyEvent.VK_K);
            uninstall.setActionCommand("uninstall");
            uninstall.addActionListener(parent);
            add(start);
            add(stop);
            add(update);
            add(install);
            add(uninstall);
        }
    }

    private class BundleEchoMouseListener implements MouseListener
    {
        public void mouseClicked(MouseEvent arg0)
        {
        }

        public void mouseEntered(MouseEvent arg0)
        {
        }

        public void mouseExited(MouseEvent arg0)
        {
        }

        public void mouseReleased(MouseEvent arg0)
        {
        }

        public void mousePressed(MouseEvent arg0)
        {
            actionPerformed(new ActionEvent(this, -1, "display"));
        }
    }
}
