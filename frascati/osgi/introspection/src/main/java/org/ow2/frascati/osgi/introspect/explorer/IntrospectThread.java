/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.explorer;

import java.util.logging.Logger;
import java.util.logging.Level;

public class IntrospectThread extends Thread
{
    private final IntrospectPanel panel;
    private boolean running;

    private static final Logger log = Logger.getLogger(
            IntrospectThread.class.getCanonicalName());
    
    public IntrospectThread(IntrospectPanel panel)
    {
        this.panel = panel;
        running = true;
    }

    public void run()
    {
        if (panel != null)
        {
            panel.init();
            while (running)
            {   
                try
                {                    
                    if (panel.getSelected() != null)
                    {
                        String[] servicesEvents = panel.getSelected()
                                .getServiceEventsStack();
                        String[] bundlesEvents = panel.getSelected()
                                .getBundleEventsStack();
                        String[] frameworkEvents = panel.getSelected()
                                .getFrameworkEventsStack();
                        
                        int i = 0;
                        for (; i < servicesEvents.length; i++)
                        {
                            panel.writeMessage(servicesEvents[i], false, true);
                        }
                        i = 0;
                        for (; i < bundlesEvents.length; i++)
                        {
                            panel.writeMessage(bundlesEvents[i], false, true);
                        }
                        i = 0;
                        for (; i < frameworkEvents.length; i++)
                        {
                            panel.writeMessage(frameworkEvents[i], false, true);
                        }
                    }
                    sleep(200);
                    
                } catch (InterruptedException e)
                {
                    log.log(Level.WARNING,e.getMessage(),e);
                    Thread.interrupted();
                    break;
                }
            }
        }
    }

    public void setRunning(boolean running)
    {
        this.running = running;
    }
}
