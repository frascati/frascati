/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.explorer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.util.logging.Logger;
import java.util.logging.Level;


/**
 * Class used to find icons
 */
public class ResourceHelper
{
    private static HashMap<String, String> iconsAddress;
    private static HashMap<String, Icon> iconsCache;
    private static final Logger log = Logger.getLogger(
            ResourceHelper.class.getCanonicalName());
    static
    {
        iconsAddress = new HashMap<String, String>();
        iconsCache = new HashMap<String, Icon>();
        try
        {
            Properties iconsProperties = new Properties();
            iconsProperties.loadFromXML(findAsStream("META-INF/icons.xml"));
            Enumeration<Object> keys = iconsProperties.keys();
            while (keys.hasMoreElements())
            {
                String key = (String) keys.nextElement();
                String value = iconsProperties.getProperty(key);
                iconsAddress.put(key, value);
                iconsCache.put(key, null);
            }
        } catch (InvalidPropertiesFormatException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IOException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    public static Icon getIcon(String name)
    {
        Icon icon = iconsCache.get(name);
        if (icon == null)
        {
            InputStream iconStream = findAsStream(iconsAddress.get(name));
            if (iconStream != null)
            {
                try
                {
                    icon = new ImageIcon(ImageIO.read(iconStream));
                    
                } catch (IOException e)
                {
                    log.log(Level.WARNING,e.getMessage(),e);
                }
            }
            iconsCache.put(name, icon);
        }
        return icon;
    }

    private static InputStream findAsStream(String path)
    {
        InputStream resource = null;
        if (path == null)
        {
            return null;
        }
        ClassLoader loader = ResourceHelper.class.getClassLoader();
        resource = loader.getResourceAsStream(path);
        return resource;
    }
}
