/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.explorer;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Logger;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.objectweb.fractal.juliac.osgi.OSGiHelper;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.ow2.frascati.osgi.introspect.api.IntrospectService;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

import org.osoa.sca.annotations.Init;

/**
 * Is the FraSCAti Explorer plugin to interact with {@link WatchService} instances.
 */
@SuppressWarnings("serial")
public class IntrospectPanel extends AbstractSelectionPanel<IntrospectService>
        implements BundleEchoListener, IntrospectMenuListener
{    
    private JTextPane contextArea;
    private JTextPane messageArea;
    private JScrollPane echosContainer;
    private JPanel echos;
    private IntrospectThread thread;
    private JFileChooser fileChooser;
    private List<IntrospectPanelListener> listeners;
    private boolean threadActive;
    
    private static BundleContextRevisionItf<?,?> BUNDLE_CONTEXT;
    static
    {      
        try
        {
            BUNDLE_CONTEXT = OSGiHelper.getPlatform().getBundleContext();
            
        } catch (OSGiRevisionException e)
        {
            e.printStackTrace();
        }   
    }
    
    private static final Logger LOGGER = Logger.getLogger(
            IntrospectPanel.class.getCanonicalName());
    
    /** Constructor */
    public IntrospectPanel()
    {
        super();            
        messageArea = new JTextPane();
        messageArea.setEditable(false);
        messageArea.setBorder(BorderFactory.createTitledBorder("Messages"));
        messageArea.setEditorKit(new HTMLEditorKit());
        
        contextArea = new JTextPane();
        contextArea.setEditable(false);
        contextArea.setBorder(BorderFactory.createTitledBorder("Infos"));
        contextArea.setEditorKit(new HTMLEditorKit());
        
        JScrollPane contextPane = new JScrollPane(contextArea);
        contextPane.getVerticalScrollBar().addAdjustmentListener(
            // adjust the scrollbar to always make the first information visible
            new AdjustmentListener()
            {
                private int maximum = 0;
                public void adjustmentValueChanged(AdjustmentEvent arg0)
                {
                    JScrollBar bar = (JScrollBar) arg0.getSource();
                    if (bar.getMaximum() != maximum)
                    {
                        maximum = bar.getMaximum();
                        bar.setValue(0);
                    }
                }
            });
        JScrollPane messagePane = new JScrollPane(messageArea);
        messagePane.getVerticalScrollBar().addAdjustmentListener(
            // adjust the scrollbar to always make the last message visible
            new AdjustmentListener()
            {
                private int maximum = 0;
                public void adjustmentValueChanged(AdjustmentEvent arg0)
                {
                    JScrollBar bar = (JScrollBar) arg0.getSource();
                    if (bar.getMaximum() != maximum)
                    {
                        maximum = bar.getMaximum();
                        bar.setValue(maximum);
                    }
                }
            });
        
        JSplitPane div = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                messagePane, contextPane);
        
        echos = new JPanel();
        echos.setPreferredSize(new Dimension(500, 500));
        echos.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        IntrospectMenu menu = new IntrospectMenu();
        menu.setIntrospectMenuListener(this);
        
        echos.setComponentPopupMenu(menu);
        echosContainer = new JScrollPane(echos);
        
        JSplitPane leftRight = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                echosContainer, div);
        leftRight.setBorder(BorderFactory.createTitledBorder("OSGI Introspection Panel"));
        setLayout(new GridLayout(1, 1));
        add(leftRight);
        
        fileChooser = new JFileChooser();
        listeners = new ArrayList<IntrospectPanelListener>();
        thread = new IntrospectThread(this);
        this.threadActive = false;

        if(BUNDLE_CONTEXT != null)
        {
            init();
        }
    }

    /**
     * Try to build the tree after the IntrospectPanel instantiation
     */
    public final void init()
    {
        if(!threadActive)
        {
            thread.start();
            threadActive = true;
        }
        if (selected != null)
        {
            try
            {   
                long[] bundlez = selected.getBundles();
                BundleGraphicEcho ge;
                
                for (int u = 0; u < bundlez.length; u++)
                {
                    BundleRevisionItf<?,?> bundle = IntrospectPanel.BUNDLE_CONTEXT.getBundle(
                            bundlez[u]);
                    boolean known = false;
                    for (IntrospectPanelListener listener : listeners)
                    {
                        if ((known = listener.alreadyKnown(bundle.getBundleId())))
                        {
                            break;
                        }
                    }
                    if (!known && bundle != null)
                    {
                        echos.add((ge = new BundleGraphicEcho(bundle)));
                        ge.setBundleEchoListener(this);
                        listeners.add(ge);
                        IntrospectPanel.BUNDLE_CONTEXT.registerBundleListener(ge);
                    }
                }
                echosContainer.repaint();
            } catch (Exception e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     * Return the selected IntrospectService
     */
    public final IntrospectService getSelected()
    {
        return selected;
    }

    /**
     * Try to kill the thread if it's still alive
     */
    public void finalize()
    {
        try
        {
            thread.setRunning(false);
            thread.join();
            
        } catch (InterruptedException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * Try to install the bundle which location is passed as parameter
     * 
     * @param location
     *            the bundle jar file location
     */
    protected void install(String location)
    {
        install(new File(location));
    }

    /**
     * Try to install the bundle which location is passed as parameter
     * 
     * @param file
     *            the bundle jar file to install
     */
    protected void install(File file)
    {
        try
        {
            selected.install("file:" + file.getAbsolutePath());
            init();
        } catch (Exception e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("ERROR[");
            builder.append("bundle installation has thrown an exception: ");
            builder.append(e.getMessage());
            builder.append("]");
            writeMessage(builder.toString(), false, true);
        }
    }

    /**
     * Try to start the bundle which id is passed as parameter
     * 
     * @param id
     *            the bundle's id to start
     */
    protected void start(long id)
    {
        try
        {
            selected.startBundle(id);
        } catch (Exception e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("ERROR[");
            builder.append("bundle starting has thrown an exception: ");
            builder.append(e.getMessage());
            builder.append("]");
            writeMessage(builder.toString(), false, true);
        }
    }

    /**
     * Try to stop the bundle which id is passed as parameter
     * 
     * @param id
     *            the bundle's id to stop
     */
    protected void stop(long id)
    {
        try
        {
            selected.stopBundle(id);
        } catch (Exception e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("ERROR[");
            builder.append("bundle stopping has thrown an exception: ");
            builder.append(e.getMessage());
            builder.append("]");
            writeMessage(builder.toString(), false, true);
        }
    }

    /**
     * Try to update the bundle which id is passed as parameter
     * 
     * @param id
     *            the bundle's id to update
     */
    protected void update(long id)
    {
        try
        {
            selected.update(id);
        } catch (Exception e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("ERROR[");
            builder.append("bundle stopping has thrown an exception: ");
            builder.append(e.getMessage());
            builder.append("]");
            writeMessage(builder.toString(), false, true);
        }
    }

    /**
     * Try to uninstall the bundle which id is passed as parameter
     * 
     * @param id
     *            the bundle's id to uninstall
     */
    protected void uninstall(long id)
    {
        try
        {
            selected.uninstall(id);
        } catch (Exception e)
        {
            StringBuilder builder = new StringBuilder();
            builder.append("ERROR[");
            builder.append("bundle uninstalling has thrown an exception: ");
            builder.append(e.getMessage());
            builder.append("]");
            writeMessage(builder.toString(), false, true);
        }
    }

    /**
     * Define or Add the string message in the contextArea or in the messageArea
     * 
     * @param message
     *            the string message to show
     * @param context
     *            is the contextArea the target
     */
    public final void writeMessage(String message, boolean context,
            boolean append)
    {
        HTMLDocument document = null;
        Element element = null;
        JTextPane text = null;
        Element root = null;
        if (message == null)
        {
            return;
        }
        message = message.replaceAll("[\\n]", "\\<br\\/\\>");
        if (context)
        {
            message = message.replaceAll(
                    "(^\\s*[^:]+|\\<br\\/\\>\\s*[^:]+)\\s*:",
                    "\\<b\\>$1 :\\<\\/b\\>");
            message = message.replaceAll(",", ",\\<br\\/\\>");
            text = contextArea;
        } else
        {
            message = message
                    .replaceAll("\\[([a-zA-Z\\s]+)\\s*:",
                            "\\[\\<span style='color:#3322ff;font-style:italic;'\\>$1 :\\<\\/span\\>");
            message = message
                    .replaceAll(
                            "\\[(ServiceEvent|FrameworkEvent|BundleEvent)\\]",
                            "\\[\\<span style='color:#883322;font-weight:bold;'\\>$1\\<\\/span\\>\\]");
            message = message
                    .replaceAll("ERROR\\[([^\\]]+)\\]",
                            "\\[\\<span style='color:#992211;font-weight:bold;'\\>$1\\<\\/span\\>\\]");
            text = messageArea;
        }
        if (append)
        {
            document = (HTMLDocument) text.getDocument();
            root = document.getDefaultRootElement();
            element = root.getElementCount() > 0 ? root.getElement(root
                    .getElementCount() - 1) : root;
        } else
        {
            document = (HTMLDocument) text.getEditorKit()
                    .createDefaultDocument();
            text.setDocument(document);
            element = root = document.getDefaultRootElement();
        }
        if (element == root)
        {
            try
            {
                document.insertAfterStart(element,
                        "<div style='text-align:left;'>" + message + "</div>");
            } catch (BadLocationException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IOException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
        } else
        {
            try
            {
                document.insertAfterEnd(element,
                        "<div style='text-align:left;'>" + message + "</div>");
            } catch (BadLocationException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IOException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForStart(long)
     */
    public void bundleEchoAsksForStart(long bundleId)
    {
        start(bundleId);
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForStop(long)
     */
    public void bundleEchoAsksForStop(long bundleId)
    {
        stop(bundleId);
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForUpdate(long)
     */
    public void bundleEchoAsksForUpdate(long bundleId)
    {
        update(bundleId);
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForUninstall(long)
     */
    public void bundleEchoAsksForUninstall(long bundleId)
    {
        for (IntrospectPanelListener listener : listeners)
        {
            if ("Uninstalled".equals(listener.getState()))
            {
                // listeners.remove(listener)
                echos.remove((JPanel) listener);
            }
        }
        echos.repaint();
        uninstall(bundleId);
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForInstall(java.lang.String)
     */
    public void bundleEchoAsksForInstall(String location)
    {
        install(location);
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoAsksForDisplay(java.lang.String)
     */
    public void bundleEchoAsksForDisplay(String echo)
    {
        writeMessage(echo, true, false);
    }

    /**
     *{@inheritDoc}
     * @see
     * org.ow2.frascati.examples.bundle.osgi.introspect.explorer.IntrospectMenuListener
     * #menuAsksForInstall()
     */
    public void menuAsksForInstall()
    {
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION)
        {
            File file = fileChooser.getSelectedFile();
            install(file);
        }
    }

    /**
     *{@inheritDoc}
     * @see org.ow2.frascati.examples.bundle.osgi.introspect.explorer.BundleEchoListener#
     * bundleEchoSelected(long)
     */
    public void bundleEchoSelected(long bundleId)
    {
        for (IntrospectPanelListener listener : listeners)
        {
            listener.newSelected(bundleId);
        }
    }
}
