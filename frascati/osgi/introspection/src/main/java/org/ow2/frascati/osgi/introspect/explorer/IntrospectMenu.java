/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.Icon;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

@SuppressWarnings("serial")
public class IntrospectMenu extends JPopupMenu implements ActionListener
{
    private JMenuItem install;
    private Icon installIcon;
    private IntrospectMenuListener listener;

    public IntrospectMenu()
    {
        super();
        installIcon = ResourceHelper.getIcon("installIcon");
        install = new JMenuItem("install");
        install.setIcon(installIcon);
        install.setMnemonic(KeyEvent.VK_I);
        install.setActionCommand("installfs");
        install.addActionListener(this);
        add(install);
    }

    public void setIntrospectMenuListener(IntrospectMenuListener listener)
    {
        this.listener = listener;
    }

    public void actionPerformed(ActionEvent arg0)
    {
        if (listener != null)
        {
            listener.menuAsksForInstall();
        }
    }
}
