/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 * 
 */
package org.ow2.frascati.osgi.deployer;

import java.io.IOException;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.juliac.osgi.OSGiHelper;
import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Scope;

import java.util.logging.Logger;
import java.util.logging.Level;

@EagerInit
@Scope("COMPOSITE")
public class Deployer
{

    private static final Logger log = Logger.getLogger(
            Deployer.class.getCanonicalName());

    private static final String BUNDLE_OSGI_ENTERPRISE = "org.osgi.enterprise.jar";
    private static final String BUNDLE_OSGI_COMPENDIUM = "org.osgi.compendium.jar";
    private static final String BUNDLE_FELIX_JETTY = "org.apache.felix.http.jetty.jar";
    private static final String BUNDLE_FELIX_WEBCONSOLE = "org.apache.felix.webconsole.jar";

    @Init
    public void init()
    {

        String[] bundles = new String[] { 
                BUNDLE_OSGI_ENTERPRISE,
                BUNDLE_OSGI_COMPENDIUM
//                ,
//                BUNDLE_FELIX_JETTY,
//                BUNDLE_FELIX_WEBCONSOLE 
                };
        int n = 0;
        for (; n < bundles.length; n++)
        {
            String bundleName = bundles[n];
            try
            {
                BundleRevisionItf<?,?> bundle = OSGiHelper.installBundleIE(bundleName,
                        Thread.currentThread().getContextClassLoader());
                if (bundle != null && n > 1)
                {
                    bundle.start();
                }
            } catch (InstantiationException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
                
            } catch (OSGiRevisionException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }
}