/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Test FraSCAti running OSGi framework
 */
public abstract class OSGiInFraSCAtiTest
{

    protected static FraSCAti frascati = null;
    protected static ClassLoader loader = null;
    
    protected Object context = null;
    protected Object service = null;

    protected Class<?> contextClass = null;
    protected Class<?> bundleClass = null;
    
    private static final Logger LOGGER = Logger.getLogger(
            OSGiInFraSCAtiTest.class.getCanonicalName());
    
    @Before
    public void setUp()
    {
        System.setProperty("org.osgi.service.http.port", "54460");
        if (frascati == null)
        {
            try
            {
                frascati = FraSCAti.newFraSCAti();
                
            } catch (FrascatiException e)
            {
                e.printStackTrace();
                LOGGER.log(Level.SEVERE,e.getMessage(),e);
                fail("Unable to instantiate FraSCAti");
            }
            try
            {
                loader = frascati.getClassLoader();
                
            } catch (FrascatiException e)
            {
                LOGGER.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        try
        {
            Class<?> helper = loader.loadClass(
                    "org.objectweb.fractal.juliac.osgi.OSGiHelper");

            Class<?> platformClass = loader.loadClass(
                    "org.objectweb.fractal.juliac.osgi.PlatformItf");

            contextClass = loader.loadClass(
                    "org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf");
            
            bundleClass = loader.loadClass(
                    "org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf");
            
            Object platform = helper.getDeclaredMethod("getPlatform").invoke(null);

            context = platformClass.getDeclaredMethod("getBundleContext").invoke(platform);
            
            service = contextClass.getDeclaredMethod("getService",
                    new Class<?>[]{String.class,String.class}).invoke(context,
                    new Object[]{"org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService",null});
            
        } catch (ClassNotFoundException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        } catch (IllegalArgumentException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        } catch (SecurityException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        } catch (InvocationTargetException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        } catch (NoSuchMethodException e)
        {
            e.printStackTrace();
            LOGGER.log(Level.CONFIG,e.getMessage(),e);
            
        }      
        assertNotNull(frascati);
        assertNotNull(context);
        assertNotNull(loader);
        assertNotNull(service);
    }

    @AfterClass
    public static void tearDown()
    {
        cleanCache();
    }

    /**
     * Test binding-osgi for an exported service by FraSCAti
     * 
     * @throws BundleException
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test
    public void testFromFraSCAtiToOSGi() throws Exception
    {
        URL serverURL = loader.getResource(
                "helloworld-binding-frascati-osgi-server.jar");
        
        loadSCA(serverURL, "server.composite");
        
        InputStream is = loader.getResourceAsStream(
                "helloworld-binding-frascati-osgi-client.jar");
        
        if(is == null)
        {
            throw new NullPointerException("Unable to find 'helloworld-binding-frascati-osgi-client.jar'");
        }
        Object bundle = contextClass.getDeclaredMethod("installBundle",
                new Class<?>[]{String.class,InputStream.class}).invoke(context,
                        new Object[]{"helloworld-binding-frascati-osgi-client",is});
        
        bundleClass.getDeclaredMethod("start").invoke(bundle);
    }
    
    /**
     * Test binding-osgi for an imported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test(expected = java.lang.Exception.class)
    public void testServiceException() throws Exception
    {
        URL fractalURL = loader.getResource("helloworld-fractal.jar");
        loadSCA(fractalURL, "fake.composite");
    }
    
    /**
     * Test binding-osgi for an imported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundCompositeException
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     */
    @Test
    public void testFromOSGiToFraSCAti() throws Exception
    {
        InputStream is = loader.getResourceAsStream("helloworld-binding-osgi-frascati-server2.jar");
        if(is == null)
        {
            throw new NullPointerException("Unable to find 'helloworld-binding-osgi-frascati-server2.jar'");
        }
        Method method = contextClass.getDeclaredMethod("installBundle",
                new Class<?>[]{String.class,InputStream.class});
        
        Object bundle = method.invoke(context,
                new Object[]{"helloworld-binding-osgi-frascati-server2",is});
        
        bundleClass.getDeclaredMethod("start").invoke(bundle);
        
        URL clientURL = loader.getResource("helloworld-binding-osgi-frascati-client.jar");
        loadSCA(clientURL, "client.composite");
        launch("helloworld-binding-osgi-client");
    }
    
    /**
     * Install a bundle which contains an embedded Composite
     * to test if the tracker recognizes it and installs 
     * using the FraSCAtiOSGiService 
     */
    @Test
    public void testTracker() throws Exception
    {
        InputStream is = loader.getResourceAsStream(
                "frascati-osgi-dynamic-bundle.jar");

        if(is == null)
        {
            throw new NullPointerException("Unable to find 'frascati-osgi-dynamic-bundle.jar'");
        }

        Method method = contextClass.getDeclaredMethod("installBundle",
                new Class<?>[]{String.class,InputStream.class});
        
        Object bundle = method.invoke(context,
                        new Object[]{"frascati-osgi-dynamic-bundle",is});
        
        bundleClass.getDeclaredMethod("start").invoke(bundle);
        
        //wait for tracker process
        try
        {
            Thread.sleep(5000);
        }catch(InterruptedException e)
        {
            Thread.interrupted();
        }
        String loading = "";
        while(loading !=null )
        {
            try
            {
                Thread.sleep(100);
            }catch(InterruptedException e)
            {
                Thread.interrupted();
            }
            try
            {
                loading = (String)service.getClass().getDeclaredMethod(
                    "loading").invoke(service);
                
            } catch(Exception e)
            {
                //do nothing
            }
        }
        boolean loaded = false;
        try
        {
            loaded = (Boolean) service.getClass().getDeclaredMethod(
                    "loaded",new Class<?>[]{String.class}).invoke(service,
                            new Object[]{"helloworld-pojo"});
        } catch (Exception e)
        {
            LOGGER.log(Level.SEVERE,e.getMessage(),e);
            fail("helloworld-pojo has not been loaded");
        }
        if(loaded)
        {
            try
            {
                launch("helloworld-pojo");
            
            } catch (Exception e)
            {
                LOGGER.log(Level.SEVERE,e.getMessage(),e);
                fail("Unable to launch helloworl-pojo");
            }
        }
    }
    
    /**
     * Call the loadSCA method of the FraSCAtiOSGiService
     * 
     * @param resource
     * @param composite
     */
    protected void loadSCA(URL resource, String composite)
            throws Exception
    {
        service.getClass().getDeclaredMethod("loadSCA",
                new Class<?>[]{ URL.class, String.class }).invoke(
                service, new Object[]{ resource, composite });
    }

    /**
     * Call the launch method of the FraSCAtiOSGiService
     * 
     * @param composite
     */
    protected void launch(String composite) throws Exception
    {
        service.getClass().getDeclaredMethod("launch",
                new Class<?>[]{ String.class }).invoke(
                service, new Object[] { composite });
    }
    
    /**
     * Delete recursively all the files contained in the specified file if the
     * specified file is a directory, and the specified file itself.
     * 
     * @param f
     *            the file to delete
     */
    private static void deleteAllRecursively(File f)
    {
        if (f != null && f.exists())
        {
            cleanDirectory(f);
            f.delete();
        }
    }

    private static void cleanDirectory(File f)
    {
        if (f.isDirectory())
        {
            File[] subs = f.listFiles();
            for (int i = 0; i < subs.length; i++)
            {
                deleteAllRecursively(subs[i]);
            }
        }
    }

    /**
     * Return the system temp directory
     **/
    private static File getTmpDir()
    {
        try
        {
            File tmpFile = File.createTempFile("test", ".tmp");
            tmpFile.delete();
            File dir = tmpFile.getParentFile();
            return dir;
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * Clean cache directories
     */
    private static void cleanCache()
    {
        // clean cache directories that have been created during
        // tests
        File tmpDir = getTmpDir();
        String[] cacheDirectories = tmpDir.list(new FilenameFilter()
        {

            public boolean accept(File arg0, String arg1)
            {
                if (arg1.startsWith("jboss") || arg1.startsWith("equinox")
                        || arg1.startsWith("concierge")
                        || arg1.startsWith("knopflerfish")
                        || arg1.startsWith("felix") || arg1.startsWith("vfs")
                        || arg1.startsWith("FraSCAti_"))
                {
                    return true;
                }
                return false;
            }
        });
        for (String cacheDirectoryName : cacheDirectories)
        {
            File cacheDirectory = new File(new StringBuilder(
                    tmpDir.getAbsolutePath()).append(File.separator)
                    .append(cacheDirectoryName).toString());
            if (cacheDirectory.exists() && cacheDirectory.isDirectory())
            {
                deleteAllRecursively(cacheDirectory);
            }
        }

    }

}
