/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.tracker.test;


import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

import java.net.URI;
import java.net.URL;
import java.util.Dictionary;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.resource.OSGiResourceProcessor;
import org.ow2.frascati.osgi.resource.FraSCAtiOSGiTracker;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * FraSCAtiOSGiTracker Test Case
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ OSGiIOUtils.class })
public class FraSCAtiOSGiTrackerTest
{
    private static Logger log = Logger.getLogger(FraSCAtiOSGiTrackerTest.class.getCanonicalName());
        
    private BundleRevisionItf<?,?> bundle = null;
    private Dictionary headers;

    private FraSCAtiOSGiContext spiedContext = null;
    private FraSCAtiOSGiService spiedService = null;
    private OSGiResourceProcessor spiedProcessor = null;
    
    private BundleContextRevisionItf bundleContext = null; 
    private FraSCAtiOSGiTracker tracker = null;
    
    /**
     * Test Configuration 
     * 
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception
    {          
        PowerMockito.mockStatic(OSGiIOUtils.class);
        
        bundleContext = mock(BundleContextRevisionItf.class);
        PowerMockito.doAnswer(new Answer<Object>(){

            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                return null;
            }
            
        }).when(bundleContext).registerBundleListener(Mockito.any(BundleListenerRevisionItf.class));
        
        FraSCAtiOSGiContext context = mock(FraSCAtiOSGiContext.class);
        spiedContext = spy(context);
        when(spiedContext.getBundleContext()).thenReturn(bundleContext);
        
        FraSCAtiOSGiService service = mock(FraSCAtiOSGiService.class);
        spiedService = spy(service);
        
        headers = mock(Dictionary.class);
        bundle = mock(BundleRevisionItf.class);
        
        PowerMockito.when(OSGiIOUtils.getHeader(Mockito.eq(bundle),
               Mockito.anyString())).thenAnswer(new Answer<String[]>()
                       {

                        public String[] answer(InvocationOnMock invocation)
                                throws Throwable
                        {
                            String headerValue = (String) headers.get(invocation.getArguments()[1]);
                            if (headerValue != null)
                            {
                                String[] headerValueElements = headerValue.split(",");
                                return headerValueElements;
                            }
                            return null;
                        }
                   
                       });
        
        tracker = new FraSCAtiOSGiTracker(spiedService,spiedContext);
        
        //Mocks injection to be able to test 
        PowerMockito.field(FraSCAtiOSGiTracker.class,"context").set(tracker, spiedContext);
        PowerMockito.field(FraSCAtiOSGiTracker.class,"service").set(tracker, spiedService);
        
        OSGiResourceProcessor processor = (OSGiResourceProcessor) PowerMockito.field(
                FraSCAtiOSGiTracker.class,"processor").get(tracker);
        
        spiedProcessor = spy(processor);
        PowerMockito.field(FraSCAtiOSGiTracker.class,"processor").set(tracker, spiedProcessor);
    }

    /**
     * If the service is null the tracker does nothing
     * 
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    @Test
    public void noService() throws IllegalArgumentException, IllegalAccessException 
    {
        log.log(Level.INFO,"Test noService ... ");

        PowerMockito.field(FraSCAtiOSGiTracker.class,"service").set(tracker, null);
        
        when(bundleContext.mapBundleEvent(Mockito.anyInt())).thenReturn("RESOLVED");
        tracker.bundleChanged(bundle,0);

        Mockito.verify(spiedProcessor, Mockito.never()).bundleResolved(Mockito.any(BundleRevisionItf.class));
        Mockito.verify(spiedService, Mockito.never()).unloadSCA(Mockito.anyLong());
    }
    
    /**
     * If the BundleEvent type is neither RESOLVED nor UNINSTALLED then the tracker 
     * does nothing 
     */
    @Test
    public void unhandledBundleEventType() 
    {
        log.log(Level.INFO,"Test unhandledBundleEventType ... ");

        when(bundleContext.mapBundleEvent(Mockito.anyInt())).thenReturn("INSTALLED");
        tracker.bundleChanged(bundle,0);

        Mockito.verify(spiedProcessor, Mockito.never()).bundleResolved(Mockito.any(BundleRevisionItf.class));
        Mockito.verify(spiedService, Mockito.never()).unloadSCA(Mockito.anyLong());
    }
    
    /**
     * If the BundleEvent type is Resovled but the Frascati-Fragment header exists
     * in the Bundle then the tracker does not ask its OSGiResourceProcessor
     * to process potential embedded composite files
     */
    @Test
    public void resolvedBundleEventTypeButFrascatiFragment() 
    {
        log.log(Level.INFO,"Test resolvedBundleEventTypeButFrascatiFragment ... ");

        when(bundleContext.mapBundleEvent(Mockito.anyInt())).thenReturn("RESOLVED");
        when(headers.get(OSGiIOUtils.FRAGMENT_HEADER)).thenReturn("true");
        tracker.bundleChanged(bundle,0);

        Mockito.verify(spiedProcessor, Mockito.never()).bundleResolved(Mockito.any(BundleRevisionItf.class));
        Mockito.verify(spiedService, Mockito.never()).unloadSCA(Mockito.anyLong());
    }
    
    /**
     * If the BundleEvent type is Resovled and is not a Frascati-Fragment then the tracker
     * asks its OSGiResourceProcessor to process potential embedded composite files
     */
    @Test
    public void resolvedBundleEventTypeAndNotFrascatiFragment() 
    {
        log.log(Level.INFO,"Test resolvedBundleEventTypeAndNotFrascatiFragment ... ");

        when(bundleContext.mapBundleEvent(Mockito.anyInt())).thenReturn("RESOLVED");
        when(headers.get(OSGiIOUtils.FRAGMENT_HEADER)).thenReturn(null);
        tracker.bundleChanged(bundle,0);
        
        Mockito.verify(spiedProcessor, Mockito.times(1)).bundleResolved(bundle);
        Mockito.verify(spiedService, Mockito.never()).unloadSCA(Mockito.anyLong());
    }

    /**
     * If the BundleEvent type is UNINSTALLED the tracker asks its FraSCAtiOSGiService to 
     * unload SCA component that have potentially loaded from the bundle 
     */
    @Test
    public void uninstalledBundleEventType() 
    {
        log.log(Level.INFO,"Test uninstalledBundleEventType ... ");

        when(bundleContext.mapBundleEvent(Mockito.anyInt())).thenReturn("UNINSTALLED");
        tracker.bundleChanged(bundle,0);

        Mockito.verify(spiedProcessor, Mockito.never()).bundleResolved(bundle);
        Mockito.verify(spiedService, Mockito.times(1)).unloadSCA(Mockito.anyLong());
    }
}

