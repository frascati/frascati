/**
 * OW2 FraSCAti OSGiResourceProcessor Tests
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.processor.test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberMatcher.constructor;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Dictionary;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.resource.OSGiResourceProcessor;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * OSGiResourceProcessor Test Case
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ OSGiIOUtils.class , AbstractResource.class , URL.class , URI.class  })
public class OSGiResourceProcessorTest
{
    private static Logger log = Logger.getLogger(OSGiResourceProcessorTest.class.getCanonicalName());
    
    private static final String BUNDLE_NAME = "bundle:mockBundle.jar";
    private static final String CACHED_BUNDLE_NAME = "mockBundle.jar";
    private static final String CACHED_BUNDLE_PATH = "/" + CACHED_BUNDLE_NAME;
    private static final String BUNDLE_RESOURCE_COMPOSITE = "bundleMockComposite.composite";
    private static final String BUNDLE_RESOURCE_COMPOSITE_URL = "jar:file:/" + CACHED_BUNDLE_NAME + "!/" + BUNDLE_RESOURCE_COMPOSITE;
    
    private static final String EMBEDDED_RESOURCE_NAME = "mockEmbedded.jar";
    private static final String EMBEDDED_RESOURCE_PATH = "/" + EMBEDDED_RESOURCE_NAME;
    private static final String EMBEDDED_RESOURCE_COMPOSITE = "embeddedMockComposite.composite";   
    private static final String EMBEDDED_RESOURCE_COMPOSITE_URL = "jar:file:/" + EMBEDDED_RESOURCE_NAME + "!/" + EMBEDDED_RESOURCE_COMPOSITE;
    
    private File cached_file = null;
    private URI cached_file_uri = null;
    private URL cached_file_url = null;

    private URL embedded_file_url = null;

    private AbstractResource bundle_resource = null;
    private AbstractResource resource = null;
    private AbstractResource embedded_resource = null;

    private URL embedded_composite_url = null;
    private URL bundle_composite_url = null;

    private BundleRevisionItf<?,?> bundle = null;
    private Dictionary headers;

    private FraSCAtiOSGiService spiedService = null;
    private OSGiResourceProcessor spiedProcessor = null;

    /**
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception
    {   
        log.log(Level.INFO,"Test Configuration ... ");
        
        mockStatic(AbstractResource.class);
        mockStatic(OSGiIOUtils.class);

        cached_file = mock(File.class);
        cached_file_uri = mock(URI.class);
        cached_file_url = mock(URL.class);
        
        embedded_file_url = mock(URL.class);
        
        headers = mock(Dictionary.class);
        bundle = mock(BundleRevisionItf.class);

        FraSCAtiOSGiService service = mock(FraSCAtiOSGiService.class);
        spiedService = spy(service);
        
        OSGiResourceProcessor processor = new OSGiResourceProcessor(spiedService);
        spiedProcessor = spy(processor);
        
        //Define the mock service behavior
        when(spiedService.getCacheDir()).thenReturn(IOUtils.getTmpDir());
        
        // Define the mock bundle behavior
        when(bundle.getLocation()).thenReturn(BUNDLE_NAME);
        when(bundle.toString()).thenReturn(BUNDLE_NAME);  
        when(bundle.getSymbolicName()).thenReturn(BUNDLE_NAME); 
        when(bundle.getHeaders()).thenReturn(headers);   
        
        //Define the mock headers behavior
        when(headers.get(OSGiIOUtils.UNMANAGED_RESOURCE)).thenReturn(Collections.EMPTY_LIST);
        when(headers.get(OSGiIOUtils.COMPOSITE_TO_PROCESS)).thenReturn(Collections.EMPTY_LIST);
        
        when(OSGiIOUtils.cacheBundle(Mockito.eq(bundle),
                Mockito.any(File.class))).thenReturn(CACHED_BUNDLE_PATH);

        whenNew(constructor(File.class, new Class<?>[] { String.class }))
                .withArguments(CACHED_BUNDLE_PATH).thenReturn(cached_file);

        when(cached_file.toURI()).thenReturn(cached_file_uri);
        when(cached_file_uri.toURL()).thenReturn(cached_file_url);
        when(cached_file_url.toExternalForm()).thenReturn(CACHED_BUNDLE_PATH);
        when(cached_file_url.getFile()).thenReturn(CACHED_BUNDLE_NAME);

        when(embedded_file_url.toExternalForm()).thenReturn(EMBEDDED_RESOURCE_PATH);
        when(embedded_file_url.getFile()).thenReturn(EMBEDDED_RESOURCE_NAME);

        embedded_composite_url = new URL(EMBEDDED_RESOURCE_COMPOSITE_URL);
        
        log.log(Level.INFO,"Configure url embedded_composite_url : "+ embedded_composite_url);
        log.log(Level.INFO,"embedded_composite_url has to be a real URL to be able to test loadSCA method arguments");

        bundle_composite_url = new URL(BUNDLE_RESOURCE_COMPOSITE_URL);
        
        log.log(Level.INFO,"Configure mock url bundle_composite_url : "+ bundle_composite_url);
        log.log(Level.INFO,"bundle_composite_url has to be a real URL to be able to test loadSCA method arguments");
        
        when(AbstractResource.class,method(AbstractResource.class,"newResource",
                new Class<?>[]{AbstractResource.class,Object.class})).withArguments(
                        Mockito.any(), Mockito.any()).thenAnswer(
                new Answer(){
                            public Object answer(InvocationOnMock invocation)
                                    throws Throwable
                            {
                                if(invocation.getArguments()[1] instanceof BundleRevisionItf)
                                {
                                    log.log(Level.INFO,"AbstractResource.newResource(" + 
                                            invocation.getArguments()[1] +") : "+ bundle_resource );
                                    
                                    return bundle_resource;
                                    
                                } else if(invocation.getArguments()[1] instanceof URL)
                                {
                                    log.log(Level.INFO,"AbstractResource.newResource(" + 
                                            invocation.getArguments()[1] +") : "+ resource );
                                    return resource;
                                }

                                log.log(Level.INFO,"AbstractResource.newResource(" + 
                                        invocation.getArguments()[1] +") : NULL " );
                                return null;
                            }
                            
                        });
    }
    
    /**
     * If the FraSCAtiOSGiService is null the process is ended
     * 
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    @Test
    public void noService() throws SecurityException,
            NoSuchMethodException
    {
        log.log(Level.INFO,"Test noService ... ");
        OSGiResourceProcessor processor = new OSGiResourceProcessor(null);
        spiedProcessor = spy(processor); 
        spiedProcessor.bundleResolved(bundle);

        verify(spiedProcessor, never()).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, never()).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
    }
    
    /**
     * If no AbstractResource object can be created using the bundle, 
     * the process is ended
     * 
     * @throws NoSuchMethodException
     * @throws SecurityException
     */
    @Test
    public void unableToCreateAbstractResource() throws SecurityException,
            NoSuchMethodException
    {
        log.log(Level.INFO,"Test unableToCreateAbstractResource ... ");
        bundle_resource = null;
        spiedProcessor.bundleResolved(bundle);

        verify(spiedProcessor, never()).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, never()).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
    }

    /**
     * If an AbstractResource object is created, but containing no composite
     * file and no jar file then the processResource and the getEmededdedComposites 
     * methods are called only once before the end of the process
     * 
     * @throws Exception
     */
    @Test
    public void noCompositeToProcessNoEmbeddedJar() throws Exception
    {
        log.log(Level.INFO,"Test noCompositeToProcessNoEmbeddedJar ... ");
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null,(String[])null,(URL[])null);
        
        spiedProcessor.bundleResolved(bundle);

        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
    }
    
    /**
     * If the bundle contains one jar file but none of those contains a composite file
     * then the processResource and the getEmbeddedComposites methods are called for each 
     * AbstractResource before the end of the process
     * 
     * @throws Exception
     */
    @Test
    public void noCompositeToProcessOneEmbeddedJar() throws Exception
    {
        log.log(Level.INFO,"Test noCompositeToProcessOneEmbeddedJar ... ");
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null,new String[]{EMBEDDED_RESOURCE_NAME},(URL[])null);

        embedded_resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                null,(String[])null,(URL[])null);
        
        when(bundle_resource.cacheEmbeddedResource(Mockito.anyString(), 
                Mockito.anyString())).thenReturn(embedded_resource);
        
        spiedProcessor.bundleResolved(bundle);
        
        //Verify that the getEmbeddedComposites and processResource methods have been
        //called once for each AbstractResource
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(embedded_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(embedded_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and processResource methods have been
        //called twice only
        verify(spiedProcessor, Mockito.times(2)).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, Mockito.times(2)).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
    }

    /**
     * If the bundle contains a composite and a jar file then the getEmbeddedComposites
     * method is called once for the bundle_resource AbstractResource and the processResource 
     * method is called once for the resource AbstractResource. The embedded_resource
     * AbstractResource is never called
     * 
     * @throws Exception
     */
    @Test
    public void oneCompositeToProcessInBundleAndOneEmbeddedJar() throws Exception
    {
        log.log(Level.INFO,"Test oneCompositeToProcessInBundleAndOneEmbeddedJar ... ");
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null,new String[]{EMBEDDED_RESOURCE_NAME},new URL[]{bundle_composite_url});

        resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                cached_file_url, (String[])null,(URL[])null);

        embedded_resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                null,(String[])null,(URL[])null);

        when(resource.cacheEmbeddedResource(Mockito.anyString(), 
                Mockito.anyString())).thenReturn(embedded_resource);
        
        spiedProcessor.bundleResolved(bundle);

        //Verify that the getEmbeddedComposites method has been called once for the bundle_resource
        //but never the processResource method
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, never()).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the processResource method has been called once for the resource
        //but never the getEmbeddedComposites method
        verify(spiedProcessor, never()).getEmbeddedComposites(resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that neither the getEmbeddedComposites nor the processResource method have
        //have been called for the embedded_resource
        verify(spiedProcessor, never()).getEmbeddedComposites(embedded_resource);
        verify(spiedProcessor, never()).processResource(Mockito.eq(embedded_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and processResource method have been
        //called once only
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //verify that the loadSCA method of the service has been called once
        verify(spiedService).loadSCA(Mockito.any(URL.class),Mockito.anyString());        
    }
    
    /**
     * If the bundle contains a jar file then the getEmbeddedComposites and the processResource
     * methods are called for both bundle_resource AbstractResource and embedded_resource
     * AbstractResource. The resource AbstractResource is never called
     * 
     * @throws Exception
     */
    @Test
    public void oneCompositeInAnEmbeddedJar() throws Exception
    {
        log.log(Level.INFO,"Test oneCompositeInAnEmbeddedJar ... ");
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null, new String[]{ EMBEDDED_RESOURCE_NAME },(URL[])null);

        resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                cached_file_url, new String[]{ EMBEDDED_RESOURCE_NAME },(URL[])null);
        
        embedded_resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                null,(String[])null,new URL[]{embedded_composite_url});
        
        when(bundle_resource.cacheEmbeddedResource(Mockito.anyString(), 
                Mockito.anyString())).thenReturn(embedded_resource);
        
        spiedProcessor.bundleResolved(bundle);

        //Verify that the getEmbeddedComposites and the processResource methods have been called once 
        //for the bundle_resource
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and the processResource methods for the resource
        //have never been called
        verify(spiedProcessor, never()).getEmbeddedComposites(resource);
        verify(spiedProcessor, never()).processResource(Mockito.eq(resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and the processResource methods have been called once 
        //for the embedded_resource
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(embedded_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(embedded_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and processResource method have been
        //called twice
        verify(spiedProcessor, Mockito.times(2)).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, Mockito.times(2)).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());

        //verify that the loadSCA method of the service has been called once
        verify(spiedService).loadSCA(Mockito.any(URL.class),Mockito.anyString());        
    }
    
    /**
     * If the bundle contains a jar file and the both of them contain a composite file then
     * the getEmbeddedComposites method is called for the bundle_resource AbstractResource and the
     * embedded_resource AbstractResource. The processResource method is called for 
     * the resource AbstractResource and the embedded_resource AbstractResource
     * 
     * @throws Exception
     */
    @Test
    public void oneCompositeInBundleAndOneInEmbeddedJar() throws Exception
    {
        log.log(Level.INFO,"Test oneCompositeInBundleAndOneInEmbeddedJar ... ");
        
        ArrayList<String> toProcess = new ArrayList<String>();
        toProcess.addAll(Arrays.asList(new String[]{EMBEDDED_RESOURCE_COMPOSITE, BUNDLE_RESOURCE_COMPOSITE}));
        
        when(OSGiIOUtils.getCompositesToProcess(bundle)).thenReturn(toProcess);
        
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null, new String[]{ EMBEDDED_RESOURCE_NAME },new URL[]{bundle_composite_url});

        resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                cached_file_url, new String[]{ EMBEDDED_RESOURCE_NAME } , new URL[]{bundle_composite_url} );
        
        embedded_resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                embedded_file_url,(String[])null,new URL[]{embedded_composite_url});
        
        when(resource.cacheEmbeddedResource(Mockito.anyString(), 
                Mockito.anyString())).thenReturn(embedded_resource);
        
        spiedProcessor.bundleResolved(bundle);

        //Verify that the getEmbeddedComposites method has been called once for the bundle_resource
        //but never the processResource method
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, Mockito.never()).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the processResource method has been called once for the resource
        //but never the getEmbeddedComposites method
        verify(spiedProcessor, never()).getEmbeddedComposites(resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and the processResource methods have
        //have been called once for the embedded_resource
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(embedded_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(embedded_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and processResource method have been
        //called twice
        verify(spiedProcessor, Mockito.times(2)).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, Mockito.times(2)).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());

        //verify that the loadSCA method of the service has been called twice
        verify(spiedService,Mockito.times(1)).loadSCA(cached_file_url,BUNDLE_RESOURCE_COMPOSITE);
        verify(spiedService,Mockito.times(1)).loadSCA(embedded_file_url,EMBEDDED_RESOURCE_COMPOSITE);      
    }
    
    /**
     * The bundle contains a jar file and a composite file but two composite files' names are defined
     * in the bundle headers. The getEmbeddedComposites method is called for the bundle_resource and the
     * embedded_resource. The processResource method is called for the resource and the embedded_resource. 
     * Finally a message alerts that all searched composite files have not been found  
     * 
     * @throws Exception
     */
    @Test
    public void oneCompositeInBundleButTwoDefinedInHeaders() throws Exception
    {
        log.log(Level.INFO,"Test oneCompositeInBundleAndOneInEmbeddedJar ... ");
        
        ArrayList<String> toProcess = new ArrayList<String>();
        toProcess.addAll(Arrays.asList(new String[]{EMBEDDED_RESOURCE_COMPOSITE, BUNDLE_RESOURCE_COMPOSITE}));
        
        when(OSGiIOUtils.getCompositesToProcess(bundle)).thenReturn(toProcess);
        
        bundle_resource = configureResource(org.ow2.frascati.util.resource.bundle.Resource.class,
                null, new String[]{ EMBEDDED_RESOURCE_NAME },new URL[]{bundle_composite_url});

        resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                cached_file_url, new String[]{ EMBEDDED_RESOURCE_NAME } , new URL[]{bundle_composite_url} );
        
        embedded_resource = configureResource(org.ow2.frascati.util.resource.jar.Resource.class,
                embedded_file_url,(String[])null,(URL[]) null);
        
        when(resource.cacheEmbeddedResource(Mockito.anyString(), 
                Mockito.anyString())).thenReturn(embedded_resource);
        
        spiedProcessor.bundleResolved(bundle);

        //Verify that the getEmbeddedComposites method has been called once for the bundle_resource
        //but never the processResource method
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(bundle_resource);
        verify(spiedProcessor, Mockito.never()).processResource(Mockito.eq(bundle_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the processResource method has been called once for the resource
        //but never the getEmbeddedComposites method
        verify(spiedProcessor, never()).getEmbeddedComposites(resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and the processResource methods have
        //have been called once for the embedded_resource
        verify(spiedProcessor, Mockito.times(1)).getEmbeddedComposites(embedded_resource);
        verify(spiedProcessor, Mockito.times(1)).processResource(Mockito.eq(embedded_resource),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());
        
        //Verify that the getEmbeddedComposites and processResource method have been
        //called twice
        verify(spiedProcessor, Mockito.times(2)).getEmbeddedComposites(Mockito.any(AbstractResource.class));
        verify(spiedProcessor, Mockito.times(2)).processResource(Mockito.any(AbstractResource.class),
                Mockito.anyList(), Mockito.any(ArrayList.class), Mockito.anyList());

        //verify that the loadSCA method of the service has been called once
        verify(spiedService,Mockito.times(1)).loadSCA(cached_file_url,BUNDLE_RESOURCE_COMPOSITE);        
    }
     /**
     * Define an AbstractResource mock, its type and behavior
     * 
     * @param <T>
     * @param clazz
     *          the AbstractResource inherited class
     * @param resourceURL
     *          the value of the AbstractResource's resourceURL attribute
     * @param embeddedJars
     *          an array of embedded jar files' name
     * @param embeddedComposites
     *          an array of embedded composites' URL
     * @return
     *          a new AbstractResource
     * @throws Exception
     */
    private <T extends AbstractResource> T configureResource(Class<T> clazz,
           URL resourceURL, final String[] embeddedJars,final URL[] embeddedComposites) throws Exception
    {        
        final T resource = Mockito.mock(clazz);
        
        PowerMockito.doAnswer(new Answer(){
            public Object answer(InvocationOnMock invocation) throws Throwable
            {
                return null;
            }            
        }).when(resource).buildEntries();
        
        when(resource,"getRegExpResources",
                Mockito.eq(".+\\.composite$"), Mockito.anyList()).thenAnswer(
                                new Answer()
                                {
                                   public Object answer(InvocationOnMock invocation)
                                           throws Throwable
                                   {
                                       List<URL> list = (List<URL>) invocation.getArguments()[1];
                                       if(list != null && embeddedComposites != null)
                                       {
                                           for(int i = 0;i<embeddedComposites.length;i++)
                                           {
                                               log.log(Level.INFO,"Composite file added : "+ embeddedComposites[i] );
                                               list.add(embeddedComposites[i]);
                                           }
                                       }
                                       return null;
                                   }                                    
                                });     
                
        PowerMockito.field(AbstractResource.class,"embeddedJars").set(
                resource,embeddedJars == null ?
                Collections.EMPTY_LIST : Arrays.asList(embeddedJars));
        
        when(resource.toString()).thenReturn(clazz + "[" + resource.hashCode() + "]");
        when(resource.getResourceURL()).thenReturn(resourceURL);

        log.log(Level.INFO,"Define AbstractResource : "+ resource );
        return resource;
    }
}
