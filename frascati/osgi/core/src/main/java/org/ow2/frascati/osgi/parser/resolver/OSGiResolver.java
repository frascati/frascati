/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.parser.resolver;

import java.util.Iterator;
import java.util.logging.Level;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.domainmodel.frascati.OsgiImplementation;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.osgi.OSGiBinding;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.resolver.AbstractResolver;

/**
 * Resolver used in the composite processing process before the 
 * resolution of the used classes to prepare osgi binding or exporting 
 */
@Scope("COMPOSITE")
public class OSGiResolver extends AbstractResolver<DocumentRoot>
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * Needed reference to the FraSCAtiOSGiContext instance
     */
    @Reference(name = "frascati-osgi-context")
    private FraSCAtiOSGiContext foContext;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
  
    /**
     * Resolve osgi binding.
     * 
     * @param osgiBinding
     *          the osgiBinding being processed 
     * @param parsingContext
     *          the processing process's ParsingContext
     */
    private final void resolveOSGiBinding(OSGiBinding osgiBinding,
            ParsingContext parsingContext)
    {
        log.log(Level.CONFIG, "resolveOSGi");
        
        String filter = osgiBinding.getFilter();
        String properties = osgiBinding.getProperties();
        String interfaceName = "";
        
        boolean binded = false;
        
        org.eclipse.stp.sca.JavaInterface jInterface = null;
        org.eclipse.emf.ecore.util.FeatureMap map = null;
        
        if (osgiBinding.eContainer() instanceof ComponentReference)
        {
            ComponentReference ref = (ComponentReference) osgiBinding
                    .eContainer();
            map = ref.getInterfaceGroup();
            binded = true;
        } else if (osgiBinding.eContainer() instanceof ComponentService)
        {
            ComponentService ser = (ComponentService) osgiBinding.eContainer();
            map = ser.getInterfaceGroup();
        } else
        {
            log.log(Level.WARNING, "Error while trying to find the interface name : "
              + "Unable to define if binding refers to Service or a Reference");
        }
        if (map != null)
        {
            Iterator<org.eclipse.emf.ecore.util.FeatureMap.Entry> it = map.iterator();
            while (it.hasNext())
            {
                org.eclipse.emf.ecore.util.FeatureMap.Entry entry = it.next();
                if (entry.getValue() instanceof org.eclipse.stp.sca.JavaInterface)
                {
                    jInterface = (org.eclipse.stp.sca.JavaInterface) entry
                            .getValue();
                    break;
                }
            }
        }
        if (jInterface == null)
        {
            log.log(Level.WARNING,
                    "Error while trying to find the Interface name");
        } else
        {
            interfaceName = jInterface.getInterface();
            if (binded)
            {
                log.log(Level.INFO, "Binded interface name : " + interfaceName);
                foContext.registerBindedInterface(interfaceName, filter,
                        parsingContext);
            } else
            {
                log.log(Level.INFO, "Exported interface name : " + interfaceName);
                foContext.registerExportedInterface(interfaceName, properties,
                        parsingContext);
            }
        }
    }

    /**
     * The osgi-revision-api library is needed by Juliac for compiling generated membranes.
     * The registerOSGiImplementation called method retrieve the library's URL and give it 
     * to Juliac 
     * @param eObject 
     *    
     * @param parsingContext
     *          The parsing context used by the processing process
     */
    private void resolveOSGiImplementation(OsgiImplementation osgiImplementation,
            ParsingContext parsingContext)
    {
        String bundleName = osgiImplementation.getBundle();
        foContext.registerOSGiImplementation(bundleName,parsingContext);
    }
    
    /**
     * Identify Binding(s) and Implementation(s) in the EObject passed on as parameter. 
     * If an OSGiBinding is found the resolveOSGiBinding method is called for it, or if an 
     * OsgiImplementation is found the resolveOSGiImplementation method is called for it
     *  
     * @param eobject
     *          the EObject to explore
     * @param parsingContext
     *          The parsing context used by the processing process
     */
    private void searchForOSGi(EObject eobject, ParsingContext parsingContext)
    {
        EList<EObject> eobjects = eobject.eContents();
        
        for (EObject subEObject : eobjects)
        {
            if (subEObject instanceof OSGiBinding)
            {
                resolveOSGiBinding((OSGiBinding) subEObject, parsingContext);
                
            } else if(subEObject instanceof OsgiImplementation)
            {
                resolveOSGiImplementation((OsgiImplementation) subEObject,
                        parsingContext);
                
            } else
            {
                searchForOSGi(subEObject, parsingContext);
            }
        }
    }
    

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    @Init
    public void init()
    {
        log.log(Level.CONFIG, "init OSGi resolver");
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.resolver.AbstractResolver#doResolve(java.lang.Object,
     *      org.ow2.frascati.parser.api.ParsingContext)
     */
    @Override
    protected DocumentRoot doResolve(DocumentRoot element,
            ParsingContext parsingContext)
    {
        searchForOSGi(element, parsingContext);
        return element;
    }
}
