/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.context;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry;
import org.ow2.frascati.util.enumeration.Bag;

/**
 * Implementation of the FraSCAtiOSGiRegitry interface
 */
@Scope("COMPOSITE")
public class FraSCAtiOSGiRegistryImpl implements FraSCAtiOSGiRegistry
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * Logger.
     */
    private static Logger log = Logger.getLogger(
            FraSCAtiOSGiRegistryImpl.class.getCanonicalName());
    
    // maps
    private HashMap<Long, Map<String, Component>> composites = 
        new HashMap<Long, Map<String, Component>>();
    private HashMap<String, Map<String, Class<?>>> services = 
        new HashMap<String, Map<String, Class<?>>>();

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @param id
     *            The bundle identifier
     * @return an array of classes
     */
    protected Class<?>[] getServices(long id)
    {
        List<String> compositeNames = new ArrayList<String>();
        List<Class<?>> serviceClasses = new ArrayList<Class<?>>();
        Map<String, Component> registeredComposites = composites.get(new Long(
                id));
        compositeNames.addAll(registeredComposites.keySet());
        int n = 0;
        for (; n < compositeNames.size(); n++)
        {
            String compositeName = (String) compositeNames.get(n);
            Map<String, Class<?>> registeredServices = services
                    .get(compositeName);
            serviceClasses.addAll(registeredServices.values());
        }
        return serviceClasses.toArray(new Class<?>[0]);
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    @Init
    public void init()
    {
        log.log(Level.CONFIG, "init FraSCAtiOSGiRegistry");
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry #register(long,
     *      org.objectweb.fractal.api.Component,
     *      org.ow2.frascati.assembly.factory.api.ProcessingContext)
     */
    public String register(long bundleId, Component composite,
            ClassLoader classLoader)
    {
        if (composite == null)
        {
            log.log(Level.WARNING, "Unable to register service(s) for "
                    + "a null Composite");
            return null;
        }
        Object[] interfaces = composite.getFcInterfaces();
        String compositeName = null;
        try
        {
            compositeName = ((NameController) composite
                    .getFcInterface("name-controller")).getFcName();
        } catch (NoSuchInterfaceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        Map<String, Class<?>> map = new HashMap<String, Class<?>>();
        for (Object o : interfaces)
        {
            Interface i = (Interface) o;
            String serviceName = i.getFcItfName();
            
            InterfaceType serviceType = (InterfaceType) i.getFcItfType();
            Class<?> serviceClass = null;            
            try
            {
                serviceClass = classLoader.loadClass(serviceType.getFcItfSignature());
                
            } catch (ClassNotFoundException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
            if (serviceClass != null)
            {
                map.put(serviceName, serviceClass);
                log.log(Level.CONFIG, "register(" + serviceName + ","
                        + serviceClass + ")");
            }
        }
        boolean newSubMap = false;
        Map<String, Component> compositeSubMap = composites.get(new Long(
                bundleId));
        if (compositeSubMap == null)
        {
            compositeSubMap = new HashMap<String, Component>();
            newSubMap = true;
        }
        compositeSubMap.put(compositeName, composite);
        if (newSubMap)
        {
            composites.put(new Long(bundleId), compositeSubMap);
        }
        services.put(compositeName, map);
        log.log(Level.INFO, "Registry has been updated");
        return compositeName;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry #unregister(long)
     */
    public String[] unregister(long id)
    {

        String[] compositeNames = null;
        Map<String, Component> registeredComposites = composites.remove(
                new Long(id));
        if (registeredComposites != null)
        {
            compositeNames = (String[]) registeredComposites.keySet().toArray(
                    new String[0]);
            int n = 0;
            for (; n < compositeNames.length; n++)
            {
                String compositeName = compositeNames[n];
                services.remove(compositeName);
            }
        }
        return compositeNames;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry #unregisterAll()
     */
    public String[] unregisterAll()
    {
        Bag<String> compositeNames = new Bag<String>()
        {
            @Override
            public String[] EArray(int size)
            {
                return (String[]) Array.newInstance(String.class, size);
            }
        };
        Iterator<Long> it = composites.keySet().iterator();
        while (it.hasNext())
        {
            Long bundleId = (Long) it.next();
            compositeNames.addArray(unregister(bundleId));
        }
        return compositeNames.toArray();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry#getComposite(java.lang.String)
     */
    public Component getComposite(String compositeName)
    {
        Component composite = null;
        Set<Long> keys = composites.keySet();
        Iterator<Long> keyIterator = keys.iterator();
        while (keyIterator.hasNext())
        {
            Map<String, Component> registeredComposites = composites
                    .get(keyIterator.next());
            Set<String> names = registeredComposites.keySet();
            Iterator<String> nameIterator = names.iterator();
            while (nameIterator.hasNext())
            {
                if (nameIterator.next().equals(compositeName))
                {
                    composite = (Component) registeredComposites
                            .get(compositeName);
                    break;
                }
            }
            if (composite != null)
            {
                break;
            }
        }
        return composite;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry#getComposites(long)
     */
    public String[] getComposites(long id)
    {
        Map<String, Component> registeredComposites = composites.get(
                new Long(id));
        
        if (registeredComposites == null)
        {
            return new String[0];
        }
        return registeredComposites.keySet().toArray(new String[0]);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry
     *      #getServices(java.lang.String)
     */
    public Map<String, Class<?>> getServices(String compositeName)
    {
        return services.get(compositeName);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry
     *      #getService(java.lang.String, java.lang.String)
     */
    public Class<?> getService(String compositeName, String serviceName)
    {
        Class<?> serviceClass = null;
        
        Map<String, Class<?>> registeredServices = services.get(
                compositeName);
        
        if (registeredServices != null)
        {
            serviceClass = (Class<?>) registeredServices.get(serviceName);
        }
        return serviceClass;
    }
}
