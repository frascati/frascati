/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.api;

import java.io.File;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.ow2.frascati.osgi.resource.AbstractResourceClassLoader;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * The FraSCAtiOSGiContext interface
 */
public interface FraSCAtiOSGiContext
{
    /**
     * Return the BundleContext associated to the FraSCAtiOSGiContext instance
     */
    BundleContextRevisionItf<?,?> getBundleContext();

    /**
     * Resolve classes before fractal membranes creation process for a binded interface
     * from the OSGi context
     */
    void registerBindedInterface(String interfaceName, String filter,
          ParsingContext parsingContext);

    /**
     * Resolve classes before fractal membranes creation process for an exported interface
     * towards the OSGi context
     */
    void registerExportedInterface(String interfaceName, String properties,
          ParsingContext parsingContext);

    /**
     * Resolve classes before fractal membranes creation process for an implementation.osgi
     */
    void registerOSGiImplementation(String bundleName,
            ParsingContext parsingContext);
    
    /**
     * Shortcut to get the FraSCAti instance's ClassLoader
     */
    ClassLoader getClassLoader(); 
    
    /**
     * Returns the class loader Manager of the FraSCAtiOSGiContext instance
     */
    AbstractResourceClassLoader getClManager();

    /**
     * Returns the cache directory of the FraSCAtiOSGiContext instance
     */
    File getCacheDir();

}
