/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.api;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.osoa.sca.annotations.Service;

/**
 * The FraSCAtiOSGiRegistry is used to memorized associations between loaded bundles in
 * the OSGi context and SCA promoted services in the FraSCAti context
 */
@Service
public interface FraSCAtiOSGiRegistry
{
    /**
     * Return a Class object of a service associated to a composite previously registered
     * 
     * @param compositeName
     *            The name of the composite containing the service
     * @param serviceName
     *            The service name
     * @return The Class object of the service
     */
    Class<?> getService(String compositeName, String serviceName);

    /**
     * Return a map of services'name and class associated to a composite
     * 
     * @param String
     *            The name of the composite
     * @return The map of names and classes
     */
    Map<String, Class<?>> getServices(String compositeName);

    /**
     * Return the composite object associated with the name passed as parameter if it
     * exists ; null otherwise
     * 
     * @param compositeName
     *            the name of the searched composite
     * @return the composite object
     */
    Component getComposite(String compositeName);

    /**
     * Return the list of composites' name associated to a bundle identifier
     * 
     * @param id
     *            the bundle identifier
     * @return an array of strings
     */
    String[] getComposites(long id);

    /**
     * Register a new composite and all its promoted services associated to a Bundle which
     * identifier is passed on as parameter
     * 
     * @param bundleId
     * @param composite
     * @param classLoader
     */
    String register(long bundleId, Component composite, ClassLoader classLoader);

    /**
     * Removes composites and all associated services that have been registered with the
     * bundle identifier passed as parameter
     * 
     * @param id
     *            the bundle identifier
     * @return an array of the name of the composites that have been unregistered
     */
    String[] unregister(long bundleId);

    /**
     * Removes composites and all associated services that have been registered for all
     * bundles identifier
     * 
     * @return an array of the name of the composites that have been unregistered
     */
    String[] unregisterAll();
}
