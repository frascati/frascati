/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.security;

import java.security.Permission;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;

/**
 * TODO: improve security for FraSCAti in OSGi
 * 
 * A really basic SecurityManager - No security check
 * 
 * @see java.lang.SecurityManager
 */
public class FraSCAtiOSGiSecurityManager extends SecurityManager
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * System's SecurityManager
     */
    private final SecurityManager systemSM;
    /**
     * The FraSCAtiOSGiContext using the current FraSCAtiOSGiSecurityManager
     * instance
     */
    private final FraSCAtiOSGiContext foContext;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * Constructor
     * 
     * @param systemSM
     *            the system's SecurityManager
     * @param foContext
     *            the FraSCAtiOSGiContext using the current
     *            FraSCAtiOSGiSecurityManager instance
     */
    public FraSCAtiOSGiSecurityManager(SecurityManager systemSM,
            FraSCAtiOSGiContext foContext)
    {
        this.systemSM = systemSM;
        this.foContext = foContext;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.SecurityManager#checkPermission(java.security.Permission)
     */
    @Override
    public void checkPermission(Permission perm)
    {
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.SecurityManager#checkPermission(java.security.Permission,
     *      java.lang.Object)
     */
    @Override
    public void checkPermission(Permission perm, Object context)
    {
    }
}
