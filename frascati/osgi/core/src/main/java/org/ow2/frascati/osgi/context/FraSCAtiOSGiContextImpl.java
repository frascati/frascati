/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.osgi.context;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.OSGiHelper;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;

import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.osgi.resource.AbstractResourceClassLoader;
import org.ow2.frascati.osgi.security.FraSCAtiOSGiSecurityManager;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;

import org.ow2.frascati.parser.api.ParsingContext;

import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.ResourceAlreadyManagedException;

/**
 * Implementation of the FraSCAtiOSGiContext interface
 */
@Scope("COMPOSITE")
public class FraSCAtiOSGiContextImpl implements FraSCAtiOSGiContext
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * The BundleContext associated to the FraSCAti instance
     */
    private BundleContextRevisionItf<?,?> bundleContext;
    
    /**
     * The cache directory
     */
    private File cache;
    
    /**
     * The logger
     */
    private final Logger LOGGER = Logger.getLogger(            
            FraSCAtiOSGiContextImpl.class.getCanonicalName());
    
    /**
     * the AbstractResourceClassLoader manager
     */
    private AbstractResourceClassLoader clManager;

    /**
     * The required classloader manager.
     */
    @Reference(name = "frascati-loader")
    private ClassLoaderManager classLoaderManager; 
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Initialize and Configure a basic SecurityManager - A security manager is
     * needed to use rmi binding
     */
    private void configureSecurityManager()
    {
        LOGGER.log(Level.CONFIG, "init FraSCAtiOSGiSecurityManager");
        FraSCAtiOSGiSecurityManager frascatiSM = new FraSCAtiOSGiSecurityManager(
                System.getSecurityManager(), this);
        try
        {
            URL policyURL = getClass().getClassLoader().getResource("frascati.policy");
            if (policyURL != null)
            {
                String policy = policyURL.toExternalForm();
                System.setProperty("java.security.policy", policy);
                java.security.Policy.getPolicy().refresh();
            }
        } catch (Exception e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        System.setSecurityManager(frascatiSM);
    }
    
    /**
     * The sca syntax is not the same has the osgi one : '!', '|', and '&' are
     * replaced by 'not', 'or', and 'and' respectively. This method makes
     * changes to create an osgi(ldap) compliant filter
     * 
     * @param filter
     *          The filter to transform
     * @return 
     *          The osgi compliant filter
     */
    private String normalizeFilter(String filter)
    {
        if (filter != null)
        {
            String normalizedFilter = filter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])and([^a-zA-Z\\-\\.\\_])", "$1&$2");
            normalizedFilter = normalizedFilter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])or([^a-zA-Z\\-\\.\\_])", "$1|$2");
            normalizedFilter = normalizedFilter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])not([^a-zA-Z\\-\\.\\_])", "$1!$2");
            return normalizedFilter;
        }
        return null;
    }
    
    /**
     * Return the list 
     * 
     * @param cloader
     *          The ClassLoader use to calculate list of packages dependencies
     * @param packageName
     *          The package name 
     * @return
     *          
     */
    private String packageDependencies(URLClassLoader cloader,
            String packageName, String interfaceName)
    {        
        List<URL> urls = new ArrayList<URL>();
        
        //prepare a ClassLoader using the juliac compiler way 
        ClassLoader currentClassLoader = cloader;        
        while(currentClassLoader!= null && URLClassLoader.class.isAssignableFrom(
                   currentClassLoader.getClass()))
        {
            urls.addAll(Arrays.asList(((URLClassLoader) currentClassLoader).getURLs()));
            currentClassLoader = currentClassLoader.getParent();
        }
        URLClassLoader tmpClassLoader = new URLClassLoader(
            urls.toArray(new URL[urls.size()]), 
            ClassLoader.getSystemClassLoader());

        StringBuilder dynamicInterfacePackageName = new StringBuilder();
        try
        {
            Class<?> interfaceClass = tmpClassLoader.loadClass(interfaceName);
            Class<?>[] classes = interfaceClass.getDeclaredClasses();
            for(Class<?> clazz : classes)
            {              
                dynamicInterfacePackageName.append(
                        clazz.getPackage().getName()).append(",");
            }
            Annotation[] annotations = interfaceClass.getAnnotations();
            for(Annotation annotation : annotations)
            {
                dynamicInterfacePackageName.append(
                    annotation.annotationType().getPackage().getName()
                    ).append(",");
            }
        } catch (Exception e)
        {
            if(LOGGER.isLoggable(Level.CONFIG))
            {
                LOGGER.log(Level.CONFIG, e.getMessage(), e);
            }
        }       
        dynamicInterfacePackageName.append("*");
        return dynamicInterfacePackageName.toString();
    }
    
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * FraSCAtiOSGiContext instance initialization
     */
    @Init
    public void init()
    {
        if(LOGGER.isLoggable(Level.CONFIG))
        {
            LOGGER.log(Level.CONFIG, "init FraSCAtiOSGiContext");
        }        
        getCacheDir();
        configureSecurityManager();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiInterfaceBinder
     *      #registerBindedInterface(java.lang.String, java.lang.String)
     */
    public void registerBindedInterface(String interfaceName, String filter,
            ParsingContext parsingContext)
    {
        long launchDuration = -1;
        long startTime = new Date().getTime();
        try
        {   
            String normalizedFilter = normalizeFilter(filter);            
            BundleRevisionItf<?,?> bundle = bundleContext.getServiceBundle(
                        interfaceName, normalizedFilter);
            
            if(bundle != null)
            {
                Class<?> interfaceClass = bundle.loadClass(interfaceName);
                //needed by JDTCompiler
                String jarFilePath = OSGiIOUtils.cacheBundle(bundle,getCacheDir());  
                if(LOGGER.isLoggable(Level.INFO))
                {
                    LOGGER.log(Level.INFO, "bundle cached : " + jarFilePath);
                }
                try
                {
                    URL jarURL = new File(jarFilePath).toURI().toURL();
                    ((FrascatiClassLoader)parsingContext.getClassLoader()).addUrl(jarURL);
                    getClManager().addLoaded(interfaceName, interfaceClass);
                    
                } catch (MalformedURLException e)
                {
                    if(LOGGER.isLoggable(Level.WARNING))
                    {
                        LOGGER.log(Level.WARNING, e.getMessage(), e);
                    }
                } catch(Exception e)
                {
                    if(LOGGER.isLoggable(Level.WARNING))
                    {
                        LOGGER.log(Level.WARNING, interfaceName + " not found in the BundleContext");
                    }
                }
            }
        } catch (ClassNotFoundException e)
        {
            if(LOGGER.isLoggable(Level.WARNING))
            {
                LOGGER.log(Level.WARNING,e.getMessage(), e);
            }
        } finally
        {
            launchDuration = new Date().getTime() - startTime;
            LOGGER.log(Level.INFO, "Binded interface registration '" + 
                    interfaceName + "' : " + launchDuration + " ms");
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiInterfaceExporter
     *      #registerExportedInterface(java.lang.String, java.lang.String)
     */
    public void registerExportedInterface(String interfaceName,
            String properties, ParsingContext parsingContext)
    {   
        long launchDuration = -1;
        long startTime = new Date().getTime();
        String interfaceSimpleName = interfaceName.substring(
                interfaceName.lastIndexOf('.') + 1);
        String interfacePackageName = interfaceName.substring(0,
                interfaceName.lastIndexOf('.'));
        String interfaceFileName = new StringBuilder(interfaceName.replace('.',
                '/')).append(".class").toString();

        String name = interfaceSimpleName;
        URL url = parsingContext.getClassLoader().getResource(interfaceFileName);        
        if (url == null)
        {
            LOGGER.log(Level.WARNING, new StringBuilder("Unable to export ")
                    .append(interfaceName).append(": resource file not found")
                    .toString());
            return;
        }
        List<URL> resources = new ArrayList<URL>();
        resources.add(url);
        
        Hashtable<String, String> headers = new Hashtable<String, String>();
        headers.put(OSGiIOUtils.MANIFESTVERSION, "1.0");
        headers.put(OSGiIOUtils.BUNDLE_MANIFESTVERSION, "2");
        headers.put(OSGiIOUtils.BUNDLE_NAME, name);
        headers.put(OSGiIOUtils.BUNDLE_SYMBOLIC_NAME, name);
        headers.put(OSGiIOUtils.BUNDLE_VENDOR, "INRIA - University of Lille 1");
        headers.put(OSGiIOUtils.BUNDLE_EXPORT_PACKAGE, interfacePackageName);
        headers.put(OSGiIOUtils.BUNDLE_IMPORT_PACKAGE, "org.osgi.framework;version=\"1.3.0\"");                
        headers.put(OSGiIOUtils.BUNDLE_DYNAMICIMPORT_PACKAGE,packageDependencies(
                (URLClassLoader) parsingContext.getClassLoader(),
                interfacePackageName, interfaceName) );
        headers.put(OSGiIOUtils.BUNDLE_CLASSPATH, ".");
        
        BundleRevisionItf<?,?> exportedBundle = OSGiIOUtils.createBundle(
               getBundleContext(), name, resources,headers);
        
        LOGGER.log(Level.CONFIG, exportedBundle + " installed");   
        try
        {            
            Class<?> interfaceClass = exportedBundle.loadClass(interfaceName); 
            getClManager().addLoaded(interfaceName, interfaceClass); 
            
        } catch (ClassNotFoundException e)
        {       
            LOGGER.log(Level.WARNING, interfaceName + " not found in the BundleContext");
            
        } catch(Exception e)
        {
            LOGGER.log(Level.WARNING, e.getMessage());
            
        } finally
        {
            launchDuration = new Date().getTime() - startTime;
            LOGGER.log(Level.INFO, "Exported interface registration '" + 
                    interfaceName + "' : " + launchDuration + " ms");
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiContext#
     * registerOSGiImplementation(org.ow2.frascati.parser.api.ParsingContext)
     */
    public void registerOSGiImplementation(String bundleName,
            ParsingContext parsingContext)
    {
        FrascatiClassLoader loader = (FrascatiClassLoader) 
        parsingContext.getClassLoader();
        
        if(bundleName != null && bundleName.length()>0)
        {
            String[] bundlesName = bundleName.split(" ");
            for(String name : bundlesName )
            {
                URL embeddedResourceURL = getClManager().getResource(name);
                if(embeddedResourceURL != null)
                {
                    AbstractResource embeddedResource = null;
                    if("frascati".equals(embeddedResourceURL.getProtocol()))
                    {
                        AbstractResource containerBundleResource = 
                            AbstractResource.newResource(null, 
                                    getBundleContext().getBundle(Long.parseLong(
                                            embeddedResourceURL.getHost())));
                        
                        embeddedResource = containerBundleResource.cacheEmbeddedResource(
                                name, getCacheDir().getAbsolutePath());
                        
                        if(embeddedResource != null)
                        {
                            loader.addUrl(embeddedResource.getResourceURL());
                        }
                    } else
                    {
                        loader.addUrl(embeddedResourceURL);
                    }
                }
            }
        }
        String osgiID = System.getProperty(OSGiRevisionItf.OSGi_REVISION_ID);
        String bundleSN = new StringBuilder("bundle-frascati-").append(osgiID).toString();
        
        BundleRevisionItf<?,?>[] bundles = bundleContext.getBundles();
        BundleRevisionItf<?,?> bundle = null;
        
        for(BundleRevisionItf<?,?> bundler : bundles)
        {
            if(bundleSN.equals(bundler.getSymbolicName()))
            {             
                bundle = bundler;
                break;
            }
        }
        if(bundle != null)
        {
            AbstractResource bundleResource = AbstractResource.newResource(null, bundle);
            bundleResource.buildEntries();
            
            for(String embeddedJarName : bundleResource.getEmbeddedJars())
            {
                try
                {
                    URL url = bundleResource.cacheEmbeddedResource(embeddedJarName,
                            getCacheDir().getAbsolutePath()).getResourceURL();
                    loader.addUrl(url);
                    
                } catch(Exception e)
                {
                    if(LOGGER.isLoggable(Level.CONFIG))
                    {
                        LOGGER.log(Level.CONFIG,e.getMessage(),e);
                    }
                }
            }
        }
    }

    /**
     *  {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiContext #getBundleContext()
     */
    public BundleContextRevisionItf<?,?> getBundleContext()
    {
        if (bundleContext == null)
        {       
            //When Equinox is instantiated it defines the ClassLoader of the current thread
            //For OSGi in FraSCAti use we must store the current context ClassLoader
            //to re-use it as the ClassLoader of the current thread after Equinox instantiation
            ClassLoader currentClassLoader = Thread.currentThread().getContextClassLoader();            
            try
            {                   
                bundleContext = OSGiHelper.getPlatform().getBundleContext();
                
            } catch (Exception e)
            {
                LOGGER.log(Level.WARNING, "Unable to find the current BundleContext",e);
                
            } finally
            {
                if(Thread.currentThread().getContextClassLoader()!= currentClassLoader)
                {
                    Thread.currentThread().setContextClassLoader(currentClassLoader);
                }
            }
        }
        return bundleContext;
    }

    /**
     * Create a temporary directory to store cache files which are used by the
     * FraSCAtiOSGiContext instance
     **/
    public File getCacheDir()
    {
        if (cache != null && cache.exists())
        {
            return cache;
        }
        cache = IOUtils.createTmpDir("FraSCAti_" + hashCode());
        return cache;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiContext#getCLManager()
     */
    public ClassLoader getClassLoader()
    {
        return classLoaderManager.getClassLoader();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.FraSCAtiOSGiContext#getClManager()
     */
    public AbstractResourceClassLoader getClManager()
    {
        if(clManager == null)
        {
            try
            {
                clManager = new AbstractResourceClassLoader(getClassLoader(),null,null);
                
            } catch (ResourceAlreadyManagedException e)
            {   
                e.printStackTrace();
            }
        }
        return clManager;
    }
}
