/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.osgi.service;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiNotFoundCompositeException;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiNotFoundServiceException;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiNotRunnableServiceException;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.resource.AbstractResourceClassLoader;
import org.ow2.frascati.osgi.resource.FraSCAtiOSGiTracker;
import org.ow2.frascati.starter.api.AbstractInitializable;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.util.reflect.ReflectionHelper;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.ResourceAlreadyManagedException;

/**
 * Implementation of the FraSCAtiOSGiService interface
 */
public class FraSCAtiOSGiServiceImpl extends AbstractInitializable implements
        FraSCAtiOSGiService
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    // Use reflection to cope with potential explorer library lack
    private ReflectionHelper explorer;
    
    /**
     * A reference of the curent instance registration in the associated 
     * BundleContext
     */
    private int fServiceRegistration;
    
    /**
     * The required FraSCAtiOSGiRegistry
     */
    @Reference(name = "frascati-osgi-registry")
    private FraSCAtiOSGiRegistry registry;
    
    /**
     * The required FraSCAtiOSGiContext
     */
    @Reference(name = "frascati-osgi-context")
    private FraSCAtiOSGiContext foContext;
    
    /**
     * The required CompositeManager
     */
    @Reference(name = "frascati-domain")
    private CompositeManager compositeManager;    
    
    /**
     * the composite name being loaded
     */
    private String loading;

    /**
     * list of loaded composites'name
     */
    private List<String> loaded;

    /**
     * Helper for Bundles 
     * */
    private ReflectionHelper bundle;

    /**
     * Tracker of bundle-embedded composites in the BundleContext 
     */
    private FraSCAtiOSGiTracker tracker;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * Unload an SCA component previously loaded using the FraSCAtiOSGiService loadSCA
     * method
     * 
     * @param compositeName
     * @param bundle
     */
    private void unloadSCA(String compositeName)
    {
        try
        {
            compositeManager.removeComposite(compositeName);
            
        } catch (ManagerException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        log.log(Level.CONFIG, compositeName + " unloaded");
        refreshExplorer();
    }

    /**
     * Call the run method of a Runnable service which name, classes, 
     * parent composite's name and owner component are passed on as parameter
     *   
     * @param serviceName
     *          The name of the service
     * @param serviceClass
     *          The Class object to which the service belong
     * @param compositeName
     *          The service parent composite's name
     * @param composite
     *          The owner component
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     */
    private void runService(String serviceName, Class<?> serviceClass,
            String compositeName, Component composite)
            throws FraSCAtiOSGiNotFoundServiceException,
            FraSCAtiOSGiNotRunnableServiceException
    {
        if (serviceClass == null)
        {
            log.log(Level.WARNING, "serviceClass parameter is null ");
            throw new FraSCAtiOSGiNotFoundServiceException();
        }
        if (!"java.lang.Runnable".equals(serviceClass.getName()))
        {
            try
            {
                serviceClass.asSubclass(Runnable.class);
            } catch (ClassCastException cce)
            {
                log.log(Level.CONFIG, serviceClass + " is not Runnable");
                throw new FraSCAtiOSGiNotRunnableServiceException();
            }
        }
        try
        {
            Runnable service = (Runnable) composite.getFcInterface(serviceName);
            service.run();
        } catch (NoSuchInterfaceException e)
        {
            throw new FraSCAtiOSGiNotFoundServiceException();
        }
    }

    /**
     * Try to find the current frascati's explorer instance for future 
     * refresh actions
     */
    private void configureExplorer()
    {
        log.log(Level.CONFIG, "FraSCAti's Explorer configuration");
        explorer = new ReflectionHelper(foContext.getClassLoader(),
                "org.ow2.frascati.explorer.ExplorerGUI");
        if (!explorer.isClassKnown())
        {
            return;
        }
        ReflectionHelper frascatiExplorer = new ReflectionHelper(foContext
                .getClassLoader(),"org.ow2.frascati.explorer.api.FraSCAtiExplorer");
        ReflectionHelper fraSCAtiExplorerSingleton = new ReflectionHelper(
                foContext.getClassLoader(),"org.ow2.frascati.explorer.api.FraSCAtiExplorerSingleton");
        try
        {
            Object singleton = frascatiExplorer.getStatic("SINGLETON");
            if (singleton != null)
            {
                fraSCAtiExplorerSingleton.set(singleton);
                explorer.set(fraSCAtiExplorerSingleton.invoke("get"));
            }
        } catch (Exception e)
        {
            log.log(Level.CONFIG,e.getMessage(),e);
        }
    }

    /**
     * Add the jar file URL to the explorer configuration
     * 
     * @param jarFileURL
     *            the jar file URL
     */
    private void refreshExplorer(ClassLoader classLoader)
    {
        if (explorer == null)
        {
            configureExplorer();
        }
        if(explorer.get() == null)
        {
            return;
        }
        explorer.invoke("updateClassLoader", new Class<?>[]
        { ClassLoader.class }, new Object[]
        { classLoader });
        refreshExplorer();
    }

    /**
     * Refresh explorer
     */
    private void refreshExplorer()
    {
        if (explorer == null)
        {
            configureExplorer();
        }
        if(explorer.get() == null)
        {
            return;
        }
        @SuppressWarnings("unchecked")
        final ReflectionHelper refreshExplorerTreeThread = new ReflectionHelper(
                (ClassLoader) ((Map<Class<?>, Object>) explorer
                        .get("frascatiExplorerServices"))
                        .get(ClassLoader.class),
                "org.ow2.frascati.explorer.plugin.RefreshExplorerTreeThread");
        // use another thread to avoid a
        // "Cannot call invokeAndWait from the event dispatcher thread" Error
        new Thread(new Runnable()
        {
            public void run()
            {
                try
                {
                    Thread.sleep(20);
                    
                } catch (InterruptedException e)
                {
                    log.log(Level.WARNING,e.getMessage(),e);
                }
                refreshExplorerTreeThread.invokeStatic("refreshFrascatiExplorerGUI");
            }
        }).start();
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * FraSCAtiOSGiService instance initialization
     */
    @Init
    public void init()
    {
        if(log.isLoggable(Level.CONFIG))
        {            
            log.log(Level.CONFIG,"init FraSCAtiOSGiService");
        }
        this.loaded = new ArrayList<String>();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.AbstractInitializable#
     * doInitialize()
     */
    public void doInitialize()
    {
        if (foContext.getBundleContext() != null)
        {   
            try
            {
                fServiceRegistration = foContext.getBundleContext().registerService(
                        FraSCAtiOSGiService.class.getCanonicalName(), this, null);
                
                this.tracker = new FraSCAtiOSGiTracker(this,foContext);
                
            } catch(Exception e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
            log.log(Level.INFO,"Registration id : " + fServiceRegistration);
            
        } else
        {
            log.log(Level.WARNING,"Unable to register the FraSCAtiOSGiService : "
                            + "no BundleContext found");
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#loading()
     */
    public String loading()
    {
        return this.loading;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#loading()
     */
    public boolean loaded(String name)
    {
        return loaded.contains(name);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#loadSCA( java.net.URL,
     * java.lang.String)
     */
    public String loadSCA(URL resourceURL, String compositeName)
            throws FraSCAtiOSGiNotFoundCompositeException
    {
        return loadSCA(AbstractResource.newResource(null,resourceURL), compositeName);
    }

    /**
     *  {@inheritDoc}
     *  
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#loadSCA(
     * org.ow2.frascati.util.resource.AbstractResource, java.lang.String)
     *
     * Load an SCA component embedded in the AbstractResource passed on as a parameter
     * 
     * @param resource
     *            the AbstractResource containing the composite file to load
     * @param compositeName
     *            the composite file name
     * @throws FraSCAtiOSGiNotFoundCompositeException
     *             ; if no component can be loaded
     */
    public synchronized String loadSCA(AbstractResource resource, String compositeName)
            throws FraSCAtiOSGiNotFoundCompositeException
    {
        if (resource == null)
        {
            log.log(Level.WARNING, "AbstractResource object is null");
        }
        long startTime = new Date().getTime();
        String registeredCompositeName = compositeName;
        loading = compositeName;
        try
        {
            ClassLoader compositeClassLoader = new AbstractResourceClassLoader(
                    foContext.getClassLoader(),foContext.getClManager(),resource);
            
            Component composite = compositeManager.getComposite(compositeName,
                    compositeClassLoader);  
            
            long bundleId = -1;
            if(bundle == null)
            {
                bundle = new ReflectionHelper(foContext.getClassLoader(),
                        "org.osgi.framework.Bundle");
            }
            try
            {
                bundle.set(resource.getResourceObject());
                if(bundle.get() != null)
                {
                    bundleId = (Long) bundle.invoke("getBundleId");
                }                
            } catch (ClassCastException e)
            {
                log.log(Level.CONFIG,e.getMessage(),e);
                
            } catch (Exception e)
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
            if (composite != null)
            {
                registeredCompositeName = registry.register(bundleId,
                        composite, compositeClassLoader);
                
                loaded.add(registeredCompositeName);
                
            } else
            {
                log.log(Level.WARNING, "ProcessComposite method has returned a "
                                + "null Component");
                throw new FraSCAtiOSGiNotFoundCompositeException();
            }
            refreshExplorer(compositeClassLoader);
            
        } catch (ManagerException e)
        {
            log.log(Level.WARNING, "Loading process of the composite has "
                    + "thrown an exception");
            throw new FraSCAtiOSGiNotFoundCompositeException(); 
            
        } catch (ResourceAlreadyManagedException e)
        {
            log.log(Level.WARNING, e.getMessage(),e);
        } 
        finally
        {
            loading = null;
        }
        log.log(Level.INFO, registeredCompositeName + " loaded in "
                + (new Date().getTime() - startTime) + " ms");
        
        return registeredCompositeName;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#unloadSCA(long)
     */
    public void unloadSCA(long bundleId)
    {
        String[] compositeNames = registry.getComposites(bundleId);
        int n = 0;
        for (; n < compositeNames.length; n++)
        {
            unloadSCA(compositeNames[n]);
        }
        registry.unregister(bundleId);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#launch(java.lang.String,
     * java.lang.String)
     */
    public synchronized void launch(String compositeName, String serviceName)
            throws FraSCAtiOSGiNotFoundCompositeException,
            FraSCAtiOSGiNotFoundServiceException,
            FraSCAtiOSGiNotRunnableServiceException
    {
        Component composite = registry.getComposite(compositeName);
        if (composite != null)
        {
            Class<?> serviceClass = registry.getService(compositeName,
                    serviceName);
            runService(serviceName, serviceClass, compositeName, composite);
        } else
        {
            log.log(Level.WARNING, "unknown composite :" + compositeName);
            throw new FraSCAtiOSGiNotFoundCompositeException();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#launch(
     * java.lang.String)
     */
    public void launch(String compositeName)
            throws FraSCAtiOSGiNotFoundCompositeException,
            FraSCAtiOSGiNotRunnableServiceException,
            FraSCAtiOSGiNotFoundServiceException
    {
        Map<String, Class<?>> map = registry.getServices(compositeName);
        Component composite = registry.getComposite(compositeName);
        if (map == null)
        {
            log.log(Level.WARNING, "No service associated to " + compositeName);
            if (composite == null)
            {
                throw new FraSCAtiOSGiNotFoundCompositeException();
            }
            throw new FraSCAtiOSGiNotFoundServiceException();
        }
        boolean oneRunnable = false;
        Iterator<String> mapIterator = map.keySet().iterator();
        while (mapIterator.hasNext())
        {
            String serviceName = (String) mapIterator.next();
            Class<?> serviceClass = map.get(serviceName);
            try
            {
                runService(serviceName, serviceClass, compositeName, composite);
                oneRunnable = true;
            } catch (FraSCAtiOSGiNotRunnableServiceException e)
            {
                log.log(Level.CONFIG, serviceName + " is not Runnable");
            }
        }
        if (!oneRunnable)
        {
            log.log(Level.WARNING, "No Runnable service for in the composite '"
                    + compositeName + "'");
            throw new FraSCAtiOSGiNotRunnableServiceException();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService#getCacheDir()
     */
    public File getCacheDir()
    {
        return foContext.getCacheDir();
    }

    /**
     * Unload the service
     * 
     * @throws FrascatiException
     */
    public void unload() throws FrascatiException
    {
        String[] compositeNames = registry.unregisterAll();
        int n = 0;
        for (; n < compositeNames.length; n++)
        {
            String compositeName = compositeNames[n];
            unloadSCA(compositeName);
        }
    }
}
