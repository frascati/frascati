/**
  * OW2 FraSCAti
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.resource;

import java.io.InputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.enumeration.URLBag;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.ResourceAlreadyManagedException;

/**
 * Inherited FrascatiClassLoader using an AbstractResource object
 */
public class AbstractResourceClassLoader extends FrascatiClassLoader
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * The logger
     */
    protected Logger logger = Logger.getLogger(getClass().getCanonicalName());

    /**
     * the delegate AbstractResourceClassLoader called in find classes and resources processes
     * 
     */
    protected AbstractResourceClassLoader delegate;
    
    /**
     * the manager AbstractResourceClassLoader, first called in the loadClass method
     */
    protected AbstractResourceClassLoader manager;
    
    /**
     * a map of already loaded classes
     */
    protected Map<String, Class<?>> loaded = new HashMap<String, Class<?>>();

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * Search for a previously loaded class in the map
     * 
     * @param name
     *          the name of the searched class
     * @return
     *          the loaded class if it exists
     *          null otherwise
     */
    protected Class<?> getLoaded(String name)
    {
        return loaded.get(name);
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param parent
     *          the parent ClassLoader
     */
    public AbstractResourceClassLoader(ClassLoader parent)
    {
        super(new URL[0],parent);
        this.delegate = null;
        this.setManager(null);
    }

    /**
     * Constructor
     * 
     * @param parent
     *            The parent ClassLoader
     * @param manager
     *            The AbstractResourceClassLoader manager
     * @param clResource
     *            The handled AbstractResource
     * @throws ResourceAlreadyManagedException
     */
    public AbstractResourceClassLoader(ClassLoader parent,
            AbstractResourceClassLoader manager,
            AbstractResource clResource)
            throws ResourceAlreadyManagedException
    {
        super(clResource!=null?new URL[]{clResource.getResourceURL()}:new URL[0],parent);
        this.delegate = null;
        this.setManager(manager);
    }

    /**
     * Set the AbstractResourceClassLoader delegate attribute
     * 
     * @param delegate
     *          the AbstractResourceClassLoader delegate
     */
    public void setDelegate(AbstractResourceClassLoader delegate)
    {
        this.delegate = delegate;
    } 

    /**
     * Set the AbstractResourceClassLoader manager attribute
     * 
     * @param manager
     *          the AbstractResourceClassLoader manager
     */
    public synchronized void setManager(AbstractResourceClassLoader manager)
    {
        this.manager = manager;
    }
    
    /**
     * Add a loaded class to the map
     * 
     * @param name
     *          the name of the class
     * @param clazz
     *          the loaded class
     */
    public void addLoaded(String name, Class<?> clazz)
    {
        loaded.put(name, clazz);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#loadClass(java.lang.String)
     */
    @Override
    public synchronized Class<?> loadClass(String name)
            throws ClassNotFoundException
    {
        return loadClass(name, false);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
     */
    @Override
    public synchronized Class<?> loadClass(String name, boolean resolve)
            throws ClassNotFoundException
    {
        Class<?> clazz = null;
        synchronized(manager)
        {
          clazz = manager.getLoaded(name);
        }
        if(clazz == null)
        {
            clazz = getLoaded(name);
        }
        if (clazz == null)
        {
            clazz = findLoadedClass(name);
        }
        if (clazz == null)
        {
            ClassLoader parent = getParent();            
            if (parent == null)
            {
                parent = ClassLoader.getSystemClassLoader();
            }
            try
            {
                clazz = parent.loadClass(name);
                
            } catch (ClassNotFoundException e)
            {
                if(logger.isLoggable(Level.CONFIG))
                {
                    logger.log(Level.CONFIG, "[" + this + "] getParent().loadClass("+
                        name+") has thrown a ClassNotFoundException");
                }
            }
        }
        if (clazz == null)
        {
            clazz = findClass(name);
        }
        if(resolve)
        {
          resolveClass(clazz);
        }
        return clazz;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#getURLs()
     */
    @Override
    public URL[] getURLs()
    {
        URLBag urlBag = new URLBag();
        if(delegate != null)
        {
            urlBag.addArray(delegate.getURLs());
        }
        urlBag.addArray(super.getURLs());
        return urlBag.toArray();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResources(java.lang.String)
     */
    @Override
    public Enumeration<URL> getResources(String name)
    {
        URLBag bag = new URLBag();
        if(delegate != null)
        {
            bag.addEnumeration(delegate.getResources(name));
        }
        try
        {
            bag.addEnumeration(super.getResources(name));
            
        } catch (IOException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        return bag;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResource(java.lang.String)
     */
    @Override
    public URL getResource(String name)
    {
        URL resourceURL = null;
        if(delegate != null)
        {
          resourceURL = delegate.getResource(name);
        }
        if(resourceURL == null)
        {
            resourceURL = super.getResource(name);
        }
        return resourceURL;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResourceAsStream(java.lang.String)
     */
    @Override
    public InputStream getResourceAsStream(String name)
    {
        InputStream is = null;
        if(delegate != null)
        {
          is = delegate.getResourceAsStream(name);
        }
        if(is == null)
        {
            is = super.getResourceAsStream(name);
        }
        return is;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.util.FrascatiClassLoader#findClass(java.lang.String)
     */
    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException
    {
       Class<?> clazz = null;
       if(delegate != null)
       {
           try
           {
               clazz = delegate.findClass(name);
               
           } catch (ClassNotFoundException e)
           {
               logger.log(Level.CONFIG, "[" + this + "] resourceLoader.findClass("+
                       name+") has thrown a ClassNotFoundException");
           }
       } 
       if(clazz == null)
       {
           clazz = super.findClass(name);
       }
       return clazz;
    }
}
