/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.resource;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiNotFoundCompositeException;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;

/**
 *Process resources found by a FraSCAtiOSGiTracker
 */
public class OSGiResourceProcessor extends AbstractLoggeable
{
    private FraSCAtiOSGiService service;

    /**
     * Constructor
     * 
     * @param service
     *            Required FraSCAtiOSGiService
     */
    public OSGiResourceProcessor(FraSCAtiOSGiService service)
    {
        this.service = service;
    }

    /**
     * Ask the FraSCAtiOSGiService to process found composite file(s)
     * 
     * The method will react in function of three parameters :
     * 
     * - composite(s) file(s) found in the studied resource object :yes | no 
     * 
     * -'Frascati-Process' header found in the parent bundle and all specified
     *   composites files have been found (equivalent to 'Frascati-Process' header
     *   not found) :yes | no 
     * 
     * - embedded jar file(s) found :yes | no
     * 
     * _________________________________________________________________________________________________
     * |                   |                       |                      |                             |
     * |  Composite File   |    Frascati-Process   | Embedded Jar File(s) |          Decision           |
     * |        Found      |           Header      |                      |                             |
     * |___________________|_______________________|______________________|_____________________________|
     * |                   |                       |                      |                             |
     * |        no         |          yes          |          no          |         do nothing          |
     * |___________________|_______________________|______________________|_____________________________|
     * |                   |                       |                      |ask the FraSCAtiOSGiService  |
     * |        yes        |          yes          |          no          |to process found composite   |
     * |                   |                       |                      |file(s)                      |
     * |___________________|_______________________|______________________|_____________________________|
     * |                   |                       |                      |ask the FraSCAtiOSGiService  |
     * |                   |                       |                      |to process found composite   |
     * |        yes        |          yes          |          yes         |file(s) without looking in   |
     * |                   |                       |                      |existing embedded jar file(s)|
     * |___________________|_______________________|______________________|_____________________________|
     * |                   |                       |                      |ask the FraSCAtiOSGiService  |
     * |         yes       |          no           |          no          |to process found composite   |
     * |                   |                       |                      |file(s) + Warning message    |
     * |___________________|_______________________|______________________|_____________________________|
     * |                   |                       |                      |ask the FraSCAtiOSGiService  |
     * |         yes       |          no           |          yes         |to process found composite   |
     * |                   |                       |                      |file(s) and look in embedded |
     * |                   |                       |                      |jar file(s) for missing      |
     * |                   |                       |                      |composite file(s)            |
     * |___________________|_______________________|______________________|_____________________________|
     *  
     * 
     * @param resource
     *            the resource object to explore
     *            
     * @param unmanagedResources
     *            the content of the EmbeddedAsResource header as a list of
     *            strings
     */
    public void processResource(AbstractResource resource,
            List<String> unmanagedResources, ArrayList<String> compositesToProcess,
            List<String> embeddedComposites)
    {
        if(log.isLoggable(Level.CONFIG))
        {
            StringBuilder messageBuilder  =  new StringBuilder();
            for(String compositeToProcess : compositesToProcess)
            {
                messageBuilder.append("\n").append(compositeToProcess);
            }
            if(messageBuilder.length() == 0)
            {
                messageBuilder.append("No composite file defined to process in the Bundle's headers");
                
            } else 
            {
                messageBuilder.append("\n ...  searched");
            }
            log.log(Level.CONFIG,messageBuilder.toString());
        }
        if (embeddedComposites.size() > 0)
        {
            int m = 0;
            for (; m < embeddedComposites.size(); m++)
            {
                String embeddedComposite = embeddedComposites.get(m);
                if(log.isLoggable(Level.INFO))
                {
                    log.log(Level.INFO,embeddedComposite + " processing ...");
                }
                try
                {
                    service.loadSCA(resource.getResourceURL(),
                            embeddedComposite);
                    
                } catch (FraSCAtiOSGiNotFoundCompositeException e)
                {
                    log.warning("Unable to find the '" + embeddedComposite + 
                            "' embedded composite");
                }
                
                if (!compositesToProcess.remove(embeddedComposite))
                {
                    compositesToProcess.remove(embeddedComposite.substring(0,
                            embeddedComposite.lastIndexOf(".composite") + 1));
                }
            }
        }
        int compositesSize = compositesToProcess.size();
        if(log.isLoggable(Level.CONFIG))
        {
            log.log(Level.CONFIG, compositesSize + 
                    " more Composite file(s) to process");
        }
        if (embeddedComposites.size() > 0 && compositesSize == 0)
        {
            if(log.isLoggable(Level.INFO))
            {
                log.log(Level.INFO,"All required Composite files have been processed");
            }
            return;
        }
        List<String> embeddedJars = resource.getEmbeddedJars();
        int jarsSize = embeddedJars.size();
        if (jarsSize == 0 && compositesSize > 0)
        {
            if(log.isLoggable(Level.WARNING))
            {
                log.log(Level.WARNING,"All declared composite files have not been found");
            }
            return;
        }
        int n = 0;
        for (; n < jarsSize; n++)
        {
            String embeddedJar = embeddedJars.get(n);
            AbstractResource embeddedResource = resource.cacheEmbeddedResource(
                    embeddedJar, service.getCacheDir().getAbsolutePath());
            embeddedResource.setUnmanagedResourcesList(unmanagedResources);
            embeddedResource.buildEntries();
            
            processResource(embeddedResource, unmanagedResources,
                    compositesToProcess,
                    getEmbeddedComposites(embeddedResource));
        }
    }

    /**
     * This method is called when a Bundle has passed to the "RESOLVED" state
     * 
     * @param bundle
     *            The resolved Bundle
     */
    public void bundleResolved(BundleRevisionItf<?,?> bundle)
    {        
        if (service == null)
        {
            if(log.isLoggable(Level.WARNING))
            {
                log.log(Level.WARNING,"No FraSCAtiOSGiService has been found");
            }
            return;
        }
        AbstractResource resource = AbstractResource.newResource(null,bundle);

        if (resource == null)
        {
            if(log.isLoggable(Level.SEVERE))
            {
                log.log(Level.SEVERE,"An error occured trying to build a Resource object"
                    + " with '" + bundle.getSymbolicName() + "' bundle");
            }
            return;
        }
        List<String> unmanagedResources = OSGiIOUtils.getUnmanagedResources(bundle);
        
        //Define as an ArrayList instead of a List because its items have to be removable        
        ArrayList<String> compositesToProcess = OSGiIOUtils.getCompositesToProcess(bundle);
        resource.setUnmanagedResourcesList(unmanagedResources);
        resource.buildEntries();
        List<String> embeddedComposites = getEmbeddedComposites(resource);
        
        if (embeddedComposites.size() > 0)
        {
            try
            {
                String cachedBundlePath = OSGiIOUtils.cacheBundle(
                        bundle,service.getCacheDir());
                
                if (cachedBundlePath != null)
                {                    
                    URL cachedBundleURL = new File(cachedBundlePath).toURI().toURL();
                    resource = AbstractResource.newResource(null,cachedBundleURL);
                    resource.setUnmanagedResourcesList(unmanagedResources);
                    resource.buildEntries();
                    
                } else if(log.isLoggable(Level.WARNING))
                {
                    log.log(Level.WARNING,
                            "Error occured while trying to cache the bundle "
                            + bundle);
                }
            } catch (MalformedURLException e)
            {
                if(log.isLoggable(Level.WARNING))
               {
                log.log(Level.WARNING,e.getMessage(),e);
               }
            }
        }
        processResource(resource, unmanagedResources, compositesToProcess,
                embeddedComposites);
    }

    /**
     * Return the list of composites files names existing in the AbstractResource object
     * passed on as a parameter
     * 
     * @param resource
     *            the resource to explorer
     * @return the list of composites files names
     */
    public List<String> getEmbeddedComposites(AbstractResource resource)
    {
        List<URL> embeddedCompositesURL = new ArrayList<URL>();
        List<String> embeddedCompositesName = new ArrayList<String>();
        resource.getRegExpResources(".+\\.composite$", embeddedCompositesURL);
        int n = 0;
        for (; n < embeddedCompositesURL.size(); n++)
        {            
            embeddedCompositesName.add(n, IOUtils.pathLastPart(
                    embeddedCompositesURL.get(n).getFile()));
        }
        return embeddedCompositesName;
    }
}
