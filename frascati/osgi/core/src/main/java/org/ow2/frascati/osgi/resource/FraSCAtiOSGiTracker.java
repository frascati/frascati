/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 *
 * Contributor(s): Christophe Munilla
 *
 */
package org.ow2.frascati.osgi.resource;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.BundleListenerRevisionItf;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.resource.OSGiResourceProcessor;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.AbstractLoggeable;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A FraSCAtiOSGiTracker listen about Bundles changes and look 
 * for SCA Composite in nearly installed ones
 */
public class FraSCAtiOSGiTracker extends AbstractLoggeable 
implements BundleListenerRevisionItf
{
    private FraSCAtiOSGiService service;
    
    private FraSCAtiOSGiContext context;
    
    private OSGiResourceProcessor processor;

    public FraSCAtiOSGiTracker(FraSCAtiOSGiService service,
            FraSCAtiOSGiContext context)
    {
        if(log.isLoggable(Level.CONFIG))
        {
            log.log(Level.CONFIG, "init FraSCAtiOSGiTracker");
        }
        this.service = service;
        this.context = context;
        context.getBundleContext().registerBundleListener(this);
        processor = new OSGiResourceProcessor(service);
    }

    public void bundleChanged(BundleRevisionItf bundleRevision,int bundleEventType)
    {
        if (service == null)
        {
            log.log(Level.WARNING, "FraSCAtiOSGiService has not been found");
            return;
        }
        String bundleEventName = context.getBundleContext().mapBundleEvent(
                bundleEventType);

        log.log(Level.CONFIG, "[" + bundleRevision + "] event is " + bundleEventName);
        if ("UNINSTALLED".equals(bundleEventName))
        {
            service.unloadSCA(bundleRevision.getBundleId());
            
        } else if ("RESOLVED".equals(bundleEventName))
        {
            if (OSGiIOUtils.getHeader(bundleRevision, OSGiIOUtils.FRAGMENT_HEADER) == null)
            {
                processor.bundleResolved(bundleRevision);
            }
        }
    }
}
