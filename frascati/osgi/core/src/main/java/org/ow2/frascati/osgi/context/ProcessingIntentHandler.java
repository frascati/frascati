/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.osgi.context;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.xml.namespace.QName;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiContext;
import org.ow2.frascati.osgi.api.FraSCAtiOSGiRegistry;
import org.ow2.frascati.osgi.resource.AbstractResourceClassLoader;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.reference.ServiceReferenceUtil;
import org.ow2.frascati.util.resource.ResourceAlreadyManagedException;

/**
 * The ProcessingIntentHandler is woven with the CompositeManager to 
 * intercept its calls and replace the used ProcessingContext in 
 * composites processing processes 
 */
@EagerInit
@Scope("COMPOSITE")
@SuppressWarnings("unused")
public class ProcessingIntentHandler implements IntentHandler
{
    /**
     * The Logger
     */
    private static Logger log = Logger.getLogger(
            ProcessingIntentHandler.class.getCanonicalName());
    
    /**
     * The required CompositeManager
     */
    @Reference(name = "frascati-domain")
    private CompositeManager compositeManager;
    
    /**
     * The required FraSCAtiOSGiRegistry
     */
    @Reference(name = "frascati-osgi-context")
    private FraSCAtiOSGiContext foContext;
   
    /**
     * Define whether a call has to be intercepted or not
     */
    private boolean enabled = true;
    
    /**
     * Weave this ProcessingIntentHandler instance with the compositeManager and
     * its processing methods
     */
    @Init
    public void init()
    {
        TinfiComponentInterceptor<?> interceptor = ServiceReferenceUtil.getInterceptor(
                compositeManager);
        try
        {
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("getContribution", 
                    new Class<?>[]{String.class}));
            
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("processContribution", 
                    new Class<?>[]{String.class,ProcessingContext.class}));
            
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("processComposite", 
                    new Class<?>[]{QName.class,ProcessingContext.class}));
            
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("getComposite", 
                    new Class<?>[]{QName.class}));
            
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("getComposite", 
                    new Class<?>[]{String.class}));

            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("getComposite", 
                    new Class<?>[]{String.class, ClassLoader.class}));
            
            interceptor.addIntentHandler(this,
                    CompositeManager.class.getDeclaredMethod("getComposite", 
                    new Class<?>[]{String.class, URL[].class}));
            
        } catch (SecurityException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (NoSuchMethodException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.tinfi.api.IntentHandler#
     * invoke(org.ow2.frascati.tinfi.api.IntentJoinPoint)
     */
    public Object invoke(IntentJoinPoint intentJP) throws Throwable
    {   
        //if the call has to be intercepted...
        if(enabled)
        {
            //find the called method...
            Method method = getClass().getDeclaredMethod(intentJP.getMethod().getName(),
                    intentJP.getMethod().getParameterTypes());
            //and invoke its overwritten version
            return method.invoke(this,intentJP.getArguments());
            
        } else 
        {   
            //otherwise just proceed
            return intentJP.proceed();
        }
    }

    /**
     * Overwritten version of the CompositeManager's "processContribution" method
     *  
     * @param contribution
     *          the name of the contribution zip file to process
     * @param processingContext
     *          the ProcessingContext used to process
     * @return
     *          the array of processed components
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public Object processContribution(String contribution,ProcessingContext processingContext) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        //calls are no more intercepted
       enabled = false;
       try
       {   
           return compositeManager.processContribution(contribution, 
                   newProcessingContext(processingContext));            
       } finally
       {
           //calls can be intercepted again
           enabled = true;
       }
    }
    
    /**
     * Overwritten version of the CompositeManager's "getContribution" method
     *  
     * @param contribution
     *          the name of the contribution zip file to process
     * @return
     *          the array of processed components
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public Object getContribution(String contribution) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        //calls are no more intercepted
       enabled = false;
       try
       {   
           return compositeManager.processContribution(contribution, 
                   newProcessingContext());            
       } finally
       {
           //calls can be intercepted again
           enabled = true;
       }
    }
    
    /**
     * Overwritten version of the CompositeManager's "processComposite" method
     *  
     * @param composite
     *          the QName of the composite file to process
     * @param processingContext
     *          the ProcessingContext used to process
     * @return
     *          the processed component
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public Object processComposite(QName composite,ProcessingContext processingContext) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        //calls are no more intercepted
       enabled = false;
       try
       {   
           return compositeManager.processComposite(composite, 
                   newProcessingContext(processingContext));            
       } finally
       {
           //calls can be intercepted again
           enabled = true;
       }
    }
    
    /**
     * Overwritten version of the CompositeManager's "getComposite(String)" method
     *  
     * @param composite
     *          the name of the composite file to process
     * @return
     *          the processed component
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public final Object getComposite(String composite) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        return getComposite(new QName(composite));  
    }

    /**
     * Overwritten version of the CompositeManager's "getComposite(QName)" method
     *  
     * @param composite
     *          the QName of the composite file to process
     * @return
     *          the processed component
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public final Object getComposite(QName composite) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        //calls are no more intercepted
        enabled = false;
        try
        {
            return compositeManager.processComposite(composite,
                    newProcessingContext());
            
        } finally
        {
            //calls can be intercepted again
            enabled = true;
        }
    }
    
    /**
     * Overwritten version of the CompositeManager's "getComposite(String,ClassLoader)" method
     *  
     * @param composite
     *          the name of the composite file to process
     * @param classLoader
     *          the class used to create a ProcessingContext
     * @return
     *          the processed component
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public final Object getComposite(String composite,ClassLoader classLoader) 
    throws ManagerException, ResourceAlreadyManagedException
    { 
        //calls are no more intercepted
        enabled = false;
        try
        {
             return compositeManager.processComposite(new QName(composite), 
                    newProcessingContext(classLoader));
        } finally
        {
            //calls can be intercepted again
            enabled = true;
        } 
    }
    

    /**
     * Overwritten version of the CompositeManager's "getComposite(String,URL[])" method
     *  
     * @param composite
     *          the name of the composite file to process
     * @param libs
     *          the array of URLs used to create ClassLoader for the ProcessingContext
     * @return
     *          the processed component
     * @throws ManagerException
     * @throws ResourceAlreadyManagedException
     */
    public final Object getComposite(String composite,URL[] libs) 
    throws ManagerException, ResourceAlreadyManagedException
    {
        //calls are no more intercepted
        enabled = false;
        try
        {
             return compositeManager.processComposite(new QName(composite), 
                    newProcessingContext(libs));
        } finally
        {
            //calls can be intercepted again
            enabled = true;
        }
    }

    /**
     * Create a new ProcessingContext using no argument
     * 
     * @return
     *          the new ProcessingContext
     *  
     * @throws ResourceAlreadyManagedException
     */
    public ProcessingContext newProcessingContext() 
    throws ResourceAlreadyManagedException
    {
        return newProcessingContext(new URL[0]);
    }
    
    /**
     * Create a new ProcessingContext using the ClassLoader passed on as a parameter
     *
     * @param classLoader
     *          the ClassLoader use to create a new ProcessingContext
     * @return
     *          the new ProcessingContext
     * @throws ResourceAlreadyManagedException
     */
    private ProcessingContext newProcessingContext(ClassLoader classLoader) 
    throws ResourceAlreadyManagedException
    {
        //If the ClassLoader is an AbstractResourceClassLoader...
        if(AbstractResourceClassLoader.class.isAssignableFrom(classLoader.getClass()))
        {
            //Create a new ProcessingContext using it
            return compositeManager.newProcessingContext(classLoader);
        }
        //Otherwise prepare an list of URLs to create a new ClassLoader
        // for a new ProcessingContext
        List<URL> libs = new ArrayList<URL>();
        
        //keep all urls that have been previously registered
        if(URLClassLoader.class.isAssignableFrom(classLoader.getClass()))
        {
           URL[] libraries = ((URLClassLoader)classLoader).getURLs();
           for(URL library : libraries)
           {
               libs.add(library);
           }
        }
        return newProcessingContext(libs.toArray(new URL[0]));
    }

    /**
     * Create a new ProcessingContext using the one passed on as a parameter
     *
     * @param processingContext
     *          the ProcessingContext used to create a new one
     * @return
     *          the new ProcessingContext
     * @throws ResourceAlreadyManagedException
     */
    private ProcessingContext newProcessingContext(ProcessingContext processingContext) 
    throws ResourceAlreadyManagedException
    {
        //get the ProcessingContext's ClassLoader
        ClassLoader classLoader = processingContext.getClassLoader();
        
        //if it's already an AbstractResourceClassLoader... 
        if(AbstractResourceClassLoader.class.isAssignableFrom(classLoader.getClass())
             || AbstractResourceClassLoader.class.isAssignableFrom(classLoader.getParent().getClass()))
        {
            //do nothing and return the processingContext argument
            return processingContext;
        }
        //otherwise prepare an list of URLs to create a new ClassLoader
        // for a new ProcessingContext
        List<URL> libs = new ArrayList<URL>();        
        
        //keep all urls that have been previously registered
        while(classLoader != null && URLClassLoader.class.isAssignableFrom(classLoader.getClass())
                && classLoader != foContext.getClassLoader())
        {
           URL[] libraries = ((URLClassLoader)classLoader).getURLs();
           for(URL library : libraries)
           {
               libs.add(library);
           }
           classLoader = classLoader.getParent();
        }   
        //create the new ProcessingContext
        ProcessingContext newContext = newProcessingContext(libs.toArray(new URL[0]));
        //define its processing mode using the one of the ProcessingContext argument
        newContext.setProcessingMode(processingContext.getProcessingMode());
        return newContext;
    }

    /**
     * Create a new ProcessingContext using the the libraries' URLs array passed on 
     * as a parameter
     *
     * @param libs
     *          the libraries' URLs array used to create the new ProcessingContext
     * @return
     *          the new ProcessingContext
     * @throws ResourceAlreadyManagedException
     */
    private ProcessingContext newProcessingContext(URL[] libs) 
    throws ResourceAlreadyManagedException
    {
        //create a new AbstractResourceClassLoader...
        AbstractResourceClassLoader loader = new AbstractResourceClassLoader(
                foContext.getClassLoader(),foContext.getClManager(),null);

        //add it URLs passed on as a parameter
        for(URL library : libs)
        {
            loader.addUrl(library);
        }
        return compositeManager.newProcessingContext(loader);
    }
}
