package org.ow2.frascati.osgi.parser;

import org.ow2.frascati.osgi.OSGiPackage;
import org.ow2.frascati.parser.metamodel.AbstractMetamodelProvider;

public class OSGiMetamodelProvider extends
        AbstractMetamodelProvider<OSGiPackage>
{
    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * @see org.ow2.frascati.parser.api.MetamodelProvider#getEPackage()
     */
    public final OSGiPackage getEPackage()
    {
        return OSGiPackage.eINSTANCE;
    }
}
