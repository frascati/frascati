/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.log.intermediate;

import java.util.ArrayList;
import java.util.List;
import java.util.Dictionary;

import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogService;
import org.osgi.framework.Bundle;

import org.ow2.frascati.osgi.log.api.LogDispatcherService;
import org.ow2.frascati.osgi.log.api.LogWriterService;

/**
 * Intermediary between logger(s) and writer(s)
 */
public class LogIntermediate implements LogListener, LogDispatcherService
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private List<LogWriterService> writers;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * Shape the log message before it is transmitted to LogWriterServices
     * 
     * @param entry
     *            the log entry to build the log message with
     * @return the log message
     */
    private String getLogEntry(LogEntry entry)
    {
        String message = entry.getMessage();
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(getSymbolicName(entry.getBundle()));
        builder.append("]");
        switch (entry.getLevel())
        {
        case LogService.LOG_DEBUG:
            builder.append("[Debug]");
            break;
        case LogService.LOG_ERROR:
            builder.append("[Error]");
            break;
        case LogService.LOG_INFO:
            builder.append("[Info]");
            break;
        case LogService.LOG_WARNING:
            builder.append("[Warning]");
        }
        builder.append(message);
        return builder.toString();
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * Constructor
     */
    public LogIntermediate()
    {
        writers = new ArrayList<LogWriterService>();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.service.log.LogListener#logged(org.osgi.service.log.LogEntry)
     */
    public void logged(LogEntry entry)
    {
        String log = getLogEntry(entry);
        if (log != null)
        {
            for (LogWriterService writer : writers)
            {
                writer.writeOut(log);
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.log.api.LogDispatcherService#add(org.ow2.frascati.osgi.log.api.LogWriterService)
     */
    public void add(LogWriterService writer)
    {
        writers.add(writer);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.log.api.LogDispatcherService#remove(org.ow2.frascati.osgi.log.api.LogWriterService)
     */
    public void remove(LogWriterService writer)
    {
        writers.remove(writer);
    }
    
    public String getSymbolicName(Bundle bundle)
    {
        Dictionary<String,String> headers = bundle.getHeaders();
        String symbolicName = headers.get("Bundle-SymbolicName");
        return symbolicName;
    }
}