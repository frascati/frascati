/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.log.intermediate;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.ow2.frascati.osgi.log.api.LogDispatcherService;
import org.ow2.frascati.osgi.log.api.LogWriterService;

/**
 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
 */
public class LogWriterTracker implements ServiceTrackerCustomizer
{

    private LogDispatcherService logDispatcher;
    private List<LogWriterService> writers;

    /**
     * Constructor
     */
    public LogWriterTracker(LogDispatcherService logDispatcher)
    {
        this.logDispatcher = logDispatcher;
        writers = new ArrayList<LogWriterService>();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework.ServiceReference)
     */
    public Object addingService(ServiceReference reference)
    {
        LogWriterService writer = (LogWriterService) reference.getBundle()
                .getBundleContext().getService(reference);
        synchronized (writer)
        {
            writers.add(writer);
            logDispatcher.add(writer);
        }
        return writer;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework.ServiceReference,
     *      java.lang.Object)
     */
    public void modifiedService(ServiceReference reference, Object service)
    {

        removedService(reference, service);
        addingService(reference);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework.ServiceReference,
     *      java.lang.Object)
     */
    public void removedService(ServiceReference reference, Object service)
    {
        LogWriterService writer = (LogWriterService) service;
        synchronized (writer)
        {
            writers.remove(writer);
            logDispatcher.remove(writer);
        }
    }

    /**
     * Deactivate the tracker
     */
    public void deactivate()
    {
        for (LogWriterService writer : writers)
        {
            synchronized (writer)
            {
                logDispatcher.remove(writer);
            }
        }
        writers.clear();
    }
}
