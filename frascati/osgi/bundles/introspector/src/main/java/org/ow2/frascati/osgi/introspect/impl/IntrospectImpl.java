/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.impl;

import java.util.Stack;
import java.io.File;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.regex.Matcher;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleListener;
import org.osgi.framework.Constants;
import org.osgi.framework.FrameworkEvent;
import org.osgi.framework.FrameworkListener;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceListener;
import org.ow2.frascati.osgi.introspect.api.IntrospectService;

import java.util.logging.Logger;
import java.util.logging.Level;


/**
 * The IntrospectService interface implementation class implements all osgi
 * framework listeners to store and distribute messages
 */
public final class IntrospectImpl implements IntrospectService, BundleListener,
        FrameworkListener, ServiceListener
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private static final Logger LOGGER = Logger.getLogger(IntrospectImpl.class.getCanonicalName());
    /**
     * The spied BundleContext
     */
    private BundleContext context;
    /**
     * The framework name
     */
    private String framework;
    /**
     * The stack of bundles' events messages
     */
    private Stack<String> stackBundle;
    /**
     * The stack of framework's events messages
     */
    private Stack<String> stackFramework;
    /**
     * The stack of services' events messages
     */
    private Stack<String> stackService;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /***
     * Define the BundleContext and register beside it the instance as bundle,
     * framework and service listener
     * 
     * @param context
     *            The BundleContext
     */
    public final void setBundleContext(BundleContext context)
    {
        this.context = context;
        this.context.addBundleListener(this);
        this.context.addFrameworkListener(this);
        this.context.addServiceListener(this);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#getBundles()
     */
    public final long[] getBundles() throws Exception
    {
        Bundle[] bundles =  context.getBundles();
        long[] bundleIdentifiers = new long[bundles.length];
        int identifierIndex = 0;
        for(;identifierIndex<bundleIdentifiers.length;identifierIndex++)
        {
            bundleIdentifiers[identifierIndex] = bundles[identifierIndex].getBundleId();   
        }
        return bundleIdentifiers;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#getFramework()
     */
    public final String getFramework() throws Exception
    {
        StringBuilder builder = new StringBuilder();
        builder.append(context.getProperty(Constants.FRAMEWORK_VENDOR));
        builder.append(" - ");
        builder.append(context.getProperty(Constants.FRAMEWORK_VERSION));
        framework = builder.toString();
        return builder.toString();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#stopBundle(long)
     */
    public final void stopBundle(long id) throws BundleException
    {
        Bundle bundle = context.getBundle(id);
        bundle.stop();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#startBundle(long)
     */
    public final void startBundle(long id) throws BundleException
    {
        Bundle bundle = context.getBundle(id);
        bundle.start();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#uninstall(long)
     */
    public final void uninstall(long id) throws BundleException
    {
        Bundle bundle = context.getBundle(id);
        bundle.uninstall();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#install(java.lang.String)
     */
    public final void install(String location) throws BundleException
    {
        String protocol = "";
        String prefix = "";
        if ("\\".equals(File.separator))
        {
            if (location.startsWith("file:/"))
            {
                // do nothing
            } else if (location.startsWith("file:"))
            {
                location = location.substring(5);
                protocol = "file:";
                prefix = "/";
            } else
            {
                protocol = "file:";
                if (!location.startsWith("/"))
                {
                    prefix = "/";
                }
            }
            try
            {
                Pattern pattern = Pattern.compile("[\\\\]");
                Matcher matcher = pattern.matcher(location);
                location = matcher.replaceAll("/");
            } catch (PatternSyntaxException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
            location = new StringBuilder(protocol).append(prefix)
                    .append(location).toString();
        }
        context.installBundle(location);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#update(long)
     */
    public final void update(long id) throws BundleException
    {
        Bundle bundle = context.getBundle(id);
        bundle.update();
    }

    /**
     * Store a message, built using the ServiceEvent in the stackServcie
     */
    public final void serviceChanged(ServiceEvent arg0)
    {
        if (stackService == null)
            stackService = new Stack<String>();
        StringBuilder builder = new StringBuilder();
        builder.append("[ServiceEvent]\n");
        builder.append("[Type : ");
        switch (arg0.getType())
        {
        case ServiceEvent.MODIFIED:
        {
            builder.append("MODIFIED");
        }
            break;
        case ServiceEvent.REGISTERED:
        {
            builder.append("REGISTERED");
        }
            break;
        case ServiceEvent.UNREGISTERING:
        {
            builder.append("UNREGISTERED");
        }
        }
        builder.append(" ]\n");
        builder.append("[Source : ");
        builder.append(arg0.getSource().toString());
        builder.append(" ]\n");
        stackService.add(builder.toString());
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#getServiceEventsStack()
     */
    public String[] getServiceEventsStack()
    {
        if (stackService == null)
            return new String[0];
        String[] stack = null;
        synchronized (stackService)
        {
            stack = new String[stackService.size()];
            stackService.copyInto(stack);
            stackService.clear();
        }
        return stack;
    }

    /**
     * Store a message built using the FrameworkEvent in the stackFramework
     */
    public final void frameworkEvent(FrameworkEvent arg0)
    {
        if (stackFramework == null)
            stackFramework = new Stack<String>();
        StringBuilder builder = new StringBuilder();
        builder.append("[FrameworkEvent]\n");
        builder.append("[Type : ");
        switch (arg0.getType())
        {
        case FrameworkEvent.INFO:
        {
            builder.append("INFO");
        }
            break;
        case FrameworkEvent.ERROR:
        {
            builder.append("ERROR");
        }
            break;
        case FrameworkEvent.PACKAGES_REFRESHED:
        {
            builder.append("PACKAGES_REFRESHED");
        }
            break;
        case FrameworkEvent.STARTED:
        {
            builder.append("STARTED");
        }
            break;
        case FrameworkEvent.STARTLEVEL_CHANGED:
        {
            builder.append("STARTLEVEL_CHANGED");
        }
            break;
        case FrameworkEvent.WARNING:
        {
            builder.append("WARNING");
        }
        }
        builder.append(" ]\n");
        builder.append("[Source : ");
        builder.append(arg0.getSource().toString());
        builder.append(" ]\n");
        stackFramework.add(builder.toString());
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#getFrameworkEventsStack()
     */
    public String[] getFrameworkEventsStack()
    {
        if (stackFramework == null)
            return new String[0];
        String[] stack = null;
        synchronized (stackFramework)
        {
            stack = new String[stackFramework.size()];
            stackFramework.copyInto(stack);
            stackFramework.clear();
        }
        return stack;
    }

    /**
     * Store a message built using the BundleEvent in the stackBundle
     */
    public final void bundleChanged(BundleEvent arg0)
    {
        if (stackBundle == null)
            stackBundle = new Stack<String>();
        StringBuilder builder = new StringBuilder();
        builder.append("[BundleEvent]\n");
        builder.append("[Type : ");
        switch (arg0.getType())
        {
        case BundleEvent.INSTALLED:
        {
            builder.append("INSTALLED");
        }
            break;
        case BundleEvent.RESOLVED:
        {
            builder.append("RESOLVED");
        }
            break;
        case BundleEvent.STARTED:
        {
            builder.append("STARTED");
        }
            break;
        case BundleEvent.STARTING:
        {
            builder.append("STARTING");
        }
            break;
        case BundleEvent.STOPPED:
        {
            builder.append("STOPPED");
        }
            break;
        case BundleEvent.STOPPING:
        {
            builder.append("STOPPING");
        }
            break;
        case BundleEvent.UNINSTALLED:
        {
            builder.append("UNINSTALLED");
        }
            break;
        case BundleEvent.UNRESOLVED:
        {
            builder.append("UNRESOLVED");
        }
            break;
        case BundleEvent.UPDATED:
        {
            builder.append("UPDATED");
        }
        }
        builder.append(" ]\n");
        builder.append("[Source : ");
        builder.append(arg0.getSource().toString());
        builder.append(" ]\n");
        stackBundle.add(builder.toString());
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.introspect.api.IntrospectService#getBundleEventsStack()
     */
    public String[] getBundleEventsStack()
    {
        if (stackBundle == null)
        {
            return new String[0];
        }
        String[] stack = null;
        synchronized (stackBundle)
        {
            stack = new String[stackBundle.size()];
            stackBundle.copyInto(stack);
            stackBundle.clear();
        }
        return stack;
    }
}
