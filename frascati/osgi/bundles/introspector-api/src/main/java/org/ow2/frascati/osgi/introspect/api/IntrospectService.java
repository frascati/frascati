/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.introspect.api;

/**
 * A service which gives informations about registered bundles in a OSGI Framework
 */
public interface IntrospectService
{
    /**
     * Return the framework's name
     */
    String getFramework() throws Exception;

    /**
     * Return an array of bundles registered in the introspected BundleContext
     */
    long[] getBundles() throws Exception;

    /**
     * Start the bundle which identifier is passed on as parameter
     */
    void startBundle(long id) throws Exception;

    /**
     * Stop the bundle which identifier is passed on as parameter
     */
    void stopBundle(long id) throws Exception;

    /**
     * Update the bundle which identifier is passed on as parameter
     */
    void update(long id) throws Exception;

    /**
     * Uninstall the bundle which identifier is passed on as parameter
     */
    void uninstall(long id) throws Exception;

    /**
     * Start the bundle which location is passed on as parameter
     */
    void install(String location) throws Exception;

    /**
     * Return the stack of services' events messages as an array of strings
     */
    String[] getServiceEventsStack();

    /**
     * Return the stack of framework's events messages as an array of strings
     */
    String[] getFrameworkEventsStack();

    /**
     * Return the stack of bundles' events messages as an array of strings
     */
    String[] getBundleEventsStack();
}
