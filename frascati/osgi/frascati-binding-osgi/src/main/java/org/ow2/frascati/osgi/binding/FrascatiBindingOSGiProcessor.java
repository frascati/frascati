/**
 * OW2 FraSCAti SCA Binding OSGi
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 *
 * Contributor(s): Christophe Munilla
 *
 */
package org.ow2.frascati.osgi.binding;

import java.util.Map;
import java.util.logging.Level;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.osgi.OSGiBinding;
import org.ow2.frascati.osgi.OSGiPackage;
import org.objectweb.fractal.juliac.osgi.OSGiHelper;


/**
 * Bind components using an OSGi Binding.
 * 
 * @author Remi Melisson
 * @version 1.1
 */
public class FrascatiBindingOSGiProcessor extends
        AbstractBindingFactoryProcessor<OSGiBinding>
{
    
    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType,
     *      StringBuilder)
     */
    @Override
    protected final void toStringBuilder(OSGiBinding osgiBinding,
            StringBuilder sb)
    {
        sb.append("osgi:binding.osgi");
        super.toStringBuilder(osgiBinding, sb);
    }

    @Override
    protected final String getBindingFactoryPluginId()
    {
        return "osgi";
    }

    @Override
    protected final void initializeBindingHints(OSGiBinding osgiBinding,
            Map<String, Object> hints)
    {
        String filter = osgiBinding.getFilter();
        String properties = osgiBinding.getProperties();
        BundleContextRevisionItf<?,?> bundleContext = null;
        try
        {
            bundleContext = OSGiHelper.getPlatform().getBundleContext();
            
        } catch(Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        hints.put("filter", filter);
        hints.put("properties", properties);
        hints.put("bundleContext", bundleContext);
    }
 
    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
     */
    public final String getProcessorID()
    {
        return getID(OSGiPackage.Literals.OS_GI_BINDING);
    }
}
