/**
 * OW2 FraSCAti SCA Binding OSGi
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.binding.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mockito;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.processor.ProcessingContextImpl;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.juliac.osgi.PlatformImpl;

/**
 * JUnit test case for OW2 FraSCAti OSGi Binding. 
 */
public class FraSCAtiTest {

    FraSCAti frascati;
    CompositeManager compositeManager;

    @Before
    public void initFraSCAti() throws Exception 
    {
      frascati = FraSCAti.newFraSCAti();
      compositeManager = frascati.getCompositeManager();
    }
    
    @Test
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void processOSGiComposite() throws Exception 
    {      
      OSGiRevisionItf<?,?> osgiRevision = Mockito.mock(OSGiRevisionItf.class); 
      BundleContextRevisionItf bundleContext = Mockito.mock(BundleContextRevisionItf.class);
      Mockito.when(osgiRevision.getBundleContext()).thenReturn(bundleContext);
      Mockito.when(bundleContext.validateFilter(Mockito.anyString())).thenReturn(true);
      Mockito.when(bundleContext.getService(Mockito.anyInt())).thenReturn(new OSGiServiceTest());

      Mockito.when(bundleContext.getServices(OSGiServiceTestItf.class.getCanonicalName(),
      "(&(objectclass=org.ow2.frascati.osgi.binding.test.OSGiServiceTestItf)(&(version=1.2)(name=test)))")).thenReturn(
              new Object[]{new OSGiServiceTest()});

      Mockito.when(bundleContext.getService(OSGiServiceTestItf.class.getCanonicalName(),
      "(&(objectclass=org.ow2.frascati.osgi.binding.test.OSGiServiceTestItf)(&(version=1.2)(name=test)))")).thenReturn(
              new OSGiServiceTest());
      
      PlatformImpl.setOSGiRevision(osgiRevision);      
      ProcessingContext processingContext = new ProcessingContextImpl();
      try 
      {
        Component osgiComponent = compositeManager.processComposite(
                new QName("osgi-component"), processingContext);
        
        OSGiComponentTestItf test = frascati.getService(osgiComponent, 
                "service-test", OSGiComponentTestItf.class);
        
        System.out.println(test.getComponentMessage());
        assertEquals(OSGiServiceTestItf.TEST_MESSAGE,test.getComponentMessage());
        
      } catch(ManagerException me) {
          me.printStackTrace();
          fail();
      }
    }

}
