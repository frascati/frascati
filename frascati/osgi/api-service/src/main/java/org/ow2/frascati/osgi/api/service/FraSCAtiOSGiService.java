/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.api.service;

import java.io.File;
import java.net.URL;

/**
 * The FraSCAtiOSGiService interface
 */
public interface FraSCAtiOSGiService
{
    /**
     * Load an SCA component embedded in the resource which URL is passed on as a
     * parameter
     * 
     * @param resourceURL
     * @param compositeName
     *            the composite file name
     * @throws FraSCAtiOSGiNotFoundCompositeException
     *             ; if no component can be loaded
     */
    String loadSCA(URL resourceURL, String compositeName)
            throws FraSCAtiOSGiNotFoundCompositeException;

    /**
     * @param bundleId
     */
    void unloadSCA(long bundleId);

    /**
     * Launch a Runnable service of a component beforehand registered with the suitable
     * loadSCA method
     * 
     * @param compositeName
     *            the name of the component
     * @param serviceName
     *            the name of the service
     * @throws FraSCAtiOSGiNotFoundCompositeException
     *             ; if the component cannot be found
     * @throws FraSCAtiOSGiNotFoundServiceException
     *             ; if the service cannot be found
     * @throws FraSCAtiOSGiNotRunnableServiceException
     *             ; if the service is not a Runnable one
     */
    void launch(String compositeName, String serviceName)
            throws FraSCAtiOSGiNotFoundCompositeException,
            FraSCAtiOSGiNotFoundServiceException,
            FraSCAtiOSGiNotRunnableServiceException;

    /**
     * Launch a Runnable service if it exists and beforehand registered with the suitable
     * loadSCA method
     * 
     * @param compositeName
     *            the name of the component
     * @throws FraSCAtiOSGiNotFoundCompositeException
     *             ; if the component cannot be found
     * @throws FraSCAtiOSGiNotFoundServiceException
     *             ; if no service can be found in the component
     * @throws FraSCAtiOSGiNotRunnableServiceException
     *             ; if no Runnable service can be found
     */
    void launch(String compositeName)
            throws FraSCAtiOSGiNotFoundCompositeException,
            FraSCAtiOSGiNotRunnableServiceException,
            FraSCAtiOSGiNotFoundServiceException;

    /**
     * Return the cache directory used by the service
     * 
     * @return The cache directory
     */
    File getCacheDir();
    
    /**
     * Return the name of the composite which is loading or null if no loading
     * process is running 
     *  
     * @return
     *          the name of the composite 
     */
    String loading();
    
    
    /**
     * Return true if the composite which name is passed on as a parameter
     * has been loaded 
     * 
     * @param name
     *          the name of the composite 
     * @return
     *          true if the composite has been loaded
     *          false otherwise
     */
    boolean loaded(String name);
}
