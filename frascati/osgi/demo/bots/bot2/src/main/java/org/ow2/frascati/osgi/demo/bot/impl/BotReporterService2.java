/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.demo.bot.impl;

import org.ow2.frascati.osgi.demo.bot.api.BotReportService;

/**
 * Implementation of the BotReportService
 */
public class BotReporterService2 implements BotReportService
{

    /**
     * the bot's identifier
     */
    private String id = "bot-2";

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.demo.bot.api.BotReportService#getBotId()
     */
    public String getBotId()
    {
        return id;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.demo.bot.api.BotReportService#getReport()
     */
    public String getReport()
    {
        return "report from bot-2";
    }
}
