/**
 * OW2 FraSCAti OSGi
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Munillq
 *
 * Contributor(s):
 */
package org.ow2.frascati.osgi.demo.impl;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.osgi.demo.api.ReportRegisterer;

/**
 * Return a page web containing all registered reports
 */
@Scope("COMPOSITE")
public class ReportServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@Reference(name = "report-registerer")
	ReportRegisterer registerer;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public final void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException 
            {
              response.setContentType("text/html");
              PrintWriter out = response.getWriter();
              out.println("<html><body>" + registerer.getRegisteredReports()  + "</body></html>");
              out.close();
            }

}
