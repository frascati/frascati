/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.demo.impl;

import java.util.ArrayList;
import java.util.List;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.osgi.demo.api.ReportRegisterer;
import org.ow2.frascati.osgi.demo.bot.api.BotReportService;

/**
 * Implementation of the ReportRegisterer interface
 */
@Scope("COMPOSITE")
public class ReportRegistererImpl implements ReportRegisterer
{

    @Reference(name="bot-connector")
    BotReportService botReportService;
    
    private List<String> reports;
    
    /**
     * Constructor
     */
    public ReportRegistererImpl()
    {
        reports = new ArrayList<String>();
    }
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.demo.api.ReportRegisterer#getRegisteredReports()
     */
    public String getRegisteredReports()
    {
        try
        {
            String reportEntry = new StringBuilder("<div>").append("ID : ").append(
                botReportService.getBotId()).append(
                "<p/>").append(botReportService.getReport()).append("</div>").toString();
        
            reports.add(reportEntry);
            
        } catch(NullPointerException e)
        {
            System.out.println("no bot connected");
        }
        String report = "";
        
        for(String rap : reports)
        {
            report = report + rap;
        }        
        return report;
    }

}
