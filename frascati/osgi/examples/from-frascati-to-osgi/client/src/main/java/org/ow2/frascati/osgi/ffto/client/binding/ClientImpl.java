/**
 * OW2 FraSCAti OSGi Examples: From FraSCAti server to OSGi client 
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 * 
 * Contributor(s): 
 *                 
 */
package org.ow2.frascati.osgi.ffto.client.binding;

import java.lang.reflect.InvocationTargetException;
import org.ow2.frascati.osgi.ffto.server.binding.HelloService;

public class ClientImpl implements Runnable
{
    private HelloService printService;
    private int count = 5;

    public ClientImpl()
    {
        System.out.println("CLIENT created");
    }

    public void setPrintService(HelloService service)
    {
        this.printService = service;
    }

    public void start()
    {
        for (int i = 0; i < count; i++)
        {
            run();
        }
    }

    public void run()
    {
        System.out.println("Call the service...");
        printService.print("hello world - first OSGI Binding");
    }
}
