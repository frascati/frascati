/**
 * OW2 FraSCAti OSGi Examples: From FraSCAti server to OSGi client 
 * Copyright (c) 2010 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.ffto.client.binding;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.ow2.frascati.osgi.ffto.server.binding.HelloService;
import java.util.logging.Logger;
import java.util.logging.Level;

public class Activator implements BundleActivator
{
    private ServiceReference reference;
    
    private static final Logger LOGGER = Logger.getLogger(Activator.class.getCanonicalName());
    
    public final void start(BundleContext context)
    {
        Bundle bundle = context.getBundle();
        ClientImpl client = new ClientImpl();
        Hashtable/*<String,String>*/ properties = new Hashtable/*<String,String>*/();            
        ServiceReference[] references = null;        
        try
        {
            //ServiceReference cannot be retrieved using the 'getServiceReferences' method with Equinox
            //'getAllServiceReferences' doesn't exist in OSGi revision 3
            references = (ServiceReference[]) BundleContext.class.getDeclaredMethod("getAllServiceReferences",
                    new Class<?>[]{String.class,String.class}).invoke(
                            context, new Object[]{ "org.ow2.frascati.osgi.ffto.server.binding.HelloService",
                                    "(&(version=1.0)(name=MyFirstBinding))"});
            
        } catch(NoSuchMethodException nsme)
        {
            try
            {
                references = context.getServiceReferences(
                    "org.ow2.frascati.osgi.ffto.server.binding.HelloService",
                    "(&(version=1.0)(name=MyFirstBinding))");
            
            } catch (InvalidSyntaxException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }            
        }  catch(Exception e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }         
        if (references != null)
        {
            reference = references[0];
            HelloService service = (HelloService)context.getService(reference);
            client.setPrintService(service);            
            client.start();
            
        } else
        {
            System.err.println("No HelloService found - Client cannot be created");
        }        
        Dictionary/* <?, ?> */headers = bundle.getHeaders();
        String name = (String) headers.get("Bundle-Name");
        System.err.println("Bundle <" + name + "> started.");
    }

    public final void stop(BundleContext context)
    {
        if (reference != null)
        {
            context.ungetService(reference);
        }
        Bundle bundle = context.getBundle();
        Dictionary/* <?, ?> */dict = bundle.getHeaders();
        String name = (String) dict.get("Bundle-Name");
        System.err.println("Bundle <" + name + "> stopped.");
    }
}
