/**
 * OW2 FraSCAti Examples: Fscript reconfiguration
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.reconfig;

import org.junit.Test;
import org.objectweb.fractal.fscript.jsr223.FScriptEngineFactoryProxy;
import org.ow2.frascati.test.FraSCAtiTestCase;

public class ReconfigTestCase
     extends FraSCAtiTestCase
{
  public ReconfigTestCase()
  {
	  System.setProperty( FScriptEngineFactoryProxy.SCRIPT_ENGINE_FACTORY_PROPERTY_NAME,
	  "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory" );
	  
  }

  @Test
  public final void testService()
  {
    getService(Runnable.class, "r").run();
  }
}
