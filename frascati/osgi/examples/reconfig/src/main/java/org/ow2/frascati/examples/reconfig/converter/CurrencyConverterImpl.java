/**
 * OW2 FraSCAti Examples : Fscript reconfiguration
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.reconfig.converter;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;

/** 
 * The Currency converter service implementation. 
 */
@Scope(value="COMPOSITE")
public class CurrencyConverterImpl
  implements CurrencyConverter
{
    /** Dollar exchange rate 1 euro = dollarExchangeRate$ */
    private double dollarExchangeRate;
    /** Mexican Peso exchange rate 1 euro = pesoExchangeRate$ */    
    private double pesoExchangeRate;
    
    /** Default constructor. */
    public CurrencyConverterImpl()
    {
        System.out.println("Currency converter created.");
    }
    
    /**
     * Set the dollarExchangeRate property.
     * 
     * @param rate - the new value of the dollar exchange rate.
     */
    @Property
    public final void setDollarExchangeRate(final double rate)
    {
        System.out.println("Setting Dollar exchange rate to '" + rate + "'.");
        this.dollarExchangeRate = rate;
    }

    /**
     * Set the PesoExchangeRate property.
     * 
     * @param rate - the new value of the peso exchange rate.
     */
    @Property
    public final void setPesoExchangeRate(final double rate)
    {
        System.out.println("Setting Peso exchange rate to '" + rate + "'.");
        this.pesoExchangeRate = rate;
    }

    // Implementation of the CurrencyConverter service
    
    public final double dollarToEuro(double value)
    {
        return value * dollarExchangeRate;
    }

    public final double euroToDollar(double value)
    {
        return value / dollarExchangeRate;
    }

    public final double euroToPeso(double value)
    {
        return value * pesoExchangeRate;
    }

    public final double pesoToEuro(double value)
    {
        return value / pesoExchangeRate;
    }
}
