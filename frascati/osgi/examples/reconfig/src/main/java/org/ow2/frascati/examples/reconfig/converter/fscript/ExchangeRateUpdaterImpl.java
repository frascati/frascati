/**
 * OW2 FraSCAti Examples : Fscript reconfiguration
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.reconfig.converter.fscript;

import javax.script.Bindings;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;

import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.objectweb.fractal.fscript.jsr223.FScriptEngineFactoryProxy;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.osoa.sca.annotations.Property;
import org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory;

/**
 * A SCA component used to dynamically reconfigure another SCA component: the
 * currency converter.
 * 
 * The FScript plugin <b>MUST</b> be activated!
 * 
 * @author Christophe Demarey.
 */
public class ExchangeRateUpdaterImpl implements ExchangeRateUpdater
{
    /**
     * The required composite manager.
     */
    @Reference(name = "classloader-manager")
    private ClassLoaderManager classLoaderManager;
    
    @Property
    private String reconfigurationScript;
    
    public final void updateDollarExchangeRate(double newValue) throws ScriptException
    {   
        System.setProperty(FScriptEngineFactoryProxy.SCRIPT_ENGINE_FACTORY_PROPERTY_NAME,
                "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory");      

        ScriptEngineManager manager = new ScriptEngineManager(classLoaderManager.getClassLoader());
        
        new FraSCAtiScriptEngineFactory().addDomainToContext( manager.getBindings() ); // Add a global var for domain
        InvocableScriptEngine engine = (InvocableScriptEngine) manager.getEngineByExtension("fscript");
        
        Bindings ctx = engine.createBindings();
        
        engine.eval("converter = $domain/scachild::reconfig/scachild::converter/scachild::currency-converter;", ctx);
        engine.eval("rate = $converter/scaproperty::dollarExchangeRate;", ctx);
        engine.eval("set-value($rate,"+newValue+")", ctx);
    }
}
