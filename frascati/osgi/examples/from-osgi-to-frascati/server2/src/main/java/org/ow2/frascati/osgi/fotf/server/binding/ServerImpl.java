/**
 * OW2 FraSCAti OSGi Examples: From OSGi server to FraSCAti client 
 * Copyright (c) 2010 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.fotf.server.binding;

public class ServerImpl implements GoodByeService
{
    // --------------------------------------------------------------------------
    // Default constructor
    // --------------------------------------------------------------------------
    public ServerImpl()
    {
        System.out.println("SERVER created");
    }

    // --------------------------------------------------------------------------
    // Implementation of the GooByeService interface
    // --------------------------------------------------------------------------
    @Deprecated
    public final void print(final String msg)
    {
        printPlus(msg);
    }

    public final void printPlus(final String msg)
    {
        System.out.println("Server: begin printing...");
        System.out.println(msg);
        System.out.println("Server: print done.");
    }
}
