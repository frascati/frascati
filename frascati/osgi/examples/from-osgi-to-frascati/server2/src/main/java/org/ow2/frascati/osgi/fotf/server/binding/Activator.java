/**
 * OW2 FraSCAti OSGi Examples: From OSGi server to FraSCAti client 
 * Copyright (c) 2010 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.fotf.server.binding;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator
{
    private ServiceRegistration registration;
    private ServerImpl server = null;

    public final void start(BundleContext context)
    {
        Bundle bundle = context.getBundle();
        server = new ServerImpl();
        Hashtable/* <String,String> */properties = new Hashtable/* <String,String> */();
        Dictionary/* <?, ?> */headers = bundle.getHeaders();
        String name = (String) headers.get("Bundle-Name");
        System.err.println("Bundle <" + name + "> started.");
        properties.put("version", "1.1");
        properties.put("name", "GoodByeService");
        registration = context.registerService(GoodByeService.class.getName(),
                server, properties);
    }

    public final void stop(BundleContext context)
    {
        Bundle bundle = context.getBundle();
        Dictionary/* <?, ?> */dict = bundle.getHeaders();
        String name = (String) dict.get("Bundle-Name");
        System.err.println("Bundle <" + name + "> stopped.");
        // Bundle[] bundles = registration.getReference().getUsingBundles();
        registration.unregister();
        server = null;
    }
}
