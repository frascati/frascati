============================================================================
OW2 FraSCAti OSGi
Copyright (c) 2011 - 2012 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Christophe Munilla

============================================================================

FraSCAti & OSGi:
----------------

  Knopflerfish, Felix, Equinox, JBoss-osgi and Concierge are the functional OSGi implementations
  
  
 Compilation with Maven:
 -----------------------
 
    mvn install
    
   
 Binding OSGi use:
 ----------------
  
   "filter" attribute for binded interface : 
   
        syntax : 
        
               cf. http://www.ietf.org/rfc/rfc1960.txt
               
               [&,|,!] have to be respectively replaced by [and,or,not]


   "properties" attribute for exported interface :
   
       syntax : 

             properties := property[,properties]
             property   := <key>=<value>

 
 OSGi in FraSCAti:
 ----------------
 
      Execution with Maven:
       ---------------------
       
        See osgi-in-frascati/osgi tests to use it

	FraSCAti in OSGi:
	----------------
   		The frascati-modules subproject contains bindings and implementations bundles usable with FraSCAti 
   		in OSGi. The frascati-core bundle contains shared artifacts and FraSCati core libraries. It is 
   		needed to launch a FraSCati instance in an OSGi environment. To define which bundles are deployed 
   		use the install_bundles.xml file which can be found in the osgi framework implementation's resources 
   		directory. A property entry is jar file name and boolean value colon-separated. The boolean value 
   		define if the bundle has to be installed (look at the install_bundles.xml file as an example).
   		implementation-osgi and binding-osgi have been added to the FraSCAti core libraries and are 
   		automatically available;
		
    		POM headers : 
    		-------------
    		
    		Eight new pom headers have been defined : 
    		
    		Frascati-Fragment : Is the bundle a FraSCAti Fragment (true) ?
			Frascati-Bootstrap : Define a bootstrap class name
			Frascati-BootstrapOverwrite : Define overwritten bootstrap class name(s)
			Frascati-FragmentNeed : Needed FraSCAti's fragment(s)
			Frascati-BundleNeed : Needed Bundle(s)
			Frascati-Unmanaged : List of embedded resources to not handle
			Frascati-JTCompil : List of embedded resources to cache for compilation process
			Frascati-Process : List of embedded composite files names to process 
    		
    		Available bundles are : 
    		----------------------------
    			- binding-rmi
    			- binding-jsonrpc
    			- binding-ws
    			- binding-http
    			- binding-jms
    			- binding-rest
    			- frascati-explorer
    			- frascati-introspection
    			- frascati-velocity
				- frascati-web-explorer
    			- frascati-fscript
    			- frascati-gcs
    			- frascati-native
    			- implementation-scripts
    			- implementation-xquery
    			- implementation-spring
    			- implementation-bpel
    			- implementation-widget
    		
    		Some bundles are still in progress :
    		----------------------------------
    			- frascati-introspection-fscript
    		
    		Problems exist using bundles together :   
    		-------------------------------------
    			- implementation-xquery cannot be used with implementation-bpel because of 
    			  incompatible net-sf-saxon libraries
    			- frascati-introspection used with the frascati-explorer causes explorer's frame 
    			  not shown 
    			- implementation-bpel interfere with implementation-script : if the bpel engine has
    			  been instantiated (explorer selection or example loaded) the script example causes 
    			  an error 
    			  
    		Todo :   
    	
    			- finish unusable bundles (fscripts-introspection,..)
    		
    		Execution with Maven:
    		---------------------
    		
        See frascati-in-osgi/osgi tests to use it
    		
    		  
    		  ps : put the boolean value of the install_bundles.xml file's entry to true
