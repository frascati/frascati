/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.resource.jbfile;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.logging.Level;

import org.ow2.frascati.util.reflect.ReflectionHelper;
import org.ow2.frascati.util.resource.AbstractResource;

/**
 * JBoss VirtualFile Resource handler 
 */
public class Resource extends AbstractResource
{

    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    private ReflectionHelper vfile;
//    private Closeable vfhandle;

    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
//    /**
//     * @param virtualFile
//     * @return
//     */
//    private final Closeable mount(ReflectionHelper virtualFile)
//    {
//
//        File archiveFile = (File) virtualFile.invoke("getPhysicalFile");
//        ReflectionHelper tempFileProvider = new ReflectionHelper(
//                virtualFile.getReflectedClass().getClassLoader(),"org.jboss.vfs.TempFileProvider");
//        tempFileProvider.set(tempFileProvider.invokeStatic("create",
//                new Class<?>[] { String.class, ScheduledExecutorService.class }, 
//                new Object[] { "tmp", Executors.newScheduledThreadPool(2) }));
//        ReflectionHelper VFS = new ReflectionHelper(
//                virtualFile.getReflectedClass().getClassLoader(), "org.jboss.vfs.VFS");
//        Closeable handle = (Closeable) VFS.invokeStatic("mountZip",
//                new Class<?>[] { File.class, virtualFile.getReflectedClass(),
//                        tempFileProvider.getReflectedClass() },
//                new Object[] { archiveFile, virtualFile.get(),
//                        tempFileProvider.get() });
//        return handle;
//    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * 
     * @param resourceParent
     */
    public Resource(AbstractResource resourceParent)
    {
        super(resourceParent);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#manage(java.lang.Object)
     */
    @Override
    public boolean manage(Object resourceObject)
    {
        if ("org.jboss.vfs.VirtualFile".equals(
                resourceObject.getClass().getCanonicalName()))
        {
            vfile = new ReflectionHelper(resourceObject.getClass());
            vfile.set(resourceObject);
            
        } else if (resourceObject instanceof URL)
        {
            URL url = (URL) resourceObject;
            Object content = null;
            try
            {
                content = url.getContent();
                
            } catch (IOException e)
            {
                // log.log(Level.INFO,e.getMessage());
                return false;
            }
            if ("org.jboss.vfs.VirtualFile".equals(
                    content.getClass().getCanonicalName()))
            {
                vfile = new ReflectionHelper(content.getClass());
                vfile.set(content);
                
            } else
            {
                return false;
            }
        } else
        {
            return false;
        }
        if (vfile == null || vfile.get() == null)
        {
            return false;
        }
        resourceURL = (URL) vfile.invoke("toURL");
//        vfhandle = mount(vfile);
//        File pfile = (File) vfile.invoke("getPhysicalFile");
//        try
//        {
//            resourceURL = pfile.toURI().toURL();
//            
//        } catch (Exception e)
//        {
//            log.log(Level.INFO, e.getMessage());
//            resourceURL = (URL) vfile.invoke("toURL");
//        }
        return true;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.util.resource.AbstractResource#getBaseURL()
     */
    @Override 
    public URL getBaseURL()
    {
        return resourceURL;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceEntries()
     */
    @Override
    protected Enumeration<String> getResourceEntries()
    {
        return new Enumeration<String>()
        {
            private Stack<String> stack = new Stack<String>();
            private Stack<ReflectionHelper> searchPaths = null;

            private void buildEntries(boolean first)
            {
                ReflectionHelper currentFile =  searchPaths.pop();
                List<?> entries = (List<?>) currentFile.invoke("getChildren");
                if (entries != null)
                {
                    int n = 0;
                    while (n < entries.size())
                    {
                        ReflectionHelper entry = new ReflectionHelper(
                                vfile.getReflectedClass());
                        entry.set(entries.get(n++));
                        String entryPath = (String) entry.invoke(
                                "getPathNameRelativeTo", 
                                new Class<?>[]{ entry.getReflectedClass() },
                                new Object[] { vfile.get() });
                        if (filter.accept(null, entryPath))
                        {
                            if ((Boolean) entry.invoke("isDirectory"))
                            {
                                searchPaths.push(entry);
                            } else
                            {
                                stack.push(entryPath);
                            }
                        }
                    }
                }
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#hasMoreElements()
             */
            public boolean hasMoreElements()
            {

                boolean first = false;
                if (searchPaths == null)
                {
                    searchPaths = new Stack<ReflectionHelper>();
                    searchPaths.push(vfile);
                    first = true;
                }
                while (stack.size() == 0 && searchPaths.size() > 0)
                {
                    buildEntries(first);
                }
                return stack.size() > 0;
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#nextElement()
             */
            public String nextElement()
            {

                return stack.pop();
            }
        };
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getComparable()
     */
    @Override
    protected String getComparable()
    {

        return (String) vfile.invoke("getPathName");
    }

//    /**
//     * {@inheritDoc}
//     * 
//     * @see java.lang.Object#finalize()
//     */
//    @Override
//    public void finalize()
//    {
//        try
//        {
//            if (vfhandle != null)
//            {
//                vfhandle.close();
//            }
//        } catch (IOException e)
//        {
//            log.log(Level.WARNING,e.getMessage(),e);
//        }
//    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#getResourceObject()
     */
    @Override
    public Object getResourceObject()
    {
        return vfile;
    }
}