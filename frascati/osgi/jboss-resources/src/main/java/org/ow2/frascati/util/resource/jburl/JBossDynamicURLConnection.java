/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.jburl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;

/**
 * An URLConnection to access a JBoss resource dynamically
 */
public class JBossDynamicURLConnection extends URLConnection
{
    /**
     * Logger
     */
    private static Logger logger = Logger.getLogger(
            JBossDynamicURLConnection.class.getCanonicalName());
    
    private String entryPath;
    private BundleRevisionItf bundle;

    /**
     * Constructor
     * 
     * @param url
     */
    protected JBossDynamicURLConnection(URL url)
    {
        super(url);
    }
    
    /**
     * Constructor
     * 
     * @param bundle
     * @param entryPath
     * @param url
     */
    public JBossDynamicURLConnection(BundleRevisionItf bundle, String entryPath , URL url)
    {
        this(url);
        this.bundle = bundle;
        this.entryPath = entryPath;
    }

    /* (non-Javadoc)
     * @see java.net.URLConnection#connect()
     */
    @Override
    public void connect() throws IOException
    {
        logger.log(Level.INFO,"JBossDynamicURLConnection connected");   
    }

    /* (non-Javadoc)
     * @see java.net.URLConnection#getInputStream()
     */
    @Override
    public InputStream getInputStream()
    {
        try
        {
            URL entryURL = bundle.getEntry(entryPath) ;
            if(entryURL != null)
            {
                return entryURL.openStream();
            }
            
        } catch (IOException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }
    
    /* (non-Javadoc)
     * @see java.net.URLConnection#getContent()
     */
    @Override
    public Object getContent()
    {
        try
        {
            URL entryURL = bundle.getEntry(entryPath) ;
            if(entryURL != null)
            {
                return entryURL.getContent();
            }
            
        } catch (IOException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        } 
        return null;
    }
}
