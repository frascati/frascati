/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.resource.jbfilter;

import java.io.File;
import java.util.List;

import org.ow2.frascati.util.resource.ResourceFilter;

public class ResourceFilterImpl implements ResourceFilter
{

    private List<String> unmanagedResources;
    private List<String> embeddedJars;

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.FileFilter#setEmbeddedAsResource(java.util.
     *      List)
     */
    public void setUnmanagedResources(List<String> unmanagedResources)
    {
        this.unmanagedResources = unmanagedResources;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.FileFilter#setEmbeddedjars(java.util.List)
     */
    public void setEmbeddedjars(List<String> embeddedJars)
    {
        this.embeddedJars = embeddedJars;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(File f, String name)
    {
        String jarName = name;
        
        /*
         * Research a jar extension in the entry name Use to identify embedded
         * jar files when Jboss osgi implementation is used
         */
        int positionExtension = jarName.lastIndexOf(".jar/");
        if (positionExtension > -1)
        {
            String jarPath = jarName.substring(0, positionExtension + 4);

            if (!embeddedJars.contains(jarPath))
            {
                jarName = jarPath;
                
            } else
            {
                return false;
            }
        }
        if (unmanagedResources != null)
        {
            int current = 0;
            int size = unmanagedResources.size();
            
            for(;current<size;current++)
            {
                if (jarName.endsWith(unmanagedResources.get(current)))
                {
                    return true;
                }
            }
        }
        if (jarName.endsWith(".jar"))
        {
            embeddedJars.add(jarName);
            return false;
        }
        return true;
    }
};
