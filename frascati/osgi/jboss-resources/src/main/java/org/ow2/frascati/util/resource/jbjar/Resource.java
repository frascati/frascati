/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.util.resource.jbjar;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.jar.JarFile;
import java.util.logging.Level;

import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.frascati.FrascatiURLConnection;
import org.ow2.frascati.util.resource.frascati.Handler;

public class Resource extends org.ow2.frascati.util.resource.jar.Resource
{
    /**
     * @param resourceParent
     */
    public Resource(AbstractResource resourceParent)
    {
        super(resourceParent);
    }

    @Override
    protected JarFile getJarFile() throws IOException
    {
         try
         {
             File jarFile = (File)((FrascatiURLConnection)resourceURL.openConnection()
             ).translate(false).getContent();
             
             URL jarFileURL = new URL("jar","", jarFile.toURI().toURL().toExternalForm()+"!/");

             return ((JarURLConnection)jarFileURL.openConnection()).getJarFile();
             
         } catch (IOException e)
         {
             e.printStackTrace();
         }
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @throws IOException 
     * @throws MalformedURLException 
     * 
     * @see org.ow2.frascati.util.resource.AbstractResource#getBaseURL()
     */
    @Override 
    public URL getBaseURL()
    {
        return resourceURL;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.resource.AbstractResource#manage(java.lang.Object)
     */
    @Override
    public boolean manage(Object resourceObject)
    {
        boolean managed = false;
        
        if (resourceObject instanceof URL)
        {
            URL resourceUrl = (URL) resourceObject;
            try
            {
                URLConnection connection = resourceUrl.openConnection();
                if(FrascatiURLConnection.class.isAssignableFrom(connection.getClass()))
                {
                    Object content = ((FrascatiURLConnection)connection).translate(false).getContent();
                    
                    if(File.class.isAssignableFrom(content.getClass()) && 
                            resourceUrl.toExternalForm().endsWith(".jar"))
                    {    
                        managed = true;
                        String jarResourcePath = resourceUrl.getFile();
                        jarName = IOUtils.separatorTrim(jarResourcePath);
                        jarName = IOUtils.pathLastPart(jarName);
                        resourceURL = new URL("frascati",resourceUrl.getHost()+"#"+jarName,-1,"",
                                new Handler());
                    }
                } 
            } catch(IOException ioe)
            {
                log.log(Level.WARNING,ioe.getMessage(),ioe);
            }
        }
        return managed;
    }
}
