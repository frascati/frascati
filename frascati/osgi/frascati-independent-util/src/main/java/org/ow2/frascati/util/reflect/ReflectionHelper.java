/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Helper for reflection call
 */
public class ReflectionHelper
{
    private Class<?> reflectedClass;
    private String className;
    private Object reflected;
    private ClassLoader loader;
    private boolean unknownClass;
    
    /**
     * The Logger
     */
    private static final Logger LOGGER = Logger.getLogger(
            ReflectionHelper.class.getCanonicalName());

    /**
     * Constructor
     * 
     * @param classLoader
     *          the ClassLoader used for instantiation and/or reflection call(s)
     * @param className
     *          the Class's name for instantiation and/or reflection call(s)
     */
    public ReflectionHelper(ClassLoader classLoader, String className)
    {
        if(classLoader ==null)
        {
            this.loader = Thread.currentThread().getContextClassLoader();
            
        } else
        {
            this.loader = classLoader;
        }
        this.className = className;
        if(className != null)
        {
            reflectedClass = ReflectionUtil.forName(classLoader, className);
        }
        if (reflectedClass == null)
        {
            checkClass();
            unknownClass = true;
            
        } else
        {
            unknownClass = false;
        }
    }

    /**
     * Constructor
     * 
     * @param clazz
     *          the Class object for instantiation and/or reflection call(s)
     *          
     */
    public ReflectionHelper(Class<?> clazz)
    {
        reflectedClass = clazz;
        if (reflectedClass == null)
        {
            checkClass();
            unknownClass = true;
            this.className = null;
            loader = Thread.currentThread().getContextClassLoader();
            
        } else
        {
            unknownClass = false;
            this.className = clazz.getCanonicalName();
            loader = clazz.getClassLoader();
        }
    }

    /**
     * Does the Class object exist ? 
     */
    public boolean isClassKnown()
    {
        return !unknownClass;
    }

    /**
     * Return the object instantiated using the defines ClassLoader, class 's name
     * and the empty constructor of the class
     */
    public Object newInstance()
    {
        if (!checkClass())
        {
            return null;
        }
        return newInstance((Class<?>[]) null, (Object[]) null);
    }

    /**
     * Return the object instantiated using the defined ClassLoader, the name of the class and 
     * a parameterized constructor
     *
     * @param classes
     *          The array of object's classes of the parameterized constructor
     * @param objects
     *          The array of the parameters used for instantiation
     * @return
     */
    public Object newInstance(Class<?>[] classes, Object[] objects)
    {
        if (!checkClass())
        {
            return null;
        }
        reflected = ReflectionUtil.construct(reflectedClass, classes, objects);
        return reflected;
    }

    /**
     * @param methodName
     * @return
     */
    public Object invoke(String methodName)
    {
        if (!checkClass())
        {
            return null;
        }
        return invoke(methodName, (Class<?>[]) null, (Object[]) null);
    }

    /**
     * @param methodName
     * @return
     */
    public Object invokeStatic(String methodName)
    {
        if (!checkClass())
        {
            return null;
        }
        return invokeStatic(methodName, (Class<?>[]) null, (Object[]) null);
    }

    /**
     * @param methodName
     * @param className
     * @return
     */
    public Object invokeInherited(String methodName, String className)
    {
        if (!checkClass())
        {
            return null;
        }
        return invokeInherited(methodName, className, (Class<?>[]) null,
                (Object[]) null);
    }

    /**
     * @param methodName
     * @param classes
     * @param objects
     * @return
     */
    public Object invoke(String methodName, Class<?>[] classes, Object[] objects)
    {
        if (!checkClass())
        {
            return null;
        }
        Method method = ReflectionUtil.getMethod(reflectedClass, methodName,
                classes);
        if (method != null)
        {
            return ReflectionUtil.invoke(reflected, method, objects);
        }
        LOGGER.log(Level.WARNING,"the method " + methodName + " doesn't exist in "
                + reflectedClass.getCanonicalName());
        return null;
    }

    /**
     * @param methodName
     * @param classes
     * @param objects
     * @return
     */
    public Object invokeStatic(String methodName, Class<?>[] classes,
            Object[] objects)
    {
        if (!checkClass())
        {
            return null;
        }
        Method method = ReflectionUtil.getMethod(reflectedClass, methodName,
                classes);
        if (method != null)
        {
            return ReflectionUtil.invoke(null, method, objects);
        }
        return null;
    }

    /**
     * @param methodName
     * @param className
     * @param classes
     * @param objects
     * @return
     */
    public Object invokeInherited(String methodName, String className,
            Class<?>[] classes, Object[] objects)
    {
        if (!checkClass())
        {
            return null;
        }
        Class<?> inheritedClass = ReflectionUtil.forName(loader, className);
        Method method = ReflectionUtil.getMethod(inheritedClass, methodName,
                classes);
        if (method != null)
        {
            return ReflectionUtil.invoke(reflected, method, objects);
        }
        return null;
    }

    /**
     * @param fieldName
     * @param value
     */
    public void set(String fieldName, Object value)
    {
        if (!checkClass())
        {
            return;
        }
        Field field = ReflectionUtil.getField(reflectedClass, fieldName);
        try
        {
            field.set(reflected, value);
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * @param className
     * @param fieldName
     * @param value
     */
    public void setInherited(String className, String fieldName, Object value)
    {
        if (!checkClass())
        {
            return;
        }
        Class<?> inheritedClass = ReflectionUtil.forName(loader, className);
        Field field = ReflectionUtil.getField(inheritedClass, fieldName);
        try
        {
            field.set(reflected, value);
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * @param fieldName
     * @param value
     */
    public void setStatic(String fieldName, Object value)
    {
        if (!checkClass())
        {
            return;
        }
        Field field = ReflectionUtil.getField(reflectedClass, fieldName);
        try
        {
            field.set(null, value);
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * Return the class object associated to the ReflectionHelper instance
     */
    public Class<?> getReflectedClass()
    {
        return reflectedClass;
    }

    /**
     * 
     * @param fieldName
     * @return
     */
    public Object get(String fieldName)
    {
        if (!checkClass())
        {
            return null;
        }
        Field field = ReflectionUtil.getField(reflectedClass, fieldName);
        if (field == null)
        {
            return null;
        }
        try
        {
            return field.get(reflected);
            
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * @param fieldName
     * @return
     */
    public Object getStatic(String fieldName)
    {
        if (!checkClass())
        {
            return null;
        }
        Field field = ReflectionUtil.getField(reflectedClass, fieldName);
        if (field == null)
        {
            return null;
        }
        try
        {
            return field.get(null);
            
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * @param className
     * @param fieldName
     * @return
     */
    public Object getInherited(String className, String fieldName)
    {
        if (!checkClass())
        {
            return null;
        }
        Class<?> inheritedClass = ReflectionUtil.forName(loader, className);
        Field field = ReflectionUtil.getField(inheritedClass, fieldName);
        if (field == null)
        {
            return null;
        }
        try
        {
            return field.get(reflected);
            
        } catch (IllegalArgumentException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IllegalAccessException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * Return the wrapped reflected class instance
     */
    public Object get()
    {
        return reflected;
    }

    /**
     * A reflected class instance on which make reflection calls
     * 
     * @param reflected
     *          an instance of the reflected class to wrap
     */
    public void set(Object reflected)
    {
        if (!checkClass())
        {
            if(reflected != null)
            {
                this.reflectedClass = reflected.getClass();
                this.className = this.reflectedClass.getCanonicalName();
                this.loader = this.reflectedClass.getClassLoader();
                this.unknownClass = false;
            }
            else
            {
                return;
            }
        }
        this.reflected = reflectedClass.cast(reflected);
    }
    
    private boolean checkClass()
    {
        if (unknownClass)
        {
            if(LOGGER.isLoggable(Level.WARNING))
            {
                LOGGER.log(Level.WARNING, "The Class " + this.className==null?"":"'" + this.className + "'"
                        + " is null");
            }
            return false;
        }
        return true;
    }
}