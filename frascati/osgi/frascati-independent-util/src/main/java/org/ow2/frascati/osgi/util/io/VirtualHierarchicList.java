/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.util.io;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.ow2.frascati.util.io.IOUtils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A List as a VirtualHierarchicStrucutre
 */
public class VirtualHierarchicList implements VirtualHierarchicStructure
{
    /**
     * the Logger
     */
    private static Logger logg = Logger.getLogger(
            VirtualHierarchicList.class.getCanonicalName());
    
    /**
     * Map of the List entries
     */
    Map<String, Map<String, URL>> hierarchicalMap = null;

    /**
     * Constructor
     * 
     * @param list
     *          the List of URLs to wrap
     */
    public VirtualHierarchicList(List<URL> list)
    {
        hierarchicalMap = new HashMap<String, Map<String, URL>>();
        int n = 0;
        while (n < list.size())
        {
            URL url = (URL) list.get(n++);
            String urlFile = url.getFile();
            int jarPosition = urlFile.indexOf(".jar!/");
            String urlFilePrefix = "";
            if (jarPosition != -1 && (jarPosition + 6 < urlFile.length()))
            {
                urlFilePrefix = urlFile.substring(0, jarPosition + 5);
                urlFile = urlFile.substring(jarPosition + 6);
            }
            String[] urlFileElements = urlFile.split("/");
            StringBuilder rebuildPath = new StringBuilder("/");
            int p = 0;
            for (; p < urlFileElements.length; p++)
            {
                String urlFileElement = urlFileElements[p];
                if (urlFileElement != null && urlFileElement.length() > 0)
                {
                    boolean newMap = false;
                    String currentPath = rebuildPath.toString();
                    Map<String, URL> filesMap = hierarchicalMap
                            .get(currentPath);
                    if (filesMap == null)
                    {
                        newMap = true;
                        filesMap = new HashMap<String, URL>();
                        hierarchicalMap.put(currentPath, filesMap);
                    }
                    if (p < (urlFileElements.length - 1))
                    {
                        try
                        {
                            String key = rebuildPath.append(urlFileElement)
                                    .append('/').toString();
                            URL value = new URL(url.getProtocol(), "",
                                    new StringBuilder(urlFilePrefix)
                                            .append(key).toString());
                            hierarchicalMap.get(currentPath).put(key, value);
                        } catch (MalformedURLException e)
                        {
                            logg.log(Level.WARNING,e.getMessage(),e);
                        }
                    } else
                    {
                        String key = rebuildPath.append(urlFileElement)
                                .append(IOUtils.isDirectory(url) ? "/" : "")
                                .toString();

                        hierarchicalMap.get(currentPath).put(key, url);
                    }
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.util.io.HierarchicalStructure
     *      #getEntryPaths(java.lang.String)
     */
    public Enumeration<String> getEntryPaths(String path)
    {

        final Map<String, URL> entries = hierarchicalMap.get(path);
        if (entries == null)
        {
            return Collections.enumeration(Collections.<String>emptyList());
        }
        final Iterator<String> keyIt = entries.keySet().iterator();
        return new Enumeration<String>()
        {

            public boolean hasMoreElements()
            {

                return keyIt.hasNext();
            }

            public String nextElement()
            {

                return keyIt.next();
            }
        };
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.util.io.HierarchicalStructure
     *      #getEntry(java.lang.String)
     */
    public URL getEntry(String path)
    {

        String formatPath = path;
        if (formatPath.endsWith("/"))
        {
            formatPath = path.substring(0, path.length() - 1);
        } else
        {
            formatPath = path;
        }
        int sepPosition = formatPath.lastIndexOf('/');
        if (sepPosition == -1)
        {
            return null;
        }
        String pathDir = formatPath.substring(0, sepPosition + 1);
        Map<String, URL> entries = hierarchicalMap.get(pathDir);
        if (entries != null)
        {
            Iterator<String> keyIt = entries.keySet().iterator();
            while (keyIt.hasNext())
            {
                String key = (String) keyIt.next();
                if (path.equals(key))
                {
                    return entries.get(key);
                }
            }
        }
        return null;
    }
}
