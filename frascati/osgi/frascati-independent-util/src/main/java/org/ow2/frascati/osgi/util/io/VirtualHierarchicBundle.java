/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.util.io;

import java.net.URL;
import java.util.Enumeration;

import org.osgi.framework.Bundle;

/**
 * A Bundle as a VirtualHierarchicStrucutre
 */
public class VirtualHierarchicBundle implements VirtualHierarchicStructure
{
    /**
     * The wrapped Bundle
     */
    private Bundle bundle = null;

    /**
     * Constructor
     * 
     * @param bundle
     *          the Bundle to wrap
     */
    public VirtualHierarchicBundle(Bundle bundle)
    {
        this.bundle = bundle;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.util.io.VirtualHierarchicStructure#
     * getEntryPaths(java.lang.String)
     */
    public Enumeration<String> getEntryPaths(String path)
    {
        return bundle.getEntryPaths(path);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.util.io.VirtualHierarchicStructure#
     * getEntry(java.lang.String)
     */
    public URL getEntry(String path)
    {
        String localPath = null;
        
        if(!path.startsWith("/"))
        {
            localPath = new StringBuilder('/').append(
                    path).toString();
        } else
        {
            localPath = path;
        }
        return bundle.getEntry(localPath);
    }
}
