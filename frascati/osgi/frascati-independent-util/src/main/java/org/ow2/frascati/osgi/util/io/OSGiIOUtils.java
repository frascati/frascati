/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.osgi.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;

import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.ow2.frascati.util.io.IOUtils;

/**
 * Helper methods for creation and manipulation of Bundles in an OSGi context
 */
public abstract class OSGiIOUtils
{
    //New Headers for FraSCAti's Bundles
    //Is the Bundle a FraSCAti fragment
    public static final String FRAGMENT_HEADER = "Frascati-Fragment";
    //Needed bootstrap to use the FraSCAti fragment owning this entry
    public static final String BOOTSTRAP_HEADER = "Frascati-Bootstrap";
    //Bootstrap(s) that can be overwritten by the one defined by the "Frascati-Bootstrap" header 
    public static final String OVERWRITTEN_BOOSTRAP_HEADER = "Frascati-BootstrapOverwrite";
    //Needed FraSCAti fragment(s) to use the FraSCAti fragment owning this entry
    public static final String FRAGMENT_NEED_HEADER = "Frascati-FragmentNeed";
    //Needed Bundle(s) to use the FraSCAti fragment owning this entry
    public static final String BUNDLE_NEED_HEADER = "Frascati-BundleNeed";
    //Resource(s) to exclude of the building of the AbstractResources hierarchy
    public static final String UNMANAGED_RESOURCE = "Frascati-Unmanaged";
    //Resource(s) to copy as temporary file(s) - needed by the JDTCompiler
    public static final String JULIAC_TINFI_COMPIL = "Frascati-JTCompil";
    //Composite resource(s) contained by the Bundle to process
    public static final String COMPOSITE_TO_PROCESS = "Frascati-Process";
    //FraSCAti's properties
    public static final String BOOTSTRAP_PROPERTY = "org.ow2.frascati.bootstrap";
    public static final String DEFAULT_BOOTSTRAP = "org.ow2.frascati.bootstrap.FraSCAtiFractal";
    //Existing Headers
    public static final String MANIFESTVERSION = "Manifest-Version";
    public static final String BUNDLE_CLASSPATH = "Bundle-ClassPath";
    public static final String BUNDLE_NAME = "Bundle-Name";
    public static final String BUNDLE_SYMBOLIC_NAME = "Bundle-SymbolicName";
    public static final String BUNDLE_VERSION = "Bundle-Version";
    public static final String BUNDLE_VENDOR = "Bundle-Vendor";
    public static final String BUNDLE_IMPORT_PACKAGE = "Import-Package";
    public static final String BUNDLE_EXPORT_PACKAGE = "Export-Package";
    public static final String BUNDLE_DYNAMICIMPORT_PACKAGE = "DynamicImport-Package";
    public static final String BUNDLE_MANIFESTVERSION = "Bundle-ManifestVersion";

    /**
     * The Logger
     */
    private static final Logger LOGGER = Logger.getLogger(OSGiIOUtils.class.getCanonicalName());

    /**
     * Create a directory where data of the Bundle which identifier is passed 
     * on as a parameter are stored
     *  
     * @param context
     *          the directory where to create the data storage directory
     * @param bundleId
     *          the identifier of the Bundle
     * @return
     *          the data storage directory
     * @throws IOException
     */
    public static File createBundleDataStore(File context, long bundleId)
            throws IOException
    {
        String root = new StringBuilder("bundle_").append(bundleId).toString();
        File file = new File(new StringBuilder(context.getAbsolutePath()).append(
                        File.separator).append(root).toString());
        if (!file.exists() && !file.mkdir())
        {
            file = IOUtils.createTmpDir(root);
        }
        return file;
    }
    
    /**
     * Create a directory where data of the Bundle passed on as a parameter
     * are stored
     *  
     * @param context
     *          the directory where to create the data storage directory
     * @param bundle
     *          the Bundle for which to create a data storage directory
     * @return
     *          the data storage directory
     * @throws IOException
     */
    public static File createBundleDataStore(File context, Bundle bundle)
            throws IOException
    {
        return createBundleDataStore(context,bundle.getBundleId());
    }
    
    /**
     * Create a directory where data of the BundleRevision passed on as a parameter
     * are stored
     *  
     * @param context
     *          the directory where to create the data storage directory
     * @param bundle
     *          the BundleRevisionItf for which to create a data storage directory
     * @return
     *          the data storage directory
     * @throws IOException
     */
    public static File createBundleDataStore(File context, BundleRevisionItf<?,?> bundle)
            throws IOException
    {
        return createBundleDataStore(context,bundle.getBundleId());
    }

    /**
     * Store the Bundle which location is passed on as a parameter 
     * as a Jar File in a temporary directory
     * 
     * @param bundleLocation
     *          the location of the Bundle to store
     * @param cache
     *          the directory where the Bundle is stored
     * @return
     *          the path of the created Jar File
     */
    public static String cacheBundle(String bundleLocation, File cache)
    {
        File bundleFile = new File(bundleLocation);
        String bundleFileName = bundleFile.getName();
        
        if(bundleFile.exists())
        {  
            try
            {      
                String jarPath = new StringBuilder(cache.getCanonicalPath()).append(
                                File.separatorChar).append(bundleFileName).append(
                                        bundleFileName.endsWith(".jar")?"":".jar"
                                            ).toString();  
                int index = 0;
                String old_jarPath = jarPath;
                File bundleCopy = new File(jarPath);
                
                while(bundleCopy.exists())
                {
                    jarPath = new StringBuilder(index++).append("_").append(
                            old_jarPath).toString();
                    bundleCopy = new File(jarPath);
                }
                IOUtils.copyFromURL(bundleFile.toURI().toURL(), jarPath);                    
                return jarPath;
                
            } catch (MalformedURLException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IOException e)
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e);
            }
        }
        return null;
    }
    
    /**
     * Store the Bundle wrapped in the BundleRevisionItf passed on as a parameter 
     * as a Jar File in a temporary directory
     * 
     * @param bundle
     *          the Bundle to store
     * @param cache
     *          the directory where the Bundle is stored
     * @return
     *          the path of the created Jar File
     */
    public static String cacheBundle(BundleRevisionItf<?,?> bundle, File cache)
    {
        String path = cacheBundle(bundle.getLocation(),cache);
        if(path == null)
        {
            path = copyBundle(bundle, cache);
        }
        return path;
    }
    
    /**
     * Store the Bundle passed on as a parameter as a Jar File in a temporary directory
     * 
     * @param bundle
     *          the Bundle to store
     * @param cache
     *          the directory where the Bundle is stored
     * @return
     *          the path of the created Jar File
     */
    public static String cacheBundle(Bundle bundle, File cache)
    {
        String path = cacheBundle(bundle.getLocation(),cache);
        if(path == null)
        {
            path = copyBundle(bundle, cache);
        }
        return path;
    }

    /**
     * As the cacheBundle method, store the Bundle wrapped in a VirtualHierarchicStructure
     * as a Jar File in a temporary directory by copying all its entries
     * 
     * @param headers
     *          a set of keys and values used to create the Manifest file
     * @param structure
     *          the VirtualHierarchicStructure containing the resources to store in
     *          the created jar file
     * @param cache
     *          the directory where the created jar file is stored
     * @return
     *          the path of the created Jar File 
     */
    public static String copyBundle(String name,Dictionary<String, String> headers,
            final VirtualHierarchicStructure structure, final File cache)
    {
        if (cache == null)
        {
            return null;
        }
        String bundleJarName = name;
        bundleJarName = bundleJarName.substring(bundleJarName.lastIndexOf(File.separatorChar) + 1);
        if (!bundleJarName.endsWith(".jar"))
        {
            bundleJarName = new StringBuilder(bundleJarName).append(".jar").toString();
        }
        String copyPath = new StringBuilder(cache.getAbsolutePath())
                .append(File.separatorChar).append(bundleJarName).toString();
        String jarPath = createJarFile(copyPath, structure, headers);
        return jarPath;
    }
    
    /**
     * As the cacheBundle method, store the Bundle wrapped in a BundleRevisionItf passed on as a 
     * parameter as a Jar File in a temporary directory by copying all its entries
     * 
     * @param bundle
     *          the Bundle to store
     * @param cache
     *          the directory where the Bundle is stored
     * @return
     *          the path of the create Jar File 
     */
    public static String copyBundle(final BundleRevisionItf<?,?> bundle, final File cache)
    {
        return copyBundle(bundle.getSymbolicName(),bundle.getHeaders(),
                new VirtualHierarchicBundleRevision(bundle),cache);
    }
    
    /**
     * As the cacheBundle method, store the Bundle passed on as a parameter 
     * as a Jar File in a temporary directory by copying all its entries
     * 
     * @param bundle
     *          the Bundle to store
     * @param cache
     *          the directory where the Bundle is stored
     * @return
     *          the path of the create Jar File 
     */
    public static String copyBundle(final Bundle bundle, final File cache)
    {
        return copyBundle(getSymbolicName(bundle),bundle.getHeaders(),
                new VirtualHierarchicBundle(bundle),cache);
    }
    
    /**
     * Create a Jar File using the resources contained in the VirtualHierarchicStructure
     * passed on as a parameter
     * 
     * @param jarPath
     *          the path of the Jar File to create
     * @param struct
     *          the VirtualHierarchicStructure containing resources to put in the 
     *          Jar File
     * @param headers
     *          Dictionary of the Jar File's manifest
     * @return
     *          the path of the created Jar File 
     */
    private static String createJarFile(String jarPath,
            final VirtualHierarchicStructure struct,
            final Dictionary<String, String> headers)
    {
        @SuppressWarnings("unused")
        String jarName = jarPath.substring(
                jarPath.lastIndexOf(File.separatorChar) + 1,
                jarPath.length() - 4);
        
        Manifest manifest = new Manifest();
        Attributes att = manifest.getMainAttributes();
        if (headers != null)
        {
            Enumeration<String> keys = headers.keys();
            while (keys.hasMoreElements())
            {
                String key = keys.nextElement();
                String value = headers.get(key);
                att.putValue(key, value);
            }
        }
        try
        {
            final JarOutputStream target = new JarOutputStream(
                    new FileOutputStream(jarPath), manifest);
           
            Enumeration<String> structEntries = new Enumeration<String>()
            {
                private Stack<String> stack = new Stack<String>();
                private Stack<String> searchPaths = null;

                private void buildEntries()
                {
                    String path = (String) searchPaths.pop();
                    Enumeration<String> entries = struct.getEntryPaths(path);
                    if (entries != null)
                    {
                            while (entries.hasMoreElements())
                            {
                                String entry = (String) entries.nextElement();
                                URL entryUrl = struct.getEntry(entry);
                                if (entryUrl != null && (headers == null || 
                                        !entry.endsWith("MANIFEST.MF")))
                                {
                                    JarEntry je;
                                    if (entry.startsWith("/") || entry.startsWith("\\"))
                                    {
                                        je = new JarEntry(entry.substring(1).replace('\\', '/'));
                                        
                                    } else
                                    {
                                        je = new JarEntry(entry.replace('\\', '/'));
                                    }
                                    je.setTime(new Date().getTime());
                                    try
                                    {
                                        target.putNextEntry(je);
                                        if (IOUtils.isDirectory(entryUrl))
                                        {
                                            target.closeEntry();
                                            searchPaths.push(entry);
                                            
                                        } else
                                        {
                                            InputStream in = entryUrl.openStream();
                                            byte[] buffer = new byte[1024];
                                            while (true)
                                            {
                                                int count = in.read(buffer);
                                                if (count == -1)
                                                {
                                                    break;
                                                }
                                                target.write(buffer, 0, count);
                                            }
                                            target.closeEntry();
                                            if (in != null)
                                            {
                                                in.close();
                                            }
                                            stack.push(entry);
                                        }
                                    } catch (ZipException ze)
                                    {
                                        if(LOGGER.isLoggable(Level.CONFIG))
                                        {
                                            LOGGER.log(Level.CONFIG,ze.getMessage(),ze);
                                        }
                                    } catch (IOException ioe)
                                    {
                                        if(LOGGER.isLoggable(Level.CONFIG))
                                        {
                                            LOGGER.log(Level.CONFIG,ioe.getMessage(),ioe);
                                        }
                                    }
                                }
                            }
                        } 
                }

                /**
                 * {@inheritDoc}
                 * 
                 * @see java.util.Enumeration#hasMoreElements()
                 */
                public boolean hasMoreElements()
                {

                    if (searchPaths == null)
                    {
                        searchPaths = new Stack<String>();
                        searchPaths.push("/");
                    }
                    while (stack.size() == 0 && searchPaths.size() > 0)
                    {
                        buildEntries();
                    }
                    return stack.size() > 0;
                }

                /**
                 * {@inheritDoc}
                 * 
                 * @see java.util.Enumeration#nextElement()
                 */
                public String nextElement()
                {

                    String result = stack.pop();
                    return result;
                }
            };
            while (structEntries.hasMoreElements())
            {
                structEntries.nextElement();
            }
            target.flush();
            target.close();
            return jarPath;
        } catch (FileNotFoundException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * Create a BundleRevisionItf instance using the resources which URLs are passed on 
     * as a parameter
     * 
     * @param bundleContext
     *          the BundleContextRevisionItf instance where to install the new Bundle
     * @param bundleSymbolicName
     *          the "Bundle-SymbolicName" headers manifest's entry value of the Bundle to create
     * @param resources
     *           the list of URLs entries the Bundle will contain
     * @param headers
     *          the Dictionary of the BundleRevision's manifest
     * @return
     *          the installed BundleRevisionItf instance
     */
    public static BundleRevisionItf<?,?> createBundle(BundleContextRevisionItf<?,?> bundleContext,
            String bundleSymbolicName, List<URL> resources,
            Hashtable<String, String> headers)
    {
        String location = createBundle(bundleSymbolicName, resources, headers);
        BundleRevisionItf<?,?> bundle = null;
        
        if(location != null)
        {
            bundle = installBundle(location, bundleContext);
        }
        return bundle;
    }    
    
    /**
     * Create a Bundle using the resources which URLs are passed on as a parameter
     * 
     * @param bundleContext
     *          the BundleContext which will receive the new Bundle
     * @param bundleSymbolicName
     *          the "Bundle-SymbolicName" headers manifest's entry value of the Bundle to create
     * @param resources
     *           the list of URLs entries the Bundle will contain
     * @param headers
     *          the dictionary of the Bundle's manifest
     * @return
     *          the installed Bundle
     */
    public static Bundle createBundle(BundleContext bundleContext,
            String bundleSymbolicName, List<URL> resources,
            Hashtable<String, String> headers)
    {
        String location = createBundle(bundleSymbolicName, resources, headers);
        Bundle bundle = null;
        
        if(location != null)
        {
            bundle = installBundle(location, bundleContext);
        }
        return bundle;
    }
    
    /**
     * Create the bundle jar file using the resources which URLs are passed on as a parameter
     * 
     * @param bundleSymbolicName
     *          the name of the bundle jar file to create
     * @param resources
     *           the list of URLs entries the Bundle will contain
     * @param headers
     *          the dictionary of the Bundle's manifest
     * @return
     *          the bundle jar file
     */
    private static String createBundle(String bundleSymbolicName, List<URL> resources,
            Hashtable<String, String> headers)
    {
        File tmpDir = IOUtils.getTmpDir();
        String copyPath = new StringBuilder(tmpDir.getAbsolutePath())
                .append(File.separatorChar).append(bundleSymbolicName)
                .append(".jar").toString();
        
        String jarPath = createJarFile(copyPath, new VirtualHierarchicList(
                resources), headers);
        
        if (jarPath == null)
        {
            return null;
        }
        String location = jarPath;            
        if(!location.startsWith("file:/"))
        {
         location = new StringBuilder(location.startsWith("/")?"file:":"file:/").append(
                 location.startsWith("file:")?location.substring(5):location).toString();
        }
        location = location.replace('\\', '/');
        
        LOGGER.log(Level.CONFIG, "Try to install bundle from " + location);
        return location;            
    }    
    
    /**
     * @param location
     * @param context
     * @return
     */
    private static BundleRevisionItf<?,?> installBundle(String location,
            BundleContextRevisionItf<?,?> context)
    {
        BundleRevisionItf<?,?> bundle = null;
        try{
            
            bundle = context.installBundle(location);
        
        } catch (OSGiRevisionException e)
        {
            try
            {   
                String jarPath = location.substring(6);
                // if the OSGi implementation is JBoss
                bundle = context.installBundle(jarPath);
                
            } catch (OSGiRevisionException e1)
            {
                e1.printStackTrace();
                LOGGER.log(Level.CONFIG,e1.getMessage(),e1);
            }
            e.printStackTrace();
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        
        }
        return bundle;
    }
    
    /**
     * Install and return the 
     * 
     * @param location
     * @param context
     * @return
     */
    private static Bundle installBundle(String location, BundleContext context)
    {
        Bundle bundle = null;
        try
        {
            bundle = context.installBundle(location);
            
        } catch (BundleException e)
        {
            try
            {   
                String jarPath = location.substring(6);
                // if the OSGi implementation is JBoss
                bundle = context.installBundle(jarPath);
                
            } catch (BundleException e1)
            {
                e1.printStackTrace();
                LOGGER.log(Level.CONFIG,e1.getMessage(),e1);
            }
            e.printStackTrace();
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return bundle;
    }
    
    /**
     * Return an enumeration of resources contained by the Bundle passed on as a parameter
     *  
     * @param bundle
     *          the Bundle which entries have to be enumerated
     * @param filter
     *          the FilenameFilter to filter bundle's entries
     * @return
     *          the Enumeration of the Bundle's entries
     */
    private static Enumeration<String> getBundleEntries(
            final VirtualHierarchicStructure bundleStructure,
            final FilenameFilter filter)
    {
        return new Enumeration<String>()
        {
            private VirtualHierarchicStructure localBundleStructure= bundleStructure;
            private FilenameFilter filenameFilter = filter;
            private Stack<String> stack = new Stack<String>();
            private Stack<String> searchPaths = null;

            /**
             * fill searchPaths with all paths related to a directory fill stack
             * with entries accepted by the filter, or if it doesn't exist, with
             * all entries which are not related to a directory
             */
            private void buildEntries()
            {
                String path = (String) searchPaths.pop();
                Enumeration<String> entries = localBundleStructure.getEntryPaths(path);
                if (entries != null)
                {
                    while (entries.hasMoreElements())
                    {
                        String entry = (String) entries.nextElement();
                        URL entryUrl = localBundleStructure.getEntry(entry);
                        
                        if(entry != null)
                        {
                            //add the 'entry.endsWith(".jar/")' test for jboss use 
                            if ((entry.endsWith("/") || IOUtils.isDirectory(entryUrl))
                                 && (filenameFilter == null || filenameFilter.accept(null, entry)))
                            {
                                //If, using JBoss, the entry is an unmanaged jar resource then
                                //only register it as an entry and not as a search path 
                                if(entry.endsWith(".jar/"))
                                {
                                    stack.push(entry);
                                    
                                } else
                                {
                                    searchPaths.push(entry);
                                    stack.push(entry);
                                }
                                
                            } else if (filenameFilter == null || filenameFilter.accept(null, entry))
                            {
                                stack.push(entry);
                            }
                        }
                    }
                }
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#hasMoreElements()
             */
            public boolean hasMoreElements()
            {
                if (searchPaths == null)
                {
                    searchPaths = new Stack<String>();
                    searchPaths.push("/");
                }
                while (stack.size() == 0 && searchPaths.size() > 0)
                {
                    buildEntries();
                }
                return stack.size() > 0;
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#nextElement()
             */
            public String nextElement()
            {
                String result = stack.pop();
                return result;
            }
        };
    }
    
    /**
     * Return an enumeration of resources contained by the Bundle passed on as a parameter
     *  
     * @param bundle
     *          the Bundle which entries have to be enumerated
     * @param filter
     *          the FilenameFilter to filter bundle's entries
     * @return
     *          the Enumeration of the Bundle's entries
     */
    public static Enumeration<String> getBundleEntries(final Bundle bundle,
            final FilenameFilter filter)
    {
        return getBundleEntries(new VirtualHierarchicBundle(bundle) ,filter);
    }
    
    /**
     * Return an enumeration of resources contained by the Bundle passed on as a parameter
     *  
     * @param bundle
     *          the Bundle which entries have to be enumerated
     * @param filter
     *          the FilenameFilter to filter bundle's entries
     * @return
     *          the Enumeration of the Bundle's entries
     */
    public static Enumeration<String> getBundleEntries(final BundleRevisionItf<?,?> bundle,
            final FilenameFilter filter)
    {
        return getBundleEntries(new VirtualHierarchicBundleRevision(bundle) ,filter);
    }

    /**
     * Return the list of resources'names which appear in the 
     * "Frascati-Unmanaged" header of the Bundle which identifier 
     * is passed on as a parameter
     * 
     * @param bundleContext
     *          the BundleContext where to look for the Bundle
     * @param bundleId
     *          the identifier of the Bundle to explore
     * @return
     *          the list of unmanaged resources' name
     */
    public static ArrayList<String> getCompositesToProcess(BundleRevisionItf<?,?> bundle)
    {
        return getCompositesToProcess((Bundle)bundle.getBundle());
    }
    
    /**
      * Return the list of resources'names which appear in the 
     * "Frascati-Process" header of the Bundle passed on as a parameter
     *  
     * @param bundle
     *          the Bundle to explore
     * @return
     *          the list of composite files'names to process
     */
    public static ArrayList<String> getCompositesToProcess(Bundle bundle)
    {
        if (bundle == null)
        {
            return (ArrayList<String>) null;
        }
        ArrayList<String> compositesToProcess = new ArrayList<String>();
        String[] compositesToProcessTab = getHeader(bundle,COMPOSITE_TO_PROCESS);

        if (compositesToProcessTab != null)
        {
            for (String compositeToProcess : compositesToProcessTab)
            {
                compositesToProcess.add(compositeToProcess);
            }
        }
        return compositesToProcess;
    }
    
    /**
     * Return the list of resources'names which appear in the 
     * "Frascati-Unmanaged" header of the Bundle which identifier 
     * is passed on as a parameter
     * 
     * @param bundleRevision
     *          the BundleRevision to explore
     * @return
     *          the list of unmanaged resources' name
     */
    public static List<String> getUnmanagedResources(BundleRevisionItf<?,?> bundleRevision)
    {
        return getUnmanagedResources((Bundle)bundleRevision.getBundle());
    }

    /**
     * Return the list of resources'names which appear in the 
     * "Frascati-Unmanaged" header of the Bundle passed on as a parameter
     * 
     * @param bundle
     *          the Bundle object to explore
     * @return
     *          the list of unmanaged resources' name
     */
    public static List<String> getUnmanagedResources(Bundle bundle)
    {
        if (bundle == null)
        {
            if(LOGGER.isLoggable(Level.CONFIG))
            {
                LOGGER.log(Level.CONFIG,
                        "Ask for unmanaged resources list for a NULL Bundle");
            }
            return null;
        }
        List<String> unmanagedResources = null;
        String[] unmanagedResourcesTab = getHeader(bundle, UNMANAGED_RESOURCE);
        if (unmanagedResourcesTab != null)
        {
            unmanagedResources = new ArrayList<String>();
            for (String unmanagedResource : unmanagedResourcesTab)
            {
                unmanagedResources.add(unmanagedResource);
            }
        }
        return unmanagedResources;
    }

    /**
     * Cache the list of jar files which names appear in the Frascati-JTCompil header
     * and return a list of those copies' URLs
     * 
     * @param bundle
     *          the container Bundle
     * @param cache
     *          the cache directory
     * @return
     *          the list of urls of cache files
     */
    public static String[] getJarsForJuliac(BundleRevisionItf<?,?> bundle)
    {   
        if (bundle == null)
        {
            if(LOGGER.isLoggable(Level.CONFIG))
            {
                LOGGER.log(Level.CONFIG,
                        "Ask for Juliac's libraries list for a NULL Bundle");
            }
            return null;
        }
        String[] jarsForJuliac = getHeader(bundle, JULIAC_TINFI_COMPIL);
        return jarsForJuliac;
    }

    /**
     * Cache the list of jar files which names appear in the Frascati-JTCompil header
     * and return a list of those copies' URLs
     * 
     * @param bundle
     *          the container Bundle
     * @param cache
     *          the cache directory
     * @return
     *          the list of urls of cache files
     */
    public static String[] getJarsForJuliac(Bundle bundle)
    {        
        if (bundle == null)
        {
            if(LOGGER.isLoggable(Level.CONFIG))
            {
                LOGGER.log(Level.CONFIG,
                        "Ask for Juliac's libraries list for a NULL Bundle");
            }
            return null;
        }
        String[] jarsForJuliac = getHeader(bundle, JULIAC_TINFI_COMPIL);
        return jarsForJuliac;
    }
        
    /**
     * Return an array of semicolon separated entries of the header which name 
     * is passed on as a parameter
     * 
     * @param bundle
     * 
     * @param header
     * 
     */
    public static String[] getHeader(BundleRevisionItf<?,?> bundle, String header)
    {
        return getHeader((Bundle)bundle.getBundle(),header);
    }
    
    /**
     * Return an array of semicolon separated entries of the header which name 
     * is passed on as a parameter
     * 
     * @param bundle
     * 
     * @param header
     */
    public static String[] getHeader(Bundle bundle, String header)
    {
        String headerValue = (String) bundle.getHeaders().get(header);
        if (headerValue != null)
        {
            String[] headerValueElements = headerValue.split(",");
            return headerValueElements;
        }
        return null;
    }
    
    /**
     * @param bundle
     * @return
     */
    public static String getSymbolicName(Bundle bundle)
    {
        Dictionary<String,String> headers = bundle.getHeaders();
        String symbolicName = headers.get("Bundle-SymbolicName");
        return symbolicName;
    }
}
