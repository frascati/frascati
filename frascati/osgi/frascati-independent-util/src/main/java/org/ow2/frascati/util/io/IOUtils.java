/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Lionel Seinturier
 *
 */
package org.ow2.frascati.util.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Stack;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * Helper methods
 */
public abstract class IOUtils
{
    /**
     * Buffer size
     */
    public static final int BUFFER_SIZE = 1024 * 64;   
    
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(IOUtils.class.getCanonicalName());

    /**
     * Delete recursively all the files contained in the specified file if the
     * specified file is a directory, and the specified file itself.
     * 
     * @param file
     *            the file to delete
     */
    public static void deleteAllRecursively(File file)
    {
        if (file != null && file.exists())
        {
            cleanDirectory(file);
            file.delete();
        }
    }

    /**
     * Delete the content of the directory passed on as a parameter
     * 
     * @param file
     *          the directory which content has to be deleted
     */
    public static void cleanDirectory(File file)
    {

        if (file.isDirectory())
        {
            File[] subs = file.listFiles();
            for (int i = 0; i < subs.length; i++)
            {
                deleteAllRecursively(subs[i]);
            }
        }
    }

    /**
     * Create a temp directory
     * 
     * @param dirName
     *            the name of the directory to create
     **/
    public static File createTmpDir(String dirName)
    {

        File dir = getTmpDir();
        File resultDir = new File(dir, dirName);
        deleteAllRecursively(resultDir);
        if (resultDir.mkdir())
        {
            resultDir.deleteOnExit();
            return resultDir;
        }
        return null;
    }

    /**
     * Return the system temp directory
     **/
    public static File getTmpDir()
    {
        try
        {
            File tmpFile = File.createTempFile("ioutil", ".tmp");
            tmpFile.delete();
            File dir = tmpFile.getParentFile();
            return dir;
            
        } catch (IOException e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * Copy the resource which URL is passed on as a parameter to the 
     * directory which path is also passed on as a parameter
     * 
     * @param url
     *          the URL of the resource to copy
     * @param path
     *          the directory's path where to copy the resource
     * @throws IOException
     */
    public static void copyFromURL(URL url, String path) throws IOException
    {
        InputStream is = url.openStream();
        copyFromStream(is, path);
    }

    /**
     * Copy the resource which InputStream is passed on as a parameter to the 
     * directory which path is also passed on as a parameter
     * 
     * @param inputStream
     *          the InputStream of the resource to copy
     * @param path
     *          the directory's path where to copy the resource
     * @throws IOException
     */
    public static void copyFromStream(InputStream inputStream, String path)
    {
        // allocate a buffer to copy required jars.
        byte[] buffer = new byte[BUFFER_SIZE];
        File file = new File(path);
        if (file.exists())
        {
            return;
        }
        FileOutputStream fost = null;
        try
        {
            fost = new FileOutputStream(file);
            int len;
            while ((len = inputStream.read(buffer)) >= 0)
            {
                fost.write(buffer, 0, len);
            }
        } catch (Throwable e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);   
        }
        // close both streams.
        try
        {
            inputStream.close();
            fost.close();
            
        } catch (Throwable e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e);   
        }
    }

    /**
     * Remove slash character if it is the first or the last one of the path 
     * passed on as a parameter
     */
    public static String separatorTrim(String entryPath)
    {
        String compare = (entryPath.startsWith("/") || entryPath
                .startsWith(File.separator)) ? entryPath.substring(1)
                : entryPath;
        compare = (compare.endsWith("!/")) ? compare.substring(0,
                compare.length() - 2) : compare;
        compare = (compare.endsWith("/") || compare.endsWith(File.separator)) ? compare
                .substring(0, compare.length() - 1) : compare;
        return compare;
    }

    /**
     * Return the last part of the path passed on as a parameter
     */
    public static String pathLastPart(String path)
    {
        int position = 0;
        position = path.lastIndexOf(File.separator);
        if (position == -1 && !"/".equals(File.separator))
        {
            position = path.lastIndexOf("/");
        }
        return path.substring(position + 1);
    }

    /**
     * @param url
     * @return
     */
    public static boolean isDirectory(URL url)
    {
        if(url == null)
        {
            return false;
        }
        String file = url.getFile();
        return (file.length() > 0 && file.charAt(file.length() - 1) == '/');
    }

    /**
     * @param file
     * @param filter
     * @return
     */
    public static Enumeration<String> getFileEntries(final File file,
            final FilenameFilter filter)
    {
        return new Enumeration<String>()
        {
            private File fi = file;
            private FilenameFilter f = filter;
            private Stack<String> stack = new Stack<String>();
            private Stack<File> searchPaths = null;

            private void buildEntries()
            {
                File currentFile = searchPaths.pop();
                File[] entries;
                if (f == null)
                {
                    entries = currentFile.listFiles();
                } else
                {
                    entries = currentFile.listFiles(f);
                }
                if (entries != null)
                {
                    int n = 0;
                    while (n < entries.length)
                    {
                        File entry = entries[n++];
                        if (filter == null || filter.accept(null,
                             pathLastPart(separatorTrim(entry.getAbsolutePath()))))
                        {
                            stack.push(entry.getAbsolutePath().substring(
                                    currentFile.getAbsolutePath().length()+1));
                            if (entry.isDirectory())
                            {
                                searchPaths.push(entry);
                            }
                            break;
                        }
                    }
                }
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#hasMoreElements()
             */
            public boolean hasMoreElements()
            {

                if (searchPaths == null)
                {
                    searchPaths = new Stack<File>();
                    searchPaths.push(fi);
                }
                while (stack.size() == 0 && searchPaths.size() > 0)
                {
                    buildEntries();
                }
                return stack.size() > 0;
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#nextElement()
             */
            public String nextElement()
            {

                return stack.pop();
            }
        };
    }

    /**
     * @param jarFile
     * @param filter
     * @return
     */
    public static Enumeration<String> getJarEntries(final JarFile jarFile,
            final FilenameFilter filter)
    {
        return new Enumeration<String>()
        {
            private JarFile j = jarFile;
            private FilenameFilter f = filter;
            private Enumeration<JarEntry> jarEntries = null;
            private String next = null;

            private void findNext()
            {
                next = null;
                if (jarEntries != null)
                {
                    while (jarEntries.hasMoreElements())
                    {
                        JarEntry entry = jarEntries.nextElement();
                        String entryName = entry.getName();
                        if (entryName.endsWith("/"))
                        {
                            entryName = entryName.substring(0,
                                    entryName.length() - 1);
                        }
                        if (filter == null || filter.accept(null, entryName))
                        {
                            next = entry.getName();
                            break;
                        }
                    }
                }
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#hasMoreElements()
             */
            public boolean hasMoreElements()
            {
                if (jarEntries == null)
                {
                    jarEntries = j.entries();
                    findNext();
                }
                return next != null;
            }

            /**
             * {@inheritDoc}
             * 
             * @see java.util.Enumeration#nextElement()
             */
            public String nextElement()
            {
                String nextElement = next;
                findNext();
                return nextElement;
            }
        };
    }
}
