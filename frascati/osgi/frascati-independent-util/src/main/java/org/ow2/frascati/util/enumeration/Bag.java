/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.util.enumeration;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Helper Enumeration 
 *  
 * @param <E>
 */
public abstract class Bag<E> implements Enumeration<E>
{
    /**
     * the Logger
     */
    private static Logger logg = Logger.getLogger(Bag.class.getCanonicalName());
    
    private E[] list;
    private HashSet<E> enumerate;
    private int length;
    private int size;
    private int index;
    private E next;
    private boolean unique;

    /**
     * Constructor
     */
    public Bag()
    {
        size = 20;
        length = 0;
        index = 0;
        list = EArray(size);
        unique = false;
    }

    /**
     * Create an array of type E of the size passed on as a parameter
     * 
     * @param size
     *          the size of the array to create
     * @return
     *          an array of E type
     */
    public abstract E[] EArray(int size);
    
    /**
     * Add the <E> elements Enumeration passed on as a parameter to the enumeration
     * 
     * @param objects
     *          the <E> elements Enumeration to add
     */
    public void addEnumeration(Enumeration<E> enumeration)
    {
        if (enumeration == null || !enumeration.hasMoreElements())
        {
            return;
        }
        while (enumeration.hasMoreElements())
        {
            add(enumeration.nextElement());
        }
        reinit();
    }
    
    /**
     * Add the <E> elements List passed on as a parameter to the enumeration
     * 
     * @param objects
     *          the <E> elements List to add
     */
    public void addList(List<E> list)
    {
        if (list == null || list.size() == 0)
        {
            return;
        }
        Iterator<E> it = list.iterator();
        while (it.hasNext())
        {
            add(it.next());
        }
        reinit();
    }
    
    /**
     * Add the <E> elements array passed on as a parameter to the enumeration
     * 
     * @param objects
     *          the <E> elements array to add
     */
    public void addArray(E[] objects)
    {
        if (objects == null || objects.length == 0)
        {
            return;
        }
        int n = 0;
        for (; n < objects.length; n++)
        {
            add(objects[n]);
        }
        reinit();
    }

    /**
     * Add the <E> element passed on as a parameter to the enumeration
     * 
     * @param e
     *          the <E> element to add
     */
    public void add(E e)
    {
        if (e == null)
        {
            logg.log(Level.INFO, "Try to insert a null object in " + this);
            return;
        }
        if (size - length < 5)
        {
            increase();
        }
        list[length++] = e;
        reinit();
    }

    /**
     * Increase the size of the array where the Enumeration's <E>
     * elements are stored
     */
    private void increase()
    {
        size += 20;
        E[] newList = EArray(size);
        System.arraycopy(list, 0, newList, 0, length);
        list = newList;
    }

    /**
     * Return the Enumeration as an Array of its contained <E>
     * elements
     */
    public E[] toArray()
    {
        E[] newList = EArray(length);
        System.arraycopy(list, 0, newList, 0, length);
        return newList;
    }

    /**
     * Define what is the next element of the Enumeration 
     * to return 
     */
    private void getNext()
    {
        next = null;
        while (index < length)
        {
            E tmpNext = (E) list[index++];
            if (!enumerate.contains(tmpNext))
            {
                next = tmpNext;
                enumerate.add(next);
                break;
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.util.Enumeration#hasMoreElements()
     */
    public boolean hasMoreElements()
    {
        return next != null;
    }

    /**
     * The enumeration process can be restarted 
     */
    public void reinit()
    {
        index = 0;
        enumerate = new HashSet<E>();
        getNext();
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.util.Enumeration#nextElement()
     */
    public E nextElement()
    {
        E result = next;
        getNext();
        return result;
    }

    /**
     * Return the number of <E> elements contained by the Bag
     * 
     * @return
     *          Number of <E> elements contained 
     */
    public int length()
    {
        return length;
    }
}