/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Lionel Seinturier, Philippe Merle
 *
 */
package org.ow2.frascati.util.reference;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.TinfiComponentInterface;
import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;


/**
 * An Helper class to retrieve root component and tinfi interceptor
 * from a ServiceReference
 */
public abstract class ServiceReferenceUtil
{

    // -------------------------------------------------------------------------
    // Internal state.
    // -------------------------------------------------------------------------

    private static final Logger LOGGER = Logger.getLogger(ServiceReferenceUtil.class.getCanonicalName());
    
    // -------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------

    /**
     * Return the next tinfi membrane separating the object passed on as parameter of
     * the root component
     * 
     * @param o
     *          the object "to peel"
     * @return 
     *          the next membrane separating of the root component
     */
    private static Object getNextMembrane(Object object)
    {
        if (object == null)
        {
            return object;
        }
        if (object instanceof ServiceReferenceImpl)
        {
            return ((ServiceReferenceImpl<?>) object)._getDelegate();
            
        } else if (object instanceof TinfiComponentInterceptor)
        {
            return ((TinfiComponentInterceptor<?>) object).getFcItfDelegate();
            
        } else if (object instanceof TinfiComponentInterface)
        {
            return ((TinfiComponentInterface<?>) object).getFcItfImpl();
            
        }
        return null;
    }

    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------

    /**
     * Return the last TinfiInterceptor before the Component from the object
     * passed on as a parameter
     * 
     * @param o
     *            the object "to peel"
     * @return the membrane separating of the root component
     */
    public static TinfiComponentInterceptor<?> getInterceptor(Object object)
    {
        // Follow the track of the root component by collecting successively
        // Tinfi's interfaces and interceptors
        Object membrane = object;
        Object intermediate = null;
        TinfiComponentInterceptor<?> interceptor = null;
        
        while (membrane != null)
        {
            intermediate = membrane;
            membrane = getNextMembrane(membrane);
        }
        try
        {
            interceptor = (TinfiComponentInterceptor<?>)intermediate;
        }
        catch(ClassCastException e)
        {
            LOGGER.log(Level.WARNING, e.getMessage());            
        }
        return interceptor;
    }
    
    /**
     * If the last membrane is Julia Interceptor and not a TinfiComponentInterceptor
     * then return the associated object instance 
     * 
     * @param o
     *            the object "to peel"
     *            
     * @return the Object instance
     */
    public static Object getInstance(Object object)
    {
        // Follow the track of the root component by collecting successively
        // Tinfi's interfaces and interceptors
        Object membrane = object;
        Object intermediate = null;
        
        while (membrane != null)
        {
            intermediate = membrane;
            membrane = getNextMembrane(membrane);
        }
        if(!TinfiComponentInterceptor.class.isAssignableFrom(intermediate.getClass())
           && Interceptor.class.isAssignableFrom(intermediate.getClass()))
        {
           Object instance = ((Interceptor) intermediate).getFcItfDelegate();
           return instance;
        }
        return null;
    }

    /**
     * Return the root component associated to the ServiceReference object passed 
     * on as parameter 
     * 
     * @param o
     *          the object "to peel"
     * @return 
     *          the root component
     */
    public static Component getRootComponent(ServiceReference<?> serviceReference)
    {
        Object interceptor = getInterceptor(serviceReference);
        try
        {
            Field field = TinfiComponentInterceptor.class
                    .getDeclaredField("fcComp");
            field.setAccessible(true);
            Component component = (Component) field.get(interceptor);
            return component;
            
        } catch (Throwable e)
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e); 
        }
        return null;
    }
}
