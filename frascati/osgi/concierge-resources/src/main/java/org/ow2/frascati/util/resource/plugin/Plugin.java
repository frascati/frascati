package org.ow2.frascati.util.resource.plugin;

import org.ow2.frascati.util.resource.AbstractPlugin;
import org.ow2.frascati.util.resource.AbstractResource;
import java.util.logging.Level;

public class Plugin extends AbstractPlugin
{
    public void plug()
    {
        System.setProperty(AbstractResource.RESOURCE_PROP, "cobundle");        
        LOGGER.log(Level.INFO, "AbstractResource adapter : Concierge Plugin plugged");
    }
}