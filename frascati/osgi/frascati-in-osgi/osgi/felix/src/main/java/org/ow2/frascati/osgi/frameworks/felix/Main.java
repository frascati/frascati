/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.frameworks.felix;

import java.io.File;
import java.io.IOException;

import org.apache.felix.framework.Felix;
import org.apache.felix.framework.cache.BundleCache;
import org.apache.felix.framework.util.StringMap;
import org.osgi.framework.BundleException;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

import java.util.logging.Level;

public class Main extends org.ow2.frascati.osgi.frameworks.Main<Felix>
{
    public Main() throws IOException, BundleException
    {
        super();
        File cache = createCacheDirectory("felix");
        StringMap properties = new StringMap();
        properties.put(Constants.FRAMEWORK_STORAGE_CLEAN,
                Constants.FRAMEWORK_STORAGE_CLEAN_ONFIRSTINIT);
        properties.put(Constants.FRAMEWORK_SYSTEMPACKAGES, SYSTEM_PACKAGE_BASE);
        properties.put(BundleCache.CACHE_ROOTDIR_PROP, cache.getAbsolutePath());
        instance = new Felix(properties);
        instance.init();
        instance.start();
    }

    /**
     * @param args
     * @throws BundleException
     * @throws InterruptedException
     * @throws IOException
     */
    public static void main(String[] args) throws InterruptedException,
            BundleException, IOException
    {
        Main main = new Main();
        String clientOrServer = null;
        main.launch((args == null || args.length == 0 || (clientOrServer = args[0]) == null) ? ""
                : clientOrServer);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getSystemBundleContext()
     */
    public BundleContext getSystemBundleContext()
    {
        return instance.getBundleContext();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getBundle(long)
     */
    public Bundle getBundle(long id)
    {
        return instance.getBundleContext().getBundle(id);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#kill()
     */
    public void kill()
    {
        try
        {
            instance.stop();
            
        } catch (BundleException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        instance = null;
    }
}
