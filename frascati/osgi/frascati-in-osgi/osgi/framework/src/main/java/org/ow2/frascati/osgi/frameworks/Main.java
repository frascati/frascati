package org.ow2.frascati.osgi.frameworks;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarFile;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

public abstract class Main<F>
{
    public static final String FRAMEWORK_EXPORTED_PACKAGES = "org.ow2.frascati.fio.packages";
    public static final String JBOSS_FRAMEWORK_EXPORTED_PACKAGES = "org.ow2.frascati.fio.jboss.packages";
    public static final String LOG4J_CONFIGURATION = "org.ow2.frascati.fio.jboss.log4j";
    public static final String LAUNCH_BUNDLES = "org.ow2.frascati.fio.launch";
    public static final String INSTALL_BUNDLES = "org.ow2.frascati.fio.install";
    
    /**
     * Logger.
     */
    protected Logger logger = Logger.getLogger(getClass().getCanonicalName());
    protected F instance;
    protected List<Bundle> installed = new ArrayList<Bundle>();
    protected String resources;
    protected static int count;
    private JarFile mainRessourceJar;
    protected static String SYSTEM_PACKAGE_BASE;

    /**
     * Constructor
     */
    protected Main()
    {
        System.setProperty("org.osgi.service.http.port", "54460");
        resources = getBundleRepository();
        try
        {
            String frameworkPropsFilePath = System.getProperty(FRAMEWORK_EXPORTED_PACKAGES);
            if(frameworkPropsFilePath == null)
            {
                frameworkPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("framework.xml").toString(); 
            }
            URL frameworkPropsFileURL = new File(frameworkPropsFilePath).toURI().toURL();
            InputStream inputStream = frameworkPropsFileURL.openStream();
            
            Properties frameworkProps = new Properties();
            frameworkProps.loadFromXML(inputStream);
            try
            {
                inputStream.close();
                
            } catch (IOException e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
            SYSTEM_PACKAGE_BASE = frameworkProps.getProperty("SYSTEM_PACKAGE_BASE");
            
        } catch (Exception e)
        {
            if(logger.isLoggable(Level.WARNING))
            {
                logger.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     * Prepare the framework by installing some useful Bundles
     */
    public void init()
    {
        try
        {
            File mainRessourceFile = new File(Main.class.getProtectionDomain()
                    .getCodeSource().getLocation().getPath());
            if (mainRessourceFile.getName().endsWith(".jar"))
            {
                URL mainRessourceJarURL = new URL("jar", "", new StringBuilder(
                        mainRessourceFile.toURI().toURL().toExternalForm())
                        .append("!/").toString());
                mainRessourceJar = ((JarURLConnection) mainRessourceJarURL
                        .openConnection()).getJarFile();
            }
            String[] sharedBundles = new String[] {
                    "org.osgi.enterprise.jar",
                    "org.osgi.compendium.jar",
                    "org.apache.felix.http.bundle.jar"//,
//                    "org.apache.felix.http.jetty.jar",
//                    "org.apache.felix.webconsole.jar"
                };
            for (String sharedBundle : sharedBundles)
            {
                try
                {
                    InputStream is = null;
                    String location = null;
                    if (mainRessourceJar != null)
                    {
                        is = mainRessourceJar.getInputStream(mainRessourceJar
                                .getEntry(sharedBundle));
                        location = sharedBundle;
                    } else
                    {
                        location = new StringBuilder("file:").append(
                                mainRessourceFile.getAbsolutePath()).append(
                                File.separatorChar).append(sharedBundle)
                                .toString();
                    }
                    getSystemBundleContext().installBundle(location, is).start();
                } catch (Exception e)
                {
                    if(logger.isLoggable(Level.WARNING))
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }
        } catch (Exception e)
        {
            if(logger.isLoggable(Level.WARNING))
            {
                logger.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     * Uninstall all pre-installed Bundles
     */
    public void uninstallAll()
    {
        if (installed != null)
        {
            int n = installed.size() - 1;
            for (; n > -1; n--)
            {
                try
                {
                    installed.get(n).stop();
                    installed.get(n).uninstall();
                    installed.remove(n);
                    
                } catch (BundleException e)
                {
                    if(logger.isLoggable(Level.WARNING))
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }
            installed = null;
            installed = new ArrayList<Bundle>();
        }
    }

    /**
     * Shutdown the framework
     */
    public void shutdown()
    {
        if (getSystemBundleContext() != null)
        {
            uninstallAll();
        }
        kill();
    }

    /**
     * Install in the BundleContext each bundle presents in the list
     * 
     * @param context
     *            The BundleContext where to install bundles
     * @param locations
     *            List of bundles to install
     * @throws InterruptedException
     * @throws BundleException
     */
    protected void installBundles(BundleContext context, String[] locations)
            throws InterruptedException, BundleException
    {
        if (context == null)
        {
            if(logger.isLoggable(Level.SEVERE))
            {
                logger.log(Level.SEVERE,"BundleContext is null ; "
                    + "No bundle can be installed");
            }
            return;
        }
        String[] bundles = locations;
        int n = 0;
        for (; n < bundles.length; n++)
        {
            try
            {
                String jarName = bundles[n];
                String jarPath = new StringBuilder("file:").append(resources)
                        .append(File.separator).append(jarName).toString();
                if (installed.add(context.installBundle(jarPath)))
                {
                    if(logger.isLoggable(Level.FINE))
                    {
                        logger.log(Level.FINE,installed.get(
                                installed.size() - 1).getSymbolicName()+ " installed");
                    }
                } else
                {
                    if(logger.isLoggable(Level.WARNING))
                    {
                        logger.log(Level.WARNING,"Error occured while installing "
                            + jarPath);
                    }
                }
            } catch (Exception e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
        }
    }

    /**
     * Install and start bundles list in the BundleContext
     * 
     * @param context
     *            The BundleContext where to install bundles
     * @param locations
     *            List of bundles to install and start
     * @throws InterruptedException
     * @throws BundleException
     */
    protected void launchBundles(BundleContext context, String[] locations)
            throws InterruptedException, BundleException
    {
        if (context == null)
        {
            if(logger.isLoggable(Level.SEVERE))
            {
                logger.log(Level.SEVERE,"BundleContext is null ; No bundle can be installed");
            }
            return;
        }
        String[] bundles = locations;
        int n = 0;
        for (; n < bundles.length; n++)
        {
            int size = installed.size();
            try
            {
                String jarName = bundles[n];
                installBundles(context, new String[]{ jarName });
                
                if (installed.size() > size)
                {
                    installed.get(installed.size() - 1).start();
                    logger.log(Level.FINE,installed.get(installed.size() - 1).getSymbolicName()
                            + " started");
                    Thread.sleep(2000);
                }
            } catch (Exception e)
            {
                logger.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     * Install and launch bundles in the OSGi Framework
     * 
     * @param example
     *            the String value of the example.name command line parameter value
     */
    public void launch(String example) throws InterruptedException,
            BundleException
    {
        init();
        try
        {
            String installPropsFilePath = System.getProperty(INSTALL_BUNDLES);
            if(installPropsFilePath == null)
            {
                installPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("install_bundles.xml").toString(); 
            }
            URL installedBundlesPropsURL = new File(installPropsFilePath).toURI().toURL();
            Properties install_bundles = new Properties();
            
            InputStream inputStream = installedBundlesPropsURL.openStream();
            install_bundles.loadFromXML(inputStream);
            try
            {
                inputStream.close();
                
            }catch(IOException e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
            String[] props = install_bundles.keySet().toArray(new String[0]);
            int n = 0;
            while (n < props.length)
            {
                String bundleNum = props[n++];
                String[] jarData = install_bundles.getProperty(bundleNum)
                        .split(":");
                String toInstall = jarData[1];
                String jarName = jarData[0];
                if ("true".equals(toInstall))
                {
                    try
                    {
                        getSystemBundleContext().installBundle(
                                new StringBuilder("file:").append(resources)
                                        .append(File.separator).append(jarName)
                                        .toString());
                    } catch (Exception e)
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }

            String launchPropsFilePath = System.getProperty(LAUNCH_BUNDLES);
            if(launchPropsFilePath == null)
            {
                launchPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("launch_bundles.xml").toString(); 
            }
            URL launchedBundlesPropsURL = new File(launchPropsFilePath).toURI().toURL();
            Properties launch_bundles = new Properties();            
            inputStream = launchedBundlesPropsURL.openStream();
            launch_bundles.loadFromXML(inputStream);
            try
            {
                inputStream.close();
                
            }catch(IOException e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
            props = launch_bundles.keySet().toArray(new String[0]);
            n = 0;
            while (n < props.length)
            {
                String bundleNum = props[n++];
                String[] jarData = launch_bundles.getProperty(bundleNum).split(
                        ":");
                String toInstall = jarData[1];
                String jarName = jarData[0];
                if ("true".equals(toInstall))
                {
                    try
                    {
                        getSystemBundleContext().installBundle(
                                new StringBuilder("file:").append(resources)
                                        .append(File.separator).append(jarName)
                                        .toString()).start();
                    } catch (Exception e)
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }
        } catch (InvalidPropertiesFormatException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
            
        } catch (IOException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * Create a temporary directory for the framework to instantiate
     * 
     * @param frameworkName
     *            the name of the framework
     * @return a temporary directory
     */
    protected File createCacheDirectory(String frameworkName)
    {
        try
        {
            File tmpFile = File.createTempFile(frameworkName, ".tmp");
            tmpFile.delete();
            File dir = tmpFile.getParentFile();
            File cache = new File(dir, (frameworkName + "_" + (count++)));
            deleteAllRecursively(cache);
            if (cache.mkdir())
            {
                cache.deleteOnExit();
                return cache;
            }
        } catch (IOException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }
    
    /**
     * Delete recursively all the files contained in the specified file if the
     * specified file is a directory, and the specified file itself.
     * 
     * @param file
     *            the file to delete
     */
    public static void deleteAllRecursively(File file)
    {
        if (file != null && file.exists())
        {
            if (file.isDirectory())
            {
                File[] subs = file.listFiles();
                for (int i = 0; i < subs.length; i++)
                {
                    deleteAllRecursively(subs[i]);
                }
            }
            file.delete();
        }
    }
    /**
     * Return the directory location where to find Bundles to install
     * 
     * @return the Bundles' repository
     */
    public String getBundleRepository()
    {
        File targetClassesDir = new File(getClass().getProtectionDomain()
                .getCodeSource().getLocation().getPath());
        File targetTestClassesDir = new File(
                targetClassesDir.getParentFile().getAbsolutePath() + File.separator + "test-classes");
        return System.getProperty("frascati.in.osgi.bundle.repository",
                targetTestClassesDir.getAbsolutePath());
    }
    
    /**
     * Return the BundleContext of the instantiated framework
     * 
     * @return the BundleContext
     */
    public abstract BundleContext getSystemBundleContext();

    /**
     * Return the Bundle which identifier is passed on as parameter
     * 
     * @param id
     *            the identifier of the Bundle
     * @return the Bundle
     */
    public abstract Bundle getBundle(long id);

    /**
     * Kill the framework instance process
     */
    public abstract void kill();
}
