/**
 * 
 */
package org.ow2.frascati.osgi.test;

import static org.junit.Assert.fail;

import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;

import org.junit.Test;
import org.osgi.framework.BundleException;

/**
 * @author munilla
 *
 */
public abstract class FraSCAtiInOSGiBindingOSGiTest extends FraSCAtiInOSGiTest
{
    /**
     * Test binding-osgi for an exported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test
    public void testFromFraSCAtiToOSGi() throws Exception
    {
        URL serverURL = classLoader.getResource(
                "helloworld-binding-frascati-osgi-server.jar");
        loadSCA(serverURL, "server.composite");
        InputStream is = classLoader.getResourceAsStream(
                "helloworld-binding-frascati-osgi-client.jar");
        try
        {
            main.getSystemBundleContext().installBundle(
                    "helloworld-binding-frascati-osgi-client", is).start();
            
        } catch (BundleException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * Test binding-osgi for an imported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test
    public void testFromOSGiToFraSCAti() throws Exception
    {
        InputStream is = classLoader.getResourceAsStream(
                "helloworld-binding-osgi-frascati-server2.jar");
        try
        {
            main.getSystemBundleContext().installBundle(
                    "helloworld-binding-osgi-frascati-server2", is).start();
            
        } catch (BundleException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        URL clientURL = classLoader.getResource(
                "helloworld-binding-osgi-frascati-client.jar");
        loadSCA(clientURL, "client.composite");
        launch("helloworld-binding-osgi-client");
    }

    /**
     * Test binding-osgi for an imported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test(expected = java.lang.Exception.class)
    public void testLaunchException() throws Exception
    {
        launch("helloworld-fake");
    }

    /**
     * Test binding-osgi for an imported service by FraSCAti
     * 
     * @throws FraSCAtiOSGiNotFoundServiceException
     * @throws FraSCAtiOSGiNotRunnableServiceException
     * @throws FraSCAtiOSGiNotFoundCompositeException
     */
    @Test(expected = java.lang.Exception.class)
    public void testServiceException() throws Exception
    {
        URL fractalURL = classLoader.getResource("helloworld-fractal.jar");
        loadSCA(fractalURL, "fake.composite");
    }
    
    /**
     * Install a bundle which contains an embedded Composite
     * to test if the tracker recognizes it and installs 
     * using the FraSCAtiOSGiService 
     */
    @Test
    public void testTracker() throws Exception
    {
        InputStream is = classLoader.getResourceAsStream(
                "frascati-osgi-dynamic-bundle.jar");
        
        main.getSystemBundleContext().installBundle(
                "frascati-osgi-dynamic-bundle", is).start();
        //wait for tracker process
        try
        {
            Thread.sleep(5000);
        }catch(InterruptedException e)
        {
            Thread.interrupted();
        }
        String loading = "";
        while(loading !=null )
        {
            try
            {
                Thread.sleep(100);
            }catch(InterruptedException e)
            {
                Thread.interrupted();
            }
            try
            {
                loading = (String)service.getClass().getDeclaredMethod(
                    "loading").invoke(service);
                
            } catch(Exception e)
            {
                log.log(Level.CONFIG,e.getMessage(),e);
            }
        }
        boolean loaded = false;
        try
        {
            loaded = (Boolean) service.getClass().getDeclaredMethod(
                    "loaded",new Class<?>[]{String.class}).invoke(service,
                            new Object[]{"helloworld-pojo"});
        } catch (Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            fail("helloworld-pojo has not been loaded");
        }
        if(loaded)
        {
            try
            {
                launch("helloworld-pojo");
            
            } catch (Exception e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
                fail("Unable to launch helloworl-pojo");
            }
        }
    }

}
