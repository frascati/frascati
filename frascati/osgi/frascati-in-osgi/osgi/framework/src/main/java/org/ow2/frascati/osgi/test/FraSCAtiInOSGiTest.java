/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.Before;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.ow2.frascati.osgi.frameworks.Main;

/**
 * Abstract test for FraSCAti in OSGi framework
 */
public abstract class FraSCAtiInOSGiTest
{
 protected static Logger log = Logger.getLogger(FraSCAtiInOSGiTest.class.getCanonicalName());
    
    protected static Main<?> main = null; 
    protected static Object service;
    protected static ClassLoader classLoader = null;
    
    @Before
    public void setUp() throws Exception
    {
        if(main == null)
        {
            launchMain();
            assertNotNull(main);
        }     
        if(service == null)
        {
            BundleContext bundleContext = main.getSystemBundleContext();
            ServiceReference reference = bundleContext.getServiceReference(
                    "org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService");
            service = bundleContext.getService(reference);            
            assertNotNull(service);
        }
    }

    @AfterClass
    public static void tearDown() throws Exception
    {   
        if(main != null)
        {
            main.kill();
        }
        cleanCache();
        service = null;
        main = null;
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest#getMain()
     */
    private void launchMain()
    {
        if(main == null)
        {
            System.setProperty("frascati.in.osgi.bundle.repository",
                    new File("target/test-classes").getAbsoluteFile().getAbsolutePath());
            
            String framework = getClass().getSimpleName().substring(10);
            framework = framework.substring(0,framework.length()-4).toLowerCase();
            try
            {
                main = (Main<?>) Class.forName("org.ow2.frascati.osgi.frameworks." + 
                        framework + ".Main").newInstance(); 
                main.launch(getDynamicExample());
                classLoader = main.getClass().getClassLoader();

            } catch(Exception e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }  
   }
    
    protected String getDynamicExample()
    {
        return null;
    }
    
    /**
     * Call the loadSCA method of the FraSCAtiOSGiService
     * 
     * @param resource
     * @param composite
     */
    protected void loadSCA(URL resource, String composite)
            throws Exception
    {
        service.getClass().getDeclaredMethod("loadSCA",
                new Class<?>[]{ URL.class, String.class }).invoke(
                service, new Object[]{ resource, composite });
    }   
    
    /**
     * Call the loadSCA method of the FraSCAtiOSGiService
     * 
     * @param resource
     * @param composite
     */
    protected void unloadSCA(String composite)
            throws Exception
    {
        java.lang.reflect.Method method = service.getClass().getDeclaredMethod(
                "unloadSCA",new Class<?>[]{String.class });
        method.setAccessible(true);
        method.invoke(service, new Object[]{ composite });
    }

    /**
     * Call the launch method of the FraSCAtiOSGiService
     * 
     * @param composite
     */
    protected void launch(String composite) throws Exception
    {
        service.getClass().getDeclaredMethod("launch",
                new Class<?>[]{ String.class }).invoke(
                service, new Object[] { composite });
    }
    
    /**
     * Clean cache directories
     */
    protected static void cleanCache()
    {
        //clean cache directories that have been created during 
        //tests
        File tmpDir = getTmpDir();
        String[] cacheDirectories = tmpDir.list(new FilenameFilter(){

            public boolean accept(File arg0, String arg1)
            {
             if(arg1.startsWith("jboss")|| arg1.startsWith("equinox")
                     || arg1.startsWith("concierge")
                     || arg1.startsWith("knopflerfish")|| arg1.startsWith("felix")
                     || arg1.startsWith("vfs")|| arg1.startsWith("FraSCAti_")
                     || arg1.startsWith("Jboss_resources"))
             {
                 return true;
            }
             return false;
            }
        });
        for(String cacheDirectoryName : cacheDirectories)
        {
            File cacheDirectory = new File(new StringBuilder(
              tmpDir.getAbsolutePath()).append(File.separator).append(
                      cacheDirectoryName).toString());
            if(cacheDirectory!=null && cacheDirectory.exists() && cacheDirectory.isDirectory())
            {
                deleteAllRecursively(cacheDirectory);
            }
        }        
    }
    
    /**
     * Delete recursively all the files contained in the specified file if the
     * specified file is a directory, and the specified file itself.
     * 
     * @param f
     *            the file to delete
     */
    private static void deleteAllRecursively(File f)
    {
        if (f != null && f.exists())
        {
            cleanDirectory(f);
            f.delete();
        }
    }

    private static void cleanDirectory(File f)
    {
        if (f.isDirectory())
        {
            File[] subs = f.listFiles();
            if(subs != null)
            {
                for (int i = 0; i < subs.length; i++)
                {
                    deleteAllRecursively(subs[i]);
                }
            }
        }
    }

    /**
     * Return the system temp directory
     **/
    private static File getTmpDir()
    {
        try
        {
            File tmpFile = File.createTempFile("test", ".tmp");
            tmpFile.delete();
            File dir = tmpFile.getParentFile();
            return dir;
        } catch (IOException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }
}
