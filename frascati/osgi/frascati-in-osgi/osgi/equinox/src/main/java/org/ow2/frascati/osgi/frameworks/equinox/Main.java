/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.frameworks.equinox;

import java.io.File;
import java.io.IOException;

import org.eclipse.osgi.baseadaptor.BaseAdaptor;
import org.eclipse.osgi.framework.adaptor.FrameworkAdaptor;
import org.eclipse.osgi.framework.internal.core.Framework;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
import org.osgi.service.startlevel.StartLevel;

public class Main extends org.ow2.frascati.osgi.frameworks.Main<Framework>
{

    public Main() throws IOException, BundleException
    {
        super();
        File cache = createCacheDirectory("equinox");
        System.setProperty("osgi.instance.area", cache.getAbsolutePath());
        System.setProperty("osgi.install.area", cache.getAbsolutePath());
        System.setProperty("org.osgi.framework.system.packages",SYSTEM_PACKAGE_BASE);
        FrameworkAdaptor adaptor = new BaseAdaptor(new String[0]);
        instance = new Framework(adaptor);
        instance.launch();
        BundleContext bci = instance.getSystemBundleContext();
        ServiceReference reference = bci.getServiceReference(StartLevel.class
                .getName());
        StartLevel startService = (StartLevel) bci.getService(reference);
        startService.setStartLevel(4);
    }

    public BundleContext getSystemBundleContext()
    {
        return instance.getSystemBundleContext();
    }

    public Bundle getBundle(long id)
    {
        return instance.getBundle(id);
    }

    /**
     * @param args
     * @throws BundleException
     * @throws InterruptedException
     * @throws IOException
     */
    public static void main(String[] args) throws InterruptedException,
            BundleException, IOException
    {
        Main main = new Main();
        String clientOrServer = null;
        main.launch((args == null || args.length == 0 || (clientOrServer = args[0]) == null) ? ""
                : clientOrServer);
    }

    @Override
    public void kill()
    {
        instance.shutdown(0);
        instance = null;
    }
}
