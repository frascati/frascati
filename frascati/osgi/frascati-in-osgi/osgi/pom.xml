<?xml version="1.0"?>
<!--
 * OW2 FraSCAti OSGi
 *
 * Copyright (c) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.ow2.frascati.osgi.fio</groupId>
        <artifactId>frascati-in-osgi-parent</artifactId>
        <version>1.6-SNAPSHOT</version>
    </parent>

    <artifactId>frascati-in-osgi-runtime</artifactId>
    <name>FraSCAti in OSGi Runtime Parent</name>
    <packaging>pom</packaging>

	<properties>
		<org.ow2.frascati.fio.packages>../../bundles/frascati-modules/framework.xml</org.ow2.frascati.fio.packages>
		<org.ow2.frascati.fio.jboss.packages>../../bundles/frascati-modules/jboss-osgi-framework.properties</org.ow2.frascati.fio.jboss.packages>
		<org.ow2.frascati.fio.jboss.log4j>../../bundles/frascati-modules/log4j.xml</org.ow2.frascati.fio.jboss.log4j>
		<org.ow2.frascati.fio.launch>../../bundles/frascati-modules/launch_bundles.xml</org.ow2.frascati.fio.launch>
		<org.ow2.frascati.fio.install>../install_bundles.xml</org.ow2.frascati.fio.install>
	</properties>
	
    <dependencies>
        <dependency>
            <groupId>biz.aQute</groupId>
            <artifactId>bnd</artifactId>
            <version>0.0.384</version>
	      	<scope>provided</scope>
        </dependency>
        <!-- TEST DEPENDENCIES -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.6</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <modules>
        <module>framework</module>
        <module>felix</module>
        <module>equinox</module>
        <module>jboss</module>
        <module>knopflerfish</module>
        <module>concierge</module>
    </modules>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <configuration>
                        <source>1.5</source>
                        <target>1.5</target>
                    </configuration>
                </plugin>
	        	<plugin>
	        		<groupId>org.apache.maven.plugins</groupId>
	        		<artifactId>maven-surefire-plugin</artifactId>
	        		<configuration>
						<argLine>
						-Dorg.ow2.frascati.binding.uri.base=http://localhost:8765
						-Dorg.ow2.frascati.fio.packages=${org.ow2.frascati.fio.packages}
						-Dorg.ow2.frascati.fio.jboss.packages=${org.ow2.frascati.fio.jboss.packages}
						-Dorg.ow2.frascati.fio.jboss.log4j=${org.ow2.frascati.fio.jboss.log4j}
						-Dorg.ow2.frascati.fio.launch=${org.ow2.frascati.fio.launch}
						-Dorg.ow2.frascati.fio.install=${org.ow2.frascati.fio.install}</argLine>
	        		</configuration>
	        	</plugin> 
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-dependency-plugin</artifactId>
                    <version>2.0</version>
                    <executions>
                        <execution>
                            <id>copy-test-bundles</id>
                            <phase>generate-resources</phase>
                            <goals>
                                <goal>copy</goal>
                            </goals>
                            <configuration>
                                <outputDirectory>${project.basedir}/target/test-classes</outputDirectory>
                                <stripVersion>true</stripVersion>
                                <failBuild>false</failBuild>
                                <failOnMissingClassifierArtifact>true</failOnMissingClassifierArtifact>
                                <failOnWarning>false</failOnWarning>
                                <overWriteIfNewer>true</overWriteIfNewer>
                                <artifactItems>
                                    <artifactItem>
                                        <groupId>org.apache.felix</groupId>
                                        <artifactId>org.apache.felix.log</artifactId>
                                        <version>1.0.0</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-cxf-libs</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-resources</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-eclipse</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-sca-parser-fragment</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi</groupId>
                                        <artifactId>bundle-frascati-api</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-core</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                    	<groupId>org.ow2.frascati.osgi.fio</groupId>
    									<artifactId>bundle-frascati-osgi-r3-concierge</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                    	<groupId>org.ow2.frascati.osgi.fio</groupId>
    									<artifactId>bundle-frascati-osgi-r4</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                    	<groupId>org.ow2.frascati.osgi.fio</groupId>
    									<artifactId>bundle-frascati-util</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.fio</groupId>
                                        <artifactId>bundle-frascati-activator</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.examples</groupId>
                                        <artifactId>helloworld-binding-frascati-osgi-server</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.examples</groupId>
                                        <artifactId>helloworld-binding-frascati-osgi-client</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.examples</groupId>
                                        <artifactId>helloworld-binding-osgi-frascati-client</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.examples</groupId>
                                        <artifactId>helloworld-binding-osgi-frascati-server2</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.examples</groupId>
                                        <artifactId>helloworld-fractal</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <artifactItem>
                                        <groupId>org.ow2.frascati.osgi.examples</groupId>
                                        <artifactId>frascati-osgi-dynamic-bundle</artifactId>
                                        <version>${project.version}</version>
                                    </artifactItem>
                                    <!-- CONCIERGE BUNDLES -->
									<artifactItem>
										<groupId>org.ow2.frascati.osgi.fio</groupId>
										<artifactId>bundle-frascati-concierge-util</artifactId>
										<version>${project.version}</version>
									</artifactItem>
                                    <!-- JBOSS BUNDLES -->
									<artifactItem>
										<groupId>org.ow2.frascati.osgi.fio</groupId>
										<artifactId>bundle-frascati-jboss-util</artifactId>
										<version>${project.version}</version>
									</artifactItem>
									<!-- MINIMAL -->
									<artifactItem>
										<groupId>org.jboss.osgi.hotdeploy</groupId>
										<artifactId>jbosgi-hotdeploy</artifactId>
										<version>1.0.10</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.common</groupId>
										<artifactId>jboss-osgi-common</artifactId>
										<version>1.0.6</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.jaxb</groupId>
										<artifactId>jboss-osgi-jaxb</artifactId>
										<version>2.1.10.SP5</version>
									</artifactItem>
									<!-- DEFAULT -->
									<artifactItem>
										<groupId>org.jboss.osgi.common</groupId>
										<artifactId>jboss-osgi-common-core</artifactId>
										<version>2.2.17.SP1</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.jmx</groupId>
										<artifactId>jboss-osgi-jmx</artifactId>
										<version>1.0.10</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.aries.jmx</groupId>
										<artifactId>org.apache.aries.jmx</artifactId>
										<version>0.3</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.aries</groupId>
										<artifactId>org.apache.aries.util</artifactId>
										<version>0.3</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.felix</groupId>
										<artifactId>org.apache.felix.configadmin</artifactId>
										<version>1.2.8</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.felix</groupId>
										<artifactId>org.apache.felix.eventadmin</artifactId>
										<version>1.2.6</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.felix</groupId>
										<artifactId>org.apache.felix.scr</artifactId>
										<version>1.6.0</version>
									</artifactItem>
									<!-- WEB -->
									<artifactItem>
										<groupId>org.apache.felix</groupId>
										<artifactId>org.apache.felix.metatype</artifactId>
										<version>1.0.4</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.apache.felix</groupId>
										<artifactId>org.apache.felix.webconsole</artifactId>
										<version>3.1.6.SP1</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.http</groupId>
										<artifactId>jboss-osgi-http</artifactId>
										<version>1.0.3</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.webconsole</groupId>
										<artifactId>jbosgi-webconsole</artifactId>
										<version>1.0.6</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.webapp</groupId>
										<artifactId>jbosgi-webapp</artifactId>
										<version>1.0.2</version>
									</artifactItem>
									<!-- ALL -->
									<artifactItem>
										<groupId>org.jboss.osgi.blueprint</groupId>
										<artifactId>jbosgi-blueprint</artifactId>
										<version>1.0.2</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.jndi</groupId>
										<artifactId>jboss-osgi-jndi</artifactId>
										<version>1.0.4</version>
									</artifactItem>
									<artifactItem>
										<groupId>org.jboss.osgi.xerces</groupId>
										<artifactId>jboss-osgi-xerces</artifactId>
										<version>2.9.1.SP7</version>
									</artifactItem>
                                </artifactItems>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

</project>
