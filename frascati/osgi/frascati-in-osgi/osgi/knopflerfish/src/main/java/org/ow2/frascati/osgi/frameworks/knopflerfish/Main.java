/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.frameworks.knopflerfish;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.knopflerfish.framework.FrameworkFactoryImpl;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

import java.util.logging.Level;


public class Main extends org.ow2.frascati.osgi.frameworks.Main<Framework>
{
    public Main() throws IOException, BundleException
    {
        super();
        File cache = createCacheDirectory("knopflerfish");
        Map<String, String> fwProps = new HashMap<String, String>();
        fwProps.put("org.osgi.provisioning.spid", "knopflerfish");
        fwProps.put("org.osgi.framework.storage", cache.getAbsolutePath());
        fwProps.put("org.knopflerfish.osgi.setcontextclassloader", "false");
        fwProps.put("org.knopflerfish.osgi.registerserviceurlhandler", "true");
        fwProps.put("org.knopflerfish.log.memory.size", "250");
        fwProps.put("org.knopflerfish.log.level", "debug");
        fwProps.put("org.knopflerfish.log.grabio", "false");
        fwProps.put("org.knopflerfish.log.file", "false");
        fwProps.put("org.knopflerfish.log.out", "true");
        fwProps.put("org.knopflerfish.gosg.jars", "");
        fwProps.put("org.knopflerfish.framework.system.packages.base",SYSTEM_PACKAGE_BASE);
        String[] versions = System.getProperty("java.version").split("\\.");
        String version = versions[0] + "." + versions[1];
        fwProps.put("org.knopflerfish.framework.system.packages.version",version);
        // fwProps.put("org.knopflerfish.framework.bundlestorage", "file");
        // fwProps.put("org.knopflerfish.framework.bundlestorage.checksigned","true");
        // fwProps.put("org.knopflerfish.framework.bundlestorage.file.reference","true");
        // fwProps.put("org.knopflerfish.framework.bundlestorage.file.unpack","false");
        fwProps.put("org.knopflerfish.framework.bundlestorage.file.trusted", "true");
        // fwProps.put("org.knopflerfish.framework.bundlestorage.file.always_unpack","false");
        // fwProps.put("org.knopflerfish.framework.startlevel.compat", "false");
        fwProps.put("org.knopflerfish.framework.patch.configurl", "!!/patches.props");
        // fwProps.put("org.knopflerfish.framework.automanifest.config","!!/automanifest.props");
        // fwProps.put("org.knopflerfish.framework.is_doublechecked_locking_safe", "true");
        // fwProps.put("org.knopflerfish.framework.strictbootclassloading","false");
        // fwProps.put("org.knopflerfish.framework.all_signed", "false");
        fwProps.put("org.knopflerfish.framework.automanifest", "false");
        // fwProps.put("org.knopflerfish.framework.main.class.activation", "");
        // fwProps.put("org.knopflerfish.framework.patch", "false");
        // fwProps.put("org.knopflerfish.framework.ldap.nocache", "false");
        // fwProps.put("org.knopflerfish.framework.service.permissionadmin","true");
        // fwProps.put("org.knopflerfish.framework.service.conditionalpermissionadmin","true");
        // fwProps.put("org.knopflerfish.framework.patch.dumpclasses.dir","patchedclasses");
        // fwProps.put("org.knopflerfish.framework.patch.dumpclasses", "false");
        // fwProps.put("org.knopflerfish.startlevel.use", "true");
        // fwProps.put("org.knopflerfish.framework.validator", "none");
        FrameworkFactory factory = new FrameworkFactoryImpl();
        instance = factory.newFramework(fwProps);
        instance.start();
    }

    /**
     * @param args
     * @throws BundleException
     * @throws InterruptedException
     * @throws IOException
     */
    public static void main(String[] args) throws InterruptedException,
            BundleException, IOException
    {
        Main main = new Main();
        String clientOrServer = null;
        main.launch((args == null || args.length == 0 || 
                (clientOrServer = args[0]) == null) ? "" : clientOrServer);
    }

    @Override
    public BundleContext getSystemBundleContext()
    {
        return instance.getBundleContext();
    }

    @Override
    public Bundle getBundle(long id)
    {
        return instance.getBundleContext().getBundle(id);
    }

    @Override
    public void kill()
    {
        try
        {
            instance.stop();
            
        } catch (BundleException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        instance = null;
    }
}
