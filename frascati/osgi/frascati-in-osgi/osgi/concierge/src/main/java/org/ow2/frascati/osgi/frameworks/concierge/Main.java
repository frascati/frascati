package org.ow2.frascati.osgi.frameworks.concierge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import java.util.logging.Level;
import org.objectweb.fractal.juliac.osgi.concierge.Concierge;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

public class Main extends org.ow2.frascati.osgi.frameworks.Main<Concierge>
{
    public Main() throws IOException, BundleException
    {
        super();
        //Where is the list of bundles to install
        System.setProperty("org.ow2.frascati.fio.install",
                "src/test/resources/install_bundles.xml");
        
        //Where is list of bundles to start
        System.setProperty("org.ow2.frascati.fio.launch",
            "src/test/resources/launch_bundles.xml");
        
        //What is the name of the OSGiRevisionItf implementation class
        System.setProperty("juliac.osgi.revision","ConciergeRevision");
        
        File cache = createCacheDirectory("concierge");
        InputStream is = Thread.currentThread(
                ).getContextClassLoader().getResourceAsStream("init.xargs");

        String path = cache.getAbsolutePath() + File.separator + "storage";
        new File(path).mkdir();
        path = cache.getAbsolutePath() + File.separator + "init.xargs";

        if (write(is, path))
        {
            System.setProperty("xargs", path);
        }
        String autoInstallProperty = System.getProperty("osgi.auto.start.1", "");
        autoInstallProperty = new StringBuilder(
                "shell-1.0.0.jar service-tracker-1.0.0.jar event-admin-1.0.0.jar ")
                .append(autoInstallProperty).toString();

        System.setProperty("osgi.auto.start.1", autoInstallProperty);
        System.setProperty("osgi.auto.install.1", " ");

        StringTokenizer tokenizer = new StringTokenizer(autoInstallProperty);
        List<String> bundles = new ArrayList<String>();
        while (tokenizer.hasMoreElements())
        {
            bundles.add(tokenizer.nextToken());
        }
        installResources(cache, bundles);
        instance = Concierge.getInstance(cache);
    }

    /**
     * Prepare the framework by installing some useful Bundles
     */
    @Override
    public void init()
    {
          logger.log(Level.INFO,"Empty 'init' method");
    }
    
    /**
     * @param args
     * @throws BundleException
     * @throws InterruptedException
     * @throws IOException
     */
    public static void main(String[] args) throws InterruptedException,
            BundleException, IOException
    {
        Main main = new Main();
        String clientOrServer = null;
        // Install bundles needed to use the felix web console
        System.setProperty("org.osgi.service.http.port", "54468");
        main.launch((args == null || args.length == 0 || (clientOrServer = args[0]) == null) ? ""
                : clientOrServer);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getSystemBundleContext()
     */
    public BundleContext getSystemBundleContext()
    {
        return instance.getBundleContext();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getBundle(long)
     */
    public Bundle getBundle(long id)
    {
        return getSystemBundleContext().getBundle(id);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#kill()
     */
    public void kill()
    {
        instance.stop();
        instance = null;
    }

    private void installResources(File cache, List<String> bundles)
    {
        String[] resources = bundles.toArray(new String[0]);

        InputStream is = null;
        String path = null;

        File dataStore = new File(cache.getAbsolutePath() + File.separator
                + "data");
        dataStore.mkdir();

        for (String resource : resources)
        {
            is = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(resource);
            path = cache.getAbsolutePath() + File.separator + "data"
                    + File.separator + resource;
            write(is, path);
        }
    }

    private boolean write(InputStream is, String path)
    {
        int BUFFER_SIZE = 1024 * 64;

        if (is == null)
        {
            System.out.println("Unable to write file : '" + path + "'");
            return false;

        } else
        {
            byte[] buffer = new byte[BUFFER_SIZE];
            File file = new File(path);
            if (file.exists())
            {
                return true;
            }
            FileOutputStream fos = null;
            try
            {
                fos = new FileOutputStream(file);
                int len;
                while ((len = is.read(buffer)) >= 0)
                {
                    fos.write(buffer, 0, len);
                }
            } catch (FileNotFoundException e)
            {
                // e.printStackTrace();
                return false;
            } catch (IOException e)
            {
                // e.printStackTrace();
                return false;
            } catch (NullPointerException e)
            {
                // e.printStackTrace();
                return false;
            }
            // close both streams.
            try
            {
                is.close();
                fos.close();
                return true;
            } catch (IOException e)
            {
                // e.printStackTrace();
            } catch (NullPointerException e)
            {
                // e.printStackTrace();
            }
        }
        return false;
    }
}
