/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.frameworks.jboss;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

import org.jboss.net.protocol.URLStreamHandlerFactory;
import org.jboss.osgi.spi.framework.OSGiBootstrap;
import org.jboss.osgi.spi.framework.OSGiBootstrapProvider;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;

import java.util.logging.Level;

public class Main extends org.ow2.frascati.osgi.frameworks.Main<Framework>
{
    private final static String CONFIGURATION_MINIMAL = "minimal";
    private final static String CONFIGURATION_DEFAULT = "default";
    private final static String CONFIGURATION_WEB = "web";
    private final static String CONFIGURATION_ALL = "all";
    private final static int CONFIGURATION_MINIMAL_INDEX = 4;
    private final static int CONFIGURATION_DEFAULT_INDEX = 11;
    private final static int CONFIGURATION_WEB_INDEX = 15;
    private final static int CONFIGURATION_ALL_INDEX = 18;

    private static String[] deployables = new String[]
    {
            // <!-- MINIMAL -->
            "bundle-frascati-jboss-util.jar",
            "jboss-osgi-common.jar",
            "jbosgi-hotdeploy.jar",
            "jboss-osgi-jaxb.jar",
            // <!-- DEFAULT -->
            "org.apache.felix.configadmin.jar",
            "org.apache.felix.eventadmin.jar", "org.apache.felix.scr.jar",
            "jboss-osgi-common-core.jar", "org.apache.aries.util.jar",
            "org.apache.aries.jmx.jar",
            "jboss-osgi-jmx.jar",
            // <!-- WEB -->
            "org.apache.felix.metatype.jar", "jboss-osgi-http.jar",
            "jbosgi-webconsole.jar", "jbosgi-webapp.jar",
            // <!-- ALL -->
            "jbosgi-blueprint.jar", "jboss-osgi-jndi.jar",
            "jboss-osgi-xerces.jar" };
    
    private static URLStreamHandlerFactory ushFactory;

    public Main() throws Exception
    {
        super();
        File cache = createCacheDirectory("jboss");
        try
        {
            String log4jPropsFilePath = System.getProperty(
                    org.ow2.frascati.osgi.frameworks.Main.LOG4J_CONFIGURATION);
            if(log4jPropsFilePath == null)
            {
                log4jPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("log4j.xml").toString(); 
            }
            URL log4jConfFileURL = new File(log4jPropsFilePath).toURI().toURL();            
            System.setProperty("log4j.configuration", log4jConfFileURL
                    .toExternalForm());
            
        } catch (MalformedURLException e)
        {
            logger.info("Unable to initialize log4j properly");
        }
        System.setProperty("osgi.server.home", cache.getAbsolutePath());
        System.setProperty("org.osgi.framework.storage", new StringBuilder(
                cache.getAbsolutePath()).append(File.separator).append("data")
                .append(File.separator).append("osgi-store").toString());
        String deployPath = new StringBuilder(cache.getAbsolutePath()).append(
                File.separator).append("deploy").toString();
        new File(deployPath).mkdir();
        System.setProperty("org.jboss.osgi.hotdeploy.scandir", deployPath);
        System.setProperty("org.osgi.framework.storage.clean", "onFirstInit");
        System.setProperty("jboss.bind.address", "localhost");
        System.setProperty("jboss.vfs.leakDebugging", "true");
        System.setProperty("java.net.preferIPv4Stack", "true");
        String handlerPkgs = System.getProperty("java.protocol.handler.pkgs",
                "");
        System.setProperty("java.protocol.handler.pkgs", new StringBuilder(
                handlerPkgs).append(handlerPkgs.length() > 0 ? "|" : "")
                .append("org.jboss.net.protocol|").append(
                        "org.jboss.virtual.protocol|").append(
                        "org.jboss.vfs.protocol").toString());

        String frameworkPropsFilePath = System.getProperty(
                org.ow2.frascati.osgi.frameworks.Main.JBOSS_FRAMEWORK_EXPORTED_PACKAGES);
        if(frameworkPropsFilePath == null)
        {
            frameworkPropsFilePath = new StringBuilder(resources)
            .append(File.separator).append("jboss-osgi-framework.properties").toString(); 
        }
        InputStream configurationFileStream = new URL("file", "",                
                new File(frameworkPropsFilePath).getAbsoluteFile().getAbsolutePath()).openStream();
        try
        {
            URLStreamHandlerFactory urlStreamHandlerFactory = new URLStreamHandlerFactory();
            URL.setURLStreamHandlerFactory(urlStreamHandlerFactory);
            ushFactory = urlStreamHandlerFactory;
        } catch (Throwable e)
        {
            logger.info("URL.setURLStreamHandlerFactory Error :" + e.getMessage());
        }
        OSGiBootstrapProvider bootProvider = OSGiBootstrap.getBootstrapProvider();
        bootProvider.configure(configurationFileStream);
        try
        {
            configurationFileStream.close();
            
        }catch(IOException e)
        {
            if(logger.isLoggable(Level.WARNING))
            {
                logger.log(Level.WARNING,e.getMessage(),e);
            }
        }
        instance = bootProvider.getFramework();
        instance.start();
    }

    @Override
    public void init()
    {
        super.init();
        int index = 0;
        String config = System
                .getProperty("jbossosgi.configuration", "minimal");
        if (config.equals(CONFIGURATION_MINIMAL))
        {
            index = CONFIGURATION_MINIMAL_INDEX;
        } else if (config.equals(CONFIGURATION_DEFAULT))
        {
            index = CONFIGURATION_DEFAULT_INDEX;
        } else if (config.equals(CONFIGURATION_WEB))
        {
            index = CONFIGURATION_WEB_INDEX;
        } else if (config.equals(CONFIGURATION_ALL))
        {
            index = CONFIGURATION_ALL_INDEX;
        }
        String[] deploys = new String[index];
        System.arraycopy(deployables, 0, deploys, 0, index);
        try
        {
            launchBundles(getSystemBundleContext(), deploys);
            
        } catch (InterruptedException e)
        {
            logger.log(Level.SEVERE,e.getMessage(),e);
            
        } catch (BundleException e)
        {
            logger.log(Level.SEVERE,e.getMessage(),e);
        }
    }

    @Override
    protected void installBundles(BundleContext context, String[] locations)
            throws InterruptedException, BundleException
    {
        if (context == null)
        {
            logger.info("BundleContext is null");
            return;
        }
        String[] bundles = locations;
        int n = 0;
        for (; n < bundles.length; n++)
        {
            try
            {
                String jarName = bundles[n];
                String jarPath = new StringBuilder(resources).append(
                        File.separator).append(jarName).toString();
                if (installed.add(context.installBundle(jarPath)))
                {
                    logger.info(installed.get(installed.size() - 1).getSymbolicName()
                            + " installed");
                } else
                {
                    logger.info("Error occured while installing " + jarPath);
                }
            } catch (Exception e)
            {
                logger.log(Level.SEVERE,e.getMessage(),e);
            }
        }
    }

    /**
     * @param args
     * @throws BundleException
     * @throws InterruptedException
     * @throws IOException
     */
    public static void main(String[] args) throws Exception
    {
        int a = 1;
        if (args != null && args.length >= 1)
        {
            for (; a < args.length; a++)
            {
                String arg = args[a];
                String[] argEls = arg.split("=");
                if (argEls.length == 2)
                {
                    System.setProperty(argEls[0], argEls[1]);
                }
            }
        }
        Main main = new Main();
        String clientOrServer = null;
        main.launch(((clientOrServer = args[0]) == null) ? "" : clientOrServer);
    }

    /**
     * Install and launch bundles in the OSGi Framework
     * 
     * @param example
     *            the String value of the example.name command line parameter value
     */
    @Override
    // The method is overwritten for the Windows OS - "file:" prefix causes an exception
    public void launch(String example) throws InterruptedException,
            BundleException
    {
        init();
        try
        {
            String installPropsFilePath = System.getProperty(
                org.ow2.frascati.osgi.frameworks.Main.INSTALL_BUNDLES);
            if(installPropsFilePath == null)
            {
                installPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("install_bundles.xml").toString(); 
            }
            URL installedBundlesPropsURL = new File(installPropsFilePath).toURI().toURL();            
            Properties install_bundles = new Properties();
            InputStream inputStream = installedBundlesPropsURL.openStream();
            install_bundles.loadFromXML(inputStream);
            try
            {
                inputStream.close();
                
            }catch(IOException e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
            String[] props = install_bundles.keySet().toArray(new String[0]);
            int n = 0;
            while (n < props.length)
            {
                String bundleNum = props[n++];
                String[] jarData = install_bundles.getProperty(
                        bundleNum).split(":");
                String toInstall = jarData[1];
                String jarName = jarData[0];
                if ("true".equals(toInstall))
                {
                    try
                    {
                        getSystemBundleContext().installBundle(
                                new StringBuilder(resources).append(
                                        File.separator).append(jarName)
                                        .toString());
                    } catch (Exception e)
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }

            String launchPropsFilePath = System.getProperty(
                org.ow2.frascati.osgi.frameworks.Main.LAUNCH_BUNDLES);
            if(launchPropsFilePath == null)
            {
                launchPropsFilePath = new StringBuilder(resources)
                .append(File.separator).append("launch_bundles.xml").toString(); 
            }
            URL launchedBundlesPropsURL = new File(launchPropsFilePath).toURI().toURL();
            Properties launch_bundles = new Properties();
            inputStream = launchedBundlesPropsURL.openStream();
            launch_bundles.loadFromXML(inputStream);
            try
            {
                inputStream.close();
                
            }catch(IOException e)
            {
                if(logger.isLoggable(Level.WARNING))
                {
                    logger.log(Level.WARNING,e.getMessage(),e);
                }
            }
            props = launch_bundles.keySet().toArray(new String[0]);
            n = 0;
            while (n < props.length)
            {
                String bundleNum = props[n++];
                String[] jarData = launch_bundles.getProperty(bundleNum).split(":");
                String toInstall = jarData[1];
                String jarName = jarData[0];
                if ("true".equals(toInstall))
                {
                    try
                    {
                        getSystemBundleContext().installBundle(
                                new StringBuilder(resources).append(
                                        File.separator).append(jarName)
                                        .toString()).start();
                    } catch (Exception e)
                    {
                        logger.log(Level.WARNING,e.getMessage(),e);
                    }
                }
            }
        } catch (InvalidPropertiesFormatException e)
        {
            logger.log(Level.SEVERE,e.getMessage(),e);
            
        } catch (IOException e)
        {
            logger.log(Level.SEVERE,e.getMessage(),e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getSystemBundleContext()
     */
    public BundleContext getSystemBundleContext()
    {
        return instance.getBundleContext();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#getBundle(long)
     */
    public Bundle getBundle(long id)
    {
        return getSystemBundleContext().getBundle(id);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.osgi.frameworks.Main#kill()
     */
    public void kill()
    {
        try
        {
            instance.stop();
            
        } catch (BundleException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
        instance = null;
    }
}
