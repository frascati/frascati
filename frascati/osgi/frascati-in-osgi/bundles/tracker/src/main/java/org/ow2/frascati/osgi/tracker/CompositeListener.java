/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 *
 * Contributor(s): Christophe Munilla
 *
 */
package org.ow2.frascati.osgi.tracker;

import java.util.Dictionary;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleEvent;
import org.osgi.framework.BundleListener;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;
import org.ow2.frascati.osgi.processor.OSGiResourceProcessor;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;

public class CompositeListener implements BundleListener
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private static final Logger log = Logger.getLogger(CompositeListener.class
            .getCanonicalName());
    private FraSCAtiOSGiService service;
    private OSGiResourceProcessor processor;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * Constructor
     */
    public CompositeListener()
    {
        log.log(Level.CONFIG, "init CompositeListener");
    }

    /**
     * Define the FraSCAti service
     * 
     * @param service
     */
    public void setFraSCAtiService(FraSCAtiOSGiService service)
    {
        log.log(Level.CONFIG, "set FraSCAtiOSGiService " + service);
        this.service = service;
        processor = new OSGiResourceProcessor(service);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.framework.BundleListener#bundleChanged(org.osgi.framework.BundleEvent)
     */
    public void bundleChanged(BundleEvent bundleEvent)
    {
        if (service == null)
        {
            return;
        }
        Bundle bundle = bundleEvent.getBundle();
        if (bundleEvent.getType() == BundleEvent.UNINSTALLED)
        {
            service.unloadSCA(bundle.getBundleId());
        } else if (bundleEvent.getType() == BundleEvent.RESOLVED)
        {
            if (OSGiIOUtils.getHeader(bundle, OSGiIOUtils.FRAGMENT_HEADER) == null)
            {
                //processor.bundleResolved(bundle);
                log.info("Bundle '" + getSymbolicName(bundle)
                        + "' resolved - OSGiProcessor called");
            }
        }
    }
    
    public String getSymbolicName(Bundle bundle)
    {
        Dictionary<String,String> headers = bundle.getHeaders();
        String symbolicName = headers.get("Bundle-SymbolicName");
        return symbolicName;
    }
}
