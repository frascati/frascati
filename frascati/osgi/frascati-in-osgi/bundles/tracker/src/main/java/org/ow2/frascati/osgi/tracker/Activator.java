/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 *
 * Contributor(s): Christophe Munilla
 *
 */
package org.ow2.frascati.osgi.tracker;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;

/**
 * @see org.osgi.framework.BundleActivator
 */
public class Activator implements BundleActivator
{
    private FrascatiServiceTracker customizerFrascatiTracker;
    private CompositeListener loader;
    private ServiceTracker fraSCAtiTracker;
    private static Logger logger = Logger.getLogger(Activator.class.getCanonicalName());

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
     */
    public void start(BundleContext context) throws Exception
    {
        loader = new CompositeListener();
        context.addBundleListener(loader);
        customizerFrascatiTracker = new FrascatiServiceTracker(loader);
        fraSCAtiTracker = new ServiceTracker(context,
                FraSCAtiOSGiService.class.getCanonicalName(),
                customizerFrascatiTracker);
        fraSCAtiTracker.open();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
     */
    public void stop(BundleContext context) throws Exception
    {
        customizerFrascatiTracker.deactivate();
        fraSCAtiTracker.close();
    }
}
