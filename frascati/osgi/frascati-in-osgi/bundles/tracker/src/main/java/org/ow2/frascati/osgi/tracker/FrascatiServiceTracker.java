/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.tracker;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTrackerCustomizer;
import org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService;

/**
 * @see org.osgi.util.tracker.ServiceTrackerCustomizer
 */
public class FrascatiServiceTracker implements ServiceTrackerCustomizer
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    private static final Logger logger = Logger.getLogger(FrascatiServiceTracker.class.getCanonicalName());
    private CompositeListener compositeListener;
    private FraSCAtiOSGiService frascatiService;
    private List<FraSCAtiOSGiService> services;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * @param compositeListener
     */
    public FrascatiServiceTracker(CompositeListener compositeListener)
    {
        logger.log(Level.CONFIG, "init FraSCAti Tracker");
        this.compositeListener = compositeListener;
        services = new ArrayList<FraSCAtiOSGiService>();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#addingService(org.osgi.framework
     *      .ServiceReference)
     */
    public Object addingService(ServiceReference reference)
    {
        FraSCAtiOSGiService frService = null;
        try
        {
            frService = (FraSCAtiOSGiService) reference.getBundle()
                    .getBundleContext().getService(reference);
        } catch (ClassCastException e)
        {
            logger.log(Level.WARNING,
                    reference.getBundle().getBundleContext()
                            .getService(reference)
                            + " cannot be cast into "
                            + FraSCAtiOSGiService.class.getCanonicalName());
            return null;
        }
        synchronized (frService)
        {
            if (services.contains(frService))
            {
                return frService;
            }
            services.add(frService);
            logger.log(Level.INFO, "Tracker register FraSCAtiOSGiService :"
                    + frService);
            if (frascatiService == null)
            {
                compositeListener.setFraSCAtiService(frService);
                frascatiService = frService;
            }
        }
        return frService;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#modifiedService(org.osgi.framework
     *      .ServiceReference, java.lang.Object)
     */
    public void modifiedService(ServiceReference reference, Object service)
    {
        removedService(reference, service);
        addingService(reference);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.osgi.util.tracker.ServiceTrackerCustomizer#removedService(org.osgi.framework
     *      .ServiceReference, java.lang.Object)
     */
    public void removedService(ServiceReference reference, Object service)
    {
        FraSCAtiOSGiService frService = (FraSCAtiOSGiService) service;
        synchronized (frService)
        {
            services.remove(frService);
            if (service == frService)
            {
                frascatiService = null;
                logger.info("FraSCAtiOSGiService removed");
                if (services.size() > 0)
                {
                    compositeListener
                            .setFraSCAtiService((FraSCAtiOSGiService) services
                                    .get(0));
                } else
                {
                    compositeListener.setFraSCAtiService(null);
                }
            }
        }
    }

    /**
     * 
     */
    public void deactivate()
    {
        // TODO : unload
        services.clear();
    }
}
