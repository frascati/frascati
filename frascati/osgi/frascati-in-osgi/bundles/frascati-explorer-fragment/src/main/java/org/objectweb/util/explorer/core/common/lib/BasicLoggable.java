package org.objectweb.util.explorer.core.common.lib;

import org.objectweb.util.monolog.Monolog;
import org.objectweb.util.monolog.api.Loggable;
import org.objectweb.util.monolog.api.Logger;
import org.objectweb.util.monolog.api.LoggerFactory;
import org.objectweb.util.trace.TraceTemplate;

public class BasicLoggable implements Loggable
{
      protected LoggerFactory factory;  
      protected Logger logger;
      private TraceTemplate trace;
    
      protected TraceTemplate getTrace()
      {
          if(this.trace == null)
          {
              this.trace = new TraceTemplate(java.util.logging.Logger.getLogger(
                              getClass().getCanonicalName()));
          }
         return this.trace;
      }
    
      protected void setTrace(TraceTemplate trace)
      {
          this.trace = trace;
      }
    
      public Logger getLogger()
      {
          if(this.logger == null)
          {
              this.logger = Monolog.monologFactory.getLogger(
                      getClass().getCanonicalName());
          }
          return this.logger;
      }
    
      public void setLogger(Logger log)
      {
         setTrace(new TraceTemplate(log));
      }
    
      public LoggerFactory getLoggerFactory()
      {
          return this.factory;
      }
    
      public void setLoggerFactory(LoggerFactory loggerFactory)
      {
          this.factory = loggerFactory;
      }
}