/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.core;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.URIHandlerImpl;
import org.ow2.frascati.util.resource.frascati.Handler;

/**
 * URIHandler to handle the 'frascati' protocol
 */
public class FrascatiURIHandler extends URIHandlerImpl
{
    /**
     * 'frascati' scheme
     */
    private static final String SCHEME_FRASCATI = "frascati";
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl
     * #canHandle(org.eclipse.emf.common.util.URI)
     */
    @Override
    public boolean canHandle(URI uri)
    {
        return SCHEME_FRASCATI.equals(uri.scheme());
    }
    

    /** 
     * {@inheritDoc}
     * 
     * @see org.eclipse.emf.ecore.resource.impl.URIHandlerImpl
     * #createInputStream(org.eclipse.emf.common.util.URI, java.util.Map)
     */
    @Override
    public InputStream createInputStream(URI uri, Map<?, ?> options) throws IOException
      {
        try
        {
            int port = -1;
            if(uri.port() != null && uri.port().length()>0)
            {
                port = Integer.parseInt(uri.port());
            }
            String host = uri.host();
            String path = null;
            
            if(uri.toString().contains("#"))
            {           
                String[] fragments = uri.fragment().split("/");
                
                host = host + "#" + fragments[0]; 
                
                StringBuilder pathBuilder = new StringBuilder();
                for(int index=1;index<fragments.length;index++)
                {
                    pathBuilder.append(fragments[index]);
                    if(index <fragments.length -1)
                    {
                        pathBuilder.append('/'); 
                    }
                }
                path = pathBuilder.toString();
                
            } else
            {
                path = uri.path();
            }
            URL url = new URL(SCHEME_FRASCATI, host, port, path, new Handler());
          
          final URLConnection urlConnection = url.openConnection();
          InputStream result = urlConnection.getInputStream();
          Map<Object, Object> response = getResponse(options);
          if (response != null)
          {
              response.put(URIConverter.RESPONSE_TIME_STAMP_PROPERTY, 
                      urlConnection.getLastModified());
          }
          return result;
        }
        catch (RuntimeException exception)
        {
            throw new Resource.IOWrappedException(exception);
        }
      }
}
