/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.fscript;

import java.util.logging.Level;
import org.objectweb.fractal.fscript.jsr223.FScriptEngineFactoryProxy;
import org.ow2.frascati.starter.api.AbstractInitializable;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * The FraSCAtiOSGiFscriptRegisterer defines the 'fscript-factory' System property to 
 * be able to use implementation.script with FScript in the FraSCAtiOSGi execution context
 * 
 * Moreover the client Component has to initialize a ScriptManager using the FraSCAti 
 * instance's ClassLoader
 */
public class FraSCAtiOSGiFScriptRegisterer extends AbstractInitializable
{
    private Logger logg = Logger.getLogger(FraSCAtiOSGiFScriptRegisterer.class.getCanonicalName());
    
    /**
     * Define the 'fscript-factory' System property
     * */
    public void doInitialize()
    {
        logg.log(Level.INFO,"do initialize FraSCAtiOSGiFScriptRegisterer");
        
        System.setProperty(
                FScriptEngineFactoryProxy.SCRIPT_ENGINE_FACTORY_PROPERTY_NAME,
                "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory");
    }
}
