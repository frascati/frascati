/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;

import java.lang.management.ManagementFactory;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.junit.Test;
import org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest;

/**
 * FraSCAti in OSGi JMX Bundle TestCase
 */
public abstract class FraSCAtiInOSGiJMXTest extends FraSCAtiInOSGiTest
{
    public static final String PACKAGE = "org.ow2.frascati.jmx";
    public static final String DOMAIN = "SCA domain";
    public static final String STARTED = "STARTED";
    public static final String STOPPED = "STOPPED";
    
    @Test
    public void testJMX() throws Exception
    {
        System.out.println("... do testJMX");

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        mbs.invoke(new ObjectName(PACKAGE + ":name=FrascatiJmx"),
                "load", null, null);
        // check there a some exposed components
        Set<ObjectName> components = mbs.queryNames(new ObjectName(DOMAIN + ":*"), null);
        Assert.assertFalse(components.isEmpty());
        
        // check state of a component
        ObjectName tested = new ObjectName(DOMAIN  + ":name0=org.ow2.frascati.FraSCAti,name1=jmx");
        Assert.assertEquals(STARTED, mbs.getAttribute(tested, "state"));
        mbs.invoke(tested, "stop", null, null);
        Assert.assertEquals(STOPPED, mbs.getAttribute(tested, "state"));
        mbs.invoke(tested, "start", null, null);
        Assert.assertEquals(STARTED, mbs.getAttribute(tested, "state"));

        System.out.println("... testJMX done");
    }
}