/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.test;

//import static org.junit.Assert.assertEquals;

import org.junit.Test;
import java.io.File;
//import java.net.URL;

import org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest;

//import com.gargoylesoftware.htmlunit.BrowserVersion;
//import com.gargoylesoftware.htmlunit.Page;
//import com.gargoylesoftware.htmlunit.WebClient;
//import com.gargoylesoftware.htmlunit.WebRequest;
//import com.gargoylesoftware.htmlunit.WebResponse;
//import com.gargoylesoftware.htmlunit.WebWindow;
//import com.gargoylesoftware.htmlunit.html.HtmlForm;
//import com.gargoylesoftware.htmlunit.html.HtmlPage;
//import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
//import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
//import com.gargoylesoftware.htmlunit.javascript.JavaScriptEngine;

/**
 * FraSCAti in OSGi Implementation Widget Bundle TestCase
 */
public abstract class FraSCAtiInOSGiImplementationWidgetTest extends FraSCAtiInOSGiTest
{
    @Test
    public void testImplementationWidget() throws Exception
    {
        System.out.println("... do testImplementationWidget");
        
        loadSCA(new File("target/test-classes/helloworld-widget.jar"
                ).getAbsoluteFile().toURI().toURL(),"helloworld-widget.composite");        
        
//        WebClient webClient = new WebClient();
//        URL widgetURL = new URL("http://localhost:8765/SayHelloWidget/widget.html");        
//        HtmlPage page1 = webClient.getPage(widgetURL);
//        
//        // Get the form that we are dealing with and within that form, 
//        // find the submit button and the field that we want to change.
//        HtmlForm form = page1.getForms().get(0);
//        HtmlSubmitInput button = form.getInputByName("sayHello");
//        HtmlTextInput textField = form.getInputByName("message");
//
//        // Change the value of the text field
//        textField.setValueAttribute("test");
//
//        // Now submit the form by clicking the button and get back the second page.
//        HtmlPage page2 = button.click();
//        form = page2.getForms().get(0);
//        assertEquals("Hello test!", form.getElementById("sayHelloResponse").getTextContent());        
//        webClient.closeAllWindows();
        
        unloadSCA("helloworld-widget");
        
        System.out.println("... testImplementationWidget done");
    }
}