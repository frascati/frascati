/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Test;
import org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest;

/**
 * FraSCAti in OSGi Implementation Scripts Bundle TestCase
 */
public abstract class FraSCAtiInOSGiImplementationScriptsTest extends FraSCAtiInOSGiTest
{
    @Test
    public void testImplementationScripts() throws MalformedURLException, Exception
    {
        System.out.println("... do testImplementationScripts");
        
        loadSCA(new File("target/test-classes/helloworld-script.jar"
                ).getAbsoluteFile().toURI().toURL(),"helloworld-script.composite");
        
        service.getClass().getDeclaredMethod("launch",
                new Class<?>[]{ String.class,String.class }).invoke(
                service, new Object[] { "helloworld-script", "runnable"});   
        
        unloadSCA("helloworld-script");
        
        System.out.println("... testImplementationScripts done");
    }
}