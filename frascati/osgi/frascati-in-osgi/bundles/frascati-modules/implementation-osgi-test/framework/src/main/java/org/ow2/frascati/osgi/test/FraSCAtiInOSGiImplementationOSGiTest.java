package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;

import org.junit.Test;
import org.osgi.framework.Bundle;
import org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest;

public abstract class FraSCAtiInOSGiImplementationOSGiTest extends FraSCAtiInOSGiTest
{
    @Test
    public void testImplementationOSGi() throws MalformedURLException, Exception
    {
        System.out.println("... do testImplementationOSGi");

        String framework = getClass().getSimpleName().substring(10);
        framework = framework.substring(0,framework.length()-4).toLowerCase();
        
        Bundle bundle = null;
        try
        {
            String bundleURLStr = new File(
                    "target/test-classes/helloworld-implementation-osgi-bundle.jar"
                           ).getAbsoluteFile().toURI().toURL().toExternalForm();
            
            bundle = main.getSystemBundleContext().installBundle(bundleURLStr);

        } catch(Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        } 
        assertNotNull("Unable to install helloworld-implementation-osgi-bundle.jar",
                bundle);
        bundle.start();
        
        //wait for tracker process
        boolean helloLoaded = false;
        int sleep = 0;   
        int sleepLimit = 30000;
        while(true)
        {
            try
            {
                sleep+=50;
                Thread.sleep(50);
                helloLoaded = (Boolean) service.getClass().getDeclaredMethod(
                        "loaded",new Class<?>[]{String.class}).invoke(service,
                                new Object[]{"HelloWorld-OSGi"});
                
                if(helloLoaded)
                {
                    break;
                    
                } else if(sleep > sleepLimit)
                {
                    String loading =(String) service.getClass().getDeclaredMethod(
                            "loading").invoke(service);
                    if("helloworld-osgi.composite".equals(loading))
                    {
                        log.log(Level.WARNING,"Loading HelloWorld-OSGi composite for " 
                                + (sleep/1000) + " seconds !\r");
                        
                        sleepLimit+=10000;
                        
                    } else
                    {
                        fail("Wait for more than 30 seconds");
                        break;
                    }
                }                    
            } catch(InterruptedException e)
            {
                Thread.interrupted();
                fail("HelloWorld-OSGi has not been loaded : " + e.getMessage());
                
            } catch (Exception e)
            {
                fail("HelloWorld-OSGi has not been loaded : " + e.getMessage());
            }
        }
        if(helloLoaded)
        {
            log.log(Level.INFO,"HelloWorld-OSGi composite loaded");
            try
            {
                launch("HelloWorld-OSGi");
            
            } catch (Exception e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
                fail("Unable to launch HelloWorld-OSGi");
            }
        } else
        {
            fail("HelloWorld-OSGi has not been loaded");           
        }        
        System.out.println("... testImplementationOSGi done");
    }
}