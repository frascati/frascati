package org.ow2.frascati.osgi.test;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.junit.Test;
import java.util.logging.Level;
import org.osgi.framework.Bundle;
import org.ow2.frascati.osgi.test.FraSCAtiInOSGiTest;

public abstract class FraSCAtiInOSGiFraSCAtiExplorerTest extends FraSCAtiInOSGiTest
{
    @Test
    public void testBindingFraSCAtiExplorer() throws MalformedURLException, Exception
    {
        System.out.println("... do testFraSCAtiExplorer");

        String framework = getClass().getSimpleName().substring(10);
        framework = framework.substring(0,framework.length()-4).toLowerCase();
        
        Bundle bundle = null;
        try
        {
            String bundleURLStr = new File(
                    "target/test-classes/osgi-introspector-bundle.jar"
                           ).getAbsoluteFile().toURI().toURL().toExternalForm();
            
            bundle = main.getSystemBundleContext().installBundle(bundleURLStr);

        } catch(Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        } 
        assertNotNull("Unable to install osgi-introspector-bundle.jar",
                bundle);
        
        bundle.start();
        System.in.read();
        System.out.println("... testFraSCAtiExplorer done");
    }
}