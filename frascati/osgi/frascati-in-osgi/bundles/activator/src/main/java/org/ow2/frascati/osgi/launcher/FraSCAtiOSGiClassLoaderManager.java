/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.launcher;


import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.util.concurrent.Callable;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.io.IOUtils;
import org.ow2.frascati.util.resource.AbstractResource;

/**
 * The FraSCAtiOSGiClassLoaderManager is used to prepare an execution context to
 * instantiate a FraSCAti instance
 */
public class FraSCAtiOSGiClassLoaderManager
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * The parent of the root of managed FraSCAtiOSGiClassLoaders
     */
    private ClassLoader parentClassLoader;   
    
    /**
     * The root of managed FraSCAtiOSGiClassLoaders
     */
    private FraSCAtiOSGiClassLoader classLoader;
    
    /**
     * The map of registered substitutions
     */
    private Map<String, URL> substitutes;

    /**
     * The list of loaded and shared classes between all children
     */
    private Map<String, List<FraSCAtiOSGiClassLoader>> packagesMap;
    
    /**
     * The list of loaded and shared classes between all children
     */
    private Map<String, Class<?>> loaded;
    
    /**
     * The list of shared URLs between all children
     */
    private List<URL> urls;
    
    /**
     * The list of managed bundles
     */
    private List<BundleRevisionItf<Bundle,BundleContext>> managedBundles;
    
    /**
     * The cache directory
     */
    private File cache;
    
    /**
     * The BundleContext
     */
    private BundleContextRevisionItf<Bundle,BundleContext> bundleContext;
    
    /**
     * The Logger
     */
    private final Logger log = Logger.getLogger(
            FraSCAtiOSGiClassLoaderManager.class.getCanonicalName());

    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
    /**
     * Most of the time, FraSCAti's fragments (bundles) are just containers of FraSCAti's
     * libraries. If they're not and define some classes, these classes are pre-loaded. In
     * this way, possible redefining of classes will be loaded first.
     * 
     * @param resource
     *            A bundle resource
     */
    private void preload(AbstractResource resource)
    {
        List<URL> listOfClassesURLs = new ArrayList<URL>();
        resource.getRegExpResources(".+\\.class", listOfClassesURLs);
        for (URL url : listOfClassesURLs)
        {
            String className = url.getFile().substring(1).replace('/', '.');
            className = className.substring(0, className.length() - 6);
            substitutes.put(className, url);
        }
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
   
    /**
     * Constructor
     * 
     * @param bundleContext
     *            The BundleContext where FraSCAti has to be instantiated
     */
    public FraSCAtiOSGiClassLoaderManager(
            BundleContextRevisionItf<Bundle,BundleContext> bundleContext,
            ClassLoader parentClassLoader)
    {
        managedBundles = Collections.synchronizedList(
                new ArrayList<BundleRevisionItf<Bundle,BundleContext>>());
        loaded = new HashMap<String, Class<?>>();
        substitutes = new HashMap<String, URL>();
        urls = Collections.synchronizedList(new ArrayList<URL>());
        packagesMap = new HashMap<String, List<FraSCAtiOSGiClassLoader>>();
        this.bundleContext = bundleContext;
        this.parentClassLoader = parentClassLoader;
        getCacheDir();
    }

    /**
     * Adds a managed bundle and create a specific FraSCAtiOSGiCLassLoader child to do it
     * 
     * @param bundle
     *            The Bundle to manage
     * @return The created FraSCAtiOSGiClassLoader to manage the Bundle
     */
    public FraSCAtiOSGiClassLoader addBundle(BundleRevisionItf<Bundle,BundleContext> bundle)
    {
        FraSCAtiOSGiClassLoader result = null;
        AbstractResource resource = AbstractResource.newResource(null,bundle);
        
        if (resource == null)
        {
            log.log(Level.WARNING, "Unable to create a resource for the bundle " + bundle);
            return null;
        }
        ClassLoader parent = null; 
        parent=(classLoader == null)?parentClassLoader:classLoader;
        try
        {
            List<String> unmanagedResources = OSGiIOUtils.getUnmanagedResources(bundle);
            if (unmanagedResources == null)
            {
                unmanagedResources = new ArrayList<String>();
            }
            String[] embeddedJuliacLibraries = OSGiIOUtils.getJarsForJuliac(bundle);
            List<AbstractResource> embeddedResourcesForJuliac = null;
            if (embeddedJuliacLibraries != null)
            {
                embeddedResourcesForJuliac = new ArrayList<AbstractResource>();
                for(String embeddedJuliacLibrary : embeddedJuliacLibraries)
                {
                     unmanagedResources.add(embeddedJuliacLibrary);
                }
                resource.setUnmanagedResourcesList(unmanagedResources);
                resource.buildEntries();                
                for(String embeddedJuliacLibrary : embeddedJuliacLibraries)
                {
                    AbstractResource embeddedResource = resource.cacheEmbeddedResource(
                            embeddedJuliacLibrary,
                            getCacheDir().getAbsolutePath());                    
                    embeddedResourcesForJuliac.add(embeddedResource);
                }                
            } else
            {
                resource.setUnmanagedResourcesList(unmanagedResources);
                resource.buildEntries();
            }            
            preload(resource);                                    
            result = new FraSCAtiOSGiClassLoader(this, parent,resource, unmanagedResources);
            
            if(classLoader == null)
            {
                classLoader = result;
            }
            managedBundles.add(bundle);            
            if (embeddedResourcesForJuliac != null)
            {
                for(AbstractResource embeddedResourceForJuliac : embeddedResourcesForJuliac)
                {
                    new FraSCAtiOSGiClassLoader(this,result,
                            embeddedResourceForJuliac, unmanagedResources);
                }
            }
        } catch (Exception e)
        {
            log.log(Level.WARNING,
                    "The bundle cannot be added to the managed bundles list :"
                            + bundle, e);
        }
        return result;
    }

    /**
     * Load class using the packages map and the BundleContext if necessary
     * 
     * @param name
     *            The name of the searched class
     * @return The searched class if it has been found. Null otherwise
     */
    public synchronized Class<?> loadClass(String name) throws ClassNotFoundException
    {
        Class<?> clazz = loaded.get(name);
        if(clazz == null)
        {
            int index = name.lastIndexOf('.');
            String packageName = name.substring(0,index>-1?index:0);
            List<FraSCAtiOSGiClassLoader> loaders = packagesMap.get(packageName);
            if(loaders != null)
            {
               for(FraSCAtiOSGiClassLoader loader : loaders)
               {
                   clazz = loader.findClass(name);
                   if(clazz != null)
                   {
                       break;
                   }
               }
            }
            if(clazz == null)
            {   
                clazz = bundleContext.loadClass(name);
            }
        } 
        if(clazz == null)
        {
            throw new ClassNotFoundException(name);
        }
        return clazz;
    }

    /**
     * Load class using the packages map and the BundleContext if necessary
     * 
     * @param name
     *            The name of the searched class
     * @return The searched class if it has been found. Null otherwise
     */
    public URL getResource(final String name)
    {
        URL resourceURL = null;
        String entry = name.endsWith("/")?name.substring(0,name.length()-1):name;
        entry = entry.startsWith("/")?entry.substring(1):entry;
        int index = entry.lastIndexOf('/');
        
        String packageName = index==-1?
                "_root_":entry.substring(0,index).replace('/','.').replace('\\','.');
        
        List<FraSCAtiOSGiClassLoader> loaders = packagesMap.get(packageName);
        if(loaders != null)
        {
//           int current = 0;
//           int size = loaders.size();

           for(FraSCAtiOSGiClassLoader loader : loaders)
           {
               resourceURL = loader.findResource(name);
               if(resourceURL != null)
               {
                   break;
               }
           }
           
//           ExecutorService executor = Executors.newFixedThreadPool(size); 
//           List<Future<URL>> futures = new ArrayList<Future<URL>>(size) ;
//           while(current < size)
//           {
//               final FraSCAtiOSGiClassLoader loader = loaders.get(current++);
//               futures.add(executor.submit(new Callable<URL>()
//               {
//                    public URL call() throws Exception
//                    {
//                        return loader.findResource(name);
//                    }
//               }));
//           }
//           for(Future<URL> future : futures)
//           {
//               URL result = null;
//               try
//               {
//                    result = future.get();
//                    
//               } catch (Exception e)
//               {
//                   log.log(Level.WARNING,e.getMessage(),e);
//               }
//               if(result != null)
//               {
//                   resourceURL = result;
//                   break;
//               }
//           }
//           executor.shutdown();
        }
        if(resourceURL == null)
        {
           //resourceURL = bundleContext.getResource(name);
            resourceURL = this.parentClassLoader.getResource(name);
        }
        return resourceURL;
    }
    
    /**
     * Load class using the packages map and the BundleContext if necessary
     * 
     * @param name
     *            The name of the searched class
     * @return The searched class if it has been found. Null otherwise
     */
    public Enumeration<URL> getResources(final String name)
    {
        List<URL> resourcesURL = new ArrayList<URL>();     
        
        String entry = name.endsWith("/")?name.substring(0,name.length()-1):name;
        entry = entry.startsWith("/")?entry.substring(1):entry;
        int index = entry.lastIndexOf('/');
        
        String packageName = index==-1?
                "_root_":entry.substring(0,index).replace('/','.').replace('\\','.');

        List<FraSCAtiOSGiClassLoader> loaders = packagesMap.get(packageName);
        
        if(loaders != null)
        {
            for(FraSCAtiOSGiClassLoader loader : loaders)
            {
                Enumeration<URL> results = loader.findResources(name);
                if(results != null)
                {
                    resourcesURL.addAll(Collections.list(results));
                }
            }
//           int current = 0;
//           int size = loaders.size();
//           
//           ExecutorService executor = Executors.newFixedThreadPool(size); 
//           List<Future<Enumeration<URL>>> futures = new ArrayList<Future<Enumeration<URL>>>(size) ;
//           while(current < size)
//           {
//               final FraSCAtiOSGiClassLoader loader = loaders.get(current++);
//               futures.add(executor.submit(new Callable<Enumeration<URL>>()
//               {
//                    public Enumeration<URL> call() throws Exception
//                    {
//                        return loader.findResources(name);
//                    }
//               }));
//           }
//           for(Future<Enumeration<URL>> future : futures)
//           {
//               Enumeration<URL> results = null;
//               try
//               {
//                    results = future.get();
//                    
//               } catch (Exception e)
//               {
//                   log.log(Level.WARNING,e.getMessage(),e);
//               }
//               if(results != null)
//               {
//                  resourcesURL.addAll(Collections.list(results));
//               }
//           }
//           executor.shutdown();
        }  
        return Collections.enumeration(resourcesURL);
    }
    
    /**
     * Returns the root ClassLoader of the current manager instance
     * 
     * @return The root ClassLoader
     */
    public FraSCAtiOSGiClassLoader getClassLoader()
    {
        return classLoader;
    }

    /**
     * Adds a new Class in the map of the loaded ones
     * 
     * @param name
     *            The class name
     * @param clazz
     *            The Class object
     */
    public void registerClass(String name, Class<?> clazz)
    {
        if (!loaded.keySet().contains(name))
        {
            loaded.put(name, clazz);
        }
    }

    /**
     * Adds an URL to the list of the shared ones between FraSCAtiOSGiClassLoader children
     * 
     * @param url
     *            The URL to add
     */
    public void addURL(URL url)
    {
        urls.add(url);
    }

    /**
     * Returns the list of shared URLs between FraSCAtiOSGiClassLoader children as an
     * array
     * 
     * @return The array of shared URLs
     */
    public URL[] getURLs()
    {
        return urls.toArray(new URL[0]);
    }

    /**
     * Returns the Class object which name is passed on as parameter if it has been loaded
     * 
     * @param name
     *            The name of the searched class
     * @return The searched Class object if it has been found Null otherwise
     */
    public Class<?> getLoaded(String name)
    {
        return loaded.get(name);
    }

    /**
     * @param packages
     * @param fraSCAtiOSGiClassLoader
     */
    public synchronized void registerPackages(List<String> packages,
            FraSCAtiOSGiClassLoader fraSCAtiOSGiClassLoader)
    {
        for(String packageName : packages)
        {
            List<FraSCAtiOSGiClassLoader> loaders = packagesMap.get(packageName);
            if(loaders == null)
            {
                    loaders = new ArrayList<FraSCAtiOSGiClassLoader>();
                    loaders.add(fraSCAtiOSGiClassLoader);
                    packagesMap.put(packageName, loaders);
                    
            } else  
            {
                if(!loaders.contains(fraSCAtiOSGiClassLoader))
                {
                    loaders.add(fraSCAtiOSGiClassLoader);
                }
            }
        }
    }
    
    /**
     * Returns the URL for substitution for the name passed on as parameter
     * 
     * @param name
     *            The searched substitution
     * @return The URL for substitution if it has been defined Null otherwise
     */
    public URL getSubstitute(String name)
    {
        return (URL) substitutes.get(name);
    }

    /**
     * Adds a substitution entry
     * 
     * @param resource
     *            The resource name to substitute
     * @param replace
     *            The URL for substitution
     */
    public final void substitute(String resource, URL replace)
    {
        substitutes.put(resource, replace);
    }

    /**
     * Creates a temporary directory to store embedded jarFile
     **/
    public File getCacheDir()
    {
        if (cache != null && cache.exists())
        {
            return cache;
        }
        cache = IOUtils.createTmpDir("FraSCAti_" + hashCode());
        return null;
    }

    /**
     * Returns a bundle which name is passed on as parameter if it exists in the
     * BundleContext
     * 
     * @param symbolicName
     * @param bundleContext
     * @return
     * @throws BundleException
     */
    public BundleRevisionItf<Bundle,BundleContext> getBundle(String symbolicName)
    {
        for (BundleRevisionItf<Bundle,BundleContext> bundle : bundleContext.getBundles())
        {
            if (bundle.getSymbolicName().equals(symbolicName))
            {
                return bundle;
            }
        }
        return null;
    }
}
