/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.osgi.launcher;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FilePermission;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.SocketPermission;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.security.CodeSource;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.ow2.frascati.util.resource.AbstractResource;
import org.ow2.frascati.util.resource.ResourceAlreadyManagedException;
import org.ow2.frascati.util.resource.frascati.Handler;

/**
 * Specific ClassLoader for FraSCAti in OSGi The Class search begins by a
 * research in children ClassLoaders first
 */
public class FraSCAtiOSGiClassLoader extends URLClassLoader
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * The manager
     */
    private FraSCAtiOSGiClassLoaderManager fManager;

    /**
     * The managed resource
     */
    private AbstractResource clResource;

    /**
     * The list of the FraSCAtiOSGiClassLoader children
     */
    private List<FraSCAtiOSGiClassLoader> children;

    
    private static Logger logg = Logger.getLogger(
            FraSCAtiOSGiClassLoader.class.getCanonicalName());

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * Add a FraSCAtiOSGiClassLoader child to the list of children of the
     * current FraSCAtiOSGiClassLoader
     * 
     * @param child
     *            The FraSCAtiOSGiClassLoader child
     */
    protected void addChild(FraSCAtiOSGiClassLoader child)
    {
        children.add(child);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#addURL(java.net.URL)
     */
    @Override
    protected void addURL(URL url)
    {
        fManager.addURL(url);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
     */
    @Override
    protected synchronized Class<?> loadClass(String name, boolean resolve)
            throws ClassNotFoundException
    {
        Class<?> clazz = fManager.getLoaded(name);
        if(clazz == null)
        {   
            clazz = findClass(name);
            
            if(clazz == null)
            {
                clazz = fManager.loadClass(name);
            }
        }
        if(resolve)
        {
            resolveClass(clazz);
        }
        return clazz;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#findClass(java.lang.String)
     */
    @Override
    protected Class<?> findClass(String className)
    {
        Class<?> clazz = findLoadedClass(className);
        if(clazz == null)
        {
            URL classFileNameURL = null;
            
            int index = className.lastIndexOf('.');
            String packageName =  className.substring(0,index>-1?index:0);
            String classFileName = className.replace('.', '/') + ".class";        
            String classFileSimpleName = classFileName.substring(classFileName.lastIndexOf('/') + 1);
            
            if (fManager != null)
            {
                classFileNameURL = fManager.getSubstitute(className);
            }
            if (classFileNameURL == null)
            {
                classFileNameURL = clResource.getResource(packageName,classFileSimpleName,
                        classFileName, true);
            }
            if (classFileNameURL != null)
            {
                try
                {
                    InputStream inputStream = classFileNameURL.openStream();
                    
                    clazz = createClass(className, packageName, classFileNameURL,
                            inputStream);
                    
                } catch (Throwable throwable)
                {
                    if(logg.isLoggable(Level.WARNING))
                    {
                    logg.log(Level.INFO, "Exception thrown while trying to create Class "
                           + className + " with URL " + classFileNameURL, throwable);
                    }
                }
            }
        }
        return clazz;
    }

    /**
     * Return a Class object build from its source code
     * 
     * @param origName
     *            The original class name
     * @param packageName
     *            The package name
     * @param codeSourceURL
     *            The URL of the class source code
     * @param is
     *            The InputStream object of the class source code
     * @return The Class object
     */
    private Class<?> createClass(String origName, String packageName,
            URL codeSourceURL, InputStream is)
    {
        if (is == null)
        {
            return null;
        }
        byte[] clBuf = null;
        try
        {
            byte[] buf = new byte[4096];
            ByteArrayOutputStream bos = new ByteArrayOutputStream(4096);
            int count;
            while ((count = is.read(buf)) > 0)
            {
                bos.write(buf, 0, count);
            }
            clBuf = bos.toByteArray();
        } catch (IOException e)
        {
            return null;
        } finally
        {
            try
            {
                is.close();
                
            } catch (IOException e)
            {
                if(logg.isLoggable(Level.WARNING))
                {
                    logg.log(Level.WARNING,e.getMessage(),e);
                }
            }
        }
        if (packageName != null)
        {
            Package packageObj = getPackage(packageName);
            if (packageObj == null)
            {
                definePackage(packageName, null, null, null, null, null, null, null);
                
            } else
            {
                if (packageObj.isSealed())
                {
                    throw new SecurityException();
                }
            }
        }
        try
        {
            CodeSource cs = new CodeSource(codeSourceURL, (Certificate[]) null);
            Class<?> clazz = defineClass(origName, clBuf, 0, clBuf.length, cs);
            fManager.registerClass(origName, clazz);
            return clazz;
            
        } catch (LinkageError e)
        {
            // A duplicate class definition error can occur if
            // two threads concurrently try to load the same class file -
            // this kind of error has been detected using binding-jms
            Class<?> clazz = findLoadedClass(origName);
            if (clazz != null)
            {
                logg.log(Level.WARNING,"LinkageError :" + origName + " class already resolved ");
            }
            return clazz;
        } catch (Exception e)
        {
            logg.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#getPermissions(java.security.CodeSource)
     */
    @Override
    protected PermissionCollection getPermissions(final CodeSource codesource)
    {
        PermissionCollection pc = super.getPermissions(codesource);
        URL url = codesource.getLocation();
        
        Permission p = null;
        URLConnection urlConnection = null;
        
        try
        {           
            urlConnection = url.openConnection();
            p = urlConnection.getPermission();
            
        } catch (java.io.IOException ioe)
        {
            p = null;
            urlConnection = null;
        }
        if (url.getProtocol().equals("file")
                || url.getProtocol().equals("jar")
                || (url.getProtocol().length() >= 6 && url.getProtocol()
                        .contains("bundle")))
        {
            String path = null;
            if (p != null)
            {
                path = p.getName();
            } else
            {
                path = url.toExternalForm();
            }
            if (path.endsWith(File.separator))
            {
                path += "-";
            }
            p = new FilePermission(path, "read");
            pc.add(p);
        }
        URL locUrl = url;
        if (urlConnection instanceof JarURLConnection)
        {
            locUrl = ((JarURLConnection) urlConnection).getJarFileURL();
        }
        String host = locUrl.getHost();
        if (host != null && (host.length() > 0))
        {
            p = new SocketPermission(host, "connect, accept");
            pc.add(p);
        }
        return pc;
    }

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor Instantiate a FraSCAtiOSGiClassLoader to manage class loading
     * of classes which come from a bundle
     * 
     * @param parentCl
     *            The parent ClassLoader
     * @param fraSCAtiFragment
     *            The Bundle which will be managed
     * @throws ResourceAlreadyManagedException
     * @throws Exception
     *             If an error occurred while instantiating the
     *             FraSCAtiOSGiClassLoader
     */
    public FraSCAtiOSGiClassLoader(final FraSCAtiOSGiClassLoaderManager fManager,
            ClassLoader parent, AbstractResource resource,
            final List<String> unmanagedResources)
            throws ResourceAlreadyManagedException
    {
        super(new URL[0],parent);
        this.fManager = fManager;
        children = Collections.synchronizedList(new ArrayList<FraSCAtiOSGiClassLoader>());
        clResource = resource;

        if (!clResource.isBuilt())
        {
            clResource.setUnmanagedResourcesList(unmanagedResources);
            clResource.buildEntries();
        }
        fManager.registerPackages(clResource.packages(),this);
        
        URL clResourceURL = clResource.getResourceURL();
        if(clResourceURL != null)
        {
            logg.log(Level.CONFIG, "Register " + clResourceURL.toExternalForm());
            addURL(clResourceURL);
        }
        List<String> embeddedJars = clResource.getEmbeddedJars();
        int size = embeddedJars.size();
        int current = 0;

        if (parent instanceof FraSCAtiOSGiClassLoader)
        {           
            ((FraSCAtiOSGiClassLoader) parent).addChild(this);
        }
        if(size > 0)
        {
//            ExecutorService executor = Executors.newFixedThreadPool(size);
//            List<Future<FraSCAtiOSGiClassLoader>> futures = 
//                new ArrayList<Future<FraSCAtiOSGiClassLoader>>();
            
            for(;current < size; current++)
            {
                //final 
                AbstractResource embeddedJar = clResource.getEmbeddedResource(
                      embeddedJars.get(current));
                //final 
                ClassLoader parentClassLoader = this;
                new FraSCAtiOSGiClassLoader(fManager,parentClassLoader
                        ,embeddedJar, unmanagedResources);
                
//                futures.add(executor.submit(new Callable<FraSCAtiOSGiClassLoader>()
//                {
//                    public FraSCAtiOSGiClassLoader call()
//                            throws Exception
//                    {
//                        return new FraSCAtiOSGiClassLoader(fManager,parentClassLoader
//                                ,embeddedJar, unmanagedResources);
//                    }
//                }));
            }
//            for(Future<FraSCAtiOSGiClassLoader> future : futures)
//            {
//                try
//                {
//                    future.get();
//                    
//                } catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                    
//                } catch (ExecutionException e)
//                {
//                    e.printStackTrace();
//                }
//            }
//            executor.shutdown();
        } 
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#loadClass(java.lang.String)
     */
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException
    {
        if (name == null || name.indexOf("/") != -1)
        {
            throw new ClassNotFoundException(name);
        }
        return loadClass(name, true);
    }

    /**
     * Create a new ClassLoader to manage classes/resources coming from the jar
     * file which path is passed on as parameter
     * 
     * @param jarFilePath
     *            The jar file path containing resources to manage
     * @param unmanagedResources
     *            The list of resources to unmanaged
     * @return A new FraSCAtiOSGiClassLoader for the specified jar file
     */
    public FraSCAtiOSGiClassLoader addJar(String jarFilePath,
            List<String> unmanagedResources)
    {
        try
        {
            File jarFile = new File(jarFilePath);
            URL jarURL = jarFile.toURI().toURL();
            AbstractResource resource = AbstractResource.newResource(null,jarURL);
            return new FraSCAtiOSGiClassLoader(fManager, this, resource,
                    unmanagedResources);
            
        } catch (ResourceAlreadyManagedException e)
        {
            logg.log(Level.WARNING, e.getMessage());
        } catch (MalformedURLException e)
        {
            logg.log(Level.WARNING, e.getMessage());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#getURLs()
     */
    @Override
    public URL[] getURLs()
    {
        return fManager.getURLs();
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResources(java.lang.String)
     */
    @Override
    public Enumeration<URL> getResources(String name)
    {
        return fManager.getResources(name);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#findResources(java.lang.String)
     */
    @Override
    public Enumeration<URL> findResources(String name)
    {
        String entry = name.endsWith("/")?name.substring(0,
                name.length()-1):name;         
        entry = entry.startsWith("/")?entry.substring(1):entry;
  
        int index = entry.lastIndexOf('/');       
        String packageName = index==-1?
                "_root_":entry.substring(0,index).replace('/','.').replace('\\','.');
        String resourceName = index==-1?entry:entry.substring(index+1);  
        List<URL> resources = new ArrayList<URL>();
        clResource.getResources(packageName, resourceName, entry, resources);
        return Collections.enumeration(resources);
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResource(java.lang.String)
     */
    @Override
    public URL getResource(String name)
    {
        URL res = fManager.getSubstitute(name);
        if (res == null)
        {
            res = findResource(name);        
            if (res == null)
            {
                res = fManager.getResource(name); 
            }
        }
        return res;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.net.URLClassLoader#findResource(java.lang.String)
     */
    @Override
    public URL findResource(String name)
    {
        String entry = name.endsWith("/")?name.substring(0,
                name.length()-1):name;         
        entry = entry.startsWith("/")?entry.substring(1):entry;
  
        int index = entry.lastIndexOf('/');       
        String packageName = index==-1?
                "_root_":entry.substring(0,index).replace('/','.').replace('\\','.');
        String resourceName = index==-1?entry:entry.substring(index+1);  
         
        URL res = clResource.getResource(packageName, resourceName, entry, false);
        return res;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.ClassLoader#getResourceAsStream(java.lang.String)
     */
    @Override
    public InputStream getResourceAsStream(String name)
    {
        URL url = getResource(name);
        try
        {
            InputStream inputStream = url.openStream();
            return inputStream;
            
        } catch (Exception e)
        {
            logg.log(Level.CONFIG,e.getMessage(),e);
        }
        return null;
    }
}
