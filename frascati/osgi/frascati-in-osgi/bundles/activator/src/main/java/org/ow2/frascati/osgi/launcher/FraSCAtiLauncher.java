/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.osgi.launcher;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleRevisionItf;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.ow2.frascati.osgi.util.io.OSGiIOUtils;
import org.ow2.frascati.util.reflect.ReflectionHelper;

/**
 * A FraSCAti launcher in an OSGi Context
 */
public class FraSCAtiLauncher
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * FraSCAti in OSGi service class name
     */
    private final String FRASCATI_OSGI_API_SERVICE = 
        "org.ow2.frascati.osgi.api.service.FraSCAtiOSGiService";
    
    /**
     * Juliac OSGiRevisionItf interface name
     */
    private final String JULIAC_OSGI_REVISION_ITF = 
        "org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf";
    
    /**
     * Juliac OSGiRevisionItf implementation class name
     */
    private final String JULIAC_OSGI_REVISION_CLASSNAME = "OSGiRevision";    
    
    /**
     * Juliac OSGiRevisionItf implementation package name
     */
    private final String JULIAC_OSGI_REVISION_PACKAGENAME = 
        "org.objectweb.fractal.juliac.osgi.revision";
    
    
    /** FraSCAti instance container */
    private ReflectionHelper frascati;
    
    /** ClassLoader Manager */
    private FraSCAtiOSGiClassLoaderManager fManager;
    
    /** Logger */
    private Logger logg = Logger.getLogger(FraSCAtiLauncher.class.getCanonicalName());
        
    /**
     * Reflection helper object for the current OSGiRevisionItf implementation instance
     */
    private ReflectionHelper revision = null;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Build the list of available fragments according to there needs of others FraSCAti
     * fragments in the execution environment
     * 
     * @param context
     *            The bundle context where the FraSCAti instance has to be instantiated
     * @param nonFragmentNeeds
     *            An array of non FraSCAti fragments bundles needs
     * @return The list of available fragments
     */
    private List<BundleRevisionItf> availableFragments(
            BundleContextRevisionItf<Bundle,BundleContext> context)
    {
        BundleRevisionItf[] bundles = context.getBundles();
        
        List<BundleRevisionItf> availables = new ArrayList<BundleRevisionItf>();
        
        List<String> installedBundle = new ArrayList<String>();
        List<String> installedFragment = new ArrayList<String>();
        
        StringBuilder unresolvedMessageBuilder = new StringBuilder();
        
        // Identify each bundle which is a FraSCati fragment
        for (BundleRevisionItf bundle : bundles)
        {
            String name = bundle.getSymbolicName();
            installedBundle.add(name);
            
            // Is the bundle a FraSCAti fragment ?
            if (bundle.getHeaders().get("Frascati-Fragment") != null)
            {   
                installedFragment.add(name);
                availables.add(bundle);
            }
        }
        while(true)
        {
            boolean broken = false;
            Iterator<BundleRevisionItf> iterator = 
                availables.iterator();
            
            while(iterator.hasNext())
            {
                BundleRevisionItf<Bundle,BundleContext> available = iterator.next();
                String name = available.getSymbolicName();
                
                // Search for needed fragments in the bundle's headers
                String fragmentsNeed = available.getHeaders().get(
                         "Frascati-FragmentNeed");
                
                String[] neededFragments = null;
                if(fragmentsNeed != null)
                {
                    neededFragments = fragmentsNeed.split(",");
                    
                } else
                {
                    neededFragments = new String[0];
                }
                // Search for needed fragments in the bundle's headers
                String bundlesNeed = available.getHeaders().get(
                         "Frascati-BundleNeed");
                
                String[] neededBundles = null;
                if(bundlesNeed != null)
                {
                    neededBundles = bundlesNeed.split(",");
                    
                } else
                {
                    neededBundles = new String[0];
                }
                // Search if all non fragment bundles needed by this one have
                // been deployed in the bundle context
                if (neededBundles != null)
                {
                    for (String neededBundle : neededBundles)
                    {
                        if(!installedBundle.contains(neededBundle))
                        {
                            iterator.remove();
                            installedFragment.remove(name);
                            broken = true;

                            unresolvedMessageBuilder.append(name).append(
                                    " cannot be resolved : '").append(
                                            neededBundle).append("' missing");
                            break;
                        }
                    }
                }
                if(broken)
                {
                    break;
                }
                // Search if all fragments needed by this one have been resolved
                if (neededFragments != null)
                {
                    for (String neededFragment : neededFragments)
                    {
                        if(!installedFragment.contains(neededFragment))
                        {
                            iterator.remove();
                            installedFragment.remove(name);
                            broken = true;

                            unresolvedMessageBuilder.append(name).append(
                                    " cannot be resolved : '").append(
                                            neededFragment).append("' missing");
                            break;
                        }
                    }
                }
                if(broken)
                {
                    break;
                }
            }
            if(!broken)
            {
                break;
            }
        } 
        if(unresolvedMessageBuilder.length() > 0)
        {
           logg.log(Level.WARNING,unresolvedMessageBuilder.toString());
        }
        return availables;
    }
    
    /**
     * @param currentComponent
     * @param componentPath
     * @return
     */
    private ReflectionHelper getComponent(ReflectionHelper currentComponent,
            String componentPath)
    {
        String[] componentPathElements = componentPath.split("/");
        String lookFor = componentPathElements[0];
        String next = null;
        
        if(componentPathElements.length>1)
        {
            int n = 1;
            StringBuilder nextSB = new StringBuilder();
            for(;n<componentPathElements.length;n++)
            {
                nextSB.append(componentPathElements[n]);
                if(n<componentPathElements.length - 1)
                {
                    nextSB.append("/");
                }
            }
            next = nextSB.toString();
        }        
        ReflectionHelper contentController = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.api.control.ContentController");
        
        contentController.set(currentComponent.invoke("getFcInterface",
                new Class<?>[]{String.class},
                new Object[]{"content-controller"}));        

        ReflectionHelper component = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.api.Component");

        ReflectionHelper nameController = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.api.control.NameController");
        
        Object[] subComponents = (Object[]) 
                contentController.invoke("getFcSubComponents");
        
        if(subComponents == null)
        {
            return null;
        }
        for(Object o : subComponents)
        {
            component.set(o);
            nameController.set(component.invoke("getFcInterface",
                    new Class<?>[]{String.class},
                    new Object[]{"name-controller"}));
            String name = (String) nameController.invoke("getFcName");
            if(lookFor.equals(name))
            {
                if(next == null || next.length() ==0)
                {
                    return component;
                } else 
                {
                    return getComponent(component,next);
                }
            }
        }
        return null;
    }    

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Start a new FraSCAti instance
     * 
     * @param context
     * @throws FrascatiException
     * @throws BundleException
     * @throws IOException
     */
    public FraSCAtiLauncher(BundleContext context) throws BundleException, IOException
    {
        long launchDuration = -1;
        long startTime = new Date().getTime();
        
        logg.log(Level.INFO, "Init FraSCAtiLauncher ...");
        ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();

        // Define the BundleContext attribute of the OSGiRevisionItf instance
        configureRevision(context);
        try
        {
            @SuppressWarnings("unchecked")
            BundleContextRevisionItf<Bundle, BundleContext> contextRevision = 
                (BundleContextRevisionItf<Bundle, BundleContext>) revision.invokeInherited(
                        "getBundleContext", JULIAC_OSGI_REVISION_ITF);
            
            fManager = new FraSCAtiOSGiClassLoaderManager(contextRevision,
                   (ClassLoader)revision.invokeInherited("getFrameworkClassLoader", 
                           JULIAC_OSGI_REVISION_ITF));
            
            //build the list of FraSCAti's fragment that can be resolved in the BundleContext
            List<BundleRevisionItf> availables = availableFragments(contextRevision);

            int current = 0;
//            int current = 1;
            int size = availables.size();
            
            for(;current<size;current++)
            {
                fManager.addBundle(availables.get(current));
            }
            //The first added bundle operation is used to create the FraSCAtiOSGiClassLoaderManager's
            //root ClassLoader
//            BundleRevisionItf<Bundle,BundleContext> firstFragment = availables.get(0);
//            fManager.addBundle(firstFragment);
//            
//            //Gather all fragments together
//            ExecutorService executor = Executors.newFixedThreadPool(size);
//            List<Future<FraSCAtiOSGiClassLoader>> futures = 
//                new ArrayList<Future<FraSCAtiOSGiClassLoader>>();
//            
//            for(;current<size;current++)
//            {
//                final BundleRevisionItf<Bundle,BundleContext> fragment = availables.get(current);
//                futures.add(executor.submit(new Callable<FraSCAtiOSGiClassLoader>()
//                {
//                    public FraSCAtiOSGiClassLoader call()
//                    {
//                        // Feed the FraSCAtiOSGiClassLoaderManager ClassLoader
//                        return fManager.addBundle(fragment);
//                    }                        
//                }));  
//            }  
//            for(Future<FraSCAtiOSGiClassLoader> future : futures)
//            {
//                try
//                {
//                    future.get();
//                    
//                } catch (InterruptedException e)
//                {
//                    logg.log(Level.WARNING, e.getMessage(),e);
//                    
//                } catch (ExecutionException e)
//                {
//                    logg.log(Level.WARNING, e.getMessage(),e);
//                }
//            }
//            executor.shutdown();
            launchDuration = new Date().getTime() - startTime;
            logg.log(Level.INFO, "FraSCAti's Fragments gathered together at " + launchDuration + " ms");
            
            try
            {
                // Use the FraSCAtiOSGiService Class<?> object loaded in the BundleContext
                Class<?> serviceClass = fManager.loadClass(FRASCATI_OSGI_API_SERVICE);
                fManager.registerClass(FRASCATI_OSGI_API_SERVICE, serviceClass);
            
            } catch(ClassNotFoundException e)
            {
                logg.log(Level.WARNING,"'" + FRASCATI_OSGI_API_SERVICE +
                        "' Class has not been found. " +
                        "\nThis could have impredictable results" );
            }
            // Define the appropriate bootstrap according to the available FraSCAti'sfragments
            defineBootStrap(availables);
            
            // Define the BundleContext attribute of the OSGi PlatformItf implementation instance
            configurePlatform();

            launchDuration = new Date().getTime() - startTime;
            logg.log(Level.INFO, "Platform configuration done at " + launchDuration + " ms");
            
            // Define the default output directory
            System.setProperty("org.ow2.frascati.output.directory", 
                    fManager.getCacheDir().getAbsolutePath());
            
            frascati = new ReflectionHelper(fManager.getClassLoader(),
                    "org.ow2.frascati.FraSCAti");
            
            if (!frascati.isClassKnown())
            {
                logg.log(Level.SEVERE, "FraSCAti OSGi core has not been loaded");
                return;
            }
            //Search for the FrascatiExplorerClassResolver class in the classpath
            //Its existence means that the FraSCAti's Explorer is available
            URL resolverURL = fManager.getClassLoader().getResource(
                    "org/ow2/frascati/explorer/FrascatiExplorerClassResolver.class"); 
            
            ReflectionHelper resolver = null;                        
            if(resolverURL != null)
            {
                //Needed for FraSCAti's Explorer initialization
                logg.log(Level.CONFIG, "Define the current thread ClassLoader as the "
                                + "frascati's FraSCAtiOSGiClassLoader");   
                
                Thread.currentThread().setContextClassLoader(fManager.getClassLoader());
                
                resolver = new ReflectionHelper(fManager.getClassLoader(),
                    "org.ow2.frascati.explorer.FrascatiExplorerClassResolver");
            
                resolver.invoke("setClassLoader",
                    new Class<?>[]{ClassLoader.class},
                    new Object[]{fManager.getClassLoader()});
            }            
            // Create the FraSCAti instance
            frascati.set(frascati.invokeStatic("newFraSCAti",
                    new Class<?>[]
                    { ClassLoader.class }, new Object[]
                    { fManager.getClassLoader() }));

            //because of the explorer initialization in a separate Thread, we must wait its
            //end to avoid classloading exceptions
            if(resolverURL != null)
            {
                ReflectionHelper explorer = new ReflectionHelper(fManager.getClassLoader(),
                "org.ow2.frascati.explorer.ExplorerGUI");
                
                ReflectionHelper frascatiExplorer = new ReflectionHelper(fManager
                        .getClassLoader(),
                        "org.ow2.frascati.explorer.api.FraSCAtiExplorer");
                
                ReflectionHelper fraSCAtiExplorerSingleton = new ReflectionHelper(
                        fManager.getClassLoader(),
                        "org.ow2.frascati.explorer.api.FraSCAtiExplorerSingleton");
                try
                {
                    Object singleton = frascatiExplorer.getStatic("SINGLETON");
                    if (singleton != null)
                    {
                        fraSCAtiExplorerSingleton.set(singleton);
                        explorer.set(fraSCAtiExplorerSingleton.invoke("get"));
                    }
                } catch (Exception e)
                {
                    logg.log(Level.WARNING,e.getMessage(),e);
                }
                if (explorer.get() != null)
                {
                  @SuppressWarnings("unchecked")
                  final Map<Class<?>, Object> frascatiExplorerServices = 
                  (Map<Class<?>, Object>) explorer.get("frascatiExplorerServices");
                  
                  ClassLoader registeredClassLoader = null;
                  while(registeredClassLoader == null)
                  {
                      Thread.sleep(10);
                      registeredClassLoader = (ClassLoader) frascatiExplorerServices.get(
                              ClassLoader.class);
                  }                     
                } 
            }
        } catch (Exception e)
        {
            logg.log(Level.SEVERE,
                    "Instantiation of the FrascatiService has caused an exception",e);
        } finally
        {   
            if(Thread.currentThread().getContextClassLoader() != currentThreadClassLoader)
            {
                Thread.currentThread().setContextClassLoader(currentThreadClassLoader);
            }
            logg.log(Level.CONFIG, "Restore initial ClassLoader");
            launchDuration = new Date().getTime() - startTime;
            logg.log(Level.INFO, "FraSCAtiOSGiService launched in " + launchDuration + " ms");
        }
    }

    /**
     * Search for a bootstrap class name definition in the managed fragments headers. If
     * it has not been found, use the default one.
     */
    public void defineBootStrap(List<BundleRevisionItf> availables)
    {
        String bootstrap = null;
        for (BundleRevisionItf<Bundle,BundleContext> bundle : availables)
        {
            // Search for a bootstrap class name in the bundle's headers
            String[] bootstrapHeader = OSGiIOUtils.getHeader(bundle,
                    OSGiIOUtils.BOOTSTRAP_HEADER);
            // Search for overwritten bootstrap in the bundle's headers
            String[] overwrittenBootstrapHeader = OSGiIOUtils.getHeader(bundle,
                    OSGiIOUtils.OVERWRITTEN_BOOSTRAP_HEADER);
            // If a bootstrap class name is specified in the bundle's headers
            // and if it overwrites the define bootstrap class
            // set it
            if (bootstrapHeader != null)
            {
                if (bootstrap != null && overwrittenBootstrapHeader != null)
                {
                    for (String overwritten : overwrittenBootstrapHeader)
                    {
                        if (overwritten.equals(bootstrap))
                        {
                            bootstrap = bootstrapHeader[0];
                            break;
                        }
                    }
                } else if (bootstrap == null)
                {
                    bootstrap = bootstrapHeader[0];
                }
            }
        }
        // If the bootstrap class name is null, set the default one
        if (bootstrap == null)
        {
            bootstrap = OSGiIOUtils.DEFAULT_BOOTSTRAP;
        }
        System.setProperty(OSGiIOUtils.BOOTSTRAP_PROPERTY, bootstrap);
        logg.log(Level.INFO, "Define bootstrap class : '" + bootstrap + "'");
    }
    
    /**
     * Set the BundleContext class attribute of the Juliac PlatformImpl
     * 
     * @param context
     *            The bundle context where the FraSCAti instance will be instantiated
     */
    public void configureRevision(BundleContext context)
    {
        String osgiRevisionImplementationClassName = System.getProperty(
                "juliac.osgi.revision",
                 JULIAC_OSGI_REVISION_CLASSNAME);
        
        osgiRevisionImplementationClassName = JULIAC_OSGI_REVISION_PACKAGENAME + "." + 
            osgiRevisionImplementationClassName;
        
        try{            
            Class<?> osgiRevisionImplementationClass = FraSCAtiLauncher.class.getClassLoader(
                    ).loadClass(osgiRevisionImplementationClassName); 
            
            revision = new ReflectionHelper(osgiRevisionImplementationClass);   
            
            revision.newInstance(
                new Class<?>[] { BundleContext.class }, 
                new Object[]{ context });
            
        } catch(Exception e)
        {
            logg.log(Level.WARNING, e.getMessage(),e);
        }
    }

    /**
     * Set the BundleContext class attribute of the Juliac PlatformImpl
     * 
     * @param context
     *            The bundle context where the FraSCAti instance will be instantiated
     */
    public void configurePlatform()
    {
        ReflectionHelper platformImpl = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.juliac.osgi.PlatformImpl");
        
        ReflectionHelper helper = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.juliac.osgi.OSGiHelper");
        
        if (!platformImpl.isClassKnown() || !helper.isClassKnown())
        {
            logg.log(Level.WARNING,
                    "Unable to configure the 'org.objectweb.fractal.juliac.osgi.PlatformImpl' " +
                    "instance");
            return;
        }
        platformImpl.set(helper.invokeStatic("getPlatform"));        
        try
        {
            Class<?> juliacOsgiRevisionItfClass = Class.forName(JULIAC_OSGI_REVISION_ITF);
            
            platformImpl.invokeStatic("setOSGiRevision", 
                    new Class<?>[]{ juliacOsgiRevisionItfClass }, 
                    new Object[]{ revision.get() });
            
        } catch (ClassNotFoundException e)
        {
            logg.log(Level.WARNING, e.getMessage(),e);
        }
    }

    /**
     * @throws FrascatiException
     */
    public void stop() 
    {
        long startTime = new Date().getTime();
        logg.log(Level.INFO, "Stop FraSCAtiOSGiService ...");
        ReflectionHelper compositeManager  = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.ow2.frascati.assembly.factory.api.CompositeManager");
        
        compositeManager.set(frascati.invoke("getCompositeManager"));
        
        ReflectionHelper component = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.api.Component");
        
        component.set(compositeManager.invoke("getTopLevelDomainComposite"));
        
        component =  getComponent(component,
                "org.ow2.frascati.FraSCAti/assembly-factory");
        
        ReflectionHelper lifeCycleController = new ReflectionHelper(
                fManager.getClassLoader(),
                "org.objectweb.fractal.api.control.LifeCycleController");
        
        lifeCycleController.set(component.invoke("getFcInterface",
                new Class<?>[]{String.class},
                new Object[]{"lifecycle-controller"}));
        
        lifeCycleController.invoke("stopFc");
        fManager = null;
        frascati = null;
        long duration = new Date().getTime() - startTime;
        logg.log(Level.INFO, "FraSCAtiOSGiService stopped in " + duration + " ms");
    }
}
