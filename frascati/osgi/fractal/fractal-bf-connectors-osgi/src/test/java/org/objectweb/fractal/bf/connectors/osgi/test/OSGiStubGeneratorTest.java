/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.Service;
import org.objectweb.fractal.bf.connectors.osgi.OSGiStubContent;
import org.objectweb.fractal.bf.proxies.ASMStubGenerator;

/**
 * Test the {@link ASMStubGenerator} to generate stubs for the OSGi bindings.
 */
public class OSGiStubGeneratorTest {

	@Test
	public void testGenerateNewInstance() {

		BindingFactoryClassLoader cl = new BindingFactoryClassLoader(
		        Thread.currentThread().getContextClassLoader());
		
		try {
			Object impl = ASMStubGenerator.generateNewInstance(Service.class,
					OSGiStubContent.class, cl);
			
			assertNotNull("Impl can' be null", impl);
			assertTrue("Cannot assign impl to " + Service.class.getCanonicalName(), 
			        Service.class.isAssignableFrom(impl.getClass()));
			
		} catch (Exception e) {
		    
			e.printStackTrace();
			fail();
		}
	}

}
