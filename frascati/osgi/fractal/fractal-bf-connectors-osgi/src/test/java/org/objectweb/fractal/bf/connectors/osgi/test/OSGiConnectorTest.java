/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.Factory;
import org.objectweb.fractal.adl.FactoryFactory;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactoryClassLoader;
import org.objectweb.fractal.bf.connectors.osgi.OSGiConnector;
import org.objectweb.fractal.bf.connectors.osgi.OSGiExportHints;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 * Test the OSGi binding skeleton generation
 */
public class OSGiConnectorTest {
    
	Component service;
	Component client;

	protected OSGiConnector connector;
	private static Factory adlFactory;

	@BeforeClass
	public static void initializeSystemProperties() throws ADLException {
		System.setProperty("fractal.provider",
				"org.objectweb.fractal.julia.Julia");
		adlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);
	}

	@Before
	public void setUp() throws Exception {

		service = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Service", null);
		assertNotNull(service);
		client = (Component) adlFactory.newComponent(
				"org.objectweb.fractal.bf.connectors.Client", null);
		assertNotNull(client);

		BindingFactoryClassLoader cl = new BindingFactoryClassLoader(Thread
				.currentThread().getContextClassLoader());

		connector = new OSGiConnector();
		connector.setBindingFactoryClassLoader(cl);
		assertNotNull(connector);
	}

	@Test
	public void testCreateSkeletonAndCheckState() throws Exception {

		Component skel = connector.createSkel("service", service
				.getFcInterface("service"), service, createExportHints());
		assertNotNull("Skeleton component should not be null", skel);
		assertEquals(LifeCycleController.STOPPED, Fractal
				.getLifeCycleController(skel).getFcState());

	}

	/**
	 * Utility method
	 * 
	 * @return
	 */
	private OSGiExportHints createExportHints() {
		OSGiExportHints hints = new OSGiExportHints();
		
		hints.setProperties("name=mockService,version=1.1");
		hints.setBundleContext(Mockito.mock(BundleContextRevisionItf.class));
		return hints;
	}

}
