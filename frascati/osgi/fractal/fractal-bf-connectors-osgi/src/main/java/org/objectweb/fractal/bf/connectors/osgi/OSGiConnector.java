/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.common.AbstractConnector;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

import java.util.logging.Level;

/**
 * A OSGi-Connector can export and bind Fractal interfaces via OSGi services.
 */
public class OSGiConnector extends
        AbstractConnector<OSGiExportHints, OSGiBindHints>
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    /**
     * The associated BundleContext
     */
    private BundleContextRevisionItf<?,?> bundleContext;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      getSkeletonAdl()
     */
    protected String getSkeletonAdl()
    {
        return "org.objectweb.fractal.bf.connectors.osgi.OsgiSkeleton";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      initSkeletonAdlContext(org.objectweb.fractal.bf.ExportHints,
     *      java.util.Map)
     */
    protected void initSkeletonAdlContext(OSGiExportHints exportHints,
            Map<String, Object> context)
    {
        String properties = exportHints.getProperties();
        if (properties == null)
        {
            properties = OSGiConnectorConstants.DEFAULT_PROPERTIES_VALUE;
            log.warning("Export hints didn't specify "
                    + OSGiConnectorConstants.PROPERTIES + ", going to use "
                    + OSGiConnectorConstants.DEFAULT_PROPERTIES_VALUE);
        }
        context.put(OSGiConnectorConstants.PROPERTIES, properties);
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      getStubAdl()
     */
    protected String getStubAdl()
    {
        return "org.objectweb.fractal.bf.connectors.osgi.OsgiStub";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      initStubAdlContext(org.objectweb.fractal.bf.BindHints,
     *      java.util.Map)
     */
    protected void initStubAdlContext(OSGiBindHints bindHints,
            Map<String, Object> context)
    {
        generateStubContentClass(OSGiStubContent.class, context);
        String filter = bindHints.getFilter();
        if (filter == null)
        {
            filter = OSGiConnectorConstants.DEFAULT_FILTER_VALUE;
            log.warning("Bind hints didn't specify a "
                    + OSGiConnectorConstants.FILTER + ", going to use "
                    + OSGiConnectorConstants.DEFAULT_FILTER_VALUE);
        }
        context.put(OSGiConnectorConstants.FILTER, filter);
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
     */
    public String getPluginIdentifier()
    {
        return "osgi";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(java.util.Map)
     */
    public OSGiExportHints getExportHints(Map<String, Object> initialHints)
    {
        bundleContext = (BundleContextRevisionItf<?,?>) initialHints
                .get(OSGiConnectorConstants.BUNDLE_CONTEXT);
        OSGiExportHints osgiExportHints = new OSGiExportHints();
        osgiExportHints.setProperties((String) initialHints
                .get(OSGiConnectorConstants.PROPERTIES));
        // log.info("OSGiExportHints: " + osgiExportHints);
        return osgiExportHints;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(java.util.Map)
     */
    public OSGiBindHints getBindHints(Map<String, Object> initialHints)
    {
        bundleContext = (BundleContextRevisionItf<?,?>) initialHints
                .get(OSGiConnectorConstants.BUNDLE_CONTEXT);
        OSGiBindHints osgiBindHints = new OSGiBindHints();
        osgiBindHints.setFilter((String) initialHints
                .get(OSGiConnectorConstants.FILTER));
        // log.info("OSGiBindHints: " + osgiBindHints);
        return osgiBindHints;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector
     *      #finalizeSkeleton(org.objectweb.fractal.api.Component,
     *      java.util.Map)
     */
    @Override
    public void finalizeSkeleton(Component skel,
            Map<String, String> finalizationHints)
    {
        try
        {
            OSGiSkeletonContentAttributes ska = (OSGiSkeletonContentAttributes) Fractal
                    .getAttributeController(skel);
            ska.setBundleContext(bundleContext);
        } catch (NoSuchInterfaceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        try
        {
            super.finalizeSkeleton(skel, finalizationHints);
        } catch (BindingFactoryException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector
     *      #finalizeStub(org.objectweb.fractal.api.Component, java.util.Map)
     */
    @Override
    public void finalizeStub(Component stub, Map<String, Object> hints)
    {
        try
        {
            OSGiStubContentAttributes ska = (OSGiStubContentAttributes) Fractal
                    .getAttributeController(stub);
            ska.setBundleContext(bundleContext);
        } catch (NoSuchInterfaceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        try
        {
            super.finalizeStub(stub, hints);
        } catch (BindingFactoryException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }
}
