/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi;

import org.objectweb.fractal.bf.connectors.common.StubContentAttributes;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 * Attribute controller for OSGi stubs.
 */
public interface OSGiStubContentAttributes extends StubContentAttributes
{
    /**
     * Return the filter attribute value Filter value is used to identify binded interface
     * 
     * @return the filter attribute value
     */
    String getFilter();

    /**
     * Set the filter attribute value Filter value is used to identify binded interface
     * 
     * @param filter
     *            the filter attribute to set
     */
    void setFilter(String filter);

    /**
     * Set the BundleContext for binding
     * 
     * @param bundleContext
     */
    void setBundleContext(BundleContextRevisionItf<?,?> bundleContext);

    /**
     * Return the BundleContext for binding
     * 
     * @return the BundleContext
     */
    BundleContextRevisionItf<?,?> getBundleContext();
}
