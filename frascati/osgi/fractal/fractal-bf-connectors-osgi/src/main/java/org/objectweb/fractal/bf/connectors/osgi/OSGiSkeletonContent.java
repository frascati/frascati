/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi;

import java.util.Hashtable;
import java.util.logging.Level; 
import org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 * The content implementation of OSGi components.
 */
public class OSGiSkeletonContent extends AbstractSkeletonContent implements
        OSGiSkeletonContentAttributes
{
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    private BundleContextRevisionItf<?,?> bundleContext;
    private int registration;
    private Hashtable<String, String> properties;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiSkeletonContentAttributes#
     *      getProperties()
     */
    public String getProperties()
    {
        StringBuilder propertiesBuilder = new StringBuilder();
        
        if (properties != null)
        {
            while (properties.keys().hasMoreElements())
            {
                String key = (String) properties.keys().nextElement();
                String value = (String) properties.get(key);
                if (value != null)
                {
                    propertiesBuilder.append(propertiesBuilder.length() > 0 ? "," : "");
                    propertiesBuilder.append(key);
                    propertiesBuilder.append("=");
                    propertiesBuilder.append(value);
                }
            }
        }
        return propertiesBuilder.toString();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiSkeletonContentAttributes#
     *      setProperties(java.lang.String)
     */
    public void setProperties(String propertiesStr)
    {
        if (this.properties == null)
        {
            this.properties = new Hashtable<String, String>();
        }
        String[] propertiesEls = propertiesStr.split(",");
        for (String propertiesEl : propertiesEls)
        {
            String[] props = propertiesEl.split("=");
            if (props.length == 2)
            {
                properties.put(props[0].trim(), props[1].trim());
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent#
     *      startFc()
     */
    @Override
    public void startFc()
    {
        log.fine("Skeleton will be started...");
        registration = bundleContext.registerService(
                getServiceClass().getCanonicalName(), getServant(), properties);
        log.log(Level.INFO,"'" + getServiceClass().getCanonicalName() + "' service[" + getServant()
                + "] registration["+registration+"]");
        super.startFc();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent#
     *      stopFc()
     */
    @Override
    public void stopFc()
    {
        log.fine("Skeleton will be stopped...");
        if (registration != -1)
        {
            bundleContext.unregisterService(registration);
        }
        super.stopFc();
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiSkeletonContentAttributes
     *      #setBundleContext(org.osgi.framework.BundleContext)
     */
    public void setBundleContext(BundleContextRevisionItf<?,?> bundleContext)
    {
        this.bundleContext = bundleContext;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiSkeletonContentAttributes#
     *      getBundleContext()
     */
    public BundleContextRevisionItf<?,?> getBundleContext()
    {
        return bundleContext;
    }
}
