/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.connectors.common.AbstractStubContent;

import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.event.ServiceListenerRevisionItf;

import java.util.logging.Level;

/**
 * A super class for OSGi stubs. Concrete classes are generated on the fly.
 */
public class OSGiStubContent extends AbstractStubContent implements
        OSGiStubContentAttributes, ServiceListenerRevisionItf
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private BundleContextRevisionItf<?,?> bundleContext;
    private Object serviceObject;
    private String filter;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractStubContent#
     *      resolveReference()
     */
    protected Object resolveReference() throws Exception
    {
        serviceObject = bundleContext.getService(
                getServiceClass().getCanonicalName(), filter);
        
        return serviceObject;
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiStubContentAttributes#
     *      getFilter()
     */
    public String getFilter()
    {
        return filter;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiStubContentAttributes#
     *      setFilter(java.lang.String)
     */
    public void setFilter(String filter)
    {
        if (filter != null)
        {
            // The sca syntax is not the same has the osgi one : '!', '|', and
            // '&' are replaced by 'not', 'or', and 'and' respectively.
            this.filter = filter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])and([^a-zA-Z\\-\\.\\_])", "$1&$2");
            this.filter = this.filter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])or([^a-zA-Z\\-\\.\\_])", "$1|$2");
            this.filter = this.filter.replaceAll(
                    "(^|[^a-zA-Z\\-\\.\\_])not([^a-zA-Z\\-\\.\\_])", "$1!$2");
        }
    }

    /**
     * {@inheritDoc}
     * @return 
     * 
     * @see org.osgi.framework.ServiceListener#serviceChanged(org.osgi.framework.ServiceEvent)
     */
    public boolean serviceChanged(Object serviceObjectEvent, int eventType)
    {
        if (serviceObjectEvent == serviceObject)
        {
            if ("UNREGISTERING".equals(bundleContext.mapServiceEvent(eventType)))
            {
                serviceObject = null;
            }
        }
        if (serviceObject == null)
        {
            try
            {
                String fcState = getFcState();
                if (LifeCycleController.STARTED.equals(fcState))
                {
                    stopFc();
                    startFc();
                    
                } else if (LifeCycleController.STOPPED.equals(fcState))
                {
                    startFc();
                }
            } catch (IllegalLifeCycleException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiStubContentAttributes#
     *      setBundleContext(org.osgi.framework.BundleContext)
     */
    public void setBundleContext(BundleContextRevisionItf<?,?> bundleContext)
    {
        this.bundleContext = bundleContext;

        if (this.bundleContext != null)
        {
            if (filter != null)
            {
                filter = new StringBuilder("(&(objectclass=")
                            .append(getServiceClass().getCanonicalName())
                            .append(")").append(filter).append(")")
                            .toString();
                    
                if(!this.bundleContext.validateFilter(filter))
                {
                    log.info("filter " + filter + " is not valid");
                    filter = null;
                }
            }
            if (filter == null)
            {
                filter = new StringBuilder("(objectclass=")
                        .append(getServiceClass().getCanonicalName())
                        .append(")").toString();
            }
            log.info("filter :" + filter);
            this.bundleContext.registerServiceListener(this, filter);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.osgi.OSGiStubContentAttributes#
     *      getBundleContext()
     */
    public BundleContextRevisionItf<?,?> getBundleContext()
    {
        return bundleContext;
    }
}
