/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.osgi;

import org.objectweb.fractal.bf.BindHints;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;

/**
 * The OSGi bind hints.
 */
public class OSGiBindHints implements BindHints
{
    // --------------------------------------------------------------------------
    // Internal state.
    // -------------------------------------------------------------------------
    /**
     * The filter to use to retrieve binded interface from the BundleContext
     */
    private String filter;
    
    /**
     * The associated BundleContext
     */
    private BundleContextRevisionItf<?,?> bundleContext;

    // --------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------
    /**
     * Return the filter attribute value Filter value is used to identify binded interface
     * 
     * @return the filter attribute value
     */
    public final String getFilter()
    {
        return filter;
    }

    /**
     * Set the filter attribute value Filter value is used to identify binded interface
     * 
     * @param filter
     *            the filter attribute to set
     */
    public final void setFilter(String filter)
    {
        this.filter = filter;
    }

    /**
     * Set the BundleContext for binding
     * 
     * @param bundleContext
     */
    public void setBundleContext(BundleContextRevisionItf<?,?> bundleContext)
    {
        this.bundleContext = bundleContext;
    }

    /**
     * Return the BundleContext for binding
     * 
     * @return the BundleContext
     */
    public BundleContextRevisionItf<?,?> getBundleContext()
    {
        return bundleContext;
    }

    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("OSGiBindHints filter : ");
        builder.append(filter != null ? filter : "");
        builder.append(" }");
        return builder.toString();
    }
}
