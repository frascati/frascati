/**
 * OW2 FraSCAti OSGi
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.juliac.osgi;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.juliac.osgi.revision.OSGiRevisionException;
import org.objectweb.fractal.juliac.osgi.revision.api.BundleContextRevisionItf;
import org.objectweb.fractal.juliac.osgi.revision.api.OSGiRevisionItf;
import org.objectweb.fractal.juliac.osgi.PlatformItf;

/**
 * Conform to the fractal osgi implementation
 */
public class PlatformImpl implements PlatformItf
{
    private static final Logger LOGGER = Logger.getLogger(PlatformImpl.class.getCanonicalName());

    private static OSGiRevisionItf<?,?> osgiRevision;

    /**
     * Define the context attribute value
     * 
     * @param context
     *            the BundleContext to set
     */
    public static void setOSGiRevision(OSGiRevisionItf<?,?> osgiRevision)
    {
        PlatformImpl.osgiRevision = osgiRevision;
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.juliac.osgi.PlatformItf#getRevision()
     */
    public BundleContextRevisionItf<?,?> getBundleContext() throws OSGiRevisionException
    {
        return PlatformImpl.osgiRevision.getBundleContext();
    }

    /* (non-Javadoc)
     * @see org.objectweb.fractal.juliac.osgi.PlatformItf#stop()
     */
    public void stop() throws OSGiRevisionException
    {
        LOGGER.log(Level.CONFIG,"FraSCAti cannot stop the OSGi Framework");        
    }
}
