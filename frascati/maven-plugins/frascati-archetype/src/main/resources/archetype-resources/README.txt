The structure of this application has been created automatically by FraSCAti Archetype and contains the basic needs to construct an SCA application executing on FraSCAti.

Install your application doing "mvn clean install" in the command line.

To execute the application, do "mvn -Prun" in the command line or open the pom.xml file to see the available profiles.

A helloworld application has been created to help beginners to understand how a simple application works on FraSCAti.
A composite file describes the architecture and must to be changed to be adapted to your application.
The java files can be changed or simply deleted.

More about FraSCAti including an user guide in: http://frascati.ow2.org
