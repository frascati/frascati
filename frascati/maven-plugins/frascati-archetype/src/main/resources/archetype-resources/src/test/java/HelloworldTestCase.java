/**
 * This class has been generated automaticaly by FraSCAty Archetype
 * 
 *
 * Author: 
 *
 * Contributor(s): 
 *
 */
package ${groupId};

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

public class HelloworldTestCase
{
  private FraSCAti frascati;

  /**
   * The SCA composite to test
   */
  private Component scaComposite;

  /**
   * Load a composite
   * 
   * @param name
   *          the composite name
   * @throws FrascatiException 
   */
  @Before
  public final void loadComposite() throws FrascatiException
  {
    String compositeName = getComposite();
    System.out.println("Loading SCA composite '" + compositeName + "'...");
    frascati = FraSCAti.newFraSCAti();
    scaComposite = frascati.getComposite(compositeName);
  }

  /**
   * Get a service on the SCA composite
   * 
   * @param <T>
   *          the class type for the return
   * @param serviceName
   *          the name of the service
   * @return the service named <code>serviceName</code> or null
   */
  protected final <T> T getService(Class<T> cl, String serviceName)
  {
    System.out.println("Getting SCA service '" + serviceName + "'...");
    try {
      return frascati.getService(scaComposite, serviceName, cl);
    } catch (Exception e) {
      System.err.println("No such SCA service: " + serviceName);
      return null;
    }
  }

  /**
   * Close the SCA domain
   * 
   * @throws IllegalLifeCycleException
   *           if the domain cannot be closed
   * @throws NoSuchInterfaceException
   *           if the lifecycle controller of the component is not found
   */
  @After
  public final void close() throws FrascatiException
  {
    if(scaComposite != null) {
      frascati.close(scaComposite);
    }
  }

  public final String getComposite()
  {
    return "helloworld-wired";
  }
  
  @Test
  public final void testService()
  {
    getService(Runnable.class, "r").run();
  }
}



