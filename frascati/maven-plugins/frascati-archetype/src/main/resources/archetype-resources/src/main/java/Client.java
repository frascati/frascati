/**
 * This class has been generated automaticaly by FraSCAty Archetype
 * 
 *
 * Author: 
 *
 * Contributor(s): 
 *
 */
package ${groupId};

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

/** A print service client. */
@Service(Runnable.class)
public class Client
  implements Runnable
{
    
    @Reference(required = true)
    private PrintService s;
    
    /** Default constructor. */
    public Client() {
        System.out.println("CLIENT created.");
    }
    
    /** Run the client. */
    public final void run() {
        s.print("Hello World!");
    }
}
