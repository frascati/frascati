/**
 * This class has been generated automaticaly by FraSCAty Archetype
 * 
 *
 * Author: 
 *
 * Contributor(s): 
 *
 */
package ${groupId};

import org.osoa.sca.annotations.Service;

/**
 * A basic service used to print messages. 
 */
@Service
public interface PrintService {
    void print(String msg);
}
