/**
 * OW2 FraSCAti Compiler Maven Plugin
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Damien Fournier
 *            Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *                 Christophe Munilla
 *
 */

package org.ow2.frascati.mojo;

import java.net.URL;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;

/**
 * A Maven plugin for compiling SCA composites.
 * 
 * @execute phase="generate-sources"
 * @requiresDependencyResolution compile
 * @goal compile
 */
public class FrascatiCompilerMojo extends AbstractFrascatiCompilerMojo
{    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#getCompiledClassesDirectory()
     */
    @Override
    String getCompiledClassesDirectory()
    {
      return "target/classes";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#completeBuildSources()
     */
    @Override
    void completeBuildSources()
    { 
        getLog().info("sources already built");
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#completeBuildResources
     * (org.ow2.frascati.assembly.factory.api.ClassLoaderManager)
     */
    @Override
    void completeBuildResources(ClassLoaderManager classLoaderManager)
    {
        getLog().info("ressources already built");
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#addSource(java.lang.String)
     */
    @Override
    void addSource(String source)
    {
        project.addCompileSourceRoot(source);
    }

}
