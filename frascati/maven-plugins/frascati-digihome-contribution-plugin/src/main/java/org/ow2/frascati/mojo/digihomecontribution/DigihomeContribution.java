/**
 * OW2 FraSCAti Digihome Contribution Maven Plugin
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo.digihomecontribution;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;

import org.apache.commons.io.FileUtils;
import org.jdom.JDOMException;
import org.ow2.frascati.mojo.DigihomeParameter;
import org.ow2.frascati.mojo.Export;
import org.ow2.frascati.mojo.Import;
import org.ow2.frascati.mojo.contribution.Contribution;
import org.ow2.frascati.mojo.util.ZipUtil;

/**
 *
 */
public class DigihomeContribution
{
    private static final Logger logger = Logger.getLogger(DigihomeContribution.class.getName());

    private static final String SHAREDLIBS_DIRECTORY_NAME="shared";

    private static final String DIGIHOME_PARAMETERS_FILE_NAME="digihomeParameters";
    
    private Contribution contribution;
    
    private File contributionFile;
    
    private List<File> sharedLibs;
    
    private List<DigihomeParameter> digihomeParameters;
    
    public DigihomeContribution()
    {
        this(new Contribution());
        this.sharedLibs=new ArrayList<File>();
    }
    
    public DigihomeContribution(String name, List<String> deployables, List<Import> imports, List<Export> exports, List<File> restrictedLibs)
    {
        this(new Contribution(name, deployables, imports, exports, restrictedLibs));
    }
    
    public DigihomeContribution(Contribution contribution)
    {
        this.contribution=contribution;
        this.sharedLibs=new ArrayList<File>();
    }
    
    public DigihomeContribution(Contribution contribution, List<File> sharedLibs)
    {
        this.contribution=contribution;
        this.sharedLibs=sharedLibs;
    }
    
    public DigihomeContribution(File digihomeContributionFile) throws ZipException, IOException, JDOMException
    {
        this();
        File digihomeContributionDir=ZipUtil.newTempFile();
        ZipUtil.unzipFile(digihomeContributionFile, digihomeContributionDir);
        
        for(File digihomeContributionContentFile : digihomeContributionDir.listFiles())
        {
            /**If a zip is found it's consider as the contribution file*/
            if(digihomeContributionContentFile.getName().endsWith(".zip"))
            {
                this.contributionFile=digihomeContributionContentFile;
                this.contribution=new Contribution(contributionFile);
            }
            /**directory of the shared libs*/
            else if(digihomeContributionContentFile.isDirectory() && digihomeContributionContentFile.getName().equals(SHAREDLIBS_DIRECTORY_NAME))
            {
                for(File sharedLibFile : digihomeContributionContentFile.listFiles())
                {
                    this.sharedLibs.add(sharedLibFile);
                }
            }
            else if(digihomeContributionContentFile.getName().equals(DIGIHOME_PARAMETERS_FILE_NAME))
            {
                try
                {
                    FileInputStream digihomeParametersFis = new FileInputStream(digihomeContributionContentFile);
                    ObjectInputStream digihomeParametersOis = new ObjectInputStream(digihomeParametersFis);
                    Object digihomeParametersObject;
                    digihomeParametersObject = digihomeParametersOis.readObject();
                    digihomeParametersOis.close();
                    
                    if(digihomeParametersObject == null)
                    {
                        logger.info("No DigihomeParameters found in contribution file");
                    }
                    else
                    {
                        this.digihomeParameters = (List<DigihomeParameter>) digihomeParametersObject;
                        logger.info("DigihomeContribution "+this.digihomeParameters.size()+" DigihomeParameter(s) defined");
                    }
                }
                catch (ClassNotFoundException e1)
                {
                    logger.warning("DigihomeParameter file contains wrong data");
                }
                catch (ClassCastException e)
                {
                    logger.warning("DigihomeParameter file contains wrong data");
                }
            }
        }
    }
    
    public List<File> getShraredLibs()
    {
        return sharedLibs;
    }
    
    public File getContributionFile()
    {
        return contributionFile;
    }
    
    public Contribution getContribution()
    {
        return contribution;
    }
    
    public void setDigihomeParameters(List<DigihomeParameter> digihomeParameters)
    {
        this.digihomeParameters = digihomeParameters;
    }
    
    public List<DigihomeParameter> getDigihomeParameters()
    {
        return this.digihomeParameters;
    }
    
    public File generate(File workingDir) throws IOException
    {
        /** Temp directory where file are stored before zipping*/
        File digihomeContributionOutDir = ZipUtil.newTempFile();
        digihomeContributionOutDir.mkdir();
        
        /**copy contribution file*/
        this.contributionFile=this.contribution.generate(digihomeContributionOutDir);
        
        /** Create shared directory*/
        File shareLibDir = new File(digihomeContributionOutDir,SHAREDLIBS_DIRECTORY_NAME);
        shareLibDir.mkdir();

        for(File sharedLib : sharedLibs)
        {
            FileUtils.copyFileToDirectory(sharedLib, shareLibDir);
        }
        
        if(digihomeParameters==null)
        {
            logger.info("DigihomeContribution no DigihomeParameter defined");
        }
        else
        {
            
            File digihomeParametersFile = new File(digihomeContributionOutDir, DIGIHOME_PARAMETERS_FILE_NAME);
            digihomeParametersFile.createNewFile();
            FileOutputStream digihomeParametersFos = new FileOutputStream(digihomeParametersFile);
            ObjectOutputStream digihomeParametersOos = new ObjectOutputStream(digihomeParametersFos);
            digihomeParametersOos.writeObject(digihomeParameters);
            digihomeParametersOos.close();
            for(DigihomeParameter digihomeParameter : this.digihomeParameters)
            {
                    logger.info("DigihomeContribution parameter : "+digihomeParameter.toString());
            }
        }
        
        /** Create contribution zip file*/
        String packagename = "digihome-"+contribution.getName()+ ".zip";
        
        if (!workingDir.exists())
        {
            workingDir.mkdirs();
        }
        File outputZip = new File(workingDir.getAbsolutePath(), packagename);
        /** zip temp directory in zip file*/
        try
        {
            ZipUtil.zip(digihomeContributionOutDir, outputZip);
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Could not create contibution package " + packagename, e);
        }

        return outputZip;
    }
}
