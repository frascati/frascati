/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo;

import java.io.Serializable;

/**
 *
 */
public class DigihomeParameter implements Serializable
{
    private static final long serialVersionUID = -401026848136045194L;

    private String name;
    
    private String type;
    
    private String value;
    
    public DigihomeParameter(){}

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    public String toString()
    {
        StringBuilder toStringBuilder = new StringBuilder();
        toStringBuilder.append("[ "+this.getClass().getSimpleName()+"");
        toStringBuilder.append("name : "+this.name);
        toStringBuilder.append(" , ");
        toStringBuilder.append("type : "+this.type);
        toStringBuilder.append(" , ");
        toStringBuilder.append("value : "+this.value);
        toStringBuilder.append(" ]");
        return toStringBuilder.toString();
    }
}
