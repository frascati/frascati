/**
 * 
 */
package org.ow2.frascati.mojo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;

/**
 * A Maven plugin for compiling SCA composites for Tests.
 * 
 * @execute phase="generate-test-sources"
 * @requiresDependencyResolution compile
 * @goal compile
 */
public class FrascatiCompilerTestMojo extends AbstractFrascatiCompilerMojo
{
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#getCompiledClassesDirectory()
     */
    @Override
    String getCompiledClassesDirectory()
    {
      return "target/test-classes";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#completeBuildSources()
     */
    protected void completeBuildSources()
    {
       // Take into account the default Maven Java source directory if exists.
       File fs = newFile("src/test/java");
       if (fs.exists())
       {
           srcs.add(fs.getAbsolutePath());
       }
    }

    /**
     * {@inheritDoc}
     *
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#
     * completeBuildResources(org.ow2.frascati.assembly.factory.api.ClassLoaderManager)
     */
    protected void completeBuildResources(ClassLoaderManager classLoaderManager)
    { 
        // Add project resource directory if any into the class loader.
        File fr = newFile("src/test/resources");
        if (fr.exists())
        {
            try
            {
                try
                {
                    classLoaderManager.loadLibraries(fr.toURI().toURL());
                } catch (ManagerException e)
                {
                    e.printStackTrace();
                }
            } catch (MalformedURLException mue)
            {
                getLog().error("Invalid file: " + fr);
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see org.ow2.frascati.mojo.AbstractFrascatiMojo#
     * isArtifactToInclude(java.net.URL)
     */
    @Override
    protected boolean isArtifactToInclude(URL artifactUrl)
    {
        return (artifactUrl.toString().endsWith("/target/classes/")
                || artifactUrl.toString().endsWith("/target/test-classes/"));
    }

    /**
     * {@inheritDoc}
     *
     * @see org.ow2.frascati.mojo.AbstractFrascatiCompilerMojo#
     * addSource(java.lang.String)
     */
    @Override
    void addSource(String source)
    {
        project.addTestCompileSourceRoot(source);        
    }

}
