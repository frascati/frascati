/**
 * OW2 FraSCAti Maven Plugin
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Philippe Merle
 *
 * Contributor(s): Christophe Munilla
 *
 */

package org.ow2.frascati.mojo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.LogManager;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.util.context.ContextualProperties;

/**
 * Abstract OW2 FraSCAti Maven plugin.
 */
public abstract class AbstractFrascatiMojo extends AbstractMojo
{
    // ---------------------------------------------------------------------------
    // Constants.
    // --------------------------------------------------------------------------

    /**
     * Path to the Maven local repository.
     */
    static public String M2_REPO = "file:" + System.getProperty("user.home")
            + "/.m2/repository/";

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The Maven project reference.
     * 
     * @parameter expression="${project}"
     * @required
     * @since 1.0
     */
    protected MavenProject project;

    /**
     * The Maven project base dir absolute path.
     */
    protected String projectBaseDirAbsolutePath;

    /**
     * The path to the logging configuration file.
     * 
     * @parameter expression="${loggingConfFile}" default-value=""
     * @since 1.1
     */
    private String loggingConfFile;

    /**
     * Java system properties.
     * 
     * @parameter
     * @since 1.3
     */
    private Map<String, String> systemProperties;

    /**
     * List of contextual properties.
     *
     * @parameter
     * @since 1.6
     */
    private List<ContextualProperty> contextualProperties;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Checks if a MOJO parameter is empty or not.
     * 
     * @param s
     *            a string parameter.
     * @return True if the parameter is not filled.
     */
    protected static boolean isEmpty(String s)
    {
        return ((s == null) || (s.trim().length() == 0));
    }

    /**
     * Create a new file related to the Maven project base dir absolute path.
     * 
     * @param path
     *            the file path.
     * @return a new File.
     */
    protected final File newFile(String path)
    {
        return new File(this.projectBaseDirAbsolutePath + File.separator + path);
    }

    /**
     * Returns the URLs of all Maven project artifacts which are not already
     * present in the current class loader.
     */
    protected final List<URL> getProjectArtifactsNotPresentInCurrentClassLoader(
            ClassLoader currentClassLoader)
    {
        // Get URLs of the current class loader.
        List<URL> urlsAlreadyInCurrentClassLoader = Arrays.asList(
                ((URLClassLoader) currentClassLoader).getURLs());

        List<URL> result = new ArrayList<URL>();

        // Get all the Maven project dependency artifacts.
        Set<Artifact> artifacts = project.getArtifacts();
        for (Artifact artifact : artifacts)
        {
            try
            {
                URL artifactUrl = artifact.getFile().toURI().toURL();
                if (!urlsAlreadyInCurrentClassLoader.contains(artifactUrl))
                {
                    getLog().debug(artifactUrl + " is not present into the current class loader.");
                    
                    if(isArtifactToInclude(artifactUrl))
                    {
                        String artifactPath = M2_REPO
                                + artifact.getGroupId().replace('.', '/') + '/'
                                + artifact.getArtifactId() + '/'
                                + artifact.getVersion() + '/'
                                + artifact.getArtifactId() + '-'
                                + artifact.getVersion() + ".jar";
                        
                        URL url = new URL(artifactPath);
                        
                        if (urlsAlreadyInCurrentClassLoader.contains(url))
                        {
                            getLog().debug("But " + url + " is already present into the current class loader.");
                        } else
                        {
                            result.add(artifactUrl);
                        }
                    } else
                    {
                        result.add(artifactUrl);
                    }                    
                } else
                {
                    getLog().debug(artifactUrl + " is already present into the current class loader.");
                }
            } catch (MalformedURLException e)
            {
                getLog().warn("Malformed URL for artifact " + artifact.getId());
            }
        }
        return result;
    }

    /**
     * Define whether or not the artifact has to be included in the 
     * list of those that are not present in the current ClassLoader
     *  
     * @param artifactUrl
     * @return 
     */
    protected boolean isArtifactToInclude(URL artifactUrl)
    {
        return artifactUrl.toString().endsWith("/target/classes/");
    }

    /**
     * Initialize a ContextualProperties instance.
     *
     * @param contextualProperties The contextual properties instance to initialize.
     */
    protected void initializeContextualProperties(ContextualProperties contextualProperties)
    {
      getLog().debug("Initialize contextual properties.");
      if(this.contextualProperties != null) {
        for(ContextualProperty cp : this.contextualProperties) {
          if(cp.getFile() == null) {
            if(cp.getKey() != null && cp.getValue() != null) {
              getLog().debug("Set the contextual property '" + cp.getKey() + "' with " + cp.getValue());
              contextualProperties.setContextualProperty(cp.getKey(), cp.getValue());
            } else {
              throw new Error("Invalid <contextualProperty>: <key> and <value> must be set!");
            }
          } else {
            if(cp.getValue() != null) {
              throw new Error("Invalid <contextualProperty>: <value> must not be set!");
            }
            Properties properties = new Properties();
            try {
              properties.load(new FileInputStream(cp.getFile()));
            } catch(IOException ioe) {
              throw new Error(ioe);
            }
            if(cp.getKey() != null) {
                getLog().debug("Set the contextual property '" + cp.getKey() + "' with " + cp.getFile());
              contextualProperties.setContextualProperty(cp.getKey(), properties);
            } else {
              for(String propertyName : properties.stringPropertyNames()) {
                Object propertyValue = properties.getProperty(propertyName);
                getLog().debug("Set the contextual property '" + propertyName + "' with " + propertyValue);
                contextualProperties.setContextualProperty(propertyName, propertyValue);
              }
            }
          }
        }
      }
    }

    /**
     * Initialize the current class loader used by the MOJO.
     */
    protected abstract ClassLoader initCurrentClassLoader(
            ClassLoader currentClassLoader) throws FrascatiException;

    /**
     * Execute the MOJO.
     */
    protected abstract void executeMojo() throws FrascatiException;

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * @see AbstractMojo#execute()
     */
    public final void execute() throws MojoExecutionException,
            MojoFailureException
    {
        // Get the absolute path of the base dir of the Maven project.
        this.projectBaseDirAbsolutePath = this.project.getBasedir().getAbsolutePath();

        // Configure logging.
        if (loggingConfFile != null)
        {
            getLog().debug("Configure logging with " + loggingConfFile + ".");
            try
            {
                LogManager.getLogManager().readConfiguration(
                        new FileInputStream(loggingConfFile));
                
            } catch (Exception e)
            {
                getLog().warn("Could not load logging configuration file: " + 
                        loggingConfFile);
            }
        }

        // Get the current thread class loader.
        ClassLoader previousCurrentThreadContextClassLoader = 
            FrascatiClassLoader.getCurrentThreadContextClassLoader();
        
        ClassLoader currentClassLoader = null;

        // Get the current System Properties.
        Properties previousSystemProperties = System.getProperties();

        try
        {
            // Init the class loader used by this MOJO.
            getLog().debug("Init the current class loader.");
            currentClassLoader = initCurrentClassLoader(previousCurrentThreadContextClassLoader);
            if (currentClassLoader != previousCurrentThreadContextClassLoader)
            {
                getLog().debug("Set the current thread's context class loader.");
                FrascatiClassLoader.setCurrentThreadContextClassLoader(
                        currentClassLoader);
            }

            if (systemProperties != null)
            {
                getLog().debug("Configuring Java system properties.");
                Properties newSystemProperties = new Properties();
                newSystemProperties.putAll(previousSystemProperties);
                newSystemProperties.putAll(systemProperties);
                System.setProperties(newSystemProperties);
                getLog().debug(newSystemProperties.toString());
            }

            // Execute the MOJO.
            getLog().debug("Execute the MOJO...");
            executeMojo();
            
        } catch (FrascatiException exc)
        {
            throw new MojoExecutionException(exc.getMessage(), exc);
            
        } finally
        {
            if (currentClassLoader != previousCurrentThreadContextClassLoader)
            {
                getLog().debug("Reset the current thread's context class loader.");
                
                // Reset the current thread's context class loader.
                FrascatiClassLoader.setCurrentThreadContextClassLoader(
                        previousCurrentThreadContextClassLoader);
            }
            getLog().debug("Reset the Java system properties.");
            System.setProperties(previousSystemProperties);
        }

    }
}
