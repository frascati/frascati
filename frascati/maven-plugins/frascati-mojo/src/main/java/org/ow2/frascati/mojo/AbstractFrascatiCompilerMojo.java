/**
 * OW2 FraSCAti Compiler Maven Plugin
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Damien Fournier
 *            Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *                 Christophe Munilla
 *
 */
package org.ow2.frascati.mojo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;
import org.ow2.frascati.util.FrascatiException;

public abstract class AbstractFrascatiCompilerMojo extends AbstractFrascatiMojo
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The name of the SCA composite to compile.
     * 
     * @parameter expression="${compositeName}"
     * @since 1.0
     */
    private String composite;

    /**
     * Names of the SCA composite to compile.
     * 
     * @parameter expression="${compositeNames}"
     * @since 1.2
     */
    private String[] composites;

    /**
     * Space-separated list of source roots.
     * 
     * @parameter
     * @since 1.0
     */
    protected List<String> srcs = new ArrayList<String>();

    /**
     * Space-separated list of lib paths.
     * 
     * @parameter expression="${libs}"
     * @since 1.0
     */
    private String[] libs;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Get the directory where compiled classes are stored.
     */
    abstract String getCompiledClassesDirectory();

    /**
     * Complete the list of sources if needed
     */
    abstract void completeBuildSources();

    /**
     * Complete the list of resources if needed
     * 
     * @param classLoaderManager
     *          the class loader used to load resources
     *          
     */
    abstract void completeBuildResources(ClassLoaderManager classLoaderManager);

    /**
     * Add the source path to the project's list of source paths to compile
     * 
     * @param source
     *          the source path to add
     */
    abstract void addSource(String source);
    
    /**
     * Parse composite names specified either in the composite tag or in the
     * composites tag.
     * 
     * @return the list of composites to compile.
     */
    protected String[] getComposites() throws FrascatiException
    {
        if (!isEmpty(composite))
        { // composite tag is filled
            if (composites.length > 0)
            {
                throw new FrascatiException(
                        "You have to choose ONE (and only one) one way to specify composite name(s): "
                                + "either <composite> or <composites> tag.");
            } else
            {
                String[] tmp = new String[1];
                tmp[0] = composite;
                return tmp;
            }
        } else
        {
            if (composites.length > 0)
            {
                return composites;
            } else
            { // else no composite specified
                return new String[0];
            }
        }
    }

    /**
     * @see AbstractFrascatiMojo#initCurrentClassLoader(ClassLoader)
     */
    @Override
    protected final ClassLoader initCurrentClassLoader(
            ClassLoader currentClassLoader)
    {
        // Get the urls to project artifacts not present in the current class
        // loader.
        List<URL> urls = getProjectArtifactsNotPresentInCurrentClassLoader(currentClassLoader);

        // Add given library paths
        for (String lib : libs)
        {
            File f = null;
            // try to get file using project basedir
            f = newFile(lib);
            if (!f.exists())
            {
                // try to get file using from root directory
                f = new File(lib);
                if (!f.exists())
                {
                    getLog().error("Could not add library path: " + lib);
                    f = null;
                }
            }
            if (f != null)
            {
                try
                {
                    urls.add(f.toURI().toURL());
                } catch (MalformedURLException e)
                {
                    getLog().error("Could not add library path : " + lib);
                }
            }
        }
        // Return a new URLClassLoader with computed urls and having the current
        // class loader as parent.
        return new URLClassLoader(urls.toArray(new URL[urls.size()]),
                currentClassLoader);
    }

    /**
     * @see AbstractFrascatiMojo#executeMojo()
     */
    @Override
    protected final void executeMojo() throws FrascatiException
    {
        // Set the OW2 FraSCAti output directory.
        System.setProperty("org.ow2.frascati.output.directory",
                this.projectBaseDirAbsolutePath + File.separator + "target");

        // Set the directory where compiled classes are stored.
        System.setProperty("org.ow2.frascati.classes.directory",
               this.projectBaseDirAbsolutePath + File.separator + getCompiledClassesDirectory());

        // No Security Manager with FraSCAti Java RMI binding else an issue with
        // Sonar obscurs.
        System.setProperty("org.ow2.frascati.binding.rmi.security-manager", "no");

        // Create a new OW2 FraSCAti instance.
        FraSCAti frascati = FraSCAti.newFraSCAti();

        // Take into account the default Maven Java source directory if exists.
        File fs = newFile("src/main/java");
        if (fs.exists())
        {
            srcs.add(fs.getAbsolutePath());
        }
        completeBuildSources();

        // Get the OW2 FraSCAti class loader manager.
        ClassLoaderManager classLoaderManager = frascati.getClassLoaderManager();

        // Add project resource directory if any into the class loader.
        File fr = newFile("src/main/resources");
        if (fr.exists())
        {
            try
            {
                try
                {
                    classLoaderManager.loadLibraries(fr.toURI().toURL());
                } catch (ManagerException e)
                {
                    e.printStackTrace();
                }
            } catch (MalformedURLException mue)
            {
                getLog().error("Invalid file: " + fr);
            }
        }
        completeBuildResources(classLoaderManager);

        // Add srcs into the class loader.
        for (int i = 0; i < srcs.size(); i++)
        {
            try
            {
                classLoaderManager.loadLibraries(
                        new File(srcs.get(i)).toURI().toURL());
                
            } catch (MalformedURLException mue)
            {
                getLog().error("Invalid file: " + srcs.get(i));
            }
        }
        
        // Process SCA composites.
        for (String compositeName : getComposites())
        {
            getLog().info("Compiling the SCA composite '" + compositeName + "'");
            
            // Prepare the processing context
            ProcessingContext processingContext = frascati.newProcessingContext(
                    ProcessingMode.compile);

            // Initialize contextual properties.
            initializeContextualProperties(processingContext);

            // Declare all Java source directories to compile.
            for (String src : srcs)
            {
            	processingContext.addJavaSourceDirectoryToCompile(src);
            }

            frascati.processComposite(compositeName, processingContext);
            getLog().info("SCA composite '" + compositeName  + "' compiled successfully.");

            // Add Java sources generated by FraSCAti to the Maven project.
            // Then Maven will compile these Java sources.
            for (String directory : processingContext.getJavaSourceDirectoryToCompile())
            {
                getLog().info("Add " + directory + " as Maven compile source root.");
                addSource(directory);
            }
        }
        // Add Java sources, given as parameters to the OW2 FraSCAti compiler,
        // to the Maven project.
        // Then Maven will compile these Java sources too.
        for (String src : srcs)
        {
            addSource(src);
        }
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

}
