/**
 * OW2 FraSCAti: Launcher Maven Plugin
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Christophe Demarey
 *
 * Contributor(s): Valerio Schiavoni, Philippe Merle
 *
 */

package org.ow2.frascati.mojo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.util.FrascatiException;

/**
 * A Maven plugin for running SCA composites.
 * 
 * @requiresDependencyResolution runtime
 * @goal exec
 */
public class FrascatiLauncherMojo
     extends AbstractFrascatiMojo
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The name of the SCA composite to execute.
   *
   * @parameter expression="${compositeName}"
   * @required
   * @since 1.0
   */
  private String composite;

  /**
   * The name of the service to execute.
   *
   * @parameter expression="${serviceName}" default-value=""
   * @since 1.0
   */
  private String service;

  /**
   * The name of the method to execute.
   *
   * @parameter expression="${methodName}" default-value=""
   * @since 1.0
   */
  private String method;

  /**
   * Space-separated list of method parameters.
   *
   * @parameter expression="${params}"
   * @since 1.1
   */
  private String methodParams;

  /**
   * List of method parameters with parameter children tags.
   *
   * @parameter expression="${parameters}"
   * @since 1.1
   */
  private String[] methodParameters;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Parse method parameters specified either in the params tag or in the parameters tag.
   * 
   * Return the list of parameters.
   */
  private String[] getMethodParams() throws FrascatiException
  {
    if( !isEmpty(methodParams) ) { // params tag is filled
      if (methodParameters.length > 0) {
        throw new FrascatiException(
                   "You have to choose ONE (and only one) one way to specify parameters: "
                    + "either <params> or <parameters> tag.");
      } else {
        return methodParams.split(" ");
      }
    } else {
      if (methodParameters.length > 0) {
        return methodParameters;
      } else { // else no parameters nor params
        return new String[0];
      }
    }
  }

  /**
   * @see AbstractFrascatiMojo#initCurrentClassLoader(ClassLoader)
   */
  @Override
  protected final ClassLoader initCurrentClassLoader(ClassLoader currentClassLoader)
      throws FrascatiException
  {
    //
    // When Maven 3.x.y
    //
    if(currentClassLoader instanceof org.codehaus.plexus.classworlds.realm.ClassRealm) {
      org.codehaus.plexus.classworlds.realm.ClassRealm classRealm =
        (org.codehaus.plexus.classworlds.realm.ClassRealm)currentClassLoader;	  

      // Add the current project artifact JAR into the ClassRealm instance.
      try {
        classRealm.addURL(
          newFile( "target" + File.separator + project.getArtifactId() + "-" + project.getVersion() + ".jar")
          .toURL() );
      } catch (MalformedURLException mue) {
        throw new FrascatiException("Could not add the current project artifact jar into the ClassRealm instance", mue);
      }
      
      // Get the urls to project artifacts not present in the current class loader.
      List<URL> urls = getProjectArtifactsNotPresentInCurrentClassLoader(currentClassLoader);
      for(URL url : urls) {
        getLog().debug("Add into the MOJO class loader: " + url);
        classRealm.addURL(url);
      }

      return currentClassLoader;
    }

    //
    // When Maven 2.x.y
    //

    // Maven uses the org.codehaus.classworlds framework for managing its class loaders.
    // ClassRealm is a class loading space.
    org.codehaus.classworlds.ClassRealm classRealm = null;

    // Following allows to obtain the ClassRealm instance of the current MOJO class loader.
    // Here dynamic invocation is used as 'getRealm' is a protected method of a protected class!!!
    try {
      Method getRealmMethod = currentClassLoader.getClass().getDeclaredMethod("getRealm", new Class[0]);
      getRealmMethod.setAccessible(true);
      classRealm = (org.codehaus.classworlds.ClassRealm)getRealmMethod.invoke(currentClassLoader);
    } catch(Exception exc) {
      throw new FrascatiException("Could not get the ClassRealm instance of the current class loader", exc);
    }

    // Add the current project artifact JAR into the ClassRealm instance.
    try {
      classRealm.addConstituent(
        newFile( "target" + File.separator + project.getArtifactId() + "-" + project.getVersion() + ".jar")
        .toURL() );
    } catch (MalformedURLException mue) {
      throw new FrascatiException("Could not add the current project artifact jar into the ClassRealm instance", mue);
    }
    
	// Get the urls to project artifacts not present in the current class loader.
	List<URL> urls = getProjectArtifactsNotPresentInCurrentClassLoader(currentClassLoader);
    for(URL url : urls) {
      getLog().debug("Add into the MOJO class loader: " + url);
      classRealm.addConstituent(url);
    }

    return currentClassLoader;
  }

  /**
   * @see AbstractFrascatiMojo#executeMojo()
   */
  @Override
  protected final void executeMojo() throws FrascatiException
  {
    // Configure Java system properties.
    System.setProperty( "fscript-factory",
                        "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory" );

    // Instantiate FraSCAti.
    FraSCAti frascati = FraSCAti.newFraSCAti();

    // Instantiate and initialize a processing context.
    ProcessingContext processingContext = frascati.newProcessingContext();
    initializeContextualProperties(processingContext);

	Component scaComposite = null;

	if (composite.endsWith(".zip")) {
      File file = new File(this.composite); 
      if (file.exists()) {
        frascati.getCompositeManager().processContribution(this.composite, processingContext);
      } else {
        getLog().error("Cannot find the " + file.getAbsolutePath() + " file!");
        return;
      }
	} else {
      scaComposite = frascati.processComposite(this.composite, processingContext);
    }

    if ( isEmpty(service) ) { // Run in a server mode
      getLog().info("FraSCAti is running in a server mode...");
      getLog().info("Press Ctrl+C to quit...");
      try {
        System.in.read();
      } catch (IOException e) {
        throw new FrascatiException(e);
      }
    } else {
      String[] parameters = getMethodParams();
      getLog().info("Calling the '" + service + "' service: ");
      getLog().info("\tMethod '" + method + "'"
              + ((parameters.length == 0) ? "" : " with params: " + Arrays.toString(parameters)));

      Object result = null;
      try {
        Object object = frascati.getService(scaComposite, this.service, Object.class);
        Class<?> clazz = object.getClass();
        Class<?>[] paramTypes = new Class[parameters.length];
        for (int i = 0; i < parameters.length; ++i) {
          paramTypes[i] = String.class;
        }
        Method method = clazz.getMethod(this.method, paramTypes);

        result = method.invoke(object, parameters);
      } catch (SecurityException se) {
        throw new FrascatiException("Unable to get the method '" + this.method + "'", se);
      } catch (NoSuchMethodException nsme) {
        throw new FrascatiException("The service '" + this.service
            + "' does not provide the method " + this.method + '(' + Arrays.toString(parameters) + ')', nsme);
      } catch (IllegalArgumentException iae) {
        throw new FrascatiException(iae);
      } catch (IllegalAccessException iae) {
        throw new FrascatiException(iae);
      } catch (InvocationTargetException ite) {
        throw new FrascatiException(ite);
      }      

      getLog().info("Call done!");
      if (result != null) {
        getLog().info("Service response:");
        getLog().info(result.toString());
      }
    }

    frascati.close();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
