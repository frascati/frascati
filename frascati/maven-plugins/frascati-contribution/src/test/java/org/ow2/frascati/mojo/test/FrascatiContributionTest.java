/**
 * OW2 FraSCAti Contribution Maven Plugin
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.ZipException;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.io.FileUtils;
import org.apache.cxf.common.util.Base64Utility;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.mojo.Export;
import org.ow2.frascati.mojo.Import;
import org.ow2.frascati.mojo.contribution.Contribution;
import org.ow2.frascati.mojo.util.ZipUtil;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
public class FrascatiContributionTest
{
    private static final String FRASCATI_VERSION="1.6-SNAPSHOT";
    
    private static File workingDir;
    private static File helloworldBindingSCA;
    
    private static FraSCAti frascati;
    private static Deployment deployment;
    private static RemoteScaDomain remoteScaDomain;
    
    @BeforeClass
    public static void init() throws FrascatiException
    {
        workingDir=new File("target/test-classes");
        assertTrue(workingDir.exists());
        helloworldBindingSCA=new File("src/test/resources/helloworld-binding-sca.zip");
        assertTrue(helloworldBindingSCA.exists());
        
        frascati=FraSCAti.newFraSCAti();
        Component frascatiComponent=frascati.getFrascatiComposite();
        deployment=frascati.getService(frascatiComponent, "deployment", Deployment.class);
        remoteScaDomain=frascati.getService(frascatiComponent, "introspection", RemoteScaDomain.class);
    }
    
    @Test
    public void regex()
    {
        String contributionFileName="test";
        assertFalse(contributionFileName.matches(Contribution.CONTRIBUTION_FILE_REGEX));
        contributionFileName="META-INF/";
        assertFalse(contributionFileName.matches(Contribution.CONTRIBUTION_FILE_REGEX));
        contributionFileName="META-INF/.contrib";
        assertFalse(contributionFileName.matches(Contribution.CONTRIBUTION_FILE_REGEX));
        contributionFileName="META-INF/a.contrib";
        assertTrue(contributionFileName.matches(Contribution.CONTRIBUTION_FILE_REGEX));
        contributionFileName="META-INF/contribution.contrib";
        assertTrue(contributionFileName.matches(Contribution.CONTRIBUTION_FILE_REGEX));
        assertEquals("contribution", contributionFileName.replaceAll(Contribution.CONTRIBUTION_FILE_REGEX, "$1"));
        
        String contributionDescriptorFileName="test";
        assertFalse(contributionDescriptorFileName.matches(Contribution.CONTRIBUTION_FILEDESCRIPTOR_REGEX));
        contributionDescriptorFileName="META-INF/";
        assertFalse(contributionDescriptorFileName.matches(Contribution.CONTRIBUTION_FILEDESCRIPTOR_REGEX));
        contributionDescriptorFileName="META-INF/.xml";
        assertFalse(contributionDescriptorFileName.matches(Contribution.CONTRIBUTION_FILEDESCRIPTOR_REGEX));
        contributionDescriptorFileName="META-INF/sca-contribution.xml";
        assertTrue(contributionDescriptorFileName.matches(Contribution.CONTRIBUTION_FILEDESCRIPTOR_REGEX));
        contributionDescriptorFileName="META-INF\\sca-contribution.xml";
        assertTrue(contributionDescriptorFileName.matches(Contribution.CONTRIBUTION_FILEDESCRIPTOR_REGEX));
        
        String libFileName="test";
        assertFalse(libFileName.matches(Contribution.LIB_FILE_REGEX));
        libFileName="META-INF/";
        assertFalse(libFileName.matches(Contribution.LIB_FILE_REGEX));
        libFileName="lib/.jar";
        assertFalse(libFileName.matches(Contribution.LIB_FILE_REGEX));
        libFileName="lib/libFile.xml";
        assertFalse(libFileName.matches(Contribution.LIB_FILE_REGEX));
        libFileName="lib/libFilejar";
        assertFalse(libFileName.matches(Contribution.LIB_FILE_REGEX));
        libFileName="lib/libFile.jar";
        assertTrue(libFileName.matches(Contribution.LIB_FILE_REGEX));
        assertEquals("libFile", libFileName.replaceAll(Contribution.LIB_FILE_REGEX, "$1"));
        assertEquals("libFile.jar", libFileName.replaceAll(Contribution.LIB_FILE_REGEX, "$1$2"));
        libFileName="lib\\libFile.jar";
        assertTrue(libFileName.matches(Contribution.LIB_FILE_REGEX));
        assertEquals("libFile", libFileName.replaceAll(Contribution.LIB_FILE_REGEX, "$1"));
        assertEquals("libFile.jar", libFileName.replaceAll(Contribution.LIB_FILE_REGEX, "$1$2"));
        
        String nsDeployable="prefix:uri:composite.composite";
        assertTrue(nsDeployable.matches(Contribution.NAMESPACE_DEPLOYABLE_REGEX));
        assertEquals("prefix", nsDeployable.replaceAll(Contribution.NAMESPACE_DEPLOYABLE_REGEX, "$1"));
        assertEquals("uri", nsDeployable.replaceAll(Contribution.NAMESPACE_DEPLOYABLE_REGEX, "$2"));
        assertEquals("composite.composite", nsDeployable.replaceAll(Contribution.NAMESPACE_DEPLOYABLE_REGEX, "$3"));
        assertEquals("prefix:composite.composite", nsDeployable.replaceAll(Contribution.NAMESPACE_DEPLOYABLE_REGEX, "$1:$3"));
    }
    
    @Test
    public void create() throws IOException, JDOMException
    {
        List<String> deployables=new ArrayList<String>();
        deployables.add("ns:org/ow2/frascati/demo:composite1.composite");
        deployables.add("composite1.composite");
        deployables.add("composite2.composite");

        List<Import> imports=new ArrayList<Import>();
        Import impJava=new Import("java","package.to.import");
        imports.add(impJava);
        Import impResource=new Import("resource","path/to/resource/to/import");
        imports.add(impResource);
        
        List<Export> exports=new ArrayList<Export>();
        Export expJava=new Export("java","package.to.export");
        exports.add(expJava);
        Export expResource=new Export("resource","path/to/resource/to/export");
        exports.add(expResource);
        
        Contribution contribution=new Contribution("contribution-test-create",deployables,imports,exports);
        contribution.generate(workingDir);
        
        File contributionFile=new File(workingDir,"contribution-test-create.zip");
        assertTrue(contributionFile.exists());
        Contribution checkContribution=new Contribution(contributionFile);
        assertEquals("contribution-test-create", checkContribution.getName());
        assertEquals(3, checkContribution.getDeployable().size());
        assertTrue(checkContribution.getDeployable().contains("composite1.composite"));
        assertTrue(checkContribution.getDeployable().contains("composite2.composite"));
        assertTrue(checkContribution.getDeployable().contains("ns:composite1.composite"));
        assertEquals(2, checkContribution.getImports().size());
        assertTrue(checkContribution.getImports().contains(impJava));
        assertTrue(checkContribution.getImports().contains(impResource));
        assertEquals(2, checkContribution.getExports().size());
        assertTrue(checkContribution.getExports().contains(expJava));
        assertTrue(checkContribution.getExports().contains(expResource));
        assertEquals(0, checkContribution.getLibs().size());
    }
    
    @Test
    public void createFromFile() throws IOException, JDOMException, FrascatiException
    {
        Contribution contribution=new Contribution(helloworldBindingSCA);
        contribution.setName("contribution-renamed-0");
        contribution.generate(workingDir);
        File contributionFile=new File(workingDir,"contribution-renamed-0.zip");
        assertTrue(contributionFile.exists());
        
        contribution=new Contribution(helloworldBindingSCA);
        contribution.setName("contribution-renamed-1");
        contribution.addDeployable("deployable0");
        File lib=new File(workingDir,"helloworld-annotated.jar");
        contribution.addLib(lib);
        contribution.addDeployable("deployable1.composite");
        contribution.addImport("java", "package.to.import");
        contribution.addImport("resource", "path/to/resource/to/import");
        contribution.addExport("java", "package.to.export");
        contribution.addExport("resource", "path/to/resource/to/export");
        contribution.generate(workingDir);
        contributionFile=new File(workingDir,"contribution-renamed-1.zip");
        assertTrue(contributionFile.exists());
    }
    
    @Test
    public void addDependencies() throws IOException, JDOMException
    {
        /**create contribution*/
        Contribution helloworldAnnotatedContribution=new Contribution();
        helloworldAnnotatedContribution.setName("helloworld-annotated");
        helloworldAnnotatedContribution.addDeployable("helloworld-wired");
        /**add export*/
        helloworldAnnotatedContribution.addExport("java", "org.ow2.frascati.examples.helloworld.annotated");
        /** add lib*/
        File helloworldAnnotated=new File(workingDir,"helloworld-annotated.jar");
        assertTrue(helloworldAnnotated.exists());
        helloworldAnnotatedContribution.addLib(helloworldAnnotated);
        File helloworldAnnotatedContributionFile=helloworldAnnotatedContribution.generate(workingDir);
        String helloworldAnnotatedContributionString=this.getStringFromFile(helloworldAnnotatedContributionFile);
        /**deploy the generated contribution*/
        deployment.deployContribution(helloworldAnnotatedContributionString);
        
        /**create contribution from an other one*/
        Contribution helloworldSCAReconfigured=new Contribution(helloworldBindingSCA);
        assertNotNull(helloworldSCAReconfigured);
        helloworldSCAReconfigured.setName("helloworld-binding-sca-reconfigured");
        /**add export*/
        helloworldSCAReconfigured.addImport("java", "org.ow2.frascati.examples.helloworld.annotated");
        File helloworldSCAReconfiguredFile=helloworldSCAReconfigured.generate(workingDir);
        String helloworldSCAReconfiguredString=this.getStringFromFile(helloworldSCAReconfiguredFile);
        /**deploy the generated contribution*/
        deployment.deployContribution(helloworldSCAReconfiguredString);

        /**remove the old binding and add a new one*/
        remoteScaDomain.removeBinding("helloworld-binding-sca/client/client/s", "0");
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        params.add("kind", "SCA");
        params.add("uri", "helloworld-wired/server/PrintService");
        remoteScaDomain.addBinding("helloworld-binding-sca/client/client/s", params);

        /**invoke client method to check binding is ok*/
        params.clear();
        params.add("methodName", "run");
        remoteScaDomain.invokeMethod("helloworld-binding-sca/client/client/Runnable", params);
        
        /**undeploy previous composite*/
        deployment.undeployComposite("helloworld-wired");
        deployment.undeployComposite("helloworld-binding-sca");
    }
    
    @Test
    public void setCompositeFileName() throws ZipException, IOException, JDOMException
    {
        Contribution contribution=new Contribution(helloworldBindingSCA);
        List<String> deployables=contribution.getDeployable();
        assertTrue(deployables.contains("helloworld-binding-sca.composite"));
        assertFalse(deployables.contains("newCompositeName.composite"));
        
        contribution.setName("contribution-setcompositefilename");
        contribution.setCompositeFileName("helloworld-binding-sca", "newCompositeName");
        
        deployables=contribution.getDeployable();
        assertFalse(deployables.contains("helloworld-binding-sca.composite"));
        assertTrue(deployables.contains("newCompositeName.composite"));
        
        contribution.generate(workingDir);
        File contributionFile=new File(workingDir,"contribution-setcompositefilename.zip");
        assertTrue(contributionFile.exists());
        File unzippedContributionDir=ZipUtil.newTempFile();
        ZipUtil.unzipFile(contributionFile, unzippedContributionDir);
        assertTrue(unzippedContributionDir.exists());
        File compositeJarFile=new File(unzippedContributionDir,"lib/org_ow2_frascati_examples/helloworld-binding-sca-"+FRASCATI_VERSION+".jar");
        assertTrue(compositeJarFile.exists());
        File unzippedCompositeDir=ZipUtil.newTempFile();
        ZipUtil.unzipFile(compositeJarFile, unzippedCompositeDir);
        assertTrue(unzippedCompositeDir.exists());
        File oldCompositeFile=new File(unzippedCompositeDir,"helloworld-binding-sca.composite");
        assertFalse(oldCompositeFile.exists());
        File compositeFile=new File(unzippedCompositeDir,"newCompositeName.composite");
        assertTrue(compositeFile.exists());
    }
    
    @Test
    public void setCompositeName() throws ZipException, IOException, JDOMException
    {
        Contribution contribution=new Contribution(helloworldBindingSCA);
        contribution.setName("contribution-setcompositename");
        contribution.setCompositeName("helloworld-binding-sca", "newName");
        contribution.generate(workingDir);
        File contributionFile=new File(workingDir,"contribution-setcompositename.zip");
        assertTrue(contributionFile.exists());
        
        
        File unzippedContributionDir=ZipUtil.newTempFile();
        ZipUtil.unzipFile(contributionFile, unzippedContributionDir);
        assertTrue(unzippedContributionDir.exists());
        File compositeJarFile=new File(unzippedContributionDir,"lib/org_ow2_frascati_examples/helloworld-binding-sca-"+FRASCATI_VERSION+".jar");
        assertTrue(compositeJarFile.exists());
        File unzippedCompositeDir=ZipUtil.newTempFile();
        ZipUtil.unzipFile(compositeJarFile, unzippedCompositeDir);
        assertTrue(unzippedCompositeDir.exists());
        File compositeFile=new File(unzippedCompositeDir,"helloworld-binding-sca.composite");
        assertTrue(compositeFile.exists());
        
        SAXBuilder saxBuilder=new SAXBuilder();
        Document doc=saxBuilder.build(compositeFile);
        Element composite=doc.getRootElement();
        Attribute nameAttribute=composite.getAttribute("name");
        assertEquals("newName",nameAttribute.getValue());
    }
    
//    //@Test
    public void mcpApps() throws ZipException, IOException, JDOMException
    {
        File mcpApps=new File("src/test/resources/mcp-apps.zip");
        assertTrue(mcpApps.exists());
        String mcApssString=this.getStringFromFile(mcpApps);
        deployment.deployContribution(mcApssString);
        
        String sequence = UUID.randomUUID().toString().substring(0,6);
        Contribution c = new Contribution(mcpApps);
        c.setName("mcp-apps"+sequence);
        String compositeFileName = "metric";
        c.setCompositeFileName(compositeFileName,compositeFileName+sequence);
        String compositeName = "mutlicloudmetric";
        c.setCompositeName(compositeFileName+sequence,compositeName+sequence);
        File mcpAppsReconf=c.generate(workingDir);
        String mcpAppsReconfString=this.getStringFromFile(mcpAppsReconf);
        deployment.deployContribution(mcpAppsReconfString);
        
        deployment.undeployComposite(compositeName);
        deployment.undeployComposite(compositeName+sequence);
    }
    
    @AfterClass
    public static void close() throws FrascatiException
    {
        frascati.close();
    }
    
    private String getStringFromFile(File f) throws IOException
    {
        byte[] fileBytes=FileUtils.readFileToByteArray(f);
        return Base64Utility.encode(fileBytes);
    }
}
