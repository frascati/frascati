/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo.helper;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Dependency;
import org.apache.maven.project.MavenProject;
import org.ow2.frascati.mojo.FrascatiContributionMojo;

/**
 *
 */
public class FrascatiContributionMojoHelper extends FrascatiMojoHelper
{
    /**
     * Groups to exclude to the copied dependencies
     * 
     */
    protected List<String> excludeGroups;
    
    
    /**
     * @param project
     * @param localRepository
     * @param remoteRepositories
     * @param artifactResolver
     * @param factory
     */
    public FrascatiContributionMojoHelper(MavenProject project, ArtifactRepository localRepository, List<?> remoteRepositories,
            ArtifactResolver artifactResolver, ArtifactFactory factory, List<String> excludeGroups)
    {
        super(project, localRepository, remoteRepositories, artifactResolver, factory);
        
      if (excludeGroups == null)
      {
          excludeGroups = new ArrayList<String>();
      }

      if (!excludeGroups.contains("none"))
      {
          // by default, we automatically add "org.ow2.frascati" &
          // "org.objectweb.fractal.bf" to the ignore list, if the none
          // exclude is found all those groups are not exclude
          excludeGroups.add("org.ow2.frascati");
          excludeGroups.add("org.objectweb.fractal.bf");
          System.out.println();
          logger.info("org.ow2.frascati and org.objectweb.fractal.bf are added to excludeGroups, to avoid this add <exclude>none</exclude> to the exportGroups");
      }
      this.excludeGroups=excludeGroups;
    }
    
    /**
     * Add dependencies of the current project
     * If transitive param is true true all dependencies of the current Maven project will be transitively added to the contribution.
     * Otherwise only direct dependencies will be added.
     * 
      * @param jars libs of the contribution
     */
    public void resolveDependencies(Dependency[] include, boolean transitive, List<File> libs)
    {
        /** Add the direct dependencies of the project */
        this.addDependencies(this.project, this.project.getDependencies(),transitive, libs, true);

        /** Add include dependencies */
        if (include != null)
        {
            this.addDependencies(this.project, Arrays.asList(include),transitive, libs, false);
        }
    }

    /**
     * Add maven dependencies file to dependencies List 
     * 
     * @param project container of the dependencies
     * @param dependencies list of maven dependency
     * @param transitive dependency must be resolved in a transitive way
     * @param libs the list to fill with dependencies files
     * @param checkValidity check if the dependency is valid(ie not in exclude group), used to add include dependencies that are not checked
     */
    private void addDependencies(MavenProject project, List<?> dependencies, boolean transitive,  List<File> libs, boolean checkValidity)
    {
        Dependency dependency;
        for (Object dependencyObject : dependencies)
        {
            dependency = (Dependency) dependencyObject;
            if(checkValidity && !isValidDependency(dependency))
            {
                logger.info(dependency.toString() + " is not valid");
            }
            else
            {
                this.addDependency(project, dependency, transitive, libs);
            }
        }
    }

    /**
     * Add a dependency for a project
     * MavenProject is needed because ArtifactResolver cannot resolve dependency version defined by a Maven property
     * See org.ow2.frascati.mojo.util.MavenUtils.getPropertyValue(MavenProject, String) for further details about property value resolution
     * 
     * @param project container of the dependencies
     * @param dependency dependency to add to contribution
     * @param jars libs of the contribution
     */
    private void addDependency(MavenProject project, Dependency dependency, boolean transitive, List<File> libs)
    {
        try
        {
            Artifact dependencyArtifact=this.getDepencendyArtifact(project, dependency);
            File dependencyFile = this.getArtifactFile(dependencyArtifact);
            libs.add(dependencyFile);
            logger.info("Add lib : "+dependency);
            /** Add dependencies of the current project if transitive */
            if (transitive)
            {
                    MavenProject pomProject = this.getMavenProject(dependencyArtifact);
                    this.addDependencies(pomProject, pomProject.getDependencies(),transitive, libs, true);
            }
        } catch (Exception e)
        {
            logger.severe("Exception when resolving artifact for dependency " + dependency);
        }
    }

    /**
     * We assert that a dependency has to be included
     * 
     * @param dependency to test
     * @return
     */
    public boolean isValidDependency(Dependency dependency)
    {

        // we should not integrate dependencies for the
        // 'test' and 'provided' Maven scopes
        if ((dependency.getScope() != null) && ((dependency.getScope().equalsIgnoreCase("test")) || (dependency.getScope().equalsIgnoreCase("provided"))))
            return false;

        if (!excludeGroups.isEmpty())
        {
            // the dependency should not be part of one of
            // the forbidden groups, declared into the pom
            for (String excludeGroup : excludeGroups)
            {
                if (dependency.getGroupId().compareTo(excludeGroup) == 0)
                    return false;
            }
        }
        
        return true;
    }

}
