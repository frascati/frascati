/**
 * OW2 FraSCAti Contribution Maven Plugin
 *
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo;

/**
 *
 * Helper class to marshal import and export definition in pom file
 *
 */
public abstract class ImportExport
{
    /*type of the import : java, resource ....*/
    public String type;
    
    /*value of the import export*/
    public String value;
    
    public ImportExport(){}
    
    public ImportExport(String type, String value)
    {
        this.type=type;
        this.value=value;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    /**
     * Get the attribute tag to write in the pom file (java : package, resource : uri)
     * 
     * TODO using generic types and SCA model elements 
     * 
     * @return the related attribute tag of this import
     */
    public String getAttributeTag()
    {
        if("java".equals(type))
        {
            return "package";
        }
        
        if("resource".equals(type))
        {
            return "uri";
        }
        
        return "anyAttribute";
    }

    @Override
    public boolean equals(Object obj)
    {
        if(!this.getClass().isInstance(obj))
        {
            return false;
        }
        ImportExport importExport=(ImportExport) obj;
        return this.type.equals(importExport.getType()) && this.value.equals(importExport.getValue());
    }
    
    
    
}
