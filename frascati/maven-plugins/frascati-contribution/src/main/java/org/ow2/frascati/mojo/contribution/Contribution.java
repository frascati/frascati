/**
 * OW2 FraSCAti Contribution Maven Plugin
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.mojo.contribution;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.ow2.frascati.mojo.Export;
import org.ow2.frascati.mojo.Import;
import org.ow2.frascati.mojo.util.ZipUtil;

/**
 * Utils to create and set content of a FraSCAti contribution
 */
public class Contribution
{
    private static final Logger logger = Logger.getLogger(Contribution.class.getName());
    private static final Namespace defaultSCANamespace = Namespace.getNamespace("http://www.osoa.org/xmlns/sca/1.0");

    private static final String METAINF_DIRECTORY_NAME="META-INF";
    private static final String CONTRIBUTION_FILE_EXTENSION="contrib";
    private static final String CONTRIBUTION_FILEDESCRIPTOR_NAME="sca-contribution.xml";
    private static final String LIB_DIRECTORY_NAME="lib";
    
    public final static String CONTRIBUTION_FILE_REGEX =METAINF_DIRECTORY_NAME+".(.+)\\."+CONTRIBUTION_FILE_EXTENSION;
    public final static String CONTRIBUTION_FILEDESCRIPTOR_REGEX=METAINF_DIRECTORY_NAME+"(.)"+CONTRIBUTION_FILEDESCRIPTOR_NAME;
    public final static String LIB_FILE_REGEX=LIB_DIRECTORY_NAME+".(.+)(\\.jar)";
    public final static String NAMESPACE_DEPLOYABLE_REGEX="(.+):(.+):(.+\\.composite)";
    
    private String name;
    private List<String> deployables;
    private List<Import> imports;
    private List<Export> exports;
    private File tmpLibDirectory;
    
    /**
     * Default constructor, init list, name to "undefined" and create a lib temp directory
     */
    public Contribution()
    {
        this.name="undefined";
        this.deployables=new ArrayList<String>();
        this.imports=new ArrayList<Import>();
        this.exports=new ArrayList<Export>();
        this.tmpLibDirectory=ZipUtil.newTempFile();
        this.tmpLibDirectory.mkdir();
    }
    
    public Contribution(String name, List<String> deployables, List<Import> imports, List<Export> exports)
    {
        this();
        this.name=name;
        if(deployables!=null)
        {
            this.deployables=deployables;
        }
        if(imports!=null)
        {
            this.imports=imports;
        }
        if(exports!=null)
        {
            this.exports=exports;
        }
    }
    
    public Contribution(String name, List<String> deployables, List<Import> imports, List<Export> exports, Map<File, String> jars)
    {
        this(name,deployables,imports,exports);
        if (jars!=null)
        {
            this.copyLibsFromMap(jars);
        }
    }
    
    public Contribution(String name, List<String> deployables, List<Import> imports, List<Export> exports, List<File> jars)
    {
        this(name,deployables,imports,exports);
        for(File jar : jars)
        {
            this.addLib(jar);
        }
    }

   
    /**
     * Create a new Contribution Object from a contribution zip file
     * 
     * @param contributionFile
     * @throws ZipException
     * @throws IOException
     * @throws JDOMException
     */
    public Contribution(File contributionFile) throws ZipException, IOException, JDOMException
    {
        this();
        
        ZipFile zipFile = new ZipFile(contributionFile);
        Enumeration<? extends ZipEntry> entries = zipFile.entries();

        String entryName;
        while (entries.hasMoreElements())
        {
            ZipEntry entry = entries.nextElement();
            entryName=entry.getName();
            logger.fine("entry : "+entryName);
            if(entryName.matches(CONTRIBUTION_FILE_REGEX))
            {
                this.name=entryName.replaceAll(CONTRIBUTION_FILE_REGEX, "$1");
                logger.fine("set contribution name : "+this.name);
            }
            else if(entryName.matches(CONTRIBUTION_FILEDESCRIPTOR_REGEX))
            {
                logger.fine("Contribution descriptor : "+entryName);
                this.proceedSCAontributionFile(zipFile.getInputStream(entry));
            }
            else if(entryName.matches(LIB_FILE_REGEX))
            {
                logger.fine("lib "+entryName);
                this.copyLibFromZip(zipFile, entry);
            }
        }
    }

    /**
     * Extract information from sca-contribution.xml file and init deployable, imports and exports
     * 
     */
    @SuppressWarnings("unchecked")
    private Element proceedSCAontributionFile(InputStream contributionInputStream) throws JDOMException, IOException
    {
        logger.fine("readSCAContributionFile ");
        SAXBuilder saxBuilder=new SAXBuilder();
        Document document=saxBuilder.build(contributionInputStream);
        Element contribution=document.getRootElement();
        
        List<Element> deployablesElement=contribution.getChildren("deployable",defaultSCANamespace);
        String compositeName;
        for(Element deployable : deployablesElement)
        {
            compositeName=deployable.getAttributeValue("composite");
            logger.fine("deployable : "+compositeName);
            this.deployables.add(compositeName);
        }
        
        List<Element> javaImportsElement=contribution.getChildren("import.java",defaultSCANamespace);
        String packageName;
        Import imp;
        for(Element javaImport : javaImportsElement)
        {
            packageName=javaImport.getAttributeValue("package");
            logger.fine("import package  : "+packageName);
            imp=new Import();
            imp.setType("java");
            imp.setValue(packageName);
            this.imports.add(imp);
        }
        
        String uri;
        List<Element> resourceImportsElement=contribution.getChildren("import.resource",defaultSCANamespace);
        for(Element resourceImport : resourceImportsElement)
        {
            uri=resourceImport.getAttributeValue("uri");
            logger.fine("import resource  : "+uri);
            imp=new Import();
            imp.setType("resource");
            imp.setValue(uri);
            this.imports.add(imp);
        }
        
        List<Element> javaExportsElement=contribution.getChildren("export.java",defaultSCANamespace);
        Export exp;
        for(Element javaImport : javaExportsElement)
        {
            packageName=javaImport.getAttributeValue("package");
            logger.fine("export package  : "+packageName);
            exp=new Export();
            exp.setType("java");
            exp.setValue(packageName);
            this.exports.add(exp);
        }
        
        List<Element> resourceExportsElement=contribution.getChildren("export.resource",defaultSCANamespace);
        for(Element resourceImport : resourceExportsElement)
        {
            uri=resourceImport.getAttributeValue("uri");
            logger.fine("export resource  : "+uri);
            exp=new Export();
            exp.setType("resource");
            exp.setValue(uri);
            this.exports.add(exp);
        }
        return contribution;
    }
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    
    public List<String> getDeployable()
    {
        return this.deployables;
    }
    
    /**
     * Add all deployable element of the list
     * 
     * @param deployable the element to add
     */
    public void addDeployables(String... deployables)
    {
        for(String deployable : deployables)
        {
            this.addDeployable(deployable);
        }
    }
    
    /**
     * Add a deployable element
     * 
     * @param deployable the element to add
     */
    public void addDeployable(String deployable)
    {
        String deployableComposite=deployable;
        if(!deployable.endsWith(".composite"))
        {
            deployableComposite+=".composite";
        }
        this.deployables.add(deployableComposite);
    }
    
    /**
     * Remove a deployable element
     * 
     * @param deployable the element to remove
     * @return true if remove is done, false otherwise
     */
    public boolean removeDeployable(String deployable)
    {
        String deployableComposite=deployable;
        if(!deployable.endsWith(".composite"))
        {
            deployableComposite+=".composite";
        }
        return this.deployables.remove(deployableComposite);
    }
    
    public List<Import> getImports()
    {
        return this.imports;
    }
    
    /**
     * Add a new Import element
     * 
     * @param type type of the new Import (java, resource)
     * @param value value of the new Import
     */
    public void addImport(String type, String value)
    {
        Import imp=new Import();
        imp.setType(type);
        imp.setValue(value);
        this.addImport(imp);
    }

    /**
     * Add a new Import element
     * 
     * @param imp Import to add
     */
    public void addImport(Import imp)
    {
        this.imports.add(imp);
    }
    
    /**
     * Remove an Import element
     * 
     * @param type type of the new Import (java, resource)
     * @param value value of the new Import
     * @return true if remove is done, false otherwise
     */
    public boolean removeImport(String type, String value)
    {
        Import imp=new Import();
        imp.setType(type);
        imp.setValue(value);
        return removeImport(imp);
    }
    
    /**
     * Remove an Import element
     * 
     * @param imp Import object to remove
     * @return true if remove is done, false otherwise
     */
    public boolean removeImport(Import imp)
    {
        return imports.remove(imp);
    }
    
    public List<Export> getExports()
    {
        return this.exports;
    }
    
    /**
     * Add a new Export element
     * 
     * @param type type of the new Export (java, resource)
     * @param value value of the new Export
     */
    public void addExport(String type, String value)
    {
        Export exp=new Export();
        exp.setType(type);
        exp.setValue(value);
        this.addExport(exp);
    }

    /**
     * Add a new Export element
     * 
     * @param exp Export to add
     */
    public void addExport(Export exp)
    {
        this.exports.add(exp);
    }
    
    /**
     * Remove an Export element
     * 
     * @param type type of the new Export (java, resource)
     * @param value value of the new Export
     * @return true if remove is done, false otherwise
     */
    public boolean removeExport(String type, String value)
    {
        Export exp=new Export();
        exp.setType(type);
        exp.setValue(value);
        return removeExport(exp);
    }
    
    /**
     * Remove an Export element
     * 
     * @param imp Export object to remove
     * @return true if remove is done, false otherwise
     */
    public boolean removeExport(Export exp)
    {
        return exports.remove(exp);
    }
    
    public List<File> getLibs()
    {
        Collection<File> libFiles=ZipUtil.listFile(tmpLibDirectory);
        return new ArrayList<File>(libFiles);
    }
    
    /**
     * Copy lib file in the temp lib directory
     * 
     * @param libFile the libFile
     * @return true if lib is added, false if IOException occurs during file copy
     */
    public boolean addLib(File libFile)
    {
        try
        {
            FileUtils.copyFileToDirectory(libFile, this.tmpLibDirectory);
            return true;
        }
        catch (IOException ioException)
        {
            logger.log(Level.SEVERE,"Could not copy lib "+libFile.getName()+" in the temporary librairies directory",ioException);
            return false;
        }
    }
    
    /**
     * Remove libFile from temp lib directory
     * 
     * @param libFile the file to remove
     * @return true if remove is done, false otherwise
     */
    public boolean removeLib(File libFile)
    {
       return removeLib(libFile.getName());
    }
    
    /**
     * Remove file name libName from temp lib directory
     * 
     * @param libName the name of the lib to remove
     * @return true if remove is done, false otherwise
     */
    public boolean removeLib(String libName)
    {
        String fullLibName=libName;
        if(libName.endsWith(".jar"))
        {
            fullLibName+=".jar";
        }
        
        List<File> libFiles=getLibs();
        for(File lib : libFiles)
        {
            if(lib.getName().equals(fullLibName))
            {
                lib.delete();
                return true;
            }
        }
        logger.info("Contribution removeLib : no lib named "+fullLibName+" can be found");
        return false;
        
    }
    
    public File generate(File workingDir) throws IOException
    {
        /** Temp directory where file are stored before zipping*/
        File contributionOutDir = ZipUtil.newTempFile();
        contributionOutDir.mkdir();
        
        /** Create META-INF directory*/
        File metaDir = new File(contributionOutDir,METAINF_DIRECTORY_NAME);
        metaDir.mkdir();

        /** Create a void file having the name (identifier) of the contribution*/
        try
        {
            String contributionIdentifierName=this.name+"."+CONTRIBUTION_FILE_EXTENSION;
            File contributionIdentifier = new File(metaDir,contributionIdentifierName);
            contributionIdentifier.createNewFile();
        }
        catch (IOException ioException)
        {
            logger.log(Level.SEVERE,"Could not create contribution identifier",ioException);
        }

        /** Create JDom Element representing the contribution descriptor file content*/
        logger.fine("Creating SCA contribution descriptor");
        Element contribution = new Element("contribution");
        contribution.setNamespace(defaultSCANamespace);
        
        /** Add deployables */
        Element element;
        Attribute attribute;
        String nsPrefix, nsURI, deployableValue;
        Namespace deployableNamespace;
        for (String deployable : this.deployables)
        {
            logger.fine("deployable "+ deployable);
            deployableValue=deployable;
            deployableNamespace=defaultSCANamespace;
            
            if(deployable.matches(NAMESPACE_DEPLOYABLE_REGEX))
            {
                nsPrefix=deployable.replaceAll(NAMESPACE_DEPLOYABLE_REGEX, "$1");
                nsURI=deployable.replaceAll(NAMESPACE_DEPLOYABLE_REGEX, "$2");
                deployableValue=deployable.replaceAll(NAMESPACE_DEPLOYABLE_REGEX, "$1:$3");
                deployableNamespace=Namespace.getNamespace(nsPrefix, nsURI);
                logger.info("Add Namespace "+nsPrefix+" "+nsURI+" for deployableValue "+deployableValue);
            }
            
            element=new Element("deployable");
            element.setNamespace(defaultSCANamespace);
            element.addNamespaceDeclaration(deployableNamespace);
            attribute=new Attribute("composite", deployableValue);
            element.setAttribute(attribute);
            contribution.addContent(element);
        }
        
        /** Add imports */
        String importName;
        for (Import importObject : imports)
        {
            importName = "import." + importObject.getType();
            element = new Element(importName);
            element.setNamespace(defaultSCANamespace);
            attribute = new Attribute(importObject.getAttributeTag(), importObject.getValue());
            element.setAttribute(attribute);
            contribution.addContent(element);
        }

        /** Add exports */
        String exportName;
        for (Export exportObject : exports)
        {
            exportName = "export." + exportObject.getType();
            element = new Element(exportName);
            element.setNamespace(defaultSCANamespace);
            attribute = new Attribute(exportObject.getAttributeTag(), exportObject.getValue());
            element.setAttribute(attribute);
            contribution.addContent(element);
        }

        /** Create contribution descriptor file*/
        /** Write JDOM contribution object in contribution descriptor file*/
        Document document = new Document();
        document.setRootElement(contribution);
        File contributionFileDescriptor = new File(metaDir, CONTRIBUTION_FILEDESCRIPTOR_NAME);
        contributionFileDescriptor.createNewFile();
        XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
        FileOutputStream outputStream = new FileOutputStream(contributionFileDescriptor);
        sortie.output(document, outputStream);
        outputStream.close();
        
        /** Create lib directory and copy  temp lib directory inside it*/
        File libDir = new File(contributionOutDir,LIB_DIRECTORY_NAME);
        FileUtils.copyDirectory(tmpLibDirectory, libDir);
        
        /** Create contribution zip file*/
        String packagename = name + ".zip";
        
        if (!workingDir.exists())
        {
            workingDir.mkdirs();
        }
        File outputZip = new File(workingDir.getAbsolutePath(), packagename);
        /** zip temp directory in zip file*/
        try
        {
            ZipUtil.zip(contributionOutDir, outputZip);
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE, "Could not create contibution package " + packagename, e);
        }

        return outputZip;
    }

    /**
     * Copy all libs contained in the map into le temp lib directory
     * 
     * @param libMap a map where key are files name with directory path
     */
    private void copyLibsFromMap(Map<File,String> libMap)
    {
        File libDest;
        String libName,fullLibName;
        try
        {
            for (File lib : libMap.keySet())
            {
                libName = lib.getName();
                fullLibName = libMap.get(lib);
                if (fullLibName != null && !"".equals(fullLibName))
                {
                    libName = fullLibName;
                }

                libDest = new File(this.tmpLibDirectory, libName);
                logger.fine("copyLibFromMap libDest : " + libDest.getAbsolutePath());

                /** if the lib not already exists copy it */
                if (!libDest.exists())
                {
                    FileUtils.copyFile(lib, libDest);
                    logger.fine("Added library " + lib.getName());
                }
            }
        } catch (IOException ioException)
        {
            logger.log(Level.SEVERE, "Problem with the dependency management.", ioException);
        }
    }
    
    /**
     * Copy a lib zip entry in the lib temp directory 
     * 
     * @param zipFile
     * @param libEntry
     */
    private void copyLibFromZip(ZipFile zipFile, ZipEntry libEntry)
    {
        try
        {
            String entryName = libEntry.getName();
            String libName = entryName.replaceAll(LIB_FILE_REGEX, "$1$2");
            File libDestination = new File(this.tmpLibDirectory, libName);
            libDestination.getParentFile().mkdirs();
            InputStream in = zipFile.getInputStream(libEntry);
            OutputStream out = new FileOutputStream(libDestination);
            IOUtils.copy(in, out);
            in.close();
            out.close();
            logger.fine("Added library " + libDestination.getName());
        } catch (IOException ioException)
        {
            logger.log(Level.SEVERE, "Problem when trying to copy lib file " + libEntry.getName() + " fom Zip file " + zipFile.getName(), ioException);
        }
    }
    
    /**
     * Get the first lib jar file containing an entry named File named compositeFileName
     * 
     * @param compositeFileName the name of the searched composite
     * @return a lib jar file containing an entry named compositeFileName, null if not found
     */
    private File getLibForComposite(String compositeFileName)
    {
        logger.fine("getLibForComposite "+compositeFileName);
        for (File libFile : ZipUtil.listFile(tmpLibDirectory))
        {
            if(ZipUtil.containsEntry(libFile, compositeFileName))
            {
                return libFile;
            }
        }
        return null;
    }
    
    /**
     * Set the name of a SCA root composite contained in a lib file
     * 
     * @param compositeFileName the name of the composite file to modify
     * @param newCompositeName the new name of the SCA composite
     * @return true if the name has been modified, otherwise false see log for details
     */
    public boolean setCompositeName(String compositeFileName,String newCompositeName)
    {
        String fullCompositeFileName=compositeFileName;
        if(!fullCompositeFileName.endsWith(".composite"))
        {
            fullCompositeFileName+=".composite";
        }
        
        File libFile=getLibForComposite(fullCompositeFileName);
        if(libFile==null)
        {
            logger.severe("No provided librairy contains "+fullCompositeFileName+" file");
            return false;
        }

        /** Unzip the lib jar*/
        File unzippedLibJar=ZipUtil.newTempFile();
        ZipUtil.unzipFile(libFile, unzippedLibJar);
        File compositeFile=new File(unzippedLibJar, fullCompositeFileName);
        
        OutputStream compositeOutputStream = null;
        try
        {
            /** Parse composite file */
            SAXBuilder saxBuilder=new SAXBuilder();
            Document compositeDocument=saxBuilder.build(compositeFile);
            
            /** Set the name of the root composite */
            Element compositeElement=compositeDocument.getRootElement();
            compositeElement.setAttribute("name", newCompositeName);
            
            /** Write changes in the compositeFile*/
            compositeOutputStream=new FileOutputStream(compositeFile);
            XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
            sortie.output(compositeDocument, compositeOutputStream);
            compositeOutputStream.close();
        }
        catch (Exception e)
        {
            logger.log(Level.SEVERE,"Error when writing new composite name : "+newCompositeName+" in file "+compositeFile.getName(),e);
            return false;
        }
        finally
        {
            if(compositeOutputStream!=null)
            {
                try
                {
                    compositeOutputStream.close();
                } catch (IOException e)
                {
                   //ignore
                }
            }
        }
        
        try
        {
            /**clear content of the lib file before zipping the new content in*/
            FileUtils.writeStringToFile(libFile, "");
            /**zip the new content*/
            ZipUtil.zip(unzippedLibJar, libFile);
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE,"Error when writing new composite name : "+newCompositeName+" in file "+compositeFile.getName(),e);
            return false;
        }
        
        return true;
    }
    
    public boolean setCompositeFileName(String compositeFileName, String newCompositeFileName)
    {
        String fullCompositeFileName=compositeFileName;
        if(!fullCompositeFileName.endsWith(".composite"))
        {
            fullCompositeFileName+=".composite";
        }
        
        String newFullCompositeFileName=newCompositeFileName;
        if(!newCompositeFileName.endsWith(".composite"))
        {
            newFullCompositeFileName+=".composite";
        }
        
        File libFile=getLibForComposite(fullCompositeFileName);
        if(libFile==null)
        {
            logger.severe("No provided librairy contains "+fullCompositeFileName+" file");
            return false;
        }

        /** Unzip the lib jar*/
        File unzippedLibJar=ZipUtil.newTempFile();
        ZipUtil.unzipFile(libFile, unzippedLibJar);
        File compositeFile=new File(unzippedLibJar, fullCompositeFileName);
        File newCompositeFile=new File(compositeFile.getParentFile(),newFullCompositeFileName);

        try
        {
            FileUtils.copyFile(compositeFile, newCompositeFile);
            FileUtils.forceDelete(compositeFile);
        } catch (IOException e)
        {
            logger.log(Level.SEVERE,"Error when copying content to new composite file");
            return false;
        }
        
        
        if(this.deployables.contains(fullCompositeFileName))
        {
            this.deployables.remove(fullCompositeFileName);
        }
        this.deployables.add(newFullCompositeFileName);
        
        
        try
        {
          /**clear content of the lib file before zipping the new content in*/
           FileUtils.writeStringToFile(libFile, "");
           /**zip the new content*/
           ZipUtil.zip(unzippedLibJar, libFile);
        }
        catch (IOException e)
        {
            logger.log(Level.SEVERE,"Error when zipping new jar file to "+libFile.getPath());
            return false;
        }
        
        return true;
    }
    
}
