/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactNotFoundException;
import org.apache.maven.artifact.resolver.ArtifactResolutionException;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Model;
import org.apache.maven.model.Parent;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;

/**
 *
 */
public class FrascatiMojoHelper
{
    protected final static Logger logger = Logger.getLogger(FrascatiMojoHelper.class.getName());
    
    private final static String MAVEN_PROPERTY_REGEX="\\$\\{(.*)\\}";
    
    private final static String MAVEN_PROJECT_VERSION_PROPERTY="project.version";
    
    /**
     * The Maven project reference.
     * 
     */
    protected MavenProject project;

    /**
     * Location of the local repository.
     * 
     */
    protected ArtifactRepository localRepository;

    /**
     * List of Remote Repositories used by the resolver
     * 
     */
    protected List<?> remoteRepositories;

    /**
     * Used to look up Artifacts.
     * 
     */
    protected ArtifactResolver artifactResolver;

    /**
     * Used to create Artifacts from repositories.
     * 
     */
    protected ArtifactFactory factory;
    
    /**
     * Maven Project target directory
     */
    private File targetDirectory;
    
    /**
     * Current project JAR file
     */
    private File currentProjectJar;

    public FrascatiMojoHelper(MavenProject project, ArtifactRepository localRepository, List<?> remoteRepositories, ArtifactResolver artifactResolver, ArtifactFactory factory)
    {
        this.project = project;
        this.localRepository=localRepository;
        this.remoteRepositories=remoteRepositories;
        this.artifactResolver=artifactResolver;
        this.factory=factory;
    }
    
    public File getTargetDirectory()
    {
        if(this.targetDirectory==null)
        {
            File baseDir = project.getBasedir();
            targetDirectory = new File(baseDir.getAbsolutePath() + File.separator + "target");
        }
        return this.targetDirectory;
    }
    
    /**
     * Get the jar of the current project, because mojo executing in package
     * maven phase the current project jar is retrieved from target folder and
     * not from repository
     * 
     * @param targetDir directory of the current project jar
     * @param jars libs of the contribution
     */
    public File getCurrentProjectJar()
    {
        if(this.currentProjectJar == null)
        {
            StringBuilder projectPackageNameBuilder = new StringBuilder();
            projectPackageNameBuilder.append(this.project.getArtifactId());
            projectPackageNameBuilder.append("-");
            projectPackageNameBuilder.append(this.project.getVersion());
            projectPackageNameBuilder.append(".");
            projectPackageNameBuilder.append(this.project.getPackaging());
            String projectPackageName = projectPackageNameBuilder.toString();
            File projectJarFile = new File(this.getTargetDirectory(), projectPackageName);
            if(projectJarFile.exists())
            {
                this.currentProjectJar = projectJarFile;
            }
        }
        return this.currentProjectJar;
    }
    
    /**
     * Resolve artifact for a dependency
     * 
     * @param project
     * @param dependency
     * @return
     * @throws ArtifactResolutionException
     * @throws ArtifactNotFoundException
     */
    public Artifact getDepencendyArtifact(MavenProject project, Dependency dependency) throws ArtifactResolutionException, ArtifactNotFoundException
    {
        String dependencyVersion;
        if (!this.isMavenProperty(dependency.getVersion()))
        {
            dependencyVersion = dependency.getVersion();
        } else
        {
            dependencyVersion = this.getPropertyValue(project, dependency.getVersion());
        }

        Artifact dependencyArtifact = this.factory.createArtifact(dependency.getGroupId(), dependency.getArtifactId(), dependencyVersion, "", "pom");
        this.artifactResolver.resolve(dependencyArtifact, remoteRepositories, localRepository);
        return dependencyArtifact;
    }
    
    /**
     * Add the jar file of a dependency artifact to the list of files to include
     * into the contribution.
     * 
     * @param artifact referencing the dependency to add
     * @param jars A map with input File as key and target path as value. If
     *        value is null, the same name will be used.
     */
    public File getArtifactFile(Artifact artifact)
    {
        logger.fine("addArtifact " + artifact.toString());

        try
        {
            // we find the jar from the pom artifact
            artifact = this.factory.createArtifact(artifact.getGroupId(), artifact.getArtifactId(), artifact.getVersion(), "", "jar");
            artifactResolver.resolve(artifact, remoteRepositories, localRepository);
            if (!artifact.getScope().equalsIgnoreCase("provided") && !artifact.getScope().equalsIgnoreCase("test"))
            {
                return artifact.getFile();
            }

        } catch (Exception e)
        {
            logger.severe("Dependency " + artifact.getGroupId() + ":" + artifact.getArtifactId() + " cannot be added");
        }
        return null;
    }
    
    /**
     * Read the mavenProject and resolves inherited settings.
     * 
     * @param artifact Maven Artifact to read
     * @return
     * @throws XmlPullParserException
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ArtifactResolutionException
     * @throws ArtifactNotFoundException
     */
    public MavenProject getMavenProject(Artifact artifact) throws FileNotFoundException, IOException, XmlPullParserException
    {
        Model artifactModel = this.readMavenModel(artifact.getFile());
        MavenProject mavenProject = new MavenProject(artifactModel);

        Model currentModel = artifactModel;
        Parent currentParentModel = currentModel.getParent();

        MavenProject tmpMavenProject, currentMavenProject = mavenProject;
        Artifact currentParentModelArtifact;
        while (currentParentModel != null)
        {
            /** Resolve parent artifact */
            currentParentModelArtifact = this.factory.createArtifact(currentParentModel.getGroupId(), currentParentModel.getArtifactId(),
                    currentParentModel.getVersion(), "", "pom");
            try
            {
                artifactResolver.resolve(currentParentModelArtifact, remoteRepositories, localRepository);
                /** Read parent project model */
                currentModel = this.readMavenModel(currentParentModelArtifact.getFile());
                currentParentModel = currentModel.getParent();

                /** Set parent of the current maven project and iterate */
                tmpMavenProject = new MavenProject(currentModel);
                currentMavenProject.setParent(tmpMavenProject);
                currentMavenProject = tmpMavenProject;
            } catch (Exception e)
            {
                logger.fine("Exception when resolving artifact " + currentParentModelArtifact);
                break;
            }
        }

        return mavenProject;
    }

    /**
     * (Found on StackOverflow.com, thanks to Rich Seller) Read the mavenProject
     * without resolving any inherited settings.
     * 
     * @return the MavenProject for the project's POM
     * @throws XmlPullParserException
     * @throws IOException
     * @throws FileNotFoundException
     * @throws ArtifactNotFoundException
     * @throws ArtifactResolutionException
     * @throws MojoExecutionException if the POM can't be parsed.
     */
    private Model readMavenModel(File artifactFile) throws FileNotFoundException, IOException, XmlPullParserException
    {
        MavenXpp3Reader reader = new MavenXpp3Reader();
        Model model = reader.read(new FileReader(artifactFile));
        return model;
    }
    
    /**
     * Define if a String is a maven property
     * 
     * @param s
     * @return
     */
    protected boolean isMavenProperty(String s)
    {
        return s.matches(MAVEN_PROPERTY_REGEX);
    }
    
    
    /**
     * Get value of a maven property fine in a maven project or its parents
     * 
     * @param project the maven Project
     * @param mavenProperty name of the property to find, can be a simple String or ${property.value} form
     * @return the value of the Maven property, null if not found
     */
    protected String getPropertyValue(MavenProject project, String mavenProperty)
    {
        logger.info("getPropertyValue "+mavenProperty+", project : "+project.toString());
        String propertyKey;
        if(isMavenProperty(mavenProperty))
        {
            propertyKey = mavenProperty.replaceAll(MAVEN_PROPERTY_REGEX, "$1");
        }
        else
        {
            propertyKey = mavenProperty;
        }
        logger.info("propertyKey : "+propertyKey);
        
        /**project.version in not in the properties of the MavenProject*/
        if(propertyKey.equals(MAVEN_PROJECT_VERSION_PROPERTY))
        {
            return project.getVersion();
        }
        
        MavenProject currentProject = project;
        while(currentProject != null)
        {
            logger.info("\t"+currentProject+" parent : "+currentProject.getParent()+ " "+currentProject.getParentArtifact());
            for(Object currentPropertyKey : currentProject.getProperties().keySet())
            {
                logger.info("\t\t"+currentPropertyKey);
                if(currentPropertyKey.equals(propertyKey))
                {
                    return currentProject.getProperties().getProperty((String) currentPropertyKey);
                }
            }
            currentProject = currentProject.getParent();
        }
        return null;
    }
}
