/**
 * OW2 FraSCAti Contribution Maven Plugin
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.mojo.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.IOUtils;

/**
 *
 */
public class ZipUtil
{
    private final static int RANDOM_MAX_VALUE=1000000;
    
    /**
     * Utility method to create contribution zip file.
     * 
     * @param inFile The directory to zip.
     * @param outFile The output file name.
     * @throws IOException
     */
    public static void zip(File inFile, File outFile) throws IOException
    {
        FileOutputStream dest = new FileOutputStream(outFile);
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
        
        try
        {
            recursiveZip("", inFile, out);
        } catch (IOException e)
        {
        }
        finally
        {
          out.close();
          dest.close();
        }
    }
    
    private static void recursiveZip(String currentEntryName, File inFile, ZipOutputStream out) throws IOException
    {
        FileInputStream fis;
        ZipEntry entry;
        String entryName;
        
        for(File contentFile :inFile.listFiles())
        {
            entryName=currentEntryName+contentFile.getName();

            if(contentFile.isDirectory())
            {
                entryName+="/";
                entry=new ZipEntry(entryName);
                out.putNextEntry(entry);
                recursiveZip(entryName,contentFile,out);
            }
            else
            {
                entry=new ZipEntry(entryName);
                out.putNextEntry(entry);
                fis=new FileInputStream(contentFile);
                IOUtils.copy(fis, out);
                fis.close();
            }
        }
    }

    public static void unzipFile(File file, File dir)
    {
        /** create ZipFile from provided file*/
        ZipFile zipFile = null;
        try
        {
            zipFile=new ZipFile(file);
        }
        catch (IOException e)
        {
            Logger.getAnonymousLogger().severe("The provided file "+file.getPath()+" is not a Zip archive");
            return;
        }
        
        /**if the target directory doesn't exist create it*/
        if(!dir.exists())
        {
            dir.mkdirs();
        }
        
        File f = null;
        FileOutputStream fos = null;
        for(ZipEntry entry : getEntries(file))
        {
            try
          {
              InputStream eis = zipFile.getInputStream(entry);
              
              f=new File(dir,entry.getName());
              if (entry.isDirectory())
              {
                  f.mkdirs();
                  continue;
              } else
              {
                  f.getParentFile().mkdirs();
                  f.createNewFile();
              }
              
              fos = new FileOutputStream(f);
              IOUtils.copy(eis, fos);
          } catch (IOException e)
          {
              e.printStackTrace();
              continue;
          } finally
          {
              if (fos != null)
              {
                  try
                  {
                      fos.close();
                  } catch (IOException e)
                  {
                      // ignore
                  }
              }
          }
        }
    }

    /**
     * Return the list of entries contained in a zip file
     * Use this method instead of ZipFile.entries() to avoid ISO-8859-1 encoding of entry names
     * Bug fixed in JAVA 1.7
     * 
     * @param zipFile
     * @return
     */
    public static List<ZipEntry> getEntries(File zipFile)
    {
        List<ZipEntry> entries=new ArrayList<ZipEntry>();
        FileInputStream fi = null;
        ZipInputStream zipInputStream=null;
        
        try
        {
            fi = new FileInputStream(zipFile);
            zipInputStream=new ZipInputStream(fi);
            ZipEntry entry=zipInputStream.getNextEntry();
            while(entry!=null)
            {
                entries.add(entry);
                entry=zipInputStream.getNextEntry();
            }
        }
        catch (IOException e)
        {
           e.printStackTrace();
        }
        finally
        {
            if(fi!=null)
            {
                try
                {
                    fi.close();
                } catch (IOException e)
                {
                   //ignore
                }
            }
            
            if(zipInputStream!=null)
            {
                try
                {
                    zipInputStream.close();
                } catch (IOException e)
                {
                    //ignore
                }
            }
        }
        
        return entries;
    }
    
   public static boolean containsEntry(File zipFile, String entryName)
   {
       List<ZipEntry> entries=getEntries(zipFile);
       for(ZipEntry entry : entries)
       {
           if(entry.getName().equals(entryName))
           {
               return true;
           }
       }
       return false;
   }
    
    /**
     * List files recursively. Does not include directories.
     * 
     * @param directory The directory to list.
     * @return A list of paths relative to the directory parameter.
     */
    public static Collection<String> listFiles(File directory)
    {
        return listFiles(directory, "");
    }
    
    /**
     * List files recursively. Does not include directories.
     * 
     * @param directory The directory to list.
     * @param currentDirName The current directory name.
     * @return A list of paths relative to the directory parameter.
     */
    public static Collection<String> listFiles(File directory, String currentDirName)
    {
        // List of files / directories
        Collection<String> files = new ArrayList<String>();
        // Go over entries
        for (File entry : directory.listFiles())
        {
            if (entry.isDirectory()) {
                StringBuilder sb  = new StringBuilder(currentDirName);
                sb.append(entry.getName()).append(File.separator); 
                files.addAll( listFiles(entry, sb.toString()) );
            } else {
                files.add(currentDirName + entry.getName());
            }
        }
        
        // Return collection of files
        return files;
    }
    
    /**
     * List files recursively. Does not include directories.
     * 
     * @param directory The directory to list.
     * @return A list File relative to the directory parameter.
     */
    public static Collection<File> listFile(File directory)
    {
        Collection<File> files = new ArrayList<File>();
        for (File entry : directory.listFiles())
        {
            if (entry.isDirectory())
            {
                files.addAll(listFile(entry));
            } else
            {
                files.add(entry);
            }
        }
        // Return collection of files
        return files;
    }
    
    /**
     * create a new temp file in java temp directory
     * Use default suffix : frascati
     * Use default prefix : contribution
     * 
     * @return the new temp file
     */
    public static File newTempFile()
    {
        return newTempFile("frascati", "contribution");
    }
    
    /**
     * create a new temp file in java temp directory
     * 
     * @param suffix suffix of the temp file
     * @param prefix prefix of the temp file
     * @return the new temp file
     */
    public static File newTempFile(String suffix, String prefix)
    {
        File tempDirectory=new File(System.getProperty("java.io.tmpdir"));
        Random random = new Random();
        String tempFileName;
        int randomInt;
        File tempFile;
        do
        {
            randomInt=random.nextInt(RANDOM_MAX_VALUE);
            tempFileName =suffix+randomInt+prefix;
            tempFile=new File(tempDirectory,tempFileName);
        }
        while(tempFile.exists());

        return tempFile;
    }
}
