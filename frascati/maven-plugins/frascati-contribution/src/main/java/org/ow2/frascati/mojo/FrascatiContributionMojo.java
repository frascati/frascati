/**
 * OW2 FraSCAti Contribution Maven Plugin
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Damien Fournier
 *
 * Contributor(s): Philippe Merle, Rémi Mélisson, Christophe Demarey, Gwenael Cattez
 *
 */

package org.ow2.frascati.mojo;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectHelper;
import org.ow2.frascati.mojo.contribution.Contribution;
import org.ow2.frascati.mojo.helper.FrascatiContributionMojoHelper;

/**
 * A maven plugin for packaging FraSCAti contributions
 * 
 * @param <rep>
 * 
 * @execute phase="package"
 * @requiresDependencyResolution compile
 * @goal install
 * @since 1.1
 */
public class FrascatiContributionMojo<rep> extends AbstractMojo
{
    /**
     * The Maven project reference.
     * 
     * @parameter expression="${project}"
     * @required
     * @since 1.0
     */
    protected MavenProject project;

    /**
     * Helper class to assist in attaching artifacts to the project instance.
     * project-helper instance, used to make addition of resources simpler.
     * 
     * @component
     */
    protected MavenProjectHelper projectHelper;

    /**
     * Location of the local repository.
     * 
     * @parameter expression="${localRepository}"
     * @readonly
     * @required
     */
    protected ArtifactRepository localRepository;

    /**
     * List of Remote Repositories used by the resolver
     * 
     * @parameter expression="${project.remoteArtifactRepositories}"
     * @readonly
     * @required
     */
    protected List<?> remoteRepositories;

    /**
     * Used to look up Artifacts.
     * 
     * @parameter expression=
     *            "${component.org.apache.maven.artifact.resolver.ArtifactResolver}"
     * @required
     * @readonly
     */
    protected ArtifactResolver artifactResolver;

    /**
     * Used to create Artifacts from repositories.
     * 
     * @parameter expression=
     *            "${component.org.apache.maven.artifact.factory.ArtifactFactory}"
     * @required
     * @readonly
     */
    protected ArtifactFactory factory;
    
    /**
     * name of the contribution file
     * 
     * @parameter default-value="${project.artifactId}"
     * 
     */
    protected String name;
    
    /**
     * List of deployables composite
     * 
     * @parameter expression="${parameters}"
     * @since 1.1
     */
    protected String[] deployables;
    
    /**
     * Define if dependencies are added transitively
     * 
     * @parameter
     */
    protected boolean transitive;

    /**
     * List of dependencies to include into the contribution.
     * 
     * @parameter
     */
    protected Dependency[] include;

    /**
     * Groups to exclude to the copied dependencies
     * 
     * @parameter
     */
    protected List<String> excludeGroups;
    
    /**
     * classifier of the zip deployed in local repository
     * 
     * @parameter default-value="frascati-contribution"
     */
    private String classifier;

    /**
     * Import to add to contribution
     * 
     * @parameter
     */
    private List<Import> imports;

    /**
     * Export to add to contribution
     * 
     * @parameter
     */
    private List<Export> exports;

    /**
     * @see org.apache.maven.plugin.Mojo#execute()
     */
    public final void execute() throws MojoExecutionException, MojoFailureException
    {
        try
        {
            FrascatiContributionMojoHelper frascatiContributionMojoHelper = new FrascatiContributionMojoHelper(project, localRepository, remoteRepositories, artifactResolver, factory, excludeGroups);
            List<File> libs = new ArrayList<File>();
            File currentProjectJar = frascatiContributionMojoHelper.getCurrentProjectJar();
            /**If the current Maven project is packaging pom current jar is null*/
            if(currentProjectJar!=null)
            {
                libs.add(currentProjectJar);
            }
            
            frascatiContributionMojoHelper.resolveDependencies(include, transitive,libs);
            Contribution contribution = new Contribution(this.name, Arrays.asList(deployables), imports, exports, libs);
            File contributionFile = contribution.generate(frascatiContributionMojoHelper.getTargetDirectory());
            // install the generated zip in the local repository
            projectHelper.attachArtifact(project, "zip", this.classifier, contributionFile);
        } catch (Exception e)
        {
            getLog().warn("Error when create Contribution Object", e);
        }
    }
}
