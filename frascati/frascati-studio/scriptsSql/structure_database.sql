-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Mar 20 Mars 2012 à 16:23
-- Version du serveur: 5.5.16
-- Version de PHP: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `easysoa`
--

-- --------------------------------------------------------

--
-- Structure de la table `Application`
--

CREATE TABLE IF NOT EXISTS `Application` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `compositeLocation` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `packageName` varchar(255) DEFAULT NULL,
  `resources` varchar(255) DEFAULT NULL,
  `sources` varchar(255) DEFAULT NULL,
  `origin_id` bigint(20) DEFAULT NULL,
  `root` varchar(255) DEFAULT NULL,
  `currentVersion` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `compositeLocation` (`compositeLocation`),
  KEY `FKC00DAD308256154A` (`origin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Structure de la table `AppUser`
--

CREATE TABLE IF NOT EXISTS `AppUser` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `birthday` varchar(255) DEFAULT NULL,
  `civility` int(11) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `workspaceUrl` varchar(255) DEFAULT NULL,
  `town_id` bigint(20) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_unique_index` (`login`),
  KEY `FK33E6796CC6094814` (`town_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structure de la table `AppUser_Application`
--

CREATE TABLE IF NOT EXISTS `AppUser_Application` (
  `AppUser_id` bigint(20) NOT NULL,
  `providedApplications_id` bigint(20) NOT NULL,
  UNIQUE KEY `providedApplications_id` (`providedApplications_id`),
  KEY `FKB339C2DDE10FA033` (`AppUser_id`),
  KEY `FKB339C2DDEFF719EA` (`providedApplications_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `AppUser_AppUser`
--

CREATE TABLE IF NOT EXISTS `AppUser_AppUser` (
  `AppUser_id` bigint(20) NOT NULL,
  `friends_id` bigint(20) NOT NULL,
  UNIQUE KEY `friends_id` (`friends_id`),
  KEY `FK93B26D99E10FA033` (`AppUser_id`),
  KEY `FK93B26D99A0E5E76A` (`friends_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `AppUser_FriendRequest`
--

CREATE TABLE IF NOT EXISTS `AppUser_FriendRequest` (
  `AppUser_id` bigint(20) NOT NULL,
  `friendRequests_id` bigint(20) NOT NULL,
  KEY `FKDE18805EE10FA033` (`AppUser_id`),
  KEY `FKDE18805E40C0B9CF` (`friendRequests_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `Country`
--

CREATE TABLE IF NOT EXISTS `Country` (
  `id` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `DeployedApplication`
--

CREATE TABLE IF NOT EXISTS `DeployedApplication` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `application_id` bigint(20) DEFAULT NULL,
  `deploymentServer_id` bigint(20) DEFAULT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_application_server` (`application_id`,`deploymentServer_id`),
  KEY `FK35C61D8A849CF974` (`deploymentServer_id`),
  KEY `FK35C61D8A417F16E0` (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Structure de la table `DeploymentServer`
--

CREATE TABLE IF NOT EXISTS `DeploymentServer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Structure de la table `FileType`
--

CREATE TABLE IF NOT EXISTS `FileType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


-- --------------------------------------------------------

--
-- Structure de la table `FriendRequest`
--

CREATE TABLE IF NOT EXISTS `FriendRequest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `originUser_id` bigint(20) NOT NULL,
  `targetUser_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK59B21F126E26AC3` (`targetUser_id`),
  KEY `FK59B21F1A6DFAB8E` (`originUser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Preference`
--

CREATE TABLE IF NOT EXISTS `Preference` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Town`
--

CREATE TABLE IF NOT EXISTS `Town` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `country_id` char(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `population` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `country_town_unique` (`country_id`,`name`),
  KEY `name_index` (`name`),
  KEY `longitude_index` (`longitude`),
  KEY `latitude_index` (`latitude`),
  KEY `country_index` (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47736 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `Application`
--
ALTER TABLE `Application`
  ADD CONSTRAINT `FKC00DAD308256154A` FOREIGN KEY (`origin_id`) REFERENCES `Application` (`id`);

--
-- Contraintes pour la table `AppUser`
--
ALTER TABLE `AppUser`
  ADD CONSTRAINT `FK33E6796CC6094814` FOREIGN KEY (`town_id`) REFERENCES `Town` (`id`);

--
-- Contraintes pour la table `AppUser_Application`
--
ALTER TABLE `AppUser_Application`
  ADD CONSTRAINT `FKB339C2DDE10FA033` FOREIGN KEY (`AppUser_id`) REFERENCES `AppUser` (`id`),
  ADD CONSTRAINT `FKB339C2DDEFF719EA` FOREIGN KEY (`providedApplications_id`) REFERENCES `Application` (`id`);

--
-- Contraintes pour la table `AppUser_AppUser`
--
ALTER TABLE `AppUser_AppUser`
  ADD CONSTRAINT `FK93B26D99A0E5E76A` FOREIGN KEY (`friends_id`) REFERENCES `AppUser` (`id`),
  ADD CONSTRAINT `FK93B26D99E10FA033` FOREIGN KEY (`AppUser_id`) REFERENCES `AppUser` (`id`);

--
-- Contraintes pour la table `AppUser_FriendRequest`
--
ALTER TABLE `AppUser_FriendRequest`
  ADD CONSTRAINT `FKDE18805E40C0B9CF` FOREIGN KEY (`friendRequests_id`) REFERENCES `FriendRequest` (`id`),
  ADD CONSTRAINT `FKDE18805EE10FA033` FOREIGN KEY (`AppUser_id`) REFERENCES `AppUser` (`id`);

--
-- Contraintes pour la table `DeployedApplication`
--
ALTER TABLE `DeployedApplication`
  ADD CONSTRAINT `FK35C61D8A417F16E0` FOREIGN KEY (`application_id`) REFERENCES `Application` (`id`),
  ADD CONSTRAINT `FK35C61D8A849CF974` FOREIGN KEY (`deploymentServer_id`) REFERENCES `DeploymentServer` (`id`);

--
-- Contraintes pour la table `FriendRequest`
--
ALTER TABLE `FriendRequest`
  ADD CONSTRAINT `FK59B21F126E26AC3` FOREIGN KEY (`targetUser_id`) REFERENCES `AppUser` (`id`),
  ADD CONSTRAINT `FK59B21F1A6DFAB8E` FOREIGN KEY (`originUser_id`) REFERENCES `AppUser` (`id`);

--
-- Contraintes pour la table `Town`
--
ALTER TABLE `Town`
  ADD CONSTRAINT `FK27DEB24A732C20` FOREIGN KEY (`country_id`) REFERENCES `Country` (`id`);

