package org.easysoa.helloworld.impl;

import org.easysoa.helloworld.api.InterfaceHW;



/** 
 * Implementation class of the component 'server'.
 * (Other components can share this implementation class.)
*/
public class Server implements InterfaceHW {

    private String salutation = "Hello world";
    
    /** 
     * Set the property 'salutation'.
    */
    public void setSalutation(String salutation) {
       this.salutation = salutation;
    }
    
   /** 
    * Implement the operation 'call' of the server port 'provServer'.
   */
	public String call() {
      //we currently support object-based types (Integer, Boolean etc.)
      String result = null;

      result = salutation;
      
      return result;
	}

}
