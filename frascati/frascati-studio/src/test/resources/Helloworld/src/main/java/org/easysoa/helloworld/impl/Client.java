package org.easysoa.helloworld.impl;

import org.easysoa.helloworld.api.InterfaceHW;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/** 
 * Implementation class of the component 'client'.
 * (Other components can share this implementation class.)
*/
@Scope("COMPOSITE")
@EagerInit
public class Client implements  java.lang.Runnable {

    @Reference
    private InterfaceHW reqClient;

    /** 
     * Set the client port 'reqClient'.
     */
    public void setReqClient(InterfaceHW reqClient) {
        this.reqClient = reqClient;
    }

    @Override
    @Init
    public void run() {
        System.out.println(reqClient.call() );
    }

}
