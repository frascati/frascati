/**
* EasySOA 
* 
* Copyright (c) 2011-2012 Inria, University of Lille 1 
* 
* This library is free software; you can redistribute it and/or 
* modify it under the terms of the GNU Lesser General Public 
* License as published by the Free Software Foundation; either 
* version 2 of the License, or (at your option) any later version. 
* 
* This library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
* Lesser General Public License for more details. 
* 
* You should have received a copy of the GNU Lesser General Public 
* License along with this library; if not, write to the Free Software 
* Foundation, Inc., 59 Temple Place, Suite 330,    Boston, MA 02111-1307 
* USA 
* 
* Contact: frascati@ow2.org 
* 
* Author: Antonio de Almeida Souza Neto
* 
* Contributor(s): 
*/

package org.easysoa.deploy;

import static org.junit.Assert.fail;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.easysoa.api.CompilerItf;
import org.easysoa.compiler.SCAJavaCompilerImpl;
import org.easysoa.deploy.DeployProcessor;
import org.easysoa.jpa.AdminAccessImpl;
import org.easysoa.model.Application;
import org.easysoa.model.DeploymentLocation;
import org.easysoa.utils.FileManagerImpl;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.util.FrascatiException;

public class DeployTest {

    /** The FraSCAti platform */
    @SuppressWarnings("unused")
    private static FraSCAti frascati;
    //frascati attribute is accessed remotely in this test

    
    /** REST binding URI */
    // If it uses the default value
    //private static final String BINDING_URI = AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;
    // If it changes the default URI
    private static final String BINDING_ALTERNATIVE_URI = "http://localhost:8095";
    
    private static Deployment remoteDeployment;
    
    private static final String helloWorldPath = "src"+File.separator+"test"+File.separator+"resources"+File.separator+"Helloworld";
    
    private static final String helloworldApplicationName = "helloworld";
    
    private static final String helloworldLibPath = "src"+File.separator+"test"+File.separator+"resources"+File.separator+"lib";
    
    private static final String fakeTestPath = "src"+File.separator+"test"+File.separator+"resources"+File.separator+"FakeTest";
    
    private static final String fakeTestApplicationName = "fakeTest";
    
    private static final String fakeTestLibPath = "src"+File.separator+"test"+File.separator+"resources"+File.separator+"lib";
    
    private static DeployProcessor deployment;
    
    private static Application fakeApplication;
    
    private static Application applicationTest;
    
    private static SCAJavaCompilerImpl studioCompiler;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(AdminAccessImpl.class.getCanonicalName());
    
    /**
     * Instantiate FraSCAti and retrieve services.
     * @throws InterruptedException 
     */
    @BeforeClass
    public static void loadFrascati() throws FrascatiException, InterruptedException {
        
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTRest");
        frascati = FraSCAti.newFraSCAti();
        
        // Get services by using the Rest binding
        remoteDeployment = JAXRSClientFactory.create(BINDING_ALTERNATIVE_URI + "/deploy", Deployment.class);
        
        studioCompiler = new SCAJavaCompilerImpl();
        studioCompiler.setFileManager(new FileManagerImpl());
        
        initObjects();
    }
    
    /**
     * Initialize needed objects
     */
    private static void initObjects(){
        
        fakeApplication = new Application();
        
        fakeApplication.setName(fakeTestApplicationName);
        fakeApplication.setRoot(fakeTestPath);
        fakeApplication.setLocalLibPath(fakeTestLibPath);
        
        applicationTest = new Application();
        
        applicationTest.setName(helloworldApplicationName);
        applicationTest.setRoot(helloWorldPath);
        applicationTest.setLocalLibPath(helloworldLibPath);
        deployment = new DeployProcessor();
        
        deployment.setDeployment(remoteDeployment);
        deployment.setStudioCompiler(studioCompiler);
    }
    
    /**
     * Try to load properties from the properties file and retrieve some attributes
     * 
     */
    @Test
    public void loadDeploymentProperties(){
        Deployment testDeployment = deployment.getDeployment();
        
        //equals doesn't work here
        if (testDeployment.hashCode() != remoteDeployment.hashCode()) {
            fail();
        }
        
        CompilerItf testCompiler = deployment.getScaCompiler();
        
        if (!testCompiler.equals(studioCompiler)) {
            fail();
        }
        
        deployment.loadDeploymentProperties();
        if(deployment.getAvailableDeployments() == null){
            fail();
        }
        List<String> urlList = new ArrayList<String>();
        for (DeploymentLocation deploymentLocation : deployment.getAvailableDeployments()) {
            urlList.add(deploymentLocation.getUrl());
        }
        String urlJSON = deployment.getWebExplorerUrlsInJson(urlList);
        if ("".equals(urlJSON) || urlJSON == null) {
            fail();
        }
    }
    
    
    /**
     * Try to deploy a test application on fraSCAti
     * 
     * This test covers compilation and deployment
     */
    @Test
    public void deploy(){
        
        LOG.info("Testing compilation error (It must fail)...");
        //Test compilation error
        try {
            if( deployment.deploy(new String[]{BINDING_ALTERNATIVE_URI + "/deploy"}, fakeApplication) ){
                fail();
            }
        } catch (Exception e) {
            //The exception is expected in this case
        }
        
        try {
            if(! deployment.deploy(new String[]{BINDING_ALTERNATIVE_URI + "/deploy"}, applicationTest) ){
                fail();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            fail();
        }

    }
    
    /**
     * Try to undeploy the test application previously deployed on fraSCAti
     * 
     * The undeploy method must come after the deploy method
     */
    @Test
    public void undeploy(){
        
        try {
            if (!deployment.undeploy(new String[]{BINDING_ALTERNATIVE_URI + "/deploy"}, applicationTest)) {
                fail();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
            fail();
        }

    }
    

    
}
