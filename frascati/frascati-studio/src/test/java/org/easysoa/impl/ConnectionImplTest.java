/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import org.junit.Test;
//import org.ow2.frascati.examples.test.FraSCAtiTestCase;

/**
 *
 * @author dirix
 */
public class ConnectionImplTest {//extends FraSCAtiTestCase{

    private static final String COMPOSITE_NAME = "easysoa";

    public ConnectionImplTest() {
    }

 
    /**
     * Test of connect method, of class ConnectionImpl.
     */
   /**@Test
    public void testSearchUser() throws FrascatiException {
        User user = getService(Users.class,"test").searchUser(new Long(2));
        Assert.assertNotNull(user);
    }*/

    /**
     * Test of createAccount method, of class ConnectionImpl.
     */
    /**@Test
    public void testCreateAccount() throws FrascatiException{
        try{
        String login = "test";
        String password = "test";
        String confirmPassword = "test"; 
        String mail = "test";
        String name = "test";
        String surname = "test";
        String civility = "Mr";
        String town = "Aniche";
        String country = "fr";
        String birthday="03/14/1987";
        User result = getService(Connection.class,"test2").createAccount(login, password, confirmPassword, mail, name, surname, civility,town,country,birthday);
        Assert.assertNotNull(result);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }*/

    @Test
    public void test(){
        //List<User> result = getService(Friends.class,"test").searchFriends("michel","dirix","aniche");
    }

    //@Override 
    public String getComposite() {
        return COMPOSITE_NAME;
    }

}