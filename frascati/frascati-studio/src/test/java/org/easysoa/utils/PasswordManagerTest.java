/**
* EasySOA 
* 
* Copyright (c) 2011-2012 Inria, University of Lille 1 
* 
* This library is free software; you can redistribute it and/or 
* modify it under the terms of the GNU Lesser General Public 
* License as published by the Free Software Foundation; either 
* version 2 of the License, or (at your option) any later version. 
* 
* This library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
* Lesser General Public License for more details. 
* 
* You should have received a copy of the GNU Lesser General Public 
* License along with this library; if not, write to the Free Software 
* Foundation, Inc., 59 Temple Place, Suite 330,    Boston, MA 02111-1307 
* USA 
* 
* Contact: frascati@ow2.org 
* 
* Author: Antonio de Almeida Souza Neto
* 
* Contributor(s): 
*/

package org.easysoa.utils;

import static org.junit.Assert.fail;

import org.junit.Test;

public class PasswordManagerTest {

    private static final String PASSWORD = "abcd1234";
    
    private static final String ENCRYPTED_PASSWORD = "e19d5cd5af378da5f63f891c7467af";
 
    @Test
    public void testPassword(){
        if ( ! ENCRYPTED_PASSWORD.equals(PasswordManager.cryptPassword(PASSWORD))) {
            fail();
        }
    }
    
}
