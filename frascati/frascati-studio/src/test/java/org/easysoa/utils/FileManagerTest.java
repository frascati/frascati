/**
* EasySOA 
* 
* Copyright (c) 2011-2012 Inria, University of Lille 1 
* 
* This library is free software; you can redistribute it and/or 
* modify it under the terms of the GNU Lesser General Public 
* License as published by the Free Software Foundation; either 
* version 2 of the License, or (at your option) any later version. 
* 
* This library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
* Lesser General Public License for more details. 
* 
* You should have received a copy of the GNU Lesser General Public 
* License along with this library; if not, write to the Free Software 
* Foundation, Inc., 59 Temple Place, Suite 330,    Boston, MA 02111-1307 
* USA 
* 
* Contact: frascati@ow2.org 
* 
* Author: Antonio de Almeida Souza Neto
* 
* Contributor(s): 
*/

package org.easysoa.utils;

import org.easysoa.api.ComponentsFactoryItf;
import org.easysoa.api.FileManager;
import org.easysoa.frascatiModel.ComponentsFactory;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.junit.Test;

public class FileManagerTest {

    private static final String TARGET_COMPOSIT_PATH = "src/test/resources/savetest.composite";
    
    @Test
    public void saveComposite(){
        ComponentsFactoryItf componentsFactory = new ComponentsFactory();

        Composite composite = componentsFactory.createComposite();
        composite.setName("CompositTest");
        
        Component component = componentsFactory.createComponent();
        component.setName("ComponentTest");
        composite.getComponent().add(component);
        
        ComponentReference reference = componentsFactory.createComponentReference();
        reference.setName("ReferenceTest");
        component.getReference().add(reference);
        
        ComponentService service = componentsFactory.createComponentService();
        service.setName("ServiceTest");
        component.getService().add(service);        
        
        FileManager fileManager = new FileManagerImpl();
        fileManager.saveComposite(composite, TARGET_COMPOSIT_PATH);
    }
    

    
}
