/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.catalog;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.easysoa.api.CatalogServicesProcessorItf;
import org.easysoa.deploy.DeployProcessor;
import org.easysoa.model.Publication;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Catalog Services Processor
 * 
 * @see CatalogServicesProcessorItf
 */
@Scope("Composite")
public class CatalogServicesProcessor implements CatalogServicesProcessorItf {

	@Reference
    protected List<CatalogServicesItf> catalog;
    protected Map<String, CatalogServicesItf> catalogMap;
    protected Properties properties;
	
    
    /**
     * Initialize processors
     */
    @Init
    public final void initializeProcessorsByID() {
        this.catalogMap = new HashMap<String, CatalogServicesItf>();
        for (CatalogServicesItf catalogService : this.catalog) {
            this.catalogMap.put(catalogService.getId(), catalogService);
        }
        this.properties = new Properties();
        InputStream inputStream = DeployProcessor.class
                .getResourceAsStream("/properties/social.properties");
        try {
			this.properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * @see CatalogServicesProcessorItf#getPublications(String)
     */
	@Override
	public List<Publication> getPublications(String networkId) {
		return this.catalogMap.get(networkId).getPublications(properties);
	}

	/**
	 * @see CatalogServicesProcessorItf#publish(String, String)
	 */
	@Override
	public void publish(String message, String networkId) {
		this.catalogMap.get(networkId).publish(message, properties);
	}


}
