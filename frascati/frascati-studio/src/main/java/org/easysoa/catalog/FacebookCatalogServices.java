/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.catalog;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

import org.easysoa.model.Publication;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuthService;

/**
 * Catalog for services published on Facebook
 * 
 * @see CatalogServicesItf
 */
public class FacebookCatalogServices implements CatalogServicesItf {

	private final static String SCOPE = "publish_actions,publish_stream,status_update";

	/**
	 * @see CatalogServicesItf#getId()
	 */
	@Override
	public String getId() {
		return "Facebook";
	}

	/**
	 * @see CatalogServicesItf#getPublications(Properties)
	 */
	@Override
	public List<Publication> getPublications(Properties properties) {
		try {
			OAuthService service = new ServiceBuilder()
					.provider(FacebookApi.class)
					.apiKey(properties.getProperty("apiKeyFBK"))
					.apiSecret(properties.getProperty("apiSecretFBK"))
					.scope(SCOPE).build();
			Token accessToken = new Token(
					properties.getProperty("tokenAccessFBK"),
					properties.getProperty("apiSecretFBK"));
			OAuthRequest request = new OAuthRequest(Verb.GET,
					"https://graph.facebook.com/"
							+ properties.getProperty("idPageFBK") + "/feed");
			service.signRequest(accessToken, request);
			Response response = request.send();
			return this.parse(response.getBody());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private ArrayList<Publication> parse(String json) throws JSONException {
		ArrayList<Publication> publications = new ArrayList<Publication>();
		JSONObject jsonObjet = new JSONObject(json);
		if (jsonObjet.has("data")) {
			JSONArray jsonArray = new JSONArray(jsonObjet.getString("data"));
			for (int i = 0; i < jsonArray.length(); ++i) {
				JSONObject j = jsonArray.getJSONObject(i);
				if (j.has("message") && j.getString("message").length() > 10) {
					String date = j.getString("created_time");
					String message = j.getString("message");
					Publication p = getPublication(date, message);
					publications.add(p);
				}
			}
		}
		return publications;
	}

	private Publication getPublication(String date, String message) {
		Scanner in = new Scanner(message);
		String name = null;
		name = in.next();
		ArrayList<String> http = new ArrayList<String>();
		String txt = in.next();
		String description = "";
		boolean getHttp = false;
		while (in.hasNext()) {
			if (txt.startsWith("-")) {
				http.add(txt.substring(1));
				getHttp = true;
			} else if (getHttp == true) {
				description += txt;
				if (in.hasNextLine()) {
					description += in.nextLine();
				}
			}
			if (in.hasNext()) {
				txt = in.next();
			}
		}
		return new Publication(date, name, description, http);
	}

	/**
	 * @see CatalogServicesItf#publish(String, Properties)
	 */
	@Override
	public void publish(String message, Properties properties) {
		try {
			String postfb = URLEncoder.encode(message, "UTF-8");
			OAuthService service = new ServiceBuilder()
					.provider(FacebookApi.class)
					.apiKey(properties.getProperty("apiKeyFBK"))
					.apiSecret(properties.getProperty("apiSecretFBK"))
					.scope(SCOPE).build();
			Token accessToken = new Token(
					properties.getProperty("tokenAccessFBK"),
					properties.getProperty("apiSecretFBK"));
			String urlPost = "https://api.facebook.com/method/stream.publish?format=json&message="
					+ postfb
					+ "&uid=100003869660631"
					+ "&target_id="
					+ properties.getProperty("idPageFBK");
			OAuthRequest request = new OAuthRequest(Verb.GET, urlPost);
			service.signRequest(accessToken, request);
			request.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
