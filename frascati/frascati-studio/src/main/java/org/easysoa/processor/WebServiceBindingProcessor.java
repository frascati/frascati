/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.easysoa.api.BindingProcessorItf;
import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.WebServiceBinding;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class WebServiceBindingProcessor implements ComplexProcessorItf{

    @Reference
    protected BindingProcessorItf bindingProcessor;
    @Reference
    protected HTMLProcessorItf html;

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getWebServiceBinding().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getWebServiceBinding().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Web Service";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        WebServiceBinding binding = (WebServiceBinding) eObject;
        binding.setUri(binding.getWsdlElement());
        JSONObject bindingObject = new JSONObject();
        bindingObject.put("id", "+binding+"+binding.getName());
        bindingObject.put("text", binding.getName());
        bindingObject.put("im0", "WebServiceBinding.gif");
        bindingObject.put("im1", "WebServiceBinding.gif");
        bindingObject.put("im2", "WebServiceBinding.gif");
        return bindingObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        WebServiceBinding binding = null;
        if(eObject != null){
        	binding = (WebServiceBinding)eObject;
        }
        else{
        	binding = (WebServiceBinding)this.getNewEObject(null);
        }
        
        return html.getWebServiceBindingPanel(binding);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getWebServiceBindingMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        WebServiceBinding binding = (WebServiceBinding)eObject;
        binding.setName((String)params.get("name"));
        binding.setUri((String)params.get("uri"));
        binding.setWsdlElement((String)params.get("wsdl-element"));
        List<String> locations = new ArrayList<String>();
        locations.add((String)params.get("wsdl-location"));
        binding.setWsdlLocation(locations);
        return binding;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createWebServiceBinding();
    }

}
