/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.BindingProcessorItf;
import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.domainmodel.tuscany.HTTPBinding;
import org.eclipse.stp.sca.domainmodel.tuscany.TuscanyFactory;
import org.eclipse.stp.sca.domainmodel.tuscany.TuscanyPackage;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;


public class HttpBindingProcessor implements ComplexProcessorItf {

    @Reference
    protected BindingProcessorItf bindingProcessor; 
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
    	return TuscanyPackage.eINSTANCE.getHTTPBinding().getEPackage().getNsURI() + "#"
                + TuscanyPackage.eINSTANCE.getHTTPBinding().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Http";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        HTTPBinding binding = (HTTPBinding) eObject;
        JSONObject bindingObject = new JSONObject();
        bindingObject.put("id", "+binding+"+binding.getName());
        bindingObject.put("text", binding.getName());
        bindingObject.put("im0", "HTTPBinding.gif");
        bindingObject.put("im1", "HTTPBinding.gif");
        bindingObject.put("im2", "HTTPBinding.gif");
        return bindingObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        HTTPBinding binding = null;
        if(eObject != null){
        	binding = (HTTPBinding)eObject;
        }
        else {
        	binding = (HTTPBinding)this.getNewEObject(null);
        }
        return html.getHttpBindingPanel(binding);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getHttpBindingMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        HTTPBinding binding = (HTTPBinding)eObject;
        binding.setName((String)params.get("name"));
        binding.setUri((String)params.get("uri"));
        return binding;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return TuscanyFactory.eINSTANCE.createHTTPBinding();
    }
    
}
