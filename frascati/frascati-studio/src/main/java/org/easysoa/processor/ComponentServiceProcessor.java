/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class ComponentServiceProcessor implements ComplexProcessorItf {

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
    	return ScaPackage.eINSTANCE.getComponentService().getEPackage().getNsURI() + "#"
                + ScaPackage.eINSTANCE.getComponentService().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Component Service";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        ComponentService service =(ComponentService)eObject;
        JSONObject serviceObject = new JSONObject();
        serviceObject.put("id", "+service+"+service.getName());
        serviceObject.put("text", service.getName());
        serviceObject.put("im0", "ComponentService.gif");
        serviceObject.put("im1", "ComponentService.gif");
        serviceObject.put("im2", "ComponentService.gif");
        JSONArray serviceArray = new JSONArray();
        if (service.getInterface() != null) {
            serviceArray.add(this.complexProcessor.getMenuItem(service.getInterface(), (String)serviceObject.get("id")));
        }
        for (Binding binding : service.getBinding()) {
            serviceArray.add(this.complexProcessor.getMenuItem(binding,(String)serviceObject.get("id")));
        }
        serviceObject.put("item", serviceArray);
        return serviceObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        ComponentService service = (ComponentService)eObject;
        return html.getComponentServicePanel(service);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getComponentServiceMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        ComponentService componentService = (ComponentService)eObject;
        componentService.setName((String)params.get("name"));
        if(componentService.getInterface()!=null){
            componentService.setInterface((Interface)this.complexProcessor.saveElement(componentService.getInterface(), params));
        }
        return componentService;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createComponentService();
    }

}
