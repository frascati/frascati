/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Service;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class CompositeProcessor implements ComplexProcessorItf {

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected HTMLProcessorItf html;

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getComposite().getEPackage().getNsURI() + "#"
                + ScaPackage.eINSTANCE.getComposite().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Composite";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        Composite composite = (Composite) eObject;
        JSONObject object = new JSONObject();
        object.put("id", 0);
        JSONArray compositeArray = new JSONArray();
        JSONObject compositeObject = new JSONObject();
        compositeObject.put("id", "composite+");
        compositeObject.put("text", composite.getName());
        compositeObject.put("im0", "Composite.gif");
        compositeObject.put("im1", "Composite.gif");
        compositeObject.put("im2", "Composite.gif");
        compositeArray.add(compositeObject);
        JSONArray array = new JSONArray();
        for (Component component : composite.getComponent()) {
            array.add(this.complexProcessor.getMenuItem(component, ""));
        }

        for (Service service : composite.getService()) {
            array.add(this.complexProcessor.getMenuItem(service, ""));
        }

        for (org.eclipse.stp.sca.Reference reference : composite.getReference()) {
            array.add(this.complexProcessor.getMenuItem(reference, ""));
        }
        compositeObject.put("item", array);
        object.put("item", compositeArray);
        return object;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        Composite composite = (Composite) eObject;
        return html.getCompositePanel(composite);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getCompositeMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        Composite composite = (Composite)eObject;
        composite.setName((String)params.get("name"));
        return composite;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        // TODO Auto-generated method stub
        return null;
    }

}
