/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.ImplementationsProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.metamodel.web.VelocityImplementation;
import org.ow2.frascati.metamodel.web.WebFactory;
import org.ow2.frascati.metamodel.web.WebPackage;

public class VelocityImplementationProcessor implements ComplexProcessorItf {

    @Reference
    protected ImplementationsProcessorItf implementationsProcessor; 
    @Reference 
    protected ServiceManager serviceManager;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
    	return WebPackage.eINSTANCE.getVelocityImplementation().getEPackage().getNsURI() + "#"
                + WebPackage.eINSTANCE.getVelocityImplementation().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Velocity";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        VelocityImplementation velocityImplementation = (VelocityImplementation)eObject;
        JSONObject implemObject = new JSONObject();
        implemObject.put("id", "+implementation");
        implemObject.put("text", velocityImplementation.getLocation()+"/"+velocityImplementation.getDefault());
        implemObject.put("im0", "Implementation.gif");
        implemObject.put("im1", "Implementation.gif");
        implemObject.put("im2", "Implementation.gif");
        return implemObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        VelocityImplementation velocityImplementation = null;
        if(eObject!=null){
        	velocityImplementation = (VelocityImplementation)eObject;
        }
        else{
        	velocityImplementation = (VelocityImplementation)this.getNewEObject(null);
        }
        StringBuffer stringBuffer = new StringBuffer();
        
        if(velocityImplementation.getLocation()!=null && velocityImplementation.getDefault()!=null){
            String url = this.serviceManager.isFileInApplication(userId, velocityImplementation.getLocation()+"/"+velocityImplementation.getDefault());
            this.implementationsProcessor.setUrl(url);
            if(url!=null){
                this.implementationsProcessor.setEditorMode("html");
            }
        }
        else{
            this.implementationsProcessor.setUrl(null);
        }
        return html.getVelocityImplementationPanel(velocityImplementation);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getVelocityImplementationMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        VelocityImplementation implem = (VelocityImplementation)eObject;
        String implementation = (String)params.get("implementation");
        String[] implemParams = implementation.split("/");
        implem.setLocation(implemParams[0]);
        implem.setDefault(implemParams[1]);
        return implem;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        VelocityImplementation implementation = WebFactory.eINSTANCE.createVelocityImplementation();
        return implementation;
    }
}
