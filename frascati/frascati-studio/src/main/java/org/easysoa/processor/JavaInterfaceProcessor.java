/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class JavaInterfaceProcessor implements ComplexProcessorItf {

    @Reference
    protected ServiceManager serviceManager;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor; 
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getJavaInterface().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getJavaInterface().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Java";
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        JavaInterface javaInterface = (JavaInterface) eObject;
        JSONObject interfObject = new JSONObject();
        interfObject.put("id", "+interface");
        interfObject.put("text", javaInterface.getInterface());
        interfObject.put("im0", "JavaInterface.gif");
        interfObject.put("im1", "JavaInterface.gif");
        interfObject.put("im2", "JavaInterface.gif");
        return interfObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        JavaInterface javaInterface = null;
        if(eObject != null){
        	javaInterface = (JavaInterface)eObject;
        }
        else{
        	javaInterface = (JavaInterface)this.getNewEObject(null);
        }
        
        if(javaInterface.getInterface()!=null){
            String url = this.serviceManager.isFileInApplication(userId, javaInterface.getInterface());
            this.interfaceProcessor.setUrl(url);
            this.interfaceProcessor.setEditorMode("java");
        }
        else{
            this.interfaceProcessor.setUrl(null);
        }
        
        
        
        return html.getJavaInterfacePanel(javaInterface);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getJavaInterfaceMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        JavaInterface javaInterface = (JavaInterface)eObject;
        if(params.get("interface")!=null){
            javaInterface.setInterface((String)params.get("interface"));
        }
        return javaInterface;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createJavaInterface();
    }
    

}
