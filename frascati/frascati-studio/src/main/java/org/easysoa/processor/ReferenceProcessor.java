/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class ReferenceProcessor implements ComplexProcessorItf {

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor;
    @Reference
    protected ServiceManager serviceManager;
    @Reference
    protected HTMLProcessorItf html;

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getReference().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getReference().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Reference";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        org.eclipse.stp.sca.Reference reference  = (org.eclipse.stp.sca.Reference)eObject;
        JSONObject referenceObject = new JSONObject();
        referenceObject.put("id", "reference+"+reference.getName());
        referenceObject.put("text", reference.getName());
        referenceObject.put("im0", "Reference.gif");
        referenceObject.put("im1", "Reference.gif");
        referenceObject.put("im2", "Reference.gif");
        JSONArray objectArray = new JSONArray();
        if (reference.getInterface() != null) {
            objectArray.add(this.complexProcessor.getMenuItem(reference.getInterface(), (String)referenceObject.get("id")));
        }

        for (Binding binding : reference.getBinding()) {
            objectArray.add(this.complexProcessor.getMenuItem(binding,(String)referenceObject.get("id")));
        }
        referenceObject.put("item", objectArray);
        return referenceObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        org.eclipse.stp.sca.Reference reference  = (org.eclipse.stp.sca.Reference)eObject;
        return html.getReferencePanel(reference, userId);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getReferenceMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        org.eclipse.stp.sca.Reference reference = (org.eclipse.stp.sca.Reference)eObject;
        reference.setName((String)params.get("name"));
        if(params.get("promote")!=null && !((String)params.get("promote")).equals("")){
            reference.setPromote((String)params.get("promote"));
        }

        if(params.get("target")!=null && !((String)params.get("target")).equals("")){
            reference.setTarget((String)params.get("target"));
        }
        if (((String)params.get("target")).equals("")) {
            reference.setTarget(null);
        }
        if(reference.getInterface()!=null){
            reference.setInterface((Interface)this.complexProcessor.saveElement(reference.getInterface(), params));
        }
        return reference;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createReference();
    }
}
