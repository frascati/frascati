/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class ComponentReferenceProcessor implements ComplexProcessorItf {

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
    	return ScaPackage.eINSTANCE.getComponentReference().getEPackage().getNsURI() + "#"
                + ScaPackage.eINSTANCE.getComponentReference().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Component Reference";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        ComponentReference componentReference = (ComponentReference)eObject;
        JSONObject referenceObject = new JSONObject();
        referenceObject.put("id", "+reference+"+componentReference.getName());
        referenceObject.put("text", componentReference.getName());
        referenceObject.put("im0", "ComponentReference.gif");
        referenceObject.put("im1", "ComponentReference.gif");
        referenceObject.put("im2", "ComponentReference.gif");
        JSONArray referenceArray = new JSONArray();
        if (componentReference.getInterface() != null) {
            referenceArray.add(this.complexProcessor.getMenuItem(componentReference.getInterface(), (String)referenceObject.get("id")));
        }
        for (Binding binding : componentReference.getBinding()) {
            referenceArray.add(this.complexProcessor.getMenuItem(binding, (String)referenceObject.get("id")));
        }

        referenceObject.put("item", referenceArray);
        return referenceObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        ComponentReference reference = (ComponentReference)eObject;
        return html.getComponentReferencePanel(reference);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        StringBuffer stringBuffer = new StringBuffer();
        
        stringBuffer.append("<div id=\"add\" text=\"Add\">");
            stringBuffer.append("<div id=\"addBinding\" img=\"Binding.gif\" text=\"Add Binding\"></div>");
            stringBuffer.append("<div id=\"addInterface\" img=\"Interface.gif\" class=\"opener\" text=\"Add interface\"></div>");
        stringBuffer.append("</div>");
        stringBuffer.append("<div id=\"delete\" text=\"Delete\">");
            stringBuffer.append("<div id=\"deleteComponentReference\" img=\"ComponentReference.gif\" text=\"Delete reference\"></div>");
        stringBuffer.append("</div>");
    stringBuffer.append("<div id=\"save\" img=\"filesave.png\" text=\"Save\"><hotkey>Ctrl+S</hotkey><hotkey>Cmd+S</hotkey></div>");
        return stringBuffer.toString();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        ComponentReference componentReference = (ComponentReference)eObject;
        componentReference.setName((String)params.get("name"));
        if(params.get("target")!=null && !((String)params.get("target")).equals("")){
            componentReference.setTarget((String)params.get("target"));
        }
        if (((String)params.get("target")).equals("")) {
            componentReference.setTarget(null);
        }
        if(componentReference.getInterface()!=null){
            componentReference.setInterface((Interface)this.complexProcessor.saveElement(componentReference.getInterface(), params));
        }
        return componentReference;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createComponentReference();
    }

}
