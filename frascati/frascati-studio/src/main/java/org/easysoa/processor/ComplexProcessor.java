/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("Composite")
public class ComplexProcessor implements ComplexProcessorItf {

    @Reference
    protected List<ComplexProcessorItf> processors;
    protected Map<String, ComplexProcessorItf> processorMap;

    @Init
    public final void initializeProcessorsByID() {
        this.processorMap = new HashMap<String, ComplexProcessorItf>();
        for (ComplexProcessorItf complexProcessor : this.processors) {
            this.processorMap.put(complexProcessor.getId(), complexProcessor);
        }
    }

    protected ComplexProcessorItf getProcessorById(EObject eObject) {
        if (eObject == null) { // #FRASCATI-89 3. patch : guard against empty binding (without name ; WS or other)
            return this.processorMap.get("error");
        }
        return this.processorMap.get(eObject.eClass().getEPackage().getNsURI() + "#" + eObject.eClass().getName());
    }
    
    public String getLabel(EObject eObject) {
        return getProcessorById(eObject).getLabel(eObject);       
    }
    
    
    public JSONObject getMenuItem(EObject eObject,String parentId) {
        ComplexProcessorItf processor = getProcessorById(eObject); 
        return processor.getMenuItem(eObject,parentId);
    }
    
    public String getPanel(String userId, EObject eObject) {
        return getProcessorById(eObject).getPanel(userId, eObject);
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return getProcessorById(eObject).getActionMenu(eObject);
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        return getProcessorById(eObject).saveElement(eObject, params);
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return getProcessorById(eObject).getNewEObject(eObject);
    }

}

