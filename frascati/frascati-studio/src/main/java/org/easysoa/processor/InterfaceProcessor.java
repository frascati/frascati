/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.processor;

import java.util.ArrayList;
import java.util.List;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.FileManager;
import org.easysoa.api.InterfaceProcessorItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.Service;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class InterfaceProcessor implements InterfaceProcessorItf {

    @Reference
    protected FileManager fileManager;
    @Reference
    protected ServiceManager serviceManager;
    @Reference
    protected PreferencesManagerItf preferences;
    @Reference
    private List<ComplexProcessorItf> processors;
    
    private String url;
    private String editorMode;

    @Override
    public List<String> allAvailableInterfacesLabel() {
        List<String> labels = new ArrayList<String>();
        for (ComplexProcessorItf processor : this.processors) {
            labels.add(processor.getLabel(null));
        }
        return labels;
    }

    @Override
    public String getInterfaceView(String userId, Composite composite, String modelId,
            String elementId) {
        for (ComplexProcessorItf processor : this.processors) {
            String label = processor.getLabel(null);
            if (label.equals(elementId)) {
                this.modifyInterface(userId, composite, modelId,
                        processor.getNewEObject(null));
                return processor.getPanel(userId, null);
            }
        }
        return null;
    }

    private void modifyInterface(String userId, Composite composite, String elementId, EObject eObject) {
        String[] ids = elementId.split(" ");
        if (ids[0].equals("component")) {
            EList<Component> components = composite.getComponent();
            for (Component component : components) {
                if (component.getName().equals(ids[1])) {
                    if (ids[2].equals("service")) {
                        for (ComponentService componentService : component
                                .getService()) {
                            if (componentService.getName().equals(ids[3])) {
                                componentService
                                        .setInterface((Interface) eObject);
                            }
                        }
                    }
                    else if (ids[2].equals("reference")) {
                        for (ComponentReference componentReference : component
                                .getReference()) {
                            if (componentReference.getName().equals(ids[3])) {
                                componentReference
                                        .setInterface((Interface) eObject);
                            }
                        }
                    }
                }
            }
        } else if (ids[0].equals("service")) {
            for (Service service : composite.getService()) {
                if (service.getName().equals(ids[1]) && ids.length == 2) {
                    service.setInterface((Interface) eObject);
                }
            }
        } else if (ids[0].equals("reference")) {
            for (org.eclipse.stp.sca.Reference reference : composite
                    .getReference()) {
                if (reference.getName().equals(ids[1]) && ids.length == 2) {
                    reference.setInterface((Interface)eObject);
                }
            }
        }
        Application currentApplication = serviceManager.getCurrentApplication(userId);
        currentApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
        
        fileManager.saveComposite(composite, currentApplication.retrieveAbsoluteCompositeLocation());
    }

    @Override
    public Interface createInterface(String interfaceType) {
        for (ComplexProcessorItf processor : this.processors) {
            String label = processor.getLabel(null);
            if (label.equals(interfaceType)) {
                return (Interface)processor.getNewEObject(null);
            }
        }
        return null;
    }
    
    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Override
    public String getEditorMode() {
        return editorMode;
    }
    
    @Override
    public void setEditorMode(String editorMode) {
        this.editorMode = editorMode;
    }
}
