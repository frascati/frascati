/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.BindingProcessorItf;
import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiFactory;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.RestBinding;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class RestBindingProcessor implements ComplexProcessorItf {

    @Reference
    protected BindingProcessorItf bindingProcessor;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
    	return FrascatiPackage.eINSTANCE.getRestBinding().getEPackage().getNsURI() + "#"
                + FrascatiPackage.eINSTANCE.getRestBinding().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Rest";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        RestBinding binding = (RestBinding) eObject;
        JSONObject bindingObject = new JSONObject();
        bindingObject.put("id", "+binding+"+binding.getName());
        bindingObject.put("text", binding.getName());
        bindingObject.put("im0", "RestBinding.gif");
        bindingObject.put("im1", "RestBinding.gif");
        bindingObject.put("im2", "RestBinding.gif");
        return bindingObject;
    }

    public String getPanel(String userId, EObject eObject) {
        RestBinding binding = null;
        if(eObject != null){
        	binding = (RestBinding)eObject;
        }
        else{
        	binding = (RestBinding)this.getNewEObject(null);
        }
        return html.getRestBindingPanel(binding);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<div id=\"delete\" text=\"Delete\">");
        stringBuffer.append("<div id=\"deleteBinding\" img=\"Binding.gif\" text=\"Delete binding\"></div>");
        stringBuffer.append("</div>");
        stringBuffer.append("<div id=\"save\" img=\"filesave.png\" text=\"Save\"><hotkey>Ctrl+S</hotkey><hotkey>Cmd+S</hotkey></div>");
        return stringBuffer.toString();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        RestBinding binding = (RestBinding)eObject;
        binding.setUri((String)params.get("uri"));
        binding.setName((String)params.get("name"));
        return binding;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return FrascatiFactory.eINSTANCE.createRestBinding();
    }

}
