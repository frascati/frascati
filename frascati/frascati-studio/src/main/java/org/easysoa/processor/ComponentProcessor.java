/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.ImplementationsProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class ComponentProcessor implements ComplexProcessorItf {

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected ImplementationsProcessorItf implementationProcessor;
    @Reference
    protected HTMLProcessorItf html;
    

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getComponent().getEPackage().getNsURI() + "#"
                + ScaPackage.eINSTANCE.getComponent().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Component";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        Component component = (Component) eObject;
        JSONObject componentJSONObject = new JSONObject();
        componentJSONObject.put("id", "component+" + component.getName());
        componentJSONObject.put("text", component.getName());
        componentJSONObject.put("im0", "Component.gif");
        componentJSONObject.put("im1", "Component.gif");
        componentJSONObject.put("im2", "Component.gif");
        JSONArray componentArray = new JSONArray();
        if(component.getImplementation() != null){
            componentArray.add(this.complexProcessor.getMenuItem(component.getImplementation(), (String)componentJSONObject.get("id")));
        }
        for (ComponentService service : component.getService()) {
            componentArray.add(this.complexProcessor.getMenuItem(service,
                    (String) componentJSONObject.get("id")));
        }

        for (PropertyValue property : component.getProperty()) {
            componentArray.add(this.complexProcessor.getMenuItem(property,
                    (String) componentJSONObject.get("id")));
        }

        for (ComponentReference componentReference : component.getReference()) {
            componentArray.add(this.complexProcessor.getMenuItem(componentReference,
                    (String) componentJSONObject.get("id")));
        }
        componentJSONObject.put("item", componentArray);
        return componentJSONObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        Component component = (Component) eObject;
        return html.getComponentPanel(component);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getComponentMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        Component component = (Component)eObject;
        component.setName((String)params.get("name"));
        return component;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createComponent();
    }
    
    

}
