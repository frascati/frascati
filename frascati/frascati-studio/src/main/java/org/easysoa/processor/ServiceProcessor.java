/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Service;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class ServiceProcessor implements ComplexProcessorItf{

    @Reference
    protected ComplexProcessorItf complexProcessor;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor;
    @Reference
    protected HTMLProcessorItf html;

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getService().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getService().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "Service";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        Service service = (Service)eObject;
        JSONObject serviceJSONObject = new JSONObject();
        serviceJSONObject.put("id", "service+"+service.getName());
        serviceJSONObject.put("text", service.getName());
        serviceJSONObject.put("im0", "Service.gif");
        serviceJSONObject.put("im1", "Service.gif");
        serviceJSONObject.put("im2", "Service.gif");
        JSONArray itemsArray = new JSONArray();
        if (service.getInterface() != null) {
            itemsArray.add(this.complexProcessor.getMenuItem(service.getInterface(), (String)serviceJSONObject.get("id")));
        }
        for (Binding binding : service.getBinding()) {
            JSONObject object = this.complexProcessor.getMenuItem(binding, (String)serviceJSONObject.get("id"));
            itemsArray.add(object);
        }
        serviceJSONObject.put("item", itemsArray);
        return serviceJSONObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        Service service  = (Service)eObject;
        return html.getServicePanel(service);
    }

    @Override
    public String getActionMenu(EObject eObject) {
    	Service service  = (Service)eObject;
        return html.getServiceMenu(service);
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        Service service = (Service)eObject;
        service.setName((String)params.get("name"));
        if(params.get("promote")!=null && !((String)params.get("promote")).equals("")){
            service.setPromote((String)params.get("promote"));
        }
        if(service.getInterface()!=null){
            service.setInterface((Interface)this.complexProcessor.saveElement(service.getInterface(), params));
        }
        return service;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createService();
    }
}
