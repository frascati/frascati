/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.processor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.easysoa.api.BindingProcessorItf;
import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.FileManager;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Service;
import org.osoa.sca.annotations.Reference;

public class BindingProcessor implements BindingProcessorItf {

    @Reference
    protected FileManager fileManager;
    @Reference
    protected ServiceManager serviceManager;
    @Reference
    protected PreferencesManagerItf preferences;
    @Reference
    private List<ComplexProcessorItf> processors;

    @Override
    public List<String> allAvailableBindingsLabel() {
        List<String> labels = new ArrayList<String>();
        for (ComplexProcessorItf processor : this.processors) {
            labels.add(processor.getLabel(null));
        }
        return labels;
    }

    @Override
    public String getBindingView(String userId, Composite composite, String modelId, String elementId) {
        for (ComplexProcessorItf processor : this.processors) {
            String label = processor.getLabel(null);
            if (label.equals(elementId)) {
                Binding binding = (Binding)processor.getNewEObject(null);
                String[] ids = modelId.split(" ");
                binding.setName(ids[ids.length-1]);
                this.modifyBinding(userId, composite, modelId, binding);
                return processor.getPanel(userId, null);
            }
        }
        return null;
    }

    private void modifyBinding(String userId, Composite composite, String elementId, EObject eObject) {
        String[] ids = elementId.split(" ");
        if (ids[0].equals("component")) {
            EList<Component> components = composite.getComponent();
            for (Component component : components) {
                if (component.getName().equals(ids[1])) {
                    if (ids[2].equals("service")) {
                        for (ComponentService componentService : component
                                .getService()) {
                            if (componentService.getName().equals(ids[3]) && ids.length == 6 && ids[4].equals("binding")) {
                                Iterator<Binding> iterator = componentService.getBinding().iterator();
                                while (iterator.hasNext()) {
                                    Binding binding = iterator.next();
                                    if (binding.getName().equals(((Binding) eObject).getName())) {
                                        iterator.remove();
                                        componentService.getBinding().add((Binding) eObject);
                                        break;
                                    }
                                    
                                }
                            }
                        }
                    } else if (ids[2].equals("reference")) {
                        for (ComponentReference componentReference : component
                                .getReference()) {
                            if (componentReference.getName().equals(ids[3]) && ids.length == 6 && ids[4].equals("binding")) {
                                Iterator<Binding> iterator = componentReference.getBinding().iterator();
                                while (iterator.hasNext()) {
                                    Binding binding = iterator.next();
                                    if (binding.getName().equals(((Binding) eObject).getName())) {
                                        iterator.remove();
                                        componentReference.getBinding().add((Binding) eObject);
                                        break;
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
        } else if (ids[0].equals("service")) {
            for (Service service : composite.getService()) {
             // service name
                if (service.getName().equals(ids[1]) && ids.length == 4 && ids[2].equals("binding")) {
                    Iterator<Binding> iterator = service.getBinding().iterator();
                    while (iterator.hasNext()) {
                        Binding binding = iterator.next();
                        if (binding.getName().equals(((Binding) eObject).getName())) {
                            iterator.remove();
                            service.getBinding().add((Binding) eObject);
                            break;
                        }
                        
                    }
                }
            }
        } else if (ids[0].equals("reference")) {
            for (org.eclipse.stp.sca.Reference reference : composite
                    .getReference()) {
                if (reference.getName().equals(ids[1]) && ids.length == 4 && ids[2].equals("binding")) {
                    Iterator<Binding> iterator = reference.getBinding().iterator();
                    while (iterator.hasNext()) {
                        Binding binding = iterator.next();
                        if (binding.getName().equals(((Binding) eObject).getName())) {
                            iterator.remove();
                            reference.getBinding().add((Binding) eObject);
                            break;
                        }
                        
                    }
                }
            }
        }
        Application currentApplication = serviceManager.getCurrentApplication(userId);
        currentApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
        
        fileManager.saveComposite(composite, currentApplication.retrieveAbsoluteCompositeLocation());
    }
    
    @Override
    public Binding createBinding(String bindingType){
    	bindingType = bindingType.replace(" ","");
        for (ComplexProcessorItf processor : this.processors) {
            String label = processor.getLabel(null);
            label = label.replace(" ","");
            if(label.equals(bindingType)){
                return (Binding)processor.getNewEObject(null);
            }
        }
        return null;
    }

}
