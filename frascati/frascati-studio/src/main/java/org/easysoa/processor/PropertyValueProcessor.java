/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.processor;

import java.util.Map;

import javax.xml.namespace.QName;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;


public class PropertyValueProcessor implements ComplexProcessorItf{

    static final private String PROPERTYLABEL = "Property";
    
	@Reference
    protected HTMLProcessorItf html;
	
	/**
	 * Get property Id
	 * 
	 * @return String
	 */
    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getPropertyValue().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getPropertyValue().getName();
    }

    /**
     * Get Label "Property"
     * 
     * @param eObject
     * @return String
     */
    @Override
    public String getLabel(EObject eObject) {
        return PROPERTYLABEL;
    }


    /**
     * Get JSON object to build the menu
     * 
     * @param eObject
     * @param parentId
     * @return JSONObject
     */
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        PropertyValue property = (PropertyValue)eObject;
        JSONObject propertyObject = new JSONObject();
        propertyObject.put("id", "+property+"+property.getName());
        propertyObject.put("text", property.getName());
        propertyObject.put("im0", "Property.gif");
        propertyObject.put("im1", "Property.gif");
        propertyObject.put("im2", "Property.gif");
        return propertyObject;
    }

    /**
     * Get panel
     * 
     * @param userId
     * @param eObject
     * @return String
     */
    @Override
    public String getPanel(String userId, EObject eObject) {
        PropertyValue property = (PropertyValue)eObject;
        return html.getPropertyPanel(property);
    }

    /**
     * Get action Menu
     * 
     * @param eObject
     * @return String
     */
    @Override
    public String getActionMenu(EObject eObject) {
        return html.getPropertyMenu();
    }

    /**
     * Save property modifications in the composite model.<br><br>
     * 
     * Some verifications are made in this method.<br>
     * - If the property type is <i>String</i> it's set <i>null</i>. (The type String don't need to be declared)<br>
     * - If the property type is not <i>String</i>, "xsd:" is added to the field <i>type</i>. FraSCAti need that to recognize the type
     * 
     * @param eObject - Property to be modified
     * @param params - Property parameters
     * @return EObject - modified property 
     */
    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        PropertyValue property = (PropertyValue)eObject;
        property.setName((String)params.get("name"));
        
        
        if(params.get("type")!=null ){
            if ("String".equals(params.get("type")) || ((String)params.get("type")).isEmpty()) {
                params.remove("type");
                property.setType(null);
            }else{
                String type = (String)params.get("type");
                //Verify if it is conform to types defined in http://www.w3.org/TR/xmlschema-2/
                if (!type.contains("xsd:")) {
                    type = "xsd:"+ type;
                }
                property.setType(new QName(type));
            }

        }
        if(params.get("value")!=null ){
            property.setValue((String)params.get("value"));
        }
        return property;
    }

    /**
     * Get a new Property
     * 
     * @param eObject
     * @return EObject - The new property
     */
    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createPropertyValue();
    }

}
