/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.InterfaceProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.WSDLPortType;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

public class WsdlInterfaceProcessor implements ComplexProcessorItf {

	@Reference
    protected ServiceManager serviceManager;
    @Reference
    protected InterfaceProcessorItf interfaceProcessor; 
    @Reference
    protected HTMLProcessorItf html;

    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getWSDLPortType().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getWSDLPortType().getName();
    }

    @Override
    public String getLabel(EObject eObject) {
        return "WSDL";
    }

    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        WSDLPortType wsdlInterface = (WSDLPortType) eObject;
        JSONObject interfObject = new JSONObject();
        interfObject.put("id", "+interface");
        interfObject.put("text", wsdlInterface.getInterface());
        interfObject.put("im0", "Interface.gif");
        interfObject.put("im1", "Interface.gif");
        interfObject.put("im2", "Interface.gif");
        return interfObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        WSDLPortType wsdlInterface = null;
        if(eObject != null){
        	wsdlInterface = (WSDLPortType)eObject;
        }
        else{
        	wsdlInterface = (WSDLPortType)this.getNewEObject(null);
        }
        return html.getWsdlInterfacePanel(wsdlInterface);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getWdslInterfaceMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        WSDLPortType wsdlInterface = (WSDLPortType)eObject;
        if(params.get("interface")!=null){
            wsdlInterface.setInterface((String)params.get("interface"));
        }
        return wsdlInterface;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createWSDLPortType();
    }


}
