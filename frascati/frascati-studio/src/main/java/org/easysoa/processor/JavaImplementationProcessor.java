/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.ImplementationsProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;


public class JavaImplementationProcessor implements ComplexProcessorItf {

    @Reference
    protected ImplementationsProcessorItf implementationsProcessor; 
    @Reference 
    protected ServiceManager serviceManager;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getJavaImplementation().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getJavaImplementation().getName();
    }
    
    @Override
    public String getLabel(EObject eObject) {
        return "Java";
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        JavaImplementation javaImplementation = (JavaImplementation)eObject;
        JSONObject implemObject = new JSONObject();
        implemObject.put("id", "+implementation");
        implemObject.put("text", javaImplementation.getClass_());
        implemObject.put("im0", "JavaImplementation.gif");
        implemObject.put("im1", "JavaImplementation.gif");
        implemObject.put("im2", "JavaImplementation.gif");
        return implemObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        JavaImplementation javaImplementation = null;
        if(eObject != null){
        	javaImplementation = (JavaImplementation)eObject;
        }
        else{
        	javaImplementation = (JavaImplementation)this.getNewEObject(null);
        }
                
        if(javaImplementation.getClass_()!=null){
            String url = this.serviceManager.isFileInApplication(userId, javaImplementation.getClass_());
            this.implementationsProcessor.setUrl(url);
            if(url!=null){
                this.implementationsProcessor.setEditorMode("java");
            }
        }
        else{
            this.implementationsProcessor.setUrl(null);
        }
        return html.getJavaImplementationPanel(javaImplementation);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getJavaImplementationMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        JavaImplementation implem = (JavaImplementation)eObject;
        implem.setClass((String)params.get("implementation"));
        return implem;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createJavaImplementation();
    }
    
}
