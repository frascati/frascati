/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.easysoa.processor;

import java.util.Map;
import javax.xml.namespace.QName;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.HTMLProcessorItf;
import org.easysoa.api.ImplementationsProcessorItf;
import org.easysoa.api.ServiceManager;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;


public class CompositeImplementationProcessor implements ComplexProcessorItf {

    @Reference
    protected ImplementationsProcessorItf implementationsProcessor; 
    @Reference 
    protected ServiceManager serviceManager;
    @Reference
    protected HTMLProcessorItf html;
    
    @Override
    public String getId() {
        return ScaPackage.eINSTANCE.getSCAImplementation().getEPackage().getNsURI() + "#" + ScaPackage.eINSTANCE.getSCAImplementation().getName();
    }
    
    @Override
    public String getLabel(EObject eObject) {
        return "Composite";
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public JSONObject getMenuItem(EObject eObject, String parentId) {
        SCAImplementation scaImplementation = (SCAImplementation)eObject;
        JSONObject implemObject = new JSONObject();
        implemObject.put("id", "+implementation");
        implemObject.put("text", scaImplementation.getName().toString());
        implemObject.put("im0", "Composite.gif");
        implemObject.put("im1", "Composite.gif");
        implemObject.put("im2", "Composite.gif");
        return implemObject;
    }

    @Override
    public String getPanel(String userId, EObject eObject) {
        SCAImplementation scaImplementation = null;
        if(eObject != null){
        	scaImplementation = (SCAImplementation)eObject;
        }
        else{
        	scaImplementation = (SCAImplementation)this.getNewEObject(null);
        }
                
        if(scaImplementation.getName()!=null){
            String url = this.serviceManager.isFileInApplication(userId, scaImplementation.getName().toString());
            this.implementationsProcessor.setUrl(url);
            if(url!=null){
                this.implementationsProcessor.setEditorMode("xml");
            }
        }
        else{
            this.implementationsProcessor.setUrl(null);
        }
        return html.getCompositeImplementationPanel(scaImplementation);
    }

    @Override
    public String getActionMenu(EObject eObject) {
        return html.getCompositeImplementationMenu();
    }

    @Override
    public EObject saveElement(EObject eObject, Map<String, Object> params) {
        SCAImplementation implem = (SCAImplementation)eObject;
        implem.setName(new QName((String)params.get("implementation")));
        return implem;
    }

    @Override
    public EObject getNewEObject(EObject eObject) {
        return ScaFactory.eINSTANCE.createSCAImplementation();
    }
    
}
