/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.frascatiModel;

import org.easysoa.api.ComponentsFactoryItf;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCABinding;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.WebServiceBinding;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiFactory;
import org.eclipse.stp.sca.domainmodel.frascati.RestBinding;
import org.ow2.frascati.metamodel.web.HttpBinding;
import org.ow2.frascati.metamodel.web.VelocityImplementation;
import org.ow2.frascati.metamodel.web.WebFactory;

/**
 * Factory for EMF model elements 
 *
 */
public class ComponentsFactory implements ComponentsFactoryItf {

    /**
     * @see ComponentsFactoryItf#createComposite()
     */
    @Override
    public Composite createComposite() {
        return ScaFactory.eINSTANCE.createComposite();
    }
    
    /**
     * @see ComponentsFactoryItf#createComponent()
     */
    @Override
    public Component createComponent() {
        return ScaFactory.eINSTANCE.createComponent();
    }

    /**
     * @see ComponentsFactoryItf#createComponentService()
     */
    @Override
    public ComponentService createComponentService() {
        return ScaFactory.eINSTANCE.createComponentService();
    }

    /**
     * @see ComponentsFactoryItf#createComponentReference()
     */
    @Override
    public ComponentReference createComponentReference() {
        return ScaFactory.eINSTANCE.createComponentReference();
    }

    /**
     * @see ComponentsFactoryItf#createPropertyValue()
     */
    @Override
    public PropertyValue createPropertyValue() {
        return ScaFactory.eINSTANCE.createPropertyValue();
    }

    /**
     * @see ComponentsFactoryItf#createService()
     */
    @Override
    public Service createService() {
        return ScaFactory.eINSTANCE.createService();
    }

    /**
     * @see ComponentsFactoryItf#createReference() 
     */
    @Override
    public Reference createReference() {
        return ScaFactory.eINSTANCE.createReference();
    }

    /**
     * @see ComponentsFactoryItf#createHttpBinding()
     */
    @Override
    public HttpBinding createHttpBinding() {
        return WebFactory.eINSTANCE.createHttpBinding();
    }

    /**
     * @see ComponentsFactoryItf#createJavaImplementation()
     */
    @Override
    public JavaImplementation createJavaImplementation() {
        return ScaFactory.eINSTANCE.createJavaImplementation();
    }

    /**
     * @see ComponentsFactoryItf#createVelocityImplementation()
     */
    @Override
    public VelocityImplementation createVelocityImplementation() {
        return WebFactory.eINSTANCE.createVelocityImplementation();
    }

    /**
     * @see ComponentsFactoryItf#createJavaInterface()
     */
    @Override
    public JavaInterface createJavaInterface() {
        return ScaFactory.eINSTANCE.createJavaInterface();
    }

    /**
     * @see ComponentsFactoryItf#createRestBinding()
     */
    @Override
    public RestBinding createRestBinding() {
        return FrascatiFactory.eINSTANCE.createRestBinding();
    }
    
    /**
     * @see ComponentsFactoryItf#createScaBinding()
     */
    @Override
    public SCABinding createScaBinding() {
        return ScaFactory.eINSTANCE.createSCABinding();
    }

    /**
     * @see ComponentsFactoryItf#createWebServiceBinding()
     */
    @Override
    public WebServiceBinding createWebServiceBinding() {
        return ScaFactory.eINSTANCE.createWebServiceBinding();
    }

}
