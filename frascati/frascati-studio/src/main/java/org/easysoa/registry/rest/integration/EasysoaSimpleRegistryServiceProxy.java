/**
 * 
 */
package org.easysoa.registry.rest.integration;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * @author jguillemotte
 *
 */
public interface EasysoaSimpleRegistryServiceProxy {

    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/queryWSDLInterfaces")
    //@Produces("text/xml")
    public ServiceInformations queryWSDLInterfaces(@QueryParam("search")String search, @QueryParam("subProjectId")String subProjectId) throws Exception;
    
    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/queryEndpoints")
    //@Produces("text/xml")
    public EndpointInformations queryEndpoints(@QueryParam("search")String search, @QueryParam("subProjectId")String subProjectId) throws Exception;
    
    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    @GET
    @Path("/queryServicesWithEndpoints")
    //@Produces("text/xml")
    public ServiceInformations queryServicesWithEndpoints(@QueryParam("search")String search, @QueryParam("subProjectId")String subProjectId) throws Exception;     
    
}
