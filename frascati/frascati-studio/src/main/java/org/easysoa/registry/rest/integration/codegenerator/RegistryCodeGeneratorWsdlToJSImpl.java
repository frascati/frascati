/**
 *
 */
package org.easysoa.registry.rest.integration.codegenerator;

import java.io.IOException;
import java.net.URL;

import org.easysoa.codegenerator.CodeGeneratorWsdlToJSImpl;
import org.easysoa.registry.rest.integration.resources.ResourceDownloadService;
import org.osoa.sca.annotations.Reference;

/**
 * @author jguillemotte
 *
 */
public class RegistryCodeGeneratorWsdlToJSImpl extends CodeGeneratorWsdlToJSImpl {

    @Reference
    protected ResourceDownloadService routingService;

    /**
     * TODO better : local filename, auth, reuse or overwrite
     * @param wsdlLocation
     * @return
     * @throws IOException
     */
    protected String downloadWsdl(String wsdlLocation) throws Exception{
        /*URL url = new URL(wsdlLocation);
        URLConnection uc = url.openConnection();
        String userpass = "Administrator:Administrator";//TODO
        String basicAuth = "Basic " + new String(new sun.misc.BASE64Encoder().encode(userpass.getBytes()));
        uc.setRequestProperty ("Authorization", basicAuth);
        InputStream in = uc.getInputStream();
        File localWsdlFile = new File("/home/jguillemotte/test_" + System.currentTimeMillis() + ".wsdl");//TODO dans l'app
        if (!localWsdlFile.createNewFile()) {
            throw new IOException("Unable to create local WSDL file at " + localWsdlFile.getAbsolutePath());
        }
        IOUtils.copy(in, new FileOutputStream(localWsdlFile));//TODO closes ?
        wsdlLocation = localWsdlFile.toURI().toURL().toString();
        return wsdlLocation;*/
        if(routingService != null){
            return routingService.get(new URL(wsdlLocation), preferences).toURI().toURL().toString();
        } else {
            return wsdlLocation;
        }
    }

}
