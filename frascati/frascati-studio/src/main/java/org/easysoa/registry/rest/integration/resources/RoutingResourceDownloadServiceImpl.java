/**
 *
 */
package org.easysoa.registry.rest.integration.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import org.apache.commons.io.IOUtils;
import org.easysoa.api.PreferencesManagerItf;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * @author jguillemotte
 *
 */
@Scope("COMPOSITE")
public class RoutingResourceDownloadServiceImpl implements ResourceDownloadService {

    @Reference
    protected List<UrlPatternWebResourceDownloadService> downloadServices;

    /* (non-Javadoc)
     * @see org.easysoa.registry.rest.integration.resources.RoutingWebResourceDownloadService#get(java.net.URL)
     */
    @Override
    public File get(URL url, PreferencesManagerItf preferences) throws Exception {

        // - Sort the list, to have the webResourceDownloadServices in the specified order
        // Need to be sorted manually because FraSCAti doesn't provide a native way to specify an execution order
        Collections.sort(downloadServices, new DownloadServicesComparator());

        for(UrlPatternWebResourceDownloadService service : downloadServices){
            if(service.matchUrl(url)){
                String tempFolder = System.getProperty("user.home"); // Default workspace
                if(preferences != null){
                    tempFolder = preferences.getWorkspacePath();
                }
                // file type is always WSDL ??
                if(!tempFolder.endsWith("/")){
                    tempFolder = tempFolder + "/";
                }
                File localTempFolder = new File(tempFolder + "tempWsdl");
                if(!localTempFolder.exists()){
                    localTempFolder.mkdir();
                }
                File localWsdlFile = new File(tempFolder + "tempWsdl/temp_" + System.currentTimeMillis() + ".wsdl");
                if (!localWsdlFile.createNewFile()) {
                    throw new IOException("Unable to create local WSDL file at " + localWsdlFile.getAbsolutePath());
                }
                // TODO closes ?
                IOUtils.write(service.get(url), new FileOutputStream(localWsdlFile));
                return localWsdlFile;
            }
        }
        return null;
    }

    @Override
    @GET
    @Path("/get")
    public File get(@QueryParam("fileURL") String fileURL) throws Exception {
        return get(new URL(URLDecoder.decode(fileURL, "UTF-8")), null);
    }

}
