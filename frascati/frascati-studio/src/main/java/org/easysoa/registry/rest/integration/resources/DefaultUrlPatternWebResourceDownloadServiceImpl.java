/**
 * 
 */
package org.easysoa.registry.rest.integration.resources;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * @author jguillemotte
 *
 */
@Scope("COMPOSITE")
public class DefaultUrlPatternWebResourceDownloadServiceImpl extends AbstractWebResourceDownloadService {

    @Reference
    protected WebResourceDownloadItf downloadService;
    
    @Override
    public String get(URL url) throws Exception {
        
        /*String wsdlContent = "";
        
        if(url.toURI().toURL().toString().startsWith("file://")){
            File file = new File(url.toURI());
            wsdlContent = FileUtils.readFileToString(file, "UTF-8");
        } else {
            URLConnection uc = url.openConnection();
            uc.setReadTimeout(2500);
            InputStream in = uc.getInputStream();
            
            StringWriter writer = new StringWriter();
            IOUtils.copy(in, writer, "UTF-8");
            wsdlContent = writer.toString();            
        }*/
        //return wsdlContent;
        
        return downloadService.get(url.toString());
        
    }

}
