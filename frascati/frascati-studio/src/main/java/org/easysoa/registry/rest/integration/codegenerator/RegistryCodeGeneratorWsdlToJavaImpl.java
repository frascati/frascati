/**
 *
 */
package org.easysoa.registry.rest.integration.codegenerator;

import java.io.IOException;
import java.net.URL;

import org.easysoa.codegenerator.CodeGeneratorWsdlToJavaImpl;
import org.easysoa.registry.rest.integration.resources.ResourceDownloadService;
import org.osoa.sca.annotations.Reference;

/**
 * @author jguillemotte
 *
 */
public class RegistryCodeGeneratorWsdlToJavaImpl extends CodeGeneratorWsdlToJavaImpl {

    @Reference
    protected ResourceDownloadService routingService;

    /**
     * TODO better : local filename OK TEST, auth, reuse or overwrite OK TEST
     * TODO OR DO rather queryEndpoints() and use their urls ?!??
     * @param wsdlLocation The WSDL URL of the file to download
     * @return the temporary URL of the downloaded file
     * @throws IOException
     *
     * TODO If using the serviceInformation wsdl download url, we have to get the wsdl from the nuxeo system.
     * Problem : the used url's in the generated composite use the nuxei file addresse and there some authentification problems
     *
     * The temporary solution is to use the information contained in the endpoints instead of these contained in the information service.
     * In the endpoint, there is the real address of the wsdl so there is no need to download and record a local copy of the wsdl.
     */
    protected String downloadWsdl(String wsdlLocation/*, String userId, String service*/) throws Exception {
        //File localWsdlFile;
        /*if (service == null) {
            localWsdlFile = File.createTempFile("frascati-studio_" + userId + "_" + service, "wsdl");
        } else {
            Application application = serviceManager.getCurrentApplication(userId);
            application.setCurrentWorskpacePath(preferences.getWorkspacePath());

            String sources = application.retrieveAbsoluteSources();
            File dir = new File(sources + File.separator + "impl");
            localWsdlFile = new File(dir + File.separator + service
                    + ".java");
            if (!localWsdlFile.exists()) {
                localWsdlFile.createNewFile();
            }
        }*/

        /*URL url = new URL(wsdlLocation);
        URLConnection uc = url.openConnection();
        String userpass = "Administrator:s0a";//TODO
        String basicAuth = "Basic " + new String(new sun.misc.BASE64Encoder().encode(userpass.getBytes()));
        uc.setRequestProperty ("Authorization", basicAuth);
        InputStream in = uc.getInputStream();
        localWsdlFile = new File("/home/jguillemotte/test_" + System.currentTimeMillis() + ".wsdl");//TODO dans l'app
        if (!localWsdlFile.createNewFile()) {
            throw new IOException("Unable to create local WSDL file at " + localWsdlFile.getAbsolutePath());
        }
        IOUtils.copy(in, new FileOutputStream(localWsdlFile));//TODO closes ?
        wsdlLocation = localWsdlFile.toURI().toURL().toString();
        return wsdlLocation;*/
        if(routingService != null){
            return routingService.get(new URL(wsdlLocation), preferences).toURI().toURL().toString();
        } else {
            return wsdlLocation;
        }
    }

}
