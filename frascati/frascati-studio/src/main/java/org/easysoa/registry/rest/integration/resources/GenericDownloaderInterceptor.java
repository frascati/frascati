/**
 * EasySOA Proxy Copyright 2011-2013 Open Wide
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact : easysoa-dev@googlegroups.com
 */
package org.easysoa.registry.rest.integration.resources;

import java.net.URLDecoder;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;

/**
 *
 * @author jguillemotte
 */
public class GenericDownloaderInterceptor extends AbstractPhaseInterceptor<Message> {

    //private String login;
    //private String password;

    public GenericDownloaderInterceptor() {
        super(Phase.USER_LOGICAL);
    }

    @Override
    public void handleMessage(Message message) {
        // Replace the predifined address by the given address
        String address = (String) message.get(Message.ENDPOINT_ADDRESS);
        // Removing the base address configured in the binding rest (see the webResourceService composite)
        // TODO : replace required here because the received message URL miss a /
        // Find the reason why and remove the replace method call
        address = address.substring(address.indexOf("http://localhost/")+17).replace("http:/","http://");
        //message.put(Message.ENDPOINT_ADDRESS, "http://soap.amazon.com/schemas2/AmazonWebServices.wsdl");
        try {
            address = URLDecoder.decode(address, "UTF-8");
        }
        catch(Exception ex){
            address = address.replace("%3F", "?");
        }
        message.put(Message.ENDPOINT_ADDRESS, address);

        // TODO : how to pass dynamically the login/password for authentification ??

        /*
        // Build the authentification header value
        StringBuilder authValue = new StringBuilder("basic ");
        // Encode in base 64
        authValue.append(encode64(login, password));
        List<String> authValueList = new ArrayList<String>();
        authValueList.add(authValue.toString());
        // Get the existing headers
        Map<String, List<String>> headers = (Map<String, List<String>>) message.get(Message.PROTOCOL_HEADERS);
        // Add basic authentification
        headers.put("Authorization", authValueList);
        // Add HTTP headers to the web service request
        //message.put(Message.PROTOCOL_HEADERS, headers);
        */
    }

    // TODO dummy method, replace or call a real base 64 encoding method
    private String encode64(String login, String password){
        return "";
    }
}
