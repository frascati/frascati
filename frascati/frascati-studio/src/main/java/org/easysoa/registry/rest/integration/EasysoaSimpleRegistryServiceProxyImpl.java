/**
 * 
 */
package org.easysoa.registry.rest.integration;

import org.osoa.sca.annotations.Reference;

/**
 * @author jguillemotte
 *
 */
public class EasysoaSimpleRegistryServiceProxyImpl implements EasysoaSimpleRegistryServiceProxy {

    /** Reference to the Simple registry service */
    @Reference
    private SimpleRegistryService proxyService;
    
    @Override
    public ServiceInformations queryWSDLInterfaces(String search, String subProjectId) throws Exception {
        ServiceInformations infos = proxyService.queryWSDLInterfaces(search, subProjectId); 
        return infos; 
    }

    @Override
    public EndpointInformations queryEndpoints(String search, String subProjectId) throws Exception {
        EndpointInformations infos = proxyService.queryEndpoints(search, subProjectId);
        return infos;
    }

    @Override
    public ServiceInformations queryServicesWithEndpoints(String search, String subProjectId) throws Exception {
        ServiceInformations infos = proxyService.queryServicesWithEndpoints(search, subProjectId);
        return infos;
    }

}
