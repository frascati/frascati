package org.easysoa.registry.rest.integration.client;

import org.easysoa.registry.rest.integration.EndpointInformations;
import org.easysoa.registry.rest.integration.ServiceInformations;

/**
 * Simple registry service client interface
 * 
 * @author jguillemotte
 *
 */
public interface EasysoaSimpleRegistryServiceClient {

    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    public ServiceInformations queryWSDLInterfaces(String search, String subProjectId) throws Exception;
    
    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    public EndpointInformations queryEndpoints(String search, String subProjectId) throws Exception;
    
    /**
     * 
     * @param search
     * @param subProjectId
     * @return
     * @throws Exception
     */
    public ServiceInformations queryServicesWithEndpoints(String search, String subProjectId) throws Exception;    
}