/**
 * 
 */
package org.easysoa.registry.rest.integration.resources;

import java.io.File;
import java.net.URL;

/**
 * @author jguillemotte
 *
 */
public interface UrlPatternWebResourceDownloadService {

    /**
     * Download a resource and store it locally
     * @param url The url of the resource
     * return the file where the resource is saved
     */
    public String get(URL url) throws Exception;
    
    /**
     * Check if an URL match the given regex pattern 
     * @param url The url to check
     * @return true if the url match the pattern, false otherwise
     */
    public boolean matchUrl(URL url);

    /**
     * Return the execution order number to be able to execute each webResourceDownloadService in the specified order
     * @return
     */
    public int getExecutionOrder();
    
}
