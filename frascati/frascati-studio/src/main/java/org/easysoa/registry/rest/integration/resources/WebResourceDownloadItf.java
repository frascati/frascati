package org.easysoa.registry.rest.integration.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * 
 * @author jguillemotte
 */
public interface WebResourceDownloadItf {

    //http://localhost:8080/nuxeo/nxfile/default/eb1dfac2-23f1-48a6-95e8-de742c0af7dc/blobholder:0/ContactSvc.asmx.wsdl
    @GET
    @Path("/{urlDynamicPart}")
    public String get(@PathParam("urlDynamicPart") String urlDynamicPart);
    
}
