/**
 * 
 */
package org.easysoa.registry.rest.integration.resources;

import java.net.URL;
import java.util.regex.PatternSyntaxException;
import org.osoa.sca.annotations.Property;

/**
 * @author jguillemotte
 *
 */
public abstract class AbstractWebResourceDownloadService implements UrlPatternWebResourceDownloadService {

    // integer for execution order : FraSCAti doesn't allow to use component list in a specified order
    @Property
    protected int executionOrder;
    
    // Java regex pattern
    @Property
    protected String urlPattern;
    
    /* (non-Javadoc)
     * @see org.easysoa.registry.rest.integration.resources.WebResourceDownloadService#matchUrl(java.net.URL)
     */    
    @Override
    public boolean matchUrl(URL url) throws PatternSyntaxException {
        // Check if the URL match the pattern
        if(url != null){
            return url.toString().matches(urlPattern);    
        } else {
            return false;
        }
    }
    
    /**
     * 
     * @return
     */
    @Override
    public int getExecutionOrder(){
        return this.executionOrder;
    }
    
}
