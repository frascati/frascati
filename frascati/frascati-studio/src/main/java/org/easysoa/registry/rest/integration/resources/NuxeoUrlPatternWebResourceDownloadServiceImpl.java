/**
 * 
 */
package org.easysoa.registry.rest.integration.resources;

import java.net.URL;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * @author jguillemotte
 *
 */
@Scope("COMPOSITE")
public class NuxeoUrlPatternWebResourceDownloadServiceImpl extends AbstractWebResourceDownloadService {
    
    // Nuxeo download service reference
    @Reference
    protected WebResourceDownloadItf downloadService;
    
    /* (non-Javadoc)
     * @see org.easysoa.registry.rest.integration.resources.WebResourceDownloadService#get(java.net.URL)
     */
    @Override
    public String get(URL url) throws Exception {
        // TODO download the file from the url ...
        
        // common code for download ???
        // TODO : pass only the dynamic part of the URL
        String wsdlFileContent = downloadService.get(url.toString().substring(url.toString().indexOf("default/") + 8));
        if(wsdlFileContent != null){
            return wsdlFileContent;
        } else {
            return "";
        }
    }

}
