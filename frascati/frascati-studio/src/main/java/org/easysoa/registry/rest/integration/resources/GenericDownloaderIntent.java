/**
 * EasySOA Proxy Copyright 2011-2013 Open Wide
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact : easysoa-dev@googlegroups.com
 */
package org.easysoa.registry.rest.integration.resources;

import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.WebClient;
import org.ow2.frascati.intent.cxf.AbstractBindingIntentHandler;

/**
 *
 * @author jguillemotte
 */
public class GenericDownloaderIntent extends AbstractBindingIntentHandler {

    String login = "admin";
    String password = "admin";
    
    // Add authentification (eg : to get wsdl from Nuxeo registry)
    /**
     * Apply this binding intent on a proxy instance.
     * @param proxy the proxy instance to configure.
     * @see BindingIntentHandler#apply(Object)
     */
    @Override
    public final void apply(Object proxy) {
        // Obtain the endpoint associated to the proxy instance.
        Endpoint endpoint = null;
        // Is the proxy a Apache CXF REST client ?
        if (proxy instanceof Client) {
            endpoint = WebClient.getConfig(proxy).getConduitSelector().getEndpoint();
        }

        if (endpoint == null) {
            throw new IllegalArgumentException(proxy + " is not an Apache CXF REST client!");
        }
        // Add an interceptor on outgoing requests for the endpoint.
        
        // Problem : how to pass URL and login/passwd params to the intent ???
        // TODO : add a reference  and pass it to the codeGenerator ???
        endpoint.getOutInterceptors().add(new GenericDownloaderInterceptor());
        super.apply(proxy);
    }
}
