/**
 * 
 */
package org.easysoa.registry.rest.integration.client;

import org.easysoa.registry.rest.integration.EndpointInformations;
import org.easysoa.registry.rest.integration.ServiceInformations;
import org.easysoa.registry.rest.integration.SimpleRegistryService;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * @author jguillemotte
 *
 */
@Scope("composite")
public class EasysoaSimpleRegistryServiceClientImpl implements EasysoaSimpleRegistryServiceClient {

    /** Reference to the Simple registry service */
    @Reference
    private SimpleRegistryService service;
    
    public ServiceInformations queryWSDLInterfaces(String search, String subProjectId) throws Exception {
        return service.queryWSDLInterfaces(search, subProjectId);
    }
    
    /**
     * @see 
     */
    public EndpointInformations queryEndpoints(String search, String subProjectId) throws Exception {
        return service.queryEndpoints(search, subProjectId);
    }
    
    /**
     * @see
     */
    public ServiceInformations queryServicesWithEndpoints(String search, String subProjectId) throws Exception {
        return service.queryServicesWithEndpoints(search, subProjectId);
    }
    
}
