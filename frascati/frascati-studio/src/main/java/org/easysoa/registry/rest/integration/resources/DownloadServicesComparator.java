/**
 * 
 */
package org.easysoa.registry.rest.integration.resources;

import java.util.Comparator;

/**
 * @author jguillemotte
 *
 */
public class DownloadServicesComparator implements Comparator<UrlPatternWebResourceDownloadService> {

    @Override
    public int compare(UrlPatternWebResourceDownloadService o1, UrlPatternWebResourceDownloadService o2) {

        if(o1.getExecutionOrder() > o2.getExecutionOrder()){
            return 1;
        } else if(o1.getExecutionOrder() < o2.getExecutionOrder()) {
            return -1;
        } else {
            return 0;
        }
    }

}
