package org.easysoa.registry.rest.integration.resources;

import java.io.File;
import java.net.URL;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import org.easysoa.api.PreferencesManagerItf;

/**
 *
 * @author jguillemotte
 */
public interface ResourceDownloadService {

    /**
     * Get the file corresponding to the given URL
     * @param url
     * @param preferences
     * @return
     * @throws Exception If a problem occurs
     */
    public File get(URL url, PreferencesManagerItf preferences) throws Exception;

    /**
     * Get the file corresponding to the given URL
     * @param url
     * @return
     * @throws Exception If a problem occurs
     */
    @GET
    @Path("/get")
    public File get(@QueryParam("fileURL") String fileURL) throws Exception;

}