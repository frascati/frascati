/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.easysoa.compiler;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.easysoa.api.CompilerItf;
import org.easysoa.api.FileManager;
import org.easysoa.utils.JarGenerator;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.component.factory.impl.ComponentFactoryContextImpl;
import org.ow2.frascati.mojo.contribution.Contribution;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * SCA Java compiler using javax tools and introspection api to generate contribution 
 */
@Scope("COMPOSITE")
public class SCAJavaCompilerImpl implements CompilerItf {
    
    /*
     * Static attributes
     * 
     */
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(SCAJavaCompilerImpl.class.getCanonicalName());
    
    private static FileFilter filterJavaFiles = new FileFilter() {
        public boolean accept(File file) {
            String filename = file.getName();
            return filename.endsWith(".java") || file.isDirectory();
        }
    };

    /*
     * References
     * 
     */
    
    @Reference
    protected FileManager fileManager;

    @Reference
    protected MembraneGeneration membraneGeneration;
    
    /*
     * Properties
     * 
     */
    @Property
    private String sourceDirectory = "src/main/java";
    
    @Property
    private String binDirectory = "target/classes";
    
    @Property
    private String resourcesDirectory = "src/main/resources";
    
    public FileManager getFileManager() {
        return fileManager;
    }

    public void setFileManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }
    
    /** 
     * Set the property 'sourceDirectory'.
    */
    public void setSourceDirectory(String sourceDirectory) {
       this.sourceDirectory = sourceDirectory;
    }
    
    /** 
     * Set the property 'binDirectory'.
    */
    public void setBinDirectory(String binDirectory) {
       this.binDirectory = binDirectory;
    }
        
    /** 
     * Set the property 'resourcesDirectory'.
    */
    public void setResourcesDirectory(String resourcesDirectory) {
       this.resourcesDirectory = resourcesDirectory;
    }
    
    
    /*
     * Methods
     *
     */
    /**
     * @see CompilerItf#compile(String, String, String, String)
     */
    @Override
    public File compile(String root, String applicationName, String globalLibPath, String localLibPath) throws Exception {
        String srcPath = root+File.separator+ sourceDirectory;
        String binPath = root+File.separator+ binDirectory;
        
        fileManager.deleteRecursively(new File( binPath));
        
        List<File> classPath = new ArrayList<File>();
        
        List<File> jarLocalLib = new ArrayList<File>();
         
        //add jars from the global class path
        if (globalLibPath != null && !"".equals(globalLibPath)) {
            classPath.addAll(getClassPath(globalLibPath));
        }
        if (localLibPath != null && !"".equals(localLibPath)) {
            jarLocalLib = getClassPath(localLibPath);
            
            //add jars from the local class path
            classPath.addAll(jarLocalLib);
        }

        if(membraneGeneration != null) {
          //
          // Use the Java compiler embedded in FraSCAti.
          //

          binPath = compileAllWithJavaCompilerEmbeddedByFraSCAti(srcPath, binPath, classPath);

        } else {
          //
          // Use the JDK compiler.
          //

          if(compileAll(srcPath, binPath, classPath) == false){
            return null;
          }

        }

        // Copy resources(s) to bin directory to generate jar
        File fileWhereClassesAreGenerated = new File(binPath);
        File dirResources = new File(root +File.separator+ resourcesDirectory);
        for (File fileToCopy : dirResources.listFiles()) {
          fileManager.copyFilesRecursively(fileToCopy, dirResources, fileWhereClassesAreGenerated);
        }

        File jarGenerated = JarGenerator.generateJarFromAll(binPath, binPath + File.separator + ".." + File.separator + applicationName + ".jar");

        return generateContribution(applicationName, binPath, jarGenerated, jarLocalLib);
    }

    /**
     * Compile all Java files.
     */
    public String compileAllWithJavaCompilerEmbeddedByFraSCAti(String sourcePath, String targetPath, List<File> classPath) throws Exception
    {
      //
      // TODO: This is a quick and dirty patch.
      //

      LOG.info("============================");
      LOG.info("  Compile all Java files");
      LOG.info("  - sourcePath=" + sourcePath);
      LOG.info("  - targetPath=" + targetPath);
      LOG.info("  - classPath=");
      for(File path : classPath) {
        LOG.info("     - " + path.toString());
      }
      LOG.info("============================");

      FrascatiClassLoader cl = new FrascatiClassLoader(); // this.classloaderManager.getClassLoader());
//      membraneGeneration.open(cl);
//      membraneGeneration.addJavaSource(sourcePath);
//      membraneGeneration.compileJavaSource();
      ComponentFactoryContext context = new ComponentFactoryContextImpl(cl);
      membraneGeneration.open(context);
      context.addJavaSourceDirectoryToCompile(sourcePath);
      membraneGeneration.close(context);

      String pathWhereClassesAreGenerated = cl.getURLs()[0].toString();
      LOG.info("  pathWhereClassesAreGenerated=" + pathWhereClassesAreGenerated);
      LOG.info("============================");
      return pathWhereClassesAreGenerated.substring("file:".length());
    }

    /**
     * Compile all .java in the sourcePath to .class in the targetPath
     * 
     * @param sourcePath
     * @param targetPath
     * @param classPath
     * @return true if success, false if fail
     * @throws IOException 
     * @throws ParseException
     */
    public static boolean compileAll(String sourcePath, String targetPath, List<File> classPath) throws IOException, ParseException{
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
        
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
        
        List<String> pathNameList= new ArrayList<String>();
        
        List<String> options = new ArrayList<String>();

        
        boolean success = true;
        
        File target = new File(targetPath);
        if (!target.exists() && !target.mkdirs()) {
            throw new IOException("Impossible to create directory " + targetPath);
        }
        
        File src = new File(sourcePath);
        if (!src.exists() && ! src.mkdirs()) {
            throw new IOException("Impossible to create directory " + sourcePath);
        }
        
        //Setting the directory for .class files
        options.add("-d");
        options.add(targetPath);
                
        String libList = "";
        for(int i = 0 ; i < classPath.size(); i++) {
            File classPathItem = classPath.get(i);

            if (i > 0) {
                if(SCAJavaCompilerImpl.isWindows()){
                    libList = libList.concat(";");
                }
                else{
                    libList = libList.concat(":");
                }
            }
            libList = libList.concat(classPathItem.getCanonicalPath());

        }
        options.add("-classpath");
        options.add(libList);
        
        if(checkFolder(src, pathNameList) == false){
            success = false;
        }
        
        if (pathNameList.isEmpty()) {
            LOG.info("No java files to compile");
            return success;
        }
         
                
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromStrings(pathNameList);

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, diagnostics, options, null, compilationUnits);
        
        LOG.info("Compiling...");
                
        if (task.call() == false){
            success = false;
            LOG.severe("Fail!");
            String errorMessage = "Compilation fail!\n\n";

            for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
                LOG.severe(diagnostic.getMessage(null));
                errorMessage = errorMessage.concat(diagnostic.getMessage(null) + "\n");
            }
            throw new ParseException(errorMessage, 0);
            
        } else {
            LOG.info("Ok");
            LOG.info("Compiled files:");
            for (String filePath : pathNameList) {
                LOG.info(filePath);
            }
        }
        
        fileManager.close();
        
        return success;
    }

    /**
     * Check the folders, searching by all .java recursively
     * 
     * @param src
     * @param pathNameList
     * @return
     * @throws IOException 
     */
    private static boolean checkFolder(File src, List<String> pathNameList) throws IOException {
        
        File[] files = src.listFiles(filterJavaFiles);
        
        boolean success = true;
        
        for (File file : files) {
            if(file.isDirectory()){
                checkFolder(file, pathNameList);
            } else {
                pathNameList.add(file.getCanonicalPath());
            }
        }
        return success;
        
    }
    
    /**
     * Retrieve a list of files to be used in the classpath, from the <b>libPath</b>
     * 
     * @param libPath
     * @return
     * @throws IOException
     */
    private static List<File> getClassPath(String libPath) throws IOException {
        
        File libDirectory = new File(libPath);
        List<File> jarFileList = new ArrayList<File>();
        
        if (!libDirectory.exists()) {
            libDirectory.mkdirs();
        }
        
        if (libDirectory.isDirectory()) {
            for (File jarFile : libDirectory.listFiles()) {
                jarFileList.add(jarFile);
            }
        }else{
            throw new IOException("Classpath not found");
        }
        
        return jarFileList;
    }

    private File generateContribution(String applicationName, String binPath, File jarGenerated, List<File> localJarLib) {
        List<File> jars = new ArrayList<File>();
        jars.add(jarGenerated);
        jars.addAll(localJarLib);
        
        List<String> deployables = new ArrayList<String>();
        deployables.add(applicationName + ".composite");
        
        Contribution contribution=new Contribution(applicationName, deployables, null, null, localJarLib);
        contribution.addLib(jarGenerated);
        
        try
        {
            File destDir=new File(binPath + File.separator + ".." + File.separator);
            return contribution.generate(destDir);
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean isWindows() {
        String operatingSystem = System.getProperty("os.name").toLowerCase(Locale.ENGLISH);
        return (operatingSystem.indexOf("win") >= 0);
    }
}
