/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.codegenerator;

import java.io.File;

import org.easysoa.model.Application;

/**
 * Interface for code transformation
 */
public interface CodeTransformerItf {

	/**
	 * Id of the code generator
	 * 
	 * @return the id
	 */
	String getId();
	
	/**
	 * Transform file (an interface) in json
	 * 
	 * @param fileContent
	 * @param application concerned application
	 * @return json which represent the interface
	 */
	String transform(File generatedFile, Application application) throws Exception;
}
