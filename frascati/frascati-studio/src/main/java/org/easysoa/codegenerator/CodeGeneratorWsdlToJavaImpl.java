/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto, Philippe Merle
 *
 */

package org.easysoa.codegenerator;

import com.ibm.wsdl.extensions.soap12.SOAP12AddressImpl;
import com.sun.xml.bind.api.impl.NameConverter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

import javax.wsdl.Definition;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;

import org.apache.cxf.tools.common.ToolContext;
import org.apache.cxf.tools.common.ToolException;
import org.apache.cxf.tools.wsdlto.WSDLToJava;
import org.easysoa.api.CodeGenerator;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.easysoa.utils.UrlUtils;
import org.easysoa.registry.rest.integration.resources.ResourceDownloadService;
import org.easysoa.utils.WsdlInformations;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
// TODO: to remove
// import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.factory.WebServiceCommandLine;
import org.ow2.frascati.wsdl.WsdlCompiler;
import org.ow2.frascati.wsdl.WsdlHelper;

/**
 * Code generator from WSDL
 */
@Scope("COMPOSITE")
public class CodeGeneratorWsdlToJavaImpl implements CodeGenerator {

    @Reference
    protected WsdlCompiler wsdlCompiler;
    @Reference
    protected ServiceManager serviceManager;
// TODO: to remove
//   @Reference
//   protected MembraneGeneration membraneGeneration;
    @Reference
    protected PreferencesManagerItf preferences;

    /** Logger */
    public final static Logger LOG = Logger.getLogger(CodeGeneratorWsdlToJavaImpl.class.getCanonicalName());
    
    public String getId(){
    	return "Java";
    }
    
    protected String downloadWsdl(String wsdlLocation) throws Exception {
        return wsdlLocation;
    }
    
    /**
     * @see CodeGenerator#generate(String, String, String, String)
     */
    @Override
    public String generate(String userId, String service, String portTypeName, String wsdlLocation){
        
        try{
            wsdlLocation = downloadWsdl(wsdlLocation);
        }
        catch(Exception ex){
            ex.printStackTrace();
            String msg = "Unable to download wsdl " + wsdlLocation + " " + ex.getMessage();
            LOG.info(msg);
            return msg;            
        }
        
        try{
        	this.compile(userId, wsdlLocation);
        }
        catch(ToolException te){
        	te.printStackTrace();
        	LOG.info("wsdl not supported by cxf");
        	return "wsdl not supported by cxf";
        }
        Application application = serviceManager.getCurrentApplication(userId);
        Definition definition = null;
		try {
			definition = wsdlCompiler.readWSDL(wsdlLocation);
		} catch (WSDLException e1) {
			e1.printStackTrace();
		}
		if(definition == null){
			return "Problem with the WSDL";
		}

        // Search the requested port type.
        PortType portType = WsdlHelper.getPortTypeIntoDefinitionAndAllItsImportsRecursively(definition, portTypeName);
        if(portType == null) {
          return portType + " not found";
        }

		String portTypeClassName = NameConverter.standard.toPackageName(portType.getQName().getNamespaceURI()) + '.' + portTypeName;
        PrintWriter printWriter = null;
        try {
// TODO: to remove
//            ClassLoader classLoader = membraneGeneration.compileJavaSource();
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            Class<?> interfaceClass = classLoader.loadClass(portTypeClassName);

            File javaFile = this.createJavaFile(userId, service);
            
            printWriter = new PrintWriter(javaFile);
            printWriter.println("package " + application.getPackageName() + ".impl;");
            printWriter.println("import " + portTypeClassName + ";");
            printWriter.println("@org.osoa.sca.annotations.Scope(\"COMPOSITE\")");
            printWriter.println("public class "+ service + " implements "+ portTypeName);
            printWriter.println("{");
            printWriter.println("  @org.osoa.sca.annotations.Reference");
            printWriter.println("  private " + portTypeName + " wsdlReference;");
            printWriter.println();
            for(Method method : interfaceClass.getMethods()){
            	printWriter.print("  public " + method.getReturnType().getName() + " " + method.getName()+"(");
                for (int paramIndex = 1; paramIndex <= method.getParameterTypes().length; paramIndex++) {
                    if(paramIndex == 1) printWriter.print(method.getParameterTypes()[paramIndex-1].getName() + " param"+paramIndex);
                    else printWriter.print(", " + method.getParameterTypes()[paramIndex-1].getName() + " param"+paramIndex);
                }
                printWriter.println(")");
                Class<?>[] exceptions = method.getExceptionTypes();
                if(exceptions.length > 0) {
                  printWriter.print("    throws");
                  String separator = " ";
                  for(Class<?> exception : exceptions) {
                    printWriter.print(separator);
                    printWriter.print(exception.getName());
                    separator = ", ";
                  }
                }
                printWriter.println();
                printWriter.println("  {");
                printWriter.print("    ");
                if(method.getReturnType() != void.class) {
                  printWriter.print("return ");
                }
                printWriter.print("wsdlReference."+method.getName()+"(");
                for (int paramIndex = 1; paramIndex <= method.getParameterTypes().length; paramIndex++) {
                    if(paramIndex == 1) printWriter.print("param"+paramIndex);
                    else printWriter.print(", param"+paramIndex);
                }
                printWriter.println(");");
                printWriter.println("  }");
                printWriter.println();
            }
            printWriter.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
        	if(printWriter != null){
        		printWriter.flush();
                printWriter.close();
        	}
        }
        return "";
    }

	private File createJavaFile(String userId, String service) {
        try {
            Application application = serviceManager.getCurrentApplication(userId);
            application.setCurrentWorskpacePath(preferences.getWorkspacePath());
            
            String sources = application.retrieveAbsoluteSources();
            File dir = new File(sources + File.separator + "impl");
            File javaFile = new File(dir + File.separator + service
                    + ".java");
            javaFile.createNewFile();
            return javaFile;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void compile(String userId, String wsdlLocation) throws ToolException{
        try {
            wsdlLocation = UrlUtils.checkUrl(wsdlLocation);

            // actual download
            try{
                wsdlLocation = downloadWsdl(wsdlLocation);
            }
            catch(Exception ex){
                ex.printStackTrace();
                throw new FileNotFoundException("Unable to download wsdl " + wsdlLocation + " " + ex.getMessage());
            }
            
            WebServiceCommandLine cmd = new WebServiceCommandLine("wsdl2java");

            String[] args = new String[4];
            args[0] = "-u";
            args[1] = wsdlLocation;
            args[2] = "-o";
            String outputDirectory = null;
            Application application = serviceManager.getCurrentApplication(userId);
            application.setCurrentWorskpacePath(preferences.getWorkspacePath());
            if(application.getPackageName() != null && !application.getPackageName().equals("")){
            	outputDirectory = application
                    .retrieveAbsoluteSources().substring(0, serviceManager.getCurrentApplication(userId)
                            .retrieveAbsoluteSources().indexOf(serviceManager.getCurrentApplication(userId).getPackageName().replace(".", File.separator)));
            }
            else{
            	outputDirectory = application.retrieveAbsoluteSources();
            }

            args[3] = outputDirectory;
            cmd.parse(args);

            File wsdlF = cmd.getWsdlFile();
            URL wsdlUrl = cmd.getWsdlUrl();

            if ((wsdlF == null) && (wsdlUrl == null)) {
                System.err.println("Please set the WSDL file/URL to parse");
            }
            if ((wsdlF != null) && (wsdlUrl != null)) {
                System.err
                        .println("Please choose either a WSDL file OR an URL to parse (not both!).");
            }

            String wsdl = (wsdlF == null ? wsdlUrl.toString() : wsdlF
                    .getAbsolutePath());
            String[] params = new String[] { "-d", outputDirectory, wsdl };
            ToolContext toolContext = new ToolContext();
            new WSDLToJava(params).run(toolContext);
        }   catch(ToolException te){
        	throw new ToolException();
        }	catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    /**
     * @see CodeGenerator#loadWsdlString(String)
     */
    @Override
    public String loadWsdlString(String wsdl) throws FileNotFoundException
    {
        return transformWsdlInformationsToString(loadWsdl(wsdl));
    }

    /**
     * @see CodeGenerator#loadWsdl(String)
     */
    @SuppressWarnings("unchecked")
    @Override    
    public WsdlInformations loadWsdl(String wsdl) throws FileNotFoundException
    {
        try {
            wsdl = downloadWsdl(wsdl);
        } catch (Exception ex) {
            ex.printStackTrace();            
            throw new FileNotFoundException("Unable to download wsdl " + wsdl + " " + ex.getMessage());
        }
        
        WsdlInformations wsdlInformations = new WsdlInformations();
        try {
            Definition definition = wsdlCompiler.readWSDL(wsdl);
            wsdlInformations.setTargetNameSpace(definition.getTargetNamespace());
            for(QName serviceQName : (Set<QName>)definition.getServices().keySet()){
            	Service service = definition.getService(serviceQName);
                for(String portName : (Set<String>)(service.getPorts().keySet())){
                    
                    Iterator iter = service.getPort(portName).getExtensibilityElements().iterator();
                    String endpointUrl = "";
                    while(iter.hasNext()){
                        Object o = iter.next();
                        SOAP12AddressImpl soapAddress = (SOAP12AddressImpl)o;
                        endpointUrl = soapAddress.getLocationURI();
                    }
                    
                	QName portType = service.getPort(portName).getBinding().getPortType().getQName() ;
                    wsdlInformations.addServicePort(serviceQName.getLocalPart()+"/"+portName+'/'+portType.getLocalPart());
                    wsdlInformations.addEndpointUrl(serviceQName.getLocalPart()+"/"+portName+'/'+portType.getLocalPart(), endpointUrl);                    
                }
            }            
        } catch (WSDLException e) {
            e.printStackTrace();
        }
        return wsdlInformations;
    }
    
    @SuppressWarnings("unchecked")
    private String transformWsdlInformationsToString(WsdlInformations wsdl) {
        JSONObject wsdlInformations = new JSONObject();
        wsdlInformations.put("targetNameSpace", wsdl.getTargetNameSpace());
        JSONArray servicesPorts = new JSONArray();
        for(String servicePort : wsdl.getServicesPorts()){
            JSONObject servicePortObject = new JSONObject();
            servicePortObject.put("value", servicePort);
            servicesPorts.add(servicePortObject);
        }
        wsdlInformations.put("servicesPorts", servicesPorts);
        return wsdlInformations.toJSONString();
    }

	@Override
	public String getCompositeLineImplementation(String service) {
		return "<implementation.java class=\"org.ow2.frascati.impl."+service+"\"/>";
	}
}
