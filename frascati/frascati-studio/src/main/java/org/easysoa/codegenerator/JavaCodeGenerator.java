/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.codegenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 * Code generator for java
 * 
 * @see CodeGeneratorItf
 */
public class JavaCodeGenerator implements CodeGeneratorItf {

    /**
     * @see CodeGeneratorItf#getId()
     */
	@Override
	public String getId() {
		return "Java";
	}

	/**
	 * @see CodeGeneratorItf#generate(String, File, String)
	 */
	@Override
	public String generate(String json, File file, String implementationName) {
		Object obj=JSONValue.parse(json);
		JSONObject array=(JSONObject)obj;
		PrintWriter printWriter = null;
		implementationName = implementationName.substring(0, implementationName.indexOf("."));
		try {
			printWriter = new PrintWriter(file);
			
			String interfaceName = (String)array.get("name");
			String importInterface = null;
			if(interfaceName.contains(".")){
				importInterface = interfaceName.substring(0, interfaceName.lastIndexOf("."));
				interfaceName = interfaceName.substring(interfaceName.lastIndexOf(".")+1);
			}
			JSONArray methods = (JSONArray)array.get("methods");
			
			printWriter.println("package org.ow2.frascati.impl;");
			if(importInterface != null){
				printWriter.println("import "+importInterface+"."+interfaceName+";");
			}
			printWriter.println();
			printWriter.println("public class "+implementationName+" implements "+interfaceName+ "{");
			printWriter.println();
			
			for(int i = 0; i < methods.size(); i++){
				JSONObject method = (JSONObject)methods.get(i);
				printWriter.print("    public "+ (String)method.get("return") + " " +(String)method.get("name") + "(");
				JSONArray params = (JSONArray)method.get("params");
				for(int paramIndex = 0; paramIndex < params.size(); paramIndex++){
					JSONObject param = (JSONObject)params.get(paramIndex);
					if(paramIndex != 0){
						printWriter.print(","+param.get("param")+" param"+paramIndex);
					}
					else{
						printWriter.print(param.get("param")+" param"+paramIndex);
					}
				}
				JSONArray exceptions = (JSONArray)method.get("exceptions");
				printWriter.print(")");
				if(exceptions.size() != 0){
					printWriter.print(" throws ");
					for(int exceptionIndex = 0; exceptionIndex < exceptions.size(); exceptionIndex++){
						JSONObject exception = (JSONObject) exceptions.get(exceptionIndex);
						if(exceptionIndex != 0){
							printWriter.print(","+exception.get("exception"));
						}
						else{
							printWriter.print(exception.get("exception"));
						}
					}
				}
				printWriter.println("{");
				printWriter.println();
				printWriter.println("    }");
				
			}
			
			printWriter.println("}");
			printWriter.flush();
			printWriter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

}
