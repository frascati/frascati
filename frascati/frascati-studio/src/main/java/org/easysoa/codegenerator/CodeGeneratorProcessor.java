/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.codegenerator;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.easysoa.api.CodeGeneratorProcessorItf;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Processor for code generator
 */
@Scope("COMPOSITE")
public class CodeGeneratorProcessor implements CodeGeneratorProcessorItf{

	@Reference
    protected List<CodeGeneratorItf> generators;
	protected Map<String, CodeGeneratorItf> generatorsMap;
	
	/** Logger */
    public final static Logger LOG = Logger.getLogger(CodeGeneratorProcessor.class.getCanonicalName());
    
    /**
     * Initialize processors
     */
    @Init
    public final void initializeProcessorsByID() {
    	LOG.info("code transformer");
        this.generatorsMap = new HashMap<String, CodeGeneratorItf>();
        for (CodeGeneratorItf codeGenerator : this.generators) {
            this.generatorsMap.put(codeGenerator.getId(), codeGenerator);
        }
    }

    /**
     * @see CodeGeneratorProcessorItf#generate(String, String, File, String)
     */
	@Override
	public void generate(String generator, String json, File file, String implementationName) {
		this.generatorsMap.get(generator).generate(json, file, implementationName);
	}
	
	
}
