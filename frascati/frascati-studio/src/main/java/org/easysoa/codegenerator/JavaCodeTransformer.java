/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.easysoa.codegenerator;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.easysoa.api.CompilerItf;
import org.easysoa.model.Application;
import org.easysoa.utils.exceptions.CompilationException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.component.factory.impl.ComponentFactoryContextImpl;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * Code transformer for java code
 */
public class JavaCodeTransformer implements CodeTransformerItf {

	@Reference
	protected MembraneGeneration membraneGeneration;
	@Reference
	protected CompilerItf scaCompiler;

	/**
	 * @see CodeTransformerItf#getId()
	 */
	@Override
	public String getId() {
		return "Java";
	}

	/**
	 * @see CodeTransformerItf#transform(File, Application)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String transform(File generatedFile, Application application)
			throws CompilationException {
		String json = null;
//		FrascatiClassLoader cl = new FrascatiClassLoader();
		ComponentFactoryContext context = new ComponentFactoryContextImpl();
		try {
//			membraneGeneration.open(cl);
			membraneGeneration.open(context);
		} catch (FactoryException e1) {
			e1.printStackTrace();
		}
//		membraneGeneration.addJavaSource(application.retrieveAbsoluteRoot()
		context.addJavaSourceDirectoryToCompile(application.retrieveAbsoluteRoot()
				+ File.separator + "src" + File.separator + "main"
				+ File.separator + "java");
//		membraneGeneration.addJavaSource(application.retrieveAbsoluteRoot()
		context.addJavaSourceDirectoryToCompile(application.retrieveAbsoluteRoot()
				+ File.separator + "src" + File.separator + "main"
				+ File.separator + "resources");
//		ClassLoader classLoader;
		ClassLoader classLoader = context.getClassLoader();
		try {
//			classLoader = membraneGeneration.compileJavaSource();
//			membraneGeneration.close();
			membraneGeneration.close(context);
		} catch (Exception e) {
			
			Throwable throwable = e;
			while (throwable.getCause() != null) {
			    throwable = throwable.getCause();
			}
			if (throwable instanceof IOException) {
				throw new CompilationException(
						throwable.getMessage());
			}
			throw new CompilationException("Undeclared throwable exception");
		}
		String fileName = generatedFile.getName().substring(0,
				generatedFile.getName().indexOf("."));
		String className = "org.ow2.frascati.api" + "." + fileName;
		Class<?> interfaceClass;
		try {
			interfaceClass = classLoader.loadClass(className);
		} catch (ClassNotFoundException e) {
			return null;
		}
		JSONObject interfaceJson = new JSONObject();
		interfaceJson.put("name", className);
		JSONArray methodsJSon = new JSONArray();
		interfaceJson.put("methods", methodsJSon);
		for (Method method : interfaceClass.getMethods()) {
			JSONObject methodJson = new JSONObject();
			methodJson.put("name", method.getName());
			methodJson.put("return", convertType(method.getReturnType()
					.getName()));
			JSONArray params = new JSONArray();
			methodJson.put("params", params);
			for (int paramIndex = 0; paramIndex < method.getParameterTypes().length; paramIndex++) {
				JSONObject param = new JSONObject();
				param.put("param",
						convertType(method.getParameterTypes()[paramIndex]
								.getCanonicalName()));
				params.add(param);
			}
			JSONArray exceptions = new JSONArray();
			methodJson.put("exceptions", exceptions);
			for (int paramIndex = 0; paramIndex < method.getExceptionTypes().length; paramIndex++) {
				JSONObject exception = new JSONObject();
				exception.put("exception",
						method.getExceptionTypes()[paramIndex]
								.getCanonicalName());
				exceptions.add(exception);
			}
			methodsJSon.add(methodJson);
		}
		json = interfaceJson.toJSONString();
		
		return json;

	}

	private String convertType(String type) {
		if (type.contains(".")) {
			return type.substring(type.lastIndexOf(".") + 1, type.length());
		} else {
			return type;
		}
	}

}
