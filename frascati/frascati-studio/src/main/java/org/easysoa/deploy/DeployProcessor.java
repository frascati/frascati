/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto, Philippe Merle
 *
 */

package org.easysoa.deploy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.easysoa.api.CompilerItf;
import org.easysoa.api.DeployProcessorItf;
import org.easysoa.model.Application;
import org.easysoa.model.DeploymentLocation;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.remote.introspection.stringlist.StringList;

/**
 * Process actions related to deployment
 */
@Scope("COMPOSITE")
public class DeployProcessor implements DeployProcessorItf {

    @Reference
    protected CompilerItf scaCompiler;

    protected Deployment deployment;
    
    private ArrayList<DeploymentLocation> deploymentLocations;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(DeployProcessor.class.getCanonicalName());

    public CompilerItf getScaCompiler() {
        return scaCompiler;
    }

    public void setStudioCompiler(CompilerItf studioCompiler) {
        this.scaCompiler = studioCompiler;
    }
    
    public Deployment getDeployment() {
        return deployment;
    }

    public void setDeployment(Deployment deployment) {
        this.deployment = deployment;
    }

    /**
     * Loads deployment properties
     */
    @Init
    public final void initLoadingDeploymentProperties(){
        if (loadDeploymentProperties()) {
            LOG.info("deploy.properties loaded");
        } else {
            LOG.warning("Error trying to load Deployment Properties (deploy.properties). Users applications cannot be deployed!");
        }
    }
    
    /**
     * Loads deployment properties from a file
     */
    public final boolean loadDeploymentProperties(){
        InputStream inputStream = DeployProcessor.class
                .getResourceAsStream("/properties/deploy.properties");
        this.deploymentLocations = new ArrayList<DeploymentLocation>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] keyValue = line.split("=");
                String[] values = keyValue[1].split("#");
                LOG.info("key : "+keyValue[0]);
                LOG.info("url : "+values[1]);
                LOG.info("web-explorer : "+values[1]);
                this.deploymentLocations.add(new DeploymentLocation(keyValue[0], values[0], values[1]));
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    /**
     * @see DeployProcessorItf#getAvailableDeployments()
     */
    @Override
    public List<DeploymentLocation> getAvailableDeployments(){
        return deploymentLocations;
    }
    
    /**
     * @see DeployProcessorItf#undeploy(String[], Application)
     */
    @Override
    public boolean undeploy(String[] ids, Application application) throws Exception {
        boolean deploymentResult = true;
        
        for (int i = 0; i < ids.length; i++) {
            String server = ids[i];
            deployment = JAXRSClientFactory.create(server, Deployment.class);
            
            LOG.info("Undeploying " + Integer.toString(i+1) + " of " + Integer.toString(ids.length));
            
            if (deployment.undeployComposite(application.getName()) >= 0) {
                LOG.info("Application undeployed");
            } else {
                LOG.info("Error deploying application");
                deploymentResult = false;
            }
        }
        if (deploymentResult == false) {
            throw new Exception("Fail in one or more undeployments. See the log error or contact the administrator.");            
        }
        
        return deploymentResult;
    }
    
    /**
     * @see DeployProcessorItf#deploy(String[], Application)
     */
    @Override
    public boolean deploy(String[] ids, Application application) throws Exception {
        boolean deploymentResult = true;

        File contribFile = scaCompiler.compile(application.retrieveAbsoluteRoot(), application.getName(), application.getGlobalLibPath(), application.getLocalLibPath());
        
        if (contribFile == null) {
            throw new Exception("No contribution file generated.");
        }
        
        String serversFaulty = "Errors in servers: ";
        for (int i = 0; i < ids.length; i++) {
            String server = ids[i];
            LOG.info("Deploying " + Integer.toString(i+1) + " of " + Integer.toString(ids.length));
            if(deploy(server, contribFile) == false){
                serversFaulty = serversFaulty.concat(server+", ");
                deploymentResult = false;
            }
        }
        if (deploymentResult == false) {
            throw new Exception("Fail in one or more deployments.\n"+serversFaulty+"verify log errors or contact the administrator.");            
        }
        
        return deploymentResult;
    }

    private boolean deploy(String server,File contribFile) throws Exception {
        LOG.info("Cloud url : "+server);
        
        deployment = JAXRSClientFactory.create(server, Deployment.class);
        //Deploy contribution
        String contrib = null;
        
        try {
            contrib = FileUtil.getStringFromFile(contribFile);
        } catch (IOException e) {
            throw new IOException("Cannot read the contribution!");
        }
        
        LOG.info("** Trying to deploy a contribution ...");
        try {
            StringList stringList = deployment.deployContribution(contrib);
            List<String> compositeNames=stringList.getStringList();
            if (compositeNames != null && !compositeNames.isEmpty()) {
                LOG.info("** Contribution deployed!");
                return true;
            }else{
                LOG.severe("Error trying to deploy contribution in: " + server);
                return false;
            }
        } catch (Exception e) {
            throw new Exception("Exception trying to deploy contribution in: " + server + "message: " + e.getMessage());
        }
        
    }

    /**
     * @see DeployProcessorItf#getWebExplorerUrlsInJson(List)
     */
	@SuppressWarnings("unchecked")
	@Override
	public String getWebExplorerUrlsInJson(List<String> urlsDeploy) {
		JSONObject urls = new JSONObject();
		JSONArray urlsArray = new JSONArray();
		for(DeploymentLocation deploymentLocations : this.deploymentLocations){
			if(urlsDeploy.contains(deploymentLocations.getUrl())){
				JSONObject webExplorer = new JSONObject();
				webExplorer.put("webExplorer", deploymentLocations.getWebExplorerUrl());
				webExplorer.put("cloud", deploymentLocations.getName());
				urlsArray.add(webExplorer);
			}
		}
		urls.put("urls", urlsArray);
		return urls.toJSONString();
	}
    

    


}