/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *  This table contains informations related to filename extensions.<br>
 *  The location field refers to the relative location where these kind of files 
 *  will be saved or created for example.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileType")
@XmlRootElement(name = "FileType")
@Entity(name = "FileType")
public class FileType implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlAttribute(name = "id", required = true)
    protected Integer id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "location" , required = true)
    protected String location;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(FileType.class.getCanonicalName());
    
    /**
     * FileType's Constructor
     * 
     * @param name - The name identifying the preference 
     * @param location - Upload location for this type
     */
    public FileType(String name, String location){
        this.name = name;
        this.location = location;
    }

    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(unique = true, nullable = false)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(nullable = false)
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    /**
     * Retrieve all the file type names available in FileType table
     * 
     * @param entityManager
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<String> retrieveAllFileTypes(EntityManager entityManager){
        Query query = entityManager
                .createQuery("SELECT fileType.name FROM FileType fileType");
        try{
            List<String> fileTypes = query.getResultList();
            return fileTypes;
        }catch(Exception e){
            LOG.severe("Error trying to retrive file types!" );
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Return the file type location corresponding to fileTypeName
     * 
     * @param entityManager
     * @param fileTypeName
     * @return the corresponding location
     */
    public static String retrieveLocationFileType(EntityManager entityManager, String fileTypeName){
        Query query = entityManager
                .createQuery("SELECT fileType.location FROM FileType fileType WHERE fileType.name = :fileType");
        query.setParameter("fileType", fileTypeName);
        try{
            String location = (String) query.getSingleResult();
            return location;
        }catch(NoResultException nre){
            LOG.severe("No result found for " + fileTypeName + " file type!" );
            return null;
        }catch(Exception e){
            LOG.severe("Error trying to retrive " +fileTypeName + " file type!" );
            e.printStackTrace();
            return null;
        }
    }
    
}
