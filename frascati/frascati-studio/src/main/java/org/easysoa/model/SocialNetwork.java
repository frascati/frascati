package org.easysoa.model;

import java.util.Locale;

/**
 *	Social Network enum for OAuth connection 
 *
 */
public enum SocialNetwork {

	TWITTER("Twitter"),
    FACEBOOK("Facebook"),
	GOOGLE("Google");
    
    private final String value;
    
    private SocialNetwork(String value){
        this.value = value;
    }
    
    public String value() {
        return value;
    }
    
    /**
     * Returns a SocialNetwork from the SocialNetwork value
     * 
     * @param v the SocialNetwork value
     * @return the SocialNetwork
     */
    public static SocialNetwork fromValue(String v) {
    	v = v.toLowerCase(Locale.ENGLISH);
        if(v.equals("twitter")){
        	return SocialNetwork.TWITTER;
        }
        else if(v.equals("facebook")){
        	return SocialNetwork.FACEBOOK;
        }
        else if(v.equals("google")){
        	return SocialNetwork.GOOGLE;
        }
        throw new IllegalArgumentException(v);
    }
    
}
