/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Persistence layer to manage the entity BugReport
 */
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BugReport")
@XmlRootElement(name = "BugReport")
@Entity(name = "BugReport")
public class BugReport implements Serializable{

	@XmlAttribute(name = "id")
	private Long id;
	@XmlAttribute(name = "login")
	private String login;
	@XmlAttribute(name = "bugDescription", required = true)
	private String bugDescription;
	@XmlAttribute(name = "mail")
	private String mail;
	@XmlAttribute(name = "object")
	private String object;
	
	public BugReport() {
	
	}
	
	public BugReport(String login, String mail, String object, String bugDescription){
		this.login = login;
		this.mail = mail;
		this.object = object;
		this.bugDescription = bugDescription;
	}
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getBugDescription() {
		return bugDescription;
	}

	public void setBugDescription(String bugDescription) {
		this.bugDescription = bugDescription;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getObject() {
		return object;
	}
	
	public void setObject(String object) {
		this.object = object;
	}
	
	/**
	 * Adds a bug in BugReport table
	 * 
	 * @param entityManager
	 * @param mail
	 * @param login
	 * @param object
	 * @param description
	 */
	public static void addBug(EntityManager entityManager, String mail, String login, String object, String description){
		entityManager.getTransaction().begin();
		BugReport br = new BugReport(login, mail, object, description);
		entityManager.persist(br);
		entityManager.getTransaction().commit();
	}
	
	
	/**
	 * Retrieves all bugs from database
	 * 
	 * @param entityManager
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public static List<BugReport> getAllBugs(EntityManager entityManager) {
        try {
            Query query = entityManager.createQuery("SELECT br FROM BugReport br");
            List<BugReport> bugs = (List<BugReport>) query.getResultList();
            return bugs;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
