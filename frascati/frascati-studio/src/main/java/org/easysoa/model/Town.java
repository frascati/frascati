/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto, Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.model;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.persistence.*;

/**
 * Persistence layer to manage the entity Town
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity(name="Town")
public class Town implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @XmlAttribute(name = "id")
    protected Long id;
    @XmlAttribute(name = "country")
    protected Country country;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "population")
    protected int population;
    @XmlAttribute(name = "latitude")
    protected double latitude;
    @XmlAttribute(name = "longitude")
    protected double longitude;

    public Town() {
    }

    public Town(Country country, String name, double latitude, double longitude) {
        this.country = country;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId(){
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopulation() {
		return population;
	}
    
    public void setPopulation(int population) {
		this.population = population;
	}
    
    /**
     * Returns a string which contains the available towns in the country and starting with the countryName passed in param
     * 
     * @param entityManager the entityManager
     * @param townName the beginning of a town name 
     * @param countryId the country id. ex : FR,DE,BR...
     * @return a string formatted for the jquery plug-in autocomplete. The string format is [townName1,townName2...]
     */
    public static String getTownSuggestions(EntityManager entityManager, String townName, String countryId) {
        Query query = entityManager
                .createQuery("SELECT t.name FROM Town t WHERE t.name LIKE :name AND t.country.id = :country");
        query.setParameter("name", townName + "%");
        query.setParameter("country", countryId);
        java.util.List<String> search = query.getResultList();
        StringBuffer stringBuffer = new StringBuffer("[");
        for (int i = 0; i < search.size(); i++) {
            stringBuffer.append("\"" + search.get(i) + "\"");
            if (i != search.size() - 1) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
    
    /**
     * Gets all the countries available in the database
     * 
     * @param entityManager the entity manager
     * @return all the countries which are in the database
     */
    public static List<Country> getCountries(EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT c FROM Country c");
        java.util.List<Country> search = query.getResultList();
        if (search.isEmpty()) {
            return null;
        }
        return search;
    }
    
    /**
     * Returns a town which is in the country and which the name is the countryName passed in param
     * 
     * @param entityManager the entityManager
     * @param townName the town name 
     * @param countryId the country id. ex : FR,DE,BR...
     * @return the town found
     */
    public static Town searchTown(EntityManager entityManager, String townName, String countryId){
    	 Query query = entityManager
                 .createQuery("SELECT t FROM Town t WHERE t.name = :name AND t.country.id = :country");
         query.setParameter("name", townName);
         query.setParameter("country", countryId);
         try{
             Town town = (Town)query.getSingleResult();
             return town;
         }
         catch(NoResultException nre){
             return null;
         }
    }

   
}
