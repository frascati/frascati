/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Persistence layer to manage the entity FriendRequest
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FriendRequest")
@XmlRootElement(name = "FriendRequest")
@Entity(name="FriendRequest")
public class FriendRequest implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @XmlAttribute(name = "id", required = true)
    protected Long id;
    @XmlAttribute(name = "originUser", required = true)
    protected User originUser;
    @XmlAttribute(name = "targetUser", required = true)
    protected User targetUser;

    public FriendRequest() {
    }

    
    public FriendRequest(User originUser, User targerUser) {
        this.originUser = originUser;
        this.targetUser = targerUser;
    }

    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    

    @OneToOne
    public User getOriginUser() {
        return originUser;
    }

    public void setOriginUser(User originUser) {
        this.originUser = originUser;
    }

    @OneToOne
    public User getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(User targerUser) {
        this.targetUser = targerUser;
    }
    
    
    
}
