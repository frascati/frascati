/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.model;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Persistence layer to manage the entity DeploymentServer
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeploymentServer")
@XmlRootElement(name = "DeploymentServer")
@Entity
public class DeploymentServer {

    @XmlAttribute(name = "id", required = true)
    private Long id;
    @XmlAttribute(name = "url", required = true)
    private String url;

    public DeploymentServer() {
    }
    
    public DeploymentServer(String url) {
        this.url = url;
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * Retrieves all the Deployment Servers available in database
     * 
     * @param entityManager
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<DeploymentServer> getAllDeploymentServers(EntityManager entityManager) {
        List<DeploymentServer> deploymentServers = new ArrayList<DeploymentServer>();
        Query query = entityManager
                .createQuery("SELECT d FROM DeploymentServer d");
        deploymentServers = (List<DeploymentServer>)query.getResultList();
        return deploymentServers;
    }
    

    /**
     * Retrieves a DeploymentServer from a URL
     * 
     * @param entityManager
     * @param server
     * @return
     */
    public static DeploymentServer getDeploymentServers(EntityManager entityManager, String server) {
        try{
            Query query = entityManager
                    .createQuery("SELECT d FROM DeploymentServer d where d.url = :url");
            query.setParameter("url", server);
            DeploymentServer deploymentServers = (DeploymentServer)query.getSingleResult();
            return deploymentServers;
        }catch(NoResultException eNoResult){
            Logger.getLogger("EasySOALogger").info("No result found for " + server + ". A new register will be created for this server.");
            return null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }



}
