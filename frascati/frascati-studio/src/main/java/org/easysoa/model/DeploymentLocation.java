/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

/**
 * Persistence layer to manage the entity DeploymentLocation
 */
public class DeploymentLocation {

    private String name;
    private String url;
    private String webExplorerUrl;
   
    public DeploymentLocation(String name, String url, String webExplorerUrl) {
        this.name = name;
        this.url = url;
        this.webExplorerUrl = webExplorerUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getWebExplorerUrl() {
		return webExplorerUrl;
	}
    
    public void setWebExplorerUrl(String webExplorerUrl) {
		this.webExplorerUrl = webExplorerUrl;
	}
}
