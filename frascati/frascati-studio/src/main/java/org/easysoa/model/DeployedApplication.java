/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Persistence layer to manage the entity Deployed Application (User applications)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeployedApplication")
@XmlRootElement(name = "DeployedApplication")
@Entity
public class DeployedApplication {

    @XmlAttribute(name = "id", required = true)
    private Long id;
    @XmlAttribute(name = "deploymentServer", required = true)
    private DeploymentServer deploymentServer;
    @XmlAttribute(name = "application")
    private Application application;
    @XmlAttribute(name = "version", required = true)
    private Long version;
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    public Application getApplication() {
        return application;
    }
    
    public void setApplication(Application application) {
        this.application = application;
    }
    
    @ManyToOne
    public DeploymentServer getDeploymentServer() {
        return deploymentServer;
    }

    public void setDeploymentServer(DeploymentServer deploymentServer) {
        this.deploymentServer = deploymentServer;
    }
    
    @Column(nullable = false)
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
    

    /**
     * Removes a deployed application
     * 
     * @param entityManager
     * @param appId
     */
	@SuppressWarnings("unchecked")
    public static void removeDeployedApplication(EntityManager entityManager, Long appId) {
		Query query = entityManager.createQuery("SELECT da FROM DeployedApplication da where da.application.id = :id");
		query.setParameter("id", appId);
        List<DeployedApplication> deployedapplications = (List<DeployedApplication>) query.getResultList();
        
        
        for(DeployedApplication application : deployedapplications){
        	entityManager.remove(application);
        }
	}
	
	/**
	 * Registers a deployed application in database
	 * 
	 * @param entityManager
	 * @param application
	 * @param deploymentServer
	 */
    @SuppressWarnings("unchecked")
    public static void registerDeployedApplication(EntityManager entityManager, Application application, DeploymentServer deploymentServer){
        try{
            Query query = entityManager
                    .createQuery("SELECT d FROM DeployedApplication d where application = :app and deploymentServer = :server");
            query.setParameter("app", application);
            query.setParameter("server", deploymentServer);
            List<DeployedApplication> resultQuery = query.getResultList();
            
            DeployedApplication deployedApplication = null;
            if( !resultQuery.isEmpty() ) {
                deployedApplication = resultQuery.get(0);

            }else{
                deployedApplication = new DeployedApplication();
                deployedApplication.setApplication(application);
                deployedApplication.setDeploymentServer(deploymentServer);
            }
            deployedApplication.setVersion(application.getCurrentVersion());
            entityManager.persist(deployedApplication);
        }catch(Exception e){
            Logger.getLogger("EasySOALogger").severe(e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Retrieves all deployed applications in specified deployment server
     * 
     * @param entityManager
     * @param server
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<DeployedApplication> getDeployedApplications(EntityManager entityManager, DeploymentServer server) {
        try{
            Query query = entityManager
                    .createQuery("SELECT d " +
                            "FROM DeployedApplication d " +
                            "WHERE deploymentServer = :server");
            query.setParameter("server", server);
            List<DeployedApplication> deployedApplications = query.getResultList();
            return deployedApplications;
        }catch(Exception e){
            Logger.getLogger("EasySOALogger").severe(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Retrieves the deployed application in specified deployment server
     * 
     * @param entityManager
     * @param application
     * @param server
     * @return
     */
    public static DeployedApplication getDeployedApplication(EntityManager entityManager, DeploymentServer server, Application application) {
        try{
            Query query = entityManager
                    .createQuery("SELECT d " +
                            "FROM DeployedApplication d " +
                            "WHERE deploymentServer = :server and " +
                            "   application = :application");
            query.setParameter("server", server);
            query.setParameter("application", application);
            DeployedApplication deployedApplication = (DeployedApplication) query.getSingleResult();
            return deployedApplication;
        }catch(Exception e){
            Logger.getLogger("EasySOALogger").severe(e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    

    /**
     * Removes the "deployed" status from an application in a deployment server
     * 
     * @param entityManager
     * @param application
     * @param deploymentServer
     */
    public static void deleteDeployedApplication(EntityManager entityManager, Application application, DeploymentServer deploymentServer){
        try{
            Query query = entityManager
                    .createQuery("DELETE FROM DeployedApplication where application = :app and deploymentServer = :server");
            query.setParameter("app", application);
            query.setParameter("server", deploymentServer);
            query.executeUpdate();

        }catch(Exception e){
            Logger.getLogger("EasySOALogger").severe(e.getMessage());
            e.printStackTrace();
        }
    }
    
}
