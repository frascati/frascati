/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

/**
 * Role is needed to authorize the access to the admin interface
 *
 */
public enum Role {
    ADMIN("Admin"),
    USER("User");
    
    private final String value;
    
    private Role(String value){
        this.value = value;
    }
    
    public String value() {
        return value;
    }
    
    /**
     * Returns a role from the role value
     * 
     * @param value the role value
     * @return the Role
     */
    public static Role fromValue(String value) {
        if("Admin".equals(value)){
            return Role.ADMIN;
        }
        if("User".equals(value)){
            return Role.USER;
        }
        throw new IllegalArgumentException(value);
    }
}
