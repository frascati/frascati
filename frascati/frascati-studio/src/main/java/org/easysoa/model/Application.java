/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.model;

import java.io.File;
import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * Persistence layer to manage the entity Application (User applications)
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Application")
@XmlRootElement(name = "Application")
@Entity(name="Application")
public class Application
    implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @XmlAttribute(name = "id", required = true)
    protected Long id;
    @XmlAttribute(name = "compositeLocation", required = true)
    protected String compositeLocation;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "description", required = true)
    protected String description;
    @XmlAttribute(name = "origin")
    @XmlSchemaType(name = "anyURI")
    protected Application origin;
    @XmlAttribute(name = "sources", required = true)
    protected String sources;
    @XmlAttribute(name = "resources", required = true)
    protected String resources;
    @XmlAttribute(name = "packageName")
    protected String packageName;
    @XmlAttribute(name = "root", required = true)
    protected String root;
    @XmlAttribute(name = "currentVersion", required = true)
    protected Long currentVersion = (long)0;
    

    /** Logger */
    public final static Logger LOG = Logger.getLogger(Application.class.getCanonicalName());
    
    /**
     * This attribute is not saved in database.
     *   It's used only to inform where is the global lib path used by this application
     */
    protected String globalLibPath = "";
    
    /**
     * Transient attribute to global lib path (libraries which can be used by all user pplications in the studio)  
     * 
     * @return
     */
    @Transient
    public String getGlobalLibPath() {
        if (globalLibPath.equals("") ) {
            LOG.warning("Global Lib Path is not defined for the application " + name);
            return "";
        }else{
            return globalLibPath + File.separator;
        }
        
    }

    public void setGlobalLibPath(String globalLibPath) {
        this.globalLibPath = globalLibPath;
    }
    
    /**
     * This attribute is not saved in database.
     *   It's used only to inform where is the local lib path used by this application
     */
    protected String localLibPath = "";
    
    /**
     * Transient attribute to local lib path (libraries which can be used by all user pplications in the studio)  
     * 
     * @return
     */
    @Transient
    public String getLocalLibPath() {
        if (localLibPath.equals("") ) {
            LOG.warning("Local Lib Path is not defined for the application " + name);
            return "";
        }else{
            return localLibPath + File.separator;
        }
        
    }

    public void setLocalLibPath(String localLibPath) {
        this.localLibPath = localLibPath;
    }
    
    /**
     * This attribute is not saved in database.
     *   It's used only to inform where is the current path used by this application
     */
    protected String currentWorskpacePath = "";
    @Transient    
    public String getCurrentWorskpacePath() {
        if (currentWorskpacePath.equals("") ) {
            LOG.warning("Current Workspace Path is not defined for the application " + name);
            return "";
        }else{
            return currentWorskpacePath + File.separator;
        }
    }

    public void setCurrentWorskpacePath(String currentWorskpacePath) {
        this.currentWorskpacePath = currentWorskpacePath;
    }
    
    /*
     * Database attributes
     */

    public String getRoot() {
        return root;
    }
    
    /**
     * Gets the absolute root path
     * 
     * @return
     */
    public String retrieveAbsoluteRoot() {
        return getCurrentWorskpacePath() + root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getSources() {
        return sources;
    }
    
    /**
     * Gets the absolute sources path
     * 
     * @return
     */
    public String retrieveAbsoluteSources() {
        return getCurrentWorskpacePath() + sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

    public String getResources() {
        return resources;
    }
    
    /**
     * Gets the absolute resources path
     * 
     * @return
     */
    public String retrieveAbsoluteResources() {
        return getCurrentWorskpacePath() + resources;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId(){
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(unique=true, nullable=false) 
    public String getCompositeLocation() {
        return compositeLocation;
    }
    
    /**
     * Gets the absolute composite location
     * 
     * @return
     */
    public String retrieveAbsoluteCompositeLocation() {
        return getCurrentWorskpacePath() + compositeLocation;
    }

    public void setCompositeLocation(String value) {
        this.compositeLocation = value;
    }

    @OneToOne
    public Application getOrigin() {
        return origin;
    }

    public void setOrigin(Application application) {
        this.origin = application;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageName() {
        return packageName;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    
    @Column(nullable = false)
    public Long getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(Long currentVersion) {
        this.currentVersion = currentVersion;
    }

    /**
     * Increment the application version doing some validations before 
     * 
     */
    public void incrementVersion() {
        Long currentVersion = getCurrentVersion();
        
        if (currentVersion == null) {
            currentVersion = (long) 0;
        }
        currentVersion ++;
        
        setCurrentVersion(currentVersion);
        
    }


}
