/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

import java.util.List;

import org.easysoa.reconfiguration.update.Update;
import org.eclipse.stp.sca.Composite;

/**
 * Manage data related to a deployment
 */
public class Deployment {

    String targetServer;
    
    /**
     * Previous Model Version to be compared with the current version and extracted the updates.<br>
     * If the application is not yet deployed in this server, <b>previousModelVersion</b> is <i>null</i>
     */
    Composite previousModelVersion;
    boolean isFirstDeployment;
    
    /**
     * List of updates to be done with this application in this server.<br>
     * Examples: AddComponent, AddWire, RemoveComponent etc.
     */
    List<Update> updates;
    
    
    /*
     * Getters and setters 
     */
    
    public String getTargetServer() {
        return targetServer;
    }

    public void setTargetServer(String targetServer) {
        this.targetServer = targetServer;
    }

    public Composite getPreviousModelVersion() {
        return previousModelVersion;
    }

    public void setPreviousModelVersion(Composite previousModelVersion) {
        this.previousModelVersion = previousModelVersion;
    }

    public boolean isFirstDeployment() {
        return isFirstDeployment;
    }

    public void setFirstDeployment(boolean isFirstDeployment) {
        this.isFirstDeployment = isFirstDeployment;
    }

    public List<Update> getUpdates() {
        return updates;
    }

    public void setUpdates(List<Update> updates) {
        this.updates = updates;
    }
}
