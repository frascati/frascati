/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.persistence.*;

/**
 * Persistence layer to manage the entity User (AppUser in database)
 */
@SuppressWarnings("serial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "User")
@XmlRootElement(name = "User")
@Entity(name = "AppUser")
public class User implements Serializable {
    @XmlAttribute(name = "id", required = true)
    protected Long userId;
    @XmlAttribute(name = "role", required = true)
    protected Role role;
    @XmlAttribute(name = "login", required = true)
    protected String login;
    @XmlAttribute(name = "username")
    protected String username;
    @XmlAttribute(name = "surname")
    protected String surname;
    @XmlAttribute(name = "password")
    protected String password;
    @XmlAttribute(name = "mail", required = true)
    protected String mail;
    @XmlAttribute(name = "civility")
    protected Civility civility;
    @XmlAttribute(name = "town")
    protected Town town;
    @XmlAttribute(name = "friends")
    protected List<User> friends;
    @XmlAttribute(name = "birthday")
    protected String birthday;
    @XmlAttribute(name = "workspaceUrl")
    protected String workspaceUrl;
    @XmlAttribute(name = "providedApplication")
    protected List<Application> providedApplications;
    @XmlAttribute(name="socialNetwork")
    protected SocialNetwork socialNetwork;
    @XmlAttribute(name="socialNetworkId")
    protected String socialNetworkId;
    /*
     * @XmlAttribute(name = "consumedApplication") protected List<Application>
     * consumedApplications;
     */
    @XmlAttribute(name = "friendRequests")
    protected List<FriendRequest> friendRequests;

    public User() {
    	this.friendRequests = new ArrayList<FriendRequest>();
        this.role = Role.USER;
        this.friends = new ArrayList<User>();
        this.providedApplications = new ArrayList<Application>();
    }

    public User(String login, String username, String surname, String password,
            String mail, Civility civility, Town town, String birthday, String socialNetworkId, SocialNetwork socialNetwork) {
    	this();
        this.login = login;
        this.username = username;
        this.surname = surname;
        this.password = password;
        this.mail = mail;
        this.civility = civility;
        this.town = town;
        this.birthday = birthday;
        this.socialNetworkId = socialNetworkId;
        this.socialNetwork = socialNetwork;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return this.userId;
    }

    @Enumerated(EnumType.STRING)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }

    public void setId(long userId) {
        this.userId = userId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserName() {
        return username;
    }

    public void setUserName(String value) {
        this.username = value;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String value) {
        this.surname = value;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String value) {
        this.mail = value;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility value) {
        this.civility = value;
    }

    @ManyToOne
    public Town getTown() {
        return town;
    }

    public void setTown(Town value) {
        this.town = value;
    }

    @OneToMany
    public List<User> getFriends() {
        if (friends == null) {
            friends = new ArrayList<User>();
        }
        return this.friends;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getWorkspaceUrl() {
        return workspaceUrl;
    }

    public void setWorkspaceUrl(String workspaceUrl) {
        this.workspaceUrl = workspaceUrl;
    }

    @OneToMany
    public List<Application> getProvidedApplications() {
        if (providedApplications == null) {
            providedApplications = new ArrayList<Application>();
        }
        return this.providedApplications;
    }

    public void setProvidedApplications(List<Application> providedApplications) {
        this.providedApplications = providedApplications;
    }

    /*
     * @OneToMany public List<Application> getConsumedApplications() { if
     * (consumedApplications == null) { consumedApplications = new
     * ArrayList<Application>(); } return this.consumedApplications; }
     */

    public String getSocialNetworkId() {
		return socialNetworkId;
	}
    
    public void setSocialNetworkId(String socialNetworkId) {
		this.socialNetworkId = socialNetworkId;
	}
    
    public SocialNetwork getSocialNetwork() {
		return socialNetwork;
	}
    
    public void setSocialNetwork(SocialNetwork socialNetwork) {
		this.socialNetwork = socialNetwork;
	}
    
    @ManyToMany
    public List<FriendRequest> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<FriendRequest> friendRequests) {
        this.friendRequests = friendRequests;
    }

    /**
     * Retrieves all users from database
     * 
     * @param entityManager
     * @return
     */
    @SuppressWarnings("unchecked")
    public static List<User> getAllUsers(EntityManager entityManager) {
        try {
            Query query = entityManager.createQuery("SELECT u FROM AppUser u");
            List<User> users = (List<User>) query.getResultList();
            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Retrieves an user by login
     * 
     * @param entityManager
     * @param login
     * @return
     */
    public static User getUser(EntityManager entityManager,String login) {
        try {
            Query query = entityManager.createQuery("SELECT u FROM AppUser u where u.login = :login");
            query.setParameter("login", login);
            User user = (User) query.getSingleResult();
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
