/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */
package org.easysoa.model;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Model class corresponding to <b>Preference</b> Table
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Preference")
@XmlRootElement(name = "Preference")
@Entity(name = "Preference")
public class Preference implements Serializable {

    private static final long serialVersionUID = 1L;
    @XmlAttribute(name = "id", required = true)
    protected Integer id;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "value" , required = true)
    protected String value;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(Preference.class.getCanonicalName());
    
    /**
     * Preference's Constructor
     * 
     * @param name - The name identifying the preference 
     * @param value - Preference's value
     */
    public Preference(String name, String value){
        this.name = name;
        this.value = value;
    }

    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Column(unique = true, nullable = false)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(nullable = false)
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    /**
     * Retrieves the value of a preference 
     * 
     * @param entityManager - The entity manager used to connect the database
     * @param preferenceName - The name of the preference
     * @return The preference value
     */
    public static String searchPreference(EntityManager entityManager, String preferenceName){
           Query query = entityManager
                .createQuery("SELECT p.value FROM Preference p WHERE p.name = :name");
        query.setParameter("name", preferenceName);
        try{
            String preferenceValue = (String)query.getSingleResult();
            return preferenceValue;
        }catch(NoResultException nre){
            LOG.severe("No preference found for " + preferenceName + "!" );
            return null;
        }catch(Exception e){
            LOG.severe("Error trying to retrive preference for " + preferenceName + "!" );
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Update a preference with a new value
     * 
     * @param entityManager - The entity manager used to connect the database
     * @param preferenceName - The name of the preference to be updated
     * @param preferenceValue - The new preference value 
     */
    public static void updatePreferenceValue(EntityManager entityManager, String preferenceName, String preferenceValue){
        Query query = entityManager
            .createQuery("UPDATE Preference SET value = :value WHERE name = :name");
        query.setParameter("name", preferenceName);
        query.setParameter("value", preferenceValue);
        try{
            int updatedCount = query.executeUpdate();
            LOG.info(updatedCount + " records updated in Preference table." );
        }catch(Exception e){
            LOG.severe("Error trying to update preference for " + preferenceName + "!" );
                e.printStackTrace();
        }
    }
    
}
