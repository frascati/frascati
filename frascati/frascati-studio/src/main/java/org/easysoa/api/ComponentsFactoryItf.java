/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.api;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCABinding;
import org.eclipse.stp.sca.WebServiceBinding;
import org.eclipse.stp.sca.domainmodel.frascati.RestBinding;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.metamodel.web.HttpBinding;
import org.ow2.frascati.metamodel.web.VelocityImplementation;

/**
 * Factory for EMF model elements 
 *
 */
@Service
public interface ComponentsFactoryItf {

    /**
     * Creates an instance of a Composite
     */
    Composite createComposite();
    
    /**
     * Creates an instance of a Component
     */
    Component createComponent();
    
    /**
     * Creates an instance of a Component service
     */
    ComponentService createComponentService();
    
    /**
     * Creates an instance of a Component reference
     */
    ComponentReference createComponentReference();
    
    /**
     * Creates an instance of a Property value
     */
    PropertyValue createPropertyValue();
    
    /**
     * Creates an instance of a Service
     */
    org.eclipse.stp.sca.Service createService();
    
    /**
     * Creates an instance of a Reference
     */
    Reference createReference();
    
    /**
     * Creates an instance of a Http binding
     */
    HttpBinding createHttpBinding();
    
    /**
     * Creates an instance of a Java implementation
     */
    JavaImplementation createJavaImplementation();
    
    /**
     * Creates an instance of a Velocity implementation
     */
    VelocityImplementation createVelocityImplementation();
    
    /**
     * Creates an instance of a Java interface
     */
    JavaInterface createJavaInterface();
    
    /**
     * Creates an instance of a Rest binding
     */
    RestBinding createRestBinding();
    
    /**
     * Creates an instance of a SCA binding
     */
    SCABinding createScaBinding();
    
    /**
     * Creates an instance of a Web service binding
     */
    WebServiceBinding createWebServiceBinding();

    
}
