/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.api;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import org.eclipse.emf.ecore.EObject;
import org.osoa.sca.annotations.Service;

/**
 * Manage actions related to the File System
 */
@Service
public interface FileManager {
    
    /**
     * Save Composite file.
     * 
     * @param compositeObject
     * @param path
     * @return success (true) or fail (false)
     */
    boolean saveComposite(EObject compositeObject , String path);

    /**
     * Copy recursively all files in <b>sourceFile</b> from <b>sourcePath</b> to <b>targetPath</b>
     *    that respects the filter <b>fileFilter</b> (optional)
     * 
     * @param sourceFile
     * @param sourcePath
     * @param targetPath
     * @param fileFilter
     * @throws IOException 
     */
    void copyFilesRecursively(File sourceFile, File sourcePath, File targetPath, FileFilter fileFilter) throws IOException;

    /**
     * Delete recursively all files in <b>src</b>
     * 
     * @param src
     * @throws IOException
     */
    void deleteRecursively(File src) throws IOException;

    
    /**
     * Copy recursively all files in <b>sourceFile</b> from <b>sourcePath</b> to <b>targetPath</b>
     * 
     * @param sourceFile
     * @param sourcePath
     * @param targetPath
     * @throws IOException 
     */
    void copyFilesRecursively(File sourceFile, File sourcePath, File targetPath) throws IOException;
    
}
