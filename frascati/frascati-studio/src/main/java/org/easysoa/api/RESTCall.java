/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.easysoa.model.Application;
import org.osoa.sca.annotations.Service;



@Service
public interface RESTCall {
    
  @POST
  @Path("/addElement")
  @Produces("text/plain")
  void addElement(@FormParam("userId")String userId, @FormParam("elementId")String elementId,@FormParam("action")String action);
  
  @POST
  @Path("/saveElement")
  @Produces("text/plain")
  void saveElement(@FormParam("userId")String userId, @FormParam("params")String params);
  
  @GET
  @Path("/implementationContent")
  @Produces("text/plain")
  String getImplementationContent(@FormParam("userId")String userId, @FormParam("modelId")String modelId,@FormParam("elementId")String elementId);
  
  @GET
  @Path("/interfaceContent")
  @Produces("text/plain")
  String getInterfaceContent(@FormParam("userId")String userId, @FormParam("modelId")String modelId, @FormParam("elementId")String elementId);
  
  @GET
  @Path("/bindingContent")
  @Produces("text/plain")
  String getBindingContent(@FormParam("userId")String userId, @FormParam("modelId")String modelId, @FormParam("elementId")String elementId);
  
  @GET
  @Path("/fileContent")
  @Produces("text/plain")
  String getFileContent(@FormParam("type")String type);

  @GET
  @Path("/editorMode")
  @Produces("text/plain")
  String getEditorMode(@FormParam("type")String type);
  
  @POST
  @Path("/saveFileContent")
  @Produces("text/plain")
  void saveFileContent(@FormParam("content")String content, @FormParam("type")String type);
  
  @GET
  @Path("/existingImplementations")
  @Produces("text/plain")
  String getExistingImplementations(@FormParam("userId")String userId);

  @POST
  @Path("/createNewImplementation")
  @Produces("text/plain")
  void createNewImplementation(@FormParam("userId")String userId, @FormParam("elementId")String elementId, @FormParam("className")String className, @FormParam("implemType")String implemType, @FormParam("createFile")boolean createFile);
  
  @POST
  @Path("/createNewInterface")
  @Produces("text/plain")
  void createNewInterface(@FormParam("userId")String userId, @FormParam("elementId")String elementId, @FormParam("className")String className, @FormParam("interfaceType")String interfaceType,@FormParam("createFile")boolean createFile,@FormParam("choice")String choice);

  @POST
  @Path("/createNewBinding")
  @Produces("text/plain")
  void createNewBinding(@FormParam("userId")String userId, @FormParam("elementId")String elementId, @FormParam("bindingType")String bindingType, @FormParam("uri")String bindingUri);

  @GET
  @Path("/isExistingTown")
  @Produces("text/plain")
  boolean isExistingTown(@FormParam("town")String town,@FormParam("country")String country);
  
  @GET
  @Path("/hasExistingImplementation")
  @Produces("text/plain")
  boolean hasAnExistingImplementation(@FormParam("userId")String userId,@FormParam("elementId")String elementId);

  @GET
  @Path("/hasExistingInterface")
  @Produces("text/plain")
  boolean hasAnExistingInterface(@FormParam("userId")String userId,@FormParam("elementId")String elementId);
  
  @POST
  @Path("/addIntent")
  @Produces("text/plain")
  void addIntent(@FormParam("application")Application application,@FormParam("userId")String userId, @FormParam("elementId")String elementId, @FormParam("intentName")String intentName);
  
  @GET
  @Path("/intentImplementation")
  @Produces("text/plain")
  String getIntentImplementation(@FormParam("name")String name,@FormParam("userId")String userId);

  @POST
  @Path("/saveIntent")
  @Produces("text/plain")
  void saveIntent(@FormParam("name")String name, @FormParam("userId")String userId, @FormParam("content")String content);
}
