/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.api;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Service;


@Service
public interface ComplexProcessorItf{
    
    String getId();
    
    String getLabel(EObject eObject);
    
    JSONObject getMenuItem(EObject eObject, String parentId);
    
    String getPanel(String userId, EObject eObject);
    
    String getActionMenu(EObject eObject);
    
    EObject saveElement(EObject eObject, Map<String, Object> params);
    
    EObject getNewEObject(EObject eObject);
}
