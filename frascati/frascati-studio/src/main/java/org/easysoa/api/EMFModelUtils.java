/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.api;

import org.easysoa.model.Application;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Service;

/**
 * API to manage EMF models for Service Component Architecture  
 */
@Service
public interface EMFModelUtils {

	/**
	 * Return the eobject identified by the id passed in params
	 * @param userId the userId to get the eobject on the model he's manipulating
	 * @param elementId the id of the component
	 * (example component name reference name binding)
	 * @return the eobject found 
	 */
	EObject getComponent(String userId, String elementId);
	
	/**
	 * Add a new element in the model
	 * @param userId the userId to get the model he's manipulating
	 * @param elementId the id of the element where the element will be added
	 * @param action represents the element which will be added
	 * the available actions are : 
	 *  - addComponent
	 *  - addService
	 *  - addReference
	 *  - addComponentService
	 *  - addComponentReference
	 *  - addComponentProperty
	 *  - addImplementation
	 *  - addInterface
	 *  - addBinding
	 *  - and the same actions in replacing add by delete
	 */
	void addElement(String userId, String elementId, String action);
	
	/**
	 * test if an implementation exists on the element identified by the id
	 * @param userId the userId to get the model he's manipulating
	 * @param elementId the id of the element where the test is done
	 * @return true if an implementation exists, false otherwise
	 */
	boolean hasAnImplementation(String userId, String elementId);

	/**
	 * test if an interface exists on the element identified by the id
	 * @param userId the userId to get the model he's manipulating
	 * @param elementId the id of the element where the test is done
	 * @return true if an interface exists, false otherwise
	 */
	boolean hasAnInterface(String userId, String elementId);

	/**
	 * Add an intent on the element identified by the id
	 * @param userId the userId to get the model he's manipulating
	 * @param elementId the id of the element where the addition is done
	 * @param intentName the intent's name
	 */
	EObject addIntent(String userId, String elementId, String intentName);
	
	/**
	 * Parse application composite file to get the Composite object
	 * @param application
	 * @return composite object
	 */
	Composite getCompositeFromApplication(Application application);

	/**
	 * Get the composite object contains in application
	 * @param application
	 * @param compositeName
	 * @return composite object
	 */
	Composite getIntentComposite(Application application, String compositeName);
}
