/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.api;

import java.util.List;
import java.util.Map;

import org.easysoa.model.Application;
import org.easysoa.model.User;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Service;

/**
 * Service Manager
 */
@Service
public interface ServiceManager {
    
    /**
     * To be used in methods where data of some application is accessed
     *      (for example, reloadComposite()).
     * 0 corresponds to the current version.
     */
    public final static Long CURRENT_VERSION = (long) 0;

	/**
	 * Create a new application for the user passed in param
	 * @param user the user who wants to create a new application
	 * @param name the application's name
	 * @param description a short description of the application
	 * @param packageName a package for the application (optional)
	 * @param templateName the template used for the creation
	 * @param params the params needed by the template
	 * @return a String which can contains a message
	 */
    String createApplication(User user,String name, String description, String packageName, String templateName, Map<String, Object> params) throws Exception;

    /**
     * Search the composite file of the application where the name is name and the creator is user
     * @param name the application's name
     * @param user the user who have created the application
     * @return the composite found
     */
    Composite searchComposite(String name, User user);

    /**
     * save the file edited
     * @param fileUrl the file's url
     * @param fileContent the file's content
     */
    void saveFile(String fileUrl,String fileContent);

    /**
     * Delete application
     * @param user the user who have created the application
     * @param name the application's name
     * @return the modified user
     */
    User deleteApplication(User user,String name);

    /**
     * Search created applications by keywords
     * @param keywords the keywords to find applications
     * @return a list of application which have been found
     */
    List<Application> searchApplications(String keywords);
    
    /**
     * Reload the composite used by the user
     * @param userId the user who is using the application
     * @param version the composite version to be loaded
     * @return The composite model object
     */
    Composite reloadComposite(String userId, Long version);

    /**
     * Give the current composite manipulated by the user
     * @param userId the user who wants to get its composite
     * @return the composite
     */
    Composite getComposite(String userId);
    
    /**
     * Set the current composite manipulated by the user
     * @param userId the user who changes its current composite
     * @param composite the new composite
     */
    void setComposite(String userId, Composite composite);
    
    /**
     * Give the current application used by the user
     * @param userId the user
     * @return
     */
    Application getCurrentApplication(String userId);

    /**
     * search in the application directories if the file is in
     * @param file the file name as describe in the composite file
     * @return the url if exists
     */
    String isFileInApplication(String userId, String file);

    /**
     * get all the target available for a reference
     * @return a list of the available targets
     */
    List<String> getAllTarget(String userId);

    /**
     * Create class at the good location according as the type is a script or other source file
     * @param type must be Script, Implementation
     * @param fileName 
     */
    void createFile(String userId, String type, String fileName, String apiOrImpl);

    /**
     * Change the package of the uploaded file
     * @param userId the user who upload a file
     * @param implemType the implementation type
     * @param className the className
     * @param apiOrImpl permits to know if the file is an interface or implementation
     */
    void changePackage(String userId, String implemType, String className, String apiOrImpl);
    
    /**
     * Give the package's mirror of the application the user is manipulating
     * @param userId the user
     * @return the package's mirror
     */
    String getPackageMirror(String userId);

}
