/**
 * OW2 FraSCAti
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s):
 *
 */
package org.easysoa.api;

import java.util.List;

import org.easysoa.model.Country;
import org.osoa.sca.annotations.Service;

/**
 * Interface to access database tables
 */
@Service
public interface ModelAccess{
    
    /**
     * Retrieves all the cities starting with <b>townName</b> and having the exact <b>countryName</b>
     * 
     * @param townName
     * @param countryName
     * @return The name of the suggested city
     */
    String getTownSuggestions(String townName, String countryName);
    
    /**
     * Retrieves the list of all the countries
     * 
     * @return List of countries
     */
    List<Country> getCountries();
    
    /**
     * Checks if a town exists
     * 
     * @param town
     * @param country
     * @return
     */
    boolean isTownExists(String town, String country);
    
    /**
     * Adds a bug register
     * 
     * @param login
     * @param mail
     * @param object
     * @param description
     */
    void addBug(String login, String mail, String object, String description);
}
