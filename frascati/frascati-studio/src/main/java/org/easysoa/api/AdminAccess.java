/**
 * OW2 FraSCAti
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.api;

import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.easysoa.model.DeploymentServer;
import org.osoa.sca.annotations.Service;

/**
 * Manage Administrator features
 * 
 */
@Service
public interface AdminAccess {

    /**
     * Retrieves all users in JSON format
     * 
     * @return
     */
    @GET
    @Path("/allUsersJSON")
    @Produces("text/plain")
    String getAllUsersJSON();
    
    /**
     * Retrieves an user in JSON format
     * 
     * @param userId
     * @return String - user in JSON format
     */
    @GET
    @Path("/user")
    @Produces("text/plain")
    String getUser(@FormParam("userId")String userId);
    
    /**
     * Sets the role of an user (admin or user)
     * 
     * @param userId
     * @param isAdmin - New role (admin or user) to be set in referred user
     */
    @POST
    @Path("/changeAdmin")
    @Produces("text/plain")
    void changeAdmin(@FormParam("userId")String userId,@FormParam("admin")boolean isAdmin);
    
    /**
     * Retrieves all deployed applications in JSON format
     * 
     * @return String - applications in JSON format
     */
    @GET
    @Path("/allDeployedAppJSON")
    @Produces("text/plain")
    String getAllDeployedAppJSON();
    
    /**
     * Retrieves all Deployment Servers
     * 
     * @return A list of DeploymentServer
     */
    @GET
    @Path("/allDeployedAppObjects")
    @Produces("text/plain")
    List<DeploymentServer> getAllDeployedAppObjects();
    
    /**
     * Retrieves all Deployment Servers in JASON format
     * 
     * @return String - Deployment Servers in JSON format
     */
    @GET
    @Path("/allDeployedAppLocations")
    @Produces("text/plain")
    String getAllDeployedAppLocations();
    
    /**
     * Retrieves a Deployment Server by URL
     * 
     * @param cloud - Server URL
     * @return a Deployment Server
     */
    @GET
    @Path("/getDeploymentServer")
    @Produces("text/plain")
    DeploymentServer getDeploymentServer(@FormParam("cloud")String cloud);
    
    /**
     * Redeploys all applications previously deployed in a Server and no more deployed.
     * Used when a server falls.
     * 
     * @param cloud - Server URL
     * @throws Exception
     */
    @POST
    @Path("/redeploy")
    @Produces("text/plain")
    void redeploy(@FormParam("cloud")String cloud) throws Exception;
    
    /**
     * Removes the deployed application from all the servers where it is deployed
     * 
     * @param appId - Application Id
     */
    @POST
    @Path("/delete")
    @Produces("text/plain")
    void removeDeployedApplication(@FormParam("appId")Long appId);
    
    /**
     * Get the list of all reported bugs in JSON format
     * 
     * @return String - bugs in JSON format
     */
    @GET
    @Path("/bugs")
    @Produces("text/plain")
    String getBugs();
}
