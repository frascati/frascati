/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.easysoa.api;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.SCABinding;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.WSDLPortType;
import org.eclipse.stp.sca.WebServiceBinding;
import org.eclipse.stp.sca.domainmodel.frascati.RestBinding;
import org.eclipse.stp.sca.domainmodel.frascati.ScriptImplementation;
import org.eclipse.stp.sca.domainmodel.tuscany.HTTPBinding;
import org.ow2.frascati.metamodel.web.VelocityImplementation;

public interface HTMLProcessorItf {

    String getErrorPanel();

	String getComponentPanel(Component component);
	
	String getComponentMenu();
	
	String getComponentReferencePanel(ComponentReference componentReference);
	
	String getComponentReferenceMenu();
	
	String getComponentServicePanel(ComponentService componentService);
	
	String getComponentServiceMenu();
	
	String getCompositePanel(Composite composite);
	
	String getCompositeMenu();
	
	String getHttpBindingPanel(HTTPBinding binding);
	
	String getHttpBindingMenu();
	
	String getJavaImplementationPanel(JavaImplementation implementation);
	
	String getJavaImplementationMenu();

	String getJavaInterfacePanel(JavaInterface javaInterface);
	
	String getJavaInterfaceMenu();
	
	String getPropertyPanel(PropertyValue propertyValue);
	
	String getPropertyMenu();
	
	String getReferencePanel(org.eclipse.stp.sca.Reference reference, String userId);
	
	String getReferenceMenu();
	
	String getRestBindingPanel(RestBinding binding);
	
	String getRestBindingMenu();
	
	String getScaBindingPanel(SCABinding binding);
	
	String getScaBindingMenu();
	
	String getScriptImplementationPanel(ScriptImplementation implementation);
	
	String getScriptImplementationMenu();
	
	String getServicePanel(Service service);
	
	String getServiceMenu(Service service);
	
	String getVelocityImplementationPanel(VelocityImplementation implementation);
	
	String getVelocityImplementationMenu();
	
	String getWebServiceBindingPanel(WebServiceBinding binding);
	
	String getWebServiceBindingMenu();
	
	String getWsdlInterfacePanel(WSDLPortType wsdlInterface);
	
	String getWdslInterfaceMenu();

	String getCompositeImplementationPanel(SCAImplementation implementation);
	
	String getCompositeImplementationMenu();

}
