/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.api;

import java.io.FileNotFoundException;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;

@Service
public interface TemplateRest {

	@GET
	@Path("/templateForm")
	@Produces("text/plain")
	String getTemplateForm(@FormParam("templateName")String templateName);
	  
	@POST
	@Path("/createApplication")
	@Produces("text/plain")
	String createApplication(@FormParam("params")String params);
	
	@GET
	@Path("/loadWSDL")
	@Produces("text/plain")
	String loadWSDL(@FormParam("wsdlPath")String wsdlPath) throws FileNotFoundException;
}
