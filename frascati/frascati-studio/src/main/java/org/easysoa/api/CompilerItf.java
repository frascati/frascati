/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s):
 *
 */

package org.easysoa.api;

import java.io.File;

import org.osoa.sca.annotations.Service;

/**
 * Interface to be implemented by a Java compiler for FraSCAti (JDK, maven, etc.)
 * 
 */
@Service
public interface CompilerItf {
    
    /**
     * Compile a SCA application
     * 
     * @param root - The complete application path
     * @param applicationName - The application name
     * @param globalLibPath - Global Lib path used in application class path
     * @param localLibPath - Local Lib path used in application class path
     * @return The contribution File (.zip) ready to deploy 
     * @throws Exception 
     */
    File compile(String root, String applicationName, String globalLibPath, String localLibPath) throws Exception;

}
