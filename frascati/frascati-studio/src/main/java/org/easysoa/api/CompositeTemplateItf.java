/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */

package org.easysoa.api;

import java.util.Map;

import org.osoa.sca.annotations.Service;

@Service
public interface CompositeTemplateItf {

	/**
	 * id for the template
	 * @return
	 */
    String getId();

    /**
     * Get the composite template
     * @param params form creation params
     * @return composite template
     */
    String getTemplate(Map<String, Object> params);
    
    /**
     * Label for the displaying in page
     * @return
     */
    String getLabel();
    
    /**
     * Html form for the creation
     * @return
     */
    String getForm();
    
}
