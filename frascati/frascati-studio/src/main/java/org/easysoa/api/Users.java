/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.api;

import org.easysoa.model.User;
import org.osoa.sca.annotations.Service;

/**
 * Manage users
 */
@Service
public interface Users {

    /**
     * Connect to FraSCAti Studio 
     * 
     * @param username
     * @param password
     * @return Current user, or null if connection fails
     */
    User connect(String username,String password);
    
    /**
     * Create new account with the given parameters
     * 
     * @param login
     * @param password
     * @param confirmPassword
     * @param mail
     * @param name
     * @param surname
     * @param civility
     * @param town
     * @param country
     * @param birthday
     * @return
     */
    User createAccount(String login, String password, String confirmPassword,
            String mail, String name, String surname, String civility, String town, String country, String birthday);
    
    /**
     * Search User with the corresponding <b>id</b>
     * 
     * @param userId
     * @return the user found
     */
    User searchUser(Long userId);
    
    /**
     * Search User with the corresponding <b>idString</b>
     * 
     * @param userIdString
     * @return the user found
     */
    User searchUser(String userIdString);
    
    /**
     * Test if an user exists with the given social id
     * @param socialId the social id (id of the social account)
     * @param network the id's network (Twitter, Facebook, Google)
     * @return true if the user exists, false otherwises
     */
    boolean isSocialUserExist(String socialId, String network);

    /**
     * Connect an user by socials informations 
     * @param socialId the id of the social account
     * @param network the id's network (Twitter, Facebook, Google)
     * @return
     */
	User connectBySocialId(String socialId, String network);

	/**
	 * Create new account from social network informations and account creation form
	 * @param user
	 * @param civility
	 * @param townName
	 * @param country
	 * @return the user 
	 */
	User createSocialAccount(User user, String civility, String townName,
			String country);
}
