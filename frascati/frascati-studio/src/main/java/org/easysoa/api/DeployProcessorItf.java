/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.api;

import java.util.List;

import org.easysoa.model.Application;
import org.easysoa.model.DeploymentLocation;

/**
 * Process actions related to deployment
 */
public interface DeployProcessorItf {

    /**
     * Retrieve the list of available deployments defined in src/main/resources/deploy.properties
     * 
     * @return
     */
    List<DeploymentLocation> getAvailableDeployments();
    
    /**
     * Deploy an <b>application</b> in all chosen servers 
     * 
     * @param ids - Array of Server URLs
     * @param application - Application to be deployed
     * @return Success (true) or Fail (false)
     * @throws Exception 
     * 
     * @see #undeploy(String[], Application)
     */
    boolean deploy(String ids[], Application application) throws Exception;

    /**
     * Undeploy an <b>application</b> in all chosen servers 
     * 
     * @param ids - Array of Server URLs
     * @param application - Application to be undeployed
     * @return Success (true) or Fail (false)
     * @throws Exception 
     * 
     * @see #deploy(String[], Application)
     */
    boolean undeploy(String[] ids, Application application) throws Exception;

    /**
     * Get in json all the url of the frascati web explorer relating to the deployment urls
     * @param urls the urls of the deployment component
     * @return a json representation of the urls
     */
    String getWebExplorerUrlsInJson(List<String> urls);
}
