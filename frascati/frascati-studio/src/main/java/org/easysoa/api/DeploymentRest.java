/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.api;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;

/**
 * Entry point to call actions related to Deployment
 * 
 */
@Service
public interface DeploymentRest {

    /**
     * Deploy the current application in the chosen servers.
     * This service must to be called by a client (javascript, velocity, etc.)
     * 
     * @param userId
     * @param ids
     * @return the message corresponding to the deployment result
     */
    @GET
    @Path("/deploy")
    @Produces("text/plain")
    String deploy(@FormParam("userId")String userId, @FormParam("ids")String ids);
	
    
    /**
     * Undeploy the current application in the chosen servers.
     * This service must to be called by a client (javascript, velocity, etc.)
     * 
     * @param userId
     * @param ids
     * @return the message corresponding to the undeployment result
     */
    @GET
    @Path("/undeploy")
    @Produces("text/plain")
    String undeploy(@FormParam("userId")String userId, @FormParam("ids")String ids);
    
    /**
     * Get in json all the url of the frascati web explorer relating to the deploying urls
     * @param urls the urls of deployment compoment
     * @return a json representation of the urls
     */
    @GET
    @Path("/webExplorerUrls")
    @Produces("text/plain")
    String getWebExplorerUrlsInJson(@FormParam("ids")String urls);
}
