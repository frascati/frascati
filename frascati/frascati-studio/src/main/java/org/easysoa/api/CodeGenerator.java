/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.api;

import java.io.FileNotFoundException;

import org.easysoa.utils.WsdlInformations;

/**
 * Code generator from WSDL
 */
public interface CodeGenerator {

	String getId();
	
    /**
     * Generate code
     * 
     * @param userId
     * @param service
     * @param port
     * @param wsdlLocation
     * @return
     */
    String generate(String userId, String service, String port,String wsdlLocation);

    /**
     * Load information for WSDL
     * 
     * @param wsdl
     * @return
     * @throws FileNotFoundException
     */
    String loadWsdlString(String wsdl) throws FileNotFoundException;
    
    /**
     * Return the line to add in the composite file
     * @param service 
     * @return
     */
    String getCompositeLineImplementation(String service);

    /**
     * Return WSDL informations concerning the provided wsdl
     * @param wsdl
     * @return
     * @throws FileNotFoundException
     */
    WsdlInformations loadWsdl(String wsdl) throws FileNotFoundException;
    
}
