/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.easysoa.api.MailService;
import org.easysoa.model.User;
import org.osoa.sca.annotations.Scope;

/**
 * E-mail Service
 */
@Scope("COMPOSITE")
public class MailServiceImpl implements MailService {

    /**
     * @see MailService#sendMailAccountCreation(User)
     */
    @Override
    public void sendMailAccountCreation(User user) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.inria.fr");
        Session session = Session.getDefaultInstance(props, null);
        try {
            Message mesg = new MimeMessage(session);
            mesg.setFrom(new InternetAddress("michel.dirix@inria.fr"));
            InternetAddress toAddress = new InternetAddress(user.getMail());
            mesg.addRecipient(Message.RecipientType.TO, toAddress);
            mesg.setSubject("Your account creation");
            StringBuilder body = new StringBuilder();
            body.append("Thanks for your subscription on easySOA WebSite\n");
            body.append("Your login is : ");
            body.append(user.getLogin());

            mesg.setText(body.toString());
            Transport.send(mesg);
        } catch (MessagingException ex) {
            while ((ex = (MessagingException) ex.getNextException()) != null) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @see MailService#sendMailForFriendRequest(User, User, InputStream)
     */
    @Override
    public void sendMailForFriendRequest(User originUser, User targetUser, InputStream inputStream) {
        try {

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            StringBuilder StringBuilder = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                if (line.contains(":targetFirstname")) {
                    line = line.replace(":targetFirstname", targetUser.getUserName());
                }
                if (line.contains(":originSurname")) {
                    line = line.replace(":originSurname", originUser.getSurname());
                }
                if (line.contains(":originFirstname")) {
                    line = line.replace(":originFirstname", originUser.getUserName());
                }
                StringBuilder.append(line + "\n");
            }
            bufferedReader.close();
            inputStream.close();

            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.inria.fr");
            Session session = Session.getDefaultInstance(props, null);
            Message mesg = new MimeMessage(session);
            mesg.setFrom(new InternetAddress("michel.dirix@inria.fr"));
            InternetAddress toAddress = new InternetAddress(
                    targetUser.getMail());
            mesg.addRecipient(Message.RecipientType.TO, toAddress);
            mesg.setSubject("Friend request");

            mesg.setText(StringBuilder.toString());
            Transport.send(mesg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   
}
