/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Marc Dutoo
 *
 * Contributor(s):
 *
 */
package org.easysoa.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.cxf.tools.common.ToolException;

/**
 * Helps managing URLs
 */
public class UrlUtils {

    /**
     * Checks it is an URL, else tries to complete it (ex. adds file:// or http://,
     * but without making it wrong) so it will be.
     * @param wsdlLocation
     * @return
     * @throws ToolException
     */
    // TODO maybe rather in js UI ?
    public static final String checkUrl(String wsdlLocation) throws ToolException {
        try {
            new URL(wsdlLocation);
        } catch (MalformedURLException muex) {
            
            // trying to patch URL
            // TODO rather / also in js UI ?
            String patchedWsdlLocation;
            if (wsdlLocation.startsWith("/")) {
                // making it easier for absolute files
                patchedWsdlLocation = "file://" + wsdlLocation;
            } else if (!wsdlLocation.contains(":/") && !wsdlLocation.startsWith("http://")) {
                // making it easier for http
                patchedWsdlLocation = "http://" + wsdlLocation;
                
            } else {
                throw new ToolException("wsdl2java : wsdlLocation should be an URL but is " + wsdlLocation);
            }

            // trying patched URL
            try {
                new URL(patchedWsdlLocation);
            } catch (MalformedURLException muex2) {
                throw new ToolException("wsdl2java : unable to make an URL out of wsdlLocation " + wsdlLocation);
            }
            wsdlLocation = patchedWsdlLocation;
        }
        return wsdlLocation;
    }

}
