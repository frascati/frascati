/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manipulates WSDL informations
 */
public class WsdlInformations {

    private String targetNameSpace;
    private List<String> servicesPorts;
    private Map<String, String> endpointUrls;
    
    
    public WsdlInformations() {
        this.servicesPorts = new ArrayList<String>();
        this.endpointUrls = new HashMap<String, String>();
    }

    public String getTargetNameSpace() {
        return targetNameSpace;
    }

    public void setTargetNameSpace(String targetNameSpace) {
        this.targetNameSpace = targetNameSpace;
    }

    public List<String> getServicesPorts() {
        return servicesPorts;
    }

    public void setServicesPorts(List<String> servicesPorts) {
        this.servicesPorts = servicesPorts;
    }

    public Map<String, String> getEndpointUrls() {
        return this.endpointUrls;
    }

    public void setEndpointUrls(HashMap<String, String> endpointUrls) {
        this.endpointUrls = endpointUrls;
    }    
    
    /**
     * Adds a port in service ports list
     * 
     * @param servicePort
     */
    public void addServicePort(String servicePort){
        this.servicesPorts.add(servicePort);
    }
    
    /**
     * Adds a port in service ports list
     * 
     * @param servicePort
     */
    public void addEndpointUrl(String servicePort, String endpointUrl){
        this.endpointUrls.put(servicePort, endpointUrl);
    }    
    
}
