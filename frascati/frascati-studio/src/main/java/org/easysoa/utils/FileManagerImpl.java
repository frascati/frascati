/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.logging.Logger;

import org.easysoa.api.FileManager;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.util.ScaResourceFactoryImpl;
import org.osoa.sca.annotations.Scope;

/**
 * Manage actions related to the File System
 */
@Scope("COMPOSITE")
public class FileManagerImpl implements FileManager {

    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(FileManagerImpl.class.getCanonicalName());
    
    /**
     * @see FileManager#saveComposite(EObject, String)
     */
    @Override
    public boolean saveComposite(EObject compositeObject, String path) {

        try {
            // create resource set and resource
            ResourceSet resourceSet = new ResourceSetImpl();

            // Register XML resource factory
            resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
                    .put("*", new ScaResourceFactoryImpl());

            Resource resource = resourceSet.createResource(URI
                    .createFileURI(path));

            // add the root object to the resource

            // create DR
            DocumentRoot documentRoot = ScaFactory.eINSTANCE.createDocumentRoot();
            // add frascati namespace to composite
            documentRoot.getXMLNSPrefixMap().put("frascati",
                    "http://frascati.ow2.org/xmlns/sca/1.1");
            
            //add xsd to composite. It provides dataypes to be defined in properties
            documentRoot.getXMLNSPrefixMap().put("xsd",
                    "http://www.w3.org/2001/XMLSchema");
            
            
            // add composite
            documentRoot.setComposite((Composite) compositeObject);

            resource.getContents().add(documentRoot);

            HashMap<Object, Object> options = new HashMap<Object, Object>();
            options.put(XMLResource.OPTION_ENCODING, new String("UTF-8"));

            resource.save(options);
            return true;
        } catch (IOException e) {

            return false;

        }

    }
    
    /**
     * @see FileManager#copyFilesRecursively(File, File, File)
     */
    @Override
    public void copyFilesRecursively(File sourceFile, File sourcePath, File targetPath) throws IOException{
        //Defines no filter
        FileFilter noFilter = new FileFilter() {
            public boolean accept(File file) {
                return true;
            }
        };
        
        copyFilesRecursively(sourceFile, sourcePath, targetPath, noFilter);
    }
    
    /**
     * @see FileManager#copyFilesRecursively(File, File, File, FileFilter)
     */
    @Override
    public void copyFilesRecursively(File sourceFile, File sourcePath, File targetPath, FileFilter fileFilter) throws IOException {
        try {
            copy(sourceFile,  new File(targetPath.getCanonicalPath() + sourceFile.getCanonicalPath().replace(sourcePath.getCanonicalPath(),"")));
        } catch (IOException e) {
            throw new IOException("It is not possible to copy one or more files ("+ sourceFile.getName() +"). Error: " + e.getMessage() );
        }
        if (sourceFile.isDirectory()) {
            for (File child : sourceFile.listFiles(fileFilter)) {
                copyFilesRecursively(child, sourcePath, targetPath, fileFilter);
            }
        }

    }
    
    /**
     * @see FileManager#deleteRecursively(File)
     */
    @Override
    public void deleteRecursively(File src) throws IOException {
        if (src.isDirectory()) {
            for (File file : src.listFiles()) {
                deleteRecursively(file);
            }
        }
        if (src.delete() == false){
            LOG.info("The file named " + src.getName() + " was not deleted");
        }
    }
    
    /**
     * Copy a file from <b>src</b> to <b>dst</b>
     * 
     * @param src
     * @param dst
     * @throws IOException
     */
    public static void copy(File src, File dst) throws IOException {
        
        //Don't copy if it's a directory
        if (src.isDirectory()) {
            dst.mkdir();
            return;
        }
        
        InputStream inputStream = new FileInputStream(src);
        OutputStream outputStream = new FileOutputStream(dst);
        
        byte[] buf = new byte[1024];
        int len;
        while ((len = inputStream.read(buf)) > 0) {
            outputStream.write(buf, 0, len);
        }
        inputStream.close();
        outputStream.close();
    }
   
    /**
     * Replace the file's content by the content in param
     * 
     * @param file 
     * @param content
     */
    public static void copyInFile(File file, String content){
    	try {
            FileWriter fileWriter = new FileWriter(file, false);
            BufferedWriter output = new BufferedWriter(fileWriter);

            output.write(content);
            output.flush();
            output.close();
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
