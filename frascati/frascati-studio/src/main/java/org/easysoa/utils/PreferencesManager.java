/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */

package org.easysoa.utils;

import java.io.File;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.model.Preference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;


/**
 * Initialize, manage and retrieve system preferences
 *
 * @see PreferencesManagerItf
 * @see Preference
 */
@Scope("COMPOSITE")
public class PreferencesManager implements PreferencesManagerItf {
    
    /*********************
     *  Logger
     * ********************/
    public final static Logger LOG = Logger.getLogger(PreferencesManager.class.getCanonicalName());
    
    /* *********************************
     * 
     *  Preferences names
     * 
     * Each preference in database must to have a corresponding property
     * 
     * *********************************/
    
    private static final String WORKSPACE_PATH_NAME = "workspacePath";
    
    private static final String LIB_PATH_NAME = "libPath";
    
    /**
     * Reference to the owning Fractal component.
     * 
     * It's used to access component properties directly in the component membrane
     */
    @Controller(name = "component")
    protected Component fractalComponent;
    
    /* ********************
     * References
     * ********************/
    
    @Reference
    protected Provider<EntityManager> database;
    
    /* ********************
     * Properties
     * ********************/

    @Property
    private String defaultWorkspacePath;
    
    /** auto inited */
    private String workspacePath;

    /** auto inited */
    private String libPath;
    
    /**
     * @see PreferencesManagerItf#getWorkspacePath()
     */
    @Override
    public String getWorkspacePath() {
        return workspacePath;
    }

    public void setWorkspacePath(String workspacePath) {
        this.workspacePath = workspacePath;
    }

    /**
     * @see PreferencesManagerItf#getLibPath()
     */
    @Override
    public String getLibPath() {
        return libPath;
    }

    public void setLibPath(String libPath) {
        this.libPath = libPath;
    }

    /* *********************
     * Methods
     * *********************/
    
    /**
     * Initialize the preferences retrieving from the database if exist. <br>
     *  If preferences don't exist, records in database will be created 
     *  with default values
     *  
     */
    @Init
    public void initPreferences(){
        try {
            SCAPropertyController propertyController = (SCAPropertyController)fractalComponent.getFcInterface(SCAPropertyController.NAME);

            EntityManager entityManager = database.get();
            
            if (defaultWorkspacePath == null || defaultWorkspacePath.trim().length() == 0) {
                defaultWorkspacePath = System.getProperty("user.home")+File.separator+"Documents"+File.separator+"frascati-studio";
            }
            
            workspacePath = verifyPreference(entityManager, WORKSPACE_PATH_NAME, defaultWorkspacePath);
            propertyController.setValue(WORKSPACE_PATH_NAME, workspacePath );
            
            String defaultLibPath = defaultWorkspacePath + File.separator+"lib";
            libPath = verifyPreference(entityManager, LIB_PATH_NAME, defaultLibPath);
            propertyController.setValue(LIB_PATH_NAME, libPath );

        } catch(Exception e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();
        }
        
    }

    /**
     * Verify if <b>preferenceName</b> exists.<br>
     * <i>If</i> it exists return the retrieved value. <br>
     * <i>Else</i> create preference with <b>defaultValue</b> in the database 
     * 
     * @param entityManager - The Entity manager used to connect database
     * @param preferenceName
     * @param defaultValue
     * @return The retrieved or created preference value
     */
    private String verifyPreference(EntityManager entityManager, String preferenceName, String defaultValue) {
        String currentValue = Preference.searchPreference(entityManager, preferenceName);

        if(currentValue == null){
            currentValue = defaultValue;
            Preference preference = new Preference(preferenceName, defaultValue);
            entityManager.getTransaction().begin();
            try {
                entityManager.persist(preference);
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                LOG.warning("The preference "+ preferenceName + "=" + defaultValue +"was not created");
                e.printStackTrace();
            }
        }
        
        return currentValue;
    }
    
    
}
