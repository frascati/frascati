/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */

package org.easysoa.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class to generate jar from a path containing all the jars and required resources 
 */
public final class JarGenerator {

    /**
     * Generate jar from all compiled sources
     * 
     * @param contentJarPath
     * @param outputJarPath
     * @return The generated Jar file 
     * @throws Exception
     */
	public static File generateJarFromAll(String contentJarPath, String outputJarPath) throws Exception{

		BufferedOutputStream out = null;
		
		out = new BufferedOutputStream(new FileOutputStream(outputJarPath));
		
		File src = new File(contentJarPath);
        
		JarUtils.jar(out, src);
		
        File jarFile = new File(outputJarPath);
        
        if (jarFile.exists()) {
            return jarFile;
        }else{
            throw new IOException("Jar File " + jarFile.getCanonicalPath() + " NOT FOUND!");
        }
		
	}


}
