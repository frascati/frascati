/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */

package org.easysoa.utils;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.easysoa.api.Provider;
import org.easysoa.api.FileTypesManagerItf;
import org.easysoa.model.FileType;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;


/**
 * Manages file types<br><br>
 * 
 * These file types are configured and saved in database table "FileType".
 * File types are used to configure locations where file with this type will be manipulated
 * 
 * @see FileType
 */
@Scope("COMPOSITE")
public class FileTypesManager implements FileTypesManagerItf {
    
    /*********************
     *  Logger
     * ********************/
    public final static Logger LOG = Logger.getLogger(FileTypesManager.class.getCanonicalName());
    

   
    /* ********************
     * References
     * ********************/
    
    @Reference
    protected Provider<EntityManager> database;


    /* *********************
     * Methods
     * *********************/

    /**
     * @see FileTypesManagerItf#getAllFileTypes()
     */
    @Override
    public List<String> getAllFileTypes() {
        return FileType.retrieveAllFileTypes(database.get());
    }
    
    /**
     * @see FileTypesManagerItf#getLocationFileType(String)
     */
    @Override
    public String getLocationFileType(String fileTypeName){
        return FileType.retrieveLocationFileType(database.get(), fileTypeName);
    }
    
    
}
