/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.namespace.QName;

import org.easysoa.api.EMFModelUtils;
import org.easysoa.api.FileManager;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.SCABinding;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Service;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.processor.ProcessingContextImpl;
import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * Manages EMF models for Service Component Architecture
 * 
 * @see EMFModelUtils
 */
@Scope("COMPOSITE")
public class EMFModelUtilsImpl implements EMFModelUtils{

	@Reference
    protected ServiceManager serviceManager;
	@Reference
    protected FileManager fileManager;
	@Reference
	protected PreferencesManagerItf preferences;
	@Reference
    public Parser compositeParser;
	@Reference 
    protected ClassLoaderManager classloaderManager;
	
	
	/** Logger */
    public final static Logger LOG = Logger.getLogger(EMFModelUtilsImpl.class.getCanonicalName());
	
    /**
     * @see EMFModelUtils#getComponent(String, String)
     */
	public EObject getComponent(String userId, String elementId) {
	    
        try {
            elementId = URLDecoder.decode(elementId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String[] ids = elementId.split(" ");
      

        if (ids[ids.length - 1].contains("_")) {
            ids[ids.length - 1] = ids[ids.length - 1].substring(0,
                    ids[ids.length - 1].indexOf("_"));
        }
        Composite composite = serviceManager.getComposite(userId);
        // composite
        if (ids.length == 1) {
            return composite;
        } else if (ids[0].equals("component")) {
            EList<Component> components = composite.getComponent();
            for (Component component : components) {
                if (component.getName().equals(ids[1])) {
                    // component name
                    if (ids.length == 2) {
                        return component;
                    }
                    // component name implementation
                    else if (ids.length == 3) {
                        return component.getImplementation();
                    } else {
                        // component name property name
                        if (ids[2].equals("property")) {
                            for (PropertyValue property : component
                                    .getProperty()) {
                                if (property.getName().equals(ids[3])) {
                                    return property;
                                }
                            }
                        } else if (ids[2].equals("reference")) {
                            for (ComponentReference componentReference : component
                                    .getReference()) {
                                if (componentReference.getName().equals(ids[3])) {
                                    // component name reference name
                                    if (ids.length == 4) {
                                        return componentReference;
                                    }
                                    // component name reference name binding
                                    // name
                                    // name
                                    else if (ids.length == 6
                                            && ids[4].equals("binding")) {
                                        for (Binding binding : componentReference
                                                .getBinding()) {
                                            if (ids[5]
                                                    .equals(binding.getName())) {
                                                return binding;
                                            }
                                        }
                                    } else if (ids.length == 5
                                            && ids[4].equals("interface")) {
                                        return componentReference
                                                .getInterface();
                                    }
                                }
                            }
                        } else if (ids[2].equals("service")) {
                            for (ComponentService componentService : component
                                    .getService()) {
                                if (componentService.getName().equals(ids[3])) {
                                    // component name service name
                                    if (ids.length == 4) {
                                        return componentService;
                                    }
                                    // component name service name interface
                                    else if (ids.length == 5
                                            && ids[4].equals("interface")) {
                                        return componentService.getInterface();
                                    }
                                    // component name service name binding uri
                                    else if (ids.length == 6
                                            && ids[4].equals("binding")) {
                                        for (Binding binding : componentService
                                                .getBinding()) {
                                            if (ids[5]
                                                    .equals(binding.getName())) {
                                                LOG.info("URI : "
                                                        + binding.getUri());
                                                LOG.info("NAME : "
                                                        + binding.getName());
                                                return binding;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if (ids[0].equals("service")) {
            for (Service service : composite.getService()) {
                if (service.getName().equals(ids[1])) {
                    // service name
                    if (ids.length == 2) {
                        return service;
                    }
                    // service name binding uri
                    else if (ids.length == 4 && ids[2].equals("binding")) {
                        for (Binding binding : service.getBinding()) {
                            if (ids[3].equals(binding.getName())) {
                                return binding;
                            }
                        }
                    } else if (ids.length == 3 && ids[2].equals("interface")) {
                        return service.getInterface();
                    }
                }
            }
        } else if (ids[0].equals("reference")) {
            for (org.eclipse.stp.sca.Reference reference : composite
                    .getReference()) {
                if (reference.getName().equals(ids[1])) {
                    // reference name
                    if (ids.length == 2) {
                        return reference;
                    }
                    // reference name binding uri
                    else if (ids.length == 4 && ids[2].equals("binding")) {
                        for (Binding binding : reference.getBinding()) {
                            if (ids[3].equals(binding.getName())) {
                                return binding;
                            }
                        }
                    } else if (ids.length == 3 && ids[2].equals("interface")) {
                        return reference.getInterface();
                    }
                }
            }
        }
        return null;
    }

	/**
	 * @see EMFModelUtils#addElement(String, String, String)
	 */
	@Override
	public void addElement(String userId, String elementId, String action) {
		try {
            elementId = URLDecoder.decode(elementId, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String[] ids = elementId.split(" ");
        if (ids[ids.length - 1].contains("_")) {
            ids[ids.length - 1] = ids[ids.length - 1].substring(0,
                    ids[ids.length - 1].indexOf("_"));
        }

        LOG.info("addElement# id:" + elementId + " action:" + action);
        Composite composite = serviceManager.getComposite(userId);
        // composite
        if (ids.length == 1) {
            if ("addComponent".equals(action)) {
                Component component = ScaFactory.eINSTANCE.createComponent();
                component.setName("name");
                composite.getComponent().add(component);
            } else if ("addService".equals(action)) {
                Service service = ScaFactory.eINSTANCE.createService();
                service.setName("name");
                composite.getService().add(service);
            } else if ("addReference".equals(action)) {
                org.eclipse.stp.sca.Reference reference = ScaFactory.eINSTANCE
                        .createReference();
                reference.setName("name");
                composite.getReference().add(reference);
            }
        } else if ("component".equals(ids[0])) {
            Iterator<Component> components = composite.getComponent()
                    .iterator();
            while (components.hasNext()) {
                Component component = components.next();
                if (component.getName().equals(ids[1])) {
                    // component name
                    if (ids.length == 2) {
                        if ("addComponentService".equals(action)) {
                            ComponentService componentService = ScaFactory.eINSTANCE
                                    .createComponentService();
                            componentService.setName("name");
                            component.getService().add(componentService);
                        } else if ("addComponentReference".equals(action)) {
                            ComponentReference componentReference = ScaFactory.eINSTANCE
                                    .createComponentReference();
                            componentReference.setName("name");
                            component.getReference().add(componentReference);
                        } else if ("addComponentProperty".equals(action)) {
                            PropertyValue componentProperty = ScaFactory.eINSTANCE
                                    .createPropertyValue();
                            componentProperty.setName("name");
                            component.getProperty().add(componentProperty);
                        } else if ("deleteComponent".equals(action)) {
                            components.remove();
                        }
                    }
                    // component name implementation
                    else if (ids.length == 3) {
                        if ("deleteImplementation".equals(action)) {
                            ((FeatureMap.Internal) component
                                    .getImplementationGroup()).clear();
                        }
                    } else {
                        // component name property name
                        if (ids[2].equals("property")) {
                            Iterator<PropertyValue> iteratorPropertyValue = component
                                    .getProperty().iterator();
                            while (iteratorPropertyValue.hasNext()) {
                                PropertyValue property = iteratorPropertyValue
                                        .next();
                                if (property.getName().equals(ids[3]) && "deleteComponentProperty".equals(action)) {
                                    iteratorPropertyValue.remove();
                                }
                            }
                        } else if ("reference".equals(ids[2])) {
                            Iterator<ComponentReference> iteratorComponentReference = component
                                    .getReference().iterator();
                            while (iteratorComponentReference.hasNext()) {
                                ComponentReference componentReference = iteratorComponentReference
                                        .next();
                                if (componentReference.getName().equals(ids[3])) {
                                    // component name reference name
                                    if (ids.length == 4) {
                                        if ("addBinding".equals(action)) {
                                            SCABinding binding = ScaFactory.eINSTANCE
                                                    .createSCABinding();
                                            binding.setName("name");
                                            binding.setUri("uri");
                                            componentReference.getBinding()
                                                    .add(binding);
                                        } else if ("deleteComponentReference".equals(action)) {
                                            iteratorComponentReference.remove();
                                        }
                                    }
                                    // component name reference name binding uri
                                    // name
                                    else if (ids.length == 6
                                            && "binding".equals(ids[4])) {
                                        Iterator<Binding> iteratorBinding = componentReference
                                                .getBinding().iterator();
                                        while (iteratorBinding.hasNext()) {
                                            Binding binding = iteratorBinding
                                                    .next();
                                            if (ids[5].equals(binding.getName()) && "deleteBinding".equals(action)) {
                                                iteratorBinding.remove();
                                            }
                                        }
                                    } else if (ids.length == 5
                                            && "interface".equals(ids[4])
                                            && "deleteInterface".equals(action)) {
                                        componentReference.setInterface(null);
                                    }
                                }
                            }
                        } else if (ids[2].equals("service")) {
                            Iterator<ComponentService> iteratorComponentService = component
                                    .getService().iterator();
                            while (iteratorComponentService.hasNext()) {
                                ComponentService componentService = iteratorComponentService
                                        .next();
                                if (componentService.getName().equals(ids[3])) {
                                    // component name service name
                                    if (ids.length == 4) {
                                        if ("addBinding".equals(action)) {
                                            SCABinding binding = ScaFactory.eINSTANCE
                                                    .createSCABinding();
                                            binding.setUri("uri");
                                            binding.setName("name");
                                            componentService.getBinding().add(
                                                    binding);
                                        } else if ("deleteComponentService".equals(action)) {
                                            iteratorComponentService.remove();
                                        }
                                    }
                                    // component name service name binding uri
                                    else if (ids.length == 6
                                            && "binding".equals(ids[4])) {
                                        Iterator<Binding> iteratorBinding = componentService
                                                .getBinding().iterator();
                                        while (iteratorBinding.hasNext()) {
                                            Binding binding = iteratorBinding
                                                    .next();
                                            if (ids[5].equals(binding.getName())  && "deleteBinding".equals(action)) {
                                                iteratorBinding.remove();
                                            }
                                        }
                                    } else if (ids.length == 5
                                            && "interface".equals(ids[4])
                                            && "deleteInterface".equals(action)) {
                                        componentService.setInterface(null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else if ("service".equals(ids[0])) {
            Iterator<Service> iteratorService = composite.getService()
                    .iterator();
            while (iteratorService.hasNext()) {
                Service service = iteratorService.next();
                if (service.getName().equals(ids[1])) {
                    // service name
                    if (ids.length == 2) {
                        if ("addBinding".equals(action)) {
                            SCABinding binding = ScaFactory.eINSTANCE
                                    .createSCABinding();
                            binding.setUri("uri");
                            binding.setName("name");
                            service.getBinding().add(binding);
                        }
                        if ("deleteService".equals(action)) {
                            iteratorService.remove();
                        }
                    }
                    // service name binding uri
                    else if (ids.length == 4 && "binding".equals(ids[2])) {
                        Iterator<Binding> iteratorBinding = service
                                .getBinding().iterator();
                        while (iteratorBinding.hasNext()) {
                            Binding binding = iteratorBinding.next();
                            if (ids[3].equals(binding.getName()) && "deleteBinding".equals(action)) {
                                iteratorBinding.remove();
                            }
                        }
                    } else if (ids.length == 3 && "interface".equals(ids[2]) && "deleteInterface".equals(action)) {
                        service.setInterface(null);
                    }
                }
            }
        } else if ("reference".equals(ids[0])) {
            Iterator<org.eclipse.stp.sca.Reference> iteratorReference = composite
                    .getReference().iterator();
            while (iteratorReference.hasNext()) {
                org.eclipse.stp.sca.Reference reference = iteratorReference
                        .next();
                if (reference.getName().equals(ids[1])) {
                    // reference name
                    if (ids.length == 2) {
                        if ("addBinding".equals(action)) {
                            SCABinding binding = ScaFactory.eINSTANCE
                                    .createSCABinding();
                            binding.setUri("uri");
                            binding.setName("name");
                            reference.getBinding().add(binding);
                        }
                        if ("deleteReference".equals(action)) {
                            iteratorReference.remove();
                        }
                    }
                    // reference name binding uri
                    else if (ids.length == 4 && ids[2].equals("binding")) {
                        Iterator<Binding> iteratorBinding = reference
                                .getBinding().iterator();
                        while (iteratorBinding.hasNext()) {
                            Binding binding = iteratorBinding.next();
                            if (ids[3].equals(binding.getName())  && "deleteBinding".equals(action)) {
                                iteratorBinding.remove();
                            }
                        }
                    } else if (ids.length == 3 && "interface".equals(ids[2]) && "deleteInterface".equals(action)) {
                        reference.setInterface(null);
                    }
                }
            }
        }
        Application currentApplication = serviceManager.getCurrentApplication(userId);
        currentApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
        
        fileManager.saveComposite(composite, currentApplication.retrieveAbsoluteCompositeLocation());
        serviceManager.setComposite(userId, composite);
        serviceManager.reloadComposite(userId, ServiceManager.CURRENT_VERSION);
	}
	
	/**
	 * @see EMFModelUtils#hasAnImplementation(String, String)
	 */
	public boolean hasAnImplementation(String userId, String elementId){
		EObject eobject = this.getComponent(userId, elementId);
		Component component = (Component) eobject;
		return component.getImplementation() != null;
	}

	/**
	 * @see EMFModelUtils#hasAnInterface(String, String)
	 */
	@Override
	public boolean hasAnInterface(String userId, String elementId) {
		EObject eobject = this.getComponent(userId, elementId);
		if(eobject instanceof Service){
			Service service = (Service)eobject;
			return service.getInterface() != null;
		}
		else if(eobject instanceof org.eclipse.stp.sca.Reference){
			org.eclipse.stp.sca.Reference reference = (org.eclipse.stp.sca.Reference)eobject;
			return reference.getInterface() != null;
		}
		else if(eobject instanceof ComponentService){
			ComponentService service = (ComponentService)eobject;
			return service.getInterface() != null;
		}
		else{
			ComponentReference reference = (ComponentReference)eobject;
			return reference.getInterface() != null;
		}
	}

	/**
	 * @see EMFModelUtils#addIntent(String, String, String)
	 */
	@Override
	public EObject addIntent(String userId, String elementId, String intentName) {
		EObject eobject = this.getComponent(userId, elementId);
		if(eobject instanceof Service){
			Service service = (Service)eobject;
			List<QName> requires = service.getRequires();
			if(requires == null){
				requires = new ArrayList<QName>();
			}
			requires.add(new QName(intentName));
			service.setRequires(requires);
		}
		else if(eobject instanceof org.eclipse.stp.sca.Reference){
			org.eclipse.stp.sca.Reference reference = (org.eclipse.stp.sca.Reference)eobject;
			List<QName> requires = reference.getRequires();
			if(requires == null){
				requires = new ArrayList<QName>();
			}
			requires.add(new QName(intentName));
			reference.setRequires(requires);
		}
		else if(eobject instanceof ComponentService){
			ComponentService service = (ComponentService)eobject;
			List<QName> requires = service.getRequires();
			if(requires == null){
				requires = new ArrayList<QName>();
			}
			requires.add(new QName(intentName));
			service.setRequires(requires);
		}
		else{
			ComponentReference reference = (ComponentReference)eobject;
			List<QName> requires = reference.getRequires();
			if(requires == null){
				requires = new ArrayList<QName>();
			}
			requires.add(new QName(intentName));
			reference.setRequires(requires);
			return reference;
		}
		return eobject;
	}

	/**
	 * @see EMFModelUtils#getCompositeFromApplication(Application)
	 */
	@Override
	public Composite getCompositeFromApplication(Application application) {
		try{
		String location = application.retrieveAbsoluteCompositeLocation();
        location = location
                .replaceAll(File.separator + File.separator, "/");
        FrascatiClassLoader classLoader =  new FrascatiClassLoader(this.classloaderManager.getClassLoader());
        String sourceFile = null;
        
        // Give the path until java directory
        if(application.getPackageName() != null && !application.getPackageName().equals("")){
        	sourceFile = application.retrieveAbsoluteSources().substring(
                0,
                application.retrieveAbsoluteSources().indexOf(
                		application.getPackageName().replace(".",
                                File.separator)));
        }
        else{
        	sourceFile = application.retrieveAbsoluteSources();
        }
        classLoader.addUrl(new URL("file://" + sourceFile));
        String resourceFile = application.retrieveAbsoluteResources();
        classLoader.addUrl(new URL("file://" + resourceFile));
        Composite composite = (Composite) compositeParser.parse(new QName(
                "file://" + location), new ProcessingContextImpl(
                classLoader));
        return composite;
		}
		catch(Exception e){
			return null;
		}
	}
	
	/**
	 * @see EMFModelUtils#getIntentComposite(Application, String)
	 */
	@Override
	public Composite getIntentComposite(Application application, String compositeName){
		try{
			String location = this.preferences.getWorkspacePath()+File.separator+application.getResources()+File.separator+compositeName+".composite";
	        location = location
	                .replaceAll(File.separator + File.separator, "/");
	        FrascatiClassLoader classLoader =  new FrascatiClassLoader(this.classloaderManager.getClassLoader());
	        String sourceFile = null;
	        
	        // Give the path until java directory
	        if(application.getPackageName() != null && !application.getPackageName().equals("")){
	        	sourceFile = application.retrieveAbsoluteSources().substring(
	                0,
	                application.retrieveAbsoluteSources().indexOf(
	                		application.getPackageName().replace(".",
	                                File.separator)));
	        }
	        else{
	        	sourceFile = application.retrieveAbsoluteSources();
	        }
	        classLoader.addUrl(new URL("file://" + sourceFile));
	        String resourceFile = application.retrieveAbsoluteResources();
	        classLoader.addUrl(new URL("file://" + resourceFile));
	        Composite composite = (Composite) compositeParser.parse(new QName(
	                "file://" + location), new ProcessingContextImpl(
	                classLoader));
	        return composite;
			}
			catch(Exception e){
				e.printStackTrace();
				return null;
			}
	}
}
