/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.jpa;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.easysoa.api.AdminAccess;
import org.easysoa.api.DeployProcessorItf;
import org.easysoa.api.FileTypesManagerItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.model.Application;
import org.easysoa.model.BugReport;
import org.easysoa.model.DeployedApplication;
import org.easysoa.model.DeploymentServer;
import org.easysoa.model.Role;
import org.easysoa.model.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Manage Administrator features
 * 
 */
@Scope("COMPOSITE")
public class AdminAccessImpl implements AdminAccess{

    @Reference
    protected Provider<EntityManager> database;
    @Reference
    protected DeployProcessorItf deploy;
    @Reference
    protected PreferencesManagerItf preferences;
    @Reference 
    protected FileTypesManagerItf fileTypesManager;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(AdminAccessImpl.class.getCanonicalName());
    
    /**
     * @see AdminAccess#getAllUsersJSON()
     */
    @SuppressWarnings("unchecked")
    @Override
    public String getAllUsersJSON() {
        EntityManager entityManager = database.get();
        List<User> users = User.getAllUsers(entityManager);
        JSONObject object = new JSONObject();
        object.put("id", 0);
        JSONArray usersJson = new JSONArray();
        for(User user : users){
            JSONObject userJson = new JSONObject();
            userJson.put("id", user.getId());
            userJson.put("text", user.getLogin());
            userJson.put("im0", "kuser.png");
            userJson.put("im1", "kuser.png");
            userJson.put("im2", "kuser.png");
            usersJson.add(userJson);
        }
        object.put("item", usersJson);
        return object.toJSONString();
    }

    /**
     * @see AdminAccess#getUser(String)
     */
    @SuppressWarnings("unchecked")
    @Override
    public String getUser(String userId) {
        EntityManager entityManager = database.get();
        User user = entityManager.find(User.class, Long.parseLong(userId));
        JSONObject userJson = new JSONObject();
        userJson.put("login", user.getLogin());
        userJson.put("role", user.getRole().value());
        userJson.put("mail", user.getMail());
        userJson.put("username", user.getUserName());
        userJson.put("surname", user.getSurname());
        return userJson.toJSONString();
    }

    /**
     * @see AdminAccess#changeAdmin(String, boolean)
     */
    @Override
    public void changeAdmin(String userId, boolean isAdmin) {
        EntityManager entityManager = database.get();
        User user = entityManager.find(User.class, Long.parseLong(userId));
        Role role = null;
        if(isAdmin){
            role = Role.ADMIN;
        }
        else{
            role = Role.USER;
        }
        entityManager.getTransaction().begin();
        try {
            user.setRole(role);
            entityManager.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying change Admin role: " + e.getMessage());
            e.printStackTrace();
        }
        
    }
    
    /**
     * @see AdminAccess#getAllDeployedAppJSON()
     */
    @SuppressWarnings("unchecked")
    @Override
    public String getAllDeployedAppJSON() {
        EntityManager entityManager = database.get();
        List<DeploymentServer> deployementServers = DeploymentServer.getAllDeploymentServers(entityManager);
        JSONObject object = new JSONObject();
        object.put("id", 0);
        JSONArray servers = new JSONArray();
        for(DeploymentServer deploymentServer : deployementServers){
            JSONObject server = new JSONObject();
            server.put("id", deploymentServer.getUrl());
            server.put("text", deploymentServer.getUrl());
            server.put("im0", "cloud.png");
            server.put("im1", "cloud.png");
            server.put("im2", "cloud.png");
            JSONArray applications = new JSONArray();
            for(DeployedApplication deployedApplication : DeployedApplication.getDeployedApplications(entityManager, deploymentServer)){
                JSONObject appObject = new JSONObject();
                appObject.put("id", deploymentServer.getUrl()+"#"+deployedApplication.getApplication().getId());
                appObject.put("text", deployedApplication.getApplication().getName());
                appObject.put("im0", "application.png");
                appObject.put("im1", "application.png");
                appObject.put("im2", "application.png");
                applications.add(appObject);
            }
            server.put("item", applications);
            servers.add(server);
        }
        object.put("item", servers);
        return object.toJSONString();
    }
    
    /**
     * @see AdminAccess#getAllDeployedAppObjects()
     */
    @Override
    public List<DeploymentServer> getAllDeployedAppObjects() {
        EntityManager entityManager = database.get();
        return DeploymentServer.getAllDeploymentServers(entityManager);
    }

    /**
     * @see AdminAccess#getAllDeployedAppLocations()
     */
    @Override
    public String getAllDeployedAppLocations(){
        List<DeploymentServer> deployedApp = this.getAllDeployedAppObjects();
        return this.transformDeploymentServersToString(deployedApp);
    }
    
    /**
     * Transform deployment servers to String
     * 
     * @param apps
     * @return
     */
    @SuppressWarnings("unchecked")
    private String transformDeploymentServersToString(List<DeploymentServer> apps) {
        JSONObject urls = new JSONObject();
        
        JSONArray urlsArray = new JSONArray();
        for(DeploymentServer app : apps){
            JSONObject url = new JSONObject();
            url.put("value", app.getUrl());
            urlsArray.add(url);
        }
        urls.put("urls",urlsArray);
        return urls.toJSONString();
    }
    
    /**
     * @see AdminAccess#getDeploymentServer(String)
     */
    @Override
    public DeploymentServer getDeploymentServer(String server) {
        EntityManager entityManager = database.get();
        DeploymentServer deploymentServer = DeploymentServer.getDeploymentServers(entityManager,server);
        return deploymentServer;
    }

    /**
     * @see AdminAccess#redeploy(String)
     */
    @Override
    public synchronized void redeploy(String server) throws Exception {
        LOG.info("redeploy : "+server);
        
        EntityManager entityManager = database.get();
        
        DeploymentServer deploymentServer = this.getDeploymentServer(server);
        for(DeployedApplication deployedApplication : DeployedApplication.getDeployedApplications(entityManager, deploymentServer)){
            Application application = new Application();
            application.setGlobalLibPath(preferences.getLibPath()); 
            application.setLocalLibPath(application.retrieveAbsoluteRoot() + fileTypesManager.getLocationFileType("Library"));
            application.setCurrentWorskpacePath(preferences.getWorkspacePath());
            deploy.deploy(new String[]{server}, deployedApplication.getApplication());
        }
    }

    /**
     * @see AdminAccess#removeDeployedApplication(Long)
     */
	@Override
	public void removeDeployedApplication(Long appId) {
		EntityManager entityManager = database.get();
		DeployedApplication.removeDeployedApplication(entityManager, appId);
	}

	/**
	 * @see AdminAccess#getBugs()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getBugs() {
		EntityManager entityManager = database.get();
		List<BugReport> bugs = BugReport.getAllBugs(entityManager);
		JSONArray bugsJson = new JSONArray();
		for(BugReport br : bugs){
			JSONObject brJSon = new JSONObject();
			brJSon.put("login", br.getLogin());
			brJSon.put("mail", br.getMail());
			brJSon.put("object", br.getObject());
			brJSon.put("description", br.getBugDescription());
			brJSon.put("id", br.getId());
			bugsJson.add(brJSon);
		}
		return bugsJson.toJSONString();
	}
    

}
