package org.easysoa.compositeTemplates;

import java.util.List;
import java.util.Map;
import org.easysoa.api.CodeGenerator;
import org.easysoa.api.TemplateActionItf;
import org.easysoa.model.Application;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Reference;

/**
 * 
 *  Template action for the Easysoa HTTP proxy app
 * @author jguillemotte
 *
 */
public class TemplateActionEasysoaHttpProxy implements TemplateActionItf {
    
    @Reference
    protected List<CodeGenerator> generators;    
    
    @Override
    public String getId() {
        return "DiscoveryProxy";
    }

    @Override
    public String doActionBeforeCreation(Map<String, Object> params) throws Exception {
        return "";
    }

    @Override
    public String doActionAfterCreation(Map<String, Object> params, Composite composite, Application application) throws Exception {
        return "";
    }

}
