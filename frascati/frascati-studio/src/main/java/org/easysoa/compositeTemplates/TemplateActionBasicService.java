/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): 
 *
 */
package org.easysoa.compositeTemplates;

import java.io.File;
import java.util.Map;
import java.util.logging.Logger;

import org.easysoa.api.CodeGeneratorProcessorItf;
import org.easysoa.api.CodeTransformerProcessorItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.RESTCall;
import org.easysoa.api.TemplateActionItf;
import org.easysoa.model.Application;
import org.easysoa.utils.FileManagerImpl;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Reference;

public class TemplateActionBasicService implements TemplateActionItf {

	@Reference
	protected RESTCall rest;
	@Reference
	protected PreferencesManagerItf preferences;
	@Reference
	protected CodeTransformerProcessorItf codeTransformer;
	@Reference
	protected CodeGeneratorProcessorItf codeGenerator;

	/** Logger */
	public final static Logger LOG = Logger
			.getLogger(TemplateActionBasicService.class.getCanonicalName());

	@Override
	public String getId() {
		return "BasicService";
	}

	@Override
	public String doActionBeforeCreation(Map<String, Object> params) {
		return "";
	}

	@Override
	public String doActionAfterCreation(Map<String, Object> params,
			Composite composite, Application application) throws Exception {
		try {
			LOG.info("doActionAfterCreation basicService");
			String userId = (String) params.get("userId");
			String bindingType = (String) params.get("binding");
			String className = (String) params.get("implementation");
			String implemType = (String) params.get("implementation-type");
			String interfaceName = (String) params.get("interface-name");
			String interfaceType = (String) params.get("interface-type");
			String interfaceContent = (String) params.get("temp-textarea");
			interfaceContent = "package " + application.getPackageName()
					+ ".api" + ";\n\n" + interfaceContent;
			if (implemType.equals("Java")) {
				className += ".java";
			}
			if (interfaceType.equals("Java")) {
				interfaceName += ".java";
			}

			this.rest.addElement(userId, "", "addComponent");
			this.rest.addElement(userId, "component+name",
					"addComponentService");
			this.rest.addElement(userId, "", "addService");
			String paramsForPromote = "id=service+name&promote=name/name&name=name";
			this.rest.saveElement(userId, paramsForPromote);
			this.rest.createNewBinding(userId, "service+name", bindingType, "");
			this.rest.createNewInterface(userId, "component+name+service+name",
					interfaceName, interfaceType, true, "New");
			this.rest.createNewInterface(
					userId,
					"service+name",
					"org.ow2.frascati.api."
							+ interfaceName.substring(0,
									interfaceName.indexOf(".")), interfaceType,
					false, "Use");
			this.rest.createNewImplementation(userId, "component+name",
					className, implemType, true);
			File interfaceFile = new File(preferences.getWorkspacePath()
					+ File.separator + application.getSources()
					+ File.separator + "api" + File.separator + interfaceName);
			File implementationFile = new File(preferences.getWorkspacePath()
					+ File.separator + application.getSources()
					+ File.separator + "impl" + File.separator + className);
			FileManagerImpl.copyInFile(interfaceFile, interfaceContent);

			String jsonRepresentation = codeTransformer.transform(
					interfaceType, interfaceFile, application);
			codeGenerator.generate(implemType, jsonRepresentation,
					implementationFile, className);
			return "";
		} catch (Exception e) {
			throw e;
		}
	}

}
