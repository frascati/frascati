/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto, Philippe Merle
 *
 */
package org.easysoa.compositeTemplates;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easysoa.api.CodeGenerator;
import org.easysoa.api.RESTCall;
import org.easysoa.api.TemplateActionItf;
import org.easysoa.model.Application;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;

public class TemplateActionProxyWSLog implements TemplateActionItf {

	@Reference 
	protected RESTCall rest;
	
	@Reference
	protected List<CodeGenerator> generators;
	
	private Map<String, CodeGenerator> generatorsMap;
	
	@Init
	public void initializeMapId(){
		this.generatorsMap = new HashMap<String, CodeGenerator>();
		for(CodeGenerator generator : generators){
			generatorsMap.put(generator.getId(), generator);
		}
	}
	
	@Override
	public String getId() {
		return "ProxyWSLog";
	}

	@Override
	public String doActionBeforeCreation(Map<String, Object> params) {
		return "";
	}

	@Override
	public String doActionAfterCreation(Map<String, Object> params, Composite composite, Application application) {
		String generatorId = (String)params.get("generator-id");
		params.put("implementation", generatorsMap.get(generatorId).getCompositeLineImplementation((String)params.get("service")));
		generatorsMap.get(generatorId).generate((String)params.get("userId"),(String)params.get("service"),(String)params.get("interface"),(String)params.get("url"));
		if("Java".equals(generatorId)){
			this.rest.createNewImplementation((String)params.get("userId"), "component+proxy-"+application.getName(), (String)params.get("service")+".java", "Java", false);
		}
		else if("JavaScript".equals(generatorId)){
			this.rest.createNewImplementation((String)params.get("userId"), "component+proxy-"+application.getName(), (String)params.get("service")+".js", "Script", false);
		}
		rest.addIntent(application, (String)params.get("userId"), "service+serviceProxy", "logging");
		return "";
	}

}
