/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.compositeTemplates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.easysoa.api.CompositeTemplateItf;
import org.easysoa.api.CompositeTemplateProcessorItf;
import org.easysoa.api.TemplateActionItf;
import org.easysoa.model.Application;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class CompositeTemplate implements CompositeTemplateProcessorItf {

    @Reference
    protected List<CompositeTemplateItf> templates;
    @Reference
    protected List<TemplateActionItf> actionsTemplate;
    protected Map<String, CompositeTemplateItf> templateMap;
    protected Map<String, TemplateActionItf> templateActionMap;

    /** Logger */
    public final static Logger LOG = Logger.getLogger(CompositeTemplate.class.getCanonicalName());
    
    @Init
    public final void initializeProcessorsByID() {
    	LOG.info("composite Template");
        this.templateMap = new HashMap<String, CompositeTemplateItf>();
        for (CompositeTemplateItf compositeTemplate : this.templates) {
            this.templateMap.put(compositeTemplate.getId(), compositeTemplate);
        }
        
        this.templateActionMap = new HashMap<String, TemplateActionItf>();
        for (TemplateActionItf compositeActionTemplate : this.actionsTemplate) {
            this.templateActionMap.put(compositeActionTemplate.getId(), compositeActionTemplate);
            LOG.info(compositeActionTemplate.getId());
        }
    }

    protected CompositeTemplateItf getProcessorById(String templateName) {
        return this.templateMap.get(templateName);
    }
    
    protected TemplateActionItf getTemplateActionProcessorById(String templateName) {
        return this.templateActionMap.get(templateName);
    }

    @Override
    public String getTemplate(String templateName, Map<String, Object> params) {
        String template = this.getProcessorById(templateName).getTemplate(params);
        LOG.info(template);
        return template;
    }

    @Override
    public List<String> allAvailableTemplatesLabel() {
        List<String> labels = new ArrayList<String>(templateMap.keySet());
        return labels;
    }

    @Override
    public String getForm(String templateName) {
        return this.getProcessorById(templateName).getForm();
    }
    
    @Override
    public String doActionAfterCreation(String templateName, Map<String, Object> params, Composite composite, Application application) throws Exception{
        return this.getTemplateActionProcessorById(templateName).doActionAfterCreation(params, composite, application);
    }

    @Override
    public String doActionBeforeCreation(String templateName, Map<String, Object> params) {
    	try{
    	TemplateActionItf templateAction = this.getTemplateActionProcessorById(templateName);
        String res = templateAction.doActionBeforeCreation(params);
        return res;
    	}
    	catch(Exception e){
    		e.printStackTrace();
    		return e.getMessage();
    	}
    }
}
