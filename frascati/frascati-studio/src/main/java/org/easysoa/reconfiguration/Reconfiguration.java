/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio de Almeida Souza Neto
 *
 * Contributor(s): 
 *
 */

package org.easysoa.reconfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.easysoa.impl.ServiceManagerImpl;
import org.easysoa.reconfiguration.update.AddComponent;
import org.easysoa.reconfiguration.update.AddWire;
import org.easysoa.reconfiguration.update.RemoveComponent;
import org.easysoa.reconfiguration.update.RemoveWire;
import org.easysoa.reconfiguration.update.Update;
import org.easysoa.reconfiguration.update.UpdateComponent;
import org.easysoa.reconfiguration.update.UpdateWire;
import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.impl.JavaImplementationImpl;
import org.eclipse.stp.sca.impl.JavaInterfaceImpl;
import org.osoa.sca.annotations.Scope;

/**
 * Process actions related to Dynamic Reconfiguration
 */
@Scope("COMPOSITE")
public class Reconfiguration {

    /** Logger */
    public final static Logger LOG = Logger.getLogger(ServiceManagerImpl.class.getCanonicalName());
    
    private List<Update> updates = new ArrayList<Update>();
    private Composite previousComposite;
    private Composite currentComposite;
    

    /**
     * Compare two composites and return a List of Updates.<br>
     * Updates can be:<br>
     * - AddComponent<br>
     * - RemoveComponent<br>
     * - AddWire<br>
     * - RemoveWire<br>
     * 
     * 
     * @param previousComposite
     * @param currentComposite
     * @return
     */
    public List<Update> compareComposites(Composite previousComposite, Composite currentComposite){
        
        this.previousComposite = previousComposite;
        this.currentComposite = currentComposite;
        
        compareComponents(previousComposite.getComponent(), currentComposite.getComponent());
        System.out.println("List of wires " + previousComposite.getWire().size() +" : " +previousComposite.getWire());
        
        LOG.info("Updates to do:");
        for (Update update : this.updates) {
            if (update  instanceof  UpdateComponent) {
                LOG.info(update.getClass().getName() + ": " + ((UpdateComponent)update).getComponent().getName() );
            }else if(update  instanceof  UpdateWire) {
                LOG.info(update.getClass().getName() 
                        + ": From " + ((UpdateWire)update).getFrom().getName() + "/" +((UpdateWire)update).getReference().getName() 
                        + " to " + ((UpdateWire)update).getTarget());
            }
        }
        
        return this.updates;
    }
    
    /**
     * Compare all the components in two lists and return a list of Updates.<br>
     *
     * An element <i>RemoveComponent</i> is added to the list of updates if the Component is found in previousComponents but not in currentComponents<br>
     * An element <i>AddComponent</i> is added to the list of updates if the Component is found in currentComponents but not in previousComponents<br>
     * An element <i>RemoveComponent</i> and another <i>AddComponent</i> are added to the list if the Component is modified<br>
     * 
     * @param previousComponents
     * @param currentComponents
     * @return List of updates
     */
    public List<Update> compareComponents(List<Component> previousComponents, List<Component> currentComponents){
        
        //Search for removed
        for (Component previousComponent : previousComponents) {
            boolean previousComponentRemoved = true;
            
            for (Component currentComponent : currentComponents) {
                if(previousComponent.getName().equals(currentComponent.getName())){
                    previousComponentRemoved = false;
                }
            }
            
            if (previousComponentRemoved) {
                removeComponent(previousComponent);
            }
        }
        
        //Search for added and modified
        for (Component currentComponent : currentComponents) {
            boolean currentComponentAdded = true;
            
            for (Component previousComponent : previousComponents) {
                if(previousComponent.getName().equals(currentComponent.getName())){
                    currentComponentAdded = false;
                    
                    //Check if the component changed
                    if(changeInComponent(previousComponent, currentComponent)){
                        removeComponent(previousComponent);
                        addComponent(currentComponent);
                    }
                }
            }
            
            if (currentComponentAdded) {
                addComponent(currentComponent);
            }
        }
        
        return this.updates;
    }


    
    /**
     * Check if component has been modified.
     * 
     * A Component can be classified as modified if it has the same name, but one or more attributes changed:<br>
     * - Implementation<br>
     * - Properties<br>
     * - Services<br>
     * - Reference<br>
     * 
     * @param previousComponent
     * @param currentComponent
     * @return <b>true</b> if component has changed <b>false</b> otherwise
     */
    public boolean changeInComponent(Component previousComponent, Component currentComponent) {
        
        //Check if the implementation changed
        if (!previousComponent.getImplementation().getClass().equals( currentComponent.getImplementation().getClass() ) ) {
            LOG.info("Implementation type changed in " + previousComponent.getName() + 
                        ". From: " + previousComponent.getImplementation() + 
                        ". To: " +currentComponent.getImplementation());
            return true;
        }else{
            if (previousComponent.getImplementation() instanceof JavaImplementationImpl &&
                    currentComponent.getImplementation() instanceof JavaImplementationImpl) {
                JavaImplementation previousJavaImplementation = (JavaImplementation) previousComponent.getImplementation();
                JavaImplementation currentJavaImplementation = (JavaImplementation) currentComponent.getImplementation();
                
                if (!previousJavaImplementation.getClass_().equals(currentJavaImplementation.getClass_())) {
                    LOG.info("Implementation class changed in the component '" + previousComponent.getName() + 
                            "'. From: " + previousJavaImplementation.getClass_() + 
                            ". To: " + currentJavaImplementation.getClass_());
                    return true;
                }
            }
        }
        
        if(changeInProperties(previousComponent.getProperty(), currentComponent.getProperty())
                || changeInServices(previousComponent.getService(), currentComponent.getService())
                || changeInReferences(previousComponent, currentComponent) ){
            return true;
        }
        
        
        return false;
        
    }
    

    /**
     * Check if one or more properties in two lists have changed.<br>
     * 
     * A Property can be classified as modified if it changes:<br>
     * - Name<br>
     * - Type<br>
     * - Value<br>
     * - If it is found in a list, but not in another<br>
     * 
     *  
     * @param list1
     * @param list2
     * @return <b>true</b> if some property has changed <b>false</b> otherwise
     */
    public boolean changeInProperties(List<PropertyValue> list1, List<PropertyValue> list2) {

        if (list1.size() != list2.size()) {
            //changed!
            return true;
        }

      
        for (PropertyValue property1 : list1) {
            
            boolean  propertyFound = false;
            
            for (PropertyValue property2 : list2) {
                if (property1.getName().equals(property2.getName())) {
                    
                    propertyFound = true;
                    
                    //Same property, different value -> changed
                    if ( ! property1.getValue().equals( property2.getValue() ) ) {
                        return true;
                    }
                    
                    //Same property, type changed
                    if (property1.getType() == null ) {
                        if(property2.getType() != null ){
                            return true;
                        }
                    }else{
                        if (! property1.getType().getLocalPart().equals(property2.getType().getLocalPart()) ) {
                            return true;
                        }
                    }
                }
            }
            
            //If a property in the previous component is not found in the current 
            if (!propertyFound) {
                return true;
            }
        }
        
        return false;
    }
    
    
    /**
     * Check if one or more services in two lists have changed.<br>
     * 
     * A Service can be classified as modified if it changes:<br>
     * - Name<br>
     * - Interface Type<br>
     * - Binding<br>
     * - If it is found in a list, but not in another<br>
     * 
     *  
     * @param list1
     * @param list2
     * @return <b>true</b> if some service has changed <b>false</b> otherwise
     */
    public boolean changeInServices(List<ComponentService> list1, List<ComponentService> list2) {

        if (list1.size() != list2.size()) {
            //changed!
            return true;
        }
        
        for (ComponentService service1 : list1) {
            
            System.out.println("SERVICE: " + service1);
            
            boolean  serviceFound = false;

            for (ComponentService service2 : list2) {
                if (service1.getName().equals(service2.getName())) {
                    
                    serviceFound = true;
                    
                    if (service1.getInterface() instanceof JavaInterfaceImpl) {
                        if ( !(service2.getInterface() instanceof JavaInterfaceImpl)) {
                            //changed!
                            return true;
                        }
                        
                        JavaInterface javaInterface1 = (JavaInterface)(service1.getInterface());
                        JavaInterface javaInterface2 = (JavaInterface)(service2.getInterface());
                        
                        if (javaInterface1.getInterface() != null) {
                            if (!javaInterface1.getInterface().equals(javaInterface2.getInterface())) {
                                //changed!
                                return true;
                            }
                        }else if (javaInterface2.getInterface() != null) {
                            return true;
                        }
                    }

                    if (changeInBindings(service1.getBinding(), service2.getBinding())) {
                        return true;
                    }
                }
            }
            
            //If a service in the previous component is not found in the current 
            if (!serviceFound) {
                return true;
            }
        }
        
        return false;
    }
    
    
    /**
     * Check if one or more references in two lists have changed.<br>
     * 
     * A Reference can be classified as modified if it changes:<br>
     * - Name<br>
     * - Interface Type<br>
     * - Binding<br>
     * - Target<br>
     * - If it is found in a list, but not in another<br>
     * 
     *  
     * @param previousComponent
     * @param currentComponent
     * @return <b>true</b> if some reference has changed <b>false</b> otherwise
     */
    public boolean changeInReferences(Component previousComponent, Component currentComponent) {

        if (previousComponent.getReference().size() != currentComponent.getReference().size()) {
            //changed!
            return true;
        }

        
        for (ComponentReference reference1 : previousComponent.getReference()) {

            boolean  referenceFound = false;
            
            for (ComponentReference reference2 : currentComponent.getReference()) {
                if (reference1.getName().equals(reference2.getName())) {
                    
                    referenceFound = true;
                    
                    if (reference1.getInterface() instanceof JavaInterfaceImpl) {
                        if ( !(reference2.getInterface() instanceof JavaInterfaceImpl)) {
                            //changed!
                            System.out.println("One interface Java, other not");
                            return true;
                        }
                        
                        JavaInterface javaInterface1 = (JavaInterface)(reference1.getInterface());
                        JavaInterface javaInterface2 = (JavaInterface)(reference2.getInterface());
                        
                        if (javaInterface1.getInterface() != null) {
                            if (!javaInterface1.getInterface().equals(javaInterface2.getInterface())) {
                                //changed!
                                System.out.println("Interface changed: " + javaInterface1.getInterface() + " TO " + javaInterface2.getInterface());
                                return true;
                            }
                        }else if (javaInterface2.getInterface() != null) {
                            System.out.println("First interface null, second = "+javaInterface2.getInterface());
                            return true;
                        }
                    }

                    if (changeInBindings(reference1.getBinding(), reference2.getBinding())) {
                        System.out.println("Binding changed!");
                        return true;
                    }
                    
                    System.out.println("TARGET of " + reference1.getName() + ": " + reference1.getTarget());
                    System.out.println(reference1.getTarget2());
                    if (reference1.getTarget() != null) {
                        if (reference2.getTarget() == null) {
                            removeWire(previousComponent, reference1, reference1.getTarget2(), reference2.getTarget());
                        }else if (!reference1.getTarget().equals(reference2.getTarget())) {
                            removeWire(previousComponent, reference1, reference1.getTarget2(), reference2.getTarget());
                            addWire(currentComponent, reference2, reference2.getTarget2(), reference2.getTarget());
                        }
                    }else if (reference2.getTarget() != null) {
                        addWire(currentComponent, reference2, reference2.getTarget2(), reference2.getTarget());
                    }

                }
            }
            
            //If a service in the previous component is not found in the current 
            if (!referenceFound) {
                System.out.println("Reference " + reference1.getName() + " doesn't exist in the new version!");
                return true;
            }
        }
        
        return false;
    }
    
    
    /**
     * Check if one or more bindings in two lists have changed.<br>
     * 
     * A Binding can be classified as modified if it changes:<br>
     * - Name<br>
     * - URI<br>
     * - If it is found in a list, but not in another<br>
     * 
     *  
     * @param list1
     * @param list2
     * @return <b>true</b> if some binding has changed <b>false</b> otherwise
     */
    public boolean changeInBindings(List<Binding> list1, List<Binding> list2) {

        if (list1.size() != list2.size()) {
            //changed!
            return true;
        }

      
        for (Binding binding1 : list1) {
            
            boolean  bindingFound = false;
            
            for (Binding binding2 : list2) {
                if (binding1.getName().equals(binding2.getName())) {
                    
                    bindingFound = true;
                    
                    //Same binding, different URI -> changed
                    if ( ! binding1.getUri().equals( binding2.getUri() ) ) {
                        return true;
                    }
                }
            }
            
            //If a binding in the previous component is not found in the current 
            if (!bindingFound) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * Remove component and all wires associated
     * 
     * @param component
     */
    private void removeComponent(Component component) {
        
        //Remove all the wires in references
        for (ComponentReference reference : component.getReference()) {
            removeWire(component, reference, reference.getTarget2(), reference.getTarget());
        }
        
        //Remove all the wires in services
        //First, it needs to find if these components services are referenced by other components
        for (Component otherComponent : previousComposite.getComponent()) {
            for (ComponentReference otherReference : otherComponent.getReference()) {
                for (BaseService service : component.getService()) {
                    if (otherReference.getTarget2() != null) {
                        if (otherReference.getTarget2().equals(service)) {
                            removeWire(otherComponent, otherReference, service, otherReference.getTarget());
                        } 
                    }
                }
            }
        }

        //Remove the Component
        UpdateComponent removeComponent = new RemoveComponent();
        removeComponent.setComponent(component);
        this.updates.add(removeComponent);
    }

    /**
     * Add component and all wires associated
     * 
     * @param component
     */
    private void addComponent(Component component) {
        UpdateComponent addComponent = new AddComponent();
        addComponent.setComponent(component);
        this.updates.add(addComponent);
        
        //Add all the wires in references
        for (ComponentReference reference : component.getReference()) {
            addWire(component, reference, reference.getTarget2(), reference.getTarget());
        }
        
        //Remove all the wires in services
        //First, it needs to find if these components services are referenced by other components
        for (Component otherComponent : currentComposite.getComponent()) {
            for (ComponentReference otherReference : otherComponent.getReference()) {
                for (BaseService service : component.getService()) {
                    if (otherReference.getTarget2() != null) {
                        if (otherReference.getTarget2().equals(service)) {
                            addWire(otherComponent, otherReference, service, otherReference.getTarget());
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Remove wire
     * 
     * @param from
     * @param reference
     * @param service
     * @param target
     */
    private void removeWire(Component from, ComponentReference reference, BaseService service, String target){
        UpdateWire removeWire = new RemoveWire();
        removeWire = setWireParameters(removeWire, from, reference, service, target);
        this.updates.add(removeWire);
    }
    
    /**
     * Add Wire
     * 
     * @param from
     * @param reference
     * @param service
     * @param target
     */
    private void addWire(Component from, ComponentReference reference, BaseService service, String target){
        UpdateWire addWire = new AddWire();
        addWire = setWireParameters(addWire, from, reference, service, target);
        this.updates.add(addWire);
    }
    
    
    /**
     * Generic method to set parameters in UpdateWires
     * 
     * @param updateWire
     * @param from
     * @param reference
     * @param baseService
     * @param target
     * @return updateWire
     */
    private UpdateWire setWireParameters(UpdateWire updateWire, 
                                    Component from, 
                                    ComponentReference reference, 
                                    BaseService service,
                                    String target){
        
        updateWire.setFrom(from);
        updateWire.setReference(reference);
        updateWire.setService(service);
        updateWire.setTarget(target);
        
        return updateWire;
    }
}