/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.EMFModelUtils;
import org.easysoa.api.ServiceManager;
import org.easysoa.api.StudioGUIRest;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class StudioGUIRestImpl implements StudioGUIRest {

	@Reference
    protected ServiceManager serviceManager;
    @Reference
    protected ComplexProcessorItf processor;
    @Reference
    protected EMFModelUtils emfModelUtils;
	
	 @Override
	    public String getCompositeTree(String userId) {
	        Composite composite = serviceManager.getComposite(userId);
	        return this.processor.getMenuItem(composite, "").toJSONString();
	    }
	 
	 @Override
	    public synchronized String getComponentContent(String userId, String elementId) {
	        return this.processor.getPanel(userId, emfModelUtils.getComponent(userId, elementId));
	    }

	    @Override
	    public synchronized String getComponentMenu(String userId, String elementId) {
	        return this.processor.getActionMenu(emfModelUtils.getComponent(userId, elementId));
	    }
}
