/**
 * EasySOA
 * 
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto, Philippe Merle
 *
 */

package org.easysoa.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.xml.namespace.QName;

import org.easysoa.api.AdminAccess;
import org.easysoa.api.CompositeTemplateProcessorItf;
import org.easysoa.api.FileManager;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.api.ServiceManager;
import org.easysoa.api.Users;
import org.easysoa.model.Application;
import org.easysoa.model.User;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
//import org.nuxeo.common.utils.FileUtils;
import org.apache.commons.io.FileUtils;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.processor.ProcessingContextImpl;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.component.factory.impl.ComponentFactoryContextImpl;
import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.core.ParsingContextImpl;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * Service Manager
 */
@Scope("COMPOSITE")
public class ServiceManagerImpl implements ServiceManager {

    /** Logger */
    public final static Logger LOG = Logger.getLogger(ServiceManagerImpl.class.getCanonicalName());

    
    @Reference
    public Provider<EntityManager> database;
    @Reference
    public Parser<Composite> compositeParser;
    @Reference
    public CompositeTemplateProcessorItf templates;
    @Reference
    public FileManager fileManager;
    @Reference
    protected Users connection;
    @Reference
    protected MembraneGeneration membraneGeneration;
    @Reference
    protected AdminAccess deploymentAccess;
    @Reference
    protected PreferencesManagerItf preferences;
    @Reference 
    protected ClassLoaderManager classloaderManager;

    private Map<String,Composite> composite = new HashMap<String, Composite>();
    private Map<String,Application> application = new HashMap<String, Application>();

    /**
     * @see ServiceManager#createApplication(User, String, String, String, String, Map)
     */
    @Override
    public String createApplication(User user, String name, String description,
            String packageName, String templateName, Map<String, Object> params) throws Exception{
    	String result = "";
        params.put("compositeName", name);
        params.put("login", user.getLogin());
        params.put("appName", name);
        Application application = new Application();
        application.setDescription(description);
        application.setName(name);
        if(packageName == null || packageName.equals("")){
        	packageName = "org.ow2.frascati";
        }
        application.setPackageName(packageName);
        try {
        	params.put("userId", user.getId().toString());
            String userPath = user.getWorkspaceUrl();
            application.setCurrentWorskpacePath(preferences.getWorkspacePath());
            
            application.setRoot(userPath + File.separator + name);
            
            File serviceDirectory = new File(application.retrieveAbsoluteRoot());
            serviceDirectory.mkdir();

            application.setResources(application.getRoot() + File.separator + "src"
                    + File.separator + "main" + File.separator + "resources");
            
            File resourcesDirectory = new File(application.retrieveAbsoluteResources());
            resourcesDirectory.mkdirs();
            
            application.setCompositeLocation(application.getResources() + File.separator 
                    + (String) params.get("compositeName")+ ".composite");
            

            if(packageName != null && !packageName.equals("")){
            	packageName = packageName.replace(".", File.separator);
            }
            
            
            String pathPackageDirectory = application.getRoot() + File.separator + "src"
                    + File.separator + "main" + File.separator + "java";
            
            if(packageName != null && !packageName.equals("")){
            	pathPackageDirectory += File.separator + packageName; 
            }
            application.setSources(pathPackageDirectory);
            
            File packageDirectory = new File(application.retrieveAbsoluteSources());
            
            packageDirectory.mkdirs();
            File apiDirectory = new File(packageDirectory.getPath()
                    + File.separator + "api");
            apiDirectory.mkdirs();
            File implDirectory = new File(packageDirectory.getPath()
                    + File.separator + "impl");
            implDirectory.mkdirs();
            
            File libDirectory = new File(application.retrieveAbsoluteRoot() + File.separator + "lib");
            libDirectory.mkdir();

            // HACK for Discovery proxy app
            // Copy required jar in app lib folder
            // Get *.jar from folder httpDiscoveryProxyJars
            // TODO : Once maven OK
            // - a : copy required dependencies directmy in global lib folder (eg : ${user.home}/Documents/frascati-studio/lib)
            // - b : copy HTTP proxy dependencies in cloud war lib folder 
            try{
                URL jarFolderURL = this.getClass().getResource("/appJars");
                if(jarFolderURL != null){
                    File file = new File(jarFolderURL.toURI());
                    String[] fileList = file.list();
                    // Copy these files in lib directory of the generated app
                    for(String fileToCopy : fileList){
                        if(fileToCopy.endsWith(".jar")){
                            URL jarToCopyURL = this.getClass().getResource("/appJars/" + fileToCopy);
                            File sourceJar = new File(jarToCopyURL.toURI());
                            File targetJar = new File(application.retrieveAbsoluteRoot() + File.separator + "lib" + File.separator + fileToCopy);
                            FileUtils.copyFile(sourceJar, targetJar);
                        }
                    }
                }
            }
            catch(Exception ex){
                LOG.severe("Error trying to copy app dependecies jar's in app lib folder : "+ ex.getMessage());
                ex.printStackTrace();
            }
            // HACK for Discovery proxy app END            

            // EASYSOA HACK START
            // This hack make a copy of cxf proxy intent configuration file for each Proxy WS App created on Frascati studio
            // TODO : Copy this file in a global resources folder instead of copy it in each application
            try{
                URL intentConfUrl = this.getClass().getResource("/cxfProxyIntentConf/cxfProxyIntentConf.composite");
                if(intentConfUrl != null){            
                    File intentConfFileSource = new File(intentConfUrl.toURI());
                    File intentConfFileTarget = new File(application.retrieveAbsoluteRoot() + File.separator + "src"
                            + File.separator + "main" + File.separator + "resources" + File.separator + "cxfProxyIntentConf.composite");
                    FileUtils.copyFile(intentConfFileSource, intentConfFileTarget);
                }
            }
            catch(Exception ex){
                LOG.severe("Error trying to copy cxf intent composite file in app resources folder : "+ ex.getMessage());
                ex.printStackTrace();
            }            
            // EASYSOA HACK END

//            FrascatiClassLoader cl = new FrascatiClassLoader(this.classloaderManager.getClassLoader());
            ComponentFactoryContext context = new ComponentFactoryContextImpl(this.classloaderManager.getClassLoader());
    		try {
//    			membraneGeneration.open(cl);
                membraneGeneration.open(context);
    		} catch (Exception e1) {
    			e1.printStackTrace();
    		}
            
//            membraneGeneration.addJavaSource(application.retrieveAbsoluteRoot() + File.separator + "src"
    		context.addJavaSourceDirectoryToCompile(application.retrieveAbsoluteRoot() + File.separator + "src"
                    + File.separator + "main" + File.separator + "java");
//            membraneGeneration.addJavaSource(application.retrieveAbsoluteRoot() + File.separator + "src"
    		context.addJavaSourceDirectoryToCompile(application.retrieveAbsoluteRoot() + File.separator + "src"
                    + File.separator + "main" + File.separator + "resources");
            
            this.application.put(user.getId().toString(), application);
            result = this.initializeComposite(application,application.retrieveAbsoluteCompositeLocation(), templateName,
                    params);
            	
            if(!result.isEmpty()){
            	return result;
            }
            
            File mainDirectory = new File(application.retrieveAbsoluteRoot() + File.separator + "src"
                    + File.separator + "main");
            
            this.initializeContribution(mainDirectory, name,
                    (String) params.get("compositeName"));

            membraneGeneration.close(context);
         
            user.getProvidedApplications().add(application);

            EntityManager entityManager = database.get();
            entityManager.getTransaction().begin();
            try {
                entityManager.persist(application);

                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                
                LOG.severe("Error trying to create a service: "+ e.getMessage());
                e.printStackTrace();
                return e.getMessage();
            }
            
        } catch (Exception e) {
            //e.printStackTrace();
        	File rootFile = new File(application.retrieveAbsoluteRoot());
        	if(rootFile != null && rootFile.exists()){
        		this.deleteDirectory(rootFile);
        	}
            throw e;
        }
        return result;
    }

    private void initializeContribution(File mainDirectory, String name,
            String compositeName) {
        // TODO inject template in a file
        StringBuilder toInject = new StringBuilder();
        toInject.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        toInject.append("<contribution xmlns=\"http://www.osoa.org/xmlns/sca/1.0\">");
        toInject.append("    <deployable composite=\"" + compositeName
                + ".composite\"/>");
        toInject.append("</contribution>");
        File contributionDirectory = new File(mainDirectory.getPath()
                + File.separator + "contribution");
        contributionDirectory.mkdir();
        File contribFile = new File(contributionDirectory.getPath()
                + File.separator + name + ".contrib");
        try {
            contribFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File scaContributionFile = new File(contributionDirectory.getPath()
                + File.separator + "sca-contribution.xml");
        try {
            scaContributionFile.createNewFile();
            FileWriter fileWriter = new FileWriter(scaContributionFile.getPath());
            BufferedWriter output = new BufferedWriter(fileWriter);
            output.write(toInject.toString());
            output.flush();
            output.close();
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @see ServiceManager#searchComposite(String, User)
     */
    @Override
    public Composite searchComposite(String name, User user) {
        try {
            for (Application application : user.getProvidedApplications()) {
                if (application.getName().equals(name)) {
                    this.application.put(user.getId().toString(), application);
                    this.reloadComposite(user.getId().toString(), CURRENT_VERSION);
                    return this.composite.get(user.getId().toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see ServiceManager#reloadComposite(String, Long)
     */
    @Override
    public Composite reloadComposite(String userId, Long version) {
        try {
        	Application userApplication = application.get(userId);
        	userApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
        	
        	String location = null;
        	if (version == CURRENT_VERSION) {
        	    location = userApplication.retrieveAbsoluteCompositeLocation();
            }else{
                location = userApplication.retrieveAbsoluteResources() +
                        "/version/" + 
                        userApplication.getName()+"."+version.toString();
            }
            
            location = location
                    .replaceAll(File.separator + File.separator, "/");
            FrascatiClassLoader classLoader = new FrascatiClassLoader(this.classloaderManager.getClassLoader());
            String sourceFile = null;
            
            // Give the path until java directory
            if(userApplication.getPackageName() != null && !userApplication.getPackageName().equals("")){
            	sourceFile = userApplication.retrieveAbsoluteSources().substring(
                    0,
                    userApplication.retrieveAbsoluteSources().indexOf(
                    		userApplication.getPackageName().replace(".",
                                    File.separator)));
            }
            else{
            	sourceFile = userApplication.retrieveAbsoluteSources();
            }
            classLoader.addUrl(new URL("file://" + sourceFile+'/'));
            String resourceFile = userApplication.retrieveAbsoluteResources();
            System.out.println("file://" + resourceFile);
            classLoader.addUrl(new URL("file://" + resourceFile+'/'));

            // Get the current thread's context class loader and set it.
            ClassLoader previousCurrentThreadContextClassLoader =
                FrascatiClassLoader.getAndSetCurrentThreadContextClassLoader(classLoader);

            Composite composite = null;
            try {
              composite = compositeParser.parse(
            		          new QName("file://" + location),
            		          new ProcessingContextImpl(classLoader));
            } finally {
              // Reset the previous current thread's context class loader.
              FrascatiClassLoader.setCurrentThreadContextClassLoader(previousCurrentThreadContextClassLoader);
            }

            if (version == CURRENT_VERSION) {
                this.composite.put(userId,composite);
            }
            
            return composite;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private String initializeComposite(Application application,
            String compositeFile, String templateName,
            Map<String, Object> params) throws Exception{
    	String result = "";
        try {
        	result = this.templates.doActionBeforeCreation(templateName, params); 
        	if(!result.isEmpty()){
            	return result;
            }
            String template = this.templates.getTemplate(templateName, params);
            FileWriter fileWriter = new FileWriter(compositeFile);
            BufferedWriter output = new BufferedWriter(fileWriter);
            output.write(template);
            output.flush();
            output.close();
            fileWriter.close();
            Composite composite = (Composite) compositeParser.parse(new QName(
                    "file://" + compositeFile), new ParsingContextImpl());
            fileManager.saveComposite(composite, compositeFile);
            this.setComposite((String)params.get("userId"), composite);
            this.reloadComposite((String)params.get("userId"), CURRENT_VERSION);
            result = this.templates.doActionAfterCreation(templateName, params, composite, application); 
            if(!result.equals("")){
            	return result;
            }
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    /**
     * @see ServiceManager#saveFile(String, String)
     */
    @Override
    public void saveFile(String fileUrl, String fileContent) {
        File file = new File(fileUrl);
        try {
            FileWriter fileWriter = new FileWriter(file, false);
            BufferedWriter output = new BufferedWriter(fileWriter);

            output.write(fileContent);
            output.flush();
            output.close();
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @see ServiceManager#deleteApplication(User, String)
     */
    @Override
    public User deleteApplication(User user, String name) {
        Application toRemoveApplication = null;
        for(Application application : user.getProvidedApplications()){
            if(application.getName().equals(name)){
                toRemoveApplication = application;
            }
        }
        EntityManager entityManager = database.get();
        entityManager.getTransaction().begin();
        try {
        	deploymentAccess.removeDeployedApplication(toRemoveApplication.getId());
        	user.getProvidedApplications().remove(toRemoveApplication);
            entityManager.remove(toRemoveApplication);
            
            File file = new File(preferences.getWorkspacePath() + File.separator + user.getWorkspaceUrl() + File.separator
                    + name);
            this.deleteDirectory(file);
            
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to delete a service: "+e.getMessage());
            e.printStackTrace();
        }
        

        return user;
    }

    private void deleteDirectory(File file) {
        File[] fileList = file.listFiles();
        for (File fileToBeDeleted : fileList) {
            if (fileToBeDeleted.isDirectory()) {
                deleteDirectory(fileToBeDeleted);
                fileToBeDeleted.delete();
            } else {
                fileToBeDeleted.delete();
            }
        }
        file.delete();
    }

    /**
     * @see ServiceManager#searchApplications(String)
     */
    @Override
    public List<Application> searchApplications(String keywords) {
        List<Application> applications = new ArrayList<Application>();
        String[] keywordsTab = keywords.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < keywordsTab.length; i++) {
            String keyword = keywordsTab[i];
            stringBuilder.append(" a.description LIKE '%" + keyword + "%'");
            if (i != keywordsTab.length - 1) {
                stringBuilder.append("OR");
            }
        }

        EntityManager entityManager = database.get();
        Query query = entityManager
                .createQuery("SELECT a FROM Application a WHERE"
                        + stringBuilder.toString());
        applications = query.getResultList();
        return applications;
    }

    /**
     * @see ServiceManager#createFile(String, String, String, String)
     */
    @Override
    public void createFile(String userId, String type, String fileName, String apiOrImpl) {
    	InputStream inputStream = null;
    	BufferedReader bufferedReader = null;
    	PrintWriter printWriter = null;
        try {
            LOG.info("createFile# type : " + type + " fileName : "
                    + fileName + " apiOrImpl : " + apiOrImpl);

            File location = null;
            Application userApplication = this.application.get(userId);
            userApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
            if ("Script".equals(type)) {
                location = new File(userApplication.retrieveAbsoluteResources()
                        + File.separator + "scripts");
                location.mkdirs();
                fileName = fileName.replace("/", File.separator);
            } else if ("Velocity".equals(type)) {
                String[] fileNameVel = fileName.split("/");
                location = new File(userApplication.retrieveAbsoluteResources()
                        + File.separator + fileNameVel[0]);
                if (!location.exists()) {
                    location.mkdir();
                }
                fileName = fileNameVel[1];

            } else if ("Java".equals(type)) {
                fileName = apiOrImpl + File.separator + fileName;
                location = new File(userApplication.retrieveAbsoluteSources());
            } else if ("Composite".equals(type)) {
                location = new File(userApplication.retrieveAbsoluteResources());
            }
            File createdFile = new File(location.getPath() + File.separator
                    + fileName);
            createdFile.createNewFile();
            if ("Java".equals(type)) {
                String classFileName = createdFile.getName();
                String[] divClassFileName = classFileName.split("\\.");
                classFileName = divClassFileName[0];
                if ("impl".equals(apiOrImpl)) {
                    inputStream = ServiceManagerImpl.class
                            .getResourceAsStream("/class.template");
                } else if ("api".equals(apiOrImpl)) {
                    inputStream = ServiceManagerImpl.class
                            .getResourceAsStream("/interface.template");
                }
                bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line;
                printWriter = new PrintWriter(createdFile.getPath());
                while ((line = bufferedReader.readLine()) != null) {
                    if (line.contains(":packageName")) {
                    	if(userApplication.getPackageName() != null && !userApplication.getPackageName().equals("")){
                    		line = line.replace(":packageName",
                        		userApplication.getPackageName() + "."
                                        + apiOrImpl);
                    	}
                    	else{
                    		line = line.replace(":packageName", apiOrImpl);
                    	}
                    }
                    if (line.contains(":className")) {
                        line = line.replace(":className", classFileName);
                    }
                    printWriter.println(line);
                }
                printWriter.flush();
                printWriter.close();
                bufferedReader.close();
                inputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
        	if(printWriter != null){
        		printWriter.close();
        	}
        	if(bufferedReader != null){
        		try{
        			bufferedReader.close();
        		}
        		catch(IOException ioe){
        			ioe.printStackTrace();
        		}
        	}
        	if(inputStream != null){
        		try{
        			inputStream.close();
        		}
        		catch(IOException ioe){
        			ioe.printStackTrace();
        		}
        	}
        }
    }

    /**
     * @see ServiceManager#getComposite(String)
     */
    @Override
    public Composite getComposite(String userId) {
        return this.composite.get(userId);
    }

    /**
     * @see ServiceManager#setComposite(String, Composite)
     */
    @Override
    public void setComposite(String userId, Composite composite) {
        this.composite.put(userId, composite);
    }

    /**
     * @see ServiceManager#getCurrentApplication(String)
     */
    @Override
    public Application getCurrentApplication(String userId) {
        return this.application.get(userId);
    }

    /**
     * @see ServiceManager#isFileInApplication(String, String)
     */
    @Override
    public String isFileInApplication(String userId, String file) {
        Application userApplication = this.application.get(userId);
        userApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
        String url = null;
        if(userApplication.getPackageName() != null && !userApplication.getPackageName().equals("")){
        	url = this.searchInSources(
                file,
                userApplication.retrieveAbsoluteSources().substring(
                        0,
                        userApplication.retrieveAbsoluteSources().indexOf(
                        		userApplication.getPackageName().replace(".",
                                        File.separator)) - 1));
        }
        else{
        	url = this.searchInSources(file, userApplication.retrieveAbsoluteSources());
        }
        if (url == null) {
            url = this.searchInResources(file, userApplication.retrieveAbsoluteResources());
        }
        return url;
    }

    private String searchInResources(String file, String resources) {
        File dir = new File(resources);
        String[] packageItems = file.split("/");
        if (packageItems.length > 0) {
            for (File f : dir.listFiles()) {
                if (f.getName().equals(packageItems[0])) {
                    if (f.isFile() && packageItems.length == 1) {
                        return f.getPath();
                    }
                    if (f.isDirectory() && packageItems.length > 1) {
                        return searchInResources(
                                file.substring(file.indexOf("/") + 1),
                                f.getPath());
                    }
                }
            }
        }
        return null;
    }

    private String searchInSources(String file, String sources) {
        File dir = new File(sources);
        String[] packageItems = file.split("\\.");
        if (packageItems.length > 0) {
            for (File f : dir.listFiles()) {
                if ((f.isFile() && f.getName()
                        .substring(0, f.getName().indexOf("."))
                        .equals(packageItems[0]))
                        || (f.isDirectory() && f.getName().equals(
                                packageItems[0]))) {
                    if (f.isFile() && packageItems.length == 1) {
                        return f.getPath();
                    }
                    if (f.isDirectory() && packageItems.length > 1) {
                        return searchInSources(
                                file.substring(file.indexOf(".") + 1),
                                f.getPath());
                    }
                }
            }
        }
        return null;
    }

    /**
     * @see ServiceManager#getAllTarget(String)
     */
    @Override
    public List<String> getAllTarget(String userId) {
        List<String> targets = new ArrayList<String>();
        targets.add("");
        for (Component component : this.composite.get(userId).getComponent()) {
            for (ComponentService service : component.getService()) {
                targets.add(component.getName() + "/" + service.getName());
            }
        }
        return targets;
    }

    /**
     * @see ServiceManager#changePackage(String, String, String, String)
     */
    @Override
    public void changePackage(String userId, String implemType, String classNameOrigin,
            String apiOrImpl) {
    	FileReader fileReader = null;
    	BufferedReader bufferedReader = null;
    	PrintWriter printWriter = null;
        try {
        	Application userApplication = this.application.get(userId);
        	userApplication.setCurrentWorskpacePath(preferences.getWorkspacePath());
            File fileCreated = new File(userApplication.retrieveAbsoluteSources()
                    + File.separator + apiOrImpl + File.separator
                    + classNameOrigin);
            File copyFile = File.createTempFile("tmp", null);

            fileReader = new FileReader(fileCreated);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            printWriter = new PrintWriter(copyFile);
            while ((line = bufferedReader.readLine()) != null) {
                if (line.startsWith("package")) {
                	if(userApplication.getPackageName() != null && !userApplication.getPackageName().equals("") && !userApplication.getPackageName().equals("null")){
                		line = line.replace(line.substring(line.indexOf("package") + 7), " "
                            + userApplication.getPackageName() + "."
                            + apiOrImpl + ";");
                	}
                	else{
                		line = line.replace(line.substring(line.indexOf("package") + 7), " "
                                + apiOrImpl + ";");
                	}
                }
                printWriter.println(line);
            }
            printWriter.flush();
            printWriter.close();
            fileReader.close();
            bufferedReader.close();

            fileReader = new FileReader(copyFile);
            bufferedReader = new BufferedReader(fileReader);
            printWriter = new PrintWriter(fileCreated);
            while ((line = bufferedReader.readLine()) != null) {
            	printWriter.println(line);
            }
            printWriter.flush();

        } catch (Exception e) {
            e.printStackTrace();
        } finally{
        	if(printWriter != null){
        		printWriter.close();
        	}
        	if(fileReader != null){
        		try{
        			fileReader.close();
        		}
        		catch(IOException ioe){
        			ioe.printStackTrace();
        		}
        	}
        	if(bufferedReader != null){
        		try{
        			bufferedReader.close();
        		}
        		catch(IOException ioe){
        			ioe.printStackTrace();
        		}
        	}
        }
    }
    
    /**
     * @see ServiceManager#getPackageMirror(String)
     */
    @Override
    public String getPackageMirror(String userId){
        String packageName = this.application.get(userId).getPackageName();
        if(packageName == null || packageName.equals("")){
        	return "";
        }
        String[] items = packageName.split("\\.");
        String[] res = new String[items.length];
        int itemIndex = items.length-1;
        for(String s : items){
            res[itemIndex] = s;
            itemIndex--;
        }
        String packageMirror = "";
        itemIndex = 0;
        for(String s : res){
            packageMirror+=s;
            if(itemIndex!=res.length-1){
                packageMirror+=".";
            }
            itemIndex++;
        }
        return packageMirror;
    }
    
    /**
     * File copy method
     * @param source Source file
     * @param target Target file
     * @throws Exception If a problem occurs
     */
    private void fileCopy(File source, File target) throws Exception {
        // input channel
        FileChannel in = null;
        // Output channel
        FileChannel out = null;
         
        try {
          // Init
          in = new FileInputStream(source).getChannel();
          out = new FileOutputStream(target, false).getChannel();
         
          // Copy
          in.transferTo(0, in.size(), out);
        } catch (Exception ex) {
          throw new Exception("An error occurs during the copy of " + source.getName(), ex);
        } finally { // closing channels
          if(in != null) {
            try {
              in.close();
            } catch (IOException e) {}
          }
          if(out != null) {
            try {
              out.close();
            } catch (IOException e) {}
          }
        }        
    }
}
