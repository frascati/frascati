/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import java.io.FileNotFoundException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import org.easysoa.api.CodeGenerator;
import org.easysoa.api.CompositeTemplateProcessorItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.api.TemplateRest;
import org.easysoa.api.Users;
import org.easysoa.model.User;
import org.easysoa.utils.WsdlInformations;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class TemplateRestImpl implements TemplateRest {

	@Reference
    protected CompositeTemplateProcessorItf compositeTemplates;
	@Reference
    protected ServiceManager serviceManager;
    @Reference
    protected Users users;
    @Reference
    protected CodeGenerator codeGenerator;
	
	@Override
    public String getTemplateForm(String templateName) {
        return this.compositeTemplates.getForm(templateName);
    }

	// TODO #FRASCATI-107 bad escaping :
	// manually encode / decode each param on both sides,
	// or on server-side let FraSCAti do the escaping by rather passing a Map-like argument,
	// or on client side find a more automated way (implementation.widget, see helloworld-widget example ?)
	@Override
    public String createApplication(String params) {
        try {
            params = URLDecoder.decode(params, "UTF-8");
            String[] paramsArray = params.split("&");
            Map<String, Object> map = new HashMap<String, Object>();
            for (String param : paramsArray) {
                int equalsIndex = param.indexOf('='); // patch for #FRASCATI-106
                if(equalsIndex > 0){
                    // TODO : problem when working with WSDL form nuxeo, the nuxeo URL is used in the generated composite
                    // App is successfully created but cannot be deployed
                    // How to use the "soap:address"(eg:location="http://168.192.0.28/WS/ContactSvc.asmx) in the wsdl instead of the 
                    // the nuxeo url
                    
                    // Many solutions, depends on the case 
                    // - for proxyws : use the service endpoint registered in the wsdl
                    // - for mock : use the provided endpoint
                    String paramName = param.substring(0, equalsIndex);
                    String paramValue = param.substring(equalsIndex + 1);
                    map.put(paramName, paramValue);
                }
            }
            
            // Replace the WSDL URL by the endpoint url
            // TODO : Add a test to know when the url must be replaced by the endpoint URL or by user provided URL
            // TODO : activate the generic webResourceDownloadService and test with online services
            if (map.get("url") != null) {
                map.put("wsdlLocation", map.get("url"));
                WsdlInformations wsdlInformations = codeGenerator.loadWsdl((String)map.get("url"));
                map.put("url", wsdlInformations.getEndpointUrls().get(map.get("servicePort")) + "?wsdl");
            } // else no url : ex. BasicService template
            
            User user = users.searchUser((String) map.get("user"));
            return serviceManager.createApplication(user, (String) map.get("name"),
                    (String) map.get("description"),
                    "", (String) map.get("template"),
                    map);
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

	@Override
    public String loadWSDL(String wsdlPath) throws FileNotFoundException{
        return codeGenerator.loadWsdlString(wsdlPath);
    }
	

}
