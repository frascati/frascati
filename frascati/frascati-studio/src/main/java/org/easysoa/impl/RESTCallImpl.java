/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.xml.namespace.QName;

import org.easysoa.api.BindingProcessorItf;
import org.easysoa.api.ComplexProcessorItf;
import org.easysoa.api.EMFModelUtils;
import org.easysoa.api.FileManager;
import org.easysoa.api.ImplementationsProcessorItf;
import org.easysoa.api.IntentManager;
import org.easysoa.api.InterfaceProcessorItf;
import org.easysoa.api.ModelAccess;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.api.RESTCall;
import org.easysoa.api.ServiceManager;
import org.easysoa.api.Users;
import org.easysoa.deploy.DeployProcessor;
import org.easysoa.model.Application;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.WSDLPortType;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiFactory;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.ScriptImplementation;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.metamodel.web.VelocityImplementation;
import org.ow2.frascati.metamodel.web.WebFactory;
import org.ow2.frascati.metamodel.web.WebPackage;

@Scope("COMPOSITE")
public class RESTCallImpl implements RESTCall {

	@Reference
	protected ServiceManager serviceManager;
	@Reference
	protected FileManager fileManager;
	@Reference
	protected Users users;
	@Reference
	protected ComplexProcessorItf processor;
	@Reference
	protected ImplementationsProcessorItf implementations;
	@Reference
	protected InterfaceProcessorItf interfaces;
	@Reference
	protected BindingProcessorItf bindings;
	@Reference
	protected Provider<EntityManager> database;
	@Reference
	protected ModelAccess modelAccess;
	@Reference
	protected EMFModelUtils emfModelUtils;
	@Reference
	protected PreferencesManagerItf preferences;
	@Reference
	protected IntentManager intentManager;

	/** Logger */
	public final static Logger LOG = Logger.getLogger(DeployProcessor.class
			.getCanonicalName());

	@Override
	public synchronized void addElement(String userId, String elementId,
			String action) {
		this.emfModelUtils.addElement(userId, elementId, action);
	}

	@Override
	public void saveElement(String userId, String params) {
		try {
			LOG.info("saveElement : " + params);
			params = URLDecoder.decode(params, "UTF-8");
			LOG.info(params);
			String[] paramsArray = params.split("&");
			Map<String, Object> map = new HashMap<String, Object>();
			for (String param : paramsArray) {
				String[] paramArray = param.split("=");
				// for empty values in form
				if (paramArray.length == 1)
					map.put(paramArray[0], "");
				else
					map.put(paramArray[0], paramArray[1]);
			}
			EObject eobject = emfModelUtils.getComponent(userId,
					(String) map.get("id"));

			eobject = this.processor.saveElement(eobject, map);
			this.modifyModel(userId, (String) map.get("id"), eobject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void modifyModel(String userId, String elementId, EObject eobject) {
		LOG.info("modify model");
		String[] ids = elementId.split(" ");
		Composite composite = serviceManager.getComposite(userId);
		// composite
		if (ids.length == 1) {
			composite = (Composite) eobject;
		} else if (ids[0].equals("component")) {
			EList<Component> components = composite.getComponent();
			for (Component component : components) {
				if (component.getName().equals(ids[1])) {
					// component name
					if (ids.length == 2) {
						component = (Component) eobject;

					}
					// component name implementation
					else if (ids.length == 3) {
						org.eclipse.stp.sca.Implementation osoaImplementation = transform(component
								.getImplementation());
						EReference eReference = null;
						if (osoaImplementation instanceof org.eclipse.stp.sca.SCAImplementation) {
							eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE;
						}
						if (osoaImplementation instanceof org.eclipse.stp.sca.JavaImplementation) {
							eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_JAVA;
						}
						if (osoaImplementation instanceof VelocityImplementation) {
							eReference = WebPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_VELOCITY;
						}
						if (osoaImplementation instanceof ScriptImplementation) {
							eReference = FrascatiPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT;
						}
						// TODO: manage other implementations, as BPEL, Spring,
						// etc.

						((FeatureMap.Internal) component
								.getImplementationGroup()).clear();
						if (eReference != null) {
							((FeatureMap.Internal) component
									.getImplementationGroup())
									.add(org.eclipse.stp.sca.ScaPackage.Literals.COMPONENT__IMPLEMENTATION_GROUP,
											org.eclipse.emf.ecore.util.FeatureMapUtil
													.createEntry(eReference,
															osoaImplementation));
						}
					} else {
						// component name property name
						if (ids[2].equals("property")) {
							for (PropertyValue property : component
									.getProperty()) {
								if (property.getName().equals(ids[3])) {
									property = (PropertyValue) eobject;
								}
							}
						} else if (ids[2].equals("reference")) {
							LOG.info("reference");
							for (ComponentReference componentReference : component
									.getReference()) {
								if (componentReference.getName().equals(ids[3])) {
									// component name reference name
									if (ids.length == 4) {
										if (eobject instanceof ComponentReference) {
											componentReference = (ComponentReference) eobject;
										} else {
											Iterator<Binding> iterator = componentReference
													.getBinding().iterator();
											while (iterator.hasNext()) {
												Binding binding = iterator
														.next();
												if (binding.getName().equals(
														ids[5])) {
													binding = (Binding) eobject;
												}

											}
										}
									}
									// component name reference name binding uri
									// name
									else if (ids.length == 6
											&& ids[4].equals("binding")) {
										Iterator<Binding> iterator = componentReference
												.getBinding().iterator();
										while (iterator.hasNext()) {
											Binding binding = iterator.next();
											if (binding.getName()
													.equals(ids[5])) {
												binding = (Binding) eobject;
											}

										}
									} else if (ids.length == 5) {
										Interface interf = (Interface) eobject;
										componentReference.setInterface(interf);
									}
								}
							}
						} else if (ids[2].equals("service")) {
							for (ComponentService componentService : component
									.getService()) {
								if (componentService.getName().equals(ids[3])) {
									// component name service name
									if (ids.length == 4) {
										if (eobject instanceof ComponentService) {
											componentService = (ComponentService) eobject;
										} else {
											Iterator<Binding> iterator = componentService
													.getBinding().iterator();
											while (iterator.hasNext()) {
												Binding binding = iterator
														.next();
												if (binding.getName().equals(
														ids[5])) {
													binding = (Binding) eobject;
												}
											}
										}
									}
									// component name service name interface
									else if (ids.length == 5
											&& ids[4].equals("interface")) {
										Interface interf = (Interface) eobject;
										componentService.setInterface(interf);
									}
									// component name service name binding uri
									else if (ids.length == 6
											&& ids[4].equals("binding")) {
										Iterator<Binding> iterator = componentService
												.getBinding().iterator();
										while (iterator.hasNext()) {
											Binding binding = iterator.next();
											if (binding.getName()
													.equals(ids[5])) {
												binding = (Binding) eobject;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		} else if (ids[0].equals("service")) {
			for (Service service : composite.getService()) {
				if (service.getName().equals(ids[1])) {
					// service name
					if (ids.length == 2) {
						if (eobject instanceof Service) {
							service = (Service) eobject;
						} else {
							Iterator<Binding> iterator = service.getBinding()
									.iterator();
							while (iterator.hasNext()) {
								Binding binding = iterator.next();
								if (binding.getName().equals(ids[3])) {
									iterator.remove();
									service.getBinding().add(binding);
									break;
								}

							}
						}
					}
					// service name binding uri
					else if (ids.length == 4 && ids[2].equals("binding")) {
						Iterator<Binding> iterator = service.getBinding()
								.iterator();
						while (iterator.hasNext()) {
							Binding binding = iterator.next();
							if (binding.getName().equals(ids[3])) {
								iterator.remove();
								service.getBinding().add(binding);
								break;
							}

						}
					} else if (ids.length == 3) {
						Interface interf = (Interface) eobject;
						service.setInterface(interf);
					}
				}
			}
		} else if (ids[0].equals("reference")) {
			for (org.eclipse.stp.sca.Reference reference : composite
					.getReference()) {
				if (reference.getName().equals(ids[1])) {
					// reference name
					if (ids.length == 2) {
						if (eobject instanceof org.eclipse.stp.sca.Reference) {
							reference = (org.eclipse.stp.sca.Reference) eobject;
						} else {
							Iterator<Binding> iterator = reference.getBinding()
									.iterator();
							while (iterator.hasNext()) {
								Binding binding = iterator.next();
								if (binding.getName().equals(ids[3])) {
									iterator.remove();
									reference.getBinding().add(binding);
									break;
								}

							}
						}
					}
					// reference name binding uri
					else if (ids.length == 4 && ids[2].equals("binding")) {
						Iterator<Binding> iterator = reference.getBinding()
								.iterator();
						while (iterator.hasNext()) {
							Binding binding = iterator.next();
							if (binding.getName().equals(ids[3])) {
								iterator.remove();
								reference.getBinding().add(binding);
								break;
							}

						}
					} else if (ids.length == 3) {
						Interface interf = (Interface) eobject;
						reference.setInterface(interf);
					}
				}
			}
		}
		Application currentApplication = serviceManager
				.getCurrentApplication(userId);
		currentApplication.setCurrentWorskpacePath(preferences
				.getWorkspacePath());

		fileManager.saveComposite(composite,
				currentApplication.retrieveAbsoluteCompositeLocation());
		serviceManager.setComposite(userId, composite);
		serviceManager.reloadComposite(userId, ServiceManager.CURRENT_VERSION);
	}

	@Override
	public synchronized String getImplementationContent(String userId,
			String modelId, String elementId) {
		return this.implementations.getImplementationView(userId,
				serviceManager.getComposite(userId), modelId, elementId);
	}

	@Override
	public synchronized String getInterfaceContent(String userId,
			String modelId, String elementId) {
		return this.interfaces.getInterfaceView(userId,
				serviceManager.getComposite(userId), modelId, elementId);
	}

	@Override
	public String getBindingContent(String userId, String modelId,
			String elementId) {
		return this.bindings.getBindingView(userId,
				serviceManager.getComposite(userId), modelId, elementId);
	}

	private org.eclipse.stp.sca.Implementation transform(
			Implementation oasisImplementation) {
		// If <implementation.composite>.
		if (oasisImplementation instanceof SCAImplementation) {
			SCAImplementation oasisSCAImplementation = (SCAImplementation) oasisImplementation;
			// Create an OSOA SCAImplementation.
			org.eclipse.stp.sca.SCAImplementation osoaSCAImplementation = ScaFactory.eINSTANCE
					.createSCAImplementation();
			// Copy the composite name.
			osoaSCAImplementation.setName(oasisSCAImplementation.getName());

			// TODO: Copy or transform other properties.

			return osoaSCAImplementation;
		}

		// If <implementation.java>.
		if (oasisImplementation instanceof JavaImplementation) {
			JavaImplementation oasisJavaImplementation = (JavaImplementation) oasisImplementation;
			// Create an OSOA JavaImplementation.
			org.eclipse.stp.sca.JavaImplementation osoaJavaImplementation = ScaFactory.eINSTANCE
					.createJavaImplementation();
			// Copy the Java class name.
			osoaJavaImplementation
					.setClass(oasisJavaImplementation.getClass_());

			// TODO: Copy or transform other properties.

			return osoaJavaImplementation;
		}
		if (oasisImplementation instanceof VelocityImplementation) {
			VelocityImplementation oasisVelocityImplementation = (VelocityImplementation) oasisImplementation;
			// Create an OSOA JavaImplementation.
			VelocityImplementation osoaJavaImplementation = WebFactory.eINSTANCE
					.createVelocityImplementation();
			// Copy the Java class name.
			osoaJavaImplementation.setDefault(oasisVelocityImplementation
					.getDefault());
			osoaJavaImplementation.setLocation(oasisVelocityImplementation
					.getLocation());

			// TODO: Copy or transform other properties.

			return oasisVelocityImplementation;
		}
		if (oasisImplementation instanceof ScriptImplementation) {
			ScriptImplementation oasisScriptImplementation = (ScriptImplementation) oasisImplementation;
			// Create an OSOA JavaImplementation.
			ScriptImplementation osoaScriptImplementation = FrascatiFactory.eINSTANCE
					.createScriptImplementation();
			// Copy the Java class name.
			osoaScriptImplementation.setScript(oasisScriptImplementation
					.getScript());

			// TODO: Copy or transform other properties.

			return oasisScriptImplementation;
		}

		// TODO: BPEL implementation.

		// TODO: Spring implementation.

		// TODO: other implementations.

		// Else do nothing.
		return null;
	}

	@Override
	public String getFileContent(String type) {
		try {
			String url = null;
			if ("implementation".equals(type)) {
				url = this.implementations.getUrl();
			} else if ("interface".equals(type)) {
				url = this.interfaces.getUrl();
			}
			if (url == null) {
				return "";
			}
			File impl = new File(url);

			Scanner scanner = new Scanner(impl);
			StringBuffer stringBuffer = new StringBuffer();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				stringBuffer
						.append(line + System.getProperty("line.separator"));
			}

			scanner.close();
			return stringBuffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public String getEditorMode(String type) {
		if ("implementation".equals(type))
			return this.implementations.getEditorMode();
		else if ("interface".equals(type))
			return this.interfaces.getEditorMode();
		else
			return null;
	}

	@Override
	public void saveFileContent(String content, String type) {
		String url = null;
		if ("implementation".equals(type)) {
			url = this.implementations.getUrl();
		} else if ("interface".equals(type)) {
			url = this.interfaces.getUrl();
		}
		if (url != null) {
			File file = new File(url);
			try {
				FileWriter fileWriter = new FileWriter(file, false);
				BufferedWriter output = new BufferedWriter(fileWriter);

				output.write(content);
				output.flush();
				output.close();
				fileWriter.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getExistingImplementations(String userId) {
		String implementations = "";
		Composite composite = serviceManager.getComposite(userId);
		for (Component component : composite.getComponent()) {
			Implementation implementation = component.getImplementation();
			if (implementation != null) {
				if (implementation instanceof SCAImplementation) {
					SCAImplementation oasisSCAImplementation = (SCAImplementation) implementation;
					if (!implementations.contains(oasisSCAImplementation
							.getName().getLocalPart())) {
						implementations += oasisSCAImplementation.getName()
								.getLocalPart();
					}
				}
				if (implementation instanceof JavaImplementation) {
					JavaImplementation oasisJavaImplementation = (JavaImplementation) implementation;
					if (!implementations.contains(oasisJavaImplementation
							.getClass_())) {
						implementations += oasisJavaImplementation.getClass_();
					}
				}
				if (implementation instanceof VelocityImplementation) {
					VelocityImplementation oasisVelocityImplementation = (VelocityImplementation) implementation;
					if (!implementations.contains(oasisVelocityImplementation
							.getLocation()
							+ "/"
							+ oasisVelocityImplementation.getDefault())) {
						implementations += (oasisVelocityImplementation
								.getLocation() + "/" + oasisVelocityImplementation
								.getDefault());
					}
				}
				if (implementation instanceof ScriptImplementation) {
					ScriptImplementation oasisScriptImplementation = (ScriptImplementation) implementation;
					if (!implementations.contains(oasisScriptImplementation
							.getScript())) {
						implementations += oasisScriptImplementation
								.getScript();
					}
				}
				implementations += ",";
			}
		}
		if (implementations.length() > 0
				&& implementations.lastIndexOf(",") != -1) {
			implementations = implementations.substring(0,
					implementations.length() - 1);
		}
		return implementations;
	}

	@Override
	public void createNewImplementation(String userId, String elementId,
			String className, String implemType, boolean createFile) {
		try {
			LOG.info("createNewImplementation# id : " + elementId
					+ " className : " + className + " implemType : "
					+ implemType);
			boolean hasAPackage = false;
			elementId = URLDecoder.decode(elementId, "UTF-8");
			String classNameOrigin = className;
			if ("Java".equals(implemType)) {
				Application application = this.serviceManager
						.getCurrentApplication(userId);
				if (application.getPackageName() != null
						&& !application.getPackageName().equals("")) {
					className = application.getPackageName()
							+ ".impl."
							+ className
									.substring(0, className.lastIndexOf("."));
				} else {
					className = "impl."
							+ className
									.substring(0, className.lastIndexOf("."));
				}
				hasAPackage = true;
			} else if ("Script".equals(implemType)) {
				className = "scripts/" + className;
			}
			Component component = (Component) emfModelUtils.getComponent(
					userId, elementId);
			Implementation implementation = (Implementation) this.implementations
					.createImplementation(implemType);
			org.eclipse.stp.sca.Implementation osoaImplementation = transform(implementation);
			EReference eReference = null;
			if (osoaImplementation instanceof org.eclipse.stp.sca.SCAImplementation) {
				eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE;
				SCAImplementation scaImplementation = (SCAImplementation) implementation;
				scaImplementation.setName(new QName(className));
				implementation = scaImplementation;
			}
			if (osoaImplementation instanceof org.eclipse.stp.sca.JavaImplementation) {
				eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_JAVA;
				JavaImplementation javaImplementation = (JavaImplementation) implementation;
				javaImplementation.setClass(className);
				implementation = javaImplementation;
			}
			if (osoaImplementation instanceof VelocityImplementation) {
				eReference = WebPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_VELOCITY;
				VelocityImplementation velocityImplementation = (VelocityImplementation) implementation;
				String[] classNameArgs = className.split("/");
				velocityImplementation.setLocation(classNameArgs[0]);
				velocityImplementation.setDefault(classNameArgs[1]);
				implementation = velocityImplementation;
			}
			if (osoaImplementation instanceof ScriptImplementation) {
				eReference = FrascatiPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT;
				ScriptImplementation scriptImplementation = (ScriptImplementation) implementation;
				scriptImplementation.setScript(className);
				implementation = scriptImplementation;
			}
			// TODO: manage other implementations, as BPEL, Spring,
			// etc.
			if (createFile) {
				this.serviceManager.createFile(userId, implemType,
						classNameOrigin, "impl");
			}
			if (hasAPackage) {
				this.serviceManager.changePackage(userId, implemType,
						classNameOrigin, "impl");
			}

			((FeatureMap.Internal) component.getImplementationGroup()).clear();
			if (eReference != null) {
				((FeatureMap.Internal) component.getImplementationGroup())
						.add(org.eclipse.stp.sca.ScaPackage.Literals.COMPONENT__IMPLEMENTATION_GROUP,
								org.eclipse.emf.ecore.util.FeatureMapUtil
										.createEntry(eReference, implementation));
				this.modifyModel(userId, elementId, component);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void createNewBinding(String userId, String elementId,
			String bindingType, String bindingUri) {
		elementId = elementId.replace("+", " ");
		EObject object = emfModelUtils.getComponent(userId, elementId);
		Binding binding = this.bindings.createBinding(bindingType);
		if (binding != null) {
			binding.setName("binding");
			if (!bindingUri.equals("")) {
				binding.setUri(bindingUri);
			}
		}
		if (object instanceof ComponentService) {
			ComponentService component = (ComponentService) object;
			component.getBinding().add(binding);
			this.modifyModel(userId, elementId, component);
		}
		if (object instanceof ComponentReference) {
			ComponentReference component = (ComponentReference) object;
			component.getBinding().add(binding);
			this.modifyModel(userId, elementId, component);
		}
		if (object instanceof Service) {
			Service component = (Service) object;
			component.getBinding().add(binding);
			this.modifyModel(userId, elementId, component);
		}
		if (object instanceof org.eclipse.stp.sca.Reference) {
			org.eclipse.stp.sca.Reference component = (org.eclipse.stp.sca.Reference) object;
			component.getBinding().add(binding);
			this.modifyModel(userId, elementId, component);
		}
	}

	@Override
	public void createNewInterface(String userId, String elementId,
			String className, String interfaceType, boolean createFile,
			String choice) {
		try {
			elementId = URLDecoder.decode(elementId, "UTF-8");
			LOG.info("createNewInterface# id : " + elementId + " className : "
					+ className + " interfaceType : " + interfaceType);
			EObject object = emfModelUtils.getComponent(userId, elementId);
			boolean hasAPackage = false;
			elementId = URLDecoder.decode(elementId, "UTF-8");
			String classNameOrigin = className;
			Interface interf = this.interfaces.createInterface(interfaceType);
			String packageName = null;
			if (interf instanceof JavaInterface) {
				if (!"Use".equals(choice)) {
					hasAPackage = true;
					Application application = this.serviceManager
							.getCurrentApplication(userId);
					if (application.getPackageName() != null
							&& !application.getPackageName().equals("")) {
						packageName = application.getPackageName()
								+ ".api."
								+ className.substring(0,
										className.lastIndexOf("."));
					} else {
						packageName = "api."
								+ className.substring(0,
										className.lastIndexOf("."));
					}
				} else {
					packageName = className;
				}
				((JavaInterface) interf).setInterface(packageName);
			} else if (interf instanceof WSDLPortType) {
				((WSDLPortType) interf).setInterface(className);
			}
			if (createFile) {
				this.serviceManager.createFile(userId, interfaceType,
						classNameOrigin, "api");
			}
			if (hasAPackage) {
				this.serviceManager.changePackage(userId, interfaceType,
						classNameOrigin, "api");
			}

			if (object instanceof ComponentService) {
				ComponentService component = (ComponentService) object;
				component.setInterface(interf);
			}
			if (object instanceof ComponentReference) {
				ComponentReference component = (ComponentReference) object;
				component.setInterface(interf);
			}
			if (object instanceof Service) {
				Service component = (Service) object;
				component.setInterface(interf);
			}
			if (object instanceof org.eclipse.stp.sca.Reference) {
				org.eclipse.stp.sca.Reference component = (org.eclipse.stp.sca.Reference) object;
				component.setInterface(interf);
			}

			this.modifyModel(userId, elementId, object);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean isExistingTown(String town, String country) {
		return modelAccess.isTownExists(town, country);
	}

	@Override
	public boolean hasAnExistingImplementation(String userId, String elementId) {
		return emfModelUtils.hasAnImplementation(userId, elementId);
	}

	@Override
	public boolean hasAnExistingInterface(String userId, String elementId) {
		return emfModelUtils.hasAnInterface(userId, elementId);
	}

	@Override
	public void addIntent(Application application, String userId,
			String elementId, String intentName) {
		try {
			elementId = URLDecoder.decode(elementId, "UTF-8");
			EObject eobject = this.emfModelUtils.addIntent(userId, elementId,
					intentName);
			this.modifyModel(userId, elementId, eobject);
			this.intentManager.initIntent(application, intentName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getIntentImplementation(String name, String userId) {
		Application currentApplication = serviceManager
				.getCurrentApplication(userId);
		if (currentApplication != null) {
			Composite intentComposite = this.emfModelUtils.getIntentComposite(
					currentApplication, name);
			for (Component component : intentComposite.getComponent()) {
				if (component.getImplementation() != null) {
					JavaImplementation javaImplem = (JavaImplementation) component
							.getImplementation();
					try {
						String url = preferences.getWorkspacePath()
								+ File.separator
								+ currentApplication.getSources()
								+ File.separator
								+ "intent"
								+ File.separator
								+ javaImplem.getClass_()
										.substring(
												javaImplem.getClass_()
														.lastIndexOf(".") + 1)
								+ ".java";
						File intent = new File(url);

						Scanner scanner = new Scanner(intent);
						StringBuffer stringBuffer = new StringBuffer();
						while (scanner.hasNextLine()) {
							String line = scanner.nextLine();
							stringBuffer.append(line
									+ System.getProperty("line.separator"));
						}

						scanner.close();
						return stringBuffer.toString();
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				}
			}
		} 
		return null;
	}

	@Override
	public void saveIntent(String name, String userId, String content) {
		Application currentApplication = serviceManager
				.getCurrentApplication(userId);
		if (currentApplication != null) {
			Composite intentComposite = this.emfModelUtils.getIntentComposite(
					currentApplication, name);
			for (Component component : intentComposite.getComponent()) {
				if (component.getImplementation() != null) {
					JavaImplementation javaImplem = (JavaImplementation) component
							.getImplementation();
					try {
						String url = preferences.getWorkspacePath()
								+ File.separator
								+ currentApplication.getSources()
								+ File.separator
								+ "intent"
								+ File.separator
								+ javaImplem.getClass_()
										.substring(
												javaImplem.getClass_()
														.lastIndexOf(".") + 1)
								+ ".java";
						File intent = new File(url);

						FileWriter fw = new FileWriter(intent, false);
						fw.write(content);
						fw.flush();
						fw.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
