/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easysoa.api.OAuth;
import org.easysoa.api.OAuthProcessor;
import org.easysoa.model.User;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Processor for user authentication
 *
 */
@Scope("COMPOSITE")
public class OAuthProcessorImpl implements OAuthProcessor{

	@Reference
    protected List<OAuth> processors;
    protected Map<String, OAuth> processorMap;

    /**
     * Initialize processors
     */
    @Init
    public final void initializeProcessorsByID() {
        this.processorMap = new HashMap<String, OAuth>();
        for (OAuth social : this.processors) {
            this.processorMap.put(social.getId(), social);
        }
    }

    /**
     * @see OAuthProcessor#getAuthentificationUrl(String, HttpServletRequest, HttpServletResponse)
     */
	@Override
	public String getAuthentificationUrl(String id, HttpServletRequest req,
			HttpServletResponse rep) {
		return this.processorMap.get(id).getAuthentificationUrl(req, rep);
	}

	/**
	 * @see OAuthProcessor#getUserInformations(String, HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public User getUserInformations(String id, HttpServletRequest req,
			HttpServletResponse rep) {
		return this.processorMap.get(id).getUserInformations(req,rep);
	}
}
