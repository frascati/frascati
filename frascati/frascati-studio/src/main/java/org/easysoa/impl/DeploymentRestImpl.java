/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.easysoa.api.AdminAccess;
import org.easysoa.api.DeployProcessorItf;
import org.easysoa.api.DeploymentRest;
import org.easysoa.api.FileTypesManagerItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.easysoa.model.DeployedApplication;
import org.easysoa.model.Deployment;
import org.easysoa.model.DeploymentServer;
import org.easysoa.reconfiguration.Reconfiguration;
import org.easysoa.utils.FileManagerImpl;
import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Entry point to call actions related to Deployment
 *
 */
@Scope("COMPOSITE")
public class DeploymentRestImpl implements DeploymentRest {

	@Reference
    protected DeployProcessorItf deploy;
	@Reference 
    protected AdminAccess deploymentAccess;
	@Reference
    protected ServiceManager serviceManager;
	@Reference
    protected Provider<EntityManager> database;
	@Reference
    protected PreferencesManagerItf preferences;
    @Reference
    protected FileTypesManagerItf fileTypesManager;
	
	/** Logger */
    public final static Logger LOG = Logger.getLogger(DeploymentRestImpl.class.getCanonicalName());
	
    /**
     * @see DeploymentRest#deploy(String, String)
     */
	public String deploy(String userId, String ids) {
        LOG.info("deploy");
        LOG.info("ids : " + ids);
        String[] idsTab = ids.split("<>");
        EntityManager entityManager = database.get();
        List<Deployment> deployments = new ArrayList<Deployment>();
        
        /*
         * Retrieve Data relative to the current application and model of current application
         */
        Composite currentModelVersion = serviceManager.getComposite(userId);
        Application application = serviceManager.getCurrentApplication(userId);
        

        for (String serverId : idsTab) {
            DeploymentServer deploymentserver = DeploymentServer.getDeploymentServers(entityManager, serverId);
            DeployedApplication deployedApplication = DeployedApplication.getDeployedApplication(entityManager, deploymentserver, application);

            Deployment deployment = new Deployment();
            deployment.setTargetServer(serverId);
            
            //If deployedApplication is null, the application is not currently deployed in this server 
            if (deployedApplication == null) {
                deployment.setFirstDeployment(true);
            } else {
                Composite previousModelVersion = serviceManager.reloadComposite(userId, deployedApplication.getVersion());
                deployment.setFirstDeployment(false);
                deployment.setPreviousModelVersion(previousModelVersion);
                
                Reconfiguration reconfiguration = new Reconfiguration();
                reconfiguration.compareComposites(previousModelVersion, currentModelVersion);
                
                //DIFF
                
                //deployment.setUpdates(updates here);
            }

        }
        
        application.setGlobalLibPath(preferences.getLibPath());
        application.setLocalLibPath(application.retrieveAbsoluteRoot() + fileTypesManager.getLocationFileType("Library"));
        application.setCurrentWorskpacePath(preferences.getWorkspacePath());
        
        boolean resultDeployment;
        try {
            resultDeployment = this.deploy.deploy(idsTab, application);
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();
            return e.getMessage();
        }
        
        if (resultDeployment) {
            
            entityManager.getTransaction().begin();
            
            try {
                application.incrementVersion();
                
                saveCompositeFileVersion(application);
                
                for (String server : idsTab) {

                    DeploymentServer deploymentServer = this.deploymentAccess.getDeploymentServer(server);
                    if(deploymentServer == null){
                        deploymentServer = new DeploymentServer(server);
                        entityManager.persist(deploymentServer);
                        entityManager.persist(application);
                    }
                    
                    DeployedApplication.registerDeployedApplication(entityManager, application, deploymentServer);

                    
                }
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
                LOG.severe("Error trying to save the data corresponding to the current deployment: " 
                        + e.getMessage());
                e.printStackTrace();
                return "Application deployed successfully, but an error has ocurred while trying to save the data corresponding to the current deployment.";
            }
        }
        
        return "Application deployed successfully!";
    }
    
	/**
	 * @see DeploymentRest#undeploy(String, String)
	 */
    @Override
    public String undeploy(String userId, String ids) {
        LOG.info("undeploy");
        LOG.info("ids : " + ids);
        String[] idsTab = ids.split("<>");
        
        Application application = serviceManager.getCurrentApplication(userId);
        
        boolean resultDeployment;
        try {
            resultDeployment = this.deploy.undeploy(idsTab, application);
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            e.printStackTrace();
            return e.getMessage();
        }
        
        if (resultDeployment) {
            for (String server : idsTab) {
                EntityManager entityManager = database.get();
                entityManager.getTransaction().begin();
                try {
                    DeploymentServer deploymentServer = this.deploymentAccess.getDeploymentServer(server);
                    
                    DeployedApplication.deleteDeployedApplication(entityManager, application, deploymentServer);
                    
                    entityManager.getTransaction().commit();
                } catch (Exception e) {
                    entityManager.getTransaction().rollback();
                    LOG.severe("Error trying to remove data corresponding to the current undeployment: " 
                            + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        
        return "Application undeployed successfully";
    }

    /**
     * @see DeploymentRest#getWebExplorerUrlsInJson(String)
     */
	@Override
	public String getWebExplorerUrlsInJson(String urls) {
		String[] idsTab = urls.split("<>");
		List<String> clouds = new ArrayList<String>();
		for(String name : idsTab){
			clouds.add(name);
		}
        return this.deploy.getWebExplorerUrlsInJson(clouds);
	}
	
	/**
	 * Creates a copy from the current composite file version.<br>
	 * Each version is associated to deployment. When the user deploy the
	 * application another time, the new version is compared to the precedent version
	 * to generate reconfiguration of the differences.
	 * 
	 * 
	 * @param application
	 * @return
	 * @throws IOException
	 */
	private boolean saveCompositeFileVersion(Application application) throws IOException{
	    File mainCompositeFile = new File(application.retrieveAbsoluteCompositeLocation());
	    
	    File versionDir = new File(application.retrieveAbsoluteResources()+ "/version");
	    if (!versionDir.exists()) {
            versionDir.mkdir();
        }
	    
	    File versionedCompositeFile = new File(versionDir.getAbsolutePath() + "/" 
	                                            + application.getName() + "."
	                                            + application.getCurrentVersion()
	                                            + ".composite" );
	    
	    FileManagerImpl.copy(mainCompositeFile, versionedCompositeFile);
	    
	    return true;
	}
}
