/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easysoa.api.OAuth;
import org.easysoa.model.Civility;
import org.easysoa.model.SocialNetwork;
import org.easysoa.model.User;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.GoogleApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class GoogleOAuthImpl extends HttpServlet implements OAuth{

    private static final long serialVersionUID = 1L;
    private static final String API_KEY = "sdleasysoa2.jelastic.com";
	private static final String API_SECRET = "zaoL5ddxKeo7THF_QZP4DjUC";
	private static final String CALLBACK_URL = "http://stutio-frascati-dev.jelastic.dogado.eu/easySoa/index.html?cmd=Google";
	private final static String SCOPE = "https://www.googleapis.com/auth/userinfo.profile";
	private static final String REQUEST = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json";
	
	@Override
	public String getId() {
		return "Google";
	}

	@Override
	public String getAuthentificationUrl(HttpServletRequest req,
			HttpServletResponse rep) {
		OAuthService service = new ServiceBuilder()
		.provider(GoogleApi.class)
		.apiKey(API_KEY)
		.apiSecret(API_SECRET)
		.callback(CALLBACK_URL)
		.scope(SCOPE)
		.build();
		
		Token requestToken = service.getRequestToken();
		req.getSession().setAttribute("token", requestToken);
		req.getSession().setAttribute("service", service);
		
		String authUrl = service.getAuthorizationUrl(requestToken);
		return authUrl;
	}

	@Override
	public User getUserInformations(HttpServletRequest req,
			HttpServletResponse rep) {
		OAuthService service = (OAuthService)req.getSession().getAttribute("service");
		Token requestToken = (Token) req.getSession().getAttribute("token");
		
		Verifier verifier = new Verifier(req.getParameter("oauth_verifier"));
		Token accessToken = service.getAccessToken(requestToken, verifier);
		OAuthRequest request = new OAuthRequest(Verb.GET,REQUEST);
		service.signRequest(accessToken, request);
		Response response = request.send();
		return this.parse(response.getBody());
	}

	private User parse(String body) {
		try{
			JSONObject jsonObjet = new JSONObject(body);
			String id = null;
			Civility civility = null;
			String name = null;
			String surname = null;
			
			if(jsonObjet.getString("family_name")!=null){
				surname = jsonObjet.getString("family_name");
			}
			if(jsonObjet.getString("given_name")!=null){
				name = jsonObjet.getString("given_name");
			}
			if(jsonObjet.getString("id")!=null){
				id = jsonObjet.getString("id");
			}
			User user = new User("",name,surname,"","",civility,null,"",id,SocialNetwork.GOOGLE);
			return user;
		}
		catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
