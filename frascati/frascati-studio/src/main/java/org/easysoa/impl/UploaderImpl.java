/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.easysoa.api.FileTypesManagerItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.model.Application;
import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;
import org.osoa.sca.annotations.Service;

/**
 * Manages uploads in the site
 */
@Scope("COMPOSITE")
@Service(Servlet.class)
public class UploaderImpl extends HttpServlet {
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * Reference to the OW2 FraSCAti composite manager.
     */
    @Reference
    protected ServiceManager serviceManager;
    @Reference
    protected PreferencesManagerItf preferences;
    @Reference
    protected FileTypesManagerItf fileTypesManager;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Gets the upload request from client and uploads and treats the sent file
     * 
     * @see HttpServlet#service(HttpServletRequest, HttpServletResponse)
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException  {
              
        if(ServletFileUpload.isMultipartContent(request)) {
            ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> fileItemList = null;
            try {
                fileItemList = servletFileUpload.parseRequest(request);
            } catch(FileUploadException fue) {
                throw new ServletException(fue);
            }
            String userId = null;
            String componentType = null;
            String fileType = null;
            
            for(FileItem fileItem : fileItemList) {
                if("componentType".equals(fileItem.getFieldName())) {
                    componentType = fileItem.getString();
                }
                if("userIdForm".equals(fileItem.getFieldName())){
                    userId = fileItem.getString();
                }
                if ("file-type".equals(fileItem.getFieldName())){
                    fileType = fileItem.getString();
                }
            }
            
            boolean resource = false;
            
            //TODO Refactoring - Change name from componentType to UploadType
            
            String locationComponentType = null;
            if("implementation".equals(componentType)){
                locationComponentType = "impl";
            }
            else if("interface".equals(componentType)){
                locationComponentType = "api";
            }else if ("resource".equals(componentType)) {
                
                resource = true;
                if("Library".equals(fileType)){
                    locationComponentType = fileTypesManager.getLocationFileType("Library");
                }
            }

            File saveTo = null;
            for(FileItem fileItem : fileItemList) {
                if((fileItem.getFieldName().equals("filename") || fileItem.getFieldName().equals("filenameUploadInterface"))
                        && fileItem.getSize() > 0) {
                    Application application = serviceManager.getCurrentApplication(userId);
                    application.setCurrentWorskpacePath(preferences.getWorkspacePath());

                    String dirPath;
                    
                    if (resource) {
                        dirPath = application.retrieveAbsoluteRoot()+locationComponentType;
                    }else{
                        dirPath = application.retrieveAbsoluteSources()+File.separator+locationComponentType;
                    }
                        
                    File directory = new File(dirPath);
                    if ( ! directory.exists()) {
                        directory.mkdirs();
                    }
                        
                    saveTo = new File(dirPath+File.separator+fileItem.getName());
                    
                    try {
                        fileItem.write(saveTo);
                    } catch(Exception e) {
                        throw new ServletException(e);
                    }
                }
            }

        }
    }
}
