/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): 
 *
 */
package org.easysoa.impl;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easysoa.api.OAuth;
import org.easysoa.model.SocialNetwork;
import org.easysoa.model.User;
import org.json.JSONObject;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.TwitterApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

@Service(Servlet.class)
@Scope("COMPOSITE")
public class TwitterOAuthImpl extends HttpServlet implements OAuth {

    private static final long serialVersionUID = 1L;
    private static final String API_KEY = "Oi4FAURNtHdCTlbWB6JTA";
	private static final String API_SECRET = "0MgvjRmvzYisISLe69AfDZMvpk7kmcwbGy2bwAj6ec";
	private static final String CALLBACK_URL = "http://stutio-frascati-dev.jelastic.dogado.eu/easySoa/index.html?cmd=Twitter";
	private static final String REQUEST = "http://api.twitter.com/1/account/verify_credentials.json";
	
	@Override
	public String getId() {
		return "Twitter";
	}

	@Override
	public String getAuthentificationUrl(HttpServletRequest req,
			HttpServletResponse rep) {
		OAuthService service = new ServiceBuilder()
				.provider(TwitterApi.class)
				.apiKey(API_KEY)
				.apiSecret(API_SECRET)
				.callback(CALLBACK_URL)
				.build();
		req.getSession().setAttribute("service", service);

		Token requestToken = service.getRequestToken();
		req.getSession().setAttribute("token", requestToken);
		String authUrl = service.getAuthorizationUrl(requestToken);
		return authUrl;
	}

	@Override
	public User getUserInformations(HttpServletRequest req,
			HttpServletResponse rep) {
		try {
			OAuthService service = new ServiceBuilder()
					.provider(TwitterApi.class)
					.apiKey(API_KEY)
					.apiSecret(API_SECRET)
					.callback(CALLBACK_URL)
					.build();
			Token requestToken = (Token) req.getSession().getAttribute("token");
			Verifier verifier = new Verifier(req.getParameter("oauth_verifier"));
			Token accessToken = service.getAccessToken(requestToken, verifier);
			OAuthRequest request = new OAuthRequest(Verb.GET,REQUEST);
			service.signRequest(accessToken, request);
			Response response = request.send();
			return this.parse(response.getBody());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private User parse(String body) {
		try{
			JSONObject jsonObjet = new JSONObject(body);
			String name = null;
			String id = null;
			if(jsonObjet.getString("name")!=null){
				name = jsonObjet.getString("name");
			}
			if(jsonObjet.getString("id")!=null){
				id = jsonObjet.getString("id");
			}
			User user = new User("","",name,"","",null,null,"", id, SocialNetwork.TWITTER);
			return user;
		}
		catch(Exception e){
			return null;
		}
	}
}
