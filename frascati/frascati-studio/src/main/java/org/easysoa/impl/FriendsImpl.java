/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */

package org.easysoa.impl;

import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.easysoa.api.Friends;
import org.easysoa.api.MailService;
import org.easysoa.api.Provider;
import org.easysoa.model.FriendRequest;
import org.easysoa.model.Point;
import org.easysoa.model.Town;
import org.easysoa.model.User;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class FriendsImpl implements Friends {

    @Reference
    public Provider<EntityManager> database;
    @Reference
    public MailService mailService;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(FriendsImpl.class.getCanonicalName());

    @Override
    public List<User> getFriendsAroundMe(User user, String kmsRadius) {
        List<User> users = null;
        EntityManager entityManager = database.get();
        try {
            Point userCoord = new Point(user.getTown().getLongitude(), user
                    .getTown().getLatitude());
            Float radius = Float.parseFloat(kmsRadius); // in kms
            Double latitudeLimit = radius / 111.11;// permits to define the min
                                                    // and max
            Double longitudeLimit = radius
                    / (111.11 * Math.cos(userCoord.getLatitude()));
            Double latitudeMax = userCoord.getLatitude() + latitudeLimit;
            Double latitudeMin = userCoord.getLatitude() - latitudeLimit;
            Double longitudeMax = userCoord.getLongitude() + longitudeLimit;
            Double longitudeMin = userCoord.getLongitude() - longitudeLimit;

            Query query = entityManager
                    .createQuery("SELECT t FROM Town t WHERE t.latitude >= :latitudeMin and t.latitude <= :latitudeMax and t.longitude >= :longitudeMin and t.longitude <= :longitudeMax");
            query.setParameter("latitudeMin", latitudeMin);
            query.setParameter("latitudeMax", latitudeMax);
            query.setParameter("longitudeMin", longitudeMin);
            query.setParameter("longitudeMax", longitudeMax);
            List<Town> townList = query.getResultList();
            String townids = "";
            boolean first = true;
            for (Town currentTown : townList) {
                if (isInSelectedRadius(
                        userCoord,
                        new Point(currentTown.getLongitude(), currentTown
                                .getLatitude()), Integer.parseInt(kmsRadius))) {
                    if (first) {
                        first = false;
                    } else {
                        townids = townids.concat(",");
                    }
                    townids = townids
                            .concat(String.valueOf(currentTown.getId()));
                }
            }
            Query queryUsers = entityManager
                    .createQuery("SELECT u from AppUser u where u.town.id in ("
                            + townids + ")");
            users = queryUsers.getResultList();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public List<User> searchFriends(String username, String surname, String town) {
        EntityManager entityManager = database.get();
        Query query = null;
        query = entityManager.createQuery("SELECT u " + "FROM AppUser u "
                + "WHERE (u.userName = :username or " + ":username = '') "
                + "AND (u.surname = :surname or " + ":surname = '') "
                + "AND (u.town.name = :town or " + ":town = '')");
        query.setParameter("surname", surname);
        query.setParameter("username", username);
        query.setParameter("town", town);

        java.util.List<User> search = query.getResultList();
        return search;
    }

    private boolean isInSelectedRadius(Point point1, Point point2, int kmsRadius) {
        double earthRadius = 6371; // km : Earth's radius
        double distanceLat = Math.toRadians(Math.abs(point2.getLatitude()
                - point1.getLatitude()));
        double distanceLon = Math.toRadians(Math.abs(point2.getLongitude()
                - point1.getLongitude()));
        double lat1 = Math.toRadians(point1.getLatitude());
        double lat2 = Math.toRadians(point2.getLatitude());
        
        //Use Haversine formula
        double a_value = Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) + Math.sin(distanceLon / 2)
                * Math.sin(distanceLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c_value = 2 * Math.atan2(Math.sqrt(a_value), Math.sqrt(1 - a_value));
        double d_value = earthRadius * c_value;
        return d_value < kmsRadius;
    }

    @Override
    public void sendFriendRequest(User originUser, User targetUser) {
        FriendRequest friendRequest = new FriendRequest(originUser,
                targetUser);
        EntityManager entityManager = database.get();
        entityManager.getTransaction().begin();
        try {
            entityManager.persist(friendRequest);
            
            entityManager.getTransaction().commit();
            InputStream inputStream = ServiceManagerImpl.class
                    .getResourceAsStream("/templates/friendRequestMail.template");
            mailService.sendMailForFriendRequest(originUser, targetUser, inputStream);
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to send a friend request: "+e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public List<FriendRequest> getFriendRequests(User user) {
        EntityManager entityManager = database.get();
        Query query = entityManager
                .createQuery("SELECT fr FROM FriendRequest fr WHERE fr.targetUser = :user");
        query.setParameter("user", user);
        List<FriendRequest> requests = query.getResultList();
        return requests;
    }

    @Override
    public User acceptFriend(String idFriendRequest) {
        Long idFR = Long.parseLong(idFriendRequest);
        EntityManager entityManager = database.get();
        entityManager.getTransaction().begin();
        User userTarget = null;
        
        try {
            FriendRequest friendRequest = entityManager.find(FriendRequest.class, idFR);
            friendRequest.getOriginUser().getFriends()
                    .add(friendRequest.getTargetUser());
            friendRequest.getTargetUser().getFriends()
                    .add(friendRequest.getOriginUser());
            User userOrigin = entityManager.find(User.class, friendRequest.getOriginUser()
                    .getId());
            userOrigin.setFriends(friendRequest.getOriginUser().getFriends());
            userTarget = entityManager.find(User.class, friendRequest.getTargetUser()
                    .getId());
            userTarget.setFriends(friendRequest.getTargetUser().getFriends());
            friendRequest.getTargetUser().getFriendRequests().remove(friendRequest);
            entityManager.remove(friendRequest);
            
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to accept friend: "+e.getMessage());
            e.printStackTrace();
        }
        
        
        return userTarget;
    }

    @Override
    public User refuseFriend(String idFriendRequest) {
        Long idFR = Long.parseLong(idFriendRequest);
        EntityManager entityManager = database.get();
        FriendRequest friendRequest = entityManager.find(FriendRequest.class, idFR);
        entityManager.getTransaction().begin();
        try {
            friendRequest.getTargetUser().getFriendRequests().remove(friendRequest);
            entityManager.remove(friendRequest);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to delete friend request: "+e.getMessage());
            e.printStackTrace();
        }
        
        return friendRequest.getTargetUser();
    }

    @Override
    public User removeFriend(User user, String idFriend) {
        EntityManager entityManager = database.get();
        User friendx = entityManager.find(User.class, Long.parseLong(idFriend));
        User userx = entityManager.find(User.class, user.getId());
        entityManager.getTransaction().begin();
        try {
            user.getFriends().remove(friendx);
            userx.getFriends().remove(friendx);
            friendx.getFriends().remove(userx);
            
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to remove a friend: "+e.getMessage());
            e.printStackTrace();
        }
        
        return userx;
    }
}
