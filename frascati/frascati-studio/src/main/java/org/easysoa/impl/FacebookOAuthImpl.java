/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.easysoa.api.OAuth;
import org.easysoa.model.Civility;
import org.easysoa.model.SocialNetwork;
import org.easysoa.model.User;
import org.json.JSONObject;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.FacebookApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class FacebookOAuthImpl extends HttpServlet implements OAuth{

    private static final long serialVersionUID = 1L;
    private static final String API_KEY = "381556121883076";
	private static final String API_SECRET = "13ef43c0b18a72154f9593c798b26d6c";
	private static final String CALLBACK_URL = "http://stutio-frascati-dev.jelastic.dogado.eu/easySoa/index.html?cmd=Facebook";
	private final static String SCOPE = "publish_actions,publish_stream,status_update";
	private static final String REQUEST = "https://graph.facebook.com/me";
	
	@Override
	public String getId() {
		return "Facebook";
	}

	@Override
	public String getAuthentificationUrl(HttpServletRequest req,
			HttpServletResponse rep) {
		OAuthService service = new ServiceBuilder()
		.provider(FacebookApi.class)
		.apiKey(API_KEY)
		.apiSecret(API_SECRET)
		.callback(CALLBACK_URL)
		.scope(SCOPE)
		.build();
		req.getSession().setAttribute("service", service);
		String authUrl = service.getAuthorizationUrl(null);
		return authUrl;
	}

	@Override
	public User getUserInformations(HttpServletRequest req,
			HttpServletResponse rep) {
		OAuthService service = (OAuthService)req.getSession().getAttribute("service");
		
		Verifier verifier = new Verifier(req.getParameter("code"));
		Token accessToken = service.getAccessToken(null, verifier);
		OAuthRequest request = new OAuthRequest(Verb.GET,REQUEST);
		service.signRequest(accessToken, request);
		Response response = request.send();
		return this.parse(response.getBody());
	}
	
	private User parse(String body) {
		try{
			JSONObject jsonObjet = new JSONObject(body);
			String id = null;
			Civility civility = null;
			String name = null;
			String surname = null;
			
			if(jsonObjet.getString("gender")!=null || !jsonObjet.getString("gender").equals("")){
				if(jsonObjet.getString("gender").equals("female")){
					civility = Civility.MRS;
				}
				else{
					civility = Civility.MR;
				}
			}
			if(jsonObjet.getString("last_name")!=null){
				surname = jsonObjet.getString("last_name");
			}
			if(jsonObjet.getString("first_name")!=null){
				name = jsonObjet.getString("first_name");
			}
			if(jsonObjet.getString("id")!=null){
				id = jsonObjet.getString("id");
			}
			User user = new User("",name,surname,"","",civility,null,"",id,SocialNetwork.FACEBOOK);
			return user;
		}
		catch(Exception e){
			return null;
		}
	}

}
