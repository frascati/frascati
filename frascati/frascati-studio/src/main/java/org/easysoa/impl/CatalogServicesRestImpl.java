/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s):
 *
 */
package org.easysoa.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.easysoa.api.CatalogServicesProcessorItf;
import org.easysoa.api.CatalogServicesRest;
import org.easysoa.api.EMFModelUtils;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.ServiceManager;
import org.easysoa.api.Users;
import org.easysoa.model.Application;
import org.easysoa.model.Publication;
import org.easysoa.model.User;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.Service;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.osoa.sca.annotations.Reference;

import com.google.common.io.Files;

/**
 * Entry point to Catalog services
 */
public class CatalogServicesRestImpl implements CatalogServicesRest {

	@Reference
	protected CatalogServicesProcessorItf catalogServices;
	@Reference
	protected ServiceManager serviceManager;
	@Reference
	protected Users users;
	@Reference
	protected EMFModelUtils emfUtils;
	@Reference
	protected PreferencesManagerItf preferences;

	/**
	 * @see CatalogServicesRest#getCatalogServices(String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getCatalogServices(String socialNetwork) {
		List<Publication> publications = this.catalogServices
				.getPublications(socialNetwork);
		JSONObject publicationsJson = new JSONObject();
		JSONArray publicationsTab = new JSONArray();
		publicationsJson.put("publications", publicationsTab);
		for (Publication publication : publications) {
			JSONObject publi = new JSONObject();
			publi.put("author", publication.getAuthor());
			publi.put("date", publication.getDate());
			publi.put("description", publication.getDescription());
			JSONArray urlsArray = new JSONArray();
			publi.put("urls", urlsArray);
			for (String url : publication.getUrls()) {
				JSONObject urlObject = new JSONObject();
				urlObject.put("url", url);
				urlsArray.add(urlObject);
			}
			publicationsTab.add(publi);
		}
		return publicationsJson.toJSONString();
	}

	/**
	 * @see CatalogServicesRest#getBindableElements(String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String getBindableElements(String userId) {
		Composite composite = serviceManager.getComposite(userId);
		JSONArray bindable = new JSONArray();
		if (composite.getComponent() != null) {
			for (Component component : composite.getComponent()) {
				if (component.getService() != null) {
					for (ComponentService service : component.getService()) {
						JSONObject object = new JSONObject();
						object.put("name", service.getName());
						object.put("type", "componentService");
						object.put("id", "component+" + component.getName()
								+ "+service+" + service.getName());
						bindable.add(object);
					}
				}
				if (component.getReference() != null) {
					for (ComponentReference reference : component
							.getReference()) {
						JSONObject object = new JSONObject();
						object.put("name", reference.getName());
						object.put("type", "componentReference");
						object.put("id", "component+" + component.getName()
								+ "+reference+" + reference.getName());
						bindable.add(object);
					}
				}
			}
		}
		if (composite.getReference() != null) {
			for (org.eclipse.stp.sca.Reference reference : composite
					.getReference()) {
				JSONObject object = new JSONObject();
				object.put("name", reference.getName());
				object.put("type", "reference");
				object.put("id", "service+" + reference.getName());
				bindable.add(object);
			}
		}
		if (composite.getService() != null) {
			for (Service service : composite.getService()) {
				JSONObject object = new JSONObject();
				object.put("name", service.getName());
				object.put("type", "service");
				object.put("id", "service+" + service.getName());
				bindable.add(object);
			}
		}
		return bindable.toJSONString();
	}

	/**
	 * @see CatalogServicesRest#share(String, String, String)
	 */
	@Override
	public void share(String userId, String socialNetwork, String urls) {
		Application application = serviceManager.getCurrentApplication(userId);
		Composite composite = serviceManager.getComposite(userId);
		User user = users.searchUser(userId);
		String urlBinding = this.searchBinding(composite);
		StringBuffer message = new StringBuffer();
		message.append(user.getLogin()+" hasPublished :\n");
		for(String url : urls.split("<>")){
			url = url.substring(0, url.indexOf("/deploy"));
			message.append("-"+url+urlBinding+"\n");
		}
		message.append(application.getDescription());
		catalogServices.publish(message.toString(), socialNetwork);
	}

	private String searchBinding(Composite composite) {
		for(Service service : composite.getService()){
			if(service.getBinding() != null && service.getBinding().size() > 0){
				return service.getBinding().get(0).getUri();
			}
		}
		return null;
	}

	/**
	 * @see CatalogServicesRest#copyInterfaceFromBindedApplication(String, String, String, String, String)
	 */
	@Override
	public void copyInterfaceFromBindedApplication(String login,String urlBinding,
			String bindedApplication, String userId, String uriCatalogService) {
		Application application = this.serviceManager.getCurrentApplication(userId);
		Composite bindedComposite = this.emfUtils.getCompositeFromApplication(application);
		String interfaceName = null;;
		for(Service service : bindedComposite.getService()){
			if(service.getBinding() != null){
				for(Binding binding : service.getBinding()){
					if(binding.getUri() != null && binding.getUri().endsWith(urlBinding)){
						Interface interfaceBinding = service.getInterface();
						interfaceName = interfaceBinding.getClass().getName();
					}
				}
			}
		}
		String interfacePath = null;
		if(interfaceName != null){
			interfacePath = this.preferences.getWorkspacePath() + application.retrieveAbsoluteSources() + File.separator + "api" +File.separator + interfaceName;
		}
		if(interfacePath != null){
			File bindedInterfaceFile = new File(interfacePath);
			File destinationFile = new File(this.preferences.getWorkspacePath() + application.retrieveAbsoluteSources() + File.separator + "api");
			try {
				Files.copy(bindedInterfaceFile, destinationFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
