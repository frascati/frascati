/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.impl;

import java.io.File;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.easysoa.api.Friends;
import org.easysoa.api.MailService;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.api.Provider;
import org.easysoa.api.Users;
import org.easysoa.model.Civility;
import org.easysoa.model.SocialNetwork;
import org.easysoa.model.Town;
import org.easysoa.model.User;
import org.easysoa.utils.PasswordManager;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Manage users
 */
@Scope("COMPOSITE")
public class UsersImpl implements Users {

	@Reference
    public PreferencesManagerItf preferences;
    @Reference
    public Provider<EntityManager> database;
    @Reference
    public Friends friends;
    @Reference
    public MailService mailService;
    
    /** Logger */
    public final static Logger LOG = Logger.getLogger(UsersImpl.class.getCanonicalName());
    
    /**
     * @see Users#connect(String, String)
     */
    @Override
    public User connect(String login, String password) { 
        try {
            String encryptedPass = PasswordManager.cryptPassword(password);
            EntityManager entityManager = database.get();
            Query query = entityManager.createQuery("SELECT u FROM AppUser u WHERE u.login = :login AND u.password = :password");
            query.setParameter("login", login);
            query.setParameter("password", encryptedPass);
            User user = (User) query.getSingleResult();
            user.setFriendRequests(friends.getFriendRequests(user));
            
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @see Users#createAccount(String, String, String, String, String, String, String, String, String, String)
     */
    @Override
    public User createAccount(String login, String password, String confirmPassword, String mail, String name, String surname, String civility, String town, String country, String birthday){
        String encryptedPass = PasswordManager.cryptPassword(password);
        EntityManager entityManager = database.get();
        User user = null;
        Civility civilityValue = Civility.fromValue(civility);
        Town t = null;
        try {
            t = Town.searchTown(entityManager, town, country);
        }
        catch(Exception e){
            return null;
        }
        try{
            user = new User(login, name, surname, encryptedPass, mail, civilityValue, t, birthday, null, null);
        
            this.createWorkspace(user);
            
            entityManager.getTransaction().begin();
            entityManager.persist(user);
            
            entityManager.getTransaction().commit();
            
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to create accounting: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        
        //TODO - Send mail to user
      
        return user;
    }
    
    /**
     * @see Users#createSocialAccount(User, String, String, String)
     */
    @Override
    public User createSocialAccount(User user,String civility, String townName, String country){
        EntityManager entityManager = database.get();
        Civility civilityValue = Civility.fromValue(civility);
        Town town = null;
        try {
            town = Town.searchTown(entityManager, townName, country);
        }
        catch(Exception e){
            return null;
        }
        try{
            user.setCivility(civilityValue);
            user.setTown(town);
        
            this.createWorkspace(user);
            
            entityManager.getTransaction().begin();
            entityManager.persist(user);
            
            entityManager.getTransaction().commit();
            
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            LOG.severe("Error trying to create accounting: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
        
        //TODO - Send mail to user
      
        return user;
    }

    private void createWorkspace(User user) {
        try{
            String path = preferences.getWorkspacePath();
            File space = new File(path);
            if(!space.exists()){
                space.mkdirs();
            }
            File file = new File(path + File.separator + user.getLogin());
            file.mkdirs();
            user.setWorkspaceUrl(user.getLogin());
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @see Users#searchUser(Long)
     */
    @Override
    public User searchUser(Long userId) {
        EntityManager entityManager = database.get();
        User user = entityManager.find(User.class, userId);
        return user;
    }
    
    /**
     * @see Users#searchUser(String)
     */
    @Override
    public User searchUser(String userIdString) {
        Long userId = Long.parseLong(userIdString);
        return this.searchUser(userId);
    }

    /**
     * @see Users#isSocialUserExist(String, String)
     */
	@Override
	public boolean isSocialUserExist(String socialId, String socialNetwork) {
		EntityManager entityManager = database.get();
        Query query = entityManager.createQuery("SELECT u FROM AppUser u WHERE u.socialNetworkId = :id and u.socialNetwork = :network");
        query.setParameter("id", socialId);
        query.setParameter("network", SocialNetwork.fromValue(socialNetwork));
        try{
        	User user = (User)query.getSingleResult();
        	if(user != null){
        		return true;
        	}
        }
        catch(NoResultException nre){
        	return false;
        }
        return false;
	}
	
	/**
	 * @see Users#connectBySocialId(String, String)
	 */
	@Override
    public User connectBySocialId(String socialId, String socialNetwork) {
        try {
            EntityManager entityManager = database.get();
            Query query = entityManager.createQuery("SELECT u FROM AppUser u WHERE u.socialNetworkId = :id and u.socialNetwork = :network");
            query.setParameter("id",  socialId);
            query.setParameter("network", SocialNetwork.fromValue(socialNetwork));
            User user = (User) query.getSingleResult();
            user.setFriendRequests(friends.getFriendRequests(user));
            
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
