/**
 * EasySOA
 * 
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Michel Dirix
 *
 * Contributor(s): Antonio de Almeida Souza Neto
 *
 */
package org.easysoa.impl;

import java.io.File;
import java.io.FileWriter;
import java.util.Locale;

import org.easysoa.api.IntentManager;
import org.easysoa.api.IntentTemplateItf;
import org.easysoa.api.PreferencesManagerItf;
import org.easysoa.model.Application;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * Intent Manager
 */
@Scope("COMPOSITE")
public class IntentManagerImpl implements IntentManager {

	@Reference
	protected IntentTemplateItf intentTemplate;
	@Reference
	protected PreferencesManagerItf preferences;

	/**
	 * @see IntentManager#initIntent(Application, String)
	 */
	@Override
	public void initIntent(Application application, String intentName) {
		//intentName = logging
		String upname = this.firstCharUp(intentName);
		String compositeContent = this.intentTemplate.getIntentComposite();
		File compositeFileIntent = new File(preferences.getWorkspacePath()+File.separator+application.getResources()+File.separator+intentName+".composite");
		
		try{
			FileWriter fw = new FileWriter(compositeFileIntent);
			fw.write(compositeContent);
			fw.flush();
			fw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		String implementationContent = this.intentTemplate.getIntentImpl();
		File newPackage = new File(preferences.getWorkspacePath()+File.separator+application.getSources()+File.separator+"intent");
		if(!newPackage.exists()){
			newPackage.mkdir();
		}
		File implementationFile = new File(newPackage.getPath()+File.separator+upname+"IntentHandlerImpl.java");
		try{
			FileWriter fw = new FileWriter(implementationFile);
			fw.write(implementationContent);
			fw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		String loggingItfContent = this.intentTemplate.getIntentLoggingItf();
		File loggingInterface = new File(preferences.getWorkspacePath()+File.separator+application.getSources()+File.separator+"impl"+File.separator+"IntentLogging.java");
		try{
			FileWriter fw = new FileWriter(loggingInterface);
			fw.write(loggingItfContent);
			fw.flush();
			fw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private String firstCharUp(String string){
		return string.replaceFirst(string.substring(0, 1), string.substring(0, 1).toUpperCase(Locale.ENGLISH));
	}
}
