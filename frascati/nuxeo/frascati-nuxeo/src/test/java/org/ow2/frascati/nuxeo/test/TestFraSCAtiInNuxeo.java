/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.test;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.LocalDeploy;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceProviderItf;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceBaseItf;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoException;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoServiceException;

/**
 *FraSCAti instance in Nuxeo Test Case
 */
@RunWith(FeaturesRunner.class)
@Features(FraSCAtiInNuxeoFeature.class)
@Deploy({ 
    "org.nuxeo.runtime.bridge",
    "org.ow2.frascati.nuxeo.base"
     })
@LocalDeploy(
        {
          "org.ow2.frascati.nuxeo.base:OSGI-INF/frascati-service.xml"
        })
public class TestFraSCAtiInNuxeo
{
    private final String resourcePath = "target/test-classes/";
    private final String pojoPath = resourcePath  + "helloworld-pojo.jar";
    private final String wsPath = resourcePath  + "helloworld-ws-server.jar";
    private final String servletPath = resourcePath + "frascati-helloworld-servlet.jar";

    protected static final Logger log = Logger.getLogger(
            TestFraSCAtiInNuxeo.class.getCanonicalName());

    private final String STARTED = "STARTED";
    private final String STOPPED = "STOPPED";

    FraSCAtiInNuxeoServiceBaseItf frascatiService = null;
    String fcomponent;

    @Before
    public void setUp() throws Exception
    {
        try
        {            
            frascatiService = (FraSCAtiInNuxeoServiceBaseItf) Framework.getLocalService(
                    FraSCAtiInNuxeoServiceProviderItf.class).getFraSCAtiInNuxeoService();
            
            assertNotNull(frascatiService);
            
            File scaFile = new File(pojoPath);
            scaFile = scaFile.getAbsoluteFile();
            
            fcomponent = frascatiService.processComposite(
                    "helloworld-pojo", scaFile.toURI().toURL());
            
        } catch (Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    @After
    public void tearDown() throws FraSCAtiInNuxeoException
    {
        try
        {
            frascatiService.remove("helloworld-pojo");
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        fcomponent = null;
    }

    /**
     * Test if an existing service can be retrieved
     * 
     * @throws NuxeoFraSCAtiException
     */
    @Test
    public void testGetRunnableServiceAndExecuteIt()
            throws  FraSCAtiInNuxeoException
    {
        Runnable r = null;
        try
        {
            r = (Runnable) frascatiService.getService(fcomponent, 
                    "r", Runnable.class);
            
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
        assertNotNull(r);
        r.run();
    }

    /**
     * Test NuxeoFraSCAtiException is thrown if a service is unknown
     * 
     * @throws NuxeoFraSCAtiException
     */
    @Test(expected = FraSCAtiInNuxeoServiceException.class)
    public void testThrowExceptionIfGetUnexistingService()
            throws FraSCAtiInNuxeoServiceException
    {
        frascatiService.getService(fcomponent, "unknown", Runnable.class);
    }

    @Test
    public void testSCACompositeLifeCycle()
    {
        assertTrue(STARTED.equals(frascatiService.state(fcomponent)));
        frascatiService.stop(fcomponent);
        assertTrue(STOPPED.equals(frascatiService.state(fcomponent)));
        frascatiService.start(fcomponent);
        assertTrue(STARTED.equals(frascatiService.state(fcomponent)));
    }

    @Test
    public void testUnregisterServlet()
    {
        try
        {
            File servletFile = new File(servletPath);
            int n = 0;
            for (; n <= 1; n++)
            {
                String componentServletName = frascatiService.processComposite(
                        "helloworld-servlet.composite", servletFile.toURI().toURL());
                frascatiService.remove(componentServletName);
            }
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (MalformedURLException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    @Test
    public void testUnregisterWS()
    {
        try
        {
            File wsFile = new File(wsPath);
            int n = 0;
            for (; n <= 1; n++)
            {
                String componentWSName = frascatiService.processComposite(
                        "helloworld-ws-server.composite", wsFile.toURI().toURL());
                frascatiService.remove(componentWSName);
            }
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (MalformedURLException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }


}