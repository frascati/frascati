/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.test;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.LocalDeploy;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceProviderItf;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceBaseItf;
import org.ow2.frascati.nuxeo.binding.test.NuxeoBinderItf;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoServiceException;
import org.ow2.frascati.nuxeo.exporting.test.NuxeoExporterItf;

/**
 *FraSCAti instance in Nuxeo Test Case
 */
@RunWith(FeaturesRunner.class)
@Features(FraSCAtiInNuxeoFeature.class)
@Deploy({
        "org.nuxeo.runtime.bridge",
        "org.ow2.frascati.nuxeo.base",
        "frascati.service.bridge",
        "org.ow2.frascati.binding.nuxeo.side"
        })
@LocalDeploy(
        {
          "org.ow2.frascati.nuxeo.base:OSGI-INF/frascati-service.xml"
        })
public class TestFraSCAtiNuxeoBinding
{
    private final String resourcePath = "target/test-classes/";
    private final String exportPath = resourcePath  + "exporting-frascati-side.jar";
    private final String bindPath = resourcePath  + "binding-frascati-side.jar";

    protected static final Logger log = Logger.getLogger(
            TestFraSCAtiNuxeoBinding.class.getCanonicalName());

    FraSCAtiInNuxeoServiceBaseItf frascatiService = null;
    String fcomponent;

    @Before
    public void setUp() throws Exception
    {
        try
        {            
            frascatiService = (FraSCAtiInNuxeoServiceBaseItf) Framework.getLocalService(
                    FraSCAtiInNuxeoServiceProviderItf.class).getFraSCAtiInNuxeoService();
            
            assertNotNull(frascatiService);
            
        } catch (Exception e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

    @Test
    public void testExport() throws Exception
    {
        try
        {
            File exportFile = new File(exportPath);
            frascatiService.processComposite("NuxeoExporter.composite", exportFile.toURI().toURL());            
            String message = Framework.getLocalService(NuxeoExporterItf.class).getExportedMessage();
            log.info("NuxeoExporterItf service message : " + message);
            
            assertNotNull(message);
            
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (MalformedURLException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }
    
    @Test
    public void testBind() throws Exception
    {
        try
        {
            File bindFile = new File(bindPath);
            String compositeName = frascatiService.processComposite(
                    "NuxeoBinder.composite", bindFile.toURI().toURL());      
            NuxeoBinderItf binder = frascatiService.getService(compositeName,
                    "nuxeo-binder-service", NuxeoBinderItf.class);
            assertTrue(binder.binded());
            
        } catch (FraSCAtiInNuxeoServiceException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            
        } catch (MalformedURLException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
        }
    }

}