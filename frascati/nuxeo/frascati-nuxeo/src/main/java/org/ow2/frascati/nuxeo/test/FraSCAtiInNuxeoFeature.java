/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.test;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.nuxeo.common.utils.FileUtils;
import org.nuxeo.runtime.test.WorkingDirectoryConfigurator;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.RuntimeFeature;
import org.nuxeo.runtime.test.runner.RuntimeHarness;
import org.nuxeo.runtime.test.runner.SimpleFeature;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceProviderItf;

@Features(RuntimeFeature.class)
public class FraSCAtiInNuxeoFeature extends SimpleFeature 
implements WorkingDirectoryConfigurator 
{
    public static final String FRASCATI_LIBRARIES_TEST_BASEDIR_PROP = "FRASCATI_LIBRARIES_TEST_BASEDIR";
    
    protected final Logger LOGGER = Logger.getLogger(FraSCAtiInNuxeoFeature.class.getCanonicalName());
    
    public void initialize(FeaturesRunner runner) 
    {
        runner.getFeature(RuntimeFeature.class).getHarness(
                ).addWorkingDirectoryConfigurator(this);
    }

    public void configure(RuntimeHarness harness, File workingDir) throws Exception 
    {
        char sep = File.separatorChar;
        //search for existing resources
        File dot = new File("src");
        dot = dot.getAbsoluteFile().getParentFile();

        String nuxeoFrascatiDirName = System.getProperty(
                FRASCATI_LIBRARIES_TEST_BASEDIR_PROP,"frascati-nuxeo");
        
        File nuxeoFrascati = searchDirectory(dot, null, nuxeoFrascatiDirName);

        if (!nuxeoFrascati.exists()) 
        {
            LOGGER.warning("Unable to retrieve the '"+nuxeoFrascatiDirName+"' directory");
            return;
        }
        LOGGER.info("'"+nuxeoFrascatiDirName+"' directory found : " + nuxeoFrascati.getAbsolutePath());

        File frascatiRootDirSrc = new File(nuxeoFrascati, new StringBuilder("resources"
                ).append(sep).append("frascati").toString());
        
        File frascatiLibDirSrc = new File(frascatiRootDirSrc,"lib");
        File frascatiConfDirSrc = new File(frascatiRootDirSrc,"config");

        //create configuration resources
        String home = workingDir.getAbsolutePath();
        LOGGER.info("Default environment home path: " + home);
        
        String frascatiRootDirDstPath = new StringBuilder(home).append(sep
                ).append("frascati").toString();        
        File frascatiRootDirDst = new File(frascatiRootDirDstPath);

        if (! frascatiRootDirDst.exists() && ! frascatiRootDirDst.mkdirs()) {
            LOGGER.warning("Unable to create the frascati root directory");
            return;
        }        
        LOGGER.info("Default frascati root directory path: " + 
                frascatiRootDirDst.getAbsolutePath());
        
        File frascatiConfDirDst = new File(frascatiRootDirDst,"config");

        if (!frascatiConfDirDst.exists() && !frascatiConfDirDst.mkdir()) {
            LOGGER.warning("Unable to create the 'config' directory");
            return;
        }
        LOGGER.info("Default frascati configuration directory path: " + 
                frascatiConfDirDst.getAbsolutePath());

        File configFileSrc = new File(frascatiConfDirSrc,"frascati_boot.properties");
        File configFileDst = new File(frascatiConfDirDst,"frascati_boot.properties");
        try 
        {
            FileUtils.copy(configFileSrc, configFileDst);
            LOGGER.config(configFileDst + " copied");
            
        } catch (IOException e) 
        {
            LOGGER.log(Level.WARNING,e.getMessage(),e) ;
        }
        File frascatiLibDirDst = new File(frascatiRootDirDst,"lib");
        
        if (!frascatiLibDirDst.exists() && !frascatiLibDirDst.mkdir()) {
            LOGGER.warning("Unable to create the 'lib' directory");
            return;
        }
        LOGGER.info("Default frascati libraries directory path: " + 
                frascatiLibDirDst.getAbsolutePath());
        
        File[] libs = frascatiLibDirSrc.listFiles();

        for (File srclib : libs) {
            String libName = srclib.getName();
            File destlib = new File(frascatiLibDirDst,libName);
            try 
            {
                FileUtils.copy(srclib, destlib);
                LOGGER.config(destlib + " copied");
                
            } catch (IOException e) 
            {
                LOGGER.log(Level.WARNING,e.getMessage(),e) ;
            }
        }
        System.setProperty(
                FraSCAtiInNuxeoServiceProviderItf.FRASCATI_IN_NUXEO_BASEDIR_PROP,
                frascatiRootDirDstPath);
    }

    /**
     * Search for the directory which name is passed on as a parameter. The
     * first occurrence is returned
     * 
     * @param basedir
     *            the directory from where start the research
     * @param caller
     *            the directory from where comes the research
     * @param directoryName
     *            the searched directory's name
     * @return the directory if it has been found
     */
    private File searchDirectory(File basedir, final File caller, final String directoryName) {

        if(directoryName.equals(basedir.getName()))
        {
            return basedir;
        }
        File[] children = basedir.listFiles(new FileFilter() {

            public boolean accept(File f)
            {
                if (f.isDirectory() && (caller == null || !(caller.getAbsolutePath().equals(
                        f.getAbsoluteFile().getAbsolutePath())))) {
                    return true;
                }
                return false;
            }
        });
        for (File child : children)
        {
            if (directoryName.equals(child.getName())) 
            {
                return child.getAbsoluteFile();
            }
            File c = searchDirectory(child.getAbsoluteFile(), basedir, directoryName);
            if (c != null)
            {
                return c.getAbsoluteFile();
            }
        }
        if (caller == null || !caller.getAbsolutePath().equals(basedir.getAbsoluteFile(
                ).getParentFile().getAbsolutePath())) 
        {
            File c = searchDirectory(basedir.getParentFile(), basedir, directoryName);
            if (c != null) 
            {
                return c.getAbsoluteFile();
            }
        }
        return null;
    }

}