/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.base;

import java.io.File;
import org.ow2.frascati.nuxeo.api.AbstractFraSCAtiInNuxeo;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceBaseItf;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoException;

/**
 * Base FraSCAtiInNuxeoServiceProviderItf implementation
 */
public class FraSCAtiInNuxeoBase extends AbstractFraSCAtiInNuxeo<FraSCAtiInNuxeoServiceBaseItf> 
{   
    /**
     * Constructor 
     * 
     * @param librariesDirectory
     * @param parentClassLoader
     * @throws FraSCAtiInNuxeoException
     * @throws Exception
     */
    protected FraSCAtiInNuxeoBase(File librariesDirectory,
            ClassLoader parentClassLoader) throws FraSCAtiInNuxeoException,
            Exception
    {
        super(librariesDirectory, parentClassLoader);
    }
    


}
