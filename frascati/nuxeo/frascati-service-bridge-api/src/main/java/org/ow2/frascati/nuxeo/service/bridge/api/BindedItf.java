/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge.api;

/**
 * FraSCAti binded service as it's seen in the Nuxeo context
 */
public interface BindedItf
{
    /**
     * Nuxeo informs FraSCAti that the service has been started 
     */
    void startBinded();

    /**
     * Nuxeo informs FraSCAti that the service has been stopped 
     */        
    void stopBinded();
    
    /**
     * Nuxeo asks FraSCAti for the Interface class of the service
     */
    String getInterfaceName();
    
    /**
     * Nuxeo asks FraSCAti for the Component name providing the service or the
     * Interface class name of the service
     */
    String getNxservice();
    
}