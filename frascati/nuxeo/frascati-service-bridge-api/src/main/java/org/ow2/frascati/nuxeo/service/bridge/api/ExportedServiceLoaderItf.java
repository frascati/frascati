/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge.api;

/**
 * The signature of the bridge service registered in Nuxeo
 */
public interface ExportedServiceLoaderItf
{ 
    public static final String EXPORTED_SERVICE_LOADER = 
    "org.ow2.frascati.nuxeo.service.bridge.ExportedServiceLoader";

    public static final String EXPORTED_SERVICE_FACTORY = 
    "org.ow2.frascati.nuxeo.service.bridge.ExportedServiceFactory";

    public static final String EXPORTED_SERVICE_COMPONENT = 
    "org.ow2.frascati.nuxeo.service.bridge.ExportedServiceComponent";
    
    /**
     * Create an ExportedDelegateItf implementation instance registered
     * in Nuxeo, using the ExportedItf implementation instance 
     * 
     * @param exported
     *          the ExportedItf implementation instance used
     *          to create the delegate
     * 
     * @return
     *          the ExportedDelegateItf implementation 
     *          instance registered in Nuxeo
     */
    ExportedDelegateItf createExportedDelegate(ExportedItf exported);
    
    /**
     * Create an BindedDelegateItf implementation instance registered
     * in Nuxeo, using the BindedItf implementation instance 
     * 
     * @param binded
     *          the BindedItf implementation instance used
     *          to create the delegate
     * 
     * @return
     *          the BindedDelegateItf implementation 
     *          instance registered in Nuxeo
     */
    BindedDelegateItf createBindedDelegate(BindedItf binded);
}
