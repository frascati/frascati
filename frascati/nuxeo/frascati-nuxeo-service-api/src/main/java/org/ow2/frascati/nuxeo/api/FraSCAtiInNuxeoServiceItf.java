/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.api;

import java.net.URL;

import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoServiceException;

/**
 *  FraSCAti in Nuxeo Service Signature
 */
public interface FraSCAtiInNuxeoServiceItf
{
    /**
     * Process a contribution ZIP archive.
     * 
     * @param contribution
     *            name of the contribution file to load.
     * @return 
     *          an array of loaded components' name          
     * @throws FraSCAtiInNuxeoServiceException
     *             if no component can be found in the contribution file
     */
    String[] processContribution(String contribution) 
    throws FraSCAtiInNuxeoServiceException;

    /**
     * Loads an SCA composite and create the associate FraSCAti composite
     * instance.
     * 
     * @param composite
     *            name of the composite file to load.
     * @param urls
     *            a set of URLs to build the FraSCAti ProcessingContext
     * @return the FraSCAtiCompositeItf embedded resulting FraSCAti composite
     *         instance.
     * @throws FraSCAtiInNuxeoServiceException
     *             if the composite can not be loaded
     */
    String processComposite(String composite, URL... urls)
            throws FraSCAtiInNuxeoServiceException;

    /**
     * Return the state of the component which name is passed on as a
     * parameter 
     * 
     * @param componentName
     *            the name of the component to start
     * @return
     *          "STARTED" if the component is started
     *          "STOPPED" if the component is stopped
     */
    String state(String compositeName);
    
    /**
     * Start the component which name is passed on as parameter
     * 
     * @param componentName
     *            the name of the component to start
     */
    void start(String compositeName);

    /**
     * Stop the component which name is passed on as parameter
     * 
     * @param componentName
     *            the name of the component to stop
     */
    void stop(String compositeName);

    /**
     * Remove from FraSCAti the component which name is passed on as parameter
     * 
     * @param componentName
     *            the name of the component to remove
     * @throws FraSCAtiInNuxeoServiceException
     *             if the component doesn't exist
     */
    void remove(String compositeName) 
            throws FraSCAtiInNuxeoServiceException;

    /**
     * Return the service associated to the component which name is passed on as
     * parameter, with the name and class type also passed on as parameter
     * 
     * @param compositeName
     *            the name of the composite to look the service in
     * @param serviceName
     *            the service name
     * @param serviceClass
     *            the service class
     * @return the service instance
     * @throws FraSCAtiInNuxeoServiceException
     *             if the service has not been found
     */
    <T> T getService(String compositeName, String serviceName, Class<T> serviceClass) 
        throws FraSCAtiInNuxeoServiceException;
    
}
