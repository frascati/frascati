/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.api;

/**
 * Provide an access to a FraSCAtiInNuxeoServiceItf implementation instance
 * in a Nuxeo Context
 */
public interface FraSCAtiInNuxeoServiceProviderItf<FraSCAtiInNuxeoService extends FraSCAtiInNuxeoServiceItf>
{
    /**
     * FraSCAti output directory property
     * */
    public static final String FRASCATI_IN_NUXEO_OUTPUT_DIRECTORY_PROP = 
         "org.ow2.frascati.nuxeo.output.directory";
    
    /**
     * FraSCAti's libraries base directory
     * libraries will be search in a 'lib' directory from this declared path
     */
    public static final String FRASCATI_IN_NUXEO_BASEDIR_PROP = 
        "org.ow2.frascati.nuxeo.config.basedir";
    
    /**
     * The name of component providing the service. It will be search in the FraSCAti's root
     * This field must be defined in the FraSCAtiInNuxeoServiceItf implementing class
     * It must be declared public and static
     */
    public static final String FRASCATI_IN_NUXEO_SERVICE_COMPONENT_NAME_PROP = 
        "FRASCATI_IN_NUXEO_SERVICE_COMPONENT_NAME";

    /**
     * The name of service implementing the FraSCAtiInNuxeoServiceItf interface
     * This field must be defined in the FraSCAtiInNuxeoServiceItf implementing class
     * It must be declared public and static
     */
    public static final String FRASCATI_IN_NUXEO_SERVICE_SERVICE_NAME_PROP = 
        "FRASCATI_IN_NUXEO_SERVICE_SERVICE_NAME";
    
    /**
     * Return a FraSCAtiInNuxeoServiceItf implementation instance  
     */
    FraSCAtiInNuxeoService getFraSCAtiInNuxeoService();
}
