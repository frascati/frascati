========================================================================
 OW2 FraSCAti Nuxeo
 
 Copyright (c) 2011 - 2012 Inria, University of Lille 1
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of 
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 
 Contact: frascati@ow2.org
 
========================================================================

The FraSCAti's 'nuxeo' subproject provides the possibility to launch a FraSCAti
instance in a Nuxeo one. It also contains modules allowing to create a binding 
between the FraSCAti instance and the Nuxeo one, and to expose Nuxeo's services 
through FraSCAti after being reified in SCA components.

'nuxeo' subproject content :
---------------------------
		
	Nuxeo-independent FraSCAti's modules
	------------------------------------
		
		The frascati-event-parser library is not needed to launch a Nuxeo-embedded 
		FraSCAti but the frascati-isolated is.
		
		frascati-event-parser :   
		---------------------
		FaSCAti's Parser's events SCA Components library
		
		frascati-isolated : 
		-----------------
		Allow to launch FraSCAti in an isolated way. To provide isolation,
		the FraSCAti's ClassLoader searches classes in its children first.
		
		FraSCAti instance launching configuration : 
		
			- root directory and parent ClassLoader are passed on as parameters to
			  the FraSCAtiIsolated constructor
			- if the parent ClassLoader argument is null, then the current thread's
			  ClassLoader is used 
			- if the root directory argument is null the value of the 
			  'org.ow2.frascati.isolated.root.basedir' system property is used
		
			FraSCAti's libraries are searched in a 'lib' directory in the root one. If it has been
			found in the 'config' directory in the root one, the 'frascati_boot.properties' file is 
			loaded. Each loaded property is added to system's ones
	
	Binding and Implementation
	--------------------------
		
		To be able to use binding or/and implementation functionalities associated libraries 
		have to be accessible to FraSCAti instantiated in Nuxeo (put them in the isolated 
		FraSCAti's libraries directory) 
		
		frascati-metamodel-nuxeo : 
		------------------------
		
			EMF metamodel definition for SCA <binding.nuxeo> and <implementation.nuxeo>
		
		fractal-bf-connectors-nuxeo :
		---------------------------
		
			Fractal binding factory for SCA <binding.nuxeo>
			
		frascati-binding-nuxeo :
		----------------------
		
			FraSCAti's SCA Component for <binding.nuxeo>
		
		frascati-implementation-nuxeo :		
		-----------------------------
		
			FraSCAti's SCA Components for <implementation.nuxeo>
			
	Shared libraries
	----------------
		
		'frascati-nuxeo-api' and 'frascati-nuxeo-service-api' libraries have to be accessible
		 both to FraSCAti and Nuxeo. If you want to use binding functionalities 'frascati-service-bridge-api'
		 has also to be shared
		
		frascati-nuxeo-api :
		------------------
			
			Two abstract classes are defined : an implementation of the ApplicationFactory 
			interface and an implementation of the Application one (see nuxeo-runtime-bridge 
			library).
			
		frascati-nuxeo-service-api :
		--------------------------
			
			Three interfaces are defined : 
			
				- FraSCAtiInNuxeoServiceProviderItf : signature of the service provided 
				  by the FraSCAti-in-Nuxeo ApplicationComponent(see nuxeo-runtime-bridge library).
				  
				- FraSCAtiInNuxeoServiceItf : Accessible FraSCAti's functionalities in Nuxeo 
				 (FraSCAtiInNuxeoServiceProviderItf#getFraSCAtiInNuxeo() returned service)
				
				- FraSCAtiInNuxeoServiceBaseItf : a first basic extended FraSCAtiInNuxeoServiceItf.
				  It defines two constants : FRASCATI_IN_NUXEO_SERVICE_COMPONENT_NAME , 
				  FRASCATI_IN_NUXEO_SERVICE_SERVICE_NAME. They are used to respectively 
				  retrieve the SCA Component which provides the service, and the name of this last 
				  one. Those constants must be defined in each FraSCAtiInNuxeoServiceItf extended 
				  interface 
			
		frascati-service-bridge-api : 
		--------------------------
			
			Defines interfaces which frame the binding between FraSCAti and Nuxeo
			mechanism (BindedItf,BindedDelegateItf, ExportedItf, ExportedDelegateItf,
			ExportedServiceItf, ExportedServiceLoaderItf)
	
																												
							  {Provide}			   {Call}					 {Call}					{Provide}	
			NuxeoStubContent||----------BindedItf<------->BindedDelegateItf <-------><Nuxeo-Service>------------||
					|		||											|										||
					|		||											|{Create}								||
					|		||					{Call}					|										||
					--------||----------------------------------|		|							{Provide}	||
		FRASCATI			||									|--->ExportedServiceLoaderItf-------------------||	NUXEO
					--------||----------------------------------|		|				 |						||
					|		||					{Call}					|				 |						||
					|		||											|{Create}		 |{Create}				||
					|		||											|				 |						||
		NuxeoSkeletonContent||--------ExportedItf<----->ExportedDelegateItf <---->ExportedServiceItf------------||
							  {Provide}			  {Call}					{Call}					{Provide}	
					
					
	Nuxeo components
	----------------
	
		frascati-nuxeo
		--------------
			It launches an isolated FraSCAti defined as an ApplicationComponent
			(see nuxeo-runtime-bridge library) and provides a FraSCAtiInNuxeoServiceProviderItf 
			service.  It allows to access a FraSCAtiInNuxeoServiceItf service which
			defines FraSCAti's accessible functionalities in Nuxeo
			
		frascati-service-bridge : 
		----------------------
			It provides an ExportedServiceLoaderItf service which is used by the Nuxeo-embedded
			FraSCAti to link with binded Nuxeo's service(s) and register exported FraSCAti's one(s)
			
build
-----

	mvn clean install

run
---

	 - create the isolated FraSCAti's root directory
	 	-- create the 'lib' directory and copy all required libraries in it
	 	-- create the 'config' directory and create the frascati_boot.properties file if it's necessary
	 	   to configure FraSCAti's bootstrap class for example
	 - add shared libraries to the Nuxeo associated directory (<nuxeo-home>/nxserver/lib)
	 - add nuxeo components to the Nuxeo associated directory (<nuxeo-home>/nxserver/plugins) 
	 - launch Nuxeo
 
 NB :
 --
  	* Default FraSCAti's root directory is "<nuxeo-home>/frascati"
 
 	* the only available FraSCAtiInNuxeoServiceItf implementation SCA component is 
 	  defined in the org.ow2.frascati.nuxeo.test:frascati-nuxeo-service-base:1.5-SNAPSHOT 
 	  artifact - Put this library in the FraSCAti's libraries directory if you don't
 	  want to define yours
 	    
 	* see the frascati-nuxeo tests and configuration as example
 