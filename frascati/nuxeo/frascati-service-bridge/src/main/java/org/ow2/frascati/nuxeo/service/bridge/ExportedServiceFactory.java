/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import org.ow2.frascati.nuxeo.service.bridge.ExportedServiceDescriptor;
import org.ow2.frascati.nuxeo.service.bridge.ExportedServiceFactoryItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf;

/**
 * Store the last ExportedServiceItf implementation instance registered
 */
public class ExportedServiceFactory implements ExportedServiceFactoryItf
{
    /**
     * the static ExportedServiceFactory instance
     */
    private static ExportedServiceFactory factory;
    
    /**
     * the registered ExportedServiceItf implementation instance
     */
    private ExportedServiceItf exportedService;
    
    /**
     * Constructor
     */
    public ExportedServiceFactory()
    {
        if(factory == null)
        {
            factory = this;
        }
    }
    
    /**
     * Return the static ExportedServiceFactory instance
     */
    private static ExportedServiceFactory getFactory()
    {
        if(factory == null)
        {
            new ExportedServiceFactory();
        }
        return factory;
    }
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.helper.api.ExportedServiceFactoryItf#
     * createExportedService(org.ow2.frascati.nuxeo.service.bridge.helper.api.ExportedServiceDescriptor)
     */
    public ExportedServiceItf getExportedService(ExportedServiceDescriptor descriptor)
    {
        return getFactory().exportedService;
    }
    
    /**
     * Set the exportedService attribute of the static ExportedServiceFactory instance
     *  
     * @param exportedService
     *          the ExportedServiceItf implementation instance to set
     */
    public static final void setExportedService(ExportedServiceItf exportedService)
    {
        getFactory().exportedService = exportedService;
    }

}
