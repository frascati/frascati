/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.nuxeo.runtime.api.Framework;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf;
import org.ow2.frascati.nuxeo.service.bridge.helper.ExportedServiceStreamRef;
import org.ow2.frascati.nuxeo.service.bridge.helper.url.exported.Handler;

/**
 * Delegate of the Fractal NuxeoSkeleton in the Nuxeo Context
 */
public class ExportedDelegate implements ExportedServiceItf, ExportedDelegateItf
{    
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    private static Logger logger = Logger.getLogger(ExportedDelegate.class.getCanonicalName());
    
    private ExportedItf exported;
    private ExportedServiceStreamRef exportedStreamRef;
    private Class<?> interfaceClass = null;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------    

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * @param exported
     */
    public ExportedDelegate(ExportedItf exported)
    {
        this.exported = exported;       
        try
        {
            interfaceClass = Framework.getRuntime().getContext().loadClass(
                    this.exported.getInterfaceName());
            
        } catch (ClassNotFoundException e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }        
        int hashCode = this.exported.getExportedObject().hashCode();
        hashCode = hashCode<0?0-hashCode:hashCode;
        
        String simpleName = this.exported.getInterfaceName();
        simpleName = simpleName.substring(simpleName.lastIndexOf('.')+1);
        simpleName = simpleName.toLowerCase();
        
        String nxservice = this.exported.getNxservice();
        
        String extensionName = (nxservice!=null && !nxservice.isEmpty())?
                nxservice: new StringBuilder(simpleName).append(
                        "_").append(hashCode).toString();
        
        String componentName = new StringBuilder("frascati.service.bridge.").append(
                extensionName).toString();        
        
        StringBuilder componentBuilder = new StringBuilder("<component name=\"");
        componentBuilder.append(componentName);
        componentBuilder.append("\">");
        componentBuilder.append("<implementation class=\"");
        componentBuilder.append(ExportedServiceLoaderItf.EXPORTED_SERVICE_COMPONENT);
        componentBuilder.append("\" />");
        componentBuilder.append("<property name=\"name\" value=\"");
        componentBuilder.append(extensionName);
        componentBuilder.append( "\" />");
        componentBuilder.append("<service>");
        componentBuilder.append("<provide interface=\"");
        componentBuilder.append(exported.getInterfaceName());
        componentBuilder.append( "\" />");
        componentBuilder.append("</service>");
        componentBuilder.append("<extension target=\"");
        componentBuilder.append(ExportedServiceLoaderItf.EXPORTED_SERVICE_LOADER);
        componentBuilder.append("\" point=\"exported\">");
        componentBuilder.append("<exported name=\"");
        componentBuilder.append(extensionName);
        componentBuilder.append("\" factory=\"");
        componentBuilder.append(ExportedServiceLoaderItf.EXPORTED_SERVICE_FACTORY);
        componentBuilder.append("\" />");
        componentBuilder.append("</extension>");
        componentBuilder.append("</component>");

        this.exportedStreamRef = new ExportedServiceStreamRef(
                componentName,componentBuilder.toString());
        
        Handler.registerExportedStreamRef(componentName, exportedStreamRef);
        
        ExportedServiceFactory.setExportedService(this);        
        try
        {
            Framework.getRuntime().getComponentInstance(
                    ExportedServiceLoaderItf.EXPORTED_SERVICE_LOADER).getContext().deploy(
                    exportedStreamRef);  
            
        } catch (Exception e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
    }
    
    /**
     * if the exported service is stop in FraSCAti then it's removed from the Nuxeo context
     */
    public void stopDelegate()
    {
        try
        {
            Framework.getRuntime().getComponentInstance(
                    ExportedServiceLoaderItf.EXPORTED_SERVICE_LOADER).getContext(
                            ).undeploy(exportedStreamRef);
            
        } catch (Exception e)
        {
            logger.log(Level.WARNING,e.getMessage(),e);
        }
    }
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf#getService(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    public <T> T getService(Class<T> type)
    { 
        if (interfaceClass != null && type.isAssignableFrom(interfaceClass))
        {
            return (T) this.exported.getExportedObject();
        }
        return null;
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf#destroy()
     */
    public void destroy()
    {
        this.exported.stopExported();
        this.exported = null;
        this.exportedStreamRef = null;
    }

}
