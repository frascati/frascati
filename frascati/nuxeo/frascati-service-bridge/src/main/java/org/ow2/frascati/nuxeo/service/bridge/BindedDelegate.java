/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.nuxeo.runtime.ComponentEvent;
import org.nuxeo.runtime.RuntimeService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.ComponentManager;
import org.nuxeo.runtime.model.ComponentName;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedItf;

/**
 * Delegate of the Fractal NuxeoStub in the Nuxeo Context
 */
public class BindedDelegate implements BindedDelegateItf
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    private static Logger logger = Logger.getLogger(BindedDelegate.class.getCanonicalName());

    private BindedItf binded = null;
    private ComponentName instanceName = null;
    private Object serviceObject;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Constructor
     * Register this NuxeoStubContent in the existing Nuxeo context as a 
     * component listener
     */
    public BindedDelegate(BindedItf binded)
    {
        this.binded = binded;        
        RuntimeService runtime = Framework.getRuntime();
        
        if(runtime != null)
        {
            runtime.getComponentManager().addComponentListener(this);
            logger.log(Level.INFO,"Registered as a Nuxeo ComponentListener");
            
        } else 
        {
            logger.log(Level.WARNING,"No Nuxeo RuntimeService found");
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.ComponentListener#handleEvent(org.nuxeo.runtime.ComponentEvent)
     */
    public void handleEvent(ComponentEvent event)
    {
        ComponentName componentInstanceName = event.registrationInfo.getName();
        
        if(this.instanceName != null && ComponentEvent.COMPONENT_UNREGISTERED == event.id &&
                this.instanceName.equals(componentInstanceName))
        {
            //the component doesn't exist anymore so ask the binded object to stop
            this.binded.stopBinded();  
            this.serviceObject = null;
            this.instanceName = null;
            
            //let the binded object search for another component providing 
            //the same service if the component name has not been specified using 
            //the nxservice attribute
            this.binded.startBinded();
            return;
        }
        if(this.instanceName == null && ComponentEvent.COMPONENT_RESOLVED == event.id)
        {
            String[] services = event.registrationInfo.getComponent().getProvidedServiceNames();
            for(String service : services)
            {
                if(service.equals(this.binded.getInterfaceName()))
                {
                    //let the binded object search for the component providing the service 
                    this.binded.startBinded();
                    break;
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.BindedDelegateItf#getServiceObject()
     */
    public Object getServiceObject()
    {
        RuntimeService runtime = Framework.getRuntime();
        if(runtime == null)
        {
            logger.log(Level.WARNING,"No Nuxeo RuntimeService found");
            this.serviceObject = null;
            
        } else if(serviceObject == null)
        {
            ComponentManager manager = runtime.getComponentManager();
            String componentName = this.binded.getNxservice();
            ComponentInstance instance = null;
            
            String interfaceName = this.binded.getInterfaceName();
            Class<?> interfaceClass = null;
            try
            {
                interfaceClass = runtime.getContext().loadClass(interfaceName);
                
            } catch (ClassNotFoundException e)
            {
                logger.log(Level.WARNING,e.getMessage(),e);
            }                 
            if(componentName != null && !componentName.isEmpty())
            { 
               instance = manager.getComponent(new ComponentName(componentName));
            
            } else
            {    
               instance = manager.getComponentProvidingService(interfaceClass);
            }
            if(instance != null && interfaceClass != null)
            {
                instanceName = instance.getName();
                serviceObject = instance.getAdapter(interfaceClass);
                
                logger.info("Service '" + interfaceClass.getSimpleName() + 
                        "' found - provided by the component '" + instanceName + "'"); 
            }         
        }
        return this.serviceObject;
    }
}
