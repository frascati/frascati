/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import java.util.logging.Logger;

import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.ComponentInstance;
import org.nuxeo.runtime.model.DefaultComponent;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf;

/**
 * The exported services loader
 * It is used to register services as extensions 
 */
public class ExportedServiceLoader extends DefaultComponent 
implements ExportedServiceLoaderItf
{
    public static final String EXPORTED_PROTOCOL_PKG = 
        "org.ow2.frascati.nuxeo.service.bridge.helper.url";
    
    public static final String JAVA_PROTOCOL_PKGS = "java.protocol.handler.pkgs";
    
    public static final String EXPORTED = "exported";
    
    private static Logger log = Logger.getLogger(ExportedServiceLoader.class.getCanonicalName());
    
    private static volatile ExportedServiceManager manager;

    /**
     * Constructor
     */
    public ExportedServiceLoader()
    {
       String protocolPackageHandlers = System.getProperty(JAVA_PROTOCOL_PKGS);
       
       if(protocolPackageHandlers == null)
       {
           protocolPackageHandlers = EXPORTED_PROTOCOL_PKG;
           
       } else if(!protocolPackageHandlers.contains(EXPORTED_PROTOCOL_PKG))
       {
           protocolPackageHandlers = new StringBuilder(protocolPackageHandlers).append(
                   "|").append(EXPORTED_PROTOCOL_PKG).toString();
       }
       log.info("register 'exported' protocol package Handler: " + protocolPackageHandlers);
       System.setProperty(JAVA_PROTOCOL_PKGS,protocolPackageHandlers);
    }
    
    /**
     * @return
     */
    public static ExportedServiceManager getApplicationManager()
    {
        return manager;
    }

    /**
     * @param name
     * @return
     */
    public static ExportedServiceItf getExportedService(String name)
    {
        ExportedServiceManager _manager = manager;
        return _manager != null ? _manager.getExportedService(name) : null;
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.DefaultComponent#activate(org.nuxeo.runtime.model.ComponentContext)
     */
    @Override
    public void activate(ComponentContext context) throws Exception
    {
        manager = new ExportedServiceManager();
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.DefaultComponent#deactivate(org.nuxeo.runtime.model.ComponentContext)
     */
    @Override
    public void deactivate(ComponentContext context) throws Exception
    {
        manager = null;
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.DefaultComponent#
     * registerContribution(java.lang.Object, java.lang.String, org.nuxeo.runtime.model.ComponentInstance)
     */
    @Override
    public void registerContribution(Object contribution,
            String extensionPoint, ComponentInstance contributor)
            throws Exception
    {
        if (EXPORTED.equals(extensionPoint))
        {
            ExportedServiceDescriptor desc = (ExportedServiceDescriptor) contribution;
            desc.setBundle(contributor.getContext().getBundle());
            manager.addExportedService(desc);
        }
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.DefaultComponent#
     * unregisterContribution(java.lang.Object, java.lang.String, org.nuxeo.runtime.model.ComponentInstance)
     */
    @Override
    public void unregisterContribution(Object contribution,
            String extensionPoint, ComponentInstance contributor)
            throws Exception
    {
        if (EXPORTED.equals(extensionPoint))
        {
            ExportedServiceDescriptor desc = (ExportedServiceDescriptor) contribution;
            manager.removeExportedService(desc.getName());
        }
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.DefaultComponent#getAdapter(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(Class<T> adapter)
    {        
        if (ExportedServiceManager.class == adapter)
        {
            return (T) manager;
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf#
     * createExportedDelegate(org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf)
     */
    public ExportedDelegateItf createExportedDelegate(ExportedItf exported)
    {
        return new ExportedDelegate(exported);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf#
     * createBindedDelegate(org.ow2.frascati.nuxeo.service.bridge.api.BindedItf)
     */
    public BindedDelegateItf createBindedDelegate(BindedItf binded)
    {
        return new BindedDelegate(binded);
    }
}
