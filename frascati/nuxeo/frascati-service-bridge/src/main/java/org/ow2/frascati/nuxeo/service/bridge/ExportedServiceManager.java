/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf;

/**
 * The exported service manager
 */
public class ExportedServiceManager {

    /**
     * Logger
     */
    private static final Log logger = LogFactory.getLog(ExportedServiceManager.class);

    private final Map<String, ExportedServiceItf> exportedServices = 
        new ConcurrentHashMap<String,ExportedServiceItf>();
    
    /**
     * @param desc
     * @throws Exception
     */
    public void addExportedService(ExportedServiceDescriptor desc) throws Exception 
    {
        ExportedServiceItf exported = load(desc);
        exportedServices.put(desc.getName(), exported);
    }

    /**
     * @param name
     */
    public void removeExportedService(String name) 
    {
        ExportedServiceItf exported = exportedServices.remove(name);
        if (exported != null) 
        {
            logger.info("Unloading FraSCAti's exported service : " + name);
            exported.destroy();
        }
    }

    /**
     * @param <T>
     * @param name
     * @return
     */
    @SuppressWarnings("unchecked")
    public <T extends ExportedServiceItf> T getExportedService(String name) 
    {
        return (T) exportedServices.get(name);
    }

    /**
     * @param desc
     * @return
     * @throws Exception
     */
    protected ExportedServiceItf load(ExportedServiceDescriptor desc) throws Exception 
    {
        logger.info("Loading exported service: " + desc.getName());
        
        ExportedServiceFactoryItf factory = (ExportedServiceFactoryItf)
        desc.getBundle().loadClass(desc.getFactory()).newInstance();
        
        return factory.getExportedService(desc);
    }
}
