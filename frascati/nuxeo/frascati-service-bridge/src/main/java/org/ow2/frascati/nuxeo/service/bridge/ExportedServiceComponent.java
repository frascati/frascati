/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import org.nuxeo.runtime.model.ComponentContext;
import org.nuxeo.runtime.model.DefaultComponent;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceItf;

/**
 * Generic component to integrate a bridged application as a set of Nuxeo services 
 */
public class ExportedServiceComponent extends DefaultComponent
{
    /**
     * The exported service name
     */
    protected String name;

    /**
     * Get the exported service name
     * 
     * @return
     *          the name of the exported service
     */
    public String getName()
    {
        return name;
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.model.DefaultComponent#
     * activate(org.nuxeo.runtime.model.ComponentContext)
     */
    @Override
    public void activate(ComponentContext context) throws Exception
    {
        this.name = (String) context.getPropertyValue("name");
        if (this.name == null)
        {
            throw new IllegalStateException(
                    "No application name was specified "
                            + "in application component descriptor. Bundle: "
                            + context.getRuntimeContext().getBundle());
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.model.DefaultComponent#
     * getAdapter(java.lang.Class)
     */
    @Override
    public <T> T getAdapter(Class<T> adapter)
    {
        ExportedServiceItf exported = ExportedServiceLoader.getExportedService(name);
        return exported != null ? exported.getService(adapter) : null;
    }

}
