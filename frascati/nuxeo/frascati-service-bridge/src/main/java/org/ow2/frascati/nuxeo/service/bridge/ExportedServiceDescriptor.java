/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge;

import java.util.HashMap;
import java.util.Map;

import org.nuxeo.common.xmap.annotation.XNode;
import org.nuxeo.common.xmap.annotation.XNodeMap;
import org.nuxeo.common.xmap.annotation.XObject;
import org.osgi.framework.Bundle;

/**
 * Exported service Descriptor
 */
@XObject("exported")
public class ExportedServiceDescriptor {

    /**
     * the name of the exported service 
     */
    @XNode("@name")
    protected String name;

    /**
     * the exported service's factory class name 
     */
    @XNode("@factory")
    protected String factory;
    
    /**
     * set of properties of the exported service
     */
    @XNodeMap(value="properties/property", key="@name", type=HashMap.class, 
            componentType=String.class, nullByDefault = true)
    protected Map<String,String> properties;
    
    /**
     * the associated bundle
     */
    private Bundle bundle;
    
    /**
     * Return the name of the exported service
     */
    public String getName() 
    {
        return name;
    }

    /**
     * Return the exported service's factory class name
     */
    public String getFactory() {
        return factory;
    }

    /**
     * Return the set of properties associated to the exported service
     */
    public Map<String,String> getProperties() 
    {
        return properties;
    }

    /**
     * Set the name of the exported service
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Set the exported service's factory class name
     */
    public void setFactory(String factory) 
    {
        this.factory = factory;
    }

    /**
     * Set the set of properties associated to the exported service
     */
    public void setResources(Map<String,String> properties) 
    {
        this.properties = properties;
    }

    /**
     * Return the bundle associated to the exported service
     */
    public Bundle getBundle() {
        return bundle;
    }

    /**
     * Set the bundle associated to the exported service
     */
    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }
}
