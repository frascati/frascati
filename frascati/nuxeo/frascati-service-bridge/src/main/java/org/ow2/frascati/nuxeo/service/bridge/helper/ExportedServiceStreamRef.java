/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.ow2.frascati.nuxeo.service.bridge.helper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.nuxeo.runtime.model.StreamRef;
import org.ow2.frascati.nuxeo.service.bridge.ExportedServiceLoader;

/**
 * StremRef for a FraSCAti's Exported Service
 */
public class ExportedServiceStreamRef implements StreamRef
{
    String componentXml;
    String serviceId;
    
    public ExportedServiceStreamRef(String serviceId,String componentXml)
    {
        this.serviceId = serviceId;
        this.componentXml = componentXml;
    }
    
    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.StreamRef#getId()
     */
    public String getId()
    {
        return serviceId;
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.StreamRef#getStream()
     */
    public InputStream getStream() throws IOException
    {
        return new ByteArrayInputStream(componentXml.getBytes());
    }

    /* (non-Javadoc)
     * @see org.nuxeo.runtime.model.StreamRef#asURL()
     */
    public URL asURL()
    {
        try
        {
            return new URL(ExportedServiceLoader.EXPORTED,serviceId,"");
            
        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

}
