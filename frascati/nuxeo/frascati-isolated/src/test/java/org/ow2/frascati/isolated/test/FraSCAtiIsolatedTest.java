/**
 * OW2 FraSCAti
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.isolated.test;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.ow2.frascati.examples.helloworld.pojo.PrintService;
import org.ow2.frascati.isolated.FraSCAtiIsolated;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * FraSCAtiIsolated TestCase
 */
public class FraSCAtiIsolatedTest
{    
    /**
     * Logger
     */
    Logger log = Logger.getLogger(FraSCAtiIsolatedTest.class.getCanonicalName());
    
    /**
     * the FraSCAtiIsolated instance used for tests
     */
    FraSCAtiIsolated isolatedFraSCAti;
    
    /**
     * Create a new instance of an isolated FraSCAti
     */
    @Before
    public void setUp()
    {
        File librariesBaseDir = new File("target/test-classes/frascati").getAbsoluteFile();
        try
        {
            //The parent of the FraSCAti's ClassLoader is the same as the parent of the 
            //current thread ClassLoader - so loaded libraries cannot be shared
            isolatedFraSCAti = new FraSCAtiIsolated(librariesBaseDir,
                    Thread.currentThread().getContextClassLoader().getParent());
            
        } catch (Exception e)
        {
           log.log(Level.SEVERE,e.getMessage(),e);
        }
        assertNotNull(isolatedFraSCAti);
    }
    
    /**
     * The PrintService class in the Test context must be different from the one
     * resolved in the FraSCAti context - wait for a ClassCastException
     *  
     * @throws Exception
     */
    @Test(expected = ClassCastException.class)    
    public void testIsolated() throws Exception
    {    
         //load the helloworl-pojo composite
        Class<?> compositeManagerClass = isolatedFraSCAti.getClassLoader().loadClass(
        "org.ow2.frascati.assembly.factory.api.CompositeManager");
        
        Class<?> classLoaderManagerClass = isolatedFraSCAti.getClassLoader().loadClass(
        "org.ow2.frascati.assembly.factory.api.ClassLoaderManager");
        
        Object compositeManager = isolatedFraSCAti.getService(
                compositeManagerClass,
                "composite-manager",
                "org.ow2.frascati.FraSCAti/assembly-factory");
        
        Object classLoaderManager = isolatedFraSCAti.getService(
                classLoaderManagerClass,
                "classloader-manager",
                "org.ow2.frascati.FraSCAti/assembly-factory");
        
         Object classLoader = classLoaderManagerClass.getDeclaredMethod(
                 "getClassLoader").invoke(classLoaderManager);
         
         compositeManagerClass.getDeclaredMethod("getComposite",
                 new Class<?>[]{ String.class, ClassLoader.class }).invoke(
                         compositeManager,new Object[]{ "helloworld-pojo", 
                                classLoader });
         
         //retrieve the service provided by the composite
         Object serviceInFraSCAti = isolatedFraSCAti.getService(
                 isolatedFraSCAti.getClassLoader().loadClass(
                 "org.ow2.frascati.examples.helloworld.pojo.PrintService"),
                     "printService", "helloworld-pojo/server");
         
         //the service in FraSCAti cannot have the same class as the one resolved in
         //the test context
         PrintService serviceInTest = (PrintService) serviceInFraSCAti;
         fail("No ClassCastException thrown");
    }
    
    /**
     * Stop FraSCAti
     */
    @After
    public void tearDown()
    {
        try
        {
            isolatedFraSCAti.stop();
        } catch (Exception e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
            fail("An error occured while stopping FraSCAti");
        }
    }
    

}



