/**
 * OW2 FraSCAti
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.isolated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The FraSCAtiIsolated class allow to instantiate an isolated FraSCAti
 */
public class FraSCAtiIsolated
{
    /**
     * FraSCAtiIsolated Logger
     */
    protected Logger logger = Logger.getLogger(FraSCAtiIsolated.class.getCanonicalName());

    /**
     * Constant used to set the FraSCAti's root path in System's properties  
     */
    public static final String FRASCATI_ISOLATED_BASEDIR_PROP = 
        "org.ow2.frascati.isolated.root.basedir";

    /**
     * The FraSCAti instance object
     */
    private Object frascati;
    /**
     * The FraSCAti's CompositeManager service
     */
    private Object compositeManager;
    /**
     * The FraSCAti's AssemblyFactory component
     */
    private Object assemblyFactory;
    /**
     * The isolated ClassLoader
     */
    private UpdatableURLClassLoader isolatedCl;

    //Classes used for reflective calls
    private Class<?> componentClass = null;
    private Class<?> frascatiClass = null;
    private Class<?> managerClass = null;
    
    /**
     * Constructor
     * 
     * Instantiate a new FraSCAti, isolated from the context in which it has been
     * launched
     * 
     * @param frascatiRootDirectory
     *            the directory where the FraSCAti's libraries are stored
     * 
     * @throws Exception
     */
    public FraSCAtiIsolated(File frascatiRootDirectory, ClassLoader parentClassLoader) 
    throws Exception
    {        
        //if the librariesDirectory is null
        if (frascatiRootDirectory == null)
        {
            //Use the system's property to find FraSCAti's libraries
            String frascatiRootDirectoryProp = System.getProperty(
                    FRASCATI_ISOLATED_BASEDIR_PROP);

            if (frascatiRootDirectoryProp == null || !(frascatiRootDirectory = 
                new File(frascatiRootDirectoryProp)).exists())
            {
                throw new InstantiationException("Unable to instantiate a new"
                        + " isolated FraSCAti instance : no root directory found");
            }
        }        
        File frascatiLibDirectory = new File(frascatiRootDirectory,"lib");
        if(!frascatiLibDirectory.exists() && !frascatiLibDirectory.mkdir())
        {
            throw new InstantiationException("Unable to instantiate a new"
            + " isolated FraSCAti instance : no libraries directory found");
        }
        //Retrieve all libraries and add them to the ClassLoader classpath 
        File[] libraries = frascatiLibDirectory.listFiles(new FilenameFilter()
        {
            public boolean accept(File file, String name)
            {
                if (name.endsWith(".jar"))
                {
                    return true;
                }
                return false;
            }
        });   
        String bootstrapProperty = System.getProperty("org.ow2.frascati.bootstrap");
        if(bootstrapProperty == null)
        {
            File frascatiConfigDirectory = new File(frascatiRootDirectory,"config");  
            if(frascatiConfigDirectory.exists())
            {
                File frascatiConfigFile = new File(frascatiConfigDirectory,
                        "frascati_boot.properties");
                try
                {
                    Properties props = new Properties();
                    props.loadFromXML(new FileInputStream(frascatiConfigFile));
        
                    Enumeration<Object> keys = props.keys();
                    while (keys.hasMoreElements())
                    {
                        String key = (String) keys.nextElement();
                        String value = props.getProperty(key);
                        System.setProperty(key, value);
                    }
                } catch (Exception e)
                {
                    logger.log(Level.WARNING, e.getMessage(),e);
                }
            }
        }
        if(parentClassLoader != null)
        {
            //Use the ClassLoader passed on as a parameter
            isolatedCl = new UpdatableURLClassLoader(parentClassLoader);
            
        } else
        {
            // Define a new URLClassLoader using a parent which allow to find shared
            // classes if they exist
            isolatedCl = new UpdatableURLClassLoader(
                    Thread.currentThread().getContextClassLoader());
        }
        //Add FraSCAti's libraries to the Isoalted ClassLoader's classpath
        for (File library : libraries)
        {
            isolatedCl.addURL(library.toURI().toURL());
        }
        frascatiClass = isolatedCl.loadClass("org.ow2.frascati.FraSCAti");
        
        // Instantiate the new FraSCAti
        frascati = frascatiClass.getDeclaredMethod("newFraSCAti",
                new Class<?>[] { ClassLoader.class }).invoke(null,
                new Object[] { isolatedCl });

        // Define objects that will be reused in the getService and stop methods...
        
        //the CompositeManager Class
        managerClass = isolatedCl
                .loadClass("org.ow2.frascati.assembly.factory.api.CompositeManager");

        //the Component Class 
        componentClass = isolatedCl
                .loadClass("org.objectweb.fractal.api.Component");

        //the CompositeManager service instance
        compositeManager = frascatiClass.getDeclaredMethod(
                "getCompositeManager").invoke(frascati);

        Object component = managerClass.getDeclaredMethod(
                "getTopLevelDomainComposite").invoke(compositeManager);
        
        //the AssemblyFatory component instance 
        assemblyFactory = getComponent(component,
                "org.ow2.frascati.FraSCAti/assembly-factory");
    }

    /**
     * Find a Fractal Component using a parent of it and its path
     * 
     * @param currentComponent
     *            a parent of the search component
     * @param componentPath
     *            the path of the component
     * @return the component if it has been found. Null otherwise
     * @throws Exception
     */
    private Object getComponent(Object currentComponent, String componentPath)
            throws Exception
    {
        String[] componentPathElements = componentPath.split("/");
        String lookFor = componentPathElements[0];
        String next = null;
        
        //define the part before the first '/'  in the path as being the name 
        //of the next component to find
        if (componentPathElements.length > 1)
        {
            int n = 1;
            StringBuilder nextSB = new StringBuilder();
            for (; n < componentPathElements.length; n++)
            {
                nextSB.append(componentPathElements[n]);
                if (n < componentPathElements.length - 1)
                {
                    nextSB.append("/");
                }
            }
            next = nextSB.toString();
        }
        //Retrieve the ContentController Class to enumerate sub-components
        Class<?> contentControllerClass = isolatedCl
                .loadClass("org.objectweb.fractal.api.control.ContentController");
        //Retrieve the NameController Class to check the name of sub-components
        Class<?> nameControllerClass = isolatedCl
                .loadClass("org.objectweb.fractal.api.control.NameController");
        //Retrieve the ContentController object for the currentComponent
        Object contentController = componentClass.getDeclaredMethod(
                "getFcInterface", new Class<?>[] { String.class }).invoke(
                currentComponent, "content-controller");
        //Retrieve the list of sub-components of the currentComponent
        Object[] subComponents = (Object[]) contentControllerClass
                .getDeclaredMethod("getFcSubComponents", (Class<?>[]) null)
                .invoke(contentController, (Object[]) null);
        //If there is no subComponents ...
        if (subComponents == null)
        {   //then return null 
            return null;
        }
        //For each sub-component found...
        for (Object object : subComponents)
        {
            //retrieve its NameController ... 
            Object nameController = componentClass.getDeclaredMethod(
                    "getFcInterface", new Class<?>[] { String.class }).invoke(
                    object, "name-controller");
            //get its name ...  
            String name = (String) nameControllerClass.getDeclaredMethod(
                    "getFcName", (Class<?>[]) null).invoke(nameController,
                    (Object[]) null);
            //check whether it is the one we are looking for
            if (lookFor.equals(name))
            {   //if there is no more path element to go through...
                if (next == null || next.length() == 0)
                {   //return the sub-component
                    return object;
                    
                } else
                {   //define the sub-component as the currentComponent 
                    //in a new getComponent method call
                    return getComponent(object, next);
                }
            }
        }
        return null;
    }

    /**
     * Get a service that exists in the FraSCAti's context using its class, its name and 
     * its path. The path has to be defined from the root component (ScaContainer which
     * contains the FraSCAti SCA Component) to the one which provide the service
     * 
     * @param <T>
     *          the service type
     * @param serviceClass
     *          the service class
     * @param servicePath
     *          the path  to the service
     * @return
     *          the service if it has been found
     *          
     * @throws Exception 
     */
    public <T> T getService(Class<T> serviceClass,String serviceName, String servicePath) 
    throws Exception
    {
        Object component = managerClass.getDeclaredMethod(
                "getTopLevelDomainComposite").invoke(compositeManager);
        
        Object container = getComponent(component,servicePath);
        
        @SuppressWarnings("unchecked")
        T service = (T) frascatiClass.getDeclaredMethod(
                "getService", new Class<?>[] { componentClass, String.class, Class.class })
                .invoke(frascati, new Object[] { container, serviceName, serviceClass});
        
       return  service;
    }
    
    /**
     * Return the ClassLoader used to launch the FraSCAti instance
     * 
     * @return
     *          the ClassLoader parent of the FraSCAti's one
     */
    public ClassLoader getClassLoader()
    {
        return this.isolatedCl;
    }

    /**
     * Stop the FraSCAti instance
     * 
     * @throws Exception
     */
    public void stop() throws Exception
    {
        Object lifeCycleController = componentClass.getDeclaredMethod(
                "getFcInterface", new Class<?>[] { String.class }).invoke(
                assemblyFactory, new Object[] { "lifecycle-controller" });

        Class<?> lifecycleClass = isolatedCl.loadClass(
                "org.objectweb.fractal.api.control.LifeCycleController");
        
        lifecycleClass.getDeclaredMethod("stopFc").invoke(lifeCycleController);
       
        assemblyFactory = null;
        compositeManager = null;
        frascati = null;
        isolatedCl = null;
    }

    /**
     * An URLClassLoader which allow to use the addURL method
     */
    private class UpdatableURLClassLoader extends URLClassLoader
    {
        /**
         * Constructor
         * 
         * @param parent
         *      The parent ClassLoader
         */
        public UpdatableURLClassLoader(ClassLoader parent)
        {
            super(new URL[0], parent);
        }

        /**
         *  {@inheritDoc}
         *  
         *  Define the addURL method as public
         * @see java.net.URLClassLoader#addURL(java.net.URL)
         */
        @Override
        public void addURL(URL url)
        {
            // logger.debug("adding url to load : " + url);
            super.addURL(url);
        }

        /**
         * {@inheritDoc}
         * 
         * If the name of the searched class starts with 'java',
         * 'javax', 'org.w3c' or 'org.apache.log4j' then the parent 
         * class loader is questioned first. Otherwise, the class
         * is first searched in the current class loader classpath
         * 
         * @see java.lang.ClassLoader#loadClass(java.lang.String, boolean)
         */
        protected synchronized Class<?> loadClass(String name, boolean resolve)
                throws ClassNotFoundException
        {
            // First, check if the class has already been loaded

            boolean regular = false;
            Class<?> clazz = findLoadedClass(name);

            if (clazz != null)
            {
                return clazz;
            }
            if (name.startsWith("java.") || name.startsWith("javax.")
                    || name.startsWith("org.w3c.")
                    || name.startsWith("org.apache.log4j"))
            {
                regular = true;
                if (getParent() != null)
                {
                    try
                    {
                        clazz = getParent().loadClass(name);

                    } catch (ClassNotFoundException e)
                    {
                        logger.log(Level.CONFIG,
                        "'" + name + "' class not found using the parent classloader");
                    }
                }
            }
            if (clazz == null)
            {
                try
                {
                    clazz = findClass(name);

                } catch (ClassNotFoundException e)
                {
                    logger.log(Level.CONFIG,
                    "'" + name + "' class not found using the classloader classpath");
                }
                if (clazz == null && !regular && getParent() != null)
                {
                    clazz = getParent().loadClass(name);
                }
            }
            if (clazz == null)
            {
                throw new ClassNotFoundException(name);
            }
            if (resolve)
            {
                resolveClass(clazz);
            }
            return clazz;
        }

        /**
         * {@inheritDoc}
         * 
         * If the name of the searched resource starts with 'java',
         * 'javax', 'org.w3c' or 'org.apache.log4j' then the parent 
         * class loader is questioned first. Otherwise, the resource
         * is first searched in the current class loader classpath
         * 
         * @see java.lang.ClassLoader#getResource(java.lang.String)
         */
        @Override
        public URL getResource(String name)
        {
            URL url = null;
            boolean regular = false;

            if (name.startsWith("java.") || name.startsWith("javax.")
                    || name.startsWith("org.w3c.")
                    || name.startsWith("org.apache.log4j"))
            {
                regular = true;
                if (getParent() != null)
                {
                    url = getParent().getResource(name);
                }
            }
            if (url == null)
            {
                url = findResource(name);
            }
            if (url == null && !regular && getParent() != null)
            {
                url = getParent().getResource(name);
            }
            return url;
        }
    }
}
