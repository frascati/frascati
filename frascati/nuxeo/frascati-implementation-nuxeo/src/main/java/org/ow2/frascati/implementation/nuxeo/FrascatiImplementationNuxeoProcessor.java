/**
 * OW2 FraSCAti: SCA Implementation Nuxeo
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.implementation.nuxeo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.nuxeo.runtime.api.Framework;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Reference;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.nuxeo.NuxeoPackage;
import org.ow2.frascati.nuxeo.NuxeoImplementation;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * OW2 FraSCAti SCA Implementation Nuxeo processor class.
 *
 * @author Christophe Munilla - Inria
 * @version 1.5
 */
public class FrascatiImplementationNuxeoProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<NuxeoImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(NuxeoImplementation nuxeoImplementation, StringBuilder sb) {
    sb.append("nuxeo:implementation.nuxeo");
    append(sb, "name", nuxeoImplementation.getName());
    super.toStringBuilder(nuxeoImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(NuxeoImplementation nuxeoImplementation, ProcessingContext processingContext)
	      throws ProcessorException
  {
    String nuxeoImplementationName = nuxeoImplementation.getName();
    if(isNullOrEmpty(nuxeoImplementationName)) {
      error(processingContext, nuxeoImplementation, "The attribute 'name' must be set");
    } else {
      // When FraSCAti is launched as a compiler
      // then don't check if the Nuxeo component exists
      // because the Nuxeo Framework Runtime is not started!
      if(processingContext.getProcessingMode() != ProcessingMode.compile) {
        Object nuxeoComponent = searchNuxeoComponent(nuxeoImplementation, processingContext);
        if(nuxeoComponent == null) {
          error(processingContext, nuxeoImplementation, "The Nuxeo component '" + nuxeoImplementationName + "' does not exist");
        }
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(nuxeoImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(NuxeoImplementation nuxeoImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Compute the content class name.
    Object nuxeoComponent = searchNuxeoComponent(nuxeoImplementation, processingContext);
    // contentFullClassName = nuxeoComponent.class.name + "FraSCAtiScopeCompositeContentClass"
    String contentFullClassName = nuxeoComponent.getClass().getName() + "FraSCAtiScopeCompositeContentClass";

    try {
      // Try to found the content class.
      processingContext.loadClass(contentFullClassName);
      // Found then don't need to generate and compile it.
    } catch(ClassNotFoundException cnfe) {
      // Not found then generate it.
      log.info(contentFullClassName);

      // Create the output directory for generation.
      String outputDirectory = processingContext.getOutputDirectory() + "/generated-frascati-sources";

      // Add the output directory to the Java compilation process.
      processingContext.addJavaSourceDirectoryToCompile(outputDirectory);

      // Compute content package and simple class name.
      int index = contentFullClassName.lastIndexOf('.');
      String packageName = (index != -1) ? contentFullClassName.substring(0, index) : "";
      String contentSimpleClassName = contentFullClassName.substring(index + 1);

      // Create directories.
      File packageDirectory = new File(outputDirectory + '/' + packageName.replace('.', '/'));
      packageDirectory.mkdirs();

      // Create content class file.
      PrintStream file = null;
      try {
          file = new PrintStream(new FileOutputStream(new File(
                  packageDirectory, contentSimpleClassName + ".java")));
      } catch(FileNotFoundException fnfe) {
          severe(new ProcessorException(nuxeoImplementation, fnfe));
      }

      // Generate content class.
      file.println("package " + packageName + ";\n");
      file.println("@" + Scope.class.getName() + "(\"COMPOSITE\")");
      file.println("public class " + contentSimpleClassName + " extends "
              + nuxeoComponent.getClass().getName());
      file.println("{");
      file.println("}");
      file.flush();
      file.close();
    }

    // generate the membrane for the Nuxeo component.
    generateScaPrimitiveComponent(nuxeoImplementation, processingContext, contentFullClassName);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(NuxeoImplementation nuxeoImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // instantiate the Nuxeo primitive component.
    Component component;
    try {
      logDo(processingContext, nuxeoImplementation, "instantiate the Nuxeo implementation");
      ComponentType componentType = getFractalComponentType(nuxeoImplementation, processingContext);

      // Compute the content class name.
      // contentClassName = nuxeoComponent.class.name + "FraSCAtiScopeCompositeContentClass"
      Object nuxeoComponent = searchNuxeoComponent(nuxeoImplementation, processingContext);
      String contentClassName = nuxeoComponent.getClass().getName() + "FraSCAtiScopeCompositeContentClass";

      // create instance of an SCA primitive component for this implementation.
      component = getComponentFactory().createComponent(processingContext, componentType, "scaPrimitive", contentClassName);
      // store the component into the context.
      processingContext.putData(nuxeoImplementation, Component.class, component);

      // Set the Nuxeo component as the content of the SCA component.
      SCAContentController scacc = (SCAContentController)
    		  component.getFcInterface(SCAContentController.NAME);
      scacc.setFcContent(nuxeoComponent);

      logDone(processingContext, nuxeoImplementation, "instantiate the Nuxeo implementation");
   	} catch(Exception fe) {
      severe(new ProcessorException(nuxeoImplementation, "instantiation failed", fe));
    }
  }

  /**
   * Search the Nuxeo component.
   */
  private final Object searchNuxeoComponent(NuxeoImplementation nuxeoImplementation, ProcessingContext processingContext)
  {
    // Retrieve the Nuxeo component from the Nuxeo Runtime Framework.
    return Framework.getRuntime().getComponent(nuxeoImplementation.getName());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(NuxeoPackage.Literals.NUXEO_IMPLEMENTATION);
  }

}
