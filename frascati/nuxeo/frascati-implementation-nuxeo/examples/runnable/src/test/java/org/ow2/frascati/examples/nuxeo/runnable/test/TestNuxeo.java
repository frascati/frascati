/**
 * OW2 FraSCAti: Runnable Nuxeo Component Example 
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.nuxeo.runnable.test;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.LocalDeploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.RuntimeFeature;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;

/**
 * JUnit test case for Nuxeo as SCA components. 
 *
 * @author Philippe Merle.
 */
@RunWith(FeaturesRunner.class)
@Features(RuntimeFeature.class)
@Deploy({
  "runnable-nuxeo-component"
})
@LocalDeploy({
  "runnable-nuxeo-component:OSGI-INF/runnable-nuxeo-component.xml"
})
public class TestNuxeo
{
    /**
     * Test the RunnableNuxeoComponent composite.
     */
    @Test
    public void testNuxeoComposite() throws Exception
    {
      // Instantiate FraSCAti.
      FraSCAti frascati = FraSCAti.newFraSCAti();
      // Get the RunnableNuxeoComponent composite.
      Component composite = frascati.getComposite("RunnableNuxeoComponent");
      // Retrieve the Runnable service.
      Runnable runnable = frascati.getService(composite, "Runnable", Runnable.class);
      // Run it.
      runnable.run();
      runnable.run();
      runnable.run();
    }
}
