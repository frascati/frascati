<?xml version="1.0" encoding="UTF-8"?>
<!--  OW2 FraSCAti: Nuxeo as SCA Components                                         -->
<!--  Copyright (C) 2012 Inria, University of Lille 1                               -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307          -->
<!--  USA                                                                           -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Philippe Merle                                                        -->
<!--                                                                                -->
<!--  Contributor(s):                                                               -->
<!--                                                                                -->
<!--                                                                                -->
<composite xmlns="http://www.osoa.org/xmlns/sca/1.0"
           xmlns:nuxeo="http://frascati.ow2.org/xmlns/nuxeo/1.0"
           name="Nuxeo">

  <component name="org-nuxeo-runtime-started">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.started" />
  </component>

  <component name="EventService">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.EventService" />
<!-- Following can not work as org.nuxeo.runtime.services.event.EventService is a Java class, not a Java interface !!!
    <service name="EventService">
      <interface.java interface="org.nuxeo.runtime.services.event.EventService"/>
    </service>
 -->
  </component>

  <component name="ResourceService">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.services.resource.ResourceService" />
<!-- Following can not work as org.nuxeo.runtime.services.resource.ResourceService is a Java class, not a Java interface !!!
    <service name="ResourceService">
      <interface.java interface="org.nuxeo.runtime.services.resource.ResourceService"/>
    </service>
-->
  </component>

  <component name="DefaultJBossBindings">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.api.DefaultJBossBindings" />
  </component>

  <component name="ServiceManagement">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.api.ServiceManagement" />
<!-- Following can not work as org.nuxeo.runtime.api.ServiceManager is a Java class, not a Java interface !!!
    <service name="ServiceManager">
      <interface.java interface="org.nuxeo.runtime.api.ServiceManager"/>
    </service>
-->
  </component>

  <component name="DeploymentComponent">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.services.deployment.DeploymentService"/>
<!-- Following can not work as org.nuxeo.runtime.services.deployment.DeploymentService is a Java class, not a Java interface !!!
    <service name="DeploymentService">
      <interface.java interface="org.nuxeo.runtime.services.deployment.DeploymentService"/>
      <binding.ws uri="/Nuxeo/DeploymentService"/>
    </service>
-->
  </component>

  <component name="ContributionPersistenceManager">
    <service name="ContributionPersistenceManager">
      <interface.java interface="org.nuxeo.runtime.model.persistence.ContributionPersistenceManager"/>
      <binding.ws uri="/Nuxeo/ContributionPersistenceManager"/>
    </service>
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.model.persistence"/>
  </component>
  <service name="ContributionPersistenceManager" promote="ContributionPersistenceManager/ContributionPersistenceManager"/>

  <component name="LoginComponent">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.LoginComponent" />
    <service name="LoginService">
      <interface.java interface="org.nuxeo.runtime.api.login.LoginService"/>
      <binding.ws uri="/Nuxeo/LoginService"/>
    </service>
  </component>
  <service name="LoginService" promote="LoginComponent/LoginService"/>

  <component name="ReloadComponent">
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.runtime.reload" />
    <service name="ReloadService">
      <interface.java interface="org.nuxeo.runtime.reload.ReloadService"/>
      <binding.ws uri="/Nuxeo/ReloadService"/>
    </service>
  </component>
  <service name="ReloadService" promote="ReloadComponent/ReloadService"/>

  <component name="EventServiceComponent">
    <service name="EventService">
      <interface.java interface="org.nuxeo.ecm.core.event.EventService"/>
<!-- Can not be exposed with Apache CXF!
      <binding.ws uri="/Nuxeo/ECM/EventService"/>
 -->
    </service>
    <service name="EventProducer">
      <interface.java interface="org.nuxeo.ecm.core.event.EventProducer"/>
      <binding.ws uri="/Nuxeo/ECM/EventProducer"/>
    </service>
    <service name="EventServiceAdmin">
      <interface.java interface="org.nuxeo.ecm.core.event.EventServiceAdmin"/>
<!-- Can not be exposed with Apache CXF!
      <binding.ws uri="/Nuxeo/ECM/EventServiceAdmin"/>
 -->
    </service>
    <nuxeo:implementation.nuxeo name="service:org.nuxeo.ecm.core.event.EventServiceComponent"/>
  </component>
  <service name="EventService" promote="EventServiceComponent/EventService"/>
  <service name="EventProducer" promote="EventServiceComponent/EventProducer"/>
  <service name="EventServiceAdmin" promote="EventServiceComponent/EventServiceAdmin"/>

</composite>
