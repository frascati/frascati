/**
 * OW2 FraSCAti: Nuxeo as SCA components 
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.nuxeo.test;

import java.io.FileOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.nuxeo.ecm.automation.test.RestFeature;
import org.nuxeo.ecm.core.test.CoreFeature;
import org.nuxeo.ecm.core.test.TransactionalFeature;
import org.nuxeo.ecm.platform.jbpm.test.JbpmFeature;
import org.nuxeo.ecm.platform.test.PlatformFeature;
import org.nuxeo.ecm.webengine.test.WebEngineFeature;
import org.nuxeo.ecm.webengine.test.NuxeoThemeFeature;
import org.nuxeo.runtime.RuntimeService;
import org.nuxeo.runtime.api.Framework;
import org.nuxeo.runtime.api.login.LoginService;
import org.nuxeo.runtime.api.login.SecurityDomain;
import org.nuxeo.runtime.model.ComponentName;
import org.nuxeo.runtime.model.ExtensionPoint;
import org.nuxeo.runtime.model.Extension;
import org.nuxeo.runtime.model.Property;
import org.nuxeo.runtime.model.RegistrationInfo;
import org.nuxeo.runtime.test.runner.Deploy;
import org.nuxeo.runtime.test.runner.Features;
import org.nuxeo.runtime.test.runner.FeaturesRunner;
import org.nuxeo.runtime.test.runner.JettyFeature;
import org.nuxeo.runtime.test.runner.RuntimeFeature;
import org.nuxeo.runtime.test.runner.StreamingFeature;
import org.nuxeo.runtime.test.runner.distrib.DistributionFeature;
import org.nuxeo.runtime.test.runner.web.WebDriverFeature;
import org.nuxeo.runtime.test.protocols.inline.InlineURLsFeature;
import org.nuxeo.runtime.reload.ReloadService;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;

/**
 * JUnit test case for Nuxeo as SCA components. 
 *
 * @author Philippe Merle.
 */
@RunWith(FeaturesRunner.class)
@Features({
  DistributionFeature.class,
  PlatformFeature.class,
  WebEngineFeature.class,
  WebDriverFeature.class,
  NuxeoThemeFeature.class,
  RestFeature.class,
  JettyFeature.class,
  CoreFeature.class,
  RuntimeFeature.class,
  InlineURLsFeature.class,
  JbpmFeature.class
})
//
// Problematic features:
// - StreamingFeature.class
// - TransactionalFeature.class
//
// Not tested features:
// - MultiRepositoriesCoreFeature
//
@Deploy({
  "org.nuxeo.admin.center",
  "org.nuxeo.admin.center.monitoring",
  "org.nuxeo.admin.center.oauth.opensocial",
  "org.nuxeo.admin.offline.update",
  "org.nuxeo.apidoc.core",
  "org.nuxeo.apidoc.webengine",
  "org.nuxeo.common",
  "org.nuxeo.connect.client",
  "org.nuxeo.connect.client.wrapper",
  "org.nuxeo.connect.update",
  "org.nuxeo.ecm.actions",
  "org.nuxeo.ecm.annotations",
  "org.nuxeo.ecm.annotations.client",
  "org.nuxeo.ecm.annotations.contrib",
  "org.nuxeo.ecm.annotations.http",
  "org.nuxeo.ecm.annotations.repository",
  "org.nuxeo.ecm.audit.io",
  "org.nuxeo.ecm.automation.core",
  "org.nuxeo.ecm.automation.features",
  "org.nuxeo.ecm.automation.jsf",
  "org.nuxeo.ecm.automation.server",
  "org.nuxeo.ecm.core",
  "org.nuxeo.ecm.core.api",
  "org.nuxeo.ecm.core.convert",
  "org.nuxeo.ecm.core.convert.api",
  "org.nuxeo.ecm.core.convert.plugins",
  "org.nuxeo.ecm.core.event",
  "org.nuxeo.ecm.core.io",
  "org.nuxeo.ecm.core.management",
  "org.nuxeo.ecm.core.opencmis.bindings",
  "org.nuxeo.ecm.core.opencmis.impl",
  "org.nuxeo.ecm.core.persistence",
  "org.nuxeo.ecm.core.query",
  "org.nuxeo.ecm.core.schema",
  "org.nuxeo.ecm.core.storage.sql",
  "org.nuxeo.ecm.core.storage.sql.management",
  "org.nuxeo.ecm.core.storage.sql.ra",
  "org.nuxeo.ecm.default.config",
  "org.nuxeo.ecm.directory",
  "org.nuxeo.ecm.directory.api",
  "org.nuxeo.ecm.directory.ldap",
  "org.nuxeo.ecm.directory.multi",
  "org.nuxeo.ecm.directory.sql",
  "org.nuxeo.ecm.directory.types.contrib",
  "org.nuxeo.ecm.directory.web",
  "org.nuxeo.ecm.opensocial.gwt.container",
  "org.nuxeo.ecm.opensocial.gwt.container.webapp",
  "org.nuxeo.ecm.opensocial.gwt.richtexteditor",
  "org.nuxeo.ecm.opensocial.spaces",
  "org.nuxeo.ecm.platform",
  "org.nuxeo.ecm.platform.annotations.api",
  "org.nuxeo.ecm.platform.api",
  "org.nuxeo.ecm.platform.audit",
  "org.nuxeo.ecm.platform.audit.api",
  "org.nuxeo.ecm.platform.audit.web",
  "org.nuxeo.ecm.platform.commandline.executor",
  "org.nuxeo.ecm.platform.comment",
  "org.nuxeo.ecm.platform.comment.api",
  "org.nuxeo.ecm.platform.comment.core",
  "org.nuxeo.ecm.platform.comment.web",
  "org.nuxeo.ecm.platform.comment.workflow",
  "org.nuxeo.ecm.platform.content.template",
  "org.nuxeo.ecm.platform.contentview.jsf",
  "org.nuxeo.ecm.platform.convert",
  "org.nuxeo.ecm.platform.dublincore",
  "org.nuxeo.ecm.platform.el",
  "org.nuxeo.ecm.platform.faceted.search.api",
  "org.nuxeo.ecm.platform.faceted.search.dm",
  "org.nuxeo.ecm.platform.faceted.search.jsf",
  "org.nuxeo.ecm.platform.filemanager.api",
  "org.nuxeo.ecm.platform.filemanager.core",
  "org.nuxeo.ecm.platform.filemanager.core.listener",
  "org.nuxeo.ecm.platform.forms.layout.api",
  "org.nuxeo.ecm.platform.forms.layout.client",
  "org.nuxeo.ecm.platform.forms.layout.core",
  "org.nuxeo.ecm.platform.forms.layout.export",
  "org.nuxeo.ecm.platform.forms.layout.io",
  "org.nuxeo.ecm.platform.forms.layout.io.plugins",
  "org.nuxeo.ecm.platform.forum",
  "org.nuxeo.ecm.platform.forum.api",
  "org.nuxeo.ecm.platform.forum.core",
  "org.nuxeo.ecm.platform.forum.workflow",
  "org.nuxeo.ecm.platform.htmlsanitizer",
  "org.nuxeo.ecm.platform.io.api",
  "org.nuxeo.ecm.platform.io.core",
  "org.nuxeo.ecm.platform.io.web",
  "org.nuxeo.ecm.platform.jbpm.api",
  "org.nuxeo.ecm.platform.jbpm.automation",
  "org.nuxeo.ecm.platform.jbpm.core",
  "org.nuxeo.ecm.platform.jbpm.task.migration",
  "org.nuxeo.ecm.platform.jbpm.web",
  "org.nuxeo.ecm.platform.lang",
  "org.nuxeo.ecm.platform.lang.ext",
  "org.nuxeo.ecm.platform.localconfiguration.simple",
  "org.nuxeo.ecm.platform.localconfiguration.web",
  "org.nuxeo.ecm.platform.login",
  "org.nuxeo.ecm.platform.login.default",
  "org.nuxeo.ecm.platform.login.digest",
  "org.nuxeo.ecm.platform.mail",
  "org.nuxeo.ecm.platform.mail.types",
  "org.nuxeo.ecm.platform.mail.web",
  "org.nuxeo.ecm.platform.mimetype.api",
  "org.nuxeo.ecm.platform.mimetype.core",
  "org.nuxeo.ecm.platform.notification.api",
  "org.nuxeo.ecm.platform.notification.core",
  "org.nuxeo.ecm.platform.notification.web",
  "org.nuxeo.ecm.platform.oauth",
  "org.nuxeo.ecm.platform.picture.api",
  "org.nuxeo.ecm.platform.picture.convert",
  "org.nuxeo.ecm.platform.picture.core",
  "org.nuxeo.ecm.platform.picture.dm",
  "org.nuxeo.ecm.platform.picture.jsf",
  "org.nuxeo.ecm.platform.picture.preview",
  "org.nuxeo.ecm.platform.pictures.tiles",
  "org.nuxeo.ecm.platform.pictures.tiles.preview",
  "org.nuxeo.ecm.platform.placeful.api",
  "org.nuxeo.ecm.platform.placeful.core",
  "org.nuxeo.ecm.platform.preview",
  "org.nuxeo.ecm.platform.publisher.api",
  "org.nuxeo.ecm.platform.publisher.core",
  "org.nuxeo.ecm.platform.publisher.core.contrib",
  "org.nuxeo.ecm.platform.publisher.task",
  "org.nuxeo.ecm.platform.publisher.web",
  "org.nuxeo.ecm.platform.query.api",
  "org.nuxeo.ecm.platform.rendering",
  "org.nuxeo.ecm.platform.scheduler.core",
  "org.nuxeo.ecm.platform.seam.debug",
  "org.nuxeo.ecm.platform.search.api",
  "org.nuxeo.ecm.platform.suggestbox.core",
  "org.nuxeo.ecm.platform.suggestbox.jsf",
  "org.nuxeo.ecm.platform.syndication",
  "org.nuxeo.ecm.platform.tag",
  "org.nuxeo.ecm.platform.tag.api",
  "org.nuxeo.ecm.platform.tag.web",
  "org.nuxeo.ecm.platform.task.api",
  "org.nuxeo.ecm.platform.task.automation",
  "org.nuxeo.ecm.platform.task.core",
  "org.nuxeo.ecm.platform.task.web",
  "org.nuxeo.ecm.platform.types.api",
  "org.nuxeo.ecm.platform.types.core",
  "org.nuxeo.ecm.platform.ui",
  "org.nuxeo.ecm.platform.ui.api",
  "org.nuxeo.ecm.platform.ui.compat",
  "org.nuxeo.ecm.platform.uidgen.core",
  "org.nuxeo.ecm.platform.url.api",
  "org.nuxeo.ecm.platform.url.core",
  "org.nuxeo.ecm.platform.usermanager",
  "org.nuxeo.ecm.platform.usermanager.api",
  "org.nuxeo.ecm.platform.userworkspace.api",
  "org.nuxeo.ecm.platform.userworkspace.core",
  "org.nuxeo.ecm.platform.userworkspace.types",
  "org.nuxeo.ecm.platform.userworkspace.web",
  "org.nuxeo.ecm.platform.versioning",
  "org.nuxeo.ecm.platform.versioning.api",
  "org.nuxeo.ecm.platform.web.common",
  "org.nuxeo.ecm.platform.webapp.types",
  "org.nuxeo.ecm.platform.webengine.blogs",
  "org.nuxeo.ecm.platform.webengine.blogs.api",
  "org.nuxeo.ecm.platform.webengine.blogs.core",
  "org.nuxeo.ecm.platform.webengine.sites",
  "org.nuxeo.ecm.platform.webengine.sites.api",
  "org.nuxeo.ecm.platform.webengine.sites.core.contrib",
  "org.nuxeo.ecm.platform.wi.backend",
  "org.nuxeo.ecm.platform.ws",
  "org.nuxeo.ecm.platform.ws.sun.jaxws",
  "org.nuxeo.ecm.relations",
  "org.nuxeo.ecm.relations.api",
  "org.nuxeo.ecm.relations.core.listener",
  "org.nuxeo.ecm.relations.default.config",
  "org.nuxeo.ecm.relations.io",
  "org.nuxeo.ecm.relations.jena",
  "org.nuxeo.ecm.relations.web",
  "org.nuxeo.ecm.user.center",
  "org.nuxeo.ecm.user.center.dashboard",
  "org.nuxeo.ecm.user.center.dashboard.jsf",
  "org.nuxeo.ecm.user.center.dashboard.opensocial",
  "org.nuxeo.ecm.user.center.notification",
  "org.nuxeo.ecm.user.center.oauth",
  "org.nuxeo.ecm.user.center.profile",
  "org.nuxeo.ecm.webapp.base",
  "org.nuxeo.ecm.webapp.core",
  "org.nuxeo.ecm.webapp.ui",
  "org.nuxeo.ecm.webdav",
  "org.nuxeo.ecm.webengine.admin",
  "org.nuxeo.ecm.webengine.base",
  "org.nuxeo.ecm.webengine.core",
  "org.nuxeo.ecm.webengine.gwt",
  "org.nuxeo.ecm.webengine.jaxrs",
  "org.nuxeo.ecm.webengine.ui",
  "org.nuxeo.launcher.commons",
  "org.nuxeo.opensocial.features",
  "org.nuxeo.opensocial.gadgets",
  "org.nuxeo.opensocial.gadgets.core",
  "org.nuxeo.opensocial.service",
  "org.nuxeo.opensocial.webengine.gadgets",
  "org.nuxeo.osgi",
  "org.nuxeo.platform.virtualnavigation.types",
  "org.nuxeo.platform.virtualnavigation.web",
//  "org.nuxeo.runtime",
  "org.nuxeo.runtime.datasource",
  "org.nuxeo.runtime.jtajca",
  "org.nuxeo.runtime.management",
  "org.nuxeo.runtime.nuxeo-runtime-deploy",
  "org.nuxeo.runtime.reload",
  "org.nuxeo.theme.bank",
  "org.nuxeo.theme.core",
  "org.nuxeo.theme.editor",
  "org.nuxeo.theme.fragments",
  "org.nuxeo.theme.html",
  "org.nuxeo.theme.jsf",
  "org.nuxeo.theme.styling",
  "org.nuxeo.theme.webengine"
})
public class TestNuxeo
{
    /**
     * List of Java interfaces which can not be exported by Apache CXF Aegis databinding.
     */
    static final java.util.List<String> servicesNotExportableWithApacheCXF = new java.util.ArrayList() {{
      add("org.nuxeo.ecm.platform.query.api.PageProviderService");
      add("org.nuxeo.ecm.core.security.SecurityPolicyService");
      add("org.nuxeo.ecm.core.event.EventService");
      add("org.nuxeo.ecm.core.event.EventServiceAdmin");
      add("org.nuxeo.ecm.core.storage.sql.jdbc.QueryMakerService");
      add("org.nuxeo.ecm.automation.AutomationService");
      add("org.nuxeo.ecm.core.api.localconfiguration.LocalConfigurationService");
      add("org.nuxeo.ecm.core.api.CoreSession");
      add("org.nuxeo.ecm.platform.jbpm.JbpmService");
      add("org.nuxeo.apidoc.snapshot.SnapshotManager");
      add("org.nuxeo.ecm.platform.forms.layout.service.WebLayoutManager");
      add("org.nuxeo.opensocial.container.server.service.WebContentSaverService");
      add("org.nuxeo.ecm.platform.forms.layout.api.service.LayoutStore");
      add("org.nuxeo.runtime.management.ResourcePublisher");
      add("org.nuxeo.ecm.platform.actions.ejb.ActionManager");
      add("org.nuxeo.ecm.platform.actions.ejb.ActionManager");
      add("org.nuxeo.ecm.platform.faceted.search.api.service.FacetedSearchService");
      add("org.nuxeo.opensocial.service.api.OpenSocialService");
      add("org.nuxeo.ecm.platform.contentview.jsf.ContentViewService");
      add("org.nuxeo.ecm.core.management.api.ProbeManager");
      add("org.nuxeo.ecm.core.event.EventStats");
    }};

    /**
     * Generate the SCA composite for Nuxeo.
     */
    @Test
    public void generateNuxeoComposite() throws Exception
    {
      System.out.println("-----------------------------------------------------------------------------------");
      System.out.println("-----------------------------------------------------------------------------------");
      System.out.println("-----------------------------------------------------------------------------------");
      System.out.println("Generate target/NuxeoGenerated.composite...");
      long timer = System.currentTimeMillis();
      PrintStream nuxeoComposite = new PrintStream(new FileOutputStream("target/NuxeoGenerated.composite"));
      nuxeoComposite.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
      nuxeoComposite.println("<composite xmlns=\"http://www.osoa.org/xmlns/sca/1.0\" xmlns:nuxeo=\"http://frascati.ow2.org/xmlns/nuxeo/1.0\" name=\"NuxeoGenerated\">");
      // Iterate on all registered Nuxeo components.
      for(RegistrationInfo registrationInfo : Framework.getRuntime().getComponentManager().getRegistrations()) {
        String componentName = null;
        if(registrationInfo.getImplementation() != null) {
          componentName = registrationInfo.getImplementation();
        } else {
          componentName = registrationInfo.getName().toString();
          componentName = componentName.substring(componentName.indexOf(':') + 1);
        }
//        componentName = componentName.substring(componentName.lastIndexOf('.') + 1);
    	nuxeoComposite.println("  <component name=\"" + componentName + "\">");
    	nuxeoComposite.println("    <nuxeo:implementation.nuxeo name=\"" + registrationInfo.getName() + "\"/>");
        String[] providedServiceNames = registrationInfo.getProvidedServiceNames();
        if(providedServiceNames != null) {
          for(String providedServiceName : providedServiceNames) {
        	Class<?> serviceType = Class.forName(providedServiceName);
            if(!serviceType.isInterface()) {
              nuxeoComposite.println("<!-- Following can not work as " + providedServiceName + " is a Java class, not a Java interface !!!");
            }
            String serviceName = providedServiceName;
            serviceName = serviceName.substring(serviceName.lastIndexOf('.') + 1);
            nuxeoComposite.println("    <service name=\"" + serviceName + "\">");
            nuxeoComposite.println("      <interface.java interface=\"" + providedServiceName + "\"/>");
            if(servicesNotExportableWithApacheCXF.contains(providedServiceName)) {
              nuxeoComposite.println("<!-- Can not be exposed with Apache CXF Aegis databinding!");
            }
            nuxeoComposite.println("      <binding.ws uri=\"/Nuxeo/" + providedServiceName + "\"/>");
            if(servicesNotExportableWithApacheCXF.contains(providedServiceName)) {
              nuxeoComposite.println(" -->");
            }
            nuxeoComposite.println("    </service>");
            if(!serviceType.isInterface()) {
              nuxeoComposite.println(" -->");
            }
          }
        }
    	nuxeoComposite.println("  </component>");
        if(providedServiceNames != null) {
          for(String providedServiceName : providedServiceNames) {
          	Class<?> serviceType = Class.forName(providedServiceName);
            if(!serviceType.isInterface()) {
              nuxeoComposite.println("<!-- Commented as this service is not defined");              
            }
            String serviceName = providedServiceName;
            serviceName = serviceName.substring(serviceName.lastIndexOf('.') + 1);
            nuxeoComposite.println("  <service name=\"" + serviceName + "\" promote=\"" + componentName + '/' + serviceName + "\"/>");
            if(!serviceType.isInterface()) {
              nuxeoComposite.println(" -->");
            }
          }
        }
      }
      nuxeoComposite.println("</composite>");
      nuxeoComposite.close();
      System.out.println("target/NuxeoGenerated.composite generated in " + (System.currentTimeMillis() - timer) + " ms.");
      System.out.println("-----------------------------------------------------------------------------------");
      System.out.println("-----------------------------------------------------------------------------------");
      System.out.println("-----------------------------------------------------------------------------------");
    }

    /**
     * Test the Nuxeo composite.
     */
    @Test
    public void testNuxeoComposite() throws Exception
    {
      // Instantiate FraSCAti.
      FraSCAti frascati = FraSCAti.newFraSCAti();

      // Get the NuxeoGenerated composite.
      long timer = System.currentTimeMillis();
      Component nuxeoGenerated = frascati.getComposite("target/NuxeoGenerated.composite");
      System.out.println("\n\n\ntarget/NuxeoGenerated.composite loaded in " + (System.currentTimeMillis() - timer) + " ms.\n\n\n");
      
      // Get the Nuxeo composite.
      timer = System.currentTimeMillis();
      Component nuxeo = frascati.getComposite("Nuxeo");
      System.out.println("\n\n\nNuxeo.composite loaded in " + (System.currentTimeMillis() - timer) + " ms.\n\n\n");

      // Retrieve the LoginService service.
      LoginService loginService = frascati.getService(nuxeo, "LoginService", LoginService.class);
      // Invoke it.
      for(SecurityDomain securityDomain : loginService.getSecurityDomains()) {
        System.out.println("* " + securityDomain);
      }

      // Retrieve the ReloadService service.
      ReloadService reloadService = frascati.getService(nuxeo, "ReloadService", ReloadService.class);
      // Invoke it.
      reloadService.reload();      

      // Sleep in order to use the Nuxeo services as Web Services.
      int minutesToSleep = Integer.getInteger("sleep", 0);
      if(minutesToSleep > 0) {
    	System.out.println("Will sleep " + minutesToSleep + " minutes...");
        Thread.sleep(minutesToSleep*60*1000);
      }
    }
}
