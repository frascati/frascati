/**
 * OW2 FraSCAti SCA Binding Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.binding;

import java.util.Map;

import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.nuxeo.NuxeoBinding;
import org.ow2.frascati.nuxeo.NuxeoPackage;

/**
 * Bind components using a Nuxeo Binding
 */
public class FrascatiBindingNuxeoProcessor extends
        AbstractBindingFactoryProcessor<NuxeoBinding>
{

    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#
     * toStringBuilder(EObjectType,StringBuilder)
     */
    @Override
    protected final void toStringBuilder(NuxeoBinding nuxeoBinding,
            StringBuilder sb)
    {
        sb.append("nuxeo:binding.nuxeo");
        super.toStringBuilder(nuxeoBinding, sb);
    }

    @Override
    protected final String getBindingFactoryPluginId()
    {
        return "nuxeo";
    }

    @Override
    @SuppressWarnings("static-access")
    protected final void initializeBindingHints(NuxeoBinding nuxeoBinding,
            Map<String, Object> hints)
    {
        String nxservice = nuxeoBinding.getNxservice();
        hints.put("nxservice", nxservice);
    }

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
     */
    public final String getProcessorID()
    {
        return getID(NuxeoPackage.Literals.NUXEO_BINDING);
    }
}
