/**
 * OW2 FraSCAti : Parser Event Test
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.parser.test.impl;

import java.util.logging.Logger;

import org.eclipse.stp.sca.Composite;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientCompositeItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTestItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf;


/**
 * Recipient for Composite parsing events
 */
@Service(interfaces = {
        ParserEventRecipientCompositeItf.class,
        FraSCAtiComponentTesterItf.class
        })
@Scope("COMPOSITE")
public class CompositeEventRecipient implements
        ParserEventRecipientCompositeItf,FraSCAtiComponentTesterItf
{

    Logger log = Logger.getLogger(CompositeEventRecipient.class.getCanonicalName());
    private FraSCAtiComponentTestItf test;
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.CompositeParserEventRecipientItf#compositeCheckDo(org.eclipse.stp.sca.Composite)
     */
    public void compositeCheckDo(Composite eObject)
    {
        this.test.count("DO", "COMPOSITE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.CompositeParserEventRecipientItf#compositeCheckDone(org.eclipse.stp.sca.Composite)
     */
    public void compositeCheckDone(Composite eObject)
    {
        this.test.count("DONE", "COMPOSITE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.CompositeParserEventRecipientItf#compositeCheckError(org.eclipse.stp.sca.Composite, java.lang.Throwable)
     */
    public void compositeCheckError(Composite eObject, Throwable throwable)
    {
        this.test.count("ERROR", "COMPOSITE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.test.FraSCAtiComponentTesterItf#setTester(org.ow2.frascati.parser.test.FraSCAtiComponentTestItf)
     */
    public void setTester(FraSCAtiComponentTestItf test)
    {
        this.test = test;
        this.test.init("DO", "COMPOSITE");
        this.test.init("DONE", "COMPOSITE");
        this.test.init("ERROR", "COMPOSITE");
    }

}
