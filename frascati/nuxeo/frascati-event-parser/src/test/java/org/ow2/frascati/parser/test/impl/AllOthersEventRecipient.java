/**
 * OW2 FraSCAti : Parser Event Test
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.parser.test.impl;

import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.SCAPropertyBase;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTestItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf;

/**
 * Recipient for Component, Property, Binding, Implementation, Interface parsing events
 */
@Service(interfaces = {
        ParserEventRecipientComponentItf.class,
        ParserEventRecipientPropertyItf.class,
        ParserEventRecipientBindingItf.class,
        ParserEventRecipientImplementationItf.class,
        ParserEventRecipientInterfaceItf.class,
        FraSCAtiComponentTesterItf.class
        })
@Scope("COMPOSITE")
public class AllOthersEventRecipient implements
    ParserEventRecipientComponentItf,
    ParserEventRecipientPropertyItf,
    ParserEventRecipientBindingItf,
    ParserEventRecipientImplementationItf,
    ParserEventRecipientInterfaceItf,
    FraSCAtiComponentTesterItf
{

    private FraSCAtiComponentTestItf test;

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.test.FraSCAtiComponentTesterItf#setTester(org.ow2.frascati.parser.test.FraSCAtiComponentTestItf)
     */
    public void setTester(FraSCAtiComponentTestItf test)
    {
        this.test = test;
        this.test.init("DO", "COMPONENT");
        this.test.init("DONE", "COMPONENT");
        this.test.init("ERROR", "COMPONENT");
        this.test.init("DO", "PROPERTY");
        this.test.init("DONE", "PROPERTY");
        this.test.init("ERROR", "PROPERTY");
        this.test.init("DO", "BINDING");
        this.test.init("DONE", "BINDING");
        this.test.init("ERROR", "BINDING");
        this.test.init("DO", "IMPLEMENTATION");
        this.test.init("DONE", "IMPLEMENTATION");
        this.test.init("ERROR", "IMPLEMENTATION");
        this.test.init("DO", "INTERFACE");
        this.test.init("DONE", "INTERFACE");
        this.test.init("ERROR", "INTERFACE");
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf#
     * componentCheckDo(org.eclipse.stp.sca.Component)
     */
    public void componentCheckDo(Component eObject)
    {
        this.test.count("DO", "COMPONENT",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf#
     * componentCheckDone(org.eclipse.stp.sca.Component)
     */
    public void componentCheckDone(Component eObject)
    {
        this.test.count("DONE", "COMPONENT",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf#
     * componentCheckError(org.eclipse.stp.sca.Component, java.lang.Throwable)
     */
    public void componentCheckError(Component eObject, Throwable throwable)
    {
        this.test.count("ERROR", "COMPONENT",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf#
     * propertyCheckDo(org.eclipse.stp.sca.Property)
     */
    public void propertyCheckDo(SCAPropertyBase eObject)
    {
        this.test.count("DO", "PROPERTY", eObject.getValue());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf#
     * propertyCheckDone(org.eclipse.stp.sca.Property)
     */
    public void propertyCheckDone(SCAPropertyBase eObject)
    {
        this.test.count("DONE", "PROPERTY", eObject.getValue());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf#
     * propertyCheckError(org.eclipse.stp.sca.Property, java.lang.Throwable)
     */
    public void propertyCheckError(SCAPropertyBase eObject, Throwable throwable)
    {
        System.out.println("*********************** HERE IS AN ERROR ******************");
        this.test.count("ERROR", "PROPERTY", eObject.getValue());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf#
     * bindingCheckDo(org.eclipse.stp.sca.Binding)
     */
    public void bindingCheckDo(Binding eObject)
    {
        this.test.count("DO", "BINDING", eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf#
     * bindingCheckDone(org.eclipse.stp.sca.Binding)
     */
    public void bindingCheckDone(Binding eObject)
    {
        this.test.count("DONE", "BINDING", eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf#
     * bindingCheckError(org.eclipse.stp.sca.Binding, java.lang.Throwable)
     */
    public void bindingCheckError(Binding eObject, Throwable throwable)
    {
        this.test.count("ERROR", "BINDING", eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf#interfaceCheckDo(org.eclipse.stp.sca.Interface)
     */
    public void interfaceCheckDo(Interface eObject)
    {
        this.test.count("DO", "INTERFACE",  eObject.eClass().getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf#interfaceCheckDone(org.eclipse.stp.sca.Interface)
     */
    public void interfaceCheckDone(Interface eObject)
    {
        this.test.count("DONE", "INTERFACE",  eObject.eClass().getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf#interfaceCheckError(org.eclipse.stp.sca.Interface, java.lang.Throwable)
     */
    public void interfaceCheckError(Interface eObject, Throwable throwable)
    {
        this.test.count("ERROR", "INTERFACE",  eObject.eClass().getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf#implementationCheckDo(org.eclipse.stp.sca.Implementation)
     */
    public void implementationCheckDo(Implementation eObject)
    {
        this.test.count("DO", "IMPLEMENTATION", eObject.eClass().getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf#implementationCheckDone(org.eclipse.stp.sca.Implementation)
     */
    public void implementationCheckDone(Implementation eObject)
    {
        this.test.count("DONE", "IMPLEMENTATION", eObject.eClass().getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf#implementationCheckError(org.eclipse.stp.sca.Implementation, java.lang.Throwable)
     */
    public void implementationCheckError(Implementation eObject,
            Throwable throwable)
    {
        this.test.count("ERROR", "IMPLEMENTATION", eObject.eClass().getName());
    }

}
