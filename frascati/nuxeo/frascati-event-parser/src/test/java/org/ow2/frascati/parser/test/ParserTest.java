/**
 * OW2 FraSCAti : Parser Event Test
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.parser.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTestItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf;
import org.ow2.frascati.util.FrascatiException;

/**
 * Parser Event Test Case
 */
public class ParserTest implements FraSCAtiComponentTestItf
{
    private static FraSCAti frascati = null;
    private static Component root = null;
    private static FraSCAtiComponentTesterItf compositeTester = null;
    private static FraSCAtiComponentTesterItf serrefTester = null;
    private static FraSCAtiComponentTesterItf otherTester = null;
    
    private static Logger log = Logger.getLogger(ParserTest.class.getCanonicalName());
    
    private Map<String,Map<String,Integer>> eObjectMap = null;
    
    /**
     * Test if the FraSCAti instance exists, find
     * its 'org.ow2.frascati.FraSCAti' root component if necessary
     * and get FraSCAtiComponentTesters
     */
    @Before
    public void setUp()
    {        
        if(frascati == null)
        {
            try
            {              
                frascati = FraSCAti.newFraSCAti();
                Component scaContainer = frascati.getCompositeManager().getTopLevelDomainComposite();
    
                ContentController contentController = (ContentController) scaContainer.getFcInterface(
                        "content-controller");
                
                Component[] components = contentController.getFcSubComponents();
                root = components[0];
    
                //receive parsing events for composite
                compositeTester = frascati.getService(
                        root, "composite-recipient-tester", FraSCAtiComponentTesterItf.class);

                //receive parsing events for composite service, composite reference, 
                //component service and component reference
                serrefTester = frascati.getService(
                        root, "serref-recipient-tester", FraSCAtiComponentTesterItf.class);

                //receive parsing events for component, property, binding and sca binding
                otherTester = frascati.getService(
                        root, "other-recipient-tester", FraSCAtiComponentTesterItf.class);
                
            } catch (FrascatiException e)
            {
                log.log(Level.SEVERE,e.getMessage(),e);
                fail(e.getMessage());
                
            } catch (NoSuchInterfaceException e)
            {
                log.log(Level.SEVERE,e.getMessage(),e);
                fail(e.getMessage());
            }
        }
        
        assertNotNull(compositeTester);
        assertNotNull(serrefTester);
        assertNotNull(otherTester); 

        eObjectMap = new HashMap<String,Map<String,Integer>>();
        
        compositeTester.setTester(this);
        serrefTester.setTester(this);
        otherTester.setTester(this); 
    }
    
    /**
     * Test whether StarterItf Components have been loaded
     *  
     * @throws FrascatiException
     */
    @Test
    public void testParsingHelloWorldOK() throws FrascatiException
    {
            frascati.getComposite("helloworld-pojo"); 
            //one composite has been parsed without error
            assertEquals(1,eObjectMap.get("DO").get("COMPOSITE").intValue());
            assertEquals(1,eObjectMap.get("DONE").get("COMPOSITE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("COMPOSITE").intValue());
            //two component has been parsed without error
            assertEquals(2,eObjectMap.get("DO").get("COMPONENT").intValue());
            assertEquals(2,eObjectMap.get("DONE").get("COMPONENT").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("COMPONENT").intValue());
            //one composite service parsed without error
            assertEquals(1,eObjectMap.get("DO").get("SERVICE").intValue());
            assertEquals(1,eObjectMap.get("DONE").get("SERVICE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("SERVICE").intValue());
            //no composite reference parsed
            assertEquals(0,eObjectMap.get("DO").get("REFERENCE").intValue());
            assertEquals(0,eObjectMap.get("DONE").get("REFERENCE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("REFERENCE").intValue());
            //two component service parsed without error
            assertEquals(2,eObjectMap.get("DO").get("COMPONENT-SERVICE").intValue());
            assertEquals(2,eObjectMap.get("DONE").get("COMPONENT-SERVICE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("COMPONENT-SERVICE").intValue());
            //one component reference parsed without error
            assertEquals(1,eObjectMap.get("DO").get("COMPONENT-REFERENCE").intValue());
            assertEquals(1,eObjectMap.get("DONE").get("COMPONENT-REFERENCE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("COMPONENT-REFERENCE").intValue());
            //no property parsed
            assertEquals(1,eObjectMap.get("DO").get("PROPERTY").intValue());
            assertEquals(1,eObjectMap.get("DONE").get("PROPERTY").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("PROPERTY").intValue());
            //no binding parsed
            assertEquals(0,eObjectMap.get("DO").get("BINDING").intValue());
            assertEquals(0,eObjectMap.get("DONE").get("BINDING").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("BINDING").intValue());
            //no binding parsed
            assertEquals(4,eObjectMap.get("DO").get("INTERFACE").intValue());
            assertEquals(4,eObjectMap.get("DONE").get("INTERFACE").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("INTERFACE").intValue());
            //no binding parsed
            assertEquals(2,eObjectMap.get("DO").get("IMPLEMENTATION").intValue());
            assertEquals(2,eObjectMap.get("DONE").get("IMPLEMENTATION").intValue());
            assertEquals(0,eObjectMap.get("ERROR").get("IMPLEMENTATION").intValue());
     }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.test.FraSCAtiComponentTestItf#count(java.lang.String, java.lang.String)
     */
    public void count(String method, String type, String name)
    {
        log.info(method + " : " + type + "[" + name + "]");
        Map<String,Integer> eventMap = eObjectMap.get(method);
        if(eventMap == null)
        {
            eventMap = new HashMap<String,Integer>();
            eObjectMap.put(method, eventMap); 
        }
        Integer count = eventMap.get(type);
        if(count == null)
        {
            eventMap.put(type, new Integer(1));
            
        } else 
        {
            eventMap.put(type, new Integer(count.intValue()+1));
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.test.FraSCAtiComponentTestItf#init(java.lang.String, java.lang.String)
     */
    public void init(String method, String type)
    { 
        Map<String,Integer> eventMap = eObjectMap.get(method);
        if(eventMap == null)
        {
            eventMap = new HashMap<String,Integer>();
            eObjectMap.put(method, eventMap); 
        }
        eventMap.put(type, new Integer(0)); 
    }
    
    
    
}
