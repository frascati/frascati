/**
 * OW2 FraSCAti : Parser Event Test
 * Copyright (c) 2008 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.parser.test.impl;

import java.util.logging.Logger;

import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentReferenceItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientReferenceItf;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientServiceItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTestItf;
import org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf;


/**
 * Recipient for ComponentReference, ComponentService, Reference, Service parsing events
 */
@Service(interfaces = {
        ParserEventRecipientComponentReferenceItf.class,
        ParserEventRecipientComponentServiceItf.class,
        ParserEventRecipientReferenceItf.class,
        ParserEventRecipientServiceItf.class,
        FraSCAtiComponentTesterItf.class
        })
@Scope("COMPOSITE")
public class SerrefEventRecipient implements ParserEventRecipientServiceItf,
        ParserEventRecipientReferenceItf,ParserEventRecipientComponentReferenceItf,
        ParserEventRecipientComponentServiceItf,FraSCAtiComponentTesterItf
{

    Logger log = Logger.getLogger(SerrefEventRecipient.class.getCanonicalName());
    private FraSCAtiComponentTestItf test;
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ReferenceParserEventRecipientItf#referenceCheckDo(org.eclipse.stp.sca.Reference)
     */
    public void referenceCheckDo(Reference eObject)
    {
        this.test.count("DO", "REFERENCE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ReferenceParserEventRecipientItf#referenceCheckDone(org.eclipse.stp.sca.Reference)
     */
    public void referenceCheckDone(Reference eObject)
    {
        this.test.count("DONE", "REFERENCE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ReferenceParserEventRecipientItf#referenceCheckError(org.eclipse.stp.sca.Reference, java.lang.Throwable)
     */
    public void referenceCheckError(Reference eObject, Throwable throwable)
    {
        this.test.count("ERROR", "REFERENCE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ServiceParserEventRecipientItf#serviceCheckDo(org.eclipse.stp.sca.Service)
     */
    public void serviceCheckDo(org.eclipse.stp.sca.Service eObject)
    {
        this.test.count("DO", "SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ServiceParserEventRecipientItf#serviceCheckDone(org.eclipse.stp.sca.Service)
     */
    public void serviceCheckDone(org.eclipse.stp.sca.Service eObject)
    {
        this.test.count("DONE", "SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ServiceParserEventRecipientItf#serviceCheckError(org.eclipse.stp.sca.Service, java.lang.Throwable)
     */
    public void serviceCheckError(org.eclipse.stp.sca.Service eObject, Throwable throwable)
    {
        this.test.count("ERROR", "SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ComponentReferenceParserEventRecipientItf#componentReferenceCheckDo(org.eclipse.stp.sca.ComponentReference)
     */
    public void componentReferenceCheckDo(ComponentReference eObject)
    {
        this.test.count("DO", "COMPONENT-REFERENCE",eObject.getName());    
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ComponentReferenceParserEventRecipientItf#componentReferenceCheckDone(org.eclipse.stp.sca.ComponentReference)
     */
    public void componentReferenceCheckDone(ComponentReference eObject)
    {
        this.test.count("DONE", "COMPONENT-REFERENCE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.dispatcher.ComponentReferenceParserEventRecipientItf#componentReferenceCheckError(org.eclipse.stp.sca.ComponentReference, java.lang.Throwable)
     */
    public void componentReferenceCheckError(ComponentReference eObject,
            Throwable throwable)
    {
        this.test.count("ERROR", "COMPONENT-REFERENCE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf#componentServiceCheckDo(org.eclipse.stp.sca.ComponentService)
     */
    public void componentServiceCheckDo(ComponentService eObject)
    {
        this.test.count("DO", "COMPONENT-SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf#componentServiceCheckDone(org.eclipse.stp.sca.ComponentService)
     */
    public void componentServiceCheckDone(ComponentService eObject)
    {
        this.test.count("DONE", "COMPONENT-SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf#componentServiceCheckError(org.eclipse.stp.sca.ComponentService, java.lang.Throwable)
     */
    public void componentServiceCheckError(ComponentService eObject,
            Throwable throwable)
    {
        this.test.count("ERROR", "COMPONENT-SERVICE",eObject.getName());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.parser.test.FraSCAtiComponentTesterItf#setTester(org.ow2.frascati.parser.test.FraSCAtiComponentTestItf)
     */
    public void setTester(FraSCAtiComponentTestItf test)
    {
        this.test = test;
        this.test.init("DO", "SERVICE");
        this.test.init("DONE", "SERVICE");
        this.test.init("ERROR", "SERVICE");
        this.test.init("DO", "REFERENCE");
        this.test.init("DONE", "REFERENCE");
        this.test.init("ERROR", "REFERENCE");
        this.test.init("DO", "COMPONENT-SERVICE");
        this.test.init("DONE", "COMPONENT-SERVICE");
        this.test.init("ERROR", "COMPONENT-SERVICE");
        this.test.init("DO", "COMPONENT-REFERENCE");
        this.test.init("DONE", "COMPONENT-REFERENCE");
        this.test.init("ERROR", "COMPONENT-REFERENCE");
    }

}
