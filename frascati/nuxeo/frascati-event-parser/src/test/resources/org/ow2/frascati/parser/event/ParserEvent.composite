<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- 
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *-->
 <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" name="org.ow2.frascati.parser.event.ParserEvent">  
   
    <service name="serref-recipient-tester" promote="serref-event-recipient/serref-recipient-tester">
        <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    </service>
    <service name="composite-recipient-tester" promote="composite-event-recipient/composite-recipient-tester">
        <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    </service>
    <service name="other-recipient-tester" promote="other-event-recipient/other-recipient-tester">
         <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    </service>
    	
    <component name="serref-event-recipient">
    	<implementation.java class="org.ow2.frascati.parser.test.impl.SerrefEventRecipient"/>    	
    	<service name="serref-recipient-tester">
            <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    	</service>
        <service name="service-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientServiceItf"/>
        </service>  
        <service name="reference-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientReferenceItf"/>
        </service>   
        <service name="component-reference-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentReferenceItf"/>
        </service>    
        <service name="component-service-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf"/>
        </service>       
    </component>

    <component name="composite-event-recipient">
    	<implementation.java class="org.ow2.frascati.parser.test.impl.CompositeEventRecipient"/>
    	<service name="composite-recipient-tester">
            <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    	</service>
        <service name="composite-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientCompositeItf"/>
        </service>
    </component>
    
    <component name="other-event-recipient">
    	<implementation.java class="org.ow2.frascati.parser.test.impl.AllOthersEventRecipient"/>
    	<service name="other-recipient-tester">
            <interface.java interface="org.ow2.frascati.parser.test.api.FraSCAtiComponentTesterItf"/>
    	</service>
        <service name="component-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf"/>
        </service>
        <service name="property-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf"/>
        </service>
        <service name="binding-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf"/>
        </service>
        <service name="sca-binding-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf"/>
        </service>  
        <service name="implementation-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf"/>
        </service>    
        <service name="interface-event-recipient" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf"/>
        </service>  
    </component>
    
    <wire source="composite-event-dispatcher/parser-event-recipients" target="composite-event-recipient/composite-event-recipient"/>
    
    <wire source="component-event-dispatcher/parser-event-recipients" target="other-event-recipient/component-event-recipient"/>
    <wire source="property-event-dispatcher/parser-event-recipients" target="other-event-recipient/property-event-recipient"/>
    <wire source="component-property-event-dispatcher/parser-event-recipients" target="other-event-recipient/property-event-recipient"/>
    <wire source="binding-event-dispatcher/parser-event-recipients" target="other-event-recipient/binding-event-recipient"/>
    <wire source="sca-binding-event-dispatcher/parser-event-recipients" target="other-event-recipient/binding-event-recipient"/>
    <wire source="implementation-event-dispatcher/parser-event-recipients" target="other-event-recipient/implementation-event-recipient"/>
    <wire source="interface-event-dispatcher/parser-event-recipients" target="other-event-recipient/interface-event-recipient"/>
    
    <wire source="service-event-dispatcher/parser-event-recipients" target="serref-event-recipient/service-event-recipient"/>
    <wire source="reference-event-dispatcher/parser-event-recipients" target="serref-event-recipient/reference-event-recipient"/>
    <wire source="component-reference-event-dispatcher/parser-event-recipients" target="serref-event-recipient/component-reference-event-recipient"/>
    <wire source="component-service-event-dispatcher/parser-event-recipients" target="serref-event-recipient/component-service-event-recipient"/>
 
</composite>