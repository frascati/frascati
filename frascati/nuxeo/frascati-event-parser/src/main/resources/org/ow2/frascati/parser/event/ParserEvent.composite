<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- 
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *-->
 <composite xmlns="http://www.osoa.org/xmlns/sca/1.0" 
 name="org.ow2.frascati.parser.event.ParserEvent">  
                       
    <service name="InitializableItf" promote="parser-event-weaver/InitializableItf">
        <interface.java interface="org.ow2.frascati.starter.api.InitializableItf"/>
    </service>
        
    <reference name="composite-processor" promote="parser-event-weaver/composite-processor">
          <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="component-processor" promote="parser-event-weaver/component-processor">
      <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="component-service-processor" promote="parser-event-weaver/component-service-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="component-reference-processor" promote="parser-event-weaver/component-reference-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="composite-service-processor" promote="parser-event-weaver/composite-service-processor">
        <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="composite-reference-processor" promote="parser-event-weaver/composite-reference-processor">
        <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>
    <reference name="binding-processor" promote="parser-event-weaver/binding-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>    
    <reference name="sca-binding-processor" promote="parser-event-weaver/sca-binding-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference> 
    <reference name="composite-property-processor" promote="parser-event-weaver/composite-property-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference> 
    <reference name="component-property-processor" promote="parser-event-weaver/component-property-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference>     
    <reference name="implementation-processor" promote="parser-event-weaver/implementation-processor">
       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference> 
    <reference name="interface-processor" promote="parser-event-weaver/interface-processor">
      <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
    </reference> 
        
    <component name="parser-event-weaver">
        <service name="InitializableItf">
            <interface.java interface="org.ow2.frascati.starter.api.InitializableItf"/>
        </service>       
        <reference name="composite-processor">
              <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="component-processor">
          <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="component-service-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="component-reference-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="composite-service-processor">
            <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="composite-reference-processor">
            <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>
        <reference name="binding-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference>  
        <reference name="sca-binding-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference> 
        <reference name="component-property-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference> 
        <reference name="composite-property-processor">
           <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
        </reference> 
	    <reference name="implementation-processor">
	       <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
	    </reference> 
	    <reference name="interface-processor">
	      <interface.java interface="org.ow2.frascati.assembly.factory.api.Processor"/>
	    </reference> 
        <reference multiplicity="0..1" name="next-initializable" >
            <interface.java interface="org.ow2.frascati.starter.api.InitializableItf"/>
        </reference>           
	    <reference name="binding-processor-intent" target="binding-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="sca-binding-processor-intent" target="sca-binding-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="service-processor-intent" target="service-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="reference-processor-intent" target="reference-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="component-service-processor-intent" target="component-service-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="component-reference-processor-intent" target="component-reference-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="component-property-processor-intent" target="component-property-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="property-processor-intent" target="property-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="component-processor-intent" target="component-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="composite-processor-intent" target="composite-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference> 
	    <reference name="implementation-processor-intent" target="implementation-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>
	    <reference name="interface-processor-intent" target="interface-processor-intent/processor-intent">
	    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
	    </reference>       
		<implementation.java class="org.ow2.frascati.parser.event.ParserEventWeaver"/>
    </component>
    
    <component name="binding-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentBinding"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="binding-processor-intent/parser-event-dispatcher" target="binding-event-dispatcher/binding-event-dispatcher"/>
    
    <component name="binding-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherBinding"/>
    	<service name="binding-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf"/>
        </reference>
    </component>
        
    <component name="sca-binding-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentBinding"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="sca-binding-processor-intent/parser-event-dispatcher" target="sca-binding-event-dispatcher/binding-event-dispatcher"/>
    
    <component name="sca-binding-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherBinding"/>
    	<service name="binding-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf"/>
        </reference>
    </component>
    
    <component name="service-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentService"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="service-processor-intent/parser-event-dispatcher" target="service-event-dispatcher/service-event-dispatcher"/>
    
    <component name="service-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherService"/>
    	<service name="service-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientServiceItf"/>
        </reference>
    </component>
    
    <component name="reference-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentReference"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="reference-processor-intent/parser-event-dispatcher" target="reference-event-dispatcher/reference-event-dispatcher"/>
    
    <component name="reference-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherReference"/>
    	<service name="reference-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientReferenceItf"/>
        </reference>
    </component>
    
    <component name="component-service-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentComponentService"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="component-service-processor-intent/parser-event-dispatcher" target="component-service-event-dispatcher/component-service-event-dispatcher"/>
    
    <component name="component-service-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherComponentService"/>
    	<service name="component-service-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentServiceItf"/>
        </reference>      
    </component>
    
    <component name="component-reference-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentComponentReference"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="component-reference-processor-intent/parser-event-dispatcher" target="component-reference-event-dispatcher/component-reference-event-dispatcher"/>
    
    <component name="component-reference-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherComponentReference"/>
    	<service name="component-reference-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentReferenceItf"/>
        </reference>      
    </component>
    
    <component name="property-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentProperty"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="property-processor-intent/parser-event-dispatcher" target="property-event-dispatcher/property-event-dispatcher"/>
    
    <component name="property-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherProperty"/>
    	<service name="property-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf"/>
        </reference>      
    </component>
    
    <component name="component-property-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentProperty"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="component-property-processor-intent/parser-event-dispatcher" target="component-property-event-dispatcher/component-property-event-dispatcher"/>
    
    <component name="component-property-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherProperty"/>
    	<service name="component-property-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientPropertyItf"/>
        </reference>      
    </component>
    
    <component name="component-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentComponent"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="component-processor-intent/parser-event-dispatcher" target="component-event-dispatcher/component-event-dispatcher"/>
    
    <component name="component-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherComponent"/>
    	<service name="component-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf"/>
        </reference>      
    </component>
    
    <component name="composite-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentComposite"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
    <wire source="composite-processor-intent/parser-event-dispatcher" target="composite-event-dispatcher/composite-event-dispatcher"/>
    
    <component name="composite-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherComposite"/>
    	<service name="composite-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientCompositeItf"/>
        </reference>       
    </component>
    
    <component name="implementation-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentImplementation"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
     <wire source="implementation-processor-intent/parser-event-dispatcher" target="implementation-event-dispatcher/implementation-event-dispatcher"/>
    
    <component name="implementation-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherImplementation"/>
    	<service name="implementation-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientImplementationItf"/>
        </reference>       
    </component>
    
    <component name="interface-processor-intent">
    	<implementation.java class="org.ow2.frascati.parser.event.intent.ProcessorIntentInterface"/>
    	<service name="processor-intent">
    		<interface.java interface="org.ow2.frascati.tinfi.api.IntentHandler"/>
    	</service>
    	<reference name="parser-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</reference>
    </component>
    
     <wire source="interface-processor-intent/parser-event-dispatcher" target="interface-event-dispatcher/interface-event-dispatcher"/>
    
    <component name="interface-event-dispatcher">
    	<implementation.java class="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherInterface"/>
    	<service name="interface-event-dispatcher">
    		<interface.java interface="org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf"/>
    	</service>
        <reference multiplicity="0..n" name="parser-event-recipients" >
            <interface.java interface="org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf"/>
        </reference>       
    </component>
    
</composite>