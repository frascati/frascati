/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.dispatcher;

import java.util.List;

import org.eclipse.stp.sca.Interface;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientInterfaceItf;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Implementation of a {@link ParserEventDispatcherItf} for a 
 * {@link org.eclipse.stp.sca.Interface} 
 */
public class ParserEventDispatcherInterface 
extends AbstractLoggeable implements ParserEventDispatcherItf<Interface>
{
    /**
     * The list of final recipients of parsing events
     */
    @Reference(name = "parser-event-recipients",required = false)
    protected List<ParserEventRecipientInterfaceItf> interfaceRecipients;
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDo(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDo(Interface eObject)
    {
        log.config("Interface["+eObject.eClass()+"] puchCheckDo");
        if(interfaceRecipients == null || interfaceRecipients.size()==0)
        {
            log.config("No ParserEventRecipientInterfaceItf defined");
            return;
        }
        for(ParserEventRecipientInterfaceItf interfaceRecipient : interfaceRecipients)
        {
            interfaceRecipient.interfaceCheckDo(eObject);    
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDone(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDone(Interface eObject)
    {
        log.config("Interface["+eObject.eClass()+"] puchCheckDone");
        if(interfaceRecipients == null || interfaceRecipients.size()==0)
        {
            log.config("No ParserEventRecipientInterfaceItf defined");
            return;
        }
        for(ParserEventRecipientInterfaceItf interfaceRecipient : interfaceRecipients)
        {
            interfaceRecipient.interfaceCheckDone(eObject);       
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckError(org.eclipse.emf.ecore.EObject, java.lang.Throwable)
     */
    public void pushCheckError(Interface eObject, Throwable throwable)
    {
        log.config("Interface["+eObject.eClass()+"] puchCheckError :" + throwable.getMessage());
        if(interfaceRecipients == null || interfaceRecipients.size()==0)
        {
            log.config("No ParserEventRecipientInterfaceItf defined");
            return;
        }
        for(ParserEventRecipientInterfaceItf interfaceRecipient : interfaceRecipients)
        {
            interfaceRecipient.interfaceCheckError(eObject, throwable);   
        }
    }
}
