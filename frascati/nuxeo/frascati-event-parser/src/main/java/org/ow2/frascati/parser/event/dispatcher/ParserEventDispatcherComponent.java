/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.dispatcher;

import java.util.List;

import org.eclipse.stp.sca.Component;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientComponentItf;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Implementation of a {@link ParserEventDispatcherItf} for a 
 * {@link org.eclipse.stp.sca.Component} 
 */
public class ParserEventDispatcherComponent 
extends AbstractLoggeable implements ParserEventDispatcherItf<Component>
{
    /**
     * The list of final recipients of parsing events
     */
    @Reference(name = "parser-event-recipients",required = false)
    protected List<ParserEventRecipientComponentItf> componentRecipients;
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDo(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDo(Component eObject)
    {
        log.config("Component["+eObject.eClass()+"] puchCheckDo");
        if(componentRecipients == null || componentRecipients.size()==0)
        {
            log.config("No ParserEventRecipientComponentItf defined");
            return;
        }
        for(ParserEventRecipientComponentItf componentRecipient : componentRecipients)
        {
            componentRecipient.componentCheckDo(eObject);    
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDone(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDone(Component eObject)
    {
        log.config("Component["+eObject.eClass()+"] puchCheckDone");
        if(componentRecipients == null || componentRecipients.size()==0)
        {
            log.config("No ParserEventRecipientComponentItf defined");
            return;
        }
        for(ParserEventRecipientComponentItf componentRecipient : componentRecipients)
        {
            componentRecipient.componentCheckDone(eObject);       
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckError(org.eclipse.emf.ecore.EObject, java.lang.Throwable)
     */
    public void pushCheckError(Component eObject, Throwable throwable)
    {
        log.config("Component["+eObject.eClass()+"] puchCheckError :" + throwable.getMessage());
        if(componentRecipients == null || componentRecipients.size()==0)
        {
            log.config("No ParserEventRecipientComponentItf defined");
            return;
        }
        for(ParserEventRecipientComponentItf componentRecipient : componentRecipients)
        {
            componentRecipient.componentCheckError(eObject, throwable);   
        }
    }
}
