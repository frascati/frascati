/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.dispatcher;

import java.util.List;

import org.eclipse.stp.sca.Binding;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.parser.event.recipient.ParserEventRecipientBindingItf;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Implementation of a {@link ParserEventDispatcherItf} for a 
 * {@link org.eclipse.stp.sca.Binding} 
 */
public class ParserEventDispatcherBinding 
extends AbstractLoggeable implements ParserEventDispatcherItf<Binding>
{
    /**
     * The list of final recipients of parsing events
     */
    @Reference(name = "parser-event-recipients",required = false)
    protected List<ParserEventRecipientBindingItf> bindingRecipients;
    
    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDo(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDo(Binding eObject)
    {
        log.config("Binding["+eObject.eClass()+"] puchCheckDo");
        if(bindingRecipients == null || bindingRecipients.size()==0)
        {
            log.config("No ParserEventRecipientBindingItf defined");
            return;
        }
        for(ParserEventRecipientBindingItf bindingRecipient : bindingRecipients)
        {
            bindingRecipient.bindingCheckDo(eObject);    
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckDone(org.eclipse.emf.ecore.EObject)
     */
    public void pushCheckDone(Binding eObject)
    {
        log.config("Binding["+eObject.eClass()+"] puchCheckDone");
        if(bindingRecipients == null || bindingRecipients.size()==0)
        {
            log.config("No ParserEventRecipientBindingItf defined");
            return;
        }
        for(ParserEventRecipientBindingItf bindingRecipient : bindingRecipients)
        {
            bindingRecipient.bindingCheckDone(eObject);       
        }
    }

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf#
     * pushCheckError(org.eclipse.emf.ecore.EObject, java.lang.Throwable)
     */
    public void pushCheckError(Binding eObject, Throwable throwable)
    {
        log.config("Binding["+eObject.eClass()+"] puchCheckError :" + throwable.getMessage());
        if(bindingRecipients == null || bindingRecipients.size()==0)
        {
            log.config("No ParserEventRecipientBindingItf defined");
            return;
        }
        for(ParserEventRecipientBindingItf bindingRecipient : bindingRecipients)
        {
            bindingRecipient.bindingCheckError(eObject, throwable);   
        }
    }
 }
