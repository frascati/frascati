/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.intent;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.ecore.EObject;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.parser.event.dispatcher.ParserEventDispatcherItf;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

/**
 * Define the shared code between all processors' (@see org.ow2.frascati.assembly.factory.api.Processor) 
 * Intents (@see org.ow2.frascati.tinfi.api.IntentHandler)
 * 
 * @param <EObjectType>
 *      the object type handled by the targeted processor
 * @param <P>
 *      the processor observer type, which has also to handle the T type
 */
@Scope("COMPOSITE")
@Service(interfaces = { IntentHandler.class})
public abstract class ProcessorIntent<EObjectType extends EObject> implements IntentHandler
{
    protected static Logger log = Logger.getLogger(
            ProcessorIntent.class.getCanonicalName());

    /**
     * The list of recipients of parsing events
     */
    @Reference(name = "parser-event-dispatcher")
    protected ParserEventDispatcherItf<EObjectType> dispatcher;
    
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.tinfi.api.IntentHandler
     *      #invoke(org.ow2.frascati.tinfi.api.IntentJoinPoint)
     */
    @SuppressWarnings("unchecked")
    public Object invoke(IntentJoinPoint intentJoinPoint) throws Throwable
    {
        if(dispatcher == null)
        {
            return intentJoinPoint.proceed();
        }        
        EObjectType eObject = null;
        Object argument = intentJoinPoint.getArguments()[0];      
        Object result = null;
        try
        {
            eObject = (EObjectType) argument;
            
        } catch (ClassCastException e)
        {
            log.log(Level.WARNING,e.getMessage(),e);
            return intentJoinPoint.proceed();
        }
        dispatcher.pushCheckDo(eObject); 
        try
        {
            result = intentJoinPoint.proceed();
            
        } catch(Throwable throwable)
        {
            
            dispatcher.pushCheckError(eObject,throwable); 
            throw throwable;
        } 
        dispatcher.pushCheckDone(eObject); 
        return result;
    }

}
