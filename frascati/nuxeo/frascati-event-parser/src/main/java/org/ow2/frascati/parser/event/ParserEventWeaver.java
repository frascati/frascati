/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event;

import java.lang.reflect.Method;
import java.util.logging.Level;

import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.Property;
import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.starter.api.AbstractInitializable;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.util.reference.ServiceReferenceUtil;


/**
 * The ParserEventWeaver weave an {@link org.ow2.frascati.tinfi.api.IntentHandler} to each 
 * {@link org.ow2.frascati.assembly.factory.api.Processor} implied in the SCA Composite
 * parsing process  
 * */
public class ParserEventWeaver extends AbstractInitializable
{

    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    //EObject processors to weave ProcessorIntents with
    /** required {@link org.eclipse.stp.sca.Composite} processor */
    @Reference(name = "composite-processor")
    private Processor<org.eclipse.stp.sca.Composite> processorComposite;
    
    /** required {@link org.eclipse.stp.sca.Component} processor */
    @Reference(name = "component-processor")
    private Processor<org.eclipse.stp.sca.Component> processorComponent;
    
    /** required {@link org.eclipse.stp.sca.ComponentReference} processor */
    @Reference(name = "component-reference-processor")
    private Processor<ComponentReference> processorComponentReference;
    
    /** required {@link org.eclipse.stp.sca.ComponentService} processor */
    @Reference(name = "component-service-processor")
    private Processor<ComponentService> processorComponentService;
    
    /** required {@link org.eclipse.stp.sca.Reference} processor */
    @Reference(name = "composite-reference-processor")
    private Processor<org.eclipse.stp.sca.Reference> processorReference;
    
    /** required {@link org.eclipse.stp.sca.Service} processor */
    @Reference(name = "composite-service-processor")
    private Processor<org.eclipse.stp.sca.Service> processorService;
    
    /** required {@link org.eclipse.stp.sca.Binding} processor */
    @Reference(name = "binding-processor")
    private Processor<Binding> processorBinding;
    
    /** required SCA {@link org.eclipse.stp.sca.Binding} processor */
    @Reference(name = "sca-binding-processor")
    private Processor<Binding> scaProcessorBinding;

    /** required SCA {@link org.eclipse.stp.sca.Property} processor */
    @Reference(name = "composite-property-processor")
    private Processor<Property> processorProperty;
    
    /** required SCA component {@link org.eclipse.stp.sca.Property} processor */
    @Reference(name = "component-property-processor")
    private Processor<Property> processorComponentProperty;

    /** required SCA component {@link org.eclipse.stp.sca.Implementation} processor */
    @Reference(name = "implementation-processor")
    private Processor<Implementation> processorImplementation;
    
    /** required SCA component {@link org.eclipse.stp.sca.Interface} processor */
    @Reference(name = "interface-processor")
    private Processor<Interface> processorInterface;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Binding} processor */
    @Reference(name="binding-processor-intent")
    private IntentHandler bindingProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * SCA {@link org.eclipse.stp.sca.Binding} processor */
    @Reference(name="sca-binding-processor-intent")
    private IntentHandler scaBindingProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Service} processor */
    @Reference(name="service-processor-intent")
    private IntentHandler serviceProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Reference} processor */
    @Reference(name="reference-processor-intent")
    private IntentHandler referenceProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.ComponentService} processor */
    @Reference(name="component-service-processor-intent")
    private IntentHandler componentServiceProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.ComponentReference} processor */
    @Reference(name="component-reference-processor-intent")
    private IntentHandler componentReferenceProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Property} processor */
    @Reference(name="property-processor-intent")
    private IntentHandler propertyProcessorIntent;
    
    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Property} processor */
    @Reference(name="component-property-processor-intent")
    private IntentHandler componentPropertyProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Component} processor */
    @Reference(name="component-processor-intent")
    private IntentHandler componentProcessorIntent;

    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Composite} processor */
    @Reference(name="composite-processor-intent")
    private IntentHandler compositeProcessorIntent;
    
    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Implementation} processor */
    @Reference(name="implementation-processor-intent")
    private IntentHandler implementationProcessorIntent;
    
    /** required {@link org.ow2.frascati.tinfi.api.IntentHandler} for 
     * {@link org.eclipse.stp.sca.Interface} processor */
    @Reference(name="interface-processor-intent")
    private IntentHandler interfaceProcessorIntent;
    
    // -------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------
    
    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------

    /**
     *  @{inheritDoc}
     *  
     * @see org.ow2.frascati.starter.api.AbstractInitializable#doInitialize()
     */
    public void doInitialize()
    {
        TinfiComponentInterceptor<?> tci = null;
        Component component = null;

        //list of Processors to weave the intents with
        Object[] serviceReferences = new Object[] { 
                processorComposite, 
                processorComponent, 
                processorComponentReference,
                processorComponentService, 
                processorReference,
                processorService, 
                processorBinding, 
                scaProcessorBinding,
                processorProperty ,
                processorComponentProperty,
                processorImplementation,
                processorInterface,
                };
        //list of targeted interfaces name for each Processor respectively
        String[] interfaces = new String[] { 
                "composite-processor", 
                "component-processor",
                "component-reference-processor",
                "component-service-processor",
                "composite-reference-processor",
                "composite-service-processor", 
                "binding-processor",
                "binding-processor",
                "property-processor",
                "property-processor",
                "implementation-processor",
                "interface-processor"
                };
        //list of the IntentHandlers to weave to Processors with
        IntentHandler[] processorIntents = new IntentHandler[]{
                compositeProcessorIntent,
                componentProcessorIntent,
                componentReferenceProcessorIntent,
                componentServiceProcessorIntent,
                referenceProcessorIntent,
                serviceProcessorIntent,
                bindingProcessorIntent,
                scaBindingProcessorIntent,
                propertyProcessorIntent,
                componentPropertyProcessorIntent,
                implementationProcessorIntent,
                interfaceProcessorIntent
                };
        try
        {
            //ckeck method for Processors
            Method check = Processor.class.getDeclaredMethod("check",
                    new Class<?>[] { Object.class, ProcessingContext.class });            
            int n = 0;
            for (; n < serviceReferences.length; n++)
            {   //do weave
                try
                {                        
                    component = ServiceReferenceUtil.getRootComponent(
                            (ServiceReference<?>) serviceReferences[n]);
                    
                    tci = (TinfiComponentInterceptor<?>) ((ComponentInterface) component
                            .getFcInterface(interfaces[n])).getFcItfImpl();
                    
                    IntentHandler handler = processorIntents[n];
                    
                    tci.addIntentHandler(handler, check);
                    
                } catch (NoSuchInterfaceException e)
                {
                    log.log(Level.WARNING,e.getMessage(),e);
                }
            }              
        } catch (SecurityException e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
            
        } catch (NoSuchMethodException e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
            
        } catch(NullPointerException e)
        {
            log.log(Level.SEVERE,e.getMessage(),e);
        }
    }
}
