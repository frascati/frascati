/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.dispatcher;

import org.eclipse.emf.ecore.EObject;
import org.osoa.sca.annotations.Service;

/**
 * the FraSCAti's {@link org.ow2.frascati.parser.api.Parser} events dispatcher definition 
 */
@Service
public interface ParserEventDispatcherItf<T extends EObject>
{
    /**
     * Push the {@link org.eclipse.emf.ecore.EObject} before it has being checked
     *  
     * @param eObject
     *          the EObject to push
     */
    void pushCheckDo(T eObject);
    
    /**
     * Push the {@link org.eclipse.emf.ecore.EObject} after it has being checked
     *  
     * @param eObject
     *          the EObject to push
     */
    void pushCheckDone(T eObject);
    
    /**
     * Push the {@link org.eclipse.emf.ecore.EObject} if an error occurred during its checking
     * process
     *  
     * @param eObject
     *          the EObject to push
     */
    void pushCheckError(T eObject, Throwable throwable);
}
