/**
 * OW2 FraSCAti - Parser Event
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.parser.event.recipient;

import org.eclipse.stp.sca.Component;
import org.osoa.sca.annotations.Service;

/**
 * Recipient for check events relative to {@link org.eclipse.stp.sca.Component}
 */
@Service
public interface ParserEventRecipientComponentItf
{
    /**
     * Push the {@link org.eclipse.stp.sca.Component} before it has being checked
     *  
     * @param eObject
     *          the EObject to push
     */
    void componentCheckDo(Component eObject);
    
    /**
     * Push the {@link org.eclipse.stp.sca.Component} after it has being checked
     *  
     * @param eObject
     *          the EObject to push
     */
    void componentCheckDone(Component eObject);
    
    /**
     * Push the {@link org.eclipse.stp.sca.Component} if an error occurred during its checking
     * process
     *  
     * @param eObject
     *          the EObject to push
     */
    void componentCheckError(Component eObject, Throwable throwable);
}
