/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.api;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.nuxeo.runtime.bridge.Application;

import org.ow2.frascati.isolated.FraSCAtiIsolated;

import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoException;

/**
 * Abstract implementation of the FraSCAtiServiceProviderItf interface 
 * AbstractFraSCAtiInNuxeo allow to instantiate an new FraSCAti instance in Nuxeo
 */
public abstract class AbstractFraSCAtiInNuxeo<FraSCAtiInNuxeoService extends FraSCAtiInNuxeoServiceItf> 
extends FraSCAtiIsolated implements FraSCAtiInNuxeoServiceProviderItf<FraSCAtiInNuxeoService>, Application 
{   
    /**
     * Logger
     */
    protected Logger logger = Logger.getLogger(getClass().getCanonicalName());

    /**
     * the provided FraSCAtiInNuxeoServiceItf implementation instance
     */
    private FraSCAtiInNuxeoService frascatiService;
    
    /**
     * Constructor
     * @throws Exception 
     */
    protected AbstractFraSCAtiInNuxeo(File librariesDirectory, ClassLoader parentClassLoader) 
    throws FraSCAtiInNuxeoException, Exception
    {
        super(librariesDirectory,parentClassLoader);
        logger.log(Level.INFO, "FraSCAti In Nuxeo initialisation");
        
        try
        {   
            Class<FraSCAtiInNuxeoService> providedType = getProvidedType();
            
            String serviceName = getProvidedTypeProperty(
                    FraSCAtiInNuxeoServiceProviderItf.FRASCATI_IN_NUXEO_SERVICE_SERVICE_NAME_PROP);
            
            String componentName = getProvidedTypeProperty(
                    FraSCAtiInNuxeoServiceProviderItf.FRASCATI_IN_NUXEO_SERVICE_COMPONENT_NAME_PROP);
            
            logger.log(Level.INFO, "Search for service '" +
                    serviceName + "' [" + providedType.getCanonicalName() + " ] in the '" +
                    componentName + "' component");
            
            frascatiService = super.getService(providedType, serviceName, 
                    "org.ow2.frascati.FraSCAti/" + componentName);
            
        } catch (Throwable throwable)
        {
            throw new FraSCAtiInNuxeoException(
                    "Unable to retrieve the FraSCAtiInNuxeoServiceItf implementing service",
                    throwable);
        }
    }

    /**
     * Return the type of the provided service
     */
    @SuppressWarnings("unchecked")
    private Class<FraSCAtiInNuxeoService>  getProvidedType()
    {
       return (Class<FraSCAtiInNuxeoService>)
       ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    
    /**
     * Return the value of a public static attribute declared in the provided 
     * service's implemented interface
     * 
     * @param property
     *          the search public static attribute name
     */
    private String getProvidedTypeProperty(String property)
    {
        try
        {
            Field propertyField = getProvidedType().getDeclaredField(property);
            return (String) propertyField.get(null);
            
        } catch (Throwable throwable)
        {
            logger.log(Level.WARNING,throwable.getMessage(),throwable);
        } 
        return null;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceProviderItf#
     * getFraSCAtiInNuxeoService()
     */
    public FraSCAtiInNuxeoService getFraSCAtiInNuxeoService()
    {
        return frascatiService;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.bridge.Application#getService(java.lang.Class)
     */
    @SuppressWarnings("unchecked")
    public <T> T getService(Class<T> type)
    {
        if (type.isAssignableFrom(getClass()))
        {
            return (T) this;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.bridge.Application#destroy()
     */
    public void destroy()
    {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name;
        try
        {
            name = new ObjectName("SCA domain:name0=*,*");
            Set<ObjectName> names = mbs.queryNames(name, name);
            for (ObjectName objectName : names)
            {
                mbs.unregisterMBean(objectName);
            }
            mbs.unregisterMBean(new ObjectName(
                    "org.ow2.frascati.jmx:name=FrascatiJmx"));
            
        } catch (Throwable throwable)
        {
            logger.log(Level.CONFIG,throwable.getMessage(),throwable);            
        }
        try
        {
            super.stop();
            
        } catch (Throwable throwable)
        {
            logger.log(Level.WARNING,throwable.getMessage(),throwable);
        }
        frascatiService = null;
    }

}
