/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.api;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.nuxeo.common.Environment;

import org.ow2.frascati.isolated.FraSCAtiIsolated;

import org.nuxeo.runtime.bridge.Application;
import org.nuxeo.runtime.bridge.ApplicationDescriptor;
import org.nuxeo.runtime.bridge.ApplicationFactory;

/**
 * The AbstractFraSCAtiInNuxeoFactory is used by the Nuxeo runtime bridge to build a
 * FraSCAti instance
 */
public abstract class AbstractFraSCAtiInNuxeoFactory<FraSCAtiInNuxeoService extends FraSCAtiInNuxeoServiceItf, 
FraSCAtiInNuxeo extends AbstractFraSCAtiInNuxeo<FraSCAtiInNuxeoService>> 
implements ApplicationFactory
{
    /**
     * Return a FraSCAtiInNuxeoServiceProviderItf implementation instance
     */
    protected abstract FraSCAtiInNuxeo getFraSCAtiInNuxeoInstance(String librariesDirectory,
            ClassLoader parent);  

    /**
     * FraSCAti output directory property
     * */
    private static final String FRASCATI_OUTPUT_DIRECTORY_PROPERTY = 
         "org.ow2.frascati.output.directory";
    
    // Logger
    protected Logger logger = Logger.getLogger(getClass().getCanonicalName());

    /**
     * {@inheritDoc}
     * 
     * @see org.nuxeo.runtime.bridge.ApplicationFactory
     *      #createApplication(org.nuxeo.runtime.bridge.ApplicationDescriptor)
     */
    public Application createApplication(ApplicationDescriptor desc)
    throws Exception
    {
        logger.log(Level.INFO, "Create a FraSCAti In Nuxeo Application");
        char sep = File.separatorChar;
        
        URLClassLoader urlClassLoader = (URLClassLoader) 
        Thread.currentThread().getContextClassLoader();
        
        if(logger.isLoggable(Level.CONFIG))
        {
            logger.log(Level.CONFIG, "ContextClassLoader found : " + urlClassLoader);
        }        
        String home = Environment.getDefault().getHome().getAbsolutePath();
        
        //Retrieve all configuration properties
        String frascatiRootDirPath = System.getProperty(
                FraSCAtiInNuxeoServiceProviderItf.FRASCATI_IN_NUXEO_BASEDIR_PROP,
                "");

        if(frascatiRootDirPath.isEmpty())
        {
            frascatiRootDirPath = new StringBuilder(home).append(sep
                    ).append("frascati").toString();
        }    
        String outputdir = System.getProperty(
                FraSCAtiInNuxeoServiceProviderItf.FRASCATI_IN_NUXEO_OUTPUT_DIRECTORY_PROP,
                "");         
        if(outputdir.isEmpty())
        {
            outputdir = new StringBuilder(home).append(sep).append(
                "tmp").toString();
        }        
        System.setProperty(FRASCATI_OUTPUT_DIRECTORY_PROPERTY, outputdir);
        
        if(logger.isLoggable(Level.CONFIG))
        {
            logger.log(Level.CONFIG, "Define the '" + 
                    FRASCATI_OUTPUT_DIRECTORY_PROPERTY + "' property : " + 
                    outputdir);
        }           
        URL[] urls = urlClassLoader.getURLs();
        if (urls == null || urls.length == 0)
        {
            logger.log(Level.WARNING, "No classpath entry found to initialize the ClassLoader");

        } else if (logger.isLoggable(Level.CONFIG))
        {
            for (URL url : urls)
            {
                logger.log(Level.CONFIG, "Added classpath entry :" + url.toExternalForm());
            }
        }
        if (desc != null && logger.isLoggable(Level.CONFIG))
        {
            logger.log(Level.CONFIG, "ApplicationDescriptor found\nisolated status required : "
                            + desc.isIsolated());
            
        } else if (desc == null)
        {
            logger.log(Level.WARNING, "No ApplicationDescriptor found");
        }        
        return getFraSCAtiInNuxeoInstance(frascatiRootDirPath,urlClassLoader);
    }
}
