/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.nuxeo;

import org.nuxeo.runtime.api.Framework;
import org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf;

/**
 * The content implementation of Nuxeo components.
 */
public class NuxeoSkeletonContent extends AbstractSkeletonContent implements
        NuxeoSkeletonContentAttributes, ExportedItf
{
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * the name of the component providing the service
     */
    private String nxservice;
    
    /**
     * Delegate which cope with Nuxeo interactions
     */
    private ExportedDelegateItf exportedDelegate;
   
    // ---------------------------------------------------------------------------
    // Internal methods.
    // ---------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // ---------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     *
     * @see org.objectweb.fractal.bf.connectors.nuxeo.NuxeoSkeletonContentAttributes#
     * getNxservice()
     */
    public String getNxservice()
    {
        return this.nxservice;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.objectweb.fractal.bf.connectors.nuxeo.NuxeoSkeletonContentAttributes#
     * setNxservice(java.lang.String)
     */
    public void setNxservice(String nxservice)
    {
       this.nxservice = nxservice;        
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent#
     *      startFc()
     */
    @Override
    public void startFc()
    {
        log.info("Skeleton will be started...");
        super.startFc(); 
        try
        {
            this.exportedDelegate = 
               ((ExportedServiceLoaderItf)Framework.getRuntime().getComponent(
                       ExportedServiceLoaderItf.EXPORTED_SERVICE_LOADER)
                        ).createExportedDelegate(this);
            
        } catch (Exception e)
        {
            e.printStackTrace();
        };  
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractSkeletonContent#
     *      stopFc()
     */
    @Override
    public void stopFc()
    {
        log.info("ExportedDelegate will be stopped...");
        exportedDelegate.stopDelegate();
        exportedDelegate = null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf#getExportedObject()
     */
    public Object getExportedObject()
    {
        return super.getServant();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf#stopExported()
     */
    public void stopExported()
    {
      log.info("Skeleton will be stopped...");
      super.stopFc();   
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.ExportedItf#getInterfaceName()
     */
    public String getInterfaceName()
    {
        return super.getServiceClass().getCanonicalName();
    }
}
