/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.nuxeo;

import java.util.Map;
import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

//import java.util.logging.Level;
//import java.util.HashMap;
//import org.objectweb.fractal.api.Component;
//import org.objectweb.fractal.api.NoSuchInterfaceException;
//import org.objectweb.fractal.api.factory.GenericFactory;
//import org.objectweb.fractal.api.factory.InstantiationException;
//import org.objectweb.fractal.api.type.InterfaceType;
//import org.objectweb.fractal.bf.BindingFactoryException;
//import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
//import org.objectweb.fractal.julia.type.BasicComponentType;
//import org.objectweb.fractal.julia.type.BasicInterfaceType;
//import org.objectweb.fractal.util.Fractal;

/**
 * A Nuxeo-Connector can export and bind Fractal interfaces via Nuxeo services.
 */
public class NuxeoConnector extends
        AbstractConnector<NuxeoExportHints, NuxeoBindHints>
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

//    private NuxeoBindingContainerItf bindingContainer;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    
//    private Component createComponent(String interfaceName,String interfaceClassName,Object content) 
//    throws InstantiationException, NoSuchInterfaceException
//    {
//        GenericFactory factory = (GenericFactory) Fractal.getBootstrapComponent(
//                ).getFcInterface("generic-factory");
//
//        BasicComponentType basicComponentType = new BasicComponentType(
//                new InterfaceType[]{
//                   new BasicInterfaceType(interfaceName,interfaceClassName,false,false,false)//,
//                   new BasicInterfaceType("sca-intent-controller","org.ow2.frascati.tinfi.api.control.SCABasicIntentController",false,false,false),
//                   new BasicInterfaceType("sca-component-controller","org.oasisopen.sca.ComponentContext",false,false,false),
//                   new BasicInterfaceType("sca-property-controller","org.ow2.frascati.tinfi.api.control.SCAPropertyController",false,false,false),
//                   new BasicInterfaceType("component","org.objectweb.fractal.api.Component",false,false,false),
//                   new BasicInterfaceType("binding-controller","org.objectweb.fractal.api.control.BindingController",false,false,false),
//                   new BasicInterfaceType("content-controller","org.objectweb.fractal.api.control.ContentController",false,false,false),
//                   new BasicInterfaceType("super-controller","org.objectweb.fractal.julia.control.content.SuperControllerNotifier",false,false,false),
//                   new BasicInterfaceType("lifecycle-controller","org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator",false,false,false),
//                   new BasicInterfaceType("name-controller","org.objectweb.fractal.api.control.NameController",false,false,false)
//                   });
//
//        Component component = factory.newFcInstance(
//               basicComponentType, "primitive",  content);
//        
//        Fractal.getNameController(component).setFcName(interfaceName); 
//        
//        return component;
//    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      getSkeletonAdl()
     */
    protected String getSkeletonAdl()
    {
        return "org.objectweb.fractal.bf.connectors.nuxeo.NuxeoSkeleton";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     * initSkeletonAdlContext(org.objectweb.fractal.bf.ExportHints, java.util.Map)
     */
    @Override
    protected void initSkeletonAdlContext(NuxeoExportHints exportHints,
            Map<String, Object> context)
    {
        log.info("initSkeletonAdlContext");
        String nxservice = exportHints.getNxservice();
        if (nxservice == null)
        {
            nxservice = NuxeoConnectorConstants.DEFAULT_NXSERVICE_VALUE;
            log.warning("Export hints didn't specify "
                    + NuxeoConnectorConstants.NXSERVICE + ", going to use "
                    + NuxeoConnectorConstants.DEFAULT_NXSERVICE_VALUE);
        }
        context.put(NuxeoConnectorConstants.NXSERVICE, nxservice);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     *      getStubAdl()
     */
    protected String getStubAdl()
    {
        return "org.objectweb.fractal.bf.connectors.nuxeo.NuxeoStub";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#
     * initStubAdlContext(org.objectweb.fractal.bf.BindHints, java.util.Map)
     */
    @Override
    protected void initStubAdlContext(NuxeoBindHints bindHints,
            Map<String, Object> context)
    {
        log.info("initStubAdlContext");
        String nxservice = bindHints.getNxservice();
        if (nxservice == null)
        {
            nxservice = NuxeoConnectorConstants.DEFAULT_NXSERVICE_VALUE;
            log.warning("Bind hints didn't specify "
                    + NuxeoConnectorConstants.NXSERVICE + ", going to use "
                    + NuxeoConnectorConstants.DEFAULT_NXSERVICE_VALUE);
        }
        context.put(NuxeoConnectorConstants.NXSERVICE, nxservice);
        generateStubContentClass(NuxeoStubContent.class, context);
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
     */
    public String getPluginIdentifier()
    {
        return "nuxeo";
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(java.util.Map)
     */
    public NuxeoExportHints getExportHints(Map<String, Object> initialHints)
    {
        NuxeoExportHints nuxeoExportHints = new NuxeoExportHints();
        String nxservice = (String) initialHints.get(NuxeoConnectorConstants.NXSERVICE);
        nuxeoExportHints.setNxservice(nxservice);
        return nuxeoExportHints;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(java.util.Map)
     */
    public NuxeoBindHints getBindHints(Map<String, Object> initialHints)
    {
        NuxeoBindHints nuxeoBindHints = new NuxeoBindHints();
        String nxservice = (String) initialHints.get(NuxeoConnectorConstants.NXSERVICE);        
        nuxeoBindHints.setNxservice(nxservice);
        return nuxeoBindHints;
    }
    
//    /**
//     * {@inheritDoc}
//     * 
//     * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector
//     *      #finalizeStub(org.objectweb.fractal.api.Component, java.util.Map)
//     */
//    @Override
//    public void finalizeStub(Component stub, Map<String, Object> hints)
//    {
//        if(bindingContainer == null)
//        {
//            bindingContainer = new NuxeoBindingContainer();
//        }
//        try
//        {            
//            super.finalizeStub(stub, hints);
//            
//            NuxeoStubContentAttributes ska = (NuxeoStubContentAttributes) 
//            Fractal.getAttributeController(stub);
//            
//            Map<String,Object> wshints = new HashMap<String,Object>();
//            wshints.put("plugin.id","ws");
//            wshints.put(WsConnectorConstants.URI, "http://localhost:18000/nuxeoService/" + 
//                    ska.getServiceClass().getSimpleName());           
//            try
//            {                
//                Component toBind = createComponent(
//                        "nuxeo-service-" + ska.getServiceClass().getSimpleName().toLowerCase(),
//                        ska.getServiceClass().getCanonicalName(),
//                        ska.getServiceObject());
//                
//                Component stubParent = Fractal.getSuperController(stub).getFcSuperComponents()[0];
//                Fractal.getContentController(stubParent).addFcSubComponent(toBind);
//                
//                bindingContainer.addComponent(toBind, "nuxeo-service-" + 
//                                ska.getServiceClass().getSimpleName().toLowerCase(),
//                                wshints);
//                
//            } catch (InstantiationException e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (Exception e)
//            {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        } catch (BindingFactoryException e)
//        {
//            log.log(Level.WARNING,e.getMessage(),e);
//            
//        }
//        catch (NoSuchInterfaceException e)
//        {
//            log.log(Level.WARNING,e.getMessage(),e);
//        }
//    }   
}
