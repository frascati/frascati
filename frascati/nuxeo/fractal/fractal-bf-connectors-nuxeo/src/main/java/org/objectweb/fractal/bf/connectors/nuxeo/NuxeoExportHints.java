/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.objectweb.fractal.bf.connectors.nuxeo;

import org.objectweb.fractal.bf.ExportHints;

/**
 * The Nuxeo export hints.
 */
public class NuxeoExportHints implements ExportHints
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * the name of the component providing the service
     */
    private String nxservice;
    

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    /**
     * Return the nxservice attribute value 
     * nxservice value is used to identify the component providing the binded service
     * 
     * @return the nxservice attribute value
     */
    public String getNxservice()
    {
        return this.nxservice;
    }

    /**
     * Set the nxservice attribute value
     * nxservice value is used to identify the component providing the binded service
     * 
     * @param nxservice
     *            the nxservice attribute to set
     */
    public void setNxservice(String nxservice)
    {
       this.nxservice = nxservice;        
    }
    /**
     * {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder("NuxeoExportHints {");
        builder.append(nxservice != null ? nxservice : "");
        builder.append(" }");
        return builder.toString();
    }
}
