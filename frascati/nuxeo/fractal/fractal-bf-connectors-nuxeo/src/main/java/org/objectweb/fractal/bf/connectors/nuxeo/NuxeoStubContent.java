/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 */
package org.objectweb.fractal.bf.connectors.nuxeo;

import org.nuxeo.runtime.api.Framework;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.connectors.common.AbstractStubContent;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedDelegateItf;
import org.ow2.frascati.nuxeo.service.bridge.api.BindedItf;
import org.ow2.frascati.nuxeo.service.bridge.api.ExportedServiceLoaderItf;

/**
 * A super class for Nuxeo stubs. Concrete classes are generated on the fly.
 */
public class NuxeoStubContent extends AbstractStubContent implements
        NuxeoStubContentAttributes, BindedItf
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * the name of the component providing the service
     */
    private String nxservice;
    
    private Object serviceObject = null;
    private BindedDelegateItf bindedDelegate = null;
    
    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------
    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     *
     * @see org.objectweb.fractal.bf.connectors.nuxeo.NuxeoSkeletonContentAttributes#
     * getNxservice()
     */
    public String getNxservice()
    {
        return this.nxservice;
    }

    /**
     * {@inheritDoc}
     *
     * @see org.objectweb.fractal.bf.connectors.nuxeo.NuxeoSkeletonContentAttributes#
     * setNxservice(java.lang.String)
     */
    public void setNxservice(String nxservice)
    {
       this.nxservice = nxservice;        
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.common.AbstractStubContent#resolveReference()
     */
    @Override
    protected Object resolveReference() throws Exception
    {
        if(this.bindedDelegate == null)
        {
            this.bindedDelegate = 
                ((ExportedServiceLoaderItf)Framework.getRuntime().getComponent(
                        ExportedServiceLoaderItf.EXPORTED_SERVICE_LOADER)
                ).createBindedDelegate(this); 
        }
        if(serviceObject == null)
        {
           this.serviceObject = this.bindedDelegate.getServiceObject();       
        }
        return serviceObject;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.objectweb.fractal.bf.connectors.nuxeo.NuxeoStubContentAttributes#getServiceObject()
     */
    public Object getServiceObject()
    {
        return this.serviceObject;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.binding.NuxeoStubItf#start()
     */
    public void startBinded()
    {
        try
        {
            super.startFc();
            
        } catch (IllegalLifeCycleException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.binding.NuxeoStubItf#stop()
     */
    public void stopBinded()
    {
        super.stopFc();
        this.bindedDelegate = null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.service.bridge.api.BindedItf#getInterfaceName()
     */
    public String getInterfaceName()
    {
        return super.getServiceClass().getCanonicalName();
    }
}
