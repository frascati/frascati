/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.base.api;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.namespace.QName;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceBaseItf;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoServiceException;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * Implementation of the {@link FraSCAtiNuxeoServiceItf}
 */
@Scope("COMPOSITE") 
public abstract class 
AbstractFraSCAtiInNuxeoService<Context extends ProcessingContext> extends AbstractLoggeable 
implements FraSCAtiInNuxeoServiceBaseItf
{
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * The required CompositeManager
     */
    @Reference(name = "composite-manager")
    protected CompositeManager compositeManager;
    
    /**
     * The required ClassLoaderManager
     */
    @Reference(name = "classloader-manager")
    protected ClassLoaderManager classLoaderManager;

    /**
     * Map of components
     */
    protected Map<String,Component> componentMap;
 
    // -------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------
    
    /**
     * Create a new <Context> ProcessingContext 
     * 
     * @param classloader
     *          ClassLoader used to instantiate a new ProcessingContext
     */
    protected abstract Context newProcessingContext(ClassLoader classloader);
    
    /**
     * Constructor
     */
    protected AbstractFraSCAtiInNuxeoService()
    {
        componentMap = new HashMap<String,Component>();
    }
    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#processContribution(java.
     *      lang.String, int, java.net.URL[])
     */
    public String[] processContribution(String contribution) 
        throws FraSCAtiInNuxeoServiceException
    {  
        return processContribution(contribution, newProcessingContext(
                classLoaderManager.getClassLoader()));
    }

    /**
     * @param contribution
     * @param context
     * @return
     * @throws FraSCAtiInNuxeoServiceException
     */
    protected String[] processContribution(String contribution, Context context) 
        throws FraSCAtiInNuxeoServiceException
    {  
        try
        {
            Component[] components = compositeManager.processContribution(contribution, context);
            String[] componentNames = null;
            if(components != null)
            {   
                componentNames = new String[components.length];
                int index = 0;
                for(Component component : components)
                {
                    String componentName = Fractal.getNameController(component).getFcName();
                    componentMap.put(componentName, component);
                    componentNames[index++] = componentName;
                }
            }else
            {
                componentNames = new String[0];
            }
            return componentNames;
            
        } catch(Exception e)
        {
            throw new FraSCAtiInNuxeoServiceException("Enable to process the '"
                    + contribution + "' composite",e);
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#processComposite(java.lang
     *      .String, int, java.net.URL[])
     */
    public String processComposite(String composite, URL... urls) 
        throws FraSCAtiInNuxeoServiceException
    {
        
        FrascatiClassLoader compositeClassLoader = new FrascatiClassLoader(
                (urls!=null?urls:new URL[0]), classLoaderManager.getClassLoader());
        
        Context context = newProcessingContext(compositeClassLoader);
        return processComposite(composite,context);
    }
  
    /**
     * @param composite
     * @param context
     * @return
     * @throws FraSCAtiInNuxeoServiceException
     */
    public String processComposite(String composite, Context context) 
        throws FraSCAtiInNuxeoServiceException
    {
        try
        {   
            Component component = compositeManager.processComposite(new QName(composite),context);
            String componentName = null;
            if(component != null)
            {
                componentName = Fractal.getNameController(component).getFcName();
                componentMap.put(componentName, component);
            }
            return componentName;
            
        }  catch(Exception e)
        {
            throw new FraSCAtiInNuxeoServiceException("Unable to process the '"
                    + composite + "' composite",e);
        }
    }
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#state(java.lang.String)
     */
    public String state(String compositeName)
    {
        Component component = componentMap.get(compositeName);
        if (component != null)
        {
            try
            {
               return Fractal.getLifeCycleController(component).getFcState();
                
            } catch (NoSuchInterfaceException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }
        return null;
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#start(java.lang.String)
     */
    public void start(String compositeName)
    {
        Component component = componentMap.get(compositeName);
        if (component != null)
        {
            try
            {                
                LifeCycleController lcController = Fractal.getLifeCycleController(
                        component);
                if (!LifeCycleController.STARTED.equals(lcController
                        .getFcState()))
                {
                    lcController.startFc();
                }
            } catch (NoSuchInterfaceException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IllegalLifeCycleException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#stop(java.lang.String)
     */
    public void stop(String compositeName)
    {
        Component component = componentMap.get(compositeName);
        if (component != null)
        {
            try
            {
                LifeCycleController lcController = Fractal.getLifeCycleController(
                        component);
                if (!LifeCycleController.STOPPED.equals(lcController
                        .getFcState()))
                {
                    lcController.stopFc();
                }
            } catch (NoSuchInterfaceException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
                
            } catch (IllegalLifeCycleException e)
            {
                log.log(Level.WARNING,e.getMessage(),e);
            }
        }

    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#remove(java.lang.String)
     */
    public void remove(String compositeName)
            throws FraSCAtiInNuxeoServiceException
    {
        stop(compositeName);
        try
        {
            compositeManager.removeComposite(compositeName);
            
        } catch (ManagerException e)
        {
            throw new FraSCAtiInNuxeoServiceException("Enable to remove the '" 
            + compositeName + "' component",e); 
        }

    }

    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.api.FraSCAtiNuxeoServiceItf#getService(java.lang.String,
     *      java.lang.String, java.lang.Class)
     */
    public <T> T getService(String compositeName,
            String serviceName, Class<T> serviceClass)
            throws FraSCAtiInNuxeoServiceException
    {
        Component component = componentMap.get(compositeName);
        if(component != null)
        {
            try
            {
                return (T) component.getFcInterface(serviceName);
                
            } catch (NoSuchInterfaceException e)
            {
                throw new FraSCAtiInNuxeoServiceException("Unable to retrieve the '" 
                        + serviceName + "' service in the component '" + compositeName +
                        "'",e); 
            }
        }
        return null;
    }

}
