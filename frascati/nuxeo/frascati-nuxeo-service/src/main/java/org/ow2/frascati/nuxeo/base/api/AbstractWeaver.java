/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.base.api;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.julia.ComponentInterface;
import org.ow2.frascati.starter.api.AbstractInitializable;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.util.reference.ServiceReferenceUtil;

/**
 * Abstract implementation of the WeaverItf interface
 */
public abstract class AbstractWeaver extends AbstractInitializable
{
    
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------
    
    /**
     * Map of ServiceReferences
     */
    protected Map<String,ServiceReference<?>> serviceReferenceMap;

    /**
     * Map of Interfaces
     */
    protected Map<String,String> interfaceMap;
    
    /**
     * Map of IntentHandlers
     */
    protected Map<String,IntentHandler> intentHandlerMap;

    /**
     * Map of methods
     */
    protected Map<String,Method[]> methodMap;
    
    // -------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------
    
    /**
     * Return the Interface class used to define IntentHandler and
     * ServiceReference to weave
     */
    protected abstract Class<?> getNextInterface();
    
    /**
     * Return the array of methods associated to the Interface's name
     * for the weaving process
     * 
     * @param interfaceName
     *          the Interface's name
     */
    protected Method[] getMethods(String interfaceName)
    {
        return methodMap.get(interfaceName);
    }

    /**
     * Return the IntentHandler associated to the Interface's name
     * for the weaving process
     * 
     * @param interfaceName
     *          the Interface's name
     */
    protected IntentHandler getIntentHandler(String interfaceName)
    {
        return intentHandlerMap.get(interfaceName);
    }

    /**
     * Return the IntentHandler associated to the Interface's name
     * for the weaving process
     * 
     * @param interfaceName
     *          the Interface's name
     */
    protected String getFractalInterface(String interfaceName)
    {
        return interfaceMap.get(interfaceName);
    }
    
    /**
     * Return the ServiceReference associated to the Interface's name
     * for the weaving process
     * 
     * @param interfaceName
     *          the Interface's name
     */
    protected ServiceReference<?> getServiceReference(String interfaceName)
    {
        return serviceReferenceMap.get(interfaceName);
    }
    
    /**
     * Return the TinifComponentInterceptor for the Component targeted by the ServiceReference
     * passed on as a parameter and the Interface's name also passed on as a parameter
     * 
     * @param serviceReference
     *          the ServiceReference
     * @param tinfiComponentInterceptorInterface
     *          the Interface's name
     */
    private TinfiComponentInterceptor<?> getInterceptor(ServiceReference<?> serviceReference,
            Class<?> tinfiComponentInterceptorInterface)
    {
        try
        {
            if(!tinfiComponentInterceptorInterface.isAssignableFrom(serviceReference.getClass()))
            {
                Component rootComponent =  ServiceReferenceUtil.getRootComponent(serviceReference);
                ComponentInterface componentInterface = (ComponentInterface) 
                    rootComponent.getFcInterface(getFractalInterface(
                            tinfiComponentInterceptorInterface.getCanonicalName()));
                
                return (TinfiComponentInterceptor<?>) componentInterface.getFcItfImpl();
            
            } else
            {
                return ServiceReferenceUtil.getInterceptor(serviceReference);
            }            
        } catch (NoSuchInterfaceException e)
        {
           log.log(Level.WARNING,e.getMessage(),e);
        }
        return null;
    }
    
    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------
    
    /**
     * Constructor
     * Initialize maps
     */
    public AbstractWeaver()
    {
        serviceReferenceMap = new HashMap<String,ServiceReference<?>>();
        interfaceMap = new HashMap<String,String>();
        intentHandlerMap = new HashMap<String,IntentHandler>();
        methodMap = new HashMap<String,Method[]>();
    }   
    
    /**
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.base.api.WeaverItf#weave()
     */
    public void weave()
    {

        Class<?> interfaceClass = getNextInterface();
        
        while(interfaceClass != null)
        {
            String interfaceName = interfaceClass.getCanonicalName();
            
            IntentHandler intentHandler = getIntentHandler(interfaceName);            
            ServiceReference<?> serviceReference = getServiceReference(interfaceName);
            
            if(serviceReference != null && intentHandler != null)
            {
                TinfiComponentInterceptor<?> interceptor = getInterceptor(
                    serviceReference,interfaceClass);
            
                Method[] methods = getMethods(interfaceName);
                
                if(methods != null && methods.length > 0)
                {
                    for(Method method : methods)
                    {
                        try
                        {
                            interceptor.addIntentHandler(intentHandler,method);
                            
                        } catch (NoSuchMethodException e)
                        {
                            log.log(Level.WARNING,e.getMessage(),e);
                        }
                    }                
                } else
                {
                    interceptor.addIntentHandler(intentHandler);
                }
            }
            interfaceClass = getNextInterface();
        }
    }
    
    
}
