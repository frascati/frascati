/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.nuxeo.base;

import java.util.logging.Level;

import org.ow2.frascati.nuxeo.base.api.AbstractWeaver;


/**
 * Base implementation of an AbstractWeaver
 */
public class FraSCAtiInNuxeoBaseInitializer extends AbstractWeaver
{

    // -------------------------------------------------------------------------
    // Internal state.
    // -------------------------------------------------------------------------

    // -------------------------------------------------------------------------
    // Internal methods.
    // -------------------------------------------------------------------------

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.nuxeo.base.api.AbstractWeaver#getNextInterface()
     */
    @Override
    protected Class<?> getNextInterface()
    {
        return null;
    }
    
    // -------------------------------------------------------------------------
    // Public methods.
    // -------------------------------------------------------------------------

    /** 
     * {@inheritDoc}
     * 
     * @see org.ow2.frascati.starter.api.InitializableItf#
     * initialize()
     */
    public void doInitialize()
    {   
       if(log.isLoggable(Level.INFO))
       {
           log.log(Level.INFO,"doInitialize FraSCAtiInNuxeoBaseInitializer") ;
       }          
       super.weave();
    }
}
