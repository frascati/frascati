/**
 * OW2 FraSCAti Nuxeo
 * Copyright (c) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.nuxeo.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.frascati.isolated.FraSCAtiIsolated;
import org.ow2.frascati.nuxeo.exception.FraSCAtiInNuxeoServiceException;
import org.ow2.frascati.nuxeo.api.FraSCAtiInNuxeoServiceBaseItf;

/**
 * AbstractFraSCAtiInNuxeoService Test Case
 */
public class TestFraSCAtiInNuxeoServiceBase
{

    private final String resourcePath = "target/test-classes/";
    private final String pojoPath = resourcePath
            + "helloworld-pojo.jar";

    protected static final Logger log = Logger
            .getLogger(TestFraSCAtiInNuxeoServiceBase.class.getCanonicalName());

    private final String STARTED = "STARTED";
    private final String STOPPED = "STOPPED";

    /**
     * the FraSCAtiIsolated instance used for tests
     */
    FraSCAtiIsolated isolatedFraSCAti;

    /**
     * the FraSCAtiNuxeoServiceItf instance to test
     */
    FraSCAtiInNuxeoServiceBaseItf frascatiService = null;
    
    String fcomponent;

    @Before
    public void setUp() throws Exception
    {
        File frascatiRootDir = new File("target/test-classes/frascati").getAbsoluteFile();
        frascatiRootDir.mkdirs();
        
        // The parent of the FraSCAti's ClassLoader is the same as the current thread ClassLoader
        isolatedFraSCAti = new FraSCAtiIsolated(frascatiRootDir, Thread.currentThread().getContextClassLoader());

        assertNotNull(isolatedFraSCAti);
        
        frascatiService = isolatedFraSCAti.getService(
                FraSCAtiInNuxeoServiceBaseItf.class,
                "frascati-nuxeo-service",
                "org.ow2.frascati.FraSCAti/frascati-nuxeo");

        assertNotNull(frascatiService);

        File scaFile = new File(pojoPath);
        scaFile = scaFile.getAbsoluteFile();

        fcomponent = frascatiService.processComposite(
                "helloworld-pojo.composite", scaFile.toURI().toURL());
    }

    @After
    public void tearDown() throws Exception
    {
        frascatiService.remove("helloworld-pojo");
        isolatedFraSCAti.stop();
        frascatiService = null;
        fcomponent = null;
    }

    /**
     * Test if an existing service can be retrieved
     * 
     * @throws NuxeoFraSCAtiException
     */
    @Test
    public void testGetRunnableServiceAndExecuteIt()
            throws FraSCAtiInNuxeoServiceException
    {
        Runnable r = null;
        try
        {
            r = (Runnable) frascatiService.getService(fcomponent, "r",
                    Runnable.class);

        } catch (FraSCAtiInNuxeoServiceException e)
        {
            e.printStackTrace();
        }
        assertNotNull(r);
        r.run();
    }

    /**
     * Test NuxeoFraSCAtiException is thrown if a service is unknown
     * 
     * @throws NuxeoFraSCAtiException
     */
    @Test(expected = FraSCAtiInNuxeoServiceException.class)
    public void testThrowExceptionIfGetUnexistingService()
            throws FraSCAtiInNuxeoServiceException
    {
        frascatiService.getService(fcomponent, "unknown", Runnable.class);
    }

    @Test
    public void testSCACompositeLifeCycle()
    {
        assertTrue(STARTED.equals(frascatiService.state(fcomponent)));
        frascatiService.stop(fcomponent);
        assertTrue(STOPPED.equals(frascatiService.state(fcomponent)));
        frascatiService.start(fcomponent);
        assertTrue(STARTED.equals(frascatiService.state(fcomponent)));
    }
}