/**
 * 
 */
package org.ow2.frascati.nuxeo.exporting.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

/**
 * NuxeoBinder Service Implementation
 */
@Service(NuxeoExporterItf.class)
@Scope("COMPOSITE")
public class NuxeoExporter implements NuxeoExporterItf
{
    private static final Logger log = Logger.getLogger(NuxeoExporter.class.getCanonicalName());
    
    private final String MESSAGE = "NuxeoExporter exported service";
        
    @Init
    public void init()
    {
        log.log(Level.INFO,"NuxeoExporter initialization");
    }
    
    public String getExportedMessage()
    {
        return MESSAGE;
    }
}
