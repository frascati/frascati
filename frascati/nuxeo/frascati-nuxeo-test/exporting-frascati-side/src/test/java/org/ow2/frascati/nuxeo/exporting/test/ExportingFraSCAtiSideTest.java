/**
 * OW2 FraSCAti in Nuxeo : Binding FraSCAti side
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.nuxeo.binding.test;

import static org.junit.Assert.assertNotNull;
import java.net.URL;
import java.util.logging.Logger;
import java.io.InputStream;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
/**
 * BindingFraSCAtiSideTest Test Case
 */
public class ExportingFraSCAtiSideTest
{
    
    private Logger log = Logger.getLogger(ExportingFraSCAtiSideTest.class.getCanonicalName());
    
    /**
     * If nuxeo-service reference 
     */
    @Test
    public void testExportingFraSCAtiSide() throws Exception
    {
        System.setProperty("org.ow2.frascati.binding.uri.base","http://localhost:8765");
        FraSCAti frascati = FraSCAti.newFraSCAti();
        
        Component component = frascati.getComposite("NuxeoExporter",frascati.getClassLoader());
        URL urlExporter = new URL("http://localhost:8765/NuxeoExporter?wsdl");
        InputStream stream = urlExporter.openStream();
        assertNotNull(stream);
    }
    
}
