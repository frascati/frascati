/**
 * OW2 FraSCAti in Nuxeo : Binding Nuxeo side
 * Copyright (c) 2011 - 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 * 
 * Contributors :
 *
 */
package org.ow2.frascati.nuxeo.binding.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * BindingNuxeoSideTest Test Case
 */
public class BindingNuxeoSideTest
{
    private static final String MESSAGE = "ServiceToBind";
    private static final String HELLO_MESSAGE = "Hello "+ MESSAGE;    
    
    /**
     * Test that the ServiceToBind object return the expected message
     * 
     * @throws Exception
     */
    @Test
    public void testBindingNuxeoSide() throws Exception
    {
        ServiceToBindItf serviceToBind = new ServiceToBind(); 
        assertEquals(HELLO_MESSAGE,serviceToBind.sayHello(MESSAGE));
    }
    
    
}
