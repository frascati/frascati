/**
 * 
 */
package org.ow2.frascati.nuxeo.binding.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

/**
 * NuxeoBinder Service Implementation
 */
@Service(NuxeoBinderItf.class)
@Scope("COMPOSITE")
public class NuxeoBinder implements NuxeoBinderItf
{
    private static final Logger log = Logger.getLogger(NuxeoBinder.class.getCanonicalName());
    
    @Reference(name="nuxeo-service")
    ServiceToBindItf serviceToBindAndExport;
    
    @Init
    public void init()
    {
        log.log(Level.INFO,"NuxeoBinder initialization");
    }
    
    public boolean binded()
    {
        boolean result = (serviceToBindAndExport!=null);
        if(result)
        {
            log.log(Level.INFO,":: " + serviceToBindAndExport);
            log.log(Level.INFO,serviceToBindAndExport.sayHello("From Binded Service"));
            
        } else
        {
            log.log(Level.INFO,"ServiceToBindItf not found");
        }
        return result;
    }
}
