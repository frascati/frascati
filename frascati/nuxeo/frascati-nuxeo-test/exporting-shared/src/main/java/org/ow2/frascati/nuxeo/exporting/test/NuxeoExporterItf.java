/**
 * 
 */
package org.ow2.frascati.nuxeo.exporting.test;


/**
 * NuxeoExporter Service
 */
public interface NuxeoExporterItf
{
   String getExportedMessage();
}
