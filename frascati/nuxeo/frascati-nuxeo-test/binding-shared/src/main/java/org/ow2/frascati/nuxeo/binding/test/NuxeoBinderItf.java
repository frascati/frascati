/**
 * 
 */
package org.ow2.frascati.nuxeo.binding.test;

/**
 * NuxeoBinder Service
 */
public interface NuxeoBinderItf
{
    boolean binded();
}
