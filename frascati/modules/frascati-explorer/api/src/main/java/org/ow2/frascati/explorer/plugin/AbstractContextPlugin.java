/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.explorer.plugin;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Abstract class for implementing OW2 FraSCAti Explorer Context plugins.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public abstract class AbstractContextPlugin<T>
              extends AbstractPlugin
           implements Context
{
  // ==================================================================
  // Internal state.
  // ==================================================================
  
  // ==================================================================
  // Internal methods.
  // ==================================================================

  /**
   * Create a new Entry instance.
   */
  protected final Entry newEntry(String label, Object value)
  {
    return new DefaultEntry(label, value);
  }

  /**
   * Add the entries associated to the selected instance.
   */
  protected abstract void addEntries(T selected, List<Entry> entries) throws Exception;

  // ==================================================================
  // Public methods for interface Context.
  // ==================================================================

  /**
   * @see org.objectweb.util.explorer.api.Context#getEntries(Object)
   */
  @SuppressWarnings("uncheck")
  public final Entry[] getEntries(Object object)
  {
    T selected = (T)object;
    List<Entry> entries = new ArrayList<Entry>();
    try {
      addEntries(selected, entries);
    } catch(Exception exc) {
      exc.printStackTrace();
    }
    return entries.toArray(new Entry[entries.size()]);
  }
}
