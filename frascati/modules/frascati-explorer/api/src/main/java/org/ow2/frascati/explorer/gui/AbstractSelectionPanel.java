/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.explorer.gui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;

import org.objectweb.util.explorer.api.Panel;
import org.objectweb.util.explorer.api.TreeView;

import org.ow2.frascati.explorer.plugin.RefreshExplorerTreeThread;

/**
 * Abstract class for FraSCAti Explorer {@link Panel} keeping the current selected object.
 *
 * @author Philippe Merle - INRIA
 * @version 1.3
 */
@SuppressWarnings("serial")
public abstract class AbstractSelectionPanel<SelectionType>
              extends JPanel
           implements Panel
{
  /**
   * The selected instance.
   */
  protected SelectionType selected;

  // --------------------------------------------------------------------------
  // Constructor.
  // --------------------------------------------------------------------------

  /**
   * Default constructor creates an empty white panel.
   */
  public AbstractSelectionPanel()
  {
    super();
    setBackground(Color.white);
  }

  // --------------------------------------------------------------------------
  // Methods for subclasses.
  // --------------------------------------------------------------------------

  /**
   * Get the current selected object.
   *
   * @return the current selected object.
   */
  public final SelectionType getSelection()
  {
    return selected;
  }

  public final void runAsynchronously(Runnable runnable)
  {
    // Creates a new Thread.
    Thread thread = new RefreshExplorerTreeThread(runnable);
    // Starts the Thread.
    thread.start();
  }

  // --------------------------------------------------------------------------
  // Implementation of the Panel interface.
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.Panel#getPanel()
   */
  public final Object getPanel()
  {
    if(selected instanceof Interface) {
      try {
        LifeCycleController lcc = Fractal.getLifeCycleController(((Interface)selected).getFcItfOwner());
        if(lcc.getFcState().equals(LifeCycleController.STOPPED)) {
      	  JPanel panel = new JPanel();
          panel.setBackground(Color.white);
          panel.add(new JLabel("Can't display this panel because the owner component is stopped!"));
          return panel;
        }
      } catch(NoSuchInterfaceException nsie) {
        // nothing to do when the owner component has no LifeCycleController.
      }
    }
    // Return this panel if:
    // * selected is not a Fractal Interface, or
    // * its enclosing component has no lifecycle controller, or
    // * the lifecycle state is started.
    return this;
  }

  /**
   * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
   */
  @SuppressWarnings("unchecked")
  public void selected(TreeView treeView)
  {
	selected = (SelectionType) treeView.getSelectedObject();
  }

  /**
   * @see org.objectweb.util.explorer.api.Panel#unselected(org.objectweb.util.explorer.api.TreeView)
   */
  public void unselected(TreeView treeView)
  {
    selected = null;
  }
}
