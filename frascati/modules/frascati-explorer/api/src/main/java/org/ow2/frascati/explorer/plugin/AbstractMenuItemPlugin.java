/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.explorer.plugin;

import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;

import org.ow2.frascati.explorer.api.CursorController;

/**
 * Abstract class implementing {@link MenuItem} plugins.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public abstract class AbstractMenuItemPlugin<T>
              extends AbstractPlugin
           implements MenuItem
{
  // --------------------------------------------------------------------------
  // Implementation of the MenuItem interface
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
   */
  @SuppressWarnings("unchecked")
  public final void actionPerformed(final MenuItemTreeView e) throws Exception
  {
    // Get the FraSCAti Explorer cursor controller service.
    CursorController cursorController = getFraSCAtiExplorerService(CursorController.class);
    try {
      // Set the wait cursor.
      cursorController.setWaitCursor();
      // Obtain the selected object.
      T selected = (T) e.getSelectedObject();
      // Execute the action on the selected object.
      execute(selected, e);
    } finally {
      // Set the default cursor.
      cursorController.setDefaultCursor();
    }
  }

  // --------------------------------------------------------------------------
  // Internal methods overriden by concrete subclasses.
  // --------------------------------------------------------------------------

  /**
   * Execute the action on the selected object.
   * Could be overriden in sub-classes.
   */
  protected void execute(T selected, final MenuItemTreeView e) throws Exception
  {
    execute(selected);
  }
  
  /**
   * Execute the action on the selected object.
   * Should be overriden in sub-classes.
   */
  protected void execute(T selected) throws Exception
  {
  }

}
