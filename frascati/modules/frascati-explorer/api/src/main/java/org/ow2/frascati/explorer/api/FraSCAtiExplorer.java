/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.explorer.api;

/**
 * Is the main interface of the OW2 FraSCAti Explorer.
 *
 * This interface allows plugins to get the FraSCAti Explorer singleton instance and then to get its services.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public interface FraSCAtiExplorer
{
  /**
   * The FraSCAti Explorer singleton instance attached to the current thread.
   *
   * Call FraSCAtiExplorer.SINGLETON.get() to obtain the FraSCAti Explorer singleton instance.
   */
// Previous implementation does not work when Java Swing is initialized by FraSCAti Explorer.
//	InheritableThreadLocal<FraSCAtiExplorer> SINGLETON = new InheritableThreadLocal<FraSCAtiExplorer>();
  FraSCAtiExplorerSingleton SINGLETON = new FraSCAtiExplorerSingleton();

  /**
   * Get a service provided by this FraSCAti Explorer.
   *
   * @param interfaceType the Java interface type of the service to get.
   */
  <T> T getFraSCAtiExplorerService(Class<T> interfaceType);
}
