/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */
package org.ow2.frascati.explorer.plugin;

import javax.swing.SwingUtilities;

import org.objectweb.util.explorer.api.Tree;

import org.ow2.frascati.explorer.api.FraSCAtiExplorer;

/**
 * {@link Thread} that refreshs FraSCAti Explorer GUI.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */

public class RefreshExplorerTreeThread
     extends Thread
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The constructor.
   */
  public RefreshExplorerTreeThread(final Runnable runnable)
  {
    super(runnable);

    // Configures the classloader of this thread.
    setContextClassLoader(
      FraSCAtiExplorer.SINGLETON.get()
        .getFraSCAtiExplorerService(ClassLoader.class)
    );
  }

  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------

  /**
   * @see java.lang.Runnable#run()
   */
  public final void run()
  {
    // Runs the runnable.
    super.run();

    // Refresh the FraSCAti Explorer GUI.
    refreshFrascatiExplorerGUI();
  }

  /**
   *  Refreshs the FraSCAti Explorer GUI.
   */
  public static void refreshFrascatiExplorerGUI()
  {
    // As refreshAll() is not thread-safe, then synchronizes via Swing.
    try {
      SwingUtilities.invokeAndWait(
        new Runnable() {
          /**
           * @see java.lang.Runnable#run()
           */
          public final void run() {
            FraSCAtiExplorer.SINGLETON.get()
              .getFraSCAtiExplorerService(Tree.class)
              .refreshAll();
          }
        }
      );
    } catch(Exception exception) {
      throw new Error("Could not synchronize via Java Swing", exception);
    }
  }
}
