/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.explorer.action;

import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.TreeView;

import org.ow2.frascati.explorer.plugin.AbstractMenuItemPlugin;

/**
 * Abstract class implementing always enabled {@link MenuItem} plugins.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public abstract class AbstractAlwaysEnabledMenuItem<T>
              extends AbstractMenuItemPlugin<T>
{
  // --------------------------------------------------------------------------
  // Implementation of the MenuItem interface
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
   */
  public int getStatus(final TreeView treeView)
  {
    return MenuItem.ENABLED_STATUS;
  }

}
