/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.explorer.plugin;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Abstract class for implementing OW2 FraSCAti Explorer Table plugins.
 * 
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public abstract class AbstractTablePlugin<T>
              extends AbstractPlugin
           implements Table
{
    //==================================================================
    // Internal state.
    //==================================================================

    // ==================================================================
    // Internal methods.
    // ==================================================================

    /**
     * Create a new Entry instance.
     */
    protected final Entry newEntry(String label, Object value)
    {
      return new DefaultEntry(label, value);
    }

    /**
     * Add the colunm headers.
     */
    protected abstract String[] getHeaders();

    /**
     * Add the rows associated to the selected instance.
     */
    protected abstract void addRows(T selected, List<Object[]> rows);

    //==================================================================
    // Public methods for Table interface.
    //==================================================================

    @SuppressWarnings("uncheck")
    public final String[] getHeaders(TreeView treeView)
    {
      return getHeaders();
    }

    @SuppressWarnings("uncheck")
    public final Object[][] getRows(TreeView treeView)
    {
      T selected = (T) treeView.getSelectedObject();
      List<Object[]> rows = new ArrayList<Object[]>();
      addRows(selected, rows);
      return rows.toArray( new Object[0][0] );
    }
}
