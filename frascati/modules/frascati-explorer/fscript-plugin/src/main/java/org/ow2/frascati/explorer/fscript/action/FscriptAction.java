/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.fscript.action;

import javax.script.Bindings;
import javax.script.ScriptContext;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.explorer.fscript.gui.Console;
import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory;

/**
 * This class provides a way to interact with the FScript engine.
 */
public class FscriptAction 
  implements MenuItem
{
  // --------------------------------------------------------------------------
  // Implementation of the MenuItem interface
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
   */
  public final int getStatus (final TreeView treeView)
  {
    return MenuItem.ENABLED_STATUS;
  }

  /**
   * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
   */
  public final void actionPerformed(final MenuItemTreeView tree) throws Exception
  {
	  FraSCAtiScriptEngineFactory factory =  new FraSCAtiScriptEngineFactory();
	  InvocableScriptEngine engine = FraSCAtiFScript.getSingleton().getScriptEngine();
	  Bindings bindings = engine.createBindings();
	  
	  factory.addDomainToContext( engine.getBindings(ScriptContext.GLOBAL_SCOPE) );
	  factory.updateContext( bindings, "root", (Component) tree.getSelectedObject());
      new Console(bindings).setVisible(true);
  }
}
