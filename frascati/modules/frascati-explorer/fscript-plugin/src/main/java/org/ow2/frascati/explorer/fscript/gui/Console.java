/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.fscript.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Set;
import java.util.StringTokenizer;

import javax.script.Bindings;
import javax.script.ScriptException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.explorer.api.FraSCAtiExplorer;
import org.ow2.frascati.fscript.FraSCAtiFScript;

import de.schlichtherle.io.FileInputStream;
import de.schlichtherle.io.swing.JFileChooser;

/**
 * A GUI console allowing to execute FraSCAtiScript queries / commands.
 * 
 * @author Christophe Demarey.
 */
public class Console
     extends JFrame
{
  private static final String TITLE = "FraSCAti FScript Console";
  private static final String PROMPT = "FraSCAtiFScript> ";
  
  /** The FraSCAti Script engine */
  private InvocableScriptEngine engine = null;

  /** The FraSCAti Script context to use */
  private Bindings ctx = null;
  
  /** Graphical components. */
  private JTextField input = new JTextField(40);
  private JScrollPane outputScrollPane = new JScrollPane();
  private JTextPane output = new JTextPane();
  private JPanel editorPanel = new JPanel();
  private JButton loadButton = new JButton();
  private JButton editorButton = new JButton();
  private JButton executeButton = new JButton();
  private JButton registerButton = new JButton();
  private JEditorPane editorPane = new JEditorPane();
  private JScrollPane editorScrollPane = new JScrollPane();
  /** the output document */
  private StyledDocument doc = output.getStyledDocument();
  /** The {@link JFileChooser} instance used to load scripts. */
  private static JFileChooser fileChooser = null;

  /**
   * Default constructor.
   */
  public Console(Bindings ctx)
  {
      super(TITLE);
      this.ctx = ctx;
      
      // Get the script engine
	  engine = (InvocableScriptEngine) FraSCAtiFScript.getSingleton().getScriptEngine();

      // GUI set up
      addStylesToDocument( output.getStyledDocument() );
      addComponents();
      welcome();
      this.pack();
      this.setVisible(true);
  }
  
  /**
   * Display Welcome information for the FraSCAti Script console.
   */
  protected final void welcome()
  {
      executeCommand("$root");
  }
  
  /**
   * Initialize components and add tem to the GUI.
   */
  protected final void addComponents()
  {
      outputScrollPane.setViewportView(output);
      output.setBackground(new Color(255, 255, 204));
      output.setAutoscrolls(true);
      output.setEditable(false);
      outputScrollPane.setPreferredSize(new Dimension(200,100));
      
      input.addActionListener( new ActionListener() {        
          public final void actionPerformed(ActionEvent e)
          {
              inputActionPerformed(e);
          }
        });
      
      editorButton.setText("Editor");
      editorButton.setName("editorButton");
      editorButton.addActionListener(new ActionListener() {
          public final void actionPerformed(ActionEvent e)
          {
              editorButtonActionPerformed(e);
          }
      });

      editorPane.setName("editorPane");
      editorPane.setPreferredSize(new Dimension(200,100));
      editorScrollPane.setName("editorScrollPane");
      editorScrollPane.setViewportView(editorPane);
      editorScrollPane.setAutoscrolls(true);
      
      loadButton.setText("Load script from filesystem");
      loadButton.setName("LoadButton");
      loadButton.addActionListener(new ActionListener() {
          public final void actionPerformed(ActionEvent e)
          {
              loadButtonActionPerformed(e);
          }
      });
      
      registerButton.setText("Register procedures");
      registerButton.setName("registerButton");
      registerButton.addActionListener(new ActionListener() {
          public final void actionPerformed(ActionEvent e)
          {
              registerButtonActionPerformed(e);
          }
      });
      executeButton.setText("Execute");
      executeButton.setName("executeButton");
      executeButton.addActionListener(new ActionListener() {
          public final void actionPerformed(ActionEvent e)
          {
              executeButtonActionPerformed(e);
          }
      });
      
      // Layout
      getContentPane().setLayout(new GridBagLayout());
      getContentPane().add(outputScrollPane, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  GridBagConstraints.REMAINDER,1,
    		  // weight 
    		  1d,1d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.BOTH,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      getContentPane().add(input, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  1,1,
    		  // weight 
    		  1d,0d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.HORIZONTAL,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      getContentPane().add(editorButton, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  GridBagConstraints.REMAINDER,1,
    		  // weight 
    		  0d,0d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.NONE,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      getContentPane().add(editorPanel, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  GridBagConstraints.REMAINDER,1,
    		  // weight 
    		  1d,1d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.BOTH,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      
      editorPanel.setLayout(new GridBagLayout());
      editorPanel.add(new JScrollPane(editorPane), new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  GridBagConstraints.REMAINDER,1,
    		  // weight 
    		  1d,1d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.BOTH,
    		  // inset
    		  new Insets(0,0,0,0),
    		  // ipad
    		  0,0
    		  ));
      editorPanel.add(loadButton, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  1,1,
    		  // weight 
    		  1d,0d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.NONE,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      editorPanel.add(registerButton, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  1,1,
    		  // weight 
    		  1d,0d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.NONE,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      editorPanel.add(executeButton, new GridBagConstraints(
    		  // location
    		  GridBagConstraints.RELATIVE,GridBagConstraints.RELATIVE,
    		  // size
    		  1,1,
    		  // weight 
    		  1d,0d,
    		  // anchor
    		  GridBagConstraints.CENTER,
    		  // fill
    		  GridBagConstraints.NONE,
    		  // inset
    		  new Insets(5, 5, 5, 5),
    		  // ipad
    		  0,0
    		  ));
      
      editorPanel.setVisible(false);
  }
  
  /**
   * Listener on register button click events.
   * Read editor content and register procedures in Fscript engine. 
   * 
   * @param e - The source event.
   */
  protected final void registerButtonActionPerformed(ActionEvent e)
  {
      try {
          Set<String> procs = null;
          Reader source = new StringReader( editorPane.getText() );
            
          displayMessage("Loading procedures from editor");
          procs = (Set<String>) engine.eval(source);
          for (String proc : procs) {
              displayMessage("+-> Adding procedure '"+ proc +"'");
          }
      } catch (ScriptException ise) {
          displayError(ise);
      }
  }

  /**
   * Listener on execute button click events.
   * Read editor content and execute commands line by line. 
   * 
   * @param e - The source event.
   */
  protected final void executeButtonActionPerformed(ActionEvent e)
  {
    StringTokenizer st = new StringTokenizer( editorPane.getText(), "\n" );
    while (st.hasMoreTokens()) {
        executeCommand( st.nextToken() );
    }
  }

  /**
   * Listener on load button click events.
   * open a file chooser and load input file content into editor.
   * 
   * @param e - The source event.
   */
  protected final void loadButtonActionPerformed(ActionEvent e)
  {
      // At the first call, creates the file chooser.
      if(fileChooser == null) {
        fileChooser = new JFileChooser();
        // Sets to the user's current directory.
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        // Filters SCA files.
        fileChooser.setFileFilter(            
          new FileFilter() {
            /** Whether the given file is accepted by this filter. */
            public final boolean accept(File f)
            {
              String extension = f.getName().substring(f.getName().lastIndexOf('.') + 1);
              return extension.equalsIgnoreCase("fscript") || f.isDirectory();
            }
            /** The description of this filter. */
            public final String getDescription()
            {
              return "Fscript files";
            }
          }
        );
      }
      
      // Open the file chooser to select a file.
      int returnVal = fileChooser.showOpenDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File f = fileChooser.getSelectedFile();
        InputStream is = null;
        try {
            is = new FileInputStream(f);
            BufferedReader reader = new BufferedReader( new InputStreamReader(is) );
            StringBuffer text = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
               text.append(line).append('\n');
            }
            editorPane.setText(text.toString());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
      }
  }

  /**
   * Listener on input text field validation.
   * On input validation, execute the command and display output.
   * 
   * @param e - The source event.
   */
  protected final void inputActionPerformed(ActionEvent e)
  {
      String cmd = input.getText();
      
      if ( cmd.trim().length() > 0 ) {
          executeCommand( input.getText() );
      } else {
          // Display a new line on the output console
          try {
            doc.insertString( doc.getLength(), PROMPT+"\n", doc.getStyle("prompt") );
          } catch (BadLocationException e2) {
              e2.printStackTrace(); // Should not happen
          }
          input.setText("");
      }
  }

  /**
   * Listener on editor button click events.
   * On editor button click, show/hide editor panel.
   * @param evt - The source event.
   */
  protected final void editorButtonActionPerformed(ActionEvent evt)
  {
      editorPanel.setVisible( !editorPanel.isVisible() );
      this.pack();
      this.repaint();
  }
  
  /**
   * Define some basic style for the output JTextPane.
   * @param doc - Define styles on the document doc.
   */
  protected final void addStylesToDocument(StyledDocument doc)
  {
      Style def = StyleContext.getDefaultStyleContext().
                      getStyle(StyleContext.DEFAULT_STYLE);

      Style regular = doc.addStyle("regular", def);
      StyleConstants.setFontFamily(def, "SansSerif");

      Style s = doc.addStyle("italic", regular);
      StyleConstants.setItalic(s, true);

      s = doc.addStyle("bold", regular);
      StyleConstants.setBold(s, true);

      s = doc.addStyle("small", regular);
      StyleConstants.setFontSize(s, 10);

      s = doc.addStyle("prompt", regular);
      StyleConstants.setForeground(s, Color.GREEN);
      
      s = doc.addStyle("command", regular);
      StyleConstants.setForeground(s, Color.BLUE);      
  }
  
  /**
   * Execute a FraSCAti Script command and update the display (input and output components).
   * @param cmd - The command to execute.
   */
  protected final void executeCommand(String command)
  {
	  String cmd = command;
      // Display the command on the output console
      try {
        doc.insertString( doc.getLength(), PROMPT, doc.getStyle("prompt") );
        doc.insertString( doc.getLength(), cmd +"\n", doc.getStyle("command") );
      } catch (BadLocationException e) {
          e.printStackTrace(); // Should not happen
      }
      input.setText("");
      
      // Display the command result
      if ( cmd.startsWith(":load ") ) {
          ClassLoaderManager classLoaderManager = null;
          ClassLoader cl = null;
          InputStream in = null;
          Reader source = null;
          
          classLoaderManager = FraSCAtiExplorer.SINGLETON.get().getFraSCAtiExplorerService(ClassLoaderManager.class);
          if ( classLoaderManager != null ) {
              cl = classLoaderManager.getClassLoader();
              cmd = cmd.substring(6);
              in = cl.getResourceAsStream(cmd);
              source = new InputStreamReader(in);
              try {
                Set<String> procs = null;
                  
                displayMessage("Loading procedures from "+ cmd);
                procs = (Set<String>) engine.eval(source);
                for (String proc : procs) {
                    displayMessage("+-> Adding procedure '"+ proc +"'");
                }
              } catch (ScriptException e) {
                displayError(e);
              }
          } else {
              displayError("Cannot found the domain-config interface!");
          }
      } else {
    	  try {
            Object obj = engine.eval(cmd, ctx);
            if ( obj != null ) {
              displayMessage( obj.toString() );
            }
          } catch (ScriptException e2) {
            displayError(e2);
          }
      }      
     // outputScrollPane.s
  }
  
  private void displayMessage(String message)
  {
      try {
        doc.insertString( doc.getLength(), 
                          message+"\n",
                          doc.getStyle("regular") );
      } catch (BadLocationException e1) {
          e1.printStackTrace();  // Should not happen
      }
  }
  
  private void displayError(String message)
  {
      try {
        doc.insertString( doc.getLength(), 
                "Error while executing FraSCAti Script command\n",
                doc.getStyle("bold") );
        doc.insertString( doc.getLength(), 
                message+"\n",
                doc.getStyle("small") );
      } catch (BadLocationException e1) {
          e1.printStackTrace();  // Should not happen
      }
  }
  
  private void displayError(Exception e)
  {
      try {
        doc.insertString( doc.getLength(), 
                "Error while executing FraSCAti Script command\n",
                doc.getStyle("bold") );
        doc.insertString( doc.getLength(), 
                e.getMessage()+"\n",
                doc.getStyle("small") );
      } catch (BadLocationException e1) {
          e1.printStackTrace();  // Should not happen
      }
      e.printStackTrace();      
  }
}
