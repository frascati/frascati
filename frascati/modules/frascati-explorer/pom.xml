<?xml version="1.0"?>
<!--
  * OW2 FraSCAti Explorer
  * Copyright (c) 2008-2012 Inria, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  * USA
  *
  * Contact: frascati@ow2.org
  *
  * Author: Christophe Demarey
  *
  * Contributor(s): Philippe Merle
  *
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <!-- ===================== -->
  <!-- General Information   -->
  <!-- ===================== -->

  <groupId>org.ow2.frascati</groupId>
  <artifactId>frascati-explorer</artifactId>
  <packaging>pom</packaging>

  <name>OW2 FraSCAti Explorer</name>
  <inceptionYear>2008</inceptionYear>

  <parent>
    <groupId>org.ow2.frascati</groupId>
    <artifactId>frascati-modules</artifactId>
    <version>1.6-SNAPSHOT</version>
  </parent>
  
  <!-- ============= -->
  <!-- Properties    -->
  <!-- ============= -->

  <properties>
    <fractal-explorer.version>1.1.4</fractal-explorer.version>
    <beanshell.version>2.0b4</beanshell.version>
    <truezip.version>6.6</truezip.version>
    <org.jdesktop.version>1.0.3</org.jdesktop.version>
    <org.ow2.frascati.class>org.ow2.frascati.FraSCAti</org.ow2.frascati.class>
    <org.ow2.frascati.bootstrap>org.ow2.frascati.bootstrap.FraSCAtiJDT</org.ow2.frascati.bootstrap>
    <org.ow2.frascati.composite>org.ow2.frascati.FraSCAti</org.ow2.frascati.composite>
  </properties>

  <!-- ============= -->
  <!-- Dependencies  -->
  <!-- ============= -->

  <dependencyManagement>
    <dependencies>
      <!-- Fractal Explorer -->
      <dependency>
        <groupId>org.objectweb.fractal.fractalexplorer</groupId>
        <artifactId>fractal-explorer</artifactId>
        <version>${fractal-explorer.version}</version>
        <exclusions>
            <!-- Use fractal-util >= 1.1.2 
          (FractalExplorer relies on fractal-adl 2.2 that relies on fractal-util 1.0) -->    
          <exclusion>
            <groupId>org.objectweb.fractal</groupId>
            <artifactId>fractal-util</artifactId>
          </exclusion>
          <!-- Use a fractal-adl version >= 2.3.1 (Get the possibility to name arguments) -->
          <exclusion>
            <groupId>org.objectweb.fractal.fractaladl</groupId>
            <artifactId>fractal-adl</artifactId>
          </exclusion>
          <!-- Don't use a too old version of xerces (API incompatibility) -->    
          <exclusion>
            <groupId>xerces</groupId>
            <artifactId>xercesImpl</artifactId>
          </exclusion>
          <!-- Use an asm version >= 3.0 --> 
          <exclusion>
            <groupId>asm</groupId>
            <artifactId>asm</artifactId>
          </exclusion>
          <!-- Fractal RMI unused by FraSCAti. --> 
          <exclusion>
            <groupId>org.objectweb.fractal.fractalrmi</groupId>
            <artifactId>fractal-rmi</artifactId>
          </exclusion>
          <!-- Fractal ADL Dumper unused by FraSCAti. --> 
          <exclusion>
            <groupId>org.objectweb.fractal.fractaladl.dumper</groupId>
            <artifactId>dumper</artifactId>
          </exclusion>
          <!-- Bugged version of ow_trace -->
          <exclusion>
            <groupId>org.objectweb.util.misc</groupId>
            <artifactId>ow-trace</artifactId>
          </exclusion>
          <!-- Tinfi uses a more recent Fraclet version. -->
          <exclusion>
            <groupId>org.objectweb.fractal.fraclet.java</groupId>
            <artifactId>fraclet-annotations</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.beanshell</groupId>
        <artifactId>bsh</artifactId>
        <version>${beanshell.version}</version>
      </dependency>

      <!-- Truezip -->
      <dependency>
        <groupId>de.schlichtherle.io</groupId>
        <artifactId>truezip</artifactId>
        <version>${truezip.version}</version>
      </dependency>

      <!-- FraSCAti FScript -->
      <dependency>
        <groupId>org.ow2.frascati</groupId>
        <artifactId>frascati-fscript-core</artifactId>
        <version>${project.version}</version>
        <exclusions>
          <exclusion>
            <groupId>org.objectweb.fractal.fscript</groupId>
            <artifactId>fscript-console</artifactId>
          </exclusion>
          <exclusion>
            <groupId>org.ow2.frascati.examples</groupId>
            <artifactId>helloworld-rmi-server</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

    </dependencies> 
  </dependencyManagement>

  <dependencies> 
  </dependencies> 

  <!-- ========= -->
  <!-- Profiles  -->
  <!-- ========= -->

  <!-- To execute FraSCAti Explorer type 'mvn -Pexplorer'. -->
  <profiles>
    <profile>
      <id>explorer</id>
      <build>
        <defaultGoal>exec:java</defaultGoal>          
        <plugins>
          <!-- Run the Explorer -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <!-- Use version 1.1.1 instead of 1.2 to be sure that provided artifacts are added to the classpath. -->
            <version>1.1.1</version>
            <configuration>
              <mainClass>org.ow2.frascati.explorer.FrascatiExplorerLauncher</mainClass>
              <systemProperties>
                <systemProperty>
                  <key>org.ow2.frascati.class</key>
                  <value>${org.ow2.frascati.class}</value>
                </systemProperty>
                <systemProperty>
                  <key>org.ow2.frascati.bootstrap</key>
                  <value>${org.ow2.frascati.bootstrap}</value>
                </systemProperty>
                <systemProperty>
                  <key>org.ow2.frascati.composite</key>
                  <value>${org.ow2.frascati.composite}</value>
                </systemProperty>
              </systemProperties>
            </configuration>
          </plugin>
        </plugins>
      </build>
     
      <dependencies> 
        <dependency>
          <groupId>org.ow2.frascati</groupId>
          <artifactId>frascati-binding-rest</artifactId>
          <version>${project.version}</version>
        </dependency>

        <dependency>
          <groupId>org.ow2.frascati</groupId>
          <artifactId>frascati-binding-ws</artifactId>
          <version>${project.version}</version>
        </dependency>
      </dependencies>
    </profile>

    <profile>
      <id>explorer-introspection</id>
      <build>
        <defaultGoal>exec:java</defaultGoal>          
        <plugins>
          <!-- Run the Explorer -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <!-- Use version 1.1.1 instead of 1.2 to be sure that provided artifacts are added to the classpath. -->
            <version>1.1.1</version>
            <configuration>
              <mainClass>org.ow2.frascati.explorer.FrascatiExplorerLauncher</mainClass>
              <systemProperties>
                <systemProperty>
                  <key>org.ow2.frascati.class</key>
                  <value>${org.ow2.frascati.class}</value>
                </systemProperty>
                <systemProperty>
                  <key>org.ow2.frascati.bootstrap</key>
                  <value>org.ow2.frascati.bootstrap.FraSCAtiJDTFractalRest</value>
                </systemProperty>
                <systemProperty>
                  <key>org.ow2.frascati.composite</key>
                  <value>${org.ow2.frascati.composite}</value>
                </systemProperty>
              </systemProperties>
            </configuration>
          </plugin>
        </plugins>
      </build>
     
      <dependencies> 
        <dependency>
          <groupId>org.ow2.frascati</groupId>
          <artifactId>frascati-binding-rest</artifactId>
          <version>${project.version}</version>
        </dependency>

        <dependency>
          <groupId>org.ow2.frascati</groupId>
          <artifactId>frascati-binding-ws</artifactId>
          <version>${project.version}</version>
        </dependency>
        
        <dependency>
          <groupId>org.ow2.frascati</groupId>
          <artifactId>frascati-introspection</artifactId>
          <version>${project.version}</version>
        </dependency>
      </dependencies>
    </profile>

    <profile>
      <id>run</id>
      <build>
        <defaultGoal>exec:exec</defaultGoal>
        <plugins>
          <!-- Run a FraSCAti Domain with predefined composites -->
          <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <configuration>
              <executable>java</executable>
              <arguments>
                <argument>-cp</argument>
                <classpath />
                <argument>org.ow2.frascati.explorer.FrascatiExplorerLauncher</argument>
                <!-- Load composites in the domain by default with following args: -->
                <!-- <argument>composite-name</argument> -->
              </arguments>
            </configuration>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>

  <!-- ======== -->
  <!-- Modules  -->
  <!-- ======== -->
  <modules>
    <module>api</module>
    <module>core</module>
    <module>fscript-plugin</module>
  </modules>

</project>
