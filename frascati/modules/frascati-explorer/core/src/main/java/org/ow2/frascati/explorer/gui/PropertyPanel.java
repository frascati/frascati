/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.processor.ScaPropertyTypeJavaProcessor;
import org.ow2.frascati.explorer.api.FraSCAtiExplorer;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

public class PropertyPanel 
     extends AbstractSelectionPanel<SCAPropertyController>
{
  // ==========================================================================
  // Internal state
  // ==========================================================================
   
  /** The logger */
  private static Logger logger = Logger.getLogger("org.ow2.frascati.explorer.gui.PropertyPanel");
  
  private javax.swing.JLabel propNameLabel = new javax.swing.JLabel();
  private javax.swing.JLabel propTypeLabel = new javax.swing.JLabel();
  private final javax.swing.JTextField propTypeTextField = new javax.swing.JTextField();
  private javax.swing.JLabel propValueLabel = new javax.swing.JLabel();
  private final javax.swing.JTextField propValueTextField = new javax.swing.JTextField();

  /** The property name */
  private String propName = null;

  /** The property type */
  private Class<? extends Object> propType;

  /** The property value */
  private String propValue;

  // ==========================================================================
  // Constructors
  // ==========================================================================
  
  /**
   * Default constructor.
   */
  public PropertyPanel()
  {
      super();
  }
      
  // ==========================================================================
  // Internal methods
  // ==========================================================================

  /**
   * Initialize graphical components of this panel.
   */
  protected final void init()
  {
      final ClassLoader frascatiClassLoader = FraSCAtiExplorer.SINGLETON.get()
                                             .getFraSCAtiExplorerService(ClassLoaderManager.class)
                                             .getClassLoader();
      URL iconUrl = frascatiClassLoader.getResource("icons/scaProperty.png");

      // Layout here!
      propNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
      propNameLabel.setIcon(new javax.swing.ImageIcon(iconUrl));

      propNameLabel.setText(propName + " property");
      propNameLabel.setName("propNameLabel");

      propTypeLabel.setText("Type");
      propTypeLabel.setName("propTypeLabel");

      propValueLabel.setText("Value");
      propValueLabel.setName("propValueLabel");

      propTypeTextField.setText(propType == null ? "" : propType.getCanonicalName());
      propTypeTextField.setName("propTypeTextField");
      propTypeTextField.setEditable(false);

      propValueTextField.setText(propValue);
      propValueTextField.setName("propValueTextField");
      propValueTextField.addFocusListener( new FocusListener() {        
        public final void focusLost(FocusEvent e)
        {
            propValueTextField.setBackground(Color.WHITE);
        }

        public final void focusGained(FocusEvent e)
        {
            propValueTextField.setBackground( new Color(102, 153, 255) );
        }
      });
      propValueTextField.addActionListener( new ActionListener() {        
        public final void actionPerformed(ActionEvent e)
        {
        	Object computedPropertyValue;
        	
        	try {
				computedPropertyValue = ScaPropertyTypeJavaProcessor.stringToValue(
											propType.getCanonicalName(),
											propValueTextField.getText(),
											frascatiClassLoader);
        	} catch(URISyntaxException exc) {
        		String msg = propName + " - Syntax error in URI '" + propValueTextField.getText() + "'";
        		JOptionPane.showMessageDialog(null, msg);
        		logger.warning(msg);
        		return;
        	} catch(MalformedURLException exc) {
        		String msg = propName + " - Malformed URL '" + propValueTextField.getText() + "'";
        		JOptionPane.showMessageDialog(null, msg);
        		logger.warning(msg);
        		return;
        	} catch(NumberFormatException nfe) {
        		String msg = propName +	" - Number format error in '" + propValueTextField.getText() + "'";
        		JOptionPane.showMessageDialog(null, msg);
        		logger.warning(msg);
        		return;
        	} catch (ClassNotFoundException cnfe) {
        		String msg = propName +	" - Java type known but not dealt by OW2 FraSCAti: '" + propValueTextField.getText() + "'";
        		JOptionPane.showMessageDialog(null, msg);
        		logger.severe(msg);
        		return;
			}

        	try {
              LifeCycleController lc = Fractal.getLifeCycleController(((Interface)selected).getFcItfOwner());
              lc.stopFc();
              selected.setType(propName, computedPropertyValue.getClass());
              selected.setValue(propName, computedPropertyValue);
              lc.startFc();
        	} catch(Exception exc) {
              exc.printStackTrace();
              String msg = exc.toString();
              JOptionPane.showMessageDialog(null, msg);
              logger.warning(msg);
         	}
            propValueTextField.setBackground(Color.WHITE);
        }
      });

      org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
      this.setLayout(layout);
      layout.setHorizontalGroup(
          layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
          .add(layout.createSequentialGroup()
              .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                  .add(layout.createSequentialGroup()
                      .addContainerGap()
                      .add(propNameLabel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE))
                  .add(layout.createSequentialGroup()
                      .add(20, 20, 20)
                      .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                          .add(propValueLabel)
                          .add(propTypeLabel))
                      .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                      .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                          .add(propValueTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE)
                          .add(propTypeTextField, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 322, Short.MAX_VALUE))))
              .addContainerGap())
      );
      layout.setVerticalGroup(
          layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
          .add(layout.createSequentialGroup()
              .addContainerGap()
              .add(propNameLabel)
              .add(32, 32, 32)
              .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                  .add(propTypeLabel)
                  .add(propTypeTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
              .add(18, 18, 18)
              .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                  .add(propValueLabel)
                  .add(propValueTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
              .addContainerGap(184, Short.MAX_VALUE))
      );
  }

  // ==========================================================================
  // Methods for Panel interface 
  // ==========================================================================
  
  /**
   * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
   */
  @SuppressWarnings("unchecked")
  public final void selected(TreeView treeView)
  {
      selected = (SCAPropertyController) treeView.getSelectedObject();
      propName = (String) treeView.getSelectedEntry().getName();
      propType = selected.getType(propName);
      Object value = selected.getValue(propName);
      if (value == null) {
    	  propValue = null;
      } else {
    	  propValue = value.toString();
      }
      
      init();
  }
}