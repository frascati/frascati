/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.explorer.action;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.explorer.context.InterfaceWrapper;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.ow2.frascati.explorer.ExplorerGUI;
import org.ow2.frascati.explorer.context.ScaBindingWrapper;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;

/**
 * This class provides the ability dynamically remove an SCA binding 
 * from an SCA service (or an SCA reference) via the "Remove binding"
 * Menu item {@link MenuItem}.
 *
 * @author Christophe Demarey
 */
public class RemoveBindingMenuItem
     extends AbstractAlwaysEnabledMenuItem<ScaBindingWrapper>
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------
    public static final Logger LOG = Logger.getLogger("org.ow2.frascati.explorer.action.RemoveBindingMenuItem");
       
    // --------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------

    /**
     * The default constructor.
     */
    public RemoveBindingMenuItem()
    {
    }

    // --------------------------------------------------------------------------
    // Internal methods
    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    // --------------------------------------------------------------------------
    // Implementation of the MenuItem interface
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute(Object, MenuItemTreeView)
     */
    @Override
    protected final void execute(ScaBindingWrapper selected, final MenuItemTreeView treeView)
    {
        Object parentEntry = treeView.getParentObject(); // Binding parent entry in the explorer tree
        boolean isScaReference = parentEntry instanceof TinfiComponentOutInterface<?>;
        Interface itf = (Interface) parentEntry; // The SCA interface
        String itfName = (String) treeView.getParentEntry().getName(); // The SCA interface name
        Component owner = itf.getFcItfOwner(); // The SCA interface owner component
        Component bindingComponent = ((InterfaceWrapper) selected).getItf().getFcItfOwner(); // the BF stub or skeleton
        
        BindingFactory bf = getFraSCAtiExplorerService(BindingFactory.class);
        try {
            if ( isScaReference ) {
                bf.unbind(owner, itfName, bindingComponent);
            } else {
                bf.unexport(owner, bindingComponent, new HashMap<String, Object>());
            }
        } catch (BindingFactoryException bfe) {
            LOG.log(Level.SEVERE, "Cannot unbind/unexport this service/reference!", bfe);
        }
    }
}
