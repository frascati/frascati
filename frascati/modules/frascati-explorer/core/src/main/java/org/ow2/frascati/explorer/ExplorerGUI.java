/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.explorer;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import org.objectweb.fractal.bf.BindingFactory;

import org.objectweb.util.explorer.api.Tree;
import org.objectweb.util.explorer.parser.api.ParserConfiguration;
import org.objectweb.util.explorer.swing.api.Explorer;
import org.objectweb.util.explorer.swing.api.StatusBar;
import org.objectweb.util.explorer.swing.api.ViewPanel;
import org.objectweb.util.explorer.swing.lib.DefaultTreePanel;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.explorer.api.CursorController;
import org.ow2.frascati.explorer.api.FraSCAtiExplorer;
import org.ow2.frascati.util.AbstractActiveComponent;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * The main FraSCAti Framework Explorer GUI.
 *
 * @author Christophe Demarey
 */
public class ExplorerGUI
     extends AbstractActiveComponent
  implements FraSCAtiExplorer,
             CursorController
{
  /** FraSCAti Explorer configuration files standard name. */
  public static final String EXPLORER_STD_CFG_FILES_NAME = "META-INF/FraSCAti-Explorer.xml";
    
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  /**
   * The title of the frame.
   */
  private String frameTitle;

  /**
   * The explorer configuration files to use.
   */
  @Property(name = "explorer-configuration-files")
  private String explorerConfigurationFiles;

  /**
   * The role used at the beginning.
   */
  @Property(name = "initial-role")
  private String initialRole;

  /**
   * The configuration parser.
   */
  @Reference(name = "parser-configuration")
  private ParserConfiguration parser ;
  
  /**
   * The Explorer engine.
   */
  @Reference(name = "explorer")
  private Explorer explorer;
  
  /**
   * The Explorer Tree.
   */
  private Tree explorerTree;
  
  /**
   * The Explorer view panel.
   */  
  @Reference(name = "view-panel")
  private ViewPanel viewPanel;
  
  /**
   * The Explorer Status bar.
   */
  @Reference(name = "status-bar")
  private StatusBar statusBar;

  /**
   * The Explorer GUI frame.
   */
  private JFrame frame;

  /**
   * List of FraSCAti Explorer services.
   */
  private Map<Class<?>, Object> frascatiExplorerServices = new HashMap<Class<?>, Object>();

  /**
   * Class loader associated to the FraSCAti Explorer.
   */
  private FrascatiClassLoader explorerClassLoader;

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The default constructor.
   */
  public ExplorerGUI ()
  {
	  // Init the FraSCAti Explorer singleton.
	  FraSCAtiExplorer.SINGLETON.set(this);
	  // Add the cursor manager as a FraSCAti Explorer service.
	  frascatiExplorerServices.put(CursorController.class, this);
  }

  // --------------------------------------------------------------------------
  // SCA setter methods.
  // --------------------------------------------------------------------------

  /**
   * Set the frame title.
   */
  @Property(name = "frame-title")
  private final void setFrameTitle(String frameTitle)
  {
	this.frameTitle = frameTitle;
    if(this.frame != null) {
      this.frame.setTitle(this.frameTitle);
    }
  }

  /**
   * Set the Explorer Tree reference.
   */
  @Reference(name = "tree")
  private final void setTree(Tree tree)
  {
	this.explorerTree = tree;
	// Add the tree as a FraSCAti Explorer service.
    this.frascatiExplorerServices.put(Tree.class, tree);
  }

  /**
   * Set the CompositeManager reference.
   */
  @Reference(name = "composite-manager")
  private final void setCompositeManager(CompositeManager cm)
  {
    // Add the composite manager as a FraSCAti Explorer service.
    this.frascatiExplorerServices.put(CompositeManager.class, cm);
  }
  
  /**
   * Set the classloaderManager interface.
   */
  @Reference(name = "classloader-manager")
  private final void setClassLoaderManager(ClassLoaderManager clm)
  {
    // Add the classloader manager as a FraSCAti Explorer service.
    this.frascatiExplorerServices.put(ClassLoaderManager.class, clm);
  }
  
  /**
   * Set the binding factory.
   */
  @Reference(name = "binding-factory")
  private final void setBindingFactory(BindingFactory bf)
  {
    // Add the binding factory as a FraSCAti Explorer service.
    this.frascatiExplorerServices.put(BindingFactory.class, bf);
  }

  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------

  /**
   * Run the GUI.
   */
  public final void run()
  {
	//
    // Configures the explorer engine.
    //

    if (explorerConfigurationFiles != null) {
      StringTokenizer st = new StringTokenizer(explorerConfigurationFiles, "; ");
      
      while (st.hasMoreTokens()) {
        String theFile = st.nextToken();
        parser.addPropertyFile(theFile);
      }
    }

    // Search all META-INF/FraSCAti-Explorer.xml resources in the classpath and
    // load them with the Explorer Framework.
    // Then each JAR in the classpath could provide its own FraSCAti Explorer plugins.
    try {
        for(URL url : Collections.list(getClass().getClassLoader().getResources(EXPLORER_STD_CFG_FILES_NAME))) {
            log.info("FraSCAti Explorer - Load " + url);
            parser.addPropertyFile(url.toString());
        }
    } catch(IOException ioe) {
    	// Must never happen!!!
        log.warning("FraSCAti Explorer - Impossible to get " + EXPLORER_STD_CFG_FILES_NAME + " resources!");
    }

	parser.parse();

    explorer.setCurrentRoles(new String[] { initialRole });

    //
    // Creates the GUI.
    //

    frame = new JFrame(frameTitle);

    JMenuBar menuBar = new JMenuBar();
    frame.setJMenuBar(menuBar);
    explorer.setMenuBar(menuBar);

    JPanel panel = new JPanel(new BorderLayout());
    frame.getContentPane().add(panel);

    JToolBar toolBar = new JToolBar();
    panel.add(toolBar,BorderLayout.NORTH);
    explorer.setToolBar(toolBar);

    panel.add(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true,
                             new DefaultTreePanel(explorer.getTree()),
                             viewPanel.getViewPanel() ),
              BorderLayout.CENTER);
    
    panel.add(statusBar.getStatusBar(), BorderLayout.SOUTH);

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();

    // Centers the frame.
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    frame.setLocation((screenSize.width - frame.getWidth()) / 2,
                      (screenSize.height - frame.getHeight()) / 2);

    frame.setVisible(true);
    
    // Add the root component
    explorerTree.addEntry("SCA domain", getFraSCAtiExplorerService(CompositeManager.class), 1);

    // Init the class loader of the FraSCAti Explorer.
    explorerClassLoader = new FrascatiClassLoader(getFraSCAtiExplorerService(ClassLoaderManager.class).getClassLoader());
    explorerClassLoader.setName("OW2 FraSCAti Explorer");
    frascatiExplorerServices.put(ClassLoader.class, explorerClassLoader);
    FrascatiExplorerClassResolver.setClassLoader(explorerClassLoader);
  }

  // --------------------------------------------------------------------------
  // Implementation of the FraSCAtiExplorer interface
  // --------------------------------------------------------------------------

  /**
   * @see FraSCAtiExplorer#getFraSCAtiExplorerService(Class)
   */
  @SuppressWarnings("uncheck")
  public final <T> T getFraSCAtiExplorerService(Class<T> interfaceType)
  {
    return (T)frascatiExplorerServices.get(interfaceType);
  }

  // --------------------------------------------------------------------------
  // Implementation of the CursorController interface
  // --------------------------------------------------------------------------

  /**
   * @see CursorController#setDefaultCursor()
   */
  public final void setDefaultCursor()
  {
    frame.setCursor(Cursor.DEFAULT_CURSOR);
  }

  /**
   * @see CursorController#setWaitCursor()
   */
  public final void setWaitCursor()
  {
    frame.setCursor(Cursor.WAIT_CURSOR);
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Update the FraSCAti Explorer class loader.
   * * add the given class loader to the FraSCAti Explorer class loader.
   * * update the FraSCAti Explorer configuration in order to load plug-ins dynamically.
   * 
   * @param classLoader the class loader to add.
   */
  public final void updateClassLoader(ClassLoader classLoader)
  {
    // Add the given class loader to the FraSCAti Explorer class loader.
    explorerClassLoader.addParent(classLoader);

    // Update the FraSCAti Explorer configuration in order to load plug-ins dynamically
    if(classLoader instanceof URLClassLoader) {
      for(URL url : ((URLClassLoader)classLoader).getURLs()) {
        addExplorerConfiguration(url);
      }
    } else {
      log.warning("Given class loader " + classLoader + " is not a URLClassLoader!");
    }
  }

  /**
   * Add a configuration file to the explorer (useful to load plug-ins dynamically).
   * 
   * @param url The path to the configuration file to add.
   */
  public final void addExplorerConfiguration(URL url)
  {
    de.schlichtherle.io.File cfgFile = new de.schlichtherle.io.File(url.getPath() + java.io.File.separator + ExplorerGUI.EXPLORER_STD_CFG_FILES_NAME);
    if(cfgFile.exists()) {
      String explorerXmlConfigurationFile = "jar:" + url.toString() + "!/" + ExplorerGUI.EXPLORER_STD_CFG_FILES_NAME;
      log.info("FraSCAti Explorer - Load " + explorerXmlConfigurationFile);
      parser.addPropertyFile(explorerXmlConfigurationFile);
      parser.parse();
    }
  }

}
