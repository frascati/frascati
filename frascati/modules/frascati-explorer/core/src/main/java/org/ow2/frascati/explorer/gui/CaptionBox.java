/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.gui;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.objectweb.util.explorer.swing.gui.api.ValidateReport;
import org.objectweb.util.explorer.swing.gui.lib.AbstractElementBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultValidateReport;

/**
 * This class represents the panel which allow to specify a label
 * 
 * @author Christophe Demarey.
 */
public class CaptionBox
    extends AbstractElementBox
{
    private static final long serialVersionUID = 5985520498957769873L;

    /** The value of the label */
// Unused    private String caption;

    /**
     * Default constructor.
     * @param label The label
     */
    public CaptionBox(String caption)
    {
        super(false);
//       this.caption = caption;
        //add(Box.createHorizontalGlue());
        JLabel label = new JLabel(caption, SwingConstants.LEFT);
        //label.setAlignmentX(Component.LEFT_ALIGNMENT);
        //label.setAlignmentY(Component.LEFT_ALIGNMENT);
        add(label);
    }

    // ==================================================================
    //
    //  Public methods for ElementBox.
    //
    // ==================================================================

    /**
     * Validates the content of the ElementBox. Indicates if the different value are acceptables or not.
     * @return The corresponding ValidateReport.
     */
    public final ValidateReport validateBox()
    {
        return new DefaultValidateReport();
    }
    
    /**
     * Returns the Box contains by the ElementBox.
     * @return The Box contains by the ElementBox.
     */
    public final Box getBox()
    {
        return this;
    }

    // ==================================================================
    //
    //  Public methods.
    //
    // ==================================================================

}
