/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.objectweb.util.trace;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Specialization of the Java util trace mechanism to deal with 
 * the fractal explorer log mechanism.<BR>
 * <p>
 *   The TraceTemplate class provides implementation of the trace 
 *   interface based on the Java util log implementation.
 * </p>
 *
 * @author Christophe Demarey
 */
public class TraceTemplate 
  implements Trace
{
    /** underlying Java logger reference*/
    private Logger logger = null;    
    
    /**
     * Default Constructor.
     *
     * @param log Java logger to use.
     */
    public TraceTemplate(Logger log)
    {
        setLogger(log); 
    }

    /**
     * Constructor with OW2 Monolog Logger.
     *
     * @param log OW2 Monolog Logger.
     */
    public TraceTemplate(org.objectweb.util.monolog.api.Logger log)
    {
      // Not implemented but could be invoked by the OW2 Explorer Framework
      // when Monolog is activated into the OW2 Explorer Framework.
    }

    /**
     * @return Returns the logger.
     */
    public final Logger getLogger()
    {
        return logger;
    }

    /**
     * @param logger The logger to set.
     */
    public final void setLogger(Logger logger)
    {
        this.logger = logger;
    }

    /**
     * Displays a debug message.
     *
     * @param msg debugging message to print.
     */
    public final void debug(String msg)
    {
        getLogger().log(Level.FINE, msg); 
    }

    /**
     * Displays an information message.
     *
     * @param msg information message to print.
     */
    public final void info(String msg)
    {
        getLogger().log(Level.INFO, msg); 
    }

    /**
     * Displays a warning message.
     *
     * @param msg warning message to print.
     */
    public final void warn(String msg)
    {
        getLogger().log(Level.WARNING, msg); 
    }

    /**
     * Displays an error message.
     *
     * @param msg error message to print.
     */
    public final void error(String msg)
    {
        getLogger().log(Level.SEVERE, msg); 
    }

    /**
     * Displays a fatal message.
     *
     * @param msg fatal message to print.
     */
    public final void fatal(String msg)
    {
        getLogger().log(Level.SEVERE, msg); 
    }
}
