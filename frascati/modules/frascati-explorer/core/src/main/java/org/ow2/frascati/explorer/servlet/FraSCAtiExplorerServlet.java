/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.explorer.servlet;

import java.util.Properties;

import org.ow2.frascati.servlet.FraSCAtiServlet;
import org.ow2.frascati.explorer.FrascatiExplorerLauncher;

/**
 * OW2 FraSCAti Explorer Servlet launching SCA composites with FraSCAti Explorer.
 *
 * @author Philippe Merle
 *
 */
public class FraSCAtiExplorerServlet
     extends FraSCAtiServlet
{
  /**
   * Launch SCA composites with FraSCAti Explorer.
   *
   * @param composites The SCA composite to launch.
   */
  @Override
  protected final void launch(String[] composites, Properties contextualProperties)
  {
    // Launch FraSCAti Explorer.
    FrascatiExplorerLauncher.main(composites);
  }
}
