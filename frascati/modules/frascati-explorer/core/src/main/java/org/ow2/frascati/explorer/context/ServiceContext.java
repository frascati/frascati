/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Reference context used to specified SCA Bindings as childs of Reference nodes.
 *  
 * @author Christophe Demarey.
 */
public class ServiceContext
     extends ScaInterfaceContext
{
  private static final long serialVersionUID = -6551766956254071130L;

  /**
   * Get entries as defined in the org.objectweb.fractal.explorer.context.InterfaceContext class.
   * 
   * @param object The server interface to wrap.
   * @return entries for interfaces other than client interfaces or control interfaces.
   */
  private List<Entry> getInterfaceContextEntries(Object object)
  {
    Interface itf = (Interface) object;
    InterfaceType type = (InterfaceType) itf.getFcItfType();
    
    if ( type.isFcClientItf() || type.getFcItfName().endsWith("-controller") ) {
      return Collections.emptyList();
    }
    
    String signature = type.getFcItfSignature();
    Class<?>[] itfs = object.getClass().getInterfaces();
    List<Entry> entries = new ArrayList<Entry>();
    
    for (Class<?> c : itfs) {
        if (signature.equals(c.getName())) {
            entries.add( new DefaultEntry(signature, c) );
        }
    }
    return entries;
  }
  
  /**
   * Get bindings on SCA services.
   * Bindings are implemented as Fractal components. For each binding, you can
   * find a skeleton component in the container (the parent) of the service owner.
   * So we will search for such skeleton components. 
   * 
   * @param owner The component owning the interface to search bindings on.
   * @param itf The interface to search bindings on.
   * @return a List with an explorer entry for each binding.
   */
  private List<Entry> getServiceBindingEntries(Component owner, Interface itf)
  {
    List<Entry> entries = new ArrayList<Entry>();
    
    try {
        Component[] parents = FcExplorer.getSuperController(owner).getFcSuperComponents();

        for (Component c : parents) {
           ContentController cc = FcExplorer.getContentController(c);
           Component[] children = cc.getFcSubComponents();
           
           for (Component child : children) {
               String childName = FcExplorer.getName(child);
               if ( childName.endsWith("-skeleton") ) {
                   // check that the skeleton is bound to owner
                   BindingController bc = FcExplorer.getBindingController(child);
                   try {
                	   Interface servant = (Interface) bc.lookupFc(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
                   
                	   if (servant.equals(itf)) {
                		   Object bindingEntry = null;
                		   Interface servantItf = (Interface) child.getFcInterface(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
                		   // Check the binding kind
                		   if( childName.endsWith("-ws-skeleton") ) {
                			   bindingEntry = new WsBindingWrapper(servantItf);
                		   } else if ( childName.endsWith("-RESTful-skeleton") ) {
                			   bindingEntry = new RestBindingWrapper(servantItf);
                		   } else if ( childName.endsWith("-JSON-RPC-skeleton") ) {
                			   bindingEntry = new JsonRpcBindingWrapper(servantItf);
                		   }
                		   if ( bindingEntry != null ) {
                			   entries.add( new DefaultEntry(childName + "." + AbstractSkeleton.SERVANT_CLIENT_ITF_NAME, bindingEntry) );
                		   }
                	   }
                   } catch (NoSuchInterfaceException e) {
                	   // RMI skeleton has no servant
                	   // Nothing to do, entry is added in ScaInterfaceContext.getRmiBindingEntriesFromComponentController()
                   }
               }
           }
        }
    } catch (NoSuchInterfaceException e) {
        e.printStackTrace();
    }
    
    return entries;
  }
 
  /**
   * @see org.objectweb.util.explorer.api.Context#getEntries(java.lang.Object)
   */
  public final Entry[] getEntries(Object object)
  {
    List<Entry> entries = new ArrayList<Entry>();
    Interface itf = (Interface) object;
    String itfName = itf.getFcItfName();
    Component owner = itf.getFcItfOwner();

    // Add implemented interfaces
    entries.addAll( getInterfaceContextEntries(object) );
    
    // Add entries for SCA bindings
    entries.addAll( getBindingEntries(owner, itf) );
    entries.addAll( getServiceBindingEntries(owner, itf) );
    
    // Add entries for SCA intents
    entries.addAll( getIntentEntries(owner, itfName) );

    return entries.toArray(new Entry[entries.size()]);
  }
}
