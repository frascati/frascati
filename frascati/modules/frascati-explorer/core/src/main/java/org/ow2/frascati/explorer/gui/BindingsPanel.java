/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.gui;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.attributes.AttributeControllerPanel;
import org.objectweb.fractal.explorer.attributes.AttributesTable;
import org.objectweb.fractal.explorer.attributes.AttributesTableModelBuilder;
import org.objectweb.fractal.explorer.context.InterfaceWrapper;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.TreeView;

public class BindingsPanel 
     extends AttributeControllerPanel
{
  // ==========================================================================
  // Internal state
  // ==========================================================================
  
  private static final long serialVersionUID = 522026656354592610L;
  
  /** The (stub) component owning this binding */
  private Component owner = null; 
  
  /** Flag used to know if an attribute controller was found */
  private boolean hasAttributeController = false;

  /** The logger */
  private static Logger logger = Logger.getLogger("org.ow2.frascati.explorer.gui.BindingsPanel");
  
  // ==========================================================================
  // Constructors
  // ==========================================================================
  
  /**
   * Default constructor.
   */
  public BindingsPanel()
  {
  }

  // ==========================================================================
  // Internal methods
  // ==========================================================================
   
  // ==========================================================================
  // Methods for Panel interface 
  // ==========================================================================
  
  protected final void initialize()
  {
    GridBagConstraints gridBagConstraints = new GridBagConstraints();
    JButton button = new JButton("Reconfigure");

    super.initialize();

    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraints.insets = new java.awt.Insets(10, 10, 10, 10);
    gridBagConstraints.weighty = 1.0D;
    gridBagConstraints.weightx = 1.0D;
    gridBagConstraints.anchor = GridBagConstraints.SOUTH;
    
    button.addActionListener( new ActionListener() {
      public final void actionPerformed(ActionEvent e)
      {
        // Restart stub component
        try {
          LifeCycleController lifeCycleController = Fractal.getLifeCycleController(owner);
          lifeCycleController.stopFc();
          lifeCycleController.startFc();
        } catch (NoSuchInterfaceException ex) {
          logger.log(Level.WARNING, "Cannot find LifeCycleController for this AttributeController owner", ex);
        } catch (IllegalLifeCycleException ex) {
          logger.log(Level.WARNING, "LifeCycle Exception while trying to restart this AttributeController owner", ex);
        }
      }} );
    
    this.add(button, gridBagConstraints);
  }

  /**
   * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
   */
  public final void selected(TreeView treeView)
  {
    Interface itf = null;
    AttributeController ac = null;
    Object selectedObject = treeView.getSelectedObject();
    
    try {
      itf = ((InterfaceWrapper) selectedObject).getItf();
    } catch (ClassCastException e) {
      itf = (Interface) selectedObject;
    }
    
    if (itf != null) {
        try {
          this.owner = itf.getFcItfOwner();
          ac = Fractal.getAttributeController( this.owner );
        } catch (NoSuchInterfaceException e) {
          // RMI specific case : AC is in a sub-component
          try {
            Component[] children = FcExplorer.getContentController(this.owner).getFcSubComponents();
            for (Component child : children) {
                String name = FcExplorer.getName(child);
                if (    name.equals("org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonContent")
                     || name.equals("rmi-stub-primitive") ) {
                    ac = Fractal.getAttributeController(child);
                }
            }
          } catch (NoSuchInterfaceException e1) {
              // Should not happen : bindings should have an attribute controller
              logger.log( Level.WARNING, "Cannot find an Attribute Controller for "
                          + FcExplorer.getName(this.owner) + "!" );
          }
        }
        
        if (ac != null) {
            this.hasAttributeController = true;
            attributesTable = new AttributesTable(ac, this);
      
            jPanel4.add(attributesTable.getTableHeader());
            jPanel4.add(attributesTable, null);
      
            String controlledInterfaceName = AttributesTableModelBuilder.getControlledInterfaceName();
      
            controllerNameLabel.setText(controlledInterfaceName);            
        } 
    } else {
        // Should not happen : bindings should have an attribute controller
        logger.log( Level.WARNING, "Cannot find an Attribute Controller for "
                    + FcExplorer.getName(this.owner) + "!" );
    }
  }

  @Override
  public final void unselected(TreeView treeView)
  {
    if ( this.hasAttributeController ) {
        super.unselected(treeView);
    }
  }
}
