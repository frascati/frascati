/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientCollectionInterfaceContainer;
import org.objectweb.fractal.explorer.lib.SignatureWrapper;
import org.objectweb.fractal.explorer.panel.TableClientCollectionWrapper;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Table used to display SCA services and references of the component.
 */
public class ComponentTable
     extends org.objectweb.fractal.explorer.panel.ComponentTable
{
    /**
     * @see org.objectweb.util.explorer.api.Table#getRows(org.objectweb.util.explorer.api.TreeView)
     */
    public final Object[][] getRows(TreeView treeView)
    {
        Map<String, ContextContainer> collections = new HashMap<String, ContextContainer>();
        ArrayList<Object[]> rows = new ArrayList<Object[]>();
        
        Component component = (Component)treeView.getSelectedObject();
        BindingController bc = null;
        try {
            bc = Fractal.getBindingController(component);
        } catch(Exception e) {
            // Binding-controller may be unavailable (e.g. Bootstrap component).
            // System.err.println("Client interface Error : " + e.getMessage());   
        }
        
        // Prepare the collection interfaces.
        ComponentType cType = (ComponentType)component.getFcType();
        InterfaceType[] iTypes = cType.getFcInterfaceTypes();
        for (InterfaceType iType : iTypes) {
            if (FcExplorer.isClientCollection(iType)) {
                ContextContainer cc = new ClientCollectionInterfaceContainer(iType, component);
                collections.put(iType.getFcItfName(),cc);
            }
        }
        
        // Introspect the component interfaces.
        Object[] interfaces = component.getFcInterfaces();
        for (Object interfaze : interfaces) {
            if (!component.equals(interfaze)) {
                Interface itf = (Interface) interfaze;
                if (!FcExplorer.isController(itf)) {
                    InterfaceType iType = (InterfaceType) itf.getFcItfType();
                    String signature = iType.getFcItfSignature();
                    if (FcExplorer.isClientCollection(itf)) {                    
                        String name = FcExplorer.getName(itf);
                        ((ContextContainer) collections.get(iType.getFcItfName())).addEntry(name, new TableClientCollectionWrapper(itf));
                    } else if(FcExplorer.isClient(itf)) {
                    	Object boundInterface = null;
                    	try {
                          boundInterface = getBoundInterface(itf,bc);
                    	} catch(ClassCastException cce) {
                          // This exception is due to the fact that this 'itf' reference is bound to an SCA binding.
                    	  // Do nothing.
                    	}
                        rows.add(new Object[] {new DefaultEntry(FcExplorer.getName(itf), itf),
                                                 new DefaultEntry(signature, new SignatureWrapper(signature)),
                                                 boundInterface});
                    } else {
                        rows.add(new Object[] {new DefaultEntry(FcExplorer.getName(itf), itf),
                                                 new DefaultEntry(signature, new SignatureWrapper(signature)),
                                                 null});
                    }
                }
            }
        }
        // Add the collection interfaces
        for (ContextContainer container: collections.values()) {
            ClientCollectionInterfaceContainer collection = (ClientCollectionInterfaceContainer) container;
            Entry[] entries = collection.getEntries(null);
            String signature = collection.getItf().getFcItfSignature();
            // The collection interface is not bound
            if (entries.length == 0) {
                rows.add( new Object[]{ new DefaultEntry(collection.getItf().getFcItfName(),collection),
                                        new DefaultEntry(signature, new SignatureWrapper(signature)),
                                        null } );
            } else {
                for(int j = 0 ; j < entries.length ; j++) {
                    Interface itf = ((TableClientCollectionWrapper)entries[j].getValue()).getItf();
                    rows.add( new Object[]{ entries[j],
                                            new DefaultEntry(signature, new SignatureWrapper(signature)),
                                            getBoundInterface(itf,bc) } );
                }
             } 
        }
        
        Object[][] contenu = new Object[rows.size()][2];
        for (int i=0 ; i<rows.size() ; i++) {
            contenu[i] = (Object[]) rows.get(i);
        }
        return contenu;
    }
}
