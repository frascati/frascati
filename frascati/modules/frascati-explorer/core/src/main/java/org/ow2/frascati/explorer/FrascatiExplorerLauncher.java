/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Christophe Demarey
 * 
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer;

import java.io.File;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.explorer.plugin.RefreshExplorerTreeThread;

/**
 * OW2 FraSCAti Explorer launcher.
 * It allows to:
 * <ul>
 * <li>instanciate the FraSCAti runtime,</li>
 * <li>instanciate the explorer architecture,</li>
 * <li>eventually load SCA composites,</li>
 * <li>and show the FraSCAti explorer GUI.</li>
 * 
 * @author Christophe Demarey.
 */
public final class FrascatiExplorerLauncher
{
  private static final String BOOTSTRAP_PROPERTY = "org.ow2.frascati.bootstrap";
  /**
   * Can not be instantiated.
   */
  private FrascatiExplorerLauncher()
  {
  }

  /**
   * The bootstrap method.
   * 
   * @param args - program arguments.
   */
  public static void main(String[] args)
  {
    // The SCA composite for FraSCAti Explorer uses <frascati:implementation.fractal>
	// so a FraSCAti Fractal bootstrap is required.
	String bootstrap = System.getProperty(BOOTSTRAP_PROPERTY);
	if ((bootstrap == null)) { // || !bootstrap.contains("Fractal") ) {
		System.setProperty(BOOTSTRAP_PROPERTY, "org.ow2.frascati.bootstrap.FraSCAtiFractal");
	}

    try {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      for (String argument : args) {
        if (argument.endsWith(".zip")) {
          File file = new File(argument); 
          if (file.exists())
              frascati.getContribution(argument);
          else {
              System.err.println("Cannot found " + file.getAbsolutePath());
              System.exit(0);
          }
        } else 
          frascati.getComposite(argument);
      }
    } catch(Exception e) {
      e.printStackTrace();
      throw new Error(e);
    }

    // Refresh the FraSCAti Explorer GUI.
    RefreshExplorerTreeThread.refreshFrascatiExplorerGUI();
  }

}
