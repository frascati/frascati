/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.explorer.action;

/**
 * Is the FraSCAti Explorer {@link MenuItem} to execute {@link Runnable#run()}
 * in a new {@link Thread} instance.
 *
 * @author Philippe Merle
 * @author Christophe Demarey
 */
public class RunInNewThreadMenuItem
     extends AbstractAlwaysEnabledMenuItem<Runnable>
{
  /**
   * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute()
   */
  @Override
  protected final void execute(Runnable runnable)
  {
    runAsynchronously(runnable);
  }
}
