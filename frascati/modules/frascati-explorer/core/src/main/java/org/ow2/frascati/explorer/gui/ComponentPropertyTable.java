/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.util.explorer.api.Table;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

public class ComponentPropertyTable
  implements Table
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================
    
    private Object[] getRow(String propName, SCAPropertyController propCtl)
    {
        Object[] row = new Object[3];
        Object value = null;
        
        row[0] = new DefaultEntry(propName, propCtl);
        row[1] = propCtl.getType(propName);
        value = propCtl.getValue(propName);
        if (value != null) {
        	row[2] = value.toString();
        }
        
        return row;
    }
    
    //==================================================================
    //
    // Public methods for Table interface.
    //
    //==================================================================
    
    public final String[] getHeaders(TreeView treeView)
    {
        return new String[] {"SCA Property", "Type", "Value"};
    }

    public final Object[][] getRows(TreeView treeView)
    {
        List<Object[]> rows = new ArrayList<Object[]>();
        Set<String> propNames = new TreeSet<String>();
        Component comp = (Component) treeView.getSelectedObject();
        
        try {
            SCAPropertyController propCtl = (SCAPropertyController) comp.getFcInterface(SCAPropertyController.NAME);

            for (String propName: propCtl.getPropertyNames()) { 
                rows.add( getRow(propName, propCtl) );
                propNames.add(propName);
            }
        } catch (NoSuchInterfaceException e) {
            e.printStackTrace();
        }
        
        return rows.toArray( new Object[0][0] );
    }
}
