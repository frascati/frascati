/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 *
 */
package org.ow2.frascati.explorer.context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcSkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcStubContentAttributes;
import org.objectweb.fractal.bf.connectors.rest.RestSkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.rest.RestStubContentAttributes;
import org.objectweb.fractal.bf.connectors.ws.WsSkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;

/**
 * SCA interface context used to add SCA entries as childs of this node.
 *  
 * @author Christophe Demarey.
 */
public abstract class ScaInterfaceContext
           implements Context
{
	private static final long serialVersionUID = 9057818431283621672L;

  /**
   * Tells if this interface has SCA intents or not.
   * 
   * @param owner The component owning this interface.
   * @param itfName The name of the interface to search intents on.
   * @return True if this interface has at least one intent.
   */
  private List<IntentHandler> getIntents(Component owner, String itfName)
  {
    SCABasicIntentController ic = null;

    try {
      ic = (SCABasicIntentController) owner.getFcInterface(SCAIntentController.NAME);
      return ic.listFcIntentHandler(itfName);
    } catch (NoSuchInterfaceException e) {
      return Collections.emptyList();
    }
  }

  /**
   * Get explorer tree entries for SCA intents.
   * 
   * @param owner The component owning this interface.
   * @param itfName The name of the interface to search intents on.
   * @return a List with an entry for each intent.
   */
  protected final List<Entry> getIntentEntries(Component owner, String itfName)
  {
    List<Entry> entries = new ArrayList<Entry>();
    Set<IntentHandler> intents = new HashSet<IntentHandler>();
    
    // We get 1 intent Handler / intent & method => group them to avoid duplicates
    intents.addAll( getIntents(owner, itfName) );
    
    if ( !intents.isEmpty() ) {
      for (IntentHandler intent : intents) {
        Component intentComponent = ((Interface) intent).getFcItfOwner();
        entries.add( new DefaultEntry(FcExplorer.getName(intentComponent),
                     new IntentWrapper(intentComponent)) );
      }
    }
    return entries;
  }
  
  /**
   * Get explorer tree entries for SCA bindings.
   * 
   * @param owner The component owning this interface.
   * @param itf The interface to search bindings on.
   * @return a List with an entry for each binding.
   */
  protected final List<Entry> getBindingEntries(Component owner, Interface itf)
  {
    List<Entry> entries = new ArrayList<Entry>();
    Interface boundInterface = null;
    
    try {
      BindingController bc = FcExplorer.getBindingController(owner);
      boundInterface = (Interface) bc.lookupFc( itf.getFcItfName() );
    } catch(Exception e) { 
      // Nothing to do.
    }
    
    if(boundInterface != null) {
      Component boundInterfaceOwner = boundInterface.getFcItfOwner();
      
      try {
        AttributeController ac = FcExplorer.getAttributeController( boundInterfaceOwner );
        // WS binding
        if (   (ac instanceof WsSkeletonContentAttributes)
            || (ac instanceof WsStubContentAttributes) ) {
          entries.add( new DefaultEntry(FcExplorer.getPrefixedName(boundInterface),
                       new WsBindingWrapper(boundInterface)) );
        }          
        // RESTful binding
        if (   (ac instanceof RestSkeletonContentAttributes)
            || (ac instanceof RestStubContentAttributes) ) {
          entries.add( new DefaultEntry(FcExplorer.getPrefixedName(boundInterface),
                       new RestBindingWrapper(boundInterface)) );
        }
        // Json RPC binding
        if (   (ac instanceof JsonRpcSkeletonContentAttributes)
            || (ac instanceof JsonRpcStubContentAttributes) ) {
          entries.add( new DefaultEntry(FcExplorer.getPrefixedName(boundInterface),
                       new JsonRpcBindingWrapper(boundInterface)) );
        }
        // RMI binding
        /* TODO: the rmi connector in the BF should be refactored to provide the AC on the composite
        if (   (ac instanceof RmiStubAttributes)
            || (ac instanceof RmiSkeletonAttributes) ) {
          entries.add( new DefaultEntry(FcExplorer.getPrefixedName(boundInterface),
              new RmiBindingWrapper(boundInterface)) );
        } */         
      } catch (NoSuchInterfaceException e1) {
        // Nothing to do
          
        // RMI specific case
        try {
            String name = Fractal.getNameController(boundInterfaceOwner).getFcName();
            if ( name.contains("rmi-stub") ) {
                entries.add( new DefaultEntry(FcExplorer.getPrefixedName(boundInterface),
                             new RmiBindingWrapper(boundInterface)) );
            }
        } catch (NoSuchInterfaceException e2) {
            // Should not happen
        }          
      }
    }
  
    entries.addAll( getRmiBindingEntriesFromComponentController(owner, itf) );
    return entries;
  }
  
  /**
   * This method is used because the RMI binding is not bound on the service interface
   * but on the component interface of the business component.
   * 
   * @param owner The component owning the interface to search bindings on.
   * @param itf The interface to search bindings on.
   * @return a List with an entry for each binding.
   */
  private List<Entry> getRmiBindingEntriesFromComponentController(Component owner, Interface itf)
  {
    List<Entry> entries = new ArrayList<Entry>();
    
    try {
        Component[] parents = FcExplorer.getSuperController(owner).getFcSuperComponents();

        for (Component c : parents) {
           ContentController cc = FcExplorer.getContentController(c);
           Component[] children = cc.getFcSubComponents();
           
           for (Component child : children) {
               if ( FcExplorer.getName(child).contains("-rmi-skeleton") ) {
                   // check that the rmi skeleton is bound to owner
                   BindingController bc = FcExplorer.getBindingController(child);
                   Interface delegate = (Interface) bc.lookupFc(AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME);
                   String delegateName = FcExplorer.getName(delegate.getFcItfOwner());
                   
                   if (delegateName.equals( FcExplorer.getName(owner) )) {
                       Interface delegateItf = (Interface) child.getFcInterface(AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME);
                       entries.add( new DefaultEntry(FcExplorer.getName(child) + "." + AbstractSkeleton.DELEGATE_CLIENT_ITF_NAME,
                                    new RmiBindingWrapper(delegateItf)) );
                   }
               }
           }
        }
    } catch (NoSuchInterfaceException e) {
        e.printStackTrace();
    }
    
    return entries;
  }
}
