/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.explorer.icon;

import javax.swing.Icon;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;

/**
 * Provides a specific icon depending on the status of the component.
 * 
 * @author Christophe Demarey.
 */
public class ComponentIconProvider
     extends IconProvider
{
    /** The singleton */
    private static ComponentIconProvider singleton = null;
    
    //==================================================================
    //
    // Constructors.
    //
    //==================================================================

    /** Default constructor. */
    public ComponentIconProvider()
    {
        super();
        loadSca("Primitive");
        loadSca("Composite");
        singleton = this;
    }

    //==================================================================
    //
    // Internal method.
    //
    //==================================================================

    /**
     * Load a couple of icons based on the "sca-"+name+(Started|Stopped) pattern.
     * 
     * @param name the pattern to retrieve.
     */
    protected final void loadSca(String name)
    {
        getIcons().put(name,
                       new Icon[] {load("icons/sca"+name+"Started.png"),
                                   load("icons/sca"+name+"Stopped.png")});
    }

    /**
     * Provides the couple of icons for a type of component (Primitive or Composite).
     * @param cpt the component to introspect.
     * @return the couple of icons associated to the component.
     */
    protected final Icon[] loadIcon(Component cpt)
    {
        try {
            FcExplorer.getContentController(cpt);
            return (Icon[]) getIcons().get("Composite");
        } catch (NoSuchInterfaceException e) {
            // Nothing to do.
        }
        return (Icon[]) getIcons().get("Primitive");
    }

    //==================================================================
    //
    // Public methods for IconProvider interface.
    //
    //==================================================================

    /**
     * Provides the appropriate icon.
     * @param object the component to represent.
     * @return the associated icon.
     */
    public final Object newIcon(Object object)
    {
        Component ci = (Component)object;
        Icon[] lccIcons = loadIcon(ci);
        
        if (lccIcons == null) {
          return null;
        }
        LifeCycleController lcc=null;
        try {
            lcc = FcExplorer.getLifeCycleController(ci);
        } catch (NoSuchInterfaceException e) {
            return lccIcons[0];
        }
        
        String status = lcc.getFcState();
        if (status.equals(LifeCycleController.STARTED)) {
            return lccIcons[0];
        } else if (status.equals(LifeCycleController.STOPPED)) {
            return lccIcons[1];
        } else {
            return null;
        }
    }

    //==================================================================
    //
    // Public methods
    //
    //==================================================================

    public static ComponentIconProvider getInstance()
    {
        if (singleton == null) {
            singleton = new ComponentIconProvider();
        }
        return singleton;
    }
}
