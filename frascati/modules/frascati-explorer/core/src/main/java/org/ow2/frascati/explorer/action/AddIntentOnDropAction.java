/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * This class provides the ability dynamically add an intent 
 * on an SCA service (or an SCA reference) via a DropAction
 * {@link DropAction}.
 *
 * @author Christophe Demarey
 */
public class AddIntentOnDropAction
  implements DropAction
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  private static final Logger LOG = Logger.getLogger("org.ow2.frascati.explorer.action.AddIntentOnDropAction"); 

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The default constructor.
   */
  public AddIntentOnDropAction()
  {
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Implementation of the DropAction interface
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.DropAction#execute(DropTreeView)
   */
  public final void execute(DropTreeView dropTreeView) throws Exception
  {
      if (dropTreeView != null) {
          Component intent = null,
                    owner = null;
          Interface itf = null;
          SCABasicIntentController ic = null;
          String intentName = null;
          
          try {
              intent = (Component) dropTreeView.getDragSourceObject();
          } catch (ClassCastException cce) {
              JOptionPane.showMessageDialog(null, "An intent must be an SCA component! source object is "
                      +dropTreeView.getDragSourceObject().getClass());
              return;
          }
          try {
              // SCA components
              owner = (Component)  dropTreeView.getSelectedObject();
          } catch (ClassCastException cce) {
        	  // SCA services & references
              try {
                  itf = (Interface) dropTreeView.getSelectedObject();
                  owner = itf.getFcItfOwner();
              } catch (ClassCastException cce2) {
                  JOptionPane.showMessageDialog(null, "An intent can only be applied on components, services and references!");
                  return;
              }
          }
          
          // Get intent controller
          try {
            ic = (SCABasicIntentController) owner.getFcInterface(SCABasicIntentController.NAME);
          } catch (NoSuchInterfaceException nsie) {
              JOptionPane.showMessageDialog(null, "Cannot access to intent controller");
              LOG.log(Level.SEVERE, "Cannot access to intent controller", nsie);
              return;
          }
    
          if (intent != null) {
              String itfName = null;
              
              try {
                intentName = Fractal.getNameController(intent).getFcName();
              } catch (NoSuchInterfaceException nsie) {
                  LOG.log(Level.SEVERE, "Cannot find the Name Controller!", nsie);
              }
        
              try {
                  // Stop owner component
                  Fractal.getLifeCycleController(owner).stopFc();
                  // Get intent handler
                  IntentHandler h = TinfiDomain.getService(intent, IntentHandler.class, "intent");
                  // Add intent
                  if ( itf != null ) {
                      itfName = itf.getFcItfName();
                      ic.addFcIntentHandler(h, itfName);
                  } else {
                      ic.addFcIntentHandler(h);
                  }
                  // Start owner component
                  Fractal.getLifeCycleController(owner).startFc();
              } catch (Exception e1) {
                  String errorMsg = (itf != null) ? "interface " + itfName : "component"; 
                  JOptionPane.showMessageDialog(null,"Intent '" + intentName
                          + "' cannot be added to " + errorMsg);
                  LOG.log(Level.SEVERE, "Intent '" + intentName
                          + "' cannot be added to " + errorMsg, e1);
              }
          }
      }
  }

}
