/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.context;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.explorer.FcExplorer;

import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

import org.ow2.frascati.assembly.factory.api.CompositeManager;

/**
 * The FraSCAti Explorer Context for an SCA Domain.
 * (i.e. provides composites hosted by the domain) 
 * 
 * @author Christophe Demarey
 */
public class DomainContext 
  implements Context
{
  // ==================================================================
  //
  // Internal state.
  //
  // ==================================================================
  
  private static final long serialVersionUID = 1988894854864485303L;
  
  // ==================================================================
  //
  // Constructors.
  //
  // ==================================================================
  
  // ==================================================================
  //
  // Internal methods.
  //
  // ==================================================================

  // ==================================================================
  //
  // Public methods for interface Context.
  //
  // ==================================================================

  /**
   * Get children of the Assembly Factory.
   * Note : this implementation manages only one Factory.
   * 
   * @see org.objectweb.util.explorer.api.Context#getEntries(Object)
   */
  public final Entry[] getEntries(Object object)
  {
    CompositeManager domain = (CompositeManager) object;
    Component[] components = domain.getComposites();
    List<Entry> entries = new ArrayList<Entry>();
  
    for (Component comp : components) {
      entries.add( new DefaultEntry(FcExplorer.getName(comp), comp) );
    }
    return entries.toArray(new Entry[entries.size()]);
  }

}
