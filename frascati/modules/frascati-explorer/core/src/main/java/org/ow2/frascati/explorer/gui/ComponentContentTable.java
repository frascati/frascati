/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.gui;

import java.awt.GridLayout;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.explorer.context.ComponentContext;
import org.ow2.frascati.explorer.icon.ComponentIconProvider;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

public class ComponentContentTable
     extends AbstractSelectionPanel<Component>
{
    // ==================================================================
    //
    // Internal states.
    //
    // ==================================================================

    // ==================================================================
    //
    // Constructors
    //
    // ==================================================================

    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================

    // ==================================================================
    //
    // Public methods for Panel interface.
    //
    // ==================================================================

    @Override
    public final void selected(TreeView treeView)
    {
        super.selected(treeView);

        // Layout
        JLabel title = new JLabel();

        try {
            SCAContentController contentCtl = (SCAContentController) getSelection().getFcInterface(SCAContentController.NAME);
            JTextField impl = new JTextField();
            
            this.setLayout( new GridLayout(2,1) );

            title.setText("Implementation class");
            add(title);
            
            impl.setText( contentCtl.getFcContent().getClass().getCanonicalName() );
            impl.setEditable(false);
            add(impl);
        } catch (NoSuchInterfaceException e) {
            // comp is an SCA composite
            title.setText("SCA components");
            add(title);
            
            this.setLayout( new BoxLayout(this, BoxLayout.PAGE_AXIS) );

            try {
                ContentController cc = FcExplorer.getContentController( getSelection() );
                Component[] components = cc.getFcSubComponents();
                
                for (Component c : components) {
                  try {
                    c.getFcInterface(ComponentContext.SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
                    
                    Icon icon = (Icon) ComponentIconProvider.getInstance().newIcon( getSelection() );
                    JLabel compLabel = new JLabel(FcExplorer.getName(c));
                    compLabel.setIcon(icon);
                    add(compLabel);
                  } catch (NoSuchInterfaceException e2) {
                    /* The sca-component-controller cannot be found!
                       This component is not a SCA component => Nothing to do! */
                  }
                }
            } catch (NoSuchInterfaceException e3) {
                // Nothing to do
            }
        } catch (ContentInstantiationException e) {
            e.printStackTrace();
        }
    }
}
