/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;

/**
 * This action allows to unbind an SCA reference.
 * @author Christophe Demarey
 */
public class UnwireAction
  implements MenuItem 
{
	//==================================================================
	//
	// No internal state.
	//
	//==================================================================

	//==================================================================
	//
	// No constructor.
	//
	//==================================================================

	//==================================================================
	//
	// No internal method.
	//
	//==================================================================

	//==================================================================
	//
	// Public methods for MenuItem interface.
	//
	//==================================================================

	/**
	 * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
	 */
	public final int getStatus(TreeView treeView)
	{
		boolean started = false;
		Interface ir = (Interface) treeView.getSelectedObject();
		Component component = ir.getFcItfOwner();
		try {
			LifeCycleController lcc = FcExplorer.getLifeCycleController(component);        
			String status = lcc.getFcState();
			if (status.equals(LifeCycleController.STARTED)) {
				started = true;
			}
		} catch (NoSuchInterfaceException e1) {
			e1.printStackTrace();
		}
		// Test if the component is bound
		Interface bindInterface = null;
		try {
			BindingController bc = FcExplorer.getBindingController(component);
			bindInterface = (Interface)bc.lookupFc(ir.getFcItfName());
		} catch(Exception e) {
			//System.err.println("GetStatus Error : " + e.getMessage());
			return MenuItem.NOT_VISIBLE_STATUS;
		}
		if(bindInterface != null && !started) { // Bound and not started
			return MenuItem.ENABLED_STATUS;
		} else if (bindInterface != null && started) { // Bound but started
			return MenuItem.DISABLED_STATUS;
		} else if (bindInterface == null) { // Not bound
			return MenuItem.NOT_VISIBLE_STATUS;
		}
		return MenuItem.DISABLED_STATUS;
	}

	/**
	 * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
	 */
	public final void actionPerformed(MenuItemTreeView e) throws Exception
	{
		Interface ir = (Interface) e.getSelectedObject();
		Component ci = ir.getFcItfOwner();
		BindingController bc = FcExplorer.getBindingController(ci);
		bc.unbindFc(ir.getFcItfName());

		StringBuffer message = new StringBuffer();
		message.append("\"" + ir.getFcItfName() + "\" reference has been successfully unbound !\n");
		Type t = ci.getFcType();
		if(ComponentType.class.isAssignableFrom(t.getClass())) {
			InterfaceType it = (InterfaceType) ir.getFcItfType();
			if(!it.isFcOptionalItf()&&!it.isFcCollectionItf()) {
				message.append("This service is mandatory, so, rebind it before starting the component again.");
			}
		}
		JOptionPane.showMessageDialog(null,message.toString(),"Unbind success",JOptionPane.INFORMATION_MESSAGE);
	}   
}
