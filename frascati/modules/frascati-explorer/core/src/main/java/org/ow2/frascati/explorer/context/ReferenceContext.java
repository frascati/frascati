/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.context;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;

/**
 * Reference context used to specified SCA Bindings as childs of Reference nodes.
 *  
 * @author Christophe Demarey.
 */
public class ReferenceContext
     extends ScaInterfaceContext
{
  private static final long serialVersionUID = 5151766993889528466L;

  /**
   * Get explorer tree entries for an SCA reference
   * (i.e. the Java Interface object).
   * 
   * @param itf The interface to search bindings on.
   * @return a List with Java Interfaces entries.
   */
  protected final List<Entry> getJavaInterfacesEntries(Interface itf)
  {
    List<Entry> entries = new ArrayList<Entry>();
    InterfaceType type = (InterfaceType) itf.getFcItfType();
    
    if ( type.getFcItfName().endsWith("-controller") ) {
      return entries;
    }
    
    String signature = type.getFcItfSignature();
    // Search the signature in the super class of the reference instance.
    Class<?>[] itfs = itf.getClass().getSuperclass().getInterfaces();
    for (Class<?> c : itfs) {
      if ( signature.equals(c.getName()) ) {
        entries.add( new DefaultEntry(signature, c) );
      }
    }
    return entries;
  }
  
  // ==================================================================
  //
  // Public methods for interface Context.
  //
  // ==================================================================
  
  /**
   * Returns an array containing the entries contained by the target context.
   * @return A new array of entry.
   */
  public final Entry[] getEntries(Object object)
  {
    List<Entry> entries = new ArrayList<Entry>();    
    Interface itf = (Interface)object;
    
    String itfName = itf.getFcItfName();
    Component owner = itf.getFcItfOwner();

    // Add Java Interfaces
    entries.addAll( getJavaInterfacesEntries(itf) );

    // Add entries for SCA bindings
    entries.addAll( getBindingEntries(owner, itf) );
    
    // Add entries for SCA intents
    entries.addAll( getIntentEntries(owner, itfName) );

    return entries.toArray(new Entry[entries.size()]);
  }
}
