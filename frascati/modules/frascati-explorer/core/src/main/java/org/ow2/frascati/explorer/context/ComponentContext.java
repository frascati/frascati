/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */
package org.ow2.frascati.explorer.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.fractal.explorer.context.ClientCollectionInterfaceContainer;
import org.objectweb.fractal.explorer.context.ServerCollectionInterfaceContainer;
import org.objectweb.util.explorer.api.Context;
import org.objectweb.util.explorer.api.Entry;
import org.objectweb.util.explorer.core.common.api.ContextContainer;
import org.objectweb.util.explorer.core.naming.lib.DefaultEntry;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Component context for user role.
 *  
 * @author Christophe Demarey.
 */
public class ComponentContext
  implements Context 
{
	
	final public static String SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME = "sca-component-controller";

    // ==================================================================
    //
    // Internal state.
    //
    // ==================================================================
    
    private static final long serialVersionUID = 6420654128175193919L;
    
    // ==================================================================
    //
    // No Constructors.
    //
    // ==================================================================
    
    // ==================================================================
    //
    // Internal methods.
    //
    // ==================================================================

    /**
     * Get SCA properties entry for a given SCA component
     * 
     * @param comp The SCA component owning properties.
     * @return The list of property entries.
     */
    private List<Entry> getProperties(Component comp)
    {
      List<Entry> propEntries = new ArrayList<Entry>();
      Set<String> propNames = new TreeSet<String>();
      
      try {
          SCAPropertyController propCtl = (SCAPropertyController) comp.getFcInterface(SCAPropertyController.NAME);

          for (String propName: propCtl.getPropertyNames()) { 
              propEntries.add( new DefaultEntry(propName, propCtl) );
              propNames.add(propName);
          }
      } catch (NoSuchInterfaceException e) {
          e.printStackTrace();
      }
      
      return propEntries;
    }
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public List<Entry> getInterfaceEntries(Component component) {
        List<Entry> entries = new ArrayList<Entry>();
        Map<String, ContextContainer> collections = new HashMap<String, ContextContainer>();

        // Prepare the collection interfaces.
        ComponentType cType = (ComponentType) component.getFcType();
        InterfaceType[] itfTypes = cType.getFcInterfaceTypes();
        for (InterfaceType itfType: itfTypes) {
            if ( itfType.isFcCollectionItf() ) {
            	ContextContainer cc = null;
                if ( itfType.isFcClientItf() )
                    cc = new ClientCollectionInterfaceContainer(itfType, component);
                else
                    cc = new ServerCollectionInterfaceContainer(itfType, component);
                collections.put(itfType.getFcItfName(), cc);
            }
        }
        
        // Introspect the component interfaces.
        Object[] interfaces = component.getFcInterfaces();
        for (Object o : interfaces) {
            if (!component.equals(o)) {
                Interface itf = (Interface) o;
                
                if (!FcExplorer.isController(itf)) {
	                if (FcExplorer.isCollection(itf)) {                    
	                    InterfaceType iType = (InterfaceType) itf.getFcItfType();
	                    String name = FcExplorer.getName(itf);
	                    ((ContextContainer) collections.get(iType.getFcItfName())).addEntry(name, itf);
	                } else {
	                    entries.add(new DefaultEntry(FcExplorer.getName(itf), itf));
	                }
                }
            }
        }
                
        // Add the collection interfaces
        for (String key : collections.keySet()) {
            entries.add( new DefaultEntry(key, collections.get(key)) ); 
        }
        
        return entries;        
    }
    
    // ==================================================================
    //
    // Public methods for interface Context.
    //
    // ==================================================================
    
    /**
     * Returns an array containing the entries contained by the target context.
     * @return A new array of entry.
     */
    public Entry[] getEntries(Object object)
    {
      Component ci = (Component) object;
      List<Entry> entries = new ArrayList<Entry>();
      
      // TODO a component could have an implementation Java
      // see controller of content
      // A component could be an <implementation.spring> <implementation.fractal> <implementation.script>

      // TODO a component could have intent

      // ** For SCA composites
      try {
          ContentController cc = FcExplorer.getContentController(ci);
          Component[] components = cc.getFcSubComponents();
          
          for (Component c : components) {
            try {
              c.getFcInterface(SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
              entries.add( new DefaultEntry(FcExplorer.getName(c), c) );
            } catch (NoSuchInterfaceException e) {
              /* The sca-component-controller cannot be found!
                 This component is not a SCA component => Nothing to do! */
            }
          }
      } catch (NoSuchInterfaceException e) {
          // Nothing to do
      }
      
      // ** Display SCA Properties
      entries.addAll( getProperties(ci) );
      
      // ** Display SCA Services / References
      entries.addAll( getInterfaceEntries(ci) );

      return (Entry[])  entries.toArray(new Entry[0]);
    }
}
