/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy.
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.objectweb.util.trace;

/**
 * Specialization of the Java util trace mechanism to deal with 
 * the fractal explorer log mechanism.<BR>
 * <p>
 *   The TraceTemplate class provides implementation of the trace 
 *   interface based on the Java util log implementation.
 * </p>
 *
 * @author Romain Rouvoy
 * @version 0.1
 */
public interface Trace 
{
    /**
     * Displays a debug message
     *
     * @param msg - debugging message to print
     */
    void debug(String msg) ;

    /**
     * Displays a debug message
     *
     * @param msg - information message to print
     */
    void info(String msg) ;

    /**
     * Displays a debug message
     *
     * @param msg - warning message to print
     */
    void warn(String msg) ;

    /**
     * Displays a debug message
     *
     * @param msg - error message to print
     */
    void error(String msg) ;

    /**
     * Displays a debug message
     *
     * @param msg - fatal message to print
     */
    void fatal(String msg) ;
}
