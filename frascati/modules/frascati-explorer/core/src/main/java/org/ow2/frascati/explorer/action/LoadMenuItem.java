/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Damien Fournier
 *                 Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import de.schlichtherle.io.swing.JFileChooser;

import java.io.File;
import java.net.URL;
import javax.xml.namespace.QName;

import javax.swing.filechooser.FileFilter;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.explorer.ExplorerGUI;

/**
 * This class provides the ability to load SCA composites
 * via the Load Menu item {@link MenuItem}.
 *
 * @author Christophe Demarey, Philippe Merle
 */

public class LoadMenuItem
     extends AbstractAlwaysEnabledMenuItem<CompositeManager>
{
	// --------------------------------------------------------------------------
	// Internal state
	// --------------------------------------------------------------------------

	/** The {@link JFileChooser} instance. */
	private static JFileChooser fileChooser = null;

	// --------------------------------------------------------------------------
	// Internal methods
	// --------------------------------------------------------------------------

	// --------------------------------------------------------------------------
	// Constructor
	// --------------------------------------------------------------------------

	/**
	 * The default constructor.
	 */
	public LoadMenuItem()
	{
	}

	// --------------------------------------------------------------------------
	// AbstractAlwaysEnabledMenuItem
	// --------------------------------------------------------------------------

	/**
	 * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute()
	 */
	protected final void execute(final CompositeManager domain) throws Exception
	{
		// At the first call, creates the file chooser.
		if(fileChooser == null) {
			fileChooser = new JFileChooser();
			// Sets to the user's current directory.
			fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
			fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			// Filters SCA files.
			fileChooser.setFileFilter(            
					new FileFilter() {
						/** Whether the given file is accepted by this filter. */
						public final boolean accept(File f)
						{
							String extension = f.getName().substring(f.getName().lastIndexOf('.') + 1);
							return extension.equalsIgnoreCase("composite") || extension.equalsIgnoreCase("zip") || f.isDirectory();
						}
						/** The description of this filter. */
						public final String getDescription()
						{
							return "SCA .composite or .zip files";
						}
					}
			);
		}

		// Opens the file chooser to select a file.
		int returnVal = fileChooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			String selectedFilename = selectedFile.toString(); 
			int index = selectedFilename.indexOf(".jar");

			if ( index != -1 ) { // File in a jar
				// Compute the jar and the composite filenames.
				String jarFilename = selectedFilename.substring(0, index+4);
				String compositeName = selectedFilename.substring(index + 5);
				URL urlJarFile = new File(jarFilename).toURI().toURL();

				try{
					//Is FraSCAti running with OSGi
					Class<?> frascatiServiceClass = null;
					Object service = null;
					//Find the FraSCAtiOSGiService class
					frascatiServiceClass = Class.forName("org.ow2.frascati.osgi.api.FraSCAtiOSGiService");
					//Find the FraSCAtiOSGiService instance
					service = ((ExplorerGUI)SINGLETON.get()).getFraSCAtiExplorerService(frascatiServiceClass);
					//use the service to process the composite
					frascatiServiceClass.getDeclaredMethod("loadSCA", 
							new Class<?>[]{URL.class,String.class}).invoke(service,
									new Object[]{urlJarFile,compositeName});
					return;
				} catch(ClassNotFoundException e){
					//do nothing
				} catch(Exception e){
					//do nothing
				}
				
				// Create a FraSCAti Assembly Factory processing context.
		        ProcessingContext processingContext = domain.newProcessingContext(urlJarFile);

		        // Process the SCA composite.
		        domain.processComposite(new QName(compositeName), processingContext);

		        // Update the FraSCAti Explorer class loader with the processing context class loader.
		        ((ExplorerGUI)SINGLETON.get()).updateClassLoader(processingContext.getClassLoader());

			} else {
				index = selectedFilename.indexOf(".zip");
				if ( index != -1 ) { // SCA contribution file

					// Create a FraSCAti Assembly Factory processing context.
                    ProcessingContext processingContext = domain.newProcessingContext();

                    // Process the ZIP contribution.
                    domain.processContribution(selectedFilename, processingContext);

    		        // Update the FraSCAti Explorer class loader with the processing context class loader.
                    ((ExplorerGUI)SINGLETON.get()).updateClassLoader(processingContext.getClassLoader());

				} else { // SCA composite file
                  domain.getComposite(selectedFile.getPath());
				}
			}
		}
	}

}
