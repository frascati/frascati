/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

// import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.DropAction;
import org.objectweb.util.explorer.api.DropTreeView;

/**
 * This class provides the ability dynamically add an SCA component 
 * into an SCA composite via a DropAction
 * {@link DropAction}.
 *
 * @author Christophe Demarey
 */
public class AddComponentOnDropAction
  implements DropAction
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

//  public static final Logger log = Logger.getLogger("org.ow2.frascati.explorer.action.AddComponentOnDropAction"); 

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The default constructor.
   */
  public AddComponentOnDropAction()
  {
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Implementation of the DropAction interface
  // --------------------------------------------------------------------------

  /**
   * @see org.objectweb.util.explorer.api.DropAction#execute(DropTreeView)
   */
  public final void execute(DropTreeView dropTreeView) throws Exception
  {
      if (dropTreeView != null) {
          Component src = null,
                    dest = null;
          ContentController contentCtl = null;

          // Check source component
          try {
              src = (Component) dropTreeView.getDragSourceObject();
          } catch (ClassCastException cce) {
        	  // Should not happen
              JOptionPane.showMessageDialog(null, "Can only add an SCA component! source object is "
                      +dropTreeView.getDragSourceObject().getClass());
              return;
          }
          
          // Check destination composite
          try {
              dest = (Component)  dropTreeView.getSelectedObject();
              contentCtl = Fractal.getContentController(dest);
          } catch (ClassCastException cce) {
                  JOptionPane.showMessageDialog(null, "Can only add an SCA component into an SCA composite!");
                  return;
          } catch (NoSuchInterfaceException nsie) {
              JOptionPane.showMessageDialog(null, "Can only add an SCA component into an SCA composite!");
              return;
          }
          
          // Add component
          contentCtl.addFcSubComponent(src);
      }
  }

}
