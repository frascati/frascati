/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Damien Fournier
 *                 Philippe Merle
 *
 */
package org.ow2.frascati.explorer.action;

import java.io.File;
import java.net.URL;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.ow2.frascati.explorer.ExplorerGUI;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;

/**
 * Artifact used to manage the "Add to classpath" action.
 * This action adds URLs to the FraSCAti classpath (used to load SCA composites).
 * 
 * @author Christophe Demarey.
 */
public class AddToClasspathAction
     extends AbstractAlwaysEnabledMenuItem<CompositeManager>
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  /** The {@link JFileChooser} instance. */
  private static JFileChooser fileChooser = null;

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The default constructor.
   */
  public AddToClasspathAction()
  {
  }

  // --------------------------------------------------------------------------
  // For AbstractAlwaysEnabledMenuItem
  // --------------------------------------------------------------------------

  /**
   * Add a jar / folder to the classpath (will be used to find SCA components implementation).
   * 
   * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute()
   */
  protected final void execute(final CompositeManager compositeManager) throws Exception
  {
    // At the first call, creates the file chooser.
    if (fileChooser == null) {
      fileChooser = new JFileChooser();
      // Sets to the user's current directory.
      fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
      // Filters JAR files.
      fileChooser.setFileFilter(            
        new FileFilter() {
          /** Whether the given file is accepted by this filter. */
          public final boolean accept(File f)
          {
            String extension = f.getName().substring(f.getName().lastIndexOf('.') + 1);
            return extension.equalsIgnoreCase("jar") || f.isDirectory();
          }
          /** The description of this filter. */
          public final String getDescription()
          {
            return "JAR files / Class Folder";
          }
        }
      );
    }
    
    // Opens the file chooser to select a file.
    int returnVal = fileChooser.showOpenDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File selectedFile = fileChooser.getSelectedFile();
      URL url = selectedFile.toURI().toURL();

      // Add the selected file to the FraSCAti class loader.
      getFraSCAtiExplorerService(ClassLoaderManager.class).loadLibraries(url);
      // Update the FraSCAti Explorer configuration.
      ((ExplorerGUI)SINGLETON.get()).addExplorerConfiguration(url);
    }
  }
}
