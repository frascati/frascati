/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.explorer.icon;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.mortbay.log.Log;

import org.ow2.frascati.explorer.FrascatiExplorerClassResolver;

/**
 * 
 * @author Romain rouvoy, Christophe Demarey.
 */
public abstract class IconProvider
           implements org.objectweb.util.explorer.api.IconProvider
{
  // ==================================================================
  //
  // Internal states.
  //
  // ==================================================================

  /** The different icons. */
  private Map<String, Icon[]> icons;
  
  // ==================================================================
  //
  // Constructor.
  //
  // ==================================================================

  /**
   * Default Icon provider constructor.
   */
  protected IconProvider()
  {
    setIcons(new HashMap<String, Icon[]>());
  }

  // ==================================================================
  //
  // Private methods.
  //
  // ==================================================================

  /**
   * @return Returns the icons.
   */
  protected final Map<String, Icon[]> getIcons()
  {
    return icons;
  }
  
  /**
   * @param icons The icons to set.
   */
  protected final void setIcons(Map<String, Icon[]> icons)
  {
    this.icons = icons;
  }

  /**
   * Load a icon from the classpath.
   * 
   * @param path the path for finding the requested icon.
   * @return the instance of icon found (null if not found).
   */
  protected final Icon load(String path)
  {
    Icon icon = null;
    URL image = FrascatiExplorerClassResolver.getResource(path);
    
    if (image != null) {
      icon = new ImageIcon(image);
    }
    if ((image == null) || (icon == null)) {
      Log.warn(path+" icon not found!");
    }
    return icon ;
  }
}
