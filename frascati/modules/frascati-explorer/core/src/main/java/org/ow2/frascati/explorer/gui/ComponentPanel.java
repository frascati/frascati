/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.explorer.gui;

import java.awt.Color;

import javax.swing.Icon;
import javax.swing.JLabel;

import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.panel.TitlePanel;
import org.ow2.frascati.explorer.icon.ComponentIconProvider;

public class ComponentPanel 
     extends TitlePanel
{

    /**
     * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
     */
    public final void selected(TreeView treeview)
    {
        JLabel title = new JLabel(getTitle(treeview));
        Icon icon = (Icon) ComponentIconProvider.getInstance().newIcon( treeview.getSelectedObject() );

        title.setIcon( icon );
        setBackground(Color.white);
        add(title);
    }

    @Override
    public final String getTitle(TreeView treeView)
    {
        return (String) treeView.getSelectedEntry().getName();
    }

}
