/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.objectweb.util.trace;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Specialization of the Java util trace mechanism to deal with 
 * the fractal explorer log mechanism.<BR>
 * <p>
 *   The TraceTemplate class provides implementation of the trace 
 *   interface based on the Java util log implementation.
 * </p>
 *
 * @author Christophe Demarey
 */
public final class TraceSystem
{
    /**
     * Can not be instantiated.
     */
    private TraceSystem()
    {
    }

    /** prefix used to identify Launcher loggers */
    private static final String PREFIX = "org.ow2.frascati.explorer.trace";

    /** list of current logger instancied */
    private static Map<String,TraceTemplate> list = new HashMap<String,TraceTemplate>();

    /**
     * Creates a new logger identified by logger value
     *
     * @param logger - the name of the logger
     */
    protected static void create(String logger)
    {        
        list.put(logger,
                  new TraceTemplate(Logger.getLogger(PREFIX+"."+logger)));
    }

    /**
     * Retrieve the instance of a logger
     *
     * @param id - the identifier of the logger
     */
    public static Trace get(String id)
    {
        if (!list.containsKey(id)) {
            create(id);
        }
        return (Trace) list.get(id); 
    }

    // Added because EasyBPEL Explorer requires this method.
    public static void setLevel(String level)
    {
      // TODO: Implement something?
    }

}
