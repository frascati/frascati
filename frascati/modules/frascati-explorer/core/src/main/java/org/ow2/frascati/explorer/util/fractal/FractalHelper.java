/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.explorer.util.fractal;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.Util;
import org.objectweb.fractal.util.Fractal;

/**
 * Helper functions on top of Fractal API.
 *
 * TODO: Merge this class with the 'fractalutil' module.
 *
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 */

public class FractalHelper
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * Does not need to be instantiated.
   */
  protected
  FractalHelper ()
  {
  }

  // --------------------------------------------------------------------------
  // Public methods
  // --------------------------------------------------------------------------

  /**
   * Is a public name (i.e., not starting with "internal-").
   *
   * @param name the name.
   *
   * @return true if the name does not start with "internal-" else false.
   */
  public static boolean
  isPublicName (final String name)
  {
    return !name.startsWith("internal-");
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal Interface interface
  // --------------------------------------------------------------------------

  /**
   * Is a Fractal controller {@link Interface}.
   *
   * @param itf the given Fractal {@link Interface} instance.
   *
   * @return true if it is a Fractal controller {@link Interface} else false.
   */
  public static boolean
  isControllerInterface (final Interface itf)
  {
    String itfName = itf.getFcItfName();
    return (itfName.endsWith("-controller") || itfName.equals("factory") || itfName.equals("component"));
  }

  /**
   * Is a Fractal server {@link Interface} (i.e., not a controller or a client interface).
   *
   * @param itf the given Fractal {@link Interface} instance.
   *
   * @return true if it is a Fractal server interface else false.
   */
  public static boolean
  isServerInterface (final Interface itf)
  {
    // Returns true if itf is a server interface and not a controller interface.
    return !(((InterfaceType)itf.getFcItfType()).isFcClientItf()) && !isControllerInterface(itf);
  }

  /**
   * Is a public Fractal {@link Interface} (i.e., not a controller or a client interface).
   *
   * @param itf the given Fractal {@link Interface} instance.
   *
   * @return true if it is a public interface else false.
   */
  public static boolean
  isPublicInterface (final Interface itf)
  {
    return isServerInterface(itf) && isPublicName(itf.getFcItfName());
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal Component interface
  // --------------------------------------------------------------------------

  /**
   * Gets a Fractal {@link Interface} instance for a given name.
   *
   * @param component the Fractal {@link Component} instance.
   * @param name the name of the {@link Interface} instance.
   *
   * @return the Fractal {@link Interface} instance for the given name or null.
   */
  public static Object
  getInterface (
    final Component component,
    final String name
  ) {
    try {
      return component.getFcInterface(name);
    } catch (NoSuchInterfaceException e) {
      return null;
    }
  }

  /**
   * Obtains the full path name of a given {@link Component} instance.
   *
   * @param component the given {@link Component} instance.
   *
   * @return the full path name of a given {@link Component} instance.
   */
  public static String
  toString (final Component component)
  {
    StringBuffer buffer = new StringBuffer();
    // Reuse this Julia util function.
    Util.toString(component, buffer);
    final String tmp = buffer.toString();
    // Removes the first path produced by Util.toString()
    // WARNING: This is specific to the FDF Explorer!!!
    return tmp.substring(tmp.indexOf('/',1)); 
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal LifeCycleController interface
  // --------------------------------------------------------------------------

  /**
   * Obtain the {@link LifeCycleController} interface
   * of a given {@link Component} instance.
   *
   * @param component the given {@link Component} instance.
   *
   * @return the {@link LifeCycleController} interface.
   */
  public static LifeCycleController
  getLifeCycleController (final Component component)
  throws NoSuchInterfaceException
  {
    return Fractal.getLifeCycleController(component);
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal NameController interface
  // --------------------------------------------------------------------------

  /**
   * Gets the name of the Fractal {@link Component} owning a given {@link Object} instance.
   *
   * @param itf the given {@link Object} instance.
   *
   * @return the name of the Fractal {@link Component} owning the given {@link Object} instance.
   */
  public static String
  getComponentName (final Object itf)
  {
    return getComponentName(((Interface)itf).getFcItfOwner());
  }

  /**
   * Gets the name of the Fractal {@link Component} owning a given {@link Interface} instance.
   *
   * @param itf the given {@link Interface} instance.
   *
   * @return the name of the Fractal {@link Component} owning the given {@link Interface} instance.
   */
  public static String
  getComponentName (final Interface itf)
  {
    return getComponentName(itf.getFcItfOwner());
  }

  /**
   * Gets the name of a given Fractal {@link Component} instance.
   *
   * @param component the given Fractal {@link Component} instance.
   *
   * @return the name of the given Fractal {@link Component} instance.
   */
  public static String
  getComponentName (final Component component)
  {
    try {
      NameController nc = Fractal.getNameController(component);
      return nc.getFcName();
    } catch (NoSuchInterfaceException e) {
      return "unknown";
    }
  }

  /**
   * Gets a Fractal {@link Component} instance by a given name.
   *
   * @param components an array of Fractal {@link Component} instances.
   * @param name the name of the component.
   *
   * @return the Fractal {@link Component} instance with the given name or null.
   */
  public static Component
  getComponentByName (
    final Component[] components,
    final String name
  ) {
    for(int i=0; i<components.length; i++) {
      Component c = components[i];
      if(getComponentName(c).equals(name)) {
        return c;
      }
    }
    return null;
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal SuperController interface
  // --------------------------------------------------------------------------

  /**
   * Gets the super components of a given Fractal {@link Component} instance.
   *
   * @param component the given Fractal {@link Component} instance.
   *
   * @return the super components of the given Fractal {@link Component} instance.
   */
  public static Component[]
  getSuperComponents (final Component component)
  {
    try {
      SuperController sc = Fractal.getSuperController(component);
      return sc.getFcSuperComponents();
    } catch (NoSuchInterfaceException e) {
      return new Component[0];
    }
  }

  /**
   * Gets the last super component of a given Fractal {@link Component} instance.
   *
   * @param component the given Fractal {@link Component} instance.
   *
   * @return the last super component of the given Fractal {@link Component} instance.
   */
  public static Component
  getLastSuperComponent (final Component component)
  {
    Component[] superComponents = getSuperComponents(component);
    return superComponents[superComponents.length-1];
  }

  // --------------------------------------------------------------------------
  // Public methods related to the Fractal ContentController interface
  // --------------------------------------------------------------------------

  /**
   * Gets the sub components of a given Fractal {@link Component} instance.
   *
   * @param component the given Fractal {@link Component} instance.
   *
   * @return the sub components of the given Fractal {@link Component} instance.
   */
  public static Component[]
  getSubComponents (final Component component)
  {
    try {
      ContentController cc = Fractal.getContentController(component);
      return cc.getFcSubComponents();
    } catch (NoSuchInterfaceException e) {
      return new Component[0];
    }
  }

  /**
   * Gets a sub component of a given Fractal {@link Component} instance.
   *
   * @param component the given Fractal {@link Component} instance.
   * @param name the name of the sub component.
   *
   * @return the sub component of the given Fractal {@link Component} instance.
   */
  public static Component
  getSubComponentByName (
    final Component component,
    final String name
  ) {
    return getComponentByName(getSubComponents(component), name);
  }
}
