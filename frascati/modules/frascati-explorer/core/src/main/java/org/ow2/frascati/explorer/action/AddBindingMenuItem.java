/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.rmi.RmiRegistryCreationException;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.MenuItemTreeView;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.explorer.gui.NewBindingForm;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;

/**
 * This class provides the ability dynamically add an SCA binding 
 * to an SCA service (or an SCA reference) via the "Add binding"
 * Menu item {@link MenuItem}.
 *
 * @author Christophe Demarey
 */
public class AddBindingMenuItem
     extends AbstractAlwaysEnabledMenuItem<Object>
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

    private static final Logger LOG = Logger.getLogger("org.ow2.frascati.explorer.action.AddBindingMenuItem");
    
    /** Classloader constant for binding factory */
    private static final String CLASSLOADER = "classloader";
    
    /** The interface to add binding on. */
    private String itfName = null;
    
    /** the component owning the interface to add binding on. */
    private Component owner = null;
    
    /** Is the interface to bind an SCA reference? */
    private boolean isScaReference = false;
    
    // --------------------------------------------------------------------------
    // Internal methods
    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------

    /**
     * The default constructor.
     */
    public AddBindingMenuItem ()
    {
    }

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    /**
     * Export an SCA service or Unbind an SCA reference with the Binding Factory.
     * Callback method used by the NewBindingForm GUI to create the binding.
     * 
     * @param hints A map with entries needed by the Binding Factory.
     */
    public final void createBinding(Map<String, Object> hints)
    {
        LifeCycleController lcc = null;
        
        // Set the class loader
        hints.put(CLASSLOADER, getFraSCAtiExplorerService(ClassLoaderManager.class).getClassLoader());

        try {
            lcc = Fractal.getLifeCycleController(owner);
            lcc.stopFc();
        } catch (NoSuchInterfaceException e) {
            LOG.severe("Error while getting component life cycle controller");
            return;
        } catch (IllegalLifeCycleException e) {
            LOG.severe("Cannot stop the component!");
            e.printStackTrace();
            return;
        }
        
        try {
            LOG.fine("Calling binding factory\n bind: "
                    + Fractal.getNameController(owner).getFcName() + " -> "
                    + itfName);
            // Get the FraSCAti Explorer Binding Factory service.
            BindingFactory bindingFactory = getFraSCAtiExplorerService(BindingFactory.class);
            if( isScaReference ) {
              bindingFactory.bind(owner, itfName, hints);
            } else {
              bindingFactory.export(owner, itfName, hints);
            }
        } catch (BindingFactoryException bfe) {
            LOG.severe("Error while binding "
                       + (isScaReference ? "reference" : "service") 
                       + ": " + itfName);
            bfe.printStackTrace();
            return;
        } catch (NoSuchInterfaceException nsie) {
            LOG.severe("Error while getting component name controller");
            return;
        } catch (RmiRegistryCreationException rmie) {
            String msg = rmie.getMessage() + "\nAddress already in use?";
            JOptionPane.showMessageDialog(null, msg);
            LOG.log(Level.SEVERE, msg); //, rmie);
            return;
        }
        
        try {
            lcc.startFc();
        } catch (IllegalLifeCycleException e) {
            LOG.severe("Cannot start the component!");
            e.printStackTrace();
        }
    }

    // --------------------------------------------------------------------------
    // Implementation of the MenuItem interface
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute(Object, MenuItemTreeView)
     */
    @Override
    protected final void execute(Object selected, final MenuItemTreeView e)
    {
    	if (selected instanceof TinfiComponentOutInterface<?>) { // SCA reference
            this.owner = ((Interface) selected).getFcItfOwner();    		
            this.isScaReference = true;
    	} else { // SCA service
            this.owner = ((Interface) selected).getFcItfOwner();    		
    	}
        this.itfName = (String) e.getSelectedEntry().getName();

        // Call the GUI to fill in binding parameters.
        new NewBindingForm(this, isScaReference);
    }

}
