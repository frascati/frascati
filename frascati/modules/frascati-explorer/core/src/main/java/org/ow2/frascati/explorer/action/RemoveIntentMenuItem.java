/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.explorer.context.ClientInterfaceWrapper;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.ow2.frascati.explorer.context.IntentWrapper;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * This class provides the ability dynamically remove an intent 
 * from an SCA service (or an SCA reference) via the "Remove intent"
 * Menu item {@link MenuItem}.
 *
 * @author Christophe Demarey
 */
public class RemoveIntentMenuItem
     extends AbstractAlwaysEnabledMenuItem<IntentWrapper>
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------

  private static final Logger LOG = Logger.getLogger("org.ow2.frascati.explorer.action.RemoveIntentMenuItem"); 

  // --------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------

  /**
   * The default constructor.
   */
  public RemoveIntentMenuItem()
  {
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  // --------------------------------------------------------------------------
  // Implementation of the MenuItem interface
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute(Object, MenuItemTreeView)
   */
  @Override
  protected final void execute(IntentWrapper selected)
  {
      // Need to implement this method but useless!
  }
  
  /**
   * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute(Object, MenuItemTreeView)
   */
  @Override
  protected final void execute(IntentWrapper selected, final MenuItemTreeView tree)
  {
      Component intent = selected.getIntent(),
                itfOwner = null;
      Object parent = tree.getParentEntry().getValue();
      Interface itf = null;
      SCABasicIntentController ic = null;
      
      // Get intent interface
      try {
          itf = (Interface) parent;
      } catch (ClassCastException cce) {
          try {
              itf = ((ClientInterfaceWrapper) parent).getItf();
          } catch (ClassCastException cce2) {
              cce2.printStackTrace();
          }
      }

      itfOwner = itf.getFcItfOwner();
      
      // Get intent controller
      try {
          ic = (SCABasicIntentController) itf.getFcItfOwner().getFcInterface(SCABasicIntentController.NAME);
      } catch (NoSuchInterfaceException nsie) {
          JOptionPane.showMessageDialog(null, "Cannot access to intent controller");
          LOG.log(Level.SEVERE, "Cannot access to intent controller", nsie);
      }
      
      // Remove intent
      try {
          // Stop owner component
          Fractal.getLifeCycleController(itfOwner).stopFc();
          // Get intent handler
          IntentHandler h = TinfiDomain.getService(intent, IntentHandler.class, "intent");
          // remove intent
          ic.removeFcIntentHandler(h, itf.getFcItfName());
          // Start owner component
          Fractal.getLifeCycleController(itfOwner).startFc();
      } catch (NoSuchInterfaceException nsie) {
          JOptionPane.showMessageDialog(null, "Cannot retrieve the interface name!");
          LOG.log(Level.SEVERE, "Cannot retrieve the interface name!", nsie);
      } catch (IllegalLifeCycleException ilce) {
          JOptionPane.showMessageDialog(null, "Illegal life cycle Exception!");
          LOG.log(Level.SEVERE, "Illegal life cycle Exception!", ilce);
      }
  }

}
