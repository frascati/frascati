/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import java.awt.Dimension;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.explorer.FcExplorer;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.objectweb.util.explorer.swing.gui.api.DialogAction;
import org.objectweb.util.explorer.swing.gui.api.DialogBox;
import org.objectweb.util.explorer.swing.gui.lib.DefaultDialogBox;
import org.objectweb.util.explorer.swing.gui.lib.TreeBox;

/**
 * This action allows to wire an SCA reference to an SCA service.
 * @author Christophe Demarey
 */
public class WireAction 
  implements MenuItem, 
             DialogAction 
{
    //==================================================================
    //
    // Internal states.
    //
    //==================================================================
    
    /** The TreeBox used. */
    private TreeBox treeBox = null;
    
    /** The InterfaceReference on which the interface will be bound. */
    private Interface reference = null;
    
    //==================================================================
    //
    // No constructor.
    //
    //==================================================================
    
    //==================================================================
    //
    // No Internal method.
    //
    //==================================================================
    
    //==================================================================
    //
    // Public methods for MenuItem interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    public final int getStatus(TreeView treeView)
    {
        try {
            boolean started = false;
            Interface ir = (Interface) treeView.getSelectedObject();
            Component component = ir.getFcItfOwner();
            // Test if the component is started
            LifeCycleController lcc = FcExplorer.getLifeCycleController(component);        
            String status = lcc.getFcState();
            if (status.equals(LifeCycleController.STARTED)) {
                started = true;
            }
            // Test if the component is bound
            Interface bindInterface = null;
            try {
                BindingController bc = FcExplorer.getBindingController(component);
                bindInterface = (Interface)bc.lookupFc(ir.getFcItfName());
            } catch(Exception e) {
                //System.err.println("Error : " + e.getMessage()); 
                return MenuItem.NOT_VISIBLE_STATUS;
            }
            if(bindInterface == null && !started) {
                // Not bound and not started
                return MenuItem.ENABLED_STATUS;
            } else if (bindInterface == null && started) {
                // Not bound but started
                return MenuItem.DISABLED_STATUS;
            } else if (bindInterface != null){
                // Already bound
                return MenuItem.NOT_VISIBLE_STATUS;
            }
        } catch (NoSuchInterfaceException e) {
            e.printStackTrace();
        }
        return MenuItem.DISABLED_STATUS;
    }
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#actionPerformed(org.objectweb.util.explorer.api.MenuItemTreeView)
     */
    public final void actionPerformed(MenuItemTreeView e) throws Exception
    {
        reference = (Interface) e.getSelectedObject();               
        
        treeBox = new TreeBox(e.getTree().duplicate());
        treeBox.setPreferredSize(new Dimension(450, 350));
        
        DialogBox dialog = new DefaultDialogBox("Select the service to wire");
        dialog.setValidateAction(this);
        dialog.addElementBox(treeBox);
        dialog.show();        
    }
    
    //==================================================================
    //
    // Public methods for DialogAction interface.
    //
    //==================================================================
    
    /**
     * @see org.objectweb.util.explorer.swing.gui.api.DialogAction#executeAction()
     */
    public final void executeAction() throws Exception
    {
        Object o = treeBox.getObject();
        Interface service = (Interface) o;
        FcExplorer.getBindingController(reference.getFcItfOwner()).bindFc(reference.getFcItfName(),service);
    } 
    
}

