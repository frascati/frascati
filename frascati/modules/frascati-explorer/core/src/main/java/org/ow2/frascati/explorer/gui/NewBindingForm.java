/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcConnectorConstants;
import org.objectweb.fractal.bf.connectors.rest.RestConnectorConstants;
import org.objectweb.fractal.bf.connectors.rmi.RmiConnectorConstants;
import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.ow2.frascati.explorer.action.AddBindingMenuItem;

/**
 * Swing form used to create new SCA bindings dynamically.
 * 
 * @author Christophe Demarey
 */
public class NewBindingForm
     extends javax.swing.JFrame
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

	/** The parent GUI */
	private AddBindingMenuItem addBindingMenuItem = null;

	/** plug-in id constant for Fractal Binding Factory */
    private static final String PLUGIN_ID = "plugin.id";
    
    /** True if the selected element in the explorer tree is an SCA reference. */
    private boolean isScaReference;
    
    /** GUI Components */
    private javax.swing.JButton jButtonAddBinding;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JLabel jLabelHost;
    private javax.swing.JLabel jLabelJsonUri;
    private javax.swing.JLabel jLabelPort;
    private javax.swing.JLabel jLabelRestUri;
    private javax.swing.JLabel jLabelRestWarning;
    private javax.swing.JLabel jLabelServiceName;
    private javax.swing.JLabel jLabelWsUri;
    private javax.swing.JLabel jLabelWsdl;
    private javax.swing.JLabel jLabelWsdlElement;
    private javax.swing.JLabel jLabelWsdlLocation;
    private javax.swing.JPanel jPanelJson;
    private javax.swing.JPanel jPanelRest;
    private javax.swing.JPanel jPanelRmi;
    private javax.swing.JPanel jPanelWs;
    private javax.swing.JPanel jPanelWs4Ref;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JTextField jTextFieldHost;
    private javax.swing.JTextField jTextFieldJsonUri;
    private javax.swing.JTextField jTextFieldPort;
    private javax.swing.JTextField jTextFieldRestUri;
    private javax.swing.JTextField jTextFieldServiceName;
    private javax.swing.JTextField jTextFieldWsUri;
    private javax.swing.JTextField jTextFieldWsdlEndPoint;
    private javax.swing.JTextField jTextFieldWsdlLocation;

    // --------------------------------------------------------------------------
    // Constructors
    // --------------------------------------------------------------------------
    
    /**
     * Create a new Binding form.
     * 
     * @param addBindingMenuItem A reference to the menu item class to allow callback.
     * @param isScaReference True if the selected element in the explorer tree is an SCA reference.
     */
    public NewBindingForm(AddBindingMenuItem addBindingMenuItem, boolean isScaReference)
    {
        this.addBindingMenuItem = addBindingMenuItem;
        this.isScaReference = isScaReference;
        initComponents();
        setVisible(true);
    }

    // --------------------------------------------------------------------------
    // Private methods
    // --------------------------------------------------------------------------
    
    /**
     * Build the GUI used to add bindings.
     */
    private void initComponents()
    {
        jTabbedPane = new javax.swing.JTabbedPane();
        jPanelRest = new javax.swing.JPanel();
        jTextFieldRestUri = new javax.swing.JTextField();
        jLabelRestUri = new javax.swing.JLabel();
        jLabelRestWarning = new javax.swing.JLabel();
        jPanelWs = new javax.swing.JPanel();
        jTextFieldWsUri = new javax.swing.JTextField();
        jLabelWsUri = new javax.swing.JLabel();
        jPanelJson = new javax.swing.JPanel();
        jTextFieldJsonUri = new javax.swing.JTextField();
        jLabelJsonUri = new javax.swing.JLabel();
        jPanelRmi = new javax.swing.JPanel();
        jLabelServiceName = new javax.swing.JLabel();
        jTextFieldServiceName = new javax.swing.JTextField();
        jLabelHost = new javax.swing.JLabel();
        jLabelPort = new javax.swing.JLabel();
        jTextFieldHost = new javax.swing.JTextField();
        jTextFieldPort = new javax.swing.JTextField();
        jPanelWs4Ref = new javax.swing.JPanel();
        jTextFieldWsdlEndPoint = new javax.swing.JTextField();
        jTextFieldWsdlLocation = new javax.swing.JTextField();
        jLabelWsdl = new javax.swing.JLabel();
        jLabelWsdlLocation = new javax.swing.JLabel();
        jLabelWsdlElement = new javax.swing.JLabel();
        jButtonCancel = new javax.swing.JButton();
        jButtonAddBinding = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Add new SCA Binding");

        jLabelRestUri.setText("URI");

        jTextFieldRestUri.setText("http://localhost:9000/rs");

        jLabelRestWarning.setText("WARNING: the java interface should have jax RS annotations!");

        org.jdesktop.layout.GroupLayout jPanelRestLayout = new org.jdesktop.layout.GroupLayout(jPanelRest);
        jPanelRest.setLayout(jPanelRestLayout);
        jPanelRestLayout.setHorizontalGroup(
            jPanelRestLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelRestLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelRestLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabelRestWarning, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jTextFieldRestUri, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabelRestUri))
                .addContainerGap())
        );
        jPanelRestLayout.setVerticalGroup(
            jPanelRestLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelRestLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabelRestUri)
                .add(7, 7, 7)
                .add(jTextFieldRestUri, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabelRestWarning, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane.addTab("REST", jPanelRest);

        jTextFieldWsUri.setText("http://localhost:9000/ws");

        jLabelWsUri.setText("URI");

        org.jdesktop.layout.GroupLayout jPanelWsLayout = new org.jdesktop.layout.GroupLayout(jPanelWs);
        jPanelWs.setLayout(jPanelWsLayout);
        jPanelWsLayout.setHorizontalGroup(
            jPanelWsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelWsLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelWsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jTextFieldWsUri, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                    .add(jLabelWsUri))
                .addContainerGap())
        );
        jPanelWsLayout.setVerticalGroup(
            jPanelWsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelWsLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabelWsUri, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 15, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(7, 7, 7)
                .add(jTextFieldWsUri, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 19, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        jTabbedPane.addTab("WS", jPanelWs);

        jPanelWs4Ref.setEnabled(false);

        jTextFieldWsdlEndPoint.setText("http://example#wsdl.port(Service/ServicePort)");

        jTextFieldWsdlLocation.setText("http://localhost:9000/Service?wsdl");

        jLabelWsdl.setText("Wsdl");

        jLabelWsdlLocation.setText("Location");

        jLabelWsdlElement.setText("Element");

        org.jdesktop.layout.GroupLayout jPanelWs4RefLayout = new org.jdesktop.layout.GroupLayout(jPanelWs4Ref);
        jPanelWs4Ref.setLayout(jPanelWs4RefLayout);
        jPanelWs4RefLayout.setHorizontalGroup(
            jPanelWs4RefLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelWs4RefLayout.createSequentialGroup()
                .add(jLabelWsdl)
                .add(394, 394, 394))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelWs4RefLayout.createSequentialGroup()
                .add(11, 11, 11)
                .add(jPanelWs4RefLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelWs4RefLayout.createSequentialGroup()
                        .add(jLabelWsdlElement)
                        .add(14, 14, 14)
                        .add(jTextFieldWsdlEndPoint, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE))
                    .add(jPanelWs4RefLayout.createSequentialGroup()
                        .add(jLabelWsdlLocation)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jTextFieldWsdlLocation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 340, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(9, 9, 9))
        );
        jPanelWs4RefLayout.setVerticalGroup(
            jPanelWs4RefLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelWs4RefLayout.createSequentialGroup()
                .add(jLabelWsdl)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelWs4RefLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldWsdlLocation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelWsdlLocation))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelWs4RefLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelWsdlElement)
                    .add(jTextFieldWsdlEndPoint, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jTabbedPane.addTab("WS", jPanelWs4Ref);

        jTextFieldJsonUri.setText("http://localhost:9000/json");

        jLabelJsonUri.setText("URI");

        org.jdesktop.layout.GroupLayout jPanelJsonLayout = new org.jdesktop.layout.GroupLayout(jPanelJson);
        jPanelJson.setLayout(jPanelJsonLayout);
        jPanelJsonLayout.setHorizontalGroup(
            jPanelJsonLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelJsonLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelJsonLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jTextFieldJsonUri, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                    .add(jLabelJsonUri))
                .addContainerGap())
        );
        jPanelJsonLayout.setVerticalGroup(
            jPanelJsonLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelJsonLayout.createSequentialGroup()
                .addContainerGap()
                .add(jLabelJsonUri)
                .add(7, 7, 7)
                .add(jTextFieldJsonUri, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        jTabbedPane.addTab("JsonRpc", jPanelJson);

        jLabelServiceName.setText("Service Name");

        jLabelHost.setText("Host");

        jLabelPort.setText("Port");

        jTextFieldHost.setText("localhost");

        jTextFieldPort.setText("8888");

        org.jdesktop.layout.GroupLayout jPanelRmiLayout = new org.jdesktop.layout.GroupLayout(jPanelRmi);
        jPanelRmi.setLayout(jPanelRmiLayout);
        jPanelRmiLayout.setHorizontalGroup(
            jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelRmiLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabelServiceName)
                    .add(jLabelHost)
                    .add(jLabelPort))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jTextFieldPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldServiceName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 141, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldHost, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 227, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(71, Short.MAX_VALUE))
        );
        jPanelRmiLayout.setVerticalGroup(
            jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelRmiLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelServiceName)
                    .add(jTextFieldServiceName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelHost)
                    .add(jTextFieldHost, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelRmiLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabelPort)
                    .add(jTextFieldPort, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane.addTab("RMI", jPanelRmi);

        jButtonCancel.setText("Cancel");

        jButtonAddBinding.setText("Add binding");

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jTabbedPane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 433, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jButtonCancel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jButtonAddBinding)
                .add(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jTabbedPane, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jButtonAddBinding)
                    .add(jButtonCancel)))
        );

        pack();

        if (this.isScaReference) {
        	jTabbedPane.remove(1); // Ws tab
        } else {
        	jTabbedPane.remove(2); // Ws4ref tab
        }
        
        jButtonAddBinding.addActionListener( new AddBindingButtonCallback() );
        jButtonCancel.addActionListener( new ActionListener() {
            public final void actionPerformed(ActionEvent e)
            {
                // Just return
                NewBindingForm.this.setVisible(false);
            }
        });
    }
    
    /**
     * Callback class used to create a new SCA binding.
     * A map will be populated with information from the Swing form.
     * 
     * @author Christophe Demarey
     */
    class AddBindingButtonCallback implements ActionListener
    {
        /**
         * The AddBinding button was clicked on.
         */
        public final void actionPerformed(ActionEvent e)
        {
            Map<String, Object> hints = new HashMap<String, Object>();
            int idx = NewBindingForm.this.jTabbedPane.getSelectedIndex();
            
            // Create hints for the Fractal {@link BindingFactory}.
            switch (idx) {
            case 0:
                hints.put(PLUGIN_ID, "rest");
                hints.put(RestConnectorConstants.URI, NewBindingForm.this.jTextFieldRestUri.getText());                
                break;
            case 1:
                hints.put(PLUGIN_ID, "ws");
                if (NewBindingForm.this.isScaReference) {
                	hints.put(WsConnectorConstants.WSDL_LOCATION, NewBindingForm.this.jTextFieldWsdlLocation.getText());
                	hints.put(WsConnectorConstants.WSDL_ELEMENT, NewBindingForm.this.jTextFieldWsdlEndPoint.getText());
                } else {
                	hints.put(WsConnectorConstants.URI, NewBindingForm.this.jTextFieldWsUri.getText());
                }
                break;
            case 2:
                hints.put(PLUGIN_ID, "jsonrpc");
                hints.put(JsonRpcConnectorConstants.URI, NewBindingForm.this.jTextFieldJsonUri.getText());
                break;
            case 3:
                hints.put(PLUGIN_ID, "rmi");
                hints.put(RmiConnectorConstants.SERVICE_NAME, NewBindingForm.this.jTextFieldServiceName.getText());
                hints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_PORT, NewBindingForm.this.jTextFieldPort.getText());
                hints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS, NewBindingForm.this.jTextFieldHost.getText());                            
                break;
            default:
                return;
            }
            
            addBindingMenuItem.createBinding(hints);
            NewBindingForm.this.dispose();
        }        
    }
}
