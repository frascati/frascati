/**
 * OW2 FraSCAti Explorer
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.explorer.action;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.util.explorer.api.MenuItem;
import org.objectweb.util.explorer.api.MenuItemTreeView;
import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;

/**
 * This class provides the ability dynamically remove an SCA component 
 * from its composite via the "Remove component"
 * Menu item {@link MenuItem}.
 *
 * @author Christophe Demarey
 */
public class RemoveComponentMenuItem
     extends AbstractAlwaysEnabledMenuItem<Component>
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

	private static final Logger LOG = Logger.getLogger("org.ow2.frascati.explorer.action.RemoveComponentMenuItem");
       
    // --------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------

    /**
     * The default constructor.
     */
    public RemoveComponentMenuItem()
    {
    }

    // --------------------------------------------------------------------------
    // Internal methods
    // --------------------------------------------------------------------------

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------
    
    // --------------------------------------------------------------------------
    // Implementation of the MenuItem interface
    // --------------------------------------------------------------------------
    
    /**
     * @see org.objectweb.util.explorer.api.MenuItem#getStatus(org.objectweb.util.explorer.api.TreeView)
     */
    @Override
    public final int getStatus(TreeView treeView)
    {
        Object object = treeView.getSelectedObject();
        
        if (!(object instanceof Component)) {
            return MenuItem.NOT_VISIBLE_STATUS;
        }
        
        if (!(treeView.getParentObject() instanceof Interface)) { // Parent item is the Domain (CompositeManager service)
            return MenuItem.ENABLED_STATUS;
        }

        Interface contentController = (Interface) treeView.getParentObject();
        Component component = contentController.getFcItfOwner();
        
        try {
            LifeCycleController lcc = Fractal.getLifeCycleController(component);        
            String status = lcc.getFcState();
            if (status.equals(LifeCycleController.STARTED)) {
                // The component is started
                return MenuItem.DISABLED_STATUS;
            }
        } catch (NoSuchInterfaceException e) { 
        	return MenuItem.DISABLED_STATUS;
        }
        
        return MenuItem.ENABLED_STATUS;
    }
    
    /**
     * @see org.ow2.frascati.explorer.action.AbstractAlwaysEnabledMenuItem#execute(Object, MenuItemTreeView)
     */
    @Override
    protected final void execute(Component selected, final MenuItemTreeView treeView)
    {        
        try {
            ContentController contentCtl;
            Component parent = (Component) treeView.getParentObject(); // Parent entry (composite) in the explorer tree
            
    		try {
    			contentCtl = (ContentController) Fractal.getContentController(parent);
    			contentCtl.removeFcSubComponent(selected);
    		} catch (NoSuchInterfaceException nsie) {
    			String msg = "Cannot get content controller on " + treeView.getParentEntry().getName() + "!";
    			LOG.log(Level.SEVERE, msg, nsie);
    			JOptionPane.showMessageDialog(null, msg);
    		} catch (IllegalContentException ice) {
    			String msg = "Illegal content exception!";
    			LOG.log(Level.SEVERE, msg, ice);
    			JOptionPane.showMessageDialog(null, msg);
    		} catch (IllegalLifeCycleException ilce) {
    			String msg = "Cannot remove a component if its container is started!";
    			LOG.log(Level.SEVERE, msg, ilce);
    			JOptionPane.showMessageDialog(null, msg);
    		}
        } catch (ClassCastException e) {
			// parent is the Domain
        	CompositeManager domain = (CompositeManager) treeView.getParentObject();
        	
        	try {
	            domain.removeComposite( (String) treeView.getSelectedEntry().getName() );
            } catch (ManagerException me) {
    			String msg = "Cannot remove this composite!";
    			LOG.log(Level.SEVERE, msg, me);
    			JOptionPane.showMessageDialog(null, msg);
            }
		}
        
    }
}
