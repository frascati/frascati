/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.test;

import static org.junit.Assert.*;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;

/**
 * JUnit test case for OW2 FraSCAti Assembly Factory class. 
 *
 * @author Philippe Merle.
 */
public class AssemblyFactoryTest {

	FraSCAti frascati;

	@Before
    public void initFraSCAti() throws Exception {
      frascati = FraSCAti.newFraSCAti();
    }

    @Test
    public void getAssemblyFactoryComposite() throws Exception {
      Component component = frascati.getComposite("org/ow2/frascati/assembly/factory/AssemblyFactory");
      frascati.close(component);
    }

    @Test
    public void getFraSCAtiBootstrapComposite() throws Exception {
      Component component = frascati.getComposite("org/ow2/frascati/bootstrap/FraSCAti");
      frascati.close(component);
    }

    @Test
    public void getEmptyComposite() throws Exception {
      Component component = frascati.getComposite("org/ow2/frascati/assembly/factory/EmptyComposite");
      frascati.close(component);
    }

    @Test
    public void getFraSCAtiComposite() throws Exception {
// Already loaded at bootstrap time.
//      Component component = frascati.getComposite("org/ow2/frascati/FraSCAti");
//      frascati.close(component);
    }

    /**
     * Check errors produced during the checking phase.
     */
    @Test
    public void processCheckingErrorsComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        frascati.processComposite("CheckingErrors", processingContext);
      } catch(FrascatiException fe) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrors.composite'
    	assertEquals("The number of checking errors", 159, processingContext.getErrors());
        // Let's note that one warning is produced by the OW2 FraSCAti Parser (EMF diagnostics)
        // and other should be errors but are just warnings currently.
        assertEquals("The number of checking warnings", 3, processingContext.getWarnings());
      }
    }

    /**
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processNotSupportedFeaturesComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      processingContext.setProcessingMode(ProcessingMode.generate);
      try {
        frascati.processComposite("NotSupportedFeatures", processingContext);
      } catch(FrascatiException fe) {
        assertEquals("The number of checking errors", 0, processingContext.getErrors());
        // Let's note that the file 'NotSupportedFeatures.composite' produces 24 warnings,
        // one warning is also produced by the OW2 FraSCAti Parser (EMF diagnostics).
        assertEquals("The number of checking warnings", 25, processingContext.getWarnings());
      }
    }

    @Test
    public void geThreadComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        frascati.processComposite("ThreadComposite", processingContext);
      } catch(FrascatiException fe) {
        assertEquals("The number of checking errors", 1, processingContext.getErrors());
        assertEquals("The number of checking warnings", 0, processingContext.getWarnings());
      }
    }

    @Test
    public void geCompositePropertyWithoutValue() throws Exception {
      frascati.getComposite("CompositePropertyWithoutValue");
    }
    
    @Test
    public void importExportTest()
    {
        try
        {
            ProcessingContext processingContext = frascati.newProcessingContext();
            CompositeManager compositeManager=frascati.getCompositeManager();
            compositeManager.processContribution("src/test/resources/helloworld-annotations.zip", processingContext);
        } catch (ManagerException e)
        {
            String exceptionMessage=e.getMessage();
            System.out.println(exceptionMessage);
            assertEquals(true, exceptionMessage.startsWith("20 error(s) detected"));
        }
        catch (FrascatiException e)
        {
            fail();
        }
   }
}
