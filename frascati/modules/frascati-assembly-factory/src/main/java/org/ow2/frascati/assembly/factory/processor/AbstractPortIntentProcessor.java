/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Interface;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.component.factory.api.TypeFactory;

/**
 * OW2 FraSCAti Assembly Factory port intent processor abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractPortIntentProcessor<EObjectType extends EObject>
              extends AbstractIntentProcessor<EObjectType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required Tinfi type factory.
   */
  @Reference(name = "type-factory")
  private TypeFactory typeFactory;

  /**
   * The SCA interface processor.
   */
  @Reference(name = "interface-processor")
  private Processor<Interface> interfaceProcessor;

  /**
   * The binding processor.
   */
  @Reference(name = "binding-processor")
  private Processor<Binding> bindingProcessor;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Get the type factory.
   */
  public final TypeFactory getTypeFactory()
  {
    return this.typeFactory;
  }

  /**
   * Get the interface processor.
   */
  public final Processor<Interface> getInterfaceProcessor()
  {
    return this.interfaceProcessor;
  }

  /**
   * Get the binding processor.
   */
  public final Processor<Binding> getBindingProcessor()
  {
    return this.bindingProcessor;
  }

}
