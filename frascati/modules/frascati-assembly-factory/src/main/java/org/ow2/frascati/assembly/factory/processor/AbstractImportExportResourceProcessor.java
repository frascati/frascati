/**
 * OW2 FraSCAti 
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.eclipse.emf.ecore.EObject;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.ImportExportFrascatiClassLoader;

/**
 *
 */
public abstract class AbstractImportExportResourceProcessor <EObjectType extends EObject> extends AbstractImportExportProcessor<EObjectType>
{

    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.api.ImportExportProcessor#check(java.lang.Object, org.ow2.frascati.util.FrascatiClassLoader[])
     */
    public void check(EObjectType element, FrascatiClassLoader... frascatiClassLoaders) throws ProcessorException
    {
        String resourceUri=this.getResourceUri(element);
        
        /*check resource definition is not empty*/
        if(resourceUri==null || "".equals(resourceUri))
        {
            String message="["+this.getClass().getSimpleName()+"] Empty resource definition found";
            throw new ProcessorException(element, message);
        }
        
        /*check resourceURI is well formed*/
        try
        {
            new URI(resourceUri);
        }
        catch (URISyntaxException uriSyntaxException)
        {
            String message="["+this.getClass().getSimpleName()+"] The provided uri : "+resourceUri+" is not a valid";
            throw new ProcessorException(element, message);
        }

        /*Java resource are not allowed*/
        if(resourceUri.endsWith(".java"))
        {
            String packagePath=resourceUri.substring(0, resourceUri.lastIndexOf("/"));
            String packageName=packagePath.replace("/", ".");
            String message="["+this.getClass().getSimpleName()+"] Java resource : "+resourceUri+" must be declared as java resource, try <import(export).java package=\""+packageName+"\" />";
            throw new ProcessorException(element, message);
        }
        
        /*check resource can be found in the FraSCAtiClassloaders*/
        URL resourceURL=this.getResource(resourceUri, frascatiClassLoaders);
        if(resourceURL==null)
        {
            String message="["+this.getClass().getSimpleName()+"] The resource : "+resourceUri+" can't be found";
            throw new ProcessorException(element, message);
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.api.ImportExportProcessor#instantiate(java.lang.Object, org.ow2.frascati.util.ImportExportFrascatiClassLoader)
     */
    public void instantiate(EObjectType element, ImportExportFrascatiClassLoader importExportFrascatiClassLoader) throws ProcessorException
    {
        String resourceURI=this.getResourceUri(element);
        importExportFrascatiClassLoader.addResource(resourceURI);
    }
    
    protected abstract String getResourceUri(EObjectType element);
}
