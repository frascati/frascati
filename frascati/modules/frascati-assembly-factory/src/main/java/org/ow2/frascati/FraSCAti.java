/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Damien Fournier
 *            Christophe Demarey
 *            Philippe Merle
 *
 * Contributor(s) : 
 *
 */

package org.ow2.frascati;

//import java.io.IOException;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
import java.net.URL;
//import java.util.logging.Level;
import javax.xml.namespace.QName;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.Factory;

import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.util.AbstractFractalLoggeable;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.FrascatiException;

/**
 * The Assembly Factory defines the API for factories in charge of instantiating
 * SCA composite descriptions.
 */
public class FraSCAti
     extends AbstractFractalLoggeable<FrascatiException>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The property's name of the FraSCAti class.
   */
  public static final String FRASCATI_CLASS_PROPERTY_NAME =
      "org.ow2.frascati.class";

  /**
   * Default Assembly Factory class.
   */
  public static final String FRASCATI_CLASS_DEFAULT_VALUE =
      FraSCAti.class.getName();

  /**
   * The property's name of the OW2 FraSCAti bootstrap composite.
   */
  public static final String FRASCATI_BOOTSTRAP_PROPERTY_NAME =
      "org.ow2.frascati.bootstrap";

  /**
   * Default OW2 FraSCAti bootstrap composite.
   */
  public static final String FRASCATI_BOOTSTRAP_DEFAULT_VALUE =
      "org.ow2.frascati.bootstrap.FraSCAti";

  /**
   * The property's name of the OW2 FraSCAti main composite.
   */
  public static final String FRASCATI_COMPOSITE_PROPERTY_NAME =
      "org.ow2.frascati.composite";

  /**
   * Default OW2 FraSCAti main composite.
   */
  public static final String FRASCATI_COMPOSITE_DEFAULT_VALUE =
      "org.ow2.frascati.FraSCAti";

  /**
   * The name of the service providing the interface CompositeManager.
   */
  public static final String COMPOSITE_MANAGER = "composite-manager";

  /**
   * The name of the service providing the interface ClassLoaderManager.
   */
  public static final String CLASSLOADER_MANAGER = "classloader-manager";

  /**
   * The name of the service providing the interface MembraneGeneration.
   */
  public static final String MEMBRANE_GENERATION = "membrane-generation";

  /**
   * The OW2 FraSCAti composite instance.
   */
  private Component frascatiComposite;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Default constructor.
   */
  protected FraSCAti() 
  {
  }

  /**
   * @see AbstractFractalLoggeable#newException(String)
   */
  @Override
  protected final FrascatiException newException(String message) {
    return new FrascatiException(message);
  }

  /**
   * Load and instantiate a Java class.
   */
  @SuppressWarnings("unchecked")
  protected static <T> T loadAndInstantiate(String classname, ClassLoader classLoader)
    throws  ClassNotFoundException, InstantiationException, IllegalAccessException
  {
    Class<?> clazz = classLoader.loadClass(classname);
    return (T)clazz.newInstance();
  }

  /**
   * Init the OW2 FraSCAti bootstrap composite.
   * 
   * @return The assembly factory component
   * @throws FrascatiException 
   */
  protected final void initFrascatiComposite(ClassLoader classLoader) throws FrascatiException
  {
    // Instantiate the OW2 FraSCAti bootstrap factory.
    Factory bootstrapFactory;
    try {
      bootstrapFactory = loadAndInstantiate(
          System.getProperty(FRASCATI_BOOTSTRAP_PROPERTY_NAME, FRASCATI_BOOTSTRAP_DEFAULT_VALUE),
          classLoader);
    } catch (Exception exc) {
      severe(new FrascatiException("Cannot instantiate the OW2 FraSCAti bootstrap class", exc));
      return;
    }

    // Instantiate the OW2 FraSCAti bootstrap composite.
    try {
      this.frascatiComposite = bootstrapFactory.newFcInstance();
    } catch (Exception exc) {
      severe(new FrascatiException("Cannot instantiate the OW2 FraSCAti bootstrap composite", exc));
      return;
    }

    // Start the OW2 FraSCAti bootstrap composite.
    try {
      startFractalComponent(this.frascatiComposite);
    } catch (Exception exc) {
      severe(new FrascatiException("Cannot start the OW2 FraSCAti Assembly Factory bootstrap composite", exc));
      return;
    }

    // At this time, variable 'frascati' refers to the OW2 FraSCAti generated with Juliac.
    // Now reload the OW2 FraSCAti composite with the OW2 FraSCAti bootstrap composite.
    try {
      this.frascatiComposite = getCompositeManager().getComposite(
          System.getProperty(FRASCATI_COMPOSITE_PROPERTY_NAME, FRASCATI_COMPOSITE_DEFAULT_VALUE)
          .replace('.', '/'));
    } catch (Exception exc) {
      severe(new FrascatiException("Cannot load the OW2 FraSCAti composite", exc));
      return;
    }

    // At this time, variable 'frascati' refers to the OW2 FraSCAti composite
    // dynamically loaded by the OW2 FraSCAti bootstrap composite.
 
    try {
      // Add OW2 FraSCAti into its top level domain composite.
      getCompositeManager().addComposite(this.frascatiComposite);
    } catch(Exception exc) {
      severe(new FrascatiException("Cannot add the OW2 FraSCAti composite", exc));
      return;
    }
  }

  /**
   * Get an SCA service of the OW2 FraSCAti composite.
   */
  protected final <T> T getFrascatiService(String serviceName, Class<T> clazz) 
      throws FrascatiException
  {
	return getService(this.frascatiComposite, serviceName, clazz);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  public static FraSCAti newFraSCAti() throws FrascatiException
  {
    return newFraSCAti(FrascatiClassLoader.getCurrentThreadContextClassLoader());
  }

  /**
   * @since 1.5
   */
  public static FraSCAti newFraSCAti(ClassLoader classLoader) throws FrascatiException
  {
    FraSCAti frascati;

// Uncomment next line for debugging class loader.
//    FrascatiClassLoader.print(classLoader);

    // Get the current thread's context class loader and set it.
    ClassLoader previousCurrentThreadContextClassLoader = FrascatiClassLoader.getAndSetCurrentThreadContextClassLoader(classLoader);

    try {
      try {
        frascati = loadAndInstantiate(
          System.getProperty(FRASCATI_CLASS_PROPERTY_NAME, FRASCATI_CLASS_DEFAULT_VALUE),
          classLoader);
      } catch (Exception exc) {
        throw new FrascatiException("Cannot instantiate the OW2 FraSCAti class", exc);
      }
      frascati.initFrascatiComposite(classLoader);
    } finally {
      // Reset the previous current thread's context class loader.
      FrascatiClassLoader.setCurrentThreadContextClassLoader(previousCurrentThreadContextClassLoader);
    }
    return frascati;
  }

  /**
   * Get the FraSCAti composite.
   *
   * @since 1.5
   */
  public Component getFrascatiComposite()
  {
    return this.frascatiComposite;
  }

  /**
   * Get the CompositeManager service of the OW2 FraSCAti Assembly Factory.
   */
  public final CompositeManager getCompositeManager() 
      throws FrascatiException
  {
    return getFrascatiService(COMPOSITE_MANAGER, CompositeManager.class);
  }

  /**
   * Create an SCA assembly instance from composite file
   * 
   * @param composite
   *          The composite file located into the current class path, on the
   *          local file system, or at an URL
   * 
   * @return the SCA composite component or null if an error occurs.
   * @throws FrascatiException 
   */
  public final Component getComposite(String composite) 
      throws FrascatiException
  {
    return getCompositeManager().getComposite(composite);
  }

  /**
   * Load jars and create an SCA assembly instance from composite file
   * 
   * @param composite
   *          - The composite file located into the current class path, on the
   *          local file system, or at an URL
   * @param libs
   *          - Libraries required to load this composite
   * 
   * @return the SCA composite component
   * @throws FrascatiException 
   */
  public final Component getComposite(String composite, URL[] libs)
      throws FrascatiException
  {
    return getCompositeManager().getComposite(composite, libs);
  }

  /**
   * Load and create an SCA assembly instance from composite file
   * 
   * @param composite
   *          - The composite file located into the current class path, on the
   *          local file system, or at an URL
   * @param classLoader
   *          - the class loader to use
   * 
   * @return the SCA composite component
   * @throws FrascatiException 
   */
  public final Component getComposite(String composite, ClassLoader classLoader)
      throws FrascatiException
  {
    return getCompositeManager().getComposite(composite, classLoader);
  }

  /**
   * 
   */
  public final Component[] getContribution(String contribution)
      throws FrascatiException
  {
    return getCompositeManager().getContribution(contribution);
  }

  /**
   * 
   */
  public final ProcessingContext newProcessingContext()
      throws FrascatiException
  {
    return newProcessingContext(ProcessingMode.all);
  }

  /**
   * 
   */
  public final ProcessingContext newProcessingContext(ProcessingMode processingMode)
      throws FrascatiException
  {
    ProcessingContext processingContext = getCompositeManager().newProcessingContext();
    processingContext.setProcessingMode(processingMode);
    return processingContext;
  }

  /**
   * 
   */
  public final ProcessingContext newProcessingContext(ClassLoader classLoader)
      throws FrascatiException
  {
    return getCompositeManager().newProcessingContext(classLoader);
  }

  /**
   * 
   */
  public final Component processComposite(String composite, ProcessingContext processingContext)
      throws FrascatiException
  {
    return getCompositeManager().processComposite(new QName(composite), processingContext); 
  }

  /**
   * Get an SCA service of an SCA component.
   */
  @SuppressWarnings("unchecked")
  public final <T> T getService(Component component, String serviceName, Class<T> clazz) 
      throws FrascatiException
  {
    try {
      return (T)component.getFcInterface(serviceName);
    } catch(NoSuchInterfaceException e) {
      severe(new FrascatiException("Cannot retrieve the '" + serviceName +
           "' service of an SCA composite!", e));
      return null;
    }
  }

  /**
   * Close an SCA composite.
   * 
   * @param composite
   *          The name of the SCA composite to close.
   * @throws FrascatiException
   */
  public final void close(Component composite) throws FrascatiException {
    // Closing the SCA composite.
    String compositeName = "";
    try {
      compositeName = getFractalComponentName(composite);	
      log.info("Closing the SCA composite '" + compositeName + "'.");
      getCompositeManager().removeComposite(compositeName);
    } catch (Exception exc) {
      severe(new FrascatiException("Impossible to stop the SCA composite '" + compositeName + "'!", exc));
      return;
    }
  }

  /**
   * Close FraSCAti.
   * 
   * @throws FrascatiException
   */
  public final void close() throws FrascatiException {
    // Close all running SCA composites.
    for(String compositeName : getCompositeManager().getCompositeNames()) {
      // Close the FraSCAti composite after closing all other composites.
      if(compositeName.equals(FRASCATI_COMPOSITE_DEFAULT_VALUE)) {
        continue; 
      }
      try {
        getCompositeManager().removeComposite(compositeName);
      } catch (Exception exc) {
        log.warning("Can't close the SCA composite '" + compositeName + "'.");
      }
    }
	  
    // Closing the FraSCAti composite.
    close(this.frascatiComposite);
    this.frascatiComposite = null;
  }

  /**
   * Get the ClassLoaderManager service of the OW2 FraSCAti Assembly Factory.
   */
  public final ClassLoaderManager getClassLoaderManager() 
      throws FrascatiException
  {
    return getFrascatiService(CLASSLOADER_MANAGER, ClassLoaderManager.class);
  }

  /**
   * Set the @link{ClassLoader} used by the FraSCAti Assembly Factory.
   * 
   * @param classloader
   *          the class loader to be used by the FraSCAti Assembly Factory
   * @throws FrascatiException 
   */
  public final void setClassLoader(ClassLoader classloader)
      throws FrascatiException
  {
    getClassLoaderManager().setClassLoader(classloader);
  }

  /**
   * Get the ClassLoader used by the Factory
   * 
   * @return - ClassLoader currently used by the Factory
   * @throws FrascatiException 
   */
  public final ClassLoader getClassLoader()
      throws FrascatiException
  {
    return getClassLoaderManager().getClassLoader();
  }

  /**
   * Get the MembraneGeneration service of the OW2 FraSCAti Assembly Factory.
   */
  public final MembraneGeneration getMembraneGeneration() 
      throws FrascatiException
  {
    return getFrascatiService(MEMBRANE_GENERATION, MembraneGeneration.class);
  }

  /**
   * Run the Factory with given arguments (composite path and the classpath).
   * This method is simply called by the main method of the FactoryCommandLine
   * tool.
   * 
   * @param factory
   *          - The factory instance to use (AssemblyFactory, RuntimeFactory,
   *          Generate Factory).
   * @param compositeName
   *          The name of the SCA composite to execute.
   * @param serviceName
   *          The name of the service to execute.
   * @param methodName
   *          The name of the method to execute.
   * @param classpath
   *          - List of URLs used to define the classpath.
   * @throws Exception
   */
/*  public static void run(AssemblyFactory factory, String compositeName,
      String serviceName, String methodName, URL[] classpath)
      throws FrascatiException {
    // the SCA composite
    @SuppressWarnings("unused")
    Component scaDomain = null;

    // TODO Remove commented code

    // => Run the FraSCAti runtime
    // Set the RMI SecurityManager in order to allow dynamic class loading.
    // System.setSecurityManager( new java.rmi.RMISecurityManager() );
    // ClassLoader cl = FrascatiClassLoader.getCurrentThreadContextClassLoader();
    // URL policyURL = cl.getResource("frascati.policy");
    // System.setProperty("java.security.policy", policyURL.toString());

    // factory.launch(compositeName, "r", "run");
    if ((serviceName != null) && (methodName != null)) {
      scaDomain = factory.launch(compositeName, serviceName, methodName,
          classpath);
    } else {
      try {
        scaDomain = factory.getComposite(compositeName, classpath);

        System.gc();
        System.out.println("\nPress any key to exit");

        System.in.read();
        
      } catch (IOException e) {
        e.printStackTrace();
      } catch (FrascatiException e) {
        factory.log.severe("Could not get composite \n");
        e.printStackTrace();
      }
    }

    System.out.println("\nExiting....");
    // TinfiDomain.close(scaDomain);

    System.exit(0);
  }
*/

  /**
   * Launch an SCA composite.
   * 
   * @param compositeName
   *          The name of the SCA composite to execute.
   * @param serviceName
   *          The name of the service to execute.
   * @param methodName
   *          The name of the method to execute.
   * @param jars
   *          A list of URL for jar location
   * 
   * @return The composite component.
   * @throws FrascatiException 
   */
/*  public Component launch(final String compositeName, final String serviceName,
      final String methodName, URL[] jars) throws FrascatiException {

    // Loading the SCA composite.
    Component composite = null;
    try {
      composite = getComposite(compositeName, jars);
    } catch (FrascatiException e) {
      log.severe("Could not get composite \n");
      e.printStackTrace();
    }

    if (composite != null) {
      // Obtain the service.
      Object service = null;
      try {
        log.info("Obtaining the service '" + serviceName
            + "' of the SCA composite '" + compositeName + "'...");
        service = TinfiDomain.getService(composite, Object.class, serviceName);
      } catch (NoSuchInterfaceException nsie) {
        log.log(Level.SEVERE, "The SCA composite '" + compositeName
            + "' has no service '" + serviceName + "'!", nsie);
      }

      if (service != null) {
        try {
          log.info("Executing the method '" + methodName + "' of the service '"
              + serviceName + "' of the SCA composite '" + compositeName
              + "'...");
          Method method = service.getClass().getMethod(methodName);
          method.invoke(service);
        } catch (NoSuchMethodException nsme) {
          log.log(Level.SEVERE, "The service '" + serviceName
              + "' of the SCA composite '" + compositeName
              + "' has no method '" + methodName + "'!", nsme);
        } catch (IllegalAccessException iae) {
          log.log(Level.SEVERE, "Impossible to execute the method '"
              + methodName + "' of the service '" + serviceName
              + "' of the SCA composite '" + compositeName + "'!", iae);
        } catch (IllegalArgumentException iae) {
          log.log(Level.SEVERE, "Impossible to execute the method '"
              + methodName + "' of the service '" + serviceName
              + "' of the SCA composite '" + compositeName + "'!", iae);
        } catch (InvocationTargetException ite) {
          log.log(Level.SEVERE, "Impossible to execute the method '"
              + methodName + "' of the service '" + serviceName
              + "' of the SCA composite '" + compositeName + "'!", ite);
        }
      }
    }
    return composite;
  }
*/
}
