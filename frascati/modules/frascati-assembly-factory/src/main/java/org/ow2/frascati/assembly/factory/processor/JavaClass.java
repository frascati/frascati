/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

/**
 * OW2 FraSCAti Assembly Factory Java class wrapper.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class JavaClass {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The wrapped Java class name.
   */
  private String className;

  /**
   * The wrapped Java class.
   */
  private Class<?> javaClass;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs a new JavaClass instance.
   *
   * @param className the Java class name to wrap.
   * @param javaClass the Java class to wrap.
   */
  public JavaClass(String className, Class<?> javaClass) {
    this.className = className;
    this.javaClass = javaClass;
  }

  /**
   * Get the wrapped Java class name.
   *
   * @return the wrapped Java class name.
   */
  public final String getClassName() {
    return this.className;
  }

  /**
   * Get the wrapped Java class.
   *
   * @return the wrapped Java class.
   */
  public final Class<?> getJavaClass() {
    return this.javaClass;
  }

}
