/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.stp.sca.Composite;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext.MembraneDescription;
import org.ow2.frascati.parser.core.ParsingContextImpl;

/**
 * OW2 FraSCAti Assembly Factory SCA processing context class.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ProcessingContextImpl extends ParsingContextImpl implements ProcessingContext
{

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * The current processing mode.
     */
    private ProcessingMode processingMode = ProcessingMode.all;

    private List<Composite> processedComposites = new ArrayList<Composite>();

    /**
     * The processed root SCA composite.
     */
    private Composite rootComposite;

    /**
     * Output directory.
     */
    private String outputDirectory;

    /**
     * List of Java source directories to compile.
     */
    private List<String> javaSourceDirectoryToCompile = new ArrayList<String>();

    /**
     * List of membranes to generate.
     */
    private List<MembraneDescription> membranesToGenerate = new ArrayList<MembraneDescription>();

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    // ---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Construct with the current thread context class loader.
     */
    public ProcessingContextImpl()
    {
        super();
    }

    /**
     * Construct with a class loader.
     * 
     * @param classLoader the class loader of the parser context.
     */
    public ProcessingContextImpl(ClassLoader classLoader)
    {
        super(classLoader);
    }

    /**
     * @see ParsingContextImpl#loadClass(String)
     */
    @Override
    public final <T> Class<T> loadClass(String className) throws ClassNotFoundException
    {
        try
        {
            return super.loadClass(className);
        } catch (ClassNotFoundException cnfe)
        {
            if (getResource(className.replace(".", File.separator) + ".java") != null)
            {
                return null;
            }
            throw cnfe;
        }
    }

    /**
     * @see ProcessingContext.getProcessingMode()
     */
    public final ProcessingMode getProcessingMode()
    {
        return this.processingMode;
    }

    /**
     * @see ProcessingContext.setProcessingMode(ProcessingMode)
     */
    public final void setProcessingMode(ProcessingMode processingMode)
    {
        this.processingMode = processingMode;
    }

    /**
     * @see ProcessingContext.getRootComposite()
     */
    public final Composite getRootComposite()
    {
        return this.rootComposite;
    }

    /**
     * @see ProcessingContext.setRootComposite(Composite)
     */
    public final void setRootComposite(Composite composite)
    {
        this.rootComposite = composite;
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.ProcessingContext#addProcessedComposite(org.eclipse.stp.sca.Composite)
     */
    public final void addProcessedComposite(Composite processedComposite)
    {
        this.processedComposites.add(processedComposite);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.ProcessingContext#getProcessedComposite()
     */
    public final List<Composite> getProcessedComposite()
    {
        return this.processedComposites;
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.ProcessingContext#getProcessedComposite(java.lang.String)
     */
    public Composite getProcessedComposite(String compositeName)
    {
        for (Composite processedComposite : this.processedComposites)
        {
            if (processedComposite.getName().equals(compositeName))
            {
                return processedComposite;
            }
        }
        return null;
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#getOutputDirectory()
     */
    public String getOutputDirectory()
    {
      return this.outputDirectory;
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#setOutputDirectory(String)
     */
    public void setOutputDirectory(String path)
    {
      this.outputDirectory = path;
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#addJavaSourceDirectoryToCompile(String)
     */
    public void addJavaSourceDirectoryToCompile(String path)
    {
      // Only store Java source path not already registered.
      if(!this.javaSourceDirectoryToCompile.contains(path)) {
        this.javaSourceDirectoryToCompile.add(path);
      }	  
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#getJavaSourceDirectoryToCompile()
     */
    public String[] getJavaSourceDirectoryToCompile()
    {
      return this.javaSourceDirectoryToCompile.toArray(new String[this.javaSourceDirectoryToCompile.size()]);
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#addMembraneToGenerate(MembraneDescription)
     */
    public void addMembraneToGenerate(MembraneDescription md)
    {
      this.membranesToGenerate.add(md);
    }

    /**
     * @see org.ow2.frascati.component.factory.api.ComponentFactoryContext#getMembraneToGenerate()
     */
    public MembraneDescription[] getMembraneToGenerate()
    {
      return this.membranesToGenerate.toArray(new MembraneDescription[this.membranesToGenerate.size()]);
    }
}
