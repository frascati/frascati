/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Interface;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;

/**
 * OW2 FraSCAti Assembly Factory SCA interface processor abstract class.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractInterfaceProcessor<InterfaceType extends Interface>
              extends AbstractProcessor<InterfaceType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected void toStringBuilder(InterfaceType interfaze, StringBuilder sb)
  {
  }

  /**
   * Store the Java class associated to the SCA interface.
   */
  protected final void storeJavaInterface(InterfaceType interfaze, ProcessingContext processingContext,
      String javaInterfaceName, Class<?> javaClass)
  {
    processingContext.putData(interfaze, JavaClass.class, new JavaClass(javaInterfaceName, javaClass));
  }

  /**
   * Get the Java class associated to the SCA interface.
   */
  public static Class<?> getClass(Interface interfaze, ProcessingContext processingContext)
  {
    JavaClass javaClass = processingContext.getData(interfaze, JavaClass.class);
    if(javaClass == null) {
      return null;
    }
    Class<?> result = javaClass.getJavaClass();
    if(result == null) {
      try {
        result = processingContext.loadClass(javaClass.getClassName());
      } catch (ClassNotFoundException cnfe) {
    	processingContext.warning(cnfe.getMessage());
      }
    }
    return result;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
