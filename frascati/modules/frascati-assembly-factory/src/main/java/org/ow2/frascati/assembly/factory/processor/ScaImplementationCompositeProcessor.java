/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.lang.annotation.ElementType;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Property;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.ScaPackage;
import org.objectweb.fractal.api.Component;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA implementation composite processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class ScaImplementationCompositeProcessor
     extends AbstractImplementationProcessor<SCAImplementation> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required composite processor.
   */
  @Reference(name = "composite-processor")
  private Processor<Composite> compositeProcessor;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(SCAImplementation scaImplementation, StringBuilder sb) {
    sb.append("sca:implementation.composite");
    append(sb, "name", scaImplementation.getName());
    super.toStringBuilder(scaImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SCAImplementation scaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the implementation composite name.
    checkAttributeMustBeSet(scaImplementation, "name", scaImplementation.getName(), processingContext);

    if(scaImplementation.getName() != null) {
      // Retrieve the parsed composite from the processing context.
      // This data was put by the FraSCAti SCA Parser ImplementationCompositeResolver.
      Composite composite = processingContext.getData(scaImplementation, Composite.class);
      
      // Add the properties of the enclosing component as properties of the composite.
      // This is required to manage <property ... source="$propertyName"/>.
      for(PropertyValue propertyValue : getParent(scaImplementation, org.eclipse.stp.sca.Component.class).getProperty()) {
        Property property = ScaFactory.eINSTANCE.createProperty();
        property.setName(propertyValue.getName());
        property.setElement(propertyValue.getElement());
        if(propertyValue.isSetMany()) {
          property.setMany(propertyValue.isMany());
        } else {
          property.unsetMany();
        }
        property.setType(propertyValue.getType());
        property.setValue(propertyValue.getValue());
        
        List<EObject> contents=propertyValue.eContents();
        // If the content of the property is not empty we store the resource and the propertyValue of the composite in the ProcessingContext
        // This is required to manage JAX-B properties 
        if(contents!=null && !contents.isEmpty())
        {
            processingContext.putData(property, PropertyValue.class, propertyValue);
            processingContext.putData(property, Resource.class, propertyValue.eResource());
        }
        
        // TODO: Must copy propertyValue.getFile(), propertyValue.getSource(), propertyValue.getMixed(), propertyValue.getAny() to property.
        composite.getProperty().add(property);
      }

      compositeProcessor.check(composite, processingContext);
    }

    // TODO: 'requires' must be checked here.
    // Does the intent composite exist?
    // Is this composite an intent (service Intent of interface IntentHandler)?

    // check attributes 'policySets' and 'requires'.
    checkImplementation(scaImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(SCAImplementation scaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve the composite from the processing context.
    Composite composite = processingContext.getData(scaImplementation, Composite.class);

    // Generating the composite.
    compositeProcessor.generate(composite, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(SCAImplementation scaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve the composite from the processing context.
    Composite composite = processingContext.getData(scaImplementation, Composite.class);

    // Instantiating the composite component.
    compositeProcessor.instantiate(composite, processingContext);

    // store the component into the processing context.
    processingContext.putData(scaImplementation, Component.class, getFractalComponent(composite, processingContext));
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(SCAImplementation scaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve the composite from the processing context.
    Composite composite = processingContext.getData(scaImplementation, Composite.class);

    // Completing the composite.
    compositeProcessor.complete(composite, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.SCA_IMPLEMENTATION);
  }

}
