/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.SCAPropertyBase;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.api.Processor;

/**
 * OW2 FraSCAti Assembly Factory XSD-based type property factory.
 *
 * Convert XSD typed values into Java values.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 * @since 1.1
 */
public class ScaPropertyTypeXsdProcessor
     extends AbstractPropertyTypeProcessor {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * XML Schema Definitions.
   */
  private static final String XML_SCHEMA_URL = "http://www.w3.org/2001/XMLSchema";
  private static final String XML_SCHEMA_INSTANCE_URL = "http://www.w3.org/2001/XMLSchema-instance";

  /**
   * Enumeration of supported XSD datatypes.
   */
  enum XsdType {
    ANYSIMPLETYPE("anySimpleType"),
    ANYURI("anyURI"),
    BASE64BINARY("base64Binary"),
    BOOLEAN("boolean"),
    BYTE("byte"),
    DATE("date"),
    DATETIME("dateTime"),
    DECIMAL("decimal"),
    DOUBLE("double"),
    DURATION("duration"),
    FLOAT("float"),
    G("g"),
    HEXBINARY("hexBinary"),
    INT("int"),
    INTEGER("integer"),
    LONG("long"),
    NEGATIVEINTEGER("negativeInteger"),
    NONNEGATIVEINTEGER("nonNegativeInteger"),
    NONPOSITIVEINTEGER("nonPositiveInteger"),
    NOTATION("NOTATION"),
    POSITIVEINTEGER("positiveInteger"),
    UNSIGNEDBYTE("unsignedByte"),
    UNSIGNEDINT("unsignedInt"),
    UNSIGNEDLONG("unsignedLong"),
    UNSIGNEDSHORT("unsignedShort"),
    SHORT("short"),
    STRING("string"),
    TIME("time"),
    QNAME("QName");

    /**
     * Construct an XSD datatype.
     */
    private XsdType(String label) {
      this.label = label;
    }

    /**
     * Label.
     */
    private String label;

    /**
     * Get the label.
     */
    public final String getLabel()
    {
      return this.label;
    }

    /**
     * Map of the XSD datatypes.
     */
    public static final Map<String, XsdType> VALUES = new HashMap<String, XsdType>();
    static {
      VALUES.put(ANYSIMPLETYPE.getLabel(), ANYSIMPLETYPE);
      VALUES.put(ANYURI.getLabel(), ANYURI);
      VALUES.put(BASE64BINARY.getLabel(), BASE64BINARY);
      VALUES.put(BOOLEAN.getLabel(), BOOLEAN);
      VALUES.put(BYTE.getLabel(), BYTE);
      VALUES.put(DATE.getLabel(), DATE);
      VALUES.put(DATETIME.getLabel(), DATETIME);
      VALUES.put(DECIMAL.getLabel(), DECIMAL);
      VALUES.put(DOUBLE.getLabel(), DOUBLE);
      VALUES.put(DURATION.getLabel(), DURATION);
      VALUES.put(FLOAT.getLabel(), FLOAT);
      VALUES.put(G.getLabel(), G);
      VALUES.put(HEXBINARY.getLabel(), HEXBINARY);
      VALUES.put(INT.getLabel(), INT);
      VALUES.put(INTEGER.getLabel(), INTEGER);
      VALUES.put(LONG.getLabel(), LONG);
      VALUES.put(NEGATIVEINTEGER.getLabel(), NEGATIVEINTEGER);
      VALUES.put(NONPOSITIVEINTEGER.getLabel(), NONPOSITIVEINTEGER);
      VALUES.put(NONNEGATIVEINTEGER.getLabel(), NONNEGATIVEINTEGER);
      VALUES.put(NOTATION.getLabel(), NOTATION);
      VALUES.put(POSITIVEINTEGER.getLabel(), POSITIVEINTEGER);
      VALUES.put(UNSIGNEDBYTE.getLabel(), UNSIGNEDBYTE);
      VALUES.put(UNSIGNEDINT.getLabel(), UNSIGNEDINT);
      VALUES.put(UNSIGNEDLONG.getLabel(), UNSIGNEDLONG);
      VALUES.put(UNSIGNEDSHORT.getLabel(), UNSIGNEDSHORT);
      VALUES.put(SHORT.getLabel(), SHORT);
      VALUES.put(STRING.getLabel(), STRING);
      VALUES.put(TIME.getLabel(), TIME);
      VALUES.put(QNAME.getLabel(), QNAME);
    }
  }

  /**
   * The used XSD datatype factory.
   */
  private DatatypeFactory datatypeFactory;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Get a XsdType for a given property type.
   */
  protected static XsdType getXsdType(QName propertyType)
  {
    if(propertyType != null) {
      String namespace = propertyType.getNamespaceURI();
      // is the property type namepace XSD?
      if(namespace.equals(XML_SCHEMA_URL) || namespace.equals(XML_SCHEMA_INSTANCE_URL)) {
    	// Is the property type refer to one of supported XXSD datatypes?
        XsdType xsdType = XsdType.VALUES.get(propertyType.getLocalPart());
        if(xsdType != null) {
          return xsdType;
        }
      }
    }
    return null;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SCAPropertyBase property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Get the property type of the given property.
	QName propertyType = processingContext.getData(property, QName.class);

	// Check that this property type is an XSD datatype.
	if(getXsdType(propertyType) != null) {
      // Affect this property type processor to the given property.
      processingContext.putData(property, Processor.class, this);
      // Compute the property value to set.
      doComplete(property, processingContext);
	}
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(SCAPropertyBase property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // The property value as given in a .composite file.
    String propertyValue = property.getValue();

    // Get the property type of the given property.
	QName propertyType = processingContext.getData(property, QName.class);

	// Search which the XSD datatype is.
    XsdType xsdType = getXsdType(propertyType);
    if(xsdType == null) {
      // Should never happen but we never know!
      severe(new ProcessorException("Error while converting XSD property datatype: unknown XML type '" + propertyType + "'"));
      return;
    }

    // Compute the property value.
	Object computedPropertyValue;
	try {
      switch (xsdType) {
      case ANYURI:
          try {
            computedPropertyValue = new URI(propertyValue);
          } catch(URISyntaxException exc) {
            error(processingContext, property, "Syntax error in URI '", propertyValue, "'");
            return;
          }
          break;
      case ANYSIMPLETYPE:
          computedPropertyValue = propertyValue;
          break;
      case BASE64BINARY:
          // TODO: is it the right thing to do?
          computedPropertyValue = propertyValue.getBytes();
          break;
      case BOOLEAN:
          computedPropertyValue = Boolean.valueOf(propertyValue);
          break;
      case BYTE:
          computedPropertyValue = Byte.valueOf(propertyValue);
          break;
      case DATE:
      case DATETIME:
      case TIME:
      case G:
          try {
            computedPropertyValue = datatypeFactory.newXMLGregorianCalendar(propertyValue);
          } catch(IllegalArgumentException iae) {
            error(processingContext, property, "Invalid lexical representation '", propertyValue, "'");
            return;
          }
          break;
      case DECIMAL:
          computedPropertyValue = new BigDecimal(propertyValue);
          break;
      case DOUBLE:
          computedPropertyValue = Double.valueOf(propertyValue);
          break;
      case DURATION:
          try {
            computedPropertyValue = datatypeFactory.newDuration(propertyValue);
          } catch(IllegalArgumentException iae) {
            error(processingContext, property, "Invalid lexical representation '", propertyValue, "'");
            return;
          }
          break;
      case FLOAT:
          computedPropertyValue = Float.valueOf(propertyValue);
          break;
      case HEXBINARY:
          // TODO: is it the right thing to do?
    	  computedPropertyValue = propertyValue.getBytes();
          break;
      case INT:
          computedPropertyValue = Integer.valueOf(propertyValue); 
          break;
      case INTEGER:
      case LONG:
          computedPropertyValue = Long.valueOf(propertyValue);
          break;
      case NEGATIVEINTEGER:
      case NONPOSITIVEINTEGER:
          Long negativeInteger = Long.valueOf(propertyValue);
          if(negativeInteger.longValue() > 0) {
            error(processingContext, property, "The value must be a negative integer");
            return;
    	  }
          computedPropertyValue = negativeInteger;
          break;
      case NONNEGATIVEINTEGER:
          Long nonNegativeInteger = Long.valueOf(propertyValue);
          if(nonNegativeInteger.longValue() < 0) {
            error(processingContext, property, "The value must be a non negative integer");
            return;
          }
          computedPropertyValue = nonNegativeInteger;
          break;
      case POSITIVEINTEGER:
          BigInteger positiveInteger = new BigInteger(propertyValue);
          if(positiveInteger.longValue() < 0) {
            error(processingContext, property, "The value must be a positive integer");
            return;
          }
          computedPropertyValue = positiveInteger;
          break;
      case NOTATION:
          computedPropertyValue = QName.valueOf(propertyValue);
          break;
      case UNSIGNEDBYTE:
          Short unsignedByte = Short.valueOf(propertyValue);
          if(unsignedByte.shortValue() < 0) {
            error(processingContext, property, "The value must be a positive byte");
            return;
          }
          computedPropertyValue = unsignedByte;
          break;
      case UNSIGNEDINT:
          Long unsignedInt = Long.valueOf(propertyValue);
          if(unsignedInt.longValue() < 0) {
            error(processingContext, property, "The value must be a positive integer");
            return;
          }
          computedPropertyValue = unsignedInt;
          break;
      case UNSIGNEDLONG:
          Long unsignedLong = Long.valueOf(propertyValue);
          if(unsignedLong.longValue() < 0) {
            error(processingContext, property, "The value must be a positive long");
            return;
          }
          computedPropertyValue = unsignedLong;
          break;
      case UNSIGNEDSHORT:
    	  Integer unsignedShort = Integer.valueOf(propertyValue);
          if(unsignedShort.intValue() < 0) {
            error(processingContext, property, "The value must be a positive short");
            return;
          }
          computedPropertyValue = unsignedShort;
          break;
      case SHORT:
          computedPropertyValue = Short.valueOf(propertyValue);
          break;
      case STRING:
          computedPropertyValue = propertyValue;
          break;
      case QNAME:
          computedPropertyValue = QName.valueOf(propertyValue);
          break;
      default:
          // Should never happen but we never know!
          severe(new ProcessorException(property, "The XSD-based '" + propertyType + "' property datatype is known but not dealt by OW2 FraSCAti!"));
          return ;
      }
    } catch(NumberFormatException nfe) {
      error(processingContext, property, "Number format error in '", propertyValue, "'");
      return;
    }

    // Attach the computed property value to the given property.
	processingContext.putData(property, Object.class, computedPropertyValue);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructor.
   */
  public ScaPropertyTypeXsdProcessor() throws ProcessorException {
    try {
      datatypeFactory = DatatypeFactory.newInstance();
    } catch(Exception e) {
      // Should never happen but we never know!
      throw new ProcessorException(e);
    }
  }

}
