/**
 * OW2 FraSCAti Assembly Factory
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.api;

import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.ImportExportFrascatiClassLoader;

/**
 * OW2 FraSCAti Assembly Factory SCA import and export interface.
 * 
 * The ImportExportProcessors are used to proceed SCA import and export when deploying a contribution
 *
 */
public interface ImportExportProcessor<ElementType>
{
    /**
     * Get the processor ID.
     *
     * @return the processor ID.
     */
    String getProcessorID();
    

    /**
     * Check an element
     * 
     * @param element : the element to check
     * @param frascatiClassLoader : Array of FraSCAtiClassloader where we check the validity of the element
     * @throws ProcessorException
     */
    void check(ElementType element,FrascatiClassLoader ... frascatiClassLoader) throws ProcessorException;
    

    /**
     * Instantiate an element
     * 
     * @param element : the element to proceed
     * @param importExportFrascatiClassLoader :  The ImportExportFrascatiClassLoader to modify with the valid element
     * @throws ProcessorException
     */
    void instantiate(ElementType element, ImportExportFrascatiClassLoader importExportFrascatiClassLoader) throws ProcessorException;
    
}
