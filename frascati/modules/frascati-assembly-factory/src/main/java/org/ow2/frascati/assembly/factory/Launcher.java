/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Dolet
 * 
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.assembly.factory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Level;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.FrascatiException;

/**
 * Launcher for SCA composite applications.
 * 
 * @author Nicolas Dolet
 */
public class Launcher
     extends AbstractLoggeable
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Name of the SCA composite to launch
   */
  private String compositeName;
  
  /**
   * The composite
   */
  private Component scaComposite;
  
  //---------------------------------------------------------------------------
  // Constructor(s)
  // --------------------------------------------------------------------------
  
  /**
   * Default constructor.
   */
  public Launcher()
  {
    scaComposite = null;
  }

  /**
   * Launcher for a given composite.
   * 
   * @param compositeName the composite name
   * @throws FrascatiException 
   */
  public Launcher(String compositeName) 
      throws FrascatiException
  {
      this(compositeName, FraSCAti.newFraSCAti(), null);
  }
  
  /**
   * Launcher for <code>compositeName</code> with the Factory <code>f</code>
   * 
   * @param compositeName the name of the composite to load
   * @param f the factory to use to create composite
   * @throws FrascatiException 
   */
  public Launcher(String compositeName, FraSCAti f) 
      throws FrascatiException
  {
      this(compositeName, f, null);
  }
  
  /**
   * Launcher for <code>compositeName</code> which libraries
   * are in <code>urls</code>, with the Factory <code>f</code>
   * 
   * @param compositeName the name of the composite to load
   * @param f the factory to use to create composite
   * @param urls URLs of libraries required to launch the composite
   * @throws FrascatiException 
   */
  public Launcher(String compositeName, FraSCAti f, URL[] urls) 
      throws FrascatiException
  {
    try {
        if (compositeName.endsWith(".zip")) {
            File file = new File(compositeName); 
            if (file.exists())
                f.getContribution(compositeName);
            else
                log.severe("Cannot find the " + file.getAbsolutePath() + " file!");
        } else {
            scaComposite = f.getComposite(compositeName, urls);
        }
      this.compositeName = compositeName;
    } catch (FrascatiException fe) {
      severe(compositeName, fe);
    }
  }

  //---------------------------------------------------------------------------
  // Internal methods
  // --------------------------------------------------------------------------

  /**
   * Log FraSCAti exceptions.
   */
  private void severe(String compositeName, FrascatiException fe)
    throws FrascatiException
  {
    scaComposite = null;
    log.severe("Unable to launch composite '" + compositeName + "': " + fe.getMessage());
    throw fe;  
  }

  //---------------------------------------------------------------------------
  // Public methods
  // --------------------------------------------------------------------------

  /**
   * Launch the composite
   * @throws FrascatiException 
   */
  public final void launch() throws FrascatiException
  {
    if (compositeName != null) {
      try {
        scaComposite = FraSCAti.newFraSCAti().getComposite(compositeName);
      } catch (FrascatiException fe) {
        severe(compositeName, fe);
      }
    } else {
      log.severe("FraSCAti launcher: composite name must be set!");
    }
  }
  
  /**
   * Launches an SCA composite
   * 
   * @param compositeName the composite to launch
   * @throws FrascatiException 
   */
  public final void launch(String compositeName)
      throws FrascatiException
  {
    this.compositeName = compositeName;
    launch();
  }

  /**
   * Invokes a {@link Method} on an SCA service exposed on the launched
   * composite
   * 
   * @param serviceName
   *          the service name of the SCA composite
   * @param methodName
   *          the method to invoke
   * @param responseType
   *          the response {@link Class} type
   * @param params
   *          parameters for <code>methodName</code>
   * @return the method result or null
   */
  @SuppressWarnings("unchecked")
  public final <T> T call(String serviceName, String methodName,
      Class<T> responseType, Object... params) throws FrascatiException
  {
    StringBuilder paramList = new StringBuilder();
    try {
      Object o = TinfiDomain
          .getService(scaComposite, Object.class, serviceName);
      Class<?> c = o.getClass();
      Class<?>[] paramTypes;
      boolean withParams;
      if (params == null) {
        paramTypes = new Class[0];
        withParams = false;
      } else {
        paramTypes = new Class[params.length];
        for (int i = 0; i < params.length; ++i) {
          paramTypes[i] = params[i].getClass();
          paramList.append(params[i]).append(' ');
        }
        withParams = true;
      }
      Method m = c.getMethod(methodName, paramTypes);

      log.info("Call service '" + serviceName + "' of " + compositeName
          + " - Method '" + methodName
          + (withParams ? ("' - Params: " + paramList) : "'"));
      return (T) m.invoke(o, params);
    } catch (NoSuchInterfaceException e) {
      warning(new FrascatiException("Unable to get the service '" + serviceName + "'", e));
      return null;
    } catch (SecurityException e) {
      warning(new FrascatiException("Unable to get the method '" + methodName + "'", e));
      return null;
    } catch (NoSuchMethodException e) {
      warning(new FrascatiException("The service '" + serviceName
          + "' does not provide the method " + methodName + '(' + paramList.toString() + ')', e));
      return null;
    } catch (IllegalArgumentException e) {
      warning(new FrascatiException(e));
      return null;
    } catch (IllegalAccessException e) {
      warning(new FrascatiException(e));
      return null;
    } catch (InvocationTargetException e) {
      warning(new FrascatiException(e));
      return null;
    }
  }
  
  /**
   * Close the launched composite.
   * 
   */
  public final void close()
  {
    try {
      log.info("Closing the SCA composite '" + compositeName + "'...");
      TinfiDomain.close(scaComposite);
    } catch (Exception exc) {
      log.log(Level.SEVERE, "Impossible to close the SCA composite '"
          + compositeName + "'!", exc);
    }
  }

}
