/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.Binding;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti SCA binding processor abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractBindingProcessor<BindingType extends Binding>
              extends AbstractProcessor<BindingType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** Java property name for configuring binding uri base. */
  public static final String BINDING_URI_BASE_PROPERTY_NAME = "org.ow2.frascati.binding.uri.base";

  /** Default value for binding uri base. */
  public static final String BINDING_URI_BASE_DEFAULT_VALUE = "http://localhost:18000";

  /** Empty value for binding uri base. */
  public static final String BINDING_URI_BASE_EMPTY_VALUE = "";

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected void toStringBuilder(BindingType binding, StringBuilder sb) {
    append(sb, "name", binding.getName());
    append(sb, "uri", binding.getUri());
    append(sb, "policySets", binding.getPolicySets());
    append(sb, "requires", binding.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  protected final void checkBinding(BindingType binding, ProcessingContext processingContext)
      throws ProcessorException
  {
    checkAttributeNotSupported(binding, "policySets", binding.getPolicySets() != null, processingContext);
    checkAttributeNotSupported(binding, "requires", binding.getRequires() != null, processingContext);
    checkChildrenNotSupported(binding, "sca:operation", binding.getOperation().size() > 0, processingContext);
  }

  /**
   * Test if the parent of a binding is a BaseService instance.
   *
   * @return true is the parent of a binding is a BaseService instance, else false.
   */
  protected final boolean hasBaseService(BindingType binding)
  {
    return binding.eContainer() instanceof BaseService;
  }

  /**
   * Get the BaseService parent of a binding.
   */
  protected final BaseService getBaseService(BindingType binding)
  {
    return getParent(binding, BaseService.class);
  }

  /**
   * Get the Java interface of the base service of a binding.
   */
  protected final Class<?> getBaseServiceJavaInterface(BindingType binding, ProcessingContext processingContext)
  {
    return AbstractInterfaceProcessor.getClass(getBaseService(binding).getInterface(), processingContext);
  }

  /**
   * Test if the parent of a binding is a BaseReference instance.
   *
   * @return true is the parent of a binding is a BaseReference instance, else false.
   */
  protected final boolean hasBaseReference(BindingType binding)
  {
    return binding.eContainer() instanceof BaseReference;
  }

  /**
   * Get the BaseReference parent of a binding.
   */
  protected final BaseReference getBaseReference(BindingType binding)
  {
    return getParent(binding, BaseReference.class);
  }

  /**
   * Get the Java interface of the base reference of a binding.
   */
  protected final Class<?> getBaseReferenceJavaInterface(BindingType binding, ProcessingContext processingContext)
  {
    return AbstractInterfaceProcessor.getClass(getBaseReference(binding).getInterface(), processingContext);
  }

  /**
   * Get the Fractal component for a binding.
   */
  protected final Component getFractalComponent(BindingType binding, ProcessingContext processingContext)
  {
    return processingContext.getData(binding.eContainer().eContainer(), Component.class);
  }

  /**
   * Complete binding URI with binding URI base.
   */
  protected final String completeBindingURI(BindingType binding)
  {
    String uri = binding.getUri();
    if(uri != null && uri.startsWith("/")) {
      uri = System.getProperty(BINDING_URI_BASE_PROPERTY_NAME, BINDING_URI_BASE_DEFAULT_VALUE) + uri;
    }
    return uri;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Set empty binding uri base.
   */
  public static void setEmptyBindingURIBase()
  {
	System.setProperty(BINDING_URI_BASE_PROPERTY_NAME, BINDING_URI_BASE_EMPTY_VALUE);
  }
}
