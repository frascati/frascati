/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor: Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.api;

import java.net.URL;

/**
 * OW2 FraSCAti Assembly Factory class loader management interface.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.5
 * @since 0.5
 */
public interface ClassLoaderManager {

  /**
   * Get the current class loader used by the OW2 FraSCAti Assembly Factory.
   *
   * @return the used class loader.
   */
  ClassLoader getClassLoader();

  /**
   * Set the class loader used by the OW2 FraSCAti Assembly Factory.
   *
   * @param classLoader the class loader to use.
   */
  void setClassLoader(ClassLoader classLoader);

  /**
   * Load libraries into the class loader of the OW2 FraSCAti Assembly Factory.
   *
   * @param urls URLs of the libraries to load.
   * @throws ManagerException thrown when a problem obscurs.
   */
  void loadLibraries(URL ... urls) throws ManagerException;

}
