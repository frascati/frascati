/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.emf.ecore.EObject;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * OW2 FraSCAti Assembly Factory intent processor abstract class.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version 1.3
 */
public abstract class AbstractIntentProcessor<EObjectType extends EObject>
              extends AbstractProcessor<EObjectType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Used to get intent composites by calling processComposite(QName, ProcessingContext).
   */
  @Reference(name = "intent-loader")
  private CompositeManager intentLoader;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Set required intents of an element.
   *
   * @param element the element (composite, component, service or reference).
   * @param requires list of required intents.
   * @param component the Fractal component.
   * @param processingContext the current processing context.
   * @throws ProcessorException thrown when a problem obscurs.
   */
  protected final void completeRequires(EObjectType element, List<QName> requires, Component component, ProcessingContext processingContext)
      throws ProcessorException
  {
    logDo(processingContext, element, "complete required intents");

	if(requires == null) {
      return;
	}

    // Get the intent controller of the given component.
    SCABasicIntentController intentController;
    try {
      intentController = (SCABasicIntentController) component.getFcInterface(SCABasicIntentController.NAME);
    } catch (NoSuchInterfaceException nsie) {
      severe(new ProcessorException(element, "Cannot get to the FraSCAti intent controller", nsie));
      return;
    }

    for (QName require : requires) {
      // Try to retrieve intent service from cache.
      Component intentComponent;
      try {
    	intentComponent = intentLoader.processComposite(require, processingContext);
      } catch(ManagerException me) {
        warning(new ProcessorException("Error while getting intent '" + require
                  + "'", me));
        return;
      }

      IntentHandler intentHandler;
      try {
        // Get the intent handler of the intent composite.
        intentHandler = (IntentHandler)intentComponent.getFcInterface("intent");
      } catch (NoSuchInterfaceException nsie) {
        warning(new ProcessorException(element, "Intent '" + require
              + "' does not provide an 'intent' service", nsie));
        return;
      } catch (ClassCastException cce) {
        warning(new ProcessorException(element, "Intent '" + require
                  + "' does not provide an 'intent' service of interface " +
                  IntentHandler.class.getCanonicalName(), cce));
        return;
      }

      try {
        // Add the intent handler.
    	logDo(processingContext, element, "adding intent handler");
        addIntentHandler(element, intentController, intentHandler);
    	logDone(processingContext, element, "adding intent handler");
      } catch (Exception e) {
        severe(new ProcessorException(element, "Intent '" + require + "' cannot be added", e));
        return;
      }
    }

    logDone(processingContext, element, "complete required intents");
  }
  
  /**
   * Register an intent handler for a given element.
   * 
   * @param element the element (composite, component, service or reference).
   * @param intentController the intent controller.
   * @param intentHandler the intent handler.
   * @throws Exception thrown when a problem obscurs.
   */
  protected abstract void addIntentHandler(EObjectType element,
      SCABasicIntentController intentController,
      IntentHandler intentHandler) throws Exception;

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
