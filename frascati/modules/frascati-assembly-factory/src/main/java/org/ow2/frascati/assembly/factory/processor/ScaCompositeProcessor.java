/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Property;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Wire;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.FactoryException;

/**
 * OW2 FraSCAti Assembly Factory SCA composite processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class ScaCompositeProcessor
     extends AbstractComponentProcessor<Composite, Service, Reference> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required component factory.
   */
  @org.osoa.sca.annotations.Reference(name = "component-factory")
  private ComponentFactory componentFactory;

  /**
   * The required component processor.
   */
  @org.osoa.sca.annotations.Reference(name = "component-processor")
  private Processor<org.eclipse.stp.sca.Component> componentProcessor;

  /**
   * The required composite service processor.
   */
  @org.osoa.sca.annotations.Reference(name = "service-processor")
  private Processor<Service> serviceProcessor;

  /**
   * The required composite reference processor.
   */
  @org.osoa.sca.annotations.Reference(name = "reference-processor")
  private Processor<Reference> referenceProcessor;

  /**
   * The required composite property processor.
   */
  @org.osoa.sca.annotations.Reference(name = "property-processor")
  private Processor<Property> propertyProcessor;

  /**
   * The required wire processor.
   */
  @org.osoa.sca.annotations.Reference(name = "wire-processor")
  private Processor<Wire> wireProcessor;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Composite composite, StringBuilder sb) {
    sb.append(SCA_COMPOSITE);
    append(sb, "name", composite.getName());
    append(sb, "autowire", composite.isSetAutowire(), composite.isAutowire());
    append(sb, "local", composite.isSetLocal(), composite.isLocal());
    append(sb, "targetNamespace", composite.getTargetNamespace());
    append(sb, "constrainingType", composite.getConstrainingType());
    append(sb, "policySets", composite.getPolicySets());
    append(sb, "requires", composite.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Composite composite, ProcessingContext processingContext)
      throws ProcessorException
  {
    // The OW2 FraSCAti Parser already manages:
    // - attribute 'autowire'
    // - attribute 'constrainingType'
    // - list of <sca:include>
    // So we needn't to redo these checkings.

    // Check the composite name.
    checkAttributeMustBeSet(composite, "name", composite.getName(), processingContext);

    // Check the composite local.
    checkAttributeNotSupported(composite, "local", composite.isSetLocal(), processingContext);

    // Check the composite target namespace.
    checkAttributeNotSupported(composite, "targetNamespace", composite.getTargetNamespace() != null, processingContext);

    // Check the composite policy sets.
    checkAttributeNotSupported(composite, "policySets", composite.getPolicySets() != null, processingContext);

    // TODO: 'requires' must be checked here.
    // Does each intent composite exist?
    // Is each composite an intent (service Intent of interface IntentHandler)?

    // Check that each component has a unique name and
    // store all <component name, component> tuples into a map of components.
    ComponentMap mapOfComponents = new ComponentMap();
    for(org.eclipse.stp.sca.Component component : composite.getComponent()) {
      String name = component.getName();
      if(name != null) {
        if(mapOfComponents.containsKey(name)) {
          error(processingContext, composite, "<component name='", name, "'> is already defined");
         } else {
           mapOfComponents.put(name, component);
         }
      }
    }
    // Store the map of components into the processing context.
    processingContext.putData(composite, ComponentMap.class, mapOfComponents);

    // Check the composite components.
    check(composite, SCA_COMPONENT, composite.getComponent(), componentProcessor, processingContext);

    // Check the composite services.
    check(composite, SCA_SERVICE, composite.getService(), serviceProcessor, processingContext);

    // Check the composite references.
    check(composite, SCA_REFERENCE, composite.getReference(), referenceProcessor, processingContext);

    // Check the composite properties.
    check(composite, SCA_PROPERTY, composite.getProperty(), propertyProcessor, processingContext);

    // Check the composite wires.
    check(composite, SCA_WIRE, composite.getWire(), wireProcessor, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(Composite composite, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate the composite container.
    try {
      logDo(processingContext, composite, "generate the composite container");
      componentFactory.generateScaCompositeMembrane(processingContext, null);
      logDone(processingContext, composite, "generate the composite container");
    } catch (FactoryException fe) {
      severe(new ProcessorException(composite,
	                "Unable to generate the composite container", fe));
      return;
    }

    // Generate the component type for this composite.
    // Also generate the composite services and references.
    ComponentType compositeComponentType =
      generateComponentType(composite, composite.getService(), serviceProcessor, composite.getReference(), referenceProcessor, processingContext);

    // Generating the composite.
    try {
      logDo(processingContext, composite, "generate the composite component");
	  componentFactory.generateScaCompositeMembrane(processingContext, compositeComponentType);
      logDone(processingContext, composite, "generate the composite component");
    } catch (FactoryException fe) {
      severe(new ProcessorException(composite,
                     "Unable to generate the composite component", fe));
      return;
    }

	// Generate the composite components.
    generate(composite, SCA_COMPONENT, composite.getComponent(), componentProcessor, processingContext);

    // Generate the composite properties.
    generate(composite, SCA_PROPERTY, composite.getProperty(), propertyProcessor, processingContext);

    // Generate the composite wires.
    generate(composite, SCA_WIRE, composite.getWire(), wireProcessor, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(Composite composite, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate the composite container.
    Component containerComponent = null;

    // Create a container for root composites only and
    // not for composites enclosed into composites.
    boolean isTheRootComposite = processingContext.getRootComposite() == composite;
    if(isTheRootComposite) {
      try {
        logDo(processingContext, composite, "instantiate the composite container");
        containerComponent = componentFactory.createScaCompositeComponent(processingContext, null);
        logDone(processingContext, composite, "instantiate the composite container");
      } catch (FactoryException fe) {
        severe(new ProcessorException(composite,
                     "Unable to create the composite container", fe));
        return;
      }

      // Set the name for the composite container.
      setComponentName(composite, "composite container", containerComponent, composite.getName() + "-container", processingContext);
    }

    // Retrieve the composite type from the processing context.
    ComponentType compositeComponentType = getFractalComponentType(composite, processingContext);

    // Instantiate the composite component.
    Component compositeComponent;
    try {
      logDo(processingContext, composite, "instantiate the composite component");
      compositeComponent = componentFactory.createScaCompositeComponent(processingContext, compositeComponentType);
      logDone(processingContext, composite, "instantiate the composite component");
    } catch (FactoryException fe) {
      error(processingContext, composite, "Unable to instantiate the composite: " + fe.getMessage() +
          ".\nPlease check that frascati-runtime-factory-<major>.<minor>.jar is present into your classpath.");
      throw new ProcessorException(composite, "Unable to instantiate the composite", fe);
    }

    // Keep the composite component into the processing context.
    processingContext.putData(composite, Component.class, compositeComponent);

    // Set the name for this assembly.
    setComponentName(composite, "composite", compositeComponent, composite.getName(), processingContext);

    // Add the SCA composite into the composite container.
    if(isTheRootComposite) {
      addFractalSubComponent(containerComponent, compositeComponent);
    }

    // Instantiate the composite components.
    instantiate(composite, SCA_COMPONENT, composite.getComponent(), componentProcessor, processingContext);

    // Instantiate the composite services.
    instantiate(composite, SCA_SERVICE, composite.getService(), serviceProcessor, processingContext);

    // Instantiate composite references.
    instantiate(composite, SCA_REFERENCE, composite.getReference(), referenceProcessor, processingContext);

    // Instantiate composite properties.
    instantiate(composite, SCA_PROPERTY, composite.getProperty(), propertyProcessor, processingContext);

    // Instantiate the composite wires.
    instantiate(composite, SCA_WIRE, composite.getWire(), wireProcessor, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Composite composite, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Complete the composite components.
    complete(composite, SCA_COMPONENT, composite.getComponent(), componentProcessor, processingContext);

    // Complete the composite services.
    complete(composite, SCA_SERVICE, composite.getService(), serviceProcessor, processingContext);

    // Complete the composite references.
    complete(composite, SCA_REFERENCE, composite.getReference(), referenceProcessor, processingContext);

    // Complete the composite properties.
    complete(composite, SCA_PROPERTY, composite.getProperty(), propertyProcessor, processingContext);

    // Complete the composite wires.
    complete(composite, SCA_WIRE, composite.getWire(), wireProcessor, processingContext);

    // Retrieve the composite component from the processing context.
    Component compositeComponent = getFractalComponent(composite, processingContext);

    // Complete required intents for the composite.
    completeRequires(composite, composite.getRequires(), compositeComponent, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(ScaPackage.Literals.COMPOSITE);
  }

}
