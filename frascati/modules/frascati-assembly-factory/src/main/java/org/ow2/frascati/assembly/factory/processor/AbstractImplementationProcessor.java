/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Implementation;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA implementation processor abstract class.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractImplementationProcessor<ImplementationType extends Implementation>
              extends AbstractProcessor<ImplementationType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected void toStringBuilder(ImplementationType implementation, StringBuilder sb) {
    append(sb, "policySets", implementation.getPolicySets());
    append(sb, "requires", implementation.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType, ProcessingContext)
   */
  protected final void checkImplementation(ImplementationType implementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the implementation policy sets.
	checkAttributeNotSupported(implementation, "policySets", implementation.getPolicySets() != null, processingContext);

    // Check the composite requires.
    checkAttributeNotSupported(implementation, "requires", implementation.getRequires() != null, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
