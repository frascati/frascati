/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 * 				   Christophe Demarey
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.SCAPropertyBase;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.api.Processor;

/**
 * OW2 FraSCAti Assembly Factory Java-based property type processor.
 *
 * Create Java instances for property values.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @author Christophe Demarey
 * @version 1.4
 * @since 1.1
 */
public class ScaPropertyTypeJavaProcessor
     extends AbstractPropertyTypeProcessor {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * A FraSCAti specific namespace for Java types.
   */
  public static final String OW2_FRASCATI_JAVA_NAMESPACE = "http://frascati.ow2.org/xmlns/java";

  /**
   * Enumeration of supported Java types.
   */
  enum JavaType {
    BIG_DECIMAL_OBJECT(BigDecimal.class),
    BIG_INTEGER_OBJECT(BigInteger.class),
    BOOLEAN_PRIMITIVE(boolean.class),
    BOOLEAN_OBJECT(Boolean.class),
	BYTE_PRIMITIVE(byte.class),
    BYTE_OBJECT(Byte.class),
    CLASS_OBJECT(Class.class),
    CHAR_PRIMITIVE(char.class),
    CHARACTER_OBJECT(Character.class),
    DOUBLE_PRIMITIVE(double.class),
    DOUBLE_OBJECT(Double.class),
    FLOAT_PRIMITIVE(float.class),
    FLOAT_OBJECT(Float.class),
    INT_PRIMITIVE(int.class),
    INTEGER_OBJECT(Integer.class),
    LONG_PRIMITIVE(long.class),
    LONG_OBJECT(Long.class),
    SHORT_PRIMITIVE(short.class),
    SHORT_OBJECT(Short.class),
    STRING_OBJECT(String.class),
    URI_OBJECT(URI.class),
    URL_OBJECT(URL.class),
    QNAME_OBJECT(QName.class);

    /**
     * Construct a Java type.
     */
    private JavaType(Class<?> clazz) {
      this.className = clazz.getCanonicalName();
    }

    /**
     * Store the Java type class name.
     */
    private String className;

    /**
     * Get the class name.
     */
    public final String getClassName()
    {
      return this.className;
    }

    /**
     * Map of the Java types.
     */
    public static final Map<String, JavaType> VALUES = new HashMap<String, JavaType>();
    static {
      VALUES.put(BIG_DECIMAL_OBJECT.getClassName(), BIG_DECIMAL_OBJECT);
      VALUES.put(BIG_INTEGER_OBJECT.getClassName(), BIG_INTEGER_OBJECT);
      VALUES.put(BOOLEAN_PRIMITIVE.getClassName(), BOOLEAN_PRIMITIVE);
      VALUES.put(BOOLEAN_OBJECT.getClassName(), BOOLEAN_OBJECT);
      VALUES.put(BYTE_PRIMITIVE.getClassName(), BYTE_PRIMITIVE);
      VALUES.put(BYTE_OBJECT.getClassName(), BYTE_OBJECT);
      VALUES.put(CLASS_OBJECT.getClassName(), CLASS_OBJECT);
      VALUES.put(CHAR_PRIMITIVE.getClassName(), CHAR_PRIMITIVE);
      VALUES.put(CHARACTER_OBJECT.getClassName(), CHARACTER_OBJECT);
      VALUES.put(DOUBLE_PRIMITIVE.getClassName(), DOUBLE_PRIMITIVE);
      VALUES.put(DOUBLE_OBJECT.getClassName(), DOUBLE_OBJECT);
      VALUES.put(FLOAT_PRIMITIVE.getClassName(), FLOAT_PRIMITIVE);
      VALUES.put(FLOAT_OBJECT.getClassName(), FLOAT_OBJECT);
      VALUES.put(INT_PRIMITIVE.getClassName(), INT_PRIMITIVE);
      VALUES.put(INTEGER_OBJECT.getClassName(), INTEGER_OBJECT);
      VALUES.put(LONG_PRIMITIVE.getClassName(), LONG_PRIMITIVE);
      VALUES.put(LONG_OBJECT.getClassName(), LONG_OBJECT);
      VALUES.put(SHORT_PRIMITIVE.getClassName(), SHORT_PRIMITIVE);
      VALUES.put(SHORT_OBJECT.getClassName(), SHORT_OBJECT);
      VALUES.put(STRING_OBJECT.getClassName(), STRING_OBJECT);
      VALUES.put(URI_OBJECT.getClassName(), URI_OBJECT);
      VALUES.put(URL_OBJECT.getClassName(), URL_OBJECT);
      VALUES.put(QNAME_OBJECT.getClassName(), QNAME_OBJECT);
    }

  }

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Does the given property type match a Java type?
   */
  protected final boolean isMatchingOneJavaType(QName propertyType) {
    // Matches if the property type is null, or
	// has a FraSCAti Java namespace and is a supported Java type.
    return ( propertyType == null ) ||
      ( propertyType.getNamespaceURI().equals(OW2_FRASCATI_JAVA_NAMESPACE)
        && JavaType.VALUES.get(propertyType.getLocalPart()) != null );
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SCAPropertyBase property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Obtain the property type for the given property.
	QName propertyType = processingContext.getData(property, QName.class);

	if(isMatchingOneJavaType(propertyType)) {
	  // This property type processor is attached to the given property.
      processingContext.putData(property, Processor.class, this);
      
      if(propertyType != null) {
    	// if the property type is set then compute the property value.
        doComplete(property, processingContext);
      }
	}
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(SCAPropertyBase property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // The property value as given in a .composite file.
    String propertyValue = property.getValue();

	// Obtain the property type for the given property.
	QName propertyType = processingContext.getData(property, QName.class);

	// Search which the Java type is.
	JavaType javaType = JavaType.VALUES.get(propertyType.getLocalPart());
	if(javaType == null) {
      // Should never happen but we never know!
	  severe(new ProcessorException(property,
          "Java type '" + propertyType.getLocalPart() + "' not supported"));
      return;
	}

	// Compute the property value.
	Object computedPropertyValue;
	try {
		computedPropertyValue = stringToValue(javaType, propertyValue, processingContext.getClassLoader());

	} catch(ClassNotFoundException cnfe) {
      error(processingContext, property, "Java class '", propertyValue, "' not found");
      return;
	} catch(URISyntaxException exc) {
      error(processingContext, property, "Syntax error in URI '", propertyValue, "'");
      return;
	} catch(MalformedURLException exc) {
      error(processingContext, property, "Malformed URL '", propertyValue, "'");
      return;
	} catch(NumberFormatException nfe) {
      error(processingContext, property, "Number format error in '", propertyValue, "'");
      return;
	}
	if (computedPropertyValue == null) {
        // Should never happen but we never know!
        severe(new ProcessorException("Java type known but not dealt by OW2 FraSCAti: " + propertyType));
        return;
	}

    // Attach the computed property value to the given property.
	processingContext.putData(property, Object.class, computedPropertyValue);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Get the SCA property object value from its string representation.
   * 
   * @param propertyType The property type name.
   * @param propertyValue The property value string representation.
   * @param cl The class loader to use if propertyType is of 'Class' type. Should be null if not used!
   */
  public static Object stringToValue(String propertyType, String propertyValue, ClassLoader cl)
  	throws ClassNotFoundException, URISyntaxException, MalformedURLException {
	  JavaType javaType = JavaType.VALUES.get(propertyType);
	  return stringToValue(javaType, propertyValue, cl);
  }
  
  /**
   * Get the SCA property object value from its string representation.
   * 
   * @param propertyType The property java type.
   * @param propertyValue The property value string representation.
   * @param cl The class loader to use if propertyType is of 'Class' type. Should be null if not used!
   */
  public static Object stringToValue(JavaType propertyType, String propertyValue, ClassLoader cl)
  	throws ClassNotFoundException, URISyntaxException, MalformedURLException {
	  Object computedPropertyValue;
	  
	  switch (propertyType) {
		  case BIG_DECIMAL_OBJECT:
			  computedPropertyValue = new BigDecimal(propertyValue);
			  break;
		  case BIG_INTEGER_OBJECT:
			  computedPropertyValue = new BigInteger(propertyValue);
			  break;
		  case BOOLEAN_PRIMITIVE:
		  case BOOLEAN_OBJECT:
			  computedPropertyValue = Boolean.valueOf(propertyValue);
			  break;
		  case BYTE_PRIMITIVE:
		  case BYTE_OBJECT:
			  computedPropertyValue = Byte.valueOf(propertyValue);
			  break;
		  case CLASS_OBJECT:
			  computedPropertyValue = cl.loadClass(propertyValue);
			  break;
		  case CHAR_PRIMITIVE:
		  case CHARACTER_OBJECT:
			  computedPropertyValue = propertyValue.charAt(0);
			  break;
		  case DOUBLE_PRIMITIVE:
		  case DOUBLE_OBJECT:
			  computedPropertyValue = Double.valueOf(propertyValue);
			  break;
		  case FLOAT_PRIMITIVE:
		  case FLOAT_OBJECT:
			  computedPropertyValue = Float.valueOf(propertyValue);
			  break;
		  case INT_PRIMITIVE:
		  case INTEGER_OBJECT:
			  computedPropertyValue = Integer.valueOf(propertyValue);
			  break;
		  case LONG_PRIMITIVE:
		  case LONG_OBJECT:
			  computedPropertyValue = Long.valueOf(propertyValue);
			  break;
		  case SHORT_PRIMITIVE:
		  case SHORT_OBJECT:
			  computedPropertyValue = Short.valueOf(propertyValue);
			  break;
		  case STRING_OBJECT:
			  computedPropertyValue = propertyValue;
			  break;
		  case URI_OBJECT:
			  computedPropertyValue = new URI(propertyValue);
			  break;
		  case URL_OBJECT:
			  computedPropertyValue = new URL(propertyValue);
			  break;
		  case QNAME_OBJECT:
			  computedPropertyValue = QName.valueOf(propertyValue);
			  break;
		  default:
			  return null;
	  }
	  return computedPropertyValue;
  }
}
