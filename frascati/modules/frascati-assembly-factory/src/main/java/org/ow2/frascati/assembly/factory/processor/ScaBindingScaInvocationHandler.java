/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Handler for Java.Proxy that acting as a SCA Binding
 * Because SCA binding is not support by the binding factory we use java Proxy to act as stub 
 *
 */
public class ScaBindingScaInvocationHandler implements InvocationHandler
{
    /**
     * The instance where invocations are delegated.
     */
    private Object delegate;

    /**
     * The path of the delegate service
     */
    private String bindingURI;
    
    /**
     * Set the delegate.
     */
    public final void setDelegate(Object delegate)
    {
      this.delegate = delegate;
    }
   
    public String getBindingURI()
    {
        return bindingURI;
    }

    public void setBindingURI(String bindingURI)
    {
        this.bindingURI = bindingURI;
    }
    
    /**
     * @see InvocationHandler#invoke(Object, Method, Object[])
     */
    public final Object invoke(Object proxy, Method method, Object[] args) throws Throwable
    {
      try
      {
          return method.invoke(this.delegate, args);
      }
      catch(InvocationTargetException e)
      {
          throw e.getCause();
      }
    }
}
