/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.api;

import java.util.List;

import org.eclipse.stp.sca.Composite;

import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * OW2 FraSCAti Assembly Factory SCA processing context interface.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public interface ProcessingContext extends ParsingContext, ComponentFactoryContext
{
    /**
     * Get the current processing mode for this processing context.
     * 
     * @return the current processing mode for this processing context.
     */
    ProcessingMode getProcessingMode();

    /**
     * Set the current processing mode for this processing context.
     * 
     * @param processingMode the current processing mode for this processing context.
     */
    void setProcessingMode(ProcessingMode processingMode);

    /**
     * Get the processed root SCA composite.
     * 
     * @return the processed root SCA composite.
     */
    Composite getRootComposite();

    /**
     * Set the processed root SCA composite.
     * 
     * @param composite the processed root SCA composite.
     */
    void setRootComposite(Composite composite);

    /**
     * Add a processed SCA composite to the processed composites
     * 
     * @param processedComposite the composite to store
     */
    void addProcessedComposite(Composite processedComposite);

    /**
     * Get a list of all Composite processed in this ProcessingContext
     * 
     * @return a list of all Composite processed in this ProcessingContext
     */
    List<Composite> getProcessedComposite();

    /**
     * Get a processed Composite named compositeName
     * 
     * @param compositeName the name of the searched composite
     * @return the processed composite named compositeName or null if not found
     */
    Composite getProcessedComposite(String compositeName);

}
