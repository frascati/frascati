/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.api;

import org.osoa.sca.annotations.Service;

/**
 * OW2 FraSCAti Assembly Factory SCA processor interface.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Service
public interface Processor<ElementType> {

  /**
   * Get the processor ID.
   *
   * @return the processor ID.
   */
  String getProcessorID();

  /**
   * Check an element.
   *
   * @param element the element to check.
   * @param processingContext the processing context to use.
   * @throws ProcessorException thrown when a problem obscurs.
   */
  void check(ElementType element,
      ProcessingContext processingContext) throws ProcessorException;

  /**
   * Generate an element.
   *
   * @param element the element to generate.
   * @param processingContext the processing context to use.
   * @throws ProcessorException thrown when a problem obscurs.
   */
  void generate(ElementType element,
      ProcessingContext processingContext) throws ProcessorException;

  /**
   * Instantiate an element.
   *
   * @param element the element to instantiate.
   * @param processingContext the processing context to use.
   * @return an instantiated element.
   * @throws ProcessorException thrown when a problem obscurs.
   */
  void instantiate(ElementType element,
      ProcessingContext processingContext) throws ProcessorException;

  /**
   * Complete an element.
   *
   * @param element the element to complete.
   * @param processingContext the processing context to use.
   * @throws ProcessorException thrown when a problem obscurs.
   */
  void complete(ElementType element,
      ProcessingContext processingContext) throws ProcessorException;

}
