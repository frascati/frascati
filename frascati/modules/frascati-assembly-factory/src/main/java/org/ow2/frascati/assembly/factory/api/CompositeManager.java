/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.api;

import java.net.URL;
import javax.xml.namespace.QName;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.annotations.Service;

/**
 * OW2 FraSCAti Assembly Factory SCA composite manager interface.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 * @since 0.6
 */
@Service
public interface CompositeManager {

  /**
   * Create a new processing context instance.
   *
   * @param urls the URLs to add to the class loader of the processing context.
   * @return a new processing context instance.
   * @since 1.4
   */
  ProcessingContext newProcessingContext(URL ... urls);

  /**
   * Create a new processing context instance.
   *
   * @param classLoader the class loader of the processing context.
   * @return a new processing context instance.
   * @since 1.5
   */
  ProcessingContext newProcessingContext(ClassLoader classLoader);
  
  /**
   * Get the ProcessingContext used to process the composite
   * 
   * @param compositeName the name of the composite
   * @return the ProcessingContext related to the composite or null if not found
   */
  ProcessingContext getProcessingContext(String compositeName);
  
  /**
   * Load a contribution ZIP archive.
   *
   * @param contribution name of the contribution file to load.
   * @return the set of loaded composites.
   * @throws ManagerException thrown when a problem occurs.
   * @since 1.1
   */
  Component[] getContribution(String contribution) throws ManagerException;

  /**
   * Process a contribution ZIP archive.
   *
   * @param contribution name of the contribution file to load.
   * @return the set of loaded composites.
   * @throws ManagerException thrown when a problem occurs.
   * @since 1.4
   */
  Component[] processContribution(String contribution, ProcessingContext processingContext) throws ManagerException;

  /**
   * Load an SCA composite and create the FraSCAti composite instance.
   *
   * @param composite
   *          name of the composite file to load, can be a resource for the
   *          current class path, a local file or an URL.
   * @return the resulting FraSCAti composite instance.
   * @throws ManagerException thrown when a problem occurs.
   */
  Component getComposite(String composite) throws ManagerException;

  /**
   * Load an SCA composite and create the FraSCAti composite instance.
   *
   * @param composite
   *          name of the composite file to load, can be a resource for the
   *          current class path, a local file or an URL.
   * @param libs list of JARs to load.
   * @return the resulting FraSCAti composite instance.
   * @throws ManagerException thrown when a problem occurs.
   */
  Component getComposite(String composite, URL[] libs) throws ManagerException;

  /**
   * Load an SCA composite and create the FraSCAti composite instance.
   *
   * @param composite
   *          name of the composite file to load, can be a resource for the
   *          current class path, a local file or an URL.
   * @param classLoader the class loader to use.
   * @return the resulting FraSCAti composite instance.
   * @throws ManagerException thrown when a problem occurs.
   * @since 1.5
   */
  Component getComposite(String composite, ClassLoader classLoader) throws ManagerException;

  /**
   * Load an SCA composite and create the associate FraSCAti composite instance.
   *
   * @param qname
   *          qname of the SCA composite to load, can be a resource for the
   *          current class path, a local file or a URL.
   * @return the resulting FraSCAti composite instance.
   * @throws ManagerException thrown when a problem occurs.
   */
  Component getComposite(QName qname) throws ManagerException;

  /**
   * Load an SCA composite and create the associate FraSCAti composite instance.
   *
   * @param qname
   *          qname of the SCA composite to load, can be a resource for the
   *          current class path, a local file or a URL.
   * @return the resulting FraSCAti composite instance.
   * @throws ManagerException thrown when a problem occurs.
   */
  Component processComposite(QName qname, ProcessingContext processingContext) throws ManagerException;

  /**
   * Register a composite to the top level domain composite.
   *
   * @param composite composite to register to the top level domain composite.
   * @throws ManagerException thrown when a problem occurs.
   */
  void addComposite(Component composite) throws ManagerException;

  /**
   * Remove a composite from the top level domain composite.
   *
   * @param name name of the composite to remove.
   * @throws ManagerException thrown when a problem occurs.
   */
  void removeComposite(String name) throws ManagerException;

  /**
   * Get all the composite names contained by the top level domain composite.
   * 
   * @return all the composite names contained by the top level domain composite.
   */
  String[] getCompositeNames();

  /**
   * Get all composites contained by the top level domain composite.
   *
   * @return all composites contained by the top level domain composite.
   */  
  Component[] getComposites();

  /**
   * Get the top level domain composite.
   *
   * @return the top level domain composite.
   */
  Component getTopLevelDomainComposite();

}
