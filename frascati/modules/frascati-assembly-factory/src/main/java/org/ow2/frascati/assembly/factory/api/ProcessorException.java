/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.api;

import org.ow2.frascati.util.FrascatiException;

/**
 * OW2 FraSCAti Assembly Factory processor exception.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ProcessorException extends FrascatiException {

  private static final long serialVersionUID = 1369416319243909331L;

  /**
   * The element associated to this exception.
   */
  private Object element;

  /**
   * Get the element associated to this exception.
   *
   * @return the element associated to this exception.
   */
  public final Object getElement() {
    return this.element;
  }

  /**
   * Construct a processor exception instance.
   *
   * @param element the element associated to this exception.
   */
  public ProcessorException(Object element) {
    super();
    this.element = element;
  }

  /**
   * Construct a processor exception instance.
   *
   * @param element the element associated to this exception.
   * @param message the message of this exception.
   */
  public ProcessorException(Object element, String message) {
    super(message);
    this.element = element;
  }

  /**
   * Constructs a processor exception instance.
   *
   * @param element the element associated to this exception.
   * @param message the message of this exception.
   * @param cause the cause of this exception.
   */
  public ProcessorException(Object element, String message, Throwable cause) {
    super(message, cause);
    this.element = element;
  }

  /**
   * Construct a processor exception instance.
   *
   * @param element the element associated to this exception.
   * @param cause the cause of this exception.
   */
  public ProcessorException(Object element, Throwable cause) {
    super(cause);
    this.element = element;
  }

}
