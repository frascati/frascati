/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaPackage;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA interface Java processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class ScaInterfaceJavaProcessor
     extends AbstractInterfaceProcessor<JavaInterface> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(JavaInterface javaInterface, StringBuilder sb) {
    sb.append("sca:interface.java");
    append(sb, "interface", javaInterface.getInterface());
    append(sb, "callbackInterface", javaInterface.getCallbackInterface());
    super.toStringBuilder(javaInterface, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(JavaInterface javaInterface, ProcessingContext processingContext)
      throws ProcessorException
  {
	if(javaInterface.getInterface() == null) {
      error(processingContext, javaInterface, "the attribute 'interface' must be set");
	} else {
      try {
        logDo(processingContext, javaInterface, "check the Java interface");
        Class<?> clazz = processingContext.loadClass(javaInterface.getInterface());
        logDone(processingContext, javaInterface, "check the Java interface");

        // Store the Java class into the processing context.
        storeJavaInterface(javaInterface, processingContext, javaInterface.getInterface(), clazz);

      } catch (ClassNotFoundException cnfe) {
	    error(processingContext, javaInterface,
	        "Java interface '", javaInterface.getInterface(), "' not found");
      }
    }

	// TODO: check that this is a Java interface and not a Java class.

	if(javaInterface.getCallbackInterface() != null) {
      try {
       logDo(processingContext, javaInterface, "check the Java callback interface");
       processingContext.loadClass(javaInterface.getCallbackInterface());
       logDone(processingContext, javaInterface, "check the Java callback interface");
      } catch (ClassNotFoundException cnfe) {
	    error(processingContext, javaInterface,
            "Java callback interface '", javaInterface.getCallbackInterface(), "' not found");
      }
	}
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.JAVA_INTERFACE);
  }

}
