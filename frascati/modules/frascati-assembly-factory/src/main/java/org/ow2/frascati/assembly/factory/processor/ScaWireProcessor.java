/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.Wire;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.parser.api.MultiplicityHelper;

/**
 * OW2 FraSCAti Assembly Factory SCA wire processor class.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class ScaWireProcessor
     extends AbstractProcessor<Wire>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Wire wire, StringBuilder sb)
  {
    sb.append(SCA_WIRE);
    append(sb, "source", wire.getSource());
    append(sb, "target", wire.getTarget());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Wire wire, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Get the map of components stored into the processing context.
    ComponentMap mapOfComponents = processingContext.getData(wire.eContainer(), ComponentMap.class);

	if(wire.getSource() == null) {
      error(processingContext, wire, "The attribute 'source' must be set");
    } else {
      if(wire.getSource2() == null) {
        // Retrieve both source component and reference.
        String source[] = wire.getSource().split("/");
        if(source.length != 2) {
          error(processingContext, wire, "The attribute 'source' must be 'componentName/referenceName'");
        } else {
          Component sourceComponent = mapOfComponents.get(source[0]);
          if(sourceComponent == null) {
            error(processingContext, wire, "Unknown source component '", source[0], "'");
          } else {
            ComponentReference sourceReference = null;
            for(ComponentReference reference : sourceComponent.getReference()) {
              if(source[1].equals(reference.getName())) {
                sourceReference = reference;
                break; // the for loop
              }
            }
            if(sourceReference == null) {
              error(processingContext, wire, "Unknown source reference '", source[1], "'");
            } else {
              wire.setSource2(sourceReference);
            }
          }
        }
      }
    }

	if(wire.getTarget() == null) {
      error(processingContext, wire, "The attribute 'target' must be set");    	  
    } else {
      if(wire.getTarget2() == null) {
        // Retrieve both target component and service.
        String target[] = wire.getTarget().split("/");
        if(target.length != 2) {
          error(processingContext, wire, "The attribute 'target' must be 'componentName/serviceName'");
        } else {
          org.eclipse.stp.sca.Component targetComponent = mapOfComponents.get(target[0]);
          if(targetComponent == null) {
            error(processingContext, wire, "Unknown target component '", target[0], "'");
          } else {
            ComponentService targetService = null;
            for(ComponentService service : targetComponent.getService()) {
              if(target[1].equals(service.getName())) {
                targetService = service;
                break; // the for loop
              }
            }
            if(targetService == null) {
              error(processingContext, wire, "Unknown target service '", target[1], "'");
            } else {
              wire.setTarget2(targetService);
            }
          }
        }
      }
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Wire wire, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve source reference, source reference name, source reference multiplicity,
    // source component, and source Fractal component.
    ComponentReference sourceReference = wire.getSource2();
    String sourceReferenceName = sourceReference.getName();
    Component sourceComponent = (Component)sourceReference.eContainer();
    org.objectweb.fractal.api.Component sourceComponentInstance =
      getFractalComponent(sourceComponent, processingContext);

    // Retrieve target service, target service name, target component, and target Fractal component.
    ComponentService targetService = wire.getTarget2();
    String targetServiceName = targetService.getName();
    Component targetComponent = (Component)targetService.eContainer();
    org.objectweb.fractal.api.Component targetComponentInstance =
      getFractalComponent(targetComponent, processingContext);

    // when the source reference multiplicity is 0..n or 1..n
    // then add the target component name to the reference source name.
    // In this way, each Fractal collection client interface has a unique name.
    if(MultiplicityHelper.is0Nor1N(sourceReference.getMultiplicity())) {
      sourceReferenceName = sourceReferenceName + '-' + targetComponent.getName();

      // When the source component implementation is a composite then
      // propagate the binding inside the composite, i.e.,
      // bind the promoted references.
      if(sourceComponent.getImplementation() instanceof SCAImplementation) {
        // Find the composite implementation.
        Composite composite = processingContext.getData(sourceComponent.getImplementation(), Composite.class);
        // Search the target reference inside the composite.
        String referenceName = sourceReference.getName();
        Reference targetReference = null;
        for(Reference reference : composite.getReference()) {
          if(referenceName.equals(reference.getName())) {
            targetReference = reference;
            break;
          }
        }
        // Bind component references promoted by the composite reference.
        for(ComponentReference promotedReference :
            processingContext.getData(targetReference, ComponentReference[].class))
        {
          org.objectweb.fractal.api.Component promotedComponent =
              getFractalComponent(promotedReference.eContainer(), processingContext);
          try {
            bindFractalComponent(
              promotedComponent,
              promotedReference.getName() + '-' + sourceReferenceName,
              getFractalInternalInterface(sourceComponentInstance, sourceReferenceName));
          } catch (Exception e) {
            severe(new ProcessorException(wire, "Cannot etablish " + toString(wire) + " inside the composite", e));
            return;
          }
        }
      }
    }

    // etablish a Fractal binding between source reference and target service.
    try {
      bindFractalComponent(
          sourceComponentInstance,
          sourceReferenceName,
          getFractalInterface(targetComponentInstance, targetServiceName));
    } catch (Exception e) {
      severe(new ProcessorException(wire, "Cannot etablish " + toString(wire), e));
      return;
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(ScaPackage.Literals.WIRE);
  }

}
