/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.BaseService;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.TypeFactory;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * OW2 FraSCAti Assembly Factory SCA component processor implementation.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public abstract class AbstractComponentProcessor
                          <ElementType extends EObject,
                           ServiceType extends BaseService,
                           ReferenceType extends BaseReference>
              extends AbstractIntentProcessor<ElementType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required component factory type factory.
   */
  @Reference(name = "type-factory")
  private TypeFactory typeFactory;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see AbstractIntentProcessor#addIntentHandler(ElementType, SCABasicIntentController, IntentHandler)
   */
  @Override
  protected final void addIntentHandler(ElementType element,
      SCABasicIntentController intentController,
      IntentHandler intentHandler)
      throws IllegalLifeCycleException
  {
    intentController.addFcIntentHandler(intentHandler);
  }

  /**
   * Create the Fractal component type from the component definition in the model.
   * 
   * @param element the current element
   * @param services list of services
   * @param references list of references
   * @param processingContext the current processing context
   * @return created component type
   * @throws ProcessException thrown when a problem obscurs.
   */
  protected final ComponentType generateComponentType(
          ElementType element,
          List<ServiceType> services,
          Processor<ServiceType> serviceProcessor,
          List<ReferenceType> references,
          Processor<ReferenceType> referenceProcessor,
          ProcessingContext processingContext)
      throws ProcessorException
  {
	logDo(processingContext, element, "generate the component type");

	// Create a list of Fractal interface types.
    List<InterfaceType> interfaceTypes = new ArrayList<InterfaceType>();

    // Create interface types for services.
    for (ServiceType cs : services) {
      serviceProcessor.generate(cs, processingContext);
      interfaceTypes.add(processingContext.getData(cs, InterfaceType.class));
    }

    // Create interface types for references.
    for (ReferenceType cr : references) {
      referenceProcessor.generate(cr, processingContext);
      interfaceTypes.add(processingContext.getData(cr, InterfaceType.class));
    }

    InterfaceType[] itftype = interfaceTypes.toArray(new InterfaceType[interfaceTypes.size()]);

    ComponentType componentType;
    try {
      logDo(processingContext, element, "generate the associated component type");
      componentType = typeFactory.createComponentType(itftype);
      logDone(processingContext, element, "generate the associated component type");
    } catch(FactoryException te) {
      severe(new ProcessorException(element, "component type creation for " + toString(element) + " failed", te));
      return null;
    }

    // Keep the component type into the processing context.
    processingContext.putData(element, ComponentType.class, componentType);
    
    logDone(processingContext, element, "generate the component type");
    return componentType;
  }

  /**
   * Set the name of a component.
   */
  protected final void setComponentName(ElementType element, String whatComponent, Component component, String name, ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "set the component name of the " + whatComponent + " to '" + name + "'";
    logDo(processingContext, element, message);
    // set the name of the component.
    setFractalComponentName(component, name);
    logDone(processingContext, element, message);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
