/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.Multiplicity;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * OW2 FraSCAti Assembly Factory base reference intent processor abstract class.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractBaseReferencePortIntentProcessor<EObjectType extends BaseReference>
              extends AbstractPortIntentProcessor<EObjectType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  // Inspired from Tinfi class EMFParserSupportImpl
  // but keys are Multiplicity instead of String
  // and key null values are defined also.

  private static final Map<Multiplicity,Boolean> CONTINGENCIES =
        new HashMap<Multiplicity,Boolean>() {
            private static final long serialVersionUID = 1306190174200744738L;
        {
            put(null,             TypeFactory.MANDATORY);
            put(Multiplicity._01, TypeFactory.OPTIONAL);
            put(Multiplicity._11, TypeFactory.MANDATORY);
            put(Multiplicity._0N, TypeFactory.OPTIONAL);
            put(Multiplicity._1N, TypeFactory.MANDATORY);
        }};

  private static final Map<Multiplicity, Boolean> CARDINALITIES =
        new HashMap<Multiplicity, Boolean>() {
            private static final long serialVersionUID = 1306190174200744738L;
        {
            put(null,             TypeFactory.SINGLE);
            put(Multiplicity._01, TypeFactory.SINGLE);
            put(Multiplicity._11, TypeFactory.SINGLE);
            put(Multiplicity._0N, TypeFactory.COLLECTION);
            put(Multiplicity._1N, TypeFactory.COLLECTION);
        }};

   //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see AbstractIntentProcessor#addIntentHandler(EObjectType, SCABasicIntentController, IntentHandler)
   */
  @Override
  protected final void addIntentHandler(EObjectType baseReference,
      SCABasicIntentController intentController,
      IntentHandler intentHandler)
      throws NoSuchInterfaceException, IllegalLifeCycleException
  {
    intentController.addFcIntentHandler(intentHandler, baseReference.getName());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  protected final void checkBaseReference(EObjectType baseReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the base reference name.
    checkAttributeMustBeSet(baseReference, "name", baseReference.getName(), processingContext);

    // Check the base reference policy sets.
    checkAttributeNotSupported(baseReference, "policySets", baseReference.getPolicySets() != null, processingContext);

    // Check the base reference wiredByImpl.
    checkAttributeNotSupported(baseReference, "wiredByImpl", baseReference.isSetWiredByImpl(), processingContext);

    // TODO: 'requires' must be checked here.
    // Does the intent composite exist?
    // Is this composite an intent (service Intent of interface IntentHandler)?

    // Check the base reference interface.
    checkMustBeDefined(baseReference, SCA_INTERFACE, baseReference.getInterface(), getInterfaceProcessor(), processingContext);

    // TODO check callback
    // interfaceProcessor.check(baseReference.getCallback(), processingContext);

    // Check the base reference bindings.
    check(baseReference, SCA_BINDING, baseReference.getBinding(), getBindingProcessor(), processingContext);
  }
  
  /**
   * Generate the interface type associated to this base reference.
   */
  private void generateInterfaceType(EObjectType baseReference, ProcessingContext processingContext)
    throws ProcessorException
  {
    // Generate the base reference interface.
    generate(baseReference, SCA_INTERFACE, baseReference.getInterface(), getInterfaceProcessor(), processingContext);

	// Corresponding Java interface class name.
	String interfaceClassName = processingContext.getData(baseReference.getInterface(), JavaClass.class)
	                            .getClassName();

	// Name for the generated interface type.
    String interfaceName = baseReference.getName();

    Multiplicity multiplicity = baseReference.getMultiplicity();

    // Indicates if the interface is mandatory or optional.
    boolean contingency = CONTINGENCIES.get(multiplicity);

    // indicates if component interface is single or collection.
    boolean cardinality = CARDINALITIES.get(multiplicity);

    // Create the Fractal interface type.
    InterfaceType interfaceType;
    String logMessage = "create a Fractal client interface type " + interfaceClassName + " for "
                      + interfaceName + " with contingency = " + contingency
                      + " and cardinality = " + cardinality;
    try {
      logDo(processingContext, baseReference, logMessage);
      interfaceType = getTypeFactory().createInterfaceType(interfaceName, interfaceClassName,
	    		  TypeFactory.CLIENT, contingency, cardinality);
      logDone(processingContext, baseReference, logMessage);
    } catch (FactoryException te) {
      severe(new ProcessorException(baseReference, "Could not " + logMessage, te));
      return;
    }

    // Keep the Fractal interface type into the processing context.
    processingContext.putData(baseReference, InterfaceType.class, interfaceType);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  protected final void completeBaseReference(EObjectType baseReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Complete the composite reference interface.
    complete(baseReference, SCA_INTERFACE, baseReference.getInterface(), getInterfaceProcessor(), processingContext);

    // Complete the composite reference bindings.
    complete(baseReference, SCA_BINDING, baseReference.getBinding(), getBindingProcessor(), processingContext);

    // Retrieve the component from the processing context.
	Component component = getFractalComponent(baseReference.eContainer(), processingContext);

    // Complete the composite reference required intents.
	completeRequires(baseReference, baseReference.getRequires(), component, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(EObjectType baseReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate the base reference interface type.
    generateInterfaceType(baseReference, processingContext);

    // Generate the base reference bindings.
    generate(baseReference, SCA_BINDING, baseReference.getBinding(), getBindingProcessor(), processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(EObjectType baseReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate the base reference interface.
    instantiate(baseReference, SCA_INTERFACE, baseReference.getInterface(), getInterfaceProcessor(), processingContext);

    // Instantiate the base reference bindings.
    instantiate(baseReference, SCA_BINDING, baseReference.getBinding(), getBindingProcessor(), processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
