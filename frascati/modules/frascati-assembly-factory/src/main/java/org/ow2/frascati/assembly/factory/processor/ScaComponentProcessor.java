/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.type.ComponentType;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA component processor implementation.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaComponentProcessor
     extends AbstractComponentProcessor<Component, ComponentService, ComponentReference> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The component implementation processor.
   */
  @Reference(name = "implementation-processor")
  private Processor<Implementation> implementationProcessor;

  /**
   * The component service processor.
   */
  @Reference(name = "service-processor")
  private Processor<ComponentService> serviceProcessor;

  /**
   * The component reference processor.
   */
  @Reference(name = "reference-processor")
  private Processor<ComponentReference> referenceProcessor;

  /**
   * The component property processor.
   */
  @Reference(name = "property-processor")
  private Processor<PropertyValue> propertyProcessor;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Component component, StringBuilder sb) {
    sb.append(SCA_COMPONENT);
    append(sb, "name", component.getName());
    append(sb, "autowire", component.isSetAutowire(), component.isAutowire());
    append(sb, "constrainingType", component.getConstrainingType());
    append(sb, "policySets", component.getPolicySets());
    append(sb, "requires", component.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Component component, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the component's name.
    checkAttributeMustBeSet(component, "name", component.getName(), processingContext);

    // Check the component's policy sets.
    checkAttributeNotSupported(component, "policySets", component.getPolicySets() != null, processingContext);

    // Check the component's implementation.
    checkMustBeDefined(component, SCA_IMPLEMENTATION, component.getImplementation(), implementationProcessor, processingContext);      

    // Check the component's services.
    check(component, SCA_SERVICE, component.getService(), serviceProcessor, processingContext);

    // Check the component's references.
    check(component, SCA_REFERENCE, component.getReference(), referenceProcessor, processingContext);

    // Check the component's properties.
    check(component, SCA_PROPERTY, component.getProperty(), propertyProcessor, processingContext);

    // TODO: 'requires' must be checked here.
    // Does the intent composite exist?
    // Is this composite an intent (service Intent of interface IntentHandler)?
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(Component component, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate the component type.
    // Also generate the component services and references.
    ComponentType componentType = generateComponentType(component,
         component.getService(), serviceProcessor, component.getReference(), referenceProcessor, processingContext);

    // Keep the component type into the processing context.
    processingContext.putData(component.getImplementation(), ComponentType.class, componentType);

    // Generate the component's implementation.
    generate(component, SCA_IMPLEMENTATION, component.getImplementation(), implementationProcessor, processingContext);

    // Generate the component's properties.
    generate(component, SCA_PROPERTY, component.getProperty(), propertyProcessor, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(Component component, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate the component's implementation.
    instantiate(component, SCA_IMPLEMENTATION, component.getImplementation(), implementationProcessor, processingContext);

  	org.objectweb.fractal.api.Component componentInstance =
  	    getFractalComponent(component.getImplementation(), processingContext);

  	// Add the new component to its enclosing composite.
    org.objectweb.fractal.api.Component enclosingComposite =
        getFractalComponent(component.eContainer(), processingContext);
    addFractalSubComponent(enclosingComposite, componentInstance);

    // set the name for this component.
    setComponentName(component, "component", componentInstance, component.getName(), processingContext);

    // Keep the component instance into the processing context.
    processingContext.putData(component, org.objectweb.fractal.api.Component.class, componentInstance);

    // Instantiate the component's services.
    instantiate(component, SCA_SERVICE, component.getService(), serviceProcessor, processingContext);

    // Instantiate the component's references.
    instantiate(component, SCA_REFERENCE, component.getReference(), referenceProcessor, processingContext);

    // Instantiate the component's properties.
    instantiate(component, SCA_PROPERTY, component.getProperty(), propertyProcessor, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Component component, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Complete the component's implementation.
    complete(component, SCA_IMPLEMENTATION, component.getImplementation(), implementationProcessor, processingContext);

    // Complete the component's services.
    complete(component, SCA_SERVICE, component.getService(), serviceProcessor, processingContext);

    // Complete the component's references.
    complete(component, SCA_REFERENCE, component.getReference(), referenceProcessor, processingContext);

    // Complete the component's properties.
    complete(component, SCA_PROPERTY, component.getProperty(), propertyProcessor, processingContext);

    // Retrieve the component from the processing context.
    org.objectweb.fractal.api.Component componentInstance =
        getFractalComponent(component, processingContext);

    // Complete required intents for the component.
    completeRequires(component, component.getRequires(), componentInstance, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.COMPONENT);
  }

}
