/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 * 
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.parser.api.MultiplicityHelper;

/**
 * OW2 FraSCAti Assembly Factory SCA component reference processor class.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaComponentReferenceProcessor
     extends AbstractBaseReferencePortIntentProcessor<ComponentReference> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(ComponentReference componentReference, StringBuilder sb) {
    sb.append(SCA_REFERENCE);
    append(sb, "name", componentReference.getName());
    append(sb, "target", componentReference.getTarget());
    append(sb, "autowire", componentReference.isSetAutowire(), componentReference.isAutowire());
    append(sb, "multiplicity", componentReference.isSetMultiplicity(), componentReference.getMultiplicity().getLiteral());
    append(sb, "wiredByImpl", componentReference.isSetWiredByImpl(), componentReference.isWiredByImpl());
    append(sb, "policySets", componentReference.getPolicySets());
    append(sb, "requires", componentReference.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(ComponentReference componentReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // List for storing the target component services.
    List<ComponentService> targetComponentServices = new ArrayList<ComponentService>();

    // Check the attribute 'target'.
    String targetAttribute = componentReference.getTarget();
    if(targetAttribute == null) {
      // Let's note that this is not an error as a wire could have this reference as source.
      // TODO: perhaps a trace should be reported, or better
      // all component references without target must be stored in an array, and
      // check at the end of ScaCompositeProcessor.check() if these are promoted
      // by composite references or the source of a wire.
      // An error must be thrown for component references not promoted or wired.
    } else {
      // Retrieve the map of components for the composite of this reference.    	
	  ComponentMap mapOfComponents = processingContext.getData(componentReference.eContainer().eContainer(), ComponentMap.class);
      for (String targetValue : targetAttribute.split("\\s")) {
      	// Retrieve both targetted component and reference.
        String target[] = targetValue.split("/");
        if(target.length != 2) {
          error(processingContext, componentReference, "The value '", targetValue, "' must be 'componentName/serviceName'");
        } else {
          org.eclipse.stp.sca.Component targetComponent = mapOfComponents.get(target[0]);
          if(targetComponent == null) {
            error(processingContext, componentReference, "Unknown target component '", target[0], "'");
          } else {
            ComponentService targetComponentService = null;
            for(ComponentService cs : targetComponent.getService()) {
              if(target[1].equals(cs.getName())) {
                targetComponentService = cs;
                break; // the for loop
              }
            }
            if(targetComponentService == null) {
              error(processingContext, componentReference, "Unknown target service '", target[1], "'");
            } else {
              targetComponentServices.add(targetComponentService);
            }
          }
        }
      }
    }
    // store the target component services to be dealt later in method complete().
    processingContext.putData(componentReference, ComponentService[].class,
        targetComponentServices.toArray(new ComponentService[targetComponentServices.size()]));
  
    // Check this component reference as a base reference.
    checkBaseReference(componentReference, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(ComponentReference componentReference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve the component from the processing context.
	org.eclipse.stp.sca.Component scaComponent = (org.eclipse.stp.sca.Component)componentReference.eContainer();
	Component component = getFractalComponent(scaComponent, processingContext);

	// Etablish the binding to all target component services.
    for(ComponentService targetComponentService :
        processingContext.getData(componentReference, ComponentService[].class))
    {
      org.eclipse.stp.sca.Component targetComponent =
          (org.eclipse.stp.sca.Component)targetComponentService.eContainer();
      String targetComponentServiceName = targetComponentService.getName();
      Component targetComponentInstance = getFractalComponent(targetComponent, processingContext);

      String sourceReferenceName = componentReference.getName();

      // when the source reference multiplicity is 0..n or 1..n
      // and the source component implementation is a composite then
      // propagate the binding inside the composite, i.e.,
      // bind the promoted references.
      if(MultiplicityHelper.is0Nor1N(componentReference.getMultiplicity())) {

    	sourceReferenceName = sourceReferenceName + '-' + targetComponent.getName();

    	if(scaComponent.getImplementation() instanceof SCAImplementation) {
          // Find the composite implementation.
          Composite composite = processingContext.getData(scaComponent.getImplementation(), Composite.class);
          // Search the target reference inside the composite.
          String referenceName = componentReference.getName();
          Reference targetReference = null;
          for(Reference reference : composite.getReference()) {
            if(referenceName.equals(reference.getName())) {
              targetReference = reference;
              break;
            }
          }
          // Bind component references promoted by the composite reference.
          for(ComponentReference promotedReference :
              processingContext.getData(targetReference, ComponentReference[].class))
          {
            org.objectweb.fractal.api.Component promotedComponent =
              getFractalComponent(promotedReference.eContainer(), processingContext);
            try {
              bindFractalComponent(
                promotedComponent,
                promotedReference.getName() + '-' + sourceReferenceName,
                getFractalInternalInterface(component, sourceReferenceName));
            } catch (Exception e) {
              severe(new ProcessorException(componentReference, "Cannot etablish " + toString(componentReference) + " inside the composite", e));
              return;
            }
          }
        }
      }

      // Etablish a Fractal binding between the reference and the target component service.
      try {
        bindFractalComponent(
          component,
          sourceReferenceName,
    	  getFractalInterface(targetComponentInstance, targetComponentServiceName));
      } catch (Exception e) {
        severe(new ProcessorException(componentReference, "Can't promote " + toString(componentReference), e));
        return;
      }
    }

    // Complete this component reference as a base reference.
    completeBaseReference(componentReference, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.COMPONENT_REFERENCE);
  }

}
