/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 * 
 * Contributor(s): Nicolas Dolet, Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Property;
import org.eclipse.stp.sca.ScaPackage;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA composite property processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaCompositePropertyProcessor
     extends AbstractPropertyProcessor<Property> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Property property, StringBuilder sb) {
    sb.append(SCA_PROPERTY);
    append(sb, "name", property.getName());
    append(sb, "type", property.getType());
    append(sb, "many", property.isSetMany(), property.isMany());
    append(sb, "mustSupply", property.isSetMustSupply(), property.isMustSupply());
    append(sb, "element", property.getElement());
    super.toStringBuilder(property, sb);
 }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Property property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the property attribute 'name'.
    checkAttributeMustBeSet(property, "name", property.getName(), processingContext);

    // Check the property value.
//
// Until FraSCAti 1.4, the value of a composite property is mandatory.
//
//    logDo(processingContext, property, "check the property value");
//    if(property.getValue() == null) {
//      error(processingContext, property, "The property value must be set");
//    }
//    logDone(processingContext, property, "check the property value");
//
// Since FraSCAti 1.5, the value of a composite property is optional
// in order to pass the OASIS CSA Assembly Model Test Suite.
//

    // Check the property attribute 'many'.
    checkAttributeNotSupported(property, "many", property.isSetMany(), processingContext);

    // Check the property attribute 'mustSupply'.
    checkAttributeNotSupported(property, "mustSupply", property.isSetMustSupply(), processingContext);

    // TODO: check attribute 'promote': Has the enclosing composite a property of this name?
	    
    // Check the component property type and value.
    QName propertyType = (property.getElement() != null) ? property.getElement() : property.getType();
    checkProperty(property, propertyType, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Property property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Since FraSCAti 1.5, the value of a composite property is optional
    // in order to pass the OASIS CSA Assembly Model Test Suite.
    if(property.getValue() != null) {
      // Set the composite property value.
      setProperty(property, property.getName(), processingContext);
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.PROPERTY);
  }

}
