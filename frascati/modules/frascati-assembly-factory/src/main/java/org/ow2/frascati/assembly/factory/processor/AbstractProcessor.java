/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.List;
import static java.util.logging.Level.FINE;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;

import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.util.AbstractFractalLoggeable;

/**
 * OW2 FraSCAti Assembly Factory abstract processor class.
 *
 * This class provides facilities inherited by all FraSCAti Assembly Factory processors.
 * These facilities are internal methods related to:
 * - EMF EObject and EClass instances
 * - OW2 Fractal Component and Controller instances
 * - Logging
 * - Error and warning reporting
 * - Processing actions
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public abstract class AbstractProcessor<EObjectType extends EObject>
              extends AbstractFractalLoggeable<ProcessorException>
           implements Processor<EObjectType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  protected static final String SCA_COMPOSITE = "sca:composite";
	  
  protected static final String SCA_COMPONENT = "sca:component";
	  
  protected static final String SCA_IMPLEMENTATION = "sca:implementation";

  protected static final String SCA_SERVICE = "sca:service";

  protected static final String SCA_REFERENCE = "sca:reference";

  protected static final String SCA_INTERFACE = "sca:interface";

  protected static final String SCA_BINDING = "sca:binding";

  protected static final String SCA_PROPERTY = "sca:property";

  protected static final String SCA_OPERATION = "sca:operation";

  protected static final String SCA_WIRE = "sca:wire";

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see AbstractFractalLoggeable#newException(String)
   */
  @Override
  protected final ProcessorException newException(String message)
  {
    return new ProcessorException(null, message);
  }

  /**
   * Test if a string is null or empty.
   */
  protected static boolean isNullOrEmpty(String value)
  {
    return value == null || value.equals("");
  }

  /**
   * Format reporting messages.
   *
   * @param processingContext the current processing context.
   * @param element the element.
   * @param messages the reporting messages.
   */
  private String toString(ProcessingContext processingContext, EObjectType element, String ... messages)
  {
    StringBuilder sb = new StringBuilder();
    sb.append(processingContext.getLocationURI(element));
    sb.append(": ");
    internalToStringBuilder(element, sb);
    sb.append(" - ");
    for(String msg : messages) {
      sb.append(msg);
    }
    return sb.toString();
  }

  //---------------------------------------------------------------------------
  // Internal EMF methods.
  // --------------------------------------------------------------------------

  /**
   * Return an ID for a given Element in the SCA Model.
   * 
   * @param eclass
   * @return String value calculated using EClass package and name.
   */
  protected static String getID(EClass eclass)
  {
    return eclass.getEPackage().getNsURI() + "#" + eclass.getName();
  }

  /**
   * Return an ID for a given Element in the SCA Model.
   * 
   * @param eObj
   * @return String value calculated using EClass package and name.
   */
  protected static String getID(EObject eObj)
  {
    return getID(eObj.eClass());
  }

  /**
   * Get the parent of a given model element.
   */
  @SuppressWarnings("unchecked")
  protected final <T> T getParent(EObjectType eObj, Class<T> clazz)
  {
    return (T)(eObj.eContainer());
  }

  //---------------------------------------------------------------------------
  // Internal Processor error/warning reporting methods.
  // --------------------------------------------------------------------------

  /**
   * Report an error.
   *
   * @param processingContext the current processing context.
   * @param element the element causing the error.
   * @param messages the error messages to report.
   */
  protected final void error(ProcessingContext processingContext, EObjectType element, String ... messages)
  {
    processingContext.error(toString(processingContext, element, messages));
  }

  /**
   * Report a warning.
   *
   * @param processingContext the current processing context.
   * @param element the element causing the warning.
   * @param messages the warning messages to report.
   */
  protected final void warning(ProcessingContext processingContext, EObjectType element, String ... messages)
  {
    processingContext.warning(toString(processingContext, element, messages));
  }

  //---------------------------------------------------------------------------
  // Internal logging methods.
  // --------------------------------------------------------------------------

  /**
   * Log something on an element.
   *
   * @param processingContext the current processing context.
   * @param element the element.
   * @param message the message to log.
   */
  protected final void logFine(ProcessingContext processingContext, EObjectType element, String message)
  {
    if (this.log.isLoggable(FINE)) {
      log.fine(toString(processingContext, element, message));
    }
  }

  /**
   * Log do on an element.
   *
   * @param processingContext the current processing context.
   * @param element the element.
   * @param message the message to log.
   */
  protected final void logDo(ProcessingContext processingContext, EObjectType element, String message)
  {
    if (this.log.isLoggable(FINE)) {
      log.fine(toString(processingContext, element, message, "..."));
    }
  }

  /**
   * Log done on an element.
   *
   * @param processingContext the current processing context.
   * @param element the element.
   * @param message the message to log.
   */
  protected final void logDone(ProcessingContext processingContext, EObjectType element, String message)
  {
    if (this.log.isLoggable(FINE)) {
      log.fine(toString(processingContext, element, message, " done."));
    }
  }

  //---------------------------------------------------------------------------
  // Internal methods related to ProcessingContext.
  // --------------------------------------------------------------------------

  /**
   * Get the {@link Component} instance associated to an {@link EObject} instance.
   */
  protected final Component getFractalComponent(EObject eObject, ProcessingContext processingContext)
  {
    return processingContext.getData(eObject, Component.class);
  }

  /**
   * Get the {@link ComponentType} instance associated to an {@link EObject} instance.
   */
  protected final ComponentType getFractalComponentType(EObject eObject, ProcessingContext processingContext)
  {
    return processingContext.getData(eObject, ComponentType.class);
  }

  //---------------------------------------------------------------------------
  // Internal Processor methods.
  // --------------------------------------------------------------------------

  /**
   * Get a string representation of an element.
   *
   * @param element the element to append.
   */
  protected final String toString(EObjectType element)
  {
    StringBuilder sb = new StringBuilder();
    internalToStringBuilder(element, sb);
    return sb.toString();
  }

  private void internalToStringBuilder(EObjectType element, StringBuilder sb)
  {
    sb.append('<');
    try {
      toStringBuilder(element, sb);
    } catch(Exception exception) {
      log.warning("Thrown " + exception.toString());
      sb.append(" ...");
    } catch(Error error) {
      log.warning("Thrown " + error.toString());
      sb.append(" ...");
    }
    sb.append('>');
  }

  /**
   * Append an EObjectType instance into a string builder.
   *
   * @param element the element to append.
   * @param sb the string builder.
   */
  protected abstract void toStringBuilder(EObjectType element, StringBuilder sb);

  protected static final String EQUALS_TO = "=\"";

  /**
   * Append an integer attribute to a string builder.
   */
  protected static void append(StringBuilder sb, String name, int value)
  {
    sb.append(' ').append(name).append(EQUALS_TO).append(value).append('\"');
  }

  /**
   * Append a string attribute to a string builder.
   */
  protected static void append(StringBuilder sb, String name, String value)
  {
    if(value != null) {
      sb.append(' ').append(name).append(EQUALS_TO).append(value).append('\"');
    }
  }

  /**
   * Append an Object attribute to a string builder.
   */
  protected static void append(StringBuilder sb, String name, Object value)
  {
    if(value != null) {
      sb.append(' ').append(name).append(EQUALS_TO).append(value).append('\"');
    }
  }

  /**
   * Append an Object attribute to a string builder.
   */
  protected static <T> void append(StringBuilder sb, String name, List<T> list)
  {
    if(list != null) {
      sb.append(' ').append(name).append('=');
      char c = '\"';
      for(T item : list) {
        sb.append(c);
        sb.append(item);
        c = ' ';
      }
      sb.append('\"');
    }
  }

  /**
   * Append an optional boolean attribute to a string builder.
   */
  protected static void append(StringBuilder sb, String name, boolean condition, boolean value)
  {
    if(condition) {
      sb.append(' ').append(name).append(EQUALS_TO).append(value).append('\"');
    }
  }

  /**
   * Append an optional string attribute to a string builder.
   */
  protected static void append(StringBuilder sb, String name, boolean condition, String value)
  {
    if(condition) {
      sb.append(' ').append(name).append(EQUALS_TO).append(value).append('\"');
    }
  }

  protected final void checkAttributeMustBeSet(EObjectType element,
                                         String attributeName,
                                         Object value,
                                         ProcessingContext processingContext)
  {
    String message = "check the attribute '" + attributeName + "'";
    logDo(processingContext, element, message);
    if(value == null) {
      error(processingContext, element, "The attribute '", attributeName, "' must be set");
    }
    logDone(processingContext, element, message);
  }

  protected final void checkAttributeMustBeSet(EObjectType element,
                                         String attributeName,
                                         String value,
                                         ProcessingContext processingContext)
  {
    String message = "check the attribute '" + attributeName + "'";
    logDo(processingContext, element, message);
    if(isNullOrEmpty(value)) {
      error(processingContext, element, "The attribute '", attributeName, "' must be set");
    }
    logDone(processingContext, element, message);
  }

  protected final void checkAttributeNotSupported(EObjectType element,
                                            String attributeName,
                                            boolean condition,
                                            ProcessingContext processingContext)
  {
    String message = "check the attribute '" + attributeName + "'";
    logDo(processingContext, element, message);
    if(condition) {
      warning(processingContext, element, "The attribute '", attributeName, "' is not supported by OW2 FraSCAti");      
    }
    logDone(processingContext, element, message);
  }

  protected final void checkChildrenNotSupported(EObjectType element,
                                           String childName,
                                           boolean condition,
                                           ProcessingContext processingContext)
  {
    String message = "check list of <" + childName + '>';
    logDo(processingContext, element, message);
    if(condition) {
      warning(processingContext, element, "<", childName, "> is not supported by OW2 FraSCAti");      
    }
    logDone(processingContext, element, message);
  }

  protected final <T> void checkMustBeDefined(EObjectType element,
                                        String childName,
                                        T item,
                                        Processor<T> processor,
                                        ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "check <" + childName + '>';
    logDo(processingContext, element, message);
    if(item == null) {
      error(processingContext, element, "<", childName, "> must be defined");      
    } else {
      processor.check(item, processingContext);
    }
    logDone(processingContext, element, message);
  }

  protected final <T> void check(EObjectType element,
                           String childName,
                           List<T> list,
                           Processor<T> processor,
                           ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "check list of <" + childName + '>';
    logDo(processingContext, element, message);
    for (T item : list) {
      processor.check(item, processingContext);
    }
    logDone(processingContext, element, message);
  }

  protected final <T> void generate(EObjectType element,
                              String childName,
                              T item,
                              Processor<T> processor,
                              ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "generate <" + childName + '>';
    logDo(processingContext, element, message);
    processor.generate(item, processingContext);
    logDone(processingContext, element, message);
  }

  protected final <T> void generate(EObjectType element,
                              String childName,
                              List<T> list,
                              Processor<T> processor,
                              ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "generate list of <" + childName + '>';
    logDo(processingContext, element, message);
    for (T item : list) {
      processor.generate(item, processingContext);
    }
    logDone(processingContext, element, message);
  }

  protected final <T> void instantiate(EObjectType element,
                                 String childName,
                                 T item,
                                 Processor<T> processor,
                                 ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "instantiate <" + childName + '>';
    logDo(processingContext, element, message);
    processor.instantiate(item, processingContext);
    logDone(processingContext, element, message);
  }

  protected final <T> void instantiate(EObjectType element,
                                 String childName,
                                 List<T> list,
                                 Processor<T> processor,
                                 ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "instantiate list of <" + childName + '>';
    logDo(processingContext, element, message);
    for (T item : list) {
      processor.instantiate(item, processingContext);
     }
    logDone(processingContext, element, message);
  }

  protected final <T> void complete(EObjectType element,
                              String childName,
                              T item,
                              Processor<T> processor,
                              ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "complete <" + childName + '>';
    logDo(processingContext, element, message);
    processor.complete(item, processingContext);
    logDone(processingContext, element, message);
  }

  protected final <T> void complete(EObjectType element,
                              String childName,
                              List<T> list,
                              Processor<T> processor,
                              ProcessingContext processingContext)
      throws ProcessorException
  {
    String message = "complete list of <" + childName + '>';
    logDo(processingContext, element, message);
    for (T item : list) {
      processor.complete(item, processingContext);
    }
    logDone(processingContext, element, message);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  protected void doCheck(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "nothing to check");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  protected void doGenerate(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "nothing to generate");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  protected void doInstantiate(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "nothing to instantiate");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  protected void doComplete(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "nothing to complete");
  }

  //---------------------------------------------------------------------------
  // Public org.ow2.frascati.assembly.factory.api.Processor methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  public final void check(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "check...");
    doCheck(element, processingContext);
    logFine(processingContext, element, "checked.");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  public final void generate(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "generate...");
    doGenerate(element, processingContext);
    logFine(processingContext, element, "successfully generated.");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  public final void instantiate(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "instantiate...");
    doInstantiate(element, processingContext);
    logFine(processingContext, element, "successfully instantiated.");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  public final void complete(EObjectType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, element, "complete...");
    doComplete(element, processingContext);
    logFine(processingContext, element, "successfully completed.");
  }

}
