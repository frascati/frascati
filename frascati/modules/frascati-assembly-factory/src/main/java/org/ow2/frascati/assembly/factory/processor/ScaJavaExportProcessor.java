/**
 * OW2 FraSCAti Assembly Factory
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.ExportJavaType;
import org.eclipse.stp.sca.ScaPackage;

/**
*
* Processor for SCA java exports
*
*/
public class ScaJavaExportProcessor extends AbstractImportExportJavaProcessor<ExportJavaType>
{
    
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.manager.importexport.ImportExportProcessor#getProcessorID()
     */
    public String getProcessorID()
    {
        return getID(ScaPackage.Literals.EXPORT_JAVA_TYPE);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.processor.AbstractImportExportJavaProcessor#getPackageName(org.eclipse.emf.ecore.EObject)
     */
    @Override
    protected String getPackageName(ExportJavaType element)
    {
        return element.getPackage();
    }
}
