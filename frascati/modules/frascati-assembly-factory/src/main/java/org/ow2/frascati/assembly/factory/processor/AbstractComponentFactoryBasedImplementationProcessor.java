/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Implementation;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.FactoryException;

/**
 * OW2 FraSCAti Assembly Factory abstract Component Factory -based implementation processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe MErle</a>
 * @version 1.3
 */
public abstract class AbstractComponentFactoryBasedImplementationProcessor<ImplementationType extends Implementation>
              extends AbstractImplementationProcessor<ImplementationType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required component factory.
   */
  @Reference(name = "component-factory")
  private ComponentFactory componentFactory;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Generate a FraSCAti SCA primitive component.
   *
   * @param implementation the SCA implementation of the FraSCAti SCA primitive component.
   * @param processingContext the processing context.
   * @param className the class name implementing the FraSCAti SCA primitive component.
   *
   * @since 1.4
   */
  protected final void generateScaPrimitiveComponent(ImplementationType implementation,
		                                             ProcessingContext processingContext,
		                                             String className)
      throws ProcessorException
  {
    try {
      logDo(processingContext, implementation, "generate the implementation");
      ComponentType componentType = getFractalComponentType(implementation, processingContext);
      getComponentFactory().generateScaPrimitiveMembrane(processingContext, componentType, className);
      logDone(processingContext, implementation, "generate the implementation");
    } catch(FactoryException fe) {
      severe(new ProcessorException(implementation, "generation failed", fe));
      return;
    }
  }

  /**
   * Instantiate a FraSCAti SCA primitive component.
   *
   * @param implementation the SCA implementation of the FraSCAti SCA primitive component.
   * @param processingContext the processing context.
   * @param className the class name implementing the FraSCAti SCA primitive component.
   *
   * @since 1.4
   */
  protected final Component instantiateScaPrimitiveComponent(ImplementationType implementation,
                                                        ProcessingContext processingContext,
                                                        String className)
      throws ProcessorException
  {
    Component component;
    try {
      logDo(processingContext, implementation, "instantiate the implementation");
      ComponentType componentType = getFractalComponentType(implementation, processingContext);
      // create instance of an SCA primitive component for this implementation.
      component = getComponentFactory().createScaPrimitiveComponent(processingContext, componentType, className);
      logDone(processingContext, implementation, "instantiate the implementation");
   	} catch(FactoryException fe) {
      severe(new ProcessorException(implementation, "instantiation failed", fe));
      return null;
    }
   	processingContext.putData(implementation, Component.class, component);
   	return component;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Get the component factory.
   */
  public final ComponentFactory getComponentFactory()
  {
    return this.componentFactory;
  }

}
