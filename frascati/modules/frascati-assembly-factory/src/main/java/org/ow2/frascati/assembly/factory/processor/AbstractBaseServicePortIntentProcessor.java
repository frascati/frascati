/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.BaseService;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * OW2 FraSCAti Assembly Factory base service intent processor abstract class.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractBaseServicePortIntentProcessor<EObjectType extends BaseService>
              extends AbstractPortIntentProcessor<EObjectType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see AbstractIntentProcessor#addIntentHandler(EObjectType, SCABasicIntentController, IntentHandler)
   */
  @Override
  protected final void addIntentHandler(EObjectType baseService,
      SCABasicIntentController intentController,
      IntentHandler intentHandler)
      throws NoSuchInterfaceException, IllegalLifeCycleException
  {
    intentController.addFcIntentHandler(intentHandler, baseService.getName());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  protected final void checkBaseService(EObjectType baseService, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the base service name.
    checkAttributeMustBeSet(baseService, "name", baseService.getName(), processingContext);

    // Check the base service policy sets.
    checkAttributeNotSupported(baseService, "policySets", baseService.getPolicySets() != null, processingContext);

    // TODO: 'requires' must be checked here.
    // Does the intent composite exist?
    // Is this composite an intent (service Intent of interface IntentHandler)?

    // Check the base service interface.
    checkMustBeDefined(baseService, SCA_INTERFACE, baseService.getInterface(), getInterfaceProcessor(), processingContext);

    // TODO interfaceProcessor.check(baseService.getCallback(), processingContext);
    
    // Check the base service operations.
    checkChildrenNotSupported(baseService, SCA_OPERATION, baseService.getOperation().size() > 0, processingContext);
    // TODO operationProcessor.check(baseService.getOperation(), processingContext);

    // Check the base service bindings.
    check(baseService, SCA_BINDING, baseService.getBinding(), getBindingProcessor(), processingContext);
  }

  /**
   * Generate the interface type associated to this base reference.
   */
  private void generateInterfaceType(EObjectType baseService, ProcessingContext processingContext)
    throws ProcessorException
  {
    // Generate the base service interface.
    generate(baseService, SCA_INTERFACE, baseService.getInterface(), getInterfaceProcessor(), processingContext);

	// Corresponding Java interface class name.
	String interfaceClassName = processingContext.getData(baseService.getInterface(), JavaClass.class)
	                            .getClassName();

	// Name for the generated interface type.
    String interfaceName = baseService.getName();

    // Indicates if the interface is mandatory or optional.
    boolean contingency = TypeFactory.MANDATORY;

    // indicates if component interface is single or collection.
    boolean cardinality = TypeFactory.SINGLE;

    // Create the Fractal interface type.
    InterfaceType interfaceType;
    String logMessage = "create a Fractal server interface type " + interfaceClassName + " for "
                      + interfaceName + " with contingency = " + contingency
                      + " and cardinality = " + cardinality;
    try {
      logDo(processingContext, baseService, logMessage);
      interfaceType = getTypeFactory().createInterfaceType(interfaceName, interfaceClassName,
	    		  TypeFactory.SERVER, contingency, cardinality);
      logDone(processingContext, baseService, logMessage);
    } catch (FactoryException te) {
      severe(new ProcessorException(baseService, "Could not " + logMessage, te));
      return;
    }

    // Keep the Fractal interface type into the processing context.
    processingContext.putData(baseService, InterfaceType.class, interfaceType);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  protected final void completeBaseService(EObjectType baseService, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Complete the base service interface.
    complete(baseService, SCA_INTERFACE, baseService.getInterface(), getInterfaceProcessor(), processingContext);

    // Complete the base service bindings.
    complete(baseService, SCA_BINDING, baseService.getBinding(), getBindingProcessor(), processingContext);

    // Retrieve the component from the processing context.
	Component component = getFractalComponent(baseService.eContainer(), processingContext);

	// Complete the base service required intents.
	completeRequires(baseService, baseService.getRequires(), component, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(EObjectType baseService, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate the base service interface type.
    generateInterfaceType(baseService, processingContext);

    // Generate the base service bindings.
    generate(baseService, SCA_BINDING, baseService.getBinding(), getBindingProcessor(), processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(EObjectType baseService, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate the base service interface.
    instantiate(baseService, SCA_INTERFACE, baseService.getInterface(), getInterfaceProcessor(), processingContext);

    // Instantiate the base service bindings.
    instantiate(baseService, SCA_BINDING, baseService.getBinding(), getBindingProcessor(), processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
