/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA composite service processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaCompositeServiceProcessor
     extends AbstractBaseServicePortIntentProcessor<Service> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Service service, StringBuilder sb) {
    sb.append(SCA_SERVICE);
    append(sb, "name", service.getName());
    append(sb, "promote", service.getPromote());
    append(sb, "policySets", service.getPolicySets());
    append(sb, "requires", service.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Service service, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the attribute 'promote'.
    if(service.getPromote() == null) {
      error(processingContext, service, "The attribute 'promote' must be set");    	  
    } else {
      if(service.getPromote2() == null) {
        // Retrieve both target component and service.
        String promote[] = service.getPromote().split("/");
        if(promote.length != 2) {
          error(processingContext, service, "The attribute 'promote' must be 'componentName/serviceName'");
        } else {
          org.eclipse.stp.sca.Component promotedComponent =
              processingContext.getData(service.eContainer(), ComponentMap.class).get(promote[0]);
          if(promotedComponent == null) {
            error(processingContext, service, "Unknown promoted component '", promote[0], "'");
          } else {
            ComponentService promotedService = null;
            for(ComponentService cs : promotedComponent.getService()) {
              if(promote[1].equals(cs.getName())) {
                promotedService = cs;
                break; // the for loop
              }
            }
            if(promotedService == null) {
              error(processingContext, service, "Unknown promoted service '", promote[1], "'");
            } else {
              service.setPromote2(promotedService);
            }
          }
        }
      }
    }

    // Check the composite service as a base service.
    checkBaseService(service, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Service service, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Retrieve the component from the processing context.
	Component component = getFractalComponent(service.eContainer(), processingContext);

	// Retrieve the promoted component service, and the promoted Fractal component.
    ComponentService promotedComponentService = service.getPromote2();
    Component promotedComponentInstance =
        getFractalComponent(promotedComponentService.eContainer(), processingContext);

    // Etablish a Fractal binding between the composite service and the promoted component service.
    try {
      bindFractalComponent(
          component,
          service.getName(),
    	  getFractalInterface(promotedComponentInstance, promotedComponentService.getName()));
    } catch (Exception e) {
      severe(new ProcessorException(service, "Can't promote " + toString(service), e));
      return ;
    }

    // Complete the composite service as a base service.
    completeBaseService(service, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.SERVICE);
  }

}
