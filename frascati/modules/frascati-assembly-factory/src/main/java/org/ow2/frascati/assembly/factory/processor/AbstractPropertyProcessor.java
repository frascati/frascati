/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 * 
 * Contributor(s): Nicolas Dolet, Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.List;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.SCAPropertyBase;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * OW2 FraSCAti Assembly Factory abstract property processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractPropertyProcessor<PropertyType extends SCAPropertyBase>
              extends AbstractProcessor<PropertyType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * List of property type processors.
   */
  @Reference(name="property-types")
  private List<Processor<PropertyType>> propertyTypeProcessors;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected void toStringBuilder(PropertyType property, StringBuilder sb) {
    if(property.getValue() != null) {
      sb.append('>');
      sb.append(property.getValue());
      sb.append("</sca:property");
    }
  }

  /**
   * Check a property.
   */
  protected final void checkProperty(PropertyType property, QName propertyType, ProcessingContext processingContext)
    throws ProcessorException
  {
    // Associate the property type to the property.
    processingContext.putData(property, QName.class, propertyType);

    // Search which property type processor matches the given property type.
    boolean isPropertyTypeMatchedByOnePropertyTypeProcessor = false;
    for(Processor<PropertyType> propertyTypeProcessor : propertyTypeProcessors) {
      // check the property type and value.
      propertyTypeProcessor.check(property, processingContext);

      if(processingContext.getData(property, Processor.class) != null) {
        // the current property type processor matched the property type.
        isPropertyTypeMatchedByOnePropertyTypeProcessor = true;
    	break; // the for loop.
      }
    }

    // Produce an error when no property processor type matches the property type.
    if(!isPropertyTypeMatchedByOnePropertyTypeProcessor) {
      error(processingContext, property, "Unknown property type '", propertyType.toString(), "'");
    }
  }

  /**
   * Set a property.
   */
  protected final void setProperty(PropertyType property, String propertyName, ProcessingContext processingContext)
    throws ProcessorException
  {
    // Retrieve the Fractal component associated to the SCA composite/component of the given property.
	Component component = getFractalComponent(property.eContainer(), processingContext);

    // Retrieve the SCA property controller of this Fractal component.
    SCAPropertyController propertyController;
    try {
      propertyController = (SCAPropertyController)component.getFcInterface(SCAPropertyController.NAME);
    } catch (NoSuchInterfaceException nsie) {
      severe(new ProcessorException(property, "Can't get the SCA property controller", nsie));
      return;
    }

    // Retrieve the property value set by the property type processor.
    Object propertyValue = processingContext.getData(property, Object.class);

    if(propertyValue == null) {
      // The property type processor didn't compute a property value during check()
      // Then it is needed to call complete() to compute this property value.

      // Retrieve the property type associated to this property.
      QName propertyType = processingContext.getData(property, QName.class);

      if(propertyType == null) {
        // When no property type then get the property type via the property controller.
        Class<?> clazz = propertyController.getType(propertyName);
        if(clazz == null) {
    	  // When no property type defined then consider that it is String
          clazz = String.class;
        }
        // Reput the property type into the processing context.
        processingContext.putData(property, QName.class,
            new QName(ScaPropertyTypeJavaProcessor.OW2_FRASCATI_JAVA_NAMESPACE, clazz.getCanonicalName()));
      }

      // Retrieve the property type processor set during the method checkProperty()
      @SuppressWarnings("unchecked")
      Processor<PropertyType> propertyTypeProcessor = (Processor<PropertyType>)
          processingContext.getData(property, Processor.class);

      // Compute the property value.
      propertyTypeProcessor.complete(property, processingContext);

      // Retrieve the property value set by the property type processor.
      propertyValue = processingContext.getData(property, Object.class);
    }

    // set the property value and type via the property controller.
    propertyController.setValue(propertyName, propertyValue);
    propertyController.setType(propertyName, propertyValue.getClass());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
