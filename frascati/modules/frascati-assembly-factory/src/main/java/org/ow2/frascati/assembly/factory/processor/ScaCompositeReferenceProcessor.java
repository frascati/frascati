/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 * 
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.parser.api.MultiplicityHelper;

/**
 * OW2 FraSCAti Assembly Factory SCA composite reference processor class.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaCompositeReferenceProcessor
     extends AbstractBaseReferencePortIntentProcessor<Reference> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(Reference reference, StringBuilder sb) {
    sb.append(SCA_REFERENCE);
    append(sb, "name", reference.getName());
    append(sb, "promote", reference.getPromote());
/*
TODO: Currently implemented by the used Eclipse STP SCA metamodel.
TODO: Will be removed in the next version of this metamodel.
*/
    append(sb, "target", reference.getTarget());
/*
TODO: Not implemented by the currently used Eclipse STP SCA metamodel.
TODO: Will be added in the next version of the metamodel.

    append(sb, "autowire", reference.isSetAutowire(), reference.isAutowire());
*/
    append(sb, "multiplicity", reference.isSetMultiplicity(), reference.getMultiplicity().getLiteral());
    append(sb, "wiredByImpl", reference.isSetWiredByImpl(), reference.isWiredByImpl());
    append(sb, "policySets", reference.getPolicySets());
    append(sb, "requires", reference.getRequires());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(Reference reference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // List for storing the promoted component references.
    List<ComponentReference> promotedComponentReferences = new ArrayList<ComponentReference>();

    // Check the attribute 'promote'.
    String promoteAttribute = reference.getPromote();
    if(promoteAttribute == null) {
      warning(processingContext, reference, "The attribute 'promote' is not set");    	  
    } else {
      // Retrieve the map of components for the composite of this reference.    	
      ComponentMap mapOfComponents = processingContext.getData(reference.eContainer(), ComponentMap.class);
      for (String promoteValue : promoteAttribute.split("\\s")) {
      	// Retrieve both promoted component and reference.
        String promoted[] = promoteValue.split("/");
        if(promoted.length != 2) {
          error(processingContext, reference, "The value '", promoteValue, "' must be 'componentName/referenceName'");
        } else {
          org.eclipse.stp.sca.Component promotedComponent = mapOfComponents.get(promoted[0]);
          if(promotedComponent == null) {
            error(processingContext, reference, "Unknown promoted component '", promoted[0], "'");
          } else {
            ComponentReference promotedComponentReference = null;
            for(ComponentReference cr : promotedComponent.getReference()) {
              if(promoted[1].equals(cr.getName())) {
                promotedComponentReference = cr;
                break; // the for loop
              }
            }
            if(promotedComponentReference == null) {
              error(processingContext, reference, "Unknown promoted reference '", promoted[1], "'");
            } else {
              promotedComponentReferences.add(promotedComponentReference);
            }
          }
        }
      }
    }
    // store the promoted component references to be dealt later in method complete().
    processingContext.putData(reference, ComponentReference[].class,
        promotedComponentReferences.toArray(new ComponentReference[promotedComponentReferences.size()]));

    // Check this composite reference as a base reference.
    checkBaseReference(reference, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(Reference reference, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Only bind references with multiplicity equals to 0..1 or 1..1.
    // Don't bind references with multiplicity equals to 0..N or 1..N.
	if(MultiplicityHelper.is01or11(reference.getMultiplicity())) {

      // Retrieve the component from the processing context.
      Component component = getFractalComponent(reference.eContainer(), processingContext);

      // Promote component references.
      for(ComponentReference componentReference :
          processingContext.getData(reference, ComponentReference[].class))
      {
        Component promotedComponent = getFractalComponent(componentReference.eContainer(), processingContext);
        bindFractalComponent(
          promotedComponent,
          componentReference.getName(),
          getFractalInternalInterface(component, reference.getName()));
      }
	}

    // Complete this composite reference as a base reference.
    completeBaseReference(reference, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.REFERENCE);
  }

}
