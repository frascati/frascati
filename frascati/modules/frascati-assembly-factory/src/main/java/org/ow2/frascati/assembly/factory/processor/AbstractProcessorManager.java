/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import org.osoa.sca.annotations.Init;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory abstract processor manager class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractProcessorManager<ElementType extends EObject>
              extends AbstractProcessor<ElementType>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * List of processors.
   */
  private List<Processor<ElementType>> processors = null;

  /**
   * Map indexing processors by their processor ID.
   */
  private Map<String, Processor<ElementType>> processorsByID = null;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Set the list of processors.
   */
  protected final void setProcessors(List<Processor<ElementType>> processors) 
  {
	  this.processors = processors;
	  this.processorsByID = null;
  }

  /**
   * Get the processor associated to a given EObject.
   */
  protected final Processor<ElementType> getProcessor(ElementType eObj)
      throws ProcessorException
  {
    if(processorsByID == null) {
      initializeProcessorsByID();
    }

    // retrieve registered processor associated to eObj.
    Processor<ElementType> processor = processorsByID.get(getID(eObj));
    if(processor == null) {
      severe(new ProcessorException(eObj, "No processor for " + getID(eObj)));
      return null;
    }
    return processor;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(ElementType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    getProcessor(element).check(element, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(ElementType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    getProcessor(element).generate(element, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(ElementType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    getProcessor(element).instantiate(element, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(ElementType element, ProcessingContext processingContext)
      throws ProcessorException
  {
    getProcessor(element).complete(element, processingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initialize the map of processors indexed by their id.
   */
  @Init
  public final void initializeProcessorsByID()
  {
	this.processorsByID = new HashMap<String, Processor<ElementType>>();
    for (Processor<ElementType> processor : this.processors) {
    	this.processorsByID.put(processor.getProcessorID(), processor);
    }
  }

}
