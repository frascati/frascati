/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2009-2012 Inria, SARDES
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Valerio Schiavoni
 * 
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.assembly.factory;

import java.io.IOException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Logger;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.util.FrascatiException;

/**
 * Launcher for SCA composite applications. It uses attributes in the
 * META-INF/MANIFEST.MF file of a jar archive to initialize and run the
 * {@link Launcher}. This class can be used as value for the Main-Class
 * attribute of a manifest file. It supports 3 parameters:
 * <ol>
 * <li> <code>mainComposite</code>: the name of the composite file,</li>
 * <li> <code>mainService</code> (optional): the name of the main service on the
 * mainComposite,</li>
 * <li> <code>mainMethod</code> (optional): the name of the main method, to be
 * provided by the main service</li>
 * </ol>
 * A well-formed manifest file looks like the following:<br/>
 * <div> <code>
 * Manifest-Version: 1.0 <br/>
 * Archiver-Version: Plexus Archiver <br/>
 * Created-By: Apache Maven <br/>
 * Built-By: vschiavoni <br/>
 * Build-Jdk: 1.6.0_13 <br/>
 * Main-Class: org.ow2.frascati.factory.ManifestLauncher <br/>
 * mainComposite: app
 *</code> </div> <br/>
 * This example doesn't show the two optional parameters. <br/>
 * To automatically create manifest files with the supported attributes, you can
 * use tools like Maven2 or Ant.
 * 
 * <h6>Maven2</h6> Using Maven, it is possible to specify custom attributes for
 * Manifest files via the 'assembly' plugin or the 'jar' plugin.
 * 
 * <h6>Ant</h6> Using Ant, it is possible to specify custom attributes for
 * Manifest files via configuring the Jar task.
 * 
 * @author Valerio Schiavoni
 * @see http://java.sun.com/j2se/1.3/docs/guide/jar/jar.html#Main%20Attributes
 * @see http://maven.apache.org/plugins/maven-assembly-plugin/
 * @see http://ant.apache.org/manual/CoreTasks/jar.html
 */
public class ManifestLauncher
{
    /**
     * List of Java system properties for configuring FraSCAti.
     * @since 1.5
     */
    static final String[][] FRASCATI_PROPERTY_NAMES = {
        { FraSCAti.FRASCATI_CLASS_PROPERTY_NAME,                   "FraSCAti-Class" },
        { FraSCAti.FRASCATI_BOOTSTRAP_PROPERTY_NAME,               "FraSCAti-Bootstrap" },
        { FraSCAti.FRASCATI_COMPOSITE_PROPERTY_NAME,               "FraSCAti-Composite" },
        { AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, "FraSCAti-Binding-Uri-Base" }
    };

	private ManifestLauncher()
	{
	}

	public static void main(String[] args) throws IOException
	{
		// mainComposite : the name of the main composite file
		// mainService: the name of the main service
		// mainMethod: the name of the method to call

		/*
		 * different approaches in reading the jar's manifest are possible, but
		 * they all have limitations when multiple jar files are in the
		 * classpath and each of them provides a MANIFEST.MF file. This
		 * technique seems to be the simplest solution that works.
		 */
		String jarFileName = System.getProperty("java.class.path").split(
				System.getProperty("path.separator"))[0];

		JarFile jar = new JarFile(jarFileName);

		// create a Manifest obj
		Manifest mf = jar.getManifest();

		if (mf == null) {
			throw new IllegalStateException("Cannot read  " + jarFileName
					+ ":!META-INF/MANIFEST.MF from " + jarFileName);
		}

		final Attributes mainAttributes = mf.getMainAttributes();

        // Iterates over all FraSCAti property names.
        for(String[] propertyName : FRASCATI_PROPERTY_NAMES) {
            // if this Java property is not already defined and
        	// is defined in the JAR manifest then defines it.
            if(System.getProperty(propertyName[0]) == null) {
                String value = mainAttributes.getValue(propertyName[1]);
                if(value != null) {
                    System.setProperty(propertyName[0], value);
                }
            }
        }

        // get the mainComposite attribute value
		final String mainComposite = mainAttributes.getValue("mainComposite");

		if (mainComposite == null) {
			throw new IllegalArgumentException(
					"Manifest file "
							+ jarFileName
							+ ":!META-IN/MANIFEST.MF does not provide attribute 'mainComposite'");
		}
		// mainService and mainMethod are optional
		String mainService = mainAttributes.getValue("mainService");
		String mainMethod = mainAttributes.getValue("mainMethod");

		try {
        	final Launcher launcher = new Launcher(mainComposite);
        
        	if (mainService != null && mainMethod != null) {
        		// TODO how to get the return type?
        	    @SuppressWarnings("unchecked")
        	    Object[] params = (Object[])args;
        		launcher.call(mainService, mainMethod, null, params);
        	} else {
        		launcher.launch();
        	}
		} catch (FrascatiException e) {
		    Logger.getAnonymousLogger().severe("Cannot instantiate the FraSCAti factory!");
		}
	}
}
