/**
 * OW2 FraSCAti Assembly Factory
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.net.URL;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.ow2.frascati.assembly.factory.api.ImportExportProcessor;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 *
 */
public abstract class AbstractImportExportProcessor<EObjectType extends EObject> implements ImportExportProcessor<EObjectType>
{

    /**
     * Return an ID for a given Element in the SCA Model.
     * 
     * @param eclass
     * @return String value calculated using EClass package and name.
     */
    protected static String getID(EClass eclass)
    {
      return eclass.getEPackage().getNsURI() + "#" + eclass.getName();
    }

    /**
     * Return an ID for a given Element in the SCA Model.
     * 
     * @param eObj
     * @return String value calculated using EClass package and name.
     */
    protected static String getID(EObject eObj)
    {
      return getID(eObj.eClass());
    }
    
    
    /**
     * *Get resource define by URI if it can be found in one of the FrascatiClassLoader, else return null
     * 
     * This method is used to check validity of a reource or package
     * 
     * @param resourceUri
     * @param frascatiClassLoaders
     * @return
     */
    protected URL getResource(String resourceUri, FrascatiClassLoader... frascatiClassLoaders)
    {
        URL resourceURL;
        for(FrascatiClassLoader frascatiClassLoader : frascatiClassLoaders)
        {
            resourceURL=frascatiClassLoader.getResource(resourceUri);
            if(resourceURL!=null)
            {
                return resourceURL;
            }
        }
        return null;
    }
}
