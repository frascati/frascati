/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 * 
 * Contributor(s): Nicolas Dolet, Philippe Merle
 */

package org.ow2.frascati.assembly.factory.processor;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Property;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.ScaPackage;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * OW2 FraSCAti Assembly Factory SCA component property processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaComponentPropertyProcessor
     extends AbstractPropertyProcessor<PropertyValue> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(PropertyValue property, StringBuilder sb) {
    sb.append(SCA_PROPERTY);
    append(sb, "name", property.getName());
    append(sb, "type", property.getType());
    append(sb, "many", property.isSetMany(), property.isMany());
    append(sb, "file", property.getFile());
    append(sb, "source", property.getSource());
    append(sb, "element", property.getElement());
    super.toStringBuilder(property, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(PropertyValue property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the property name.
    checkAttributeMustBeSet(property, "name", property.getName(), processingContext);

    // Check the property value or source.
    String propertyValue = property.getValue();
    String propertySource = property.getSource();
    logDo(processingContext, property, "check the attributes 'value' or 'source'");
    if( ( propertyValue == null && propertySource == null )
     || ( propertyValue != null && propertySource != null ) ) {
      error(processingContext, property, "The property value or the attribute 'source' must be set");
    }
    logDone(processingContext, property, "check the attributes 'value' or 'source'");

    // Check the attribute 'source'.   
    if(propertySource != null && propertyValue == null) {
      if(propertySource.equals("")) {
        error(processingContext, property, "The attribute 'source' must be set");
      } else {
        if (propertySource.startsWith("$")) {
          propertySource = propertySource.substring(1);
        }
        // Has the enclosing composite a property of this name?
        Property compositeProperty = null;
        for(Property cp : ((Composite)property.eContainer().eContainer()).getProperty()) {
          if(propertySource.equals(cp.getName())) {
            compositeProperty = cp;
            break; // the for loop.
          }
        }
        if(compositeProperty == null) {
          error(processingContext, property, "The source composite property '", propertySource, "' is not defined");
        }
      }
    }
        
    // Check the property attribute 'many'.
    checkAttributeNotSupported(property, "many", property.isSetMany(), processingContext);

    // Check the property attribute 'file'.
    checkAttributeNotSupported(property, "file", property.getFile() != null, processingContext);

    // Check the component property type and value.
    QName propertyType = (property.getElement() != null) ? property.getElement() : property.getType();
    checkProperty(property, propertyType, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(PropertyValue property, ProcessingContext processingContext)
      throws ProcessorException
  {
	// Setting the property value.
    String propertySource = property.getSource();
    if (propertySource != null) {
      if (propertySource.startsWith("$")) {
        propertySource = propertySource.substring(1);
      }

      // Retrieve the component from the processing context.
      Component component = getFractalComponent(property.eContainer(), processingContext);
 
      // Retrieve the SCA property controller of the component.
      SCAPropertyController propertyController;
      try {
        propertyController = (SCAPropertyController)component.getFcInterface(SCAPropertyController.NAME);
      } catch (NoSuchInterfaceException nsie) {
        severe(new ProcessorException(property, "Could not get the SCA property controller", nsie));
        return;
      }

      // Retrieve the SCA property controller of the enclosing composite.
      Component enclosingComposite =
          getFractalComponent(property.eContainer().eContainer(), processingContext);
      SCAPropertyController enclosingCompositePropertyController;
      try {
    	  enclosingCompositePropertyController =
              (SCAPropertyController) enclosingComposite.getFcInterface(SCAPropertyController.NAME);
      } catch(NoSuchInterfaceException nsie) {
        severe(new ProcessorException(property,
            "Could not get the SCA property controller of the enclosing composite", nsie));
        return;
      }
      try {
        enclosingCompositePropertyController.setPromoter(propertySource, propertyController, property.getName());
      } catch (IllegalPromoterException ipe) {
        severe(new ProcessorException(property, "Property '" + property.getName()
  	          + "' cannot be promoted by the enclosing composite", ipe));
        return;
      }

    } else {
      // Set the component property.
      setProperty(property, property.getName(), processingContext);
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.PROPERTY_VALUE);
  }

}
