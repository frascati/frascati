/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.SCAPropertyBase;
import org.eclipse.stp.sca.Property;
import org.eclipse.stp.sca.PropertyValue;

/**
 * OW2 FraSCAti Assembly Factory property type processor abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe MErle</a>
 * @version 1.3
 */
public abstract class AbstractPropertyTypeProcessor
              extends AbstractProcessor<SCAPropertyBase> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(SCAPropertyBase property, StringBuilder sb) {
    sb.append("sca:property");
    // Here a property type processor must deal with both composite and component properties.
    if(property instanceof PropertyValue) {
      PropertyValue pv = (PropertyValue)property;
      append(sb, "name", pv.getName());
      append(sb, "type", pv.getType());
      append(sb, "many", pv.isSetMany(), pv.isMany());
      append(sb, "file", pv.getFile());
      append(sb, "source", pv.getSource());
      append(sb, "element", pv.getElement());
    }
    if(property instanceof Property) {
      Property p = (Property)property;
      append(sb, "name", p.getName());
      append(sb, "type", p.getType());
      append(sb, "many", p.isSetMany(), p.isMany());
      append(sb, "mustSupply", p.isSetMustSupply(), p.isMustSupply());
      append(sb, "element", p.getElement());
    }
    if(property.getValue() != null) {
      sb.append('>');
      sb.append(property.getValue());
      sb.append("</sca:property");
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return this.getClass().getCanonicalName();
  }

}
