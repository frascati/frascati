/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.api;

import org.ow2.frascati.util.FrascatiException;

/**
 * OW2 FraSCAti Assembly Factory manager exception.
 * 
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ManagerException extends FrascatiException {

  private static final long serialVersionUID = 9035619800194492330L;

  /**
   * @see FrascatiException#FrascatiException()
   */
  public ManagerException() {
      super();
  }

  /**
   * @see FrascatiException#FrascatiException(String)
   */
  public ManagerException(String message) {
      super(message);
  }

  /**
   * @see FrascatiException#FrascatiException(String, Throwable)
   */
  public ManagerException(String message, Throwable cause) {
      super(message, cause);
  }

  /**
   * @see FrascatiException#FrascatiException(Throwable)
   */
  public ManagerException(Throwable cause) {
      super(cause);
  }

}
