/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor: Philippe Merle, Christophe Demarey, Gwenael Cattez
 *
 */

package org.ow2.frascati.assembly.factory.manager;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.BaseExportType;
import org.eclipse.stp.sca.BaseImportType;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ContributionType;
import org.eclipse.stp.sca.DeployableType;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ImportExportProcessor;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.ProcessingContextImpl;
import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.component.factory.impl.ComponentFactoryContextImpl;
import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.util.AbstractFractalLoggeable;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.util.ImportExportFrascatiClassLoader;
import org.ow2.frascati.util.Stream;

/**
 * OW2 FraSCAti Assembly Factory main class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public class AssemblyFactoryManager
     extends AbstractFractalLoggeable<ManagerException>
  implements ClassLoaderManager,
             CompositeManager
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required contribution parser.
   */
  @Reference(name = "contribution-parser")
  private Parser<ContributionType> contributionParser;

  /**
   * The required composite parser.
   */
  @Reference(name = "composite-parser")
  private Parser<Composite> compositeParser;

  /**
   * The required component factory membrane generation.
   */
  @Reference(name = "membrane-generation")
  private MembraneGeneration membraneGeneration;

  /**
   * The required component factory.
   */
  @Reference(name = "component-factory")
  private ComponentFactory componentFactory;

  /**
   * The required composite processor.
   */
  @Reference(name = "composite-processor")
  private Processor<Composite> compositeProcessor;
  
  /**
   * Processor for SCA imports
   */
  @Reference(name = "sca-import-processor")
  private ImportExportProcessor<BaseImportType> scaImportProcessor;

  /**
   * Processor for SCA exports
   */
  @Reference(name = "sca-export-processor")
  private ImportExportProcessor<BaseExportType> scaExportProcessor;
  
  /**
   * The main class loader used by the OW2 FraSCAti Assembly Factory.
   */
  private FrascatiClassLoader mainClassLoader;

  /**
   * The top level domain composite.
   */
  private Component topLevelDomainComposite;

  /**
   * Map of the ImportExportFrascatiClassLoader
   * Used to share resources between composite deployed at runtime   
   */
  private List<ImportExportFrascatiClassLoader> exportFrascatiClassLoaders=new ArrayList<ImportExportFrascatiClassLoader>();

  /**
   * Map of all processingContext used to process composite (entry composite's name)
   */   
  private Map<String, ProcessingContext> mapOfProcessingContext=new HashMap<String, ProcessingContext>();

  /**
   * Map of loaded composites.
   */
  // TODO: Use QName instead of String
  private Map<String, Component> loadedComposites = new HashMap<String, Component>();
  
  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see AbstractFractalLoggeable#newException(String)
   */
  @Override
  protected final ManagerException newException(String message)
  {
    return new ManagerException(message);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initialization.
   */
  @Init
  public final void initialize() throws FrascatiException
  {
	// init the FraSCAti class loader used.
    this.mainClassLoader = new FrascatiClassLoader("OW2 FraSCAti Assembly Factory");
    // create the top level domain composite.
    this.topLevelDomainComposite = componentFactory.createScaContainer(new ComponentFactoryContextImpl(this.mainClassLoader));
    setFractalComponentName(topLevelDomainComposite, "Top Level Domain Composite");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.ClassLoaderManager#getClassLoader()
   */
  public final ClassLoader getClassLoader()
  {
    return this.mainClassLoader;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.ClassLoaderManager#setClassLoader(ClassLoader)
   */
  public final void setClassLoader(ClassLoader classLoader)
  {
    this.mainClassLoader = (FrascatiClassLoader)classLoader;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.ClassLoaderManager#loadLibraries(URL[])
   */
  public final void loadLibraries(URL ... urls)
      throws ManagerException
  {
    // check if URLs are accessible.
    for (URL url : urls) {
      try {
        url.openConnection();
      } catch(IOException ioe) {
	    warning(new ManagerException(url.toString(), ioe));
        return;
      }
    }

    // Add them to the main class laoder.
    for (URL url : urls) {
      log.info("Load library: " + url.toString());
      this.mainClassLoader.addUrl(url);
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.newProcessingContext(URL...)
   */
  public final ProcessingContext newProcessingContext(URL ... urls)
  {
    FrascatiClassLoader fcl = new FrascatiClassLoader(urls, this.mainClassLoader);
    return new ProcessingContextImpl(fcl);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.newProcessingContext(ClassLoader)
   */
  public final ProcessingContext newProcessingContext(ClassLoader classLoader)
  {
    FrascatiClassLoader fcl = new FrascatiClassLoader(classLoader);
    fcl.addParent(this.mainClassLoader);
    return new ProcessingContextImpl(fcl);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getProcessingContext(java.lang.String)
   */
  public ProcessingContext getProcessingContext(String rootCompositeName)
  {
      return this.mapOfProcessingContext.get(rootCompositeName);
  }
  
  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getContribution(String)
   */
  public final Component[] getContribution(String contribution)
      throws ManagerException
  {
    return processContribution(contribution, newProcessingContext());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#processContribution(String, ProcessingContext)
   */
  public final Component[] processContribution(String contribution, ProcessingContext processingContext)
    throws ManagerException
  {
    // Get the processing context class loader.
      FrascatiClassLoader frascatiClassLoader = (FrascatiClassLoader)processingContext.getClassLoader();

	// Set the name of the FraSCAti class loader.
    frascatiClassLoader.setName("SCA contribution " + contribution);
    
    // The sca-contribution.xml file.
    QName scaContribution = null;

    try {
      // Load the contribution zip file.
      ZipFile zipFile = new ZipFile(contribution);

      // Get the folder name where the zip's content will be extracted.
      final String folder = zipFile.getName().substring(
          zipFile.getName().lastIndexOf(File.separator)+1,
          zipFile.getName().length() - ".zip".length());

      // Set directory for extracted files

      // TODO : use system temp directory but should use output folder given by
      // runtime component. Will be possible once Assembly Factory modules will
      // be merged

      final String tempDir = System.getProperty("java.io.tmpdir")
          + File.separator + folder + File.separator;

      // create the directory where the zip's content will be extracted.
      File outputDirectory = new File(tempDir);
      outputDirectory.mkdirs();
      // add the output directory into the class loader.
      frascatiClassLoader.addUrl(outputDirectory.toURI().toURL());
      
      // Iterate over zip entries
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        String entryName = entry.getName();

        log.fine("ZIP entry: " + entryName);

        // create directories
        if (entry.isDirectory()) {
          log.fine("create directory : " + tempDir + entryName);
          new File(tempDir, entryName).mkdirs();
        } else
        {
          File f = new File(tempDir, File.separator + entryName);
          // register jar files
          if (entryName.endsWith(".jar")) {
        	URL entryUrl = f.toURI().toURL();
            log.fine("Add to the class path " + entryUrl);
            // Add it to the class loader.
            frascatiClassLoader.addUrl(entryUrl);
          }
          // register contribution definition
          if (entryName.endsWith("sca-contribution.xml")) {
            scaContribution = new QName(f.toURI().toString());
          }
          int idx = entryName.lastIndexOf(File.separator);
          if(idx != -1) {
            String tmp = entryName.substring(0, idx);
            log.fine("create directory : " + tempDir + tmp);
            new File(tempDir, tmp).mkdirs();
          }
          // extract entry from zip to tempDir
          InputStream is = zipFile.getInputStream(entry);
          OutputStream os = new BufferedOutputStream(new FileOutputStream(f));
          Stream.copy(is, os);
          is.close();
          os.close();
        }
      }
    } catch (MalformedURLException e) {
      severe(new ManagerException(e));
      return new Component[0];
    } catch (IOException e) {
      severe(new ManagerException(e));
      return new Component[0];
    }

    if (scaContribution == null) {
      log.warning("No sca-contribution.xml in " + contribution);
      return new Component[0];
    }

    // Call the EMF parser component
    log.fine("Reading contribution " + contribution);

    // SCA contribution instance given by EMF
    ContributionType contributionType = null;
    try {
      // Use SCA parser to create contribution model instance
      contributionType = contributionParser.parse(scaContribution, processingContext);
    } catch (ParserException pe) {
      severe(new ManagerException("Error when loading the contribution file " + contribution
      + " with the SCA XML Processor", pe));
      return new Component[0];
    }

    /****************** SCA import export management ******************/
    
    /*All the exception throw during check phase*/
    List<ProcessorException> processorExceptions=new ArrayList<ProcessorException>();
    
    /*check base exports*/
    List<BaseExportType> baseExports=contributionType.getBaseExport();
    if(!baseExports.isEmpty())
    { 
        for(BaseExportType baseExportType : baseExports)
        {
            try
            {
                scaExportProcessor.check(baseExportType, frascatiClassLoader);
            }
            catch (ProcessorException processorException)
            {
                processorExceptions.add(processorException);
            }
        }
    }
    
    /*check base imports*/
    List<BaseImportType> baseImports=contributionType.getBaseImport();
    if(!baseImports.isEmpty())
    {
        ImportExportFrascatiClassLoader[] importExportFrascatiClassLoaders=this.exportFrascatiClassLoaders.toArray(new ImportExportFrascatiClassLoader[]{});
        for(BaseImportType baseImportType : baseImports)
        {
            try
            {
                /*check that exports exists in the contribution ClassLoader*/
                scaImportProcessor.check(baseImportType, importExportFrascatiClassLoaders);
            }
            catch (ProcessorException processorException)
            {
                processorExceptions.add(processorException);
            }
        }
    }
    
    /*Throw an ProcessorException if exceptions has been throw during check phase*/
    if(!processorExceptions.isEmpty())
    {
        StringBuilder exceptionMessageStringBuilder=new StringBuilder();
        exceptionMessageStringBuilder.append(processorExceptions.size()+" ");
        exceptionMessageStringBuilder.append("error(s) detected during import export check phase\n");
        for(ProcessorException processorException : processorExceptions)
        {
            exceptionMessageStringBuilder.append(processorException.getMessage());
            exceptionMessageStringBuilder.append("\n");
        }
        throw new ManagerException(exceptionMessageStringBuilder.toString());
    }
    
    /*export instantiation phase */
    ImportExportFrascatiClassLoader exportFrascatiClassLoader=null;
    if(!baseExports.isEmpty())
    { 
        exportFrascatiClassLoader=new ImportExportFrascatiClassLoader(frascatiClassLoader);
        for(BaseExportType baseExportType : baseExports)
        {
            try
            {
                scaExportProcessor.instantiate(baseExportType, exportFrascatiClassLoader);
            }
            catch (ProcessorException processorException)
            {
                processorExceptions.add(processorException);
            }
        }
    }
    
    /*import instantiation phase */
    ImportExportFrascatiClassLoader importFrascatiClassLoader=null;
    if(!baseImports.isEmpty())
    {
        importFrascatiClassLoader=new ImportExportFrascatiClassLoader(this.mainClassLoader);
        for(BaseImportType baseImportType : baseImports)
        {
            try
            {
                scaImportProcessor.instantiate(baseImportType, importFrascatiClassLoader);
            }
            catch (ProcessorException processorException)
            {
                processorExceptions.add(processorException);
            }
        }
    }
    
    /*Throw an ProcessorException if exceptions have been throw during instantiation phase*/
    if(!processorExceptions.isEmpty())
    {
        StringBuilder exceptionMessageStringBuilder=new StringBuilder();
        exceptionMessageStringBuilder.append(processorExceptions.size()+" ");
        exceptionMessageStringBuilder.append("error(s) detected during import export instantiation phase\n");
        for(ProcessorException processorException : processorExceptions)
        {
            exceptionMessageStringBuilder.append(processorException.getMessage());
            exceptionMessageStringBuilder.append("\n");
        }
        throw new ManagerException(exceptionMessageStringBuilder.toString());
    }
    
    if(importFrascatiClassLoader!=null)
    {
        /*Add all exportClassloaders as parent of the importClassloader*/
        for(ImportExportFrascatiClassLoader exportFrascatiClassLoaderTmp : this.exportFrascatiClassLoaders)
        {
            importFrascatiClassLoader.addParent(exportFrascatiClassLoaderTmp);
        }
        /*Add importClassLoader as parent of the contribution ClassLoader*/
        frascatiClassLoader.addParent(importFrascatiClassLoader);
    }
    
    if(exportFrascatiClassLoader!=null)
    {
        /*Add the contribution's ExportClassLoader in the exportClassLoader list*/
        this.exportFrascatiClassLoaders.add(exportFrascatiClassLoader);
    }
    
    /************************************/
    
    // Iterate over 'Deployable' defined in contribution descriptor
    ArrayList<Component> components = new ArrayList<Component>();
    for (DeployableType deployable : contributionType.getDeployable())
    {
      try {
        Component c = processComposite(deployable.getComposite(), processingContext);
        components.add(c);
      } catch(Exception exc) {
        severe(new ManagerException("Error when loading the composite " + deployable.getComposite(), exc));
        return new Component[0];
      }
    }
    // return loaded components
    return components.toArray(new Component[components.size()]);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getComposite(String)
   */
  public final Component getComposite(String composite)
      throws ManagerException
  {
    return processComposite(new QName(composite), newProcessingContext());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getComposite(QName)
   */
  public final Component getComposite(QName qname)
      throws ManagerException
  {
	return processComposite(qname, newProcessingContext());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getComposite(String, URL[])
   */
  public final Component getComposite(String composite, URL[] libs)
      throws ManagerException
  {
    return processComposite(new QName(composite), newProcessingContext(libs == null ? new URL[0] : libs));
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getComposite(String)
   */
  public final Component getComposite(String composite, ClassLoader classLoader)
      throws ManagerException
  {
    return processComposite(new QName(composite), newProcessingContext(classLoader));
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.processComposite(QName, ProcessingContext)
   *
   * This method is synchronized to avoid several parallel composite processing as
   * some processors have fields that need protection against concurrent accesses.
   *
   * TODO: Allow more parallelism inside the OW2 FraSCAti Assembly Factory
   *       Then this requires to find where more fine grain synchronization is required.
   */
  protected synchronized final Component internalProcessComposite(QName qname, ProcessingContext processingContext)
      throws ManagerException
  {
    logDo("Processing composite '" + qname + "'");

    // Get the processing mode to use. 
    ProcessingMode processingMode = processingContext.getProcessingMode();
    if(processingMode == null) {
      // Default processing mode when caller does not set it.
      processingMode = ProcessingMode.all;
    }

    // The instantiated composite component.
    Component component = this.loadedComposites.get(qname.getLocalPart());

    // Return it if already loaded.
    if(component != null) {
      return component;
    }

    // Use the SCA parser to create composite model instance.
    Composite composite;
    try {
      composite = compositeParser.parse(qname, processingContext);
    } catch (ParserException pe) {
      warning(new ManagerException("Error when parsing the composite file '" + qname + "'", pe));
      return null;
    }

    // The instantiated composite component.
//    Component component = this.loadedComposites.get(composite.getName());

    // Return it if already loaded.
//    if(component != null) {
//      return component;
//    }

    // If composite is the first loaded, then put it in the processing context as the root composite.
    if(processingContext.getRootComposite() == null)
    {
      processingContext.setRootComposite(composite);
    }
    
// Are errors detected during the parsing phase.
//    if(processingContext.getErrors() > 0) {
//      warning(new ManagerException("Errors detected during the parsing phase of composite '" + qname + "'"));
//      return null;
//    }
    
    // Previous was commented in order to also run the following checking phase.

    if(processingMode == ProcessingMode.parse) {
      return null;
    }

    // Checking phase for the composite.
    try {
      compositeProcessor.check(composite, processingContext);
    } catch (ProcessorException pe) {
      severe(new ManagerException("Error when checking the composite instance '" + qname + "'", pe));
      return null;
    }

    // Are errors detected during the checking phase.
    int nbErrors = processingContext.getErrors();
    if(nbErrors > 0) {
      warning(new ManagerException(nbErrors + " error" + ((nbErrors > 1) ? "s" : "") + " detected during the checking phase of composite '" + qname + "'"));
      return null;
    }

    if(processingMode == ProcessingMode.check) {
      return null;
    }

    // Open a membrane generation phase with the given FraSCAti class loader.
    try {
      membraneGeneration.open(processingContext);
    } catch (FactoryException te) {
      severe(new ManagerException(
                "Cannot open a membrane generation phase for '" + qname + "'", te));
      return null;
    }

    // Generating phase for the composite.
    try {
      compositeProcessor.generate(composite, processingContext);
    } catch (ProcessorException pe) {
      severe(new ManagerException("Error when generating the composite instance '" + qname + "'", pe));
      return null;
    }

    if(processingMode == ProcessingMode.generate) {
      return null;
    }

    // Generate the membranes.
    try {
      membraneGeneration.close(processingContext);
    } catch (FactoryException te) {
      if(te.getMessage().startsWith("Errors when compiling ")) {
        throw new ManagerException(te.getMessage() + " for '" + qname + "'", te);
      }
      severe(new ManagerException(
              "Cannot close the membrane generation phase for '" + qname + "'", te));
      return null;
    }

    if(processingMode == ProcessingMode.compile) {
      return null;
    }

    // Instantiating phase for the composite.
    try {
      compositeProcessor.instantiate(composite, processingContext);
    } catch (ProcessorException pe) {
      throw new ManagerException("Error when instantiating the composite instance '" + qname + "'", pe);
    }
    // Retrieve the instantiated component from the processing context.
    component = processingContext.getData(composite, Component.class);

    if(processingMode == ProcessingMode.instantiate) {
      return component;
    }

    // Completing phase for the composite.
    try {
      compositeProcessor.complete(composite, processingContext);
    } catch (ProcessorException pe) {
      severe(new ManagerException("Error when completing the composite instance '" + qname + "'", pe));
      return null;
    }

    if(processingMode == ProcessingMode.complete) {
      return component;
    }

    log.fine("Starting the composite '" + qname + "'...");
    try {
      // start the composite container.
      Component[] parents = Fractal.getSuperController(component).getFcSuperComponents();
      
      if (parents.length > 0) { // There is a container for bindings  
          startFractalComponent(parents[0]);
      } else {
          startFractalComponent(component);
      }
      log.info("SCA composite '" + composite.getName() + "': " + getFractalComponentState(component) + "\n");
    } catch (Exception e) {
      severe(new ManagerException("Could not start the SCA composite '" + qname + "'", e));
      return null;
    }
   
    /*Store the processing Context and the processed composite for further uses*/
    processingContext.addProcessedComposite(composite);
    this.mapOfProcessingContext.put(composite.getName(), processingContext);
    log.info("Save processing context for composite : "+composite.getName());
    
    if(processingMode == ProcessingMode.start)
    {
      return component;
    }

    addComposite(component);

    logDone("processing composite '" + qname + "'");

    return component;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.processComposite(QName, ProcessingContext)
   */
  public final Component processComposite(QName qname, ProcessingContext processingContext)
      throws ManagerException
  {
    // Get the processing context's FraSCAti class loader.
    FrascatiClassLoader frascatiClassLoader = (FrascatiClassLoader)processingContext.getClassLoader();

    // Set the name of the FraSCAti class loader.
    frascatiClassLoader.setName(qname.toString());

// Uncomment next line for debugging the FraSCAti class loader.
//    FrascatiClassLoader.print(frascatiClassLoader);

	// Get the current thread's context class loader and set it.
    ClassLoader previousCurrentThreadContextClassLoader =
        FrascatiClassLoader.getAndSetCurrentThreadContextClassLoader(frascatiClassLoader);

    try {
      return internalProcessComposite(qname, processingContext);
    } finally {
      // Reset the previous current thread's context class loader.
      FrascatiClassLoader.setCurrentThreadContextClassLoader(previousCurrentThreadContextClassLoader);
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#addComposite(Component)
   */
// TODO add a QName parameter
  public final void addComposite(Component composite)
      throws ManagerException
  {
    // Retrieve the component name.
    String compositeName = getFractalComponentName(composite);

    if (this.loadedComposites.containsKey(compositeName)) {
      warning(new ManagerException("Composite '" + compositeName +
                       "' already loaded into the top level domain composite"));
      return;
    } else {
      addFractalSubComponent(this.topLevelDomainComposite, composite);
      this.loadedComposites.put(compositeName, composite);
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#removeComposite(String)
   */
  public final void removeComposite(String name)
// TODO QName instead of String
    throws ManagerException
  {
    // Retrieve component reference from its name
    final Component component = this.loadedComposites.get(name);

    if (component == null) {
      severe(new ManagerException("Composite '" + name +"' does not exist"));
    }

    // Remove the component.
    removeFractalSubComponent(this.topLevelDomainComposite, component);

    // Remove component from loaded composites.
    this.loadedComposites.remove(name);

//    // Remove the processingContext from the saved processingContext
//    this.mapOfProcessingContext.remove(name);
//    log.info("Remove ProcessingContext realed to "+name+" comoposite");

    // Stop the SCA composite.
    if(!name.equals("org.ow2.frascati.FraSCAti")) {
      // If it is not the FraSCAti composite then just stop it.
      stopFractalComponent(component);
    } else {
      // As a business call is inside the FraSCAti composite
      // then stopping FraSCAti will block instead the current call is finished
      // so this creates a deadlock.
      // Stopping FraSCAti inside a new thread avoids this deadlock.
      new Thread(
        new Runnable() {
          public void run()
          {
            try {
              stopFractalComponent(component);
            } catch(Exception exc) {
              log.warning("Can't stop the FraSCAti composite!");
            }
          }
        }
      ).start();
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getCompositeNames()
   */
  public final String[] getCompositeNames()
// TODO: QName instead of String
  {
    Set<String> keyset = loadedComposites.keySet();
    return keyset.toArray(new String[keyset.size()]);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getComposites()
   */  
  public final Component[] getComposites()
  {
    Collection<Component> composites = this.loadedComposites.values();
    return composites.toArray(new Component[composites.size()]);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.CompositeManager#getTopLevelDomainComposite()
   */
  public final Component getTopLevelDomainComposite()
  {
    return this.topLevelDomainComposite;
  }

}
