/**
 * OW2 FraSCAti 
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.net.URL;

import org.eclipse.emf.ecore.EObject;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.ImportExportFrascatiClassLoader;

/**
 *
 */
public abstract class AbstractImportExportJavaProcessor<EObjectType extends EObject> extends AbstractImportExportProcessor<EObjectType>
{
    private final static String PACKAGENAME_REGEX="([a-zA-Z]\\w*)(\\.[a-zA-Z]\\w*)*";
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.api.ImportExportProcessor#check(java.lang.Object, org.ow2.frascati.util.FrascatiClassLoader[])
     */
    public void check(EObjectType element, FrascatiClassLoader... frascatiClassLoaders) throws ProcessorException
    {
        String packageName=this.getPackageName(element);

        /*check package definition is not empty*/
        if(packageName==null || "".equals(packageName))
        {
            String message="["+this.getClass().getSimpleName()+"] Empty package definition found";
            throw new ProcessorException(element, message);
        }
        
        /*check validity of the packageName based on PACKAGENAME_REGEX regular expression*/
        if(!packageName.matches(PACKAGENAME_REGEX))
        {
            String message="["+this.getClass().getSimpleName()+"] "+packageName+" is not a valid package name, package name must match "+PACKAGENAME_REGEX+" regular expression";
            throw new ProcessorException(element, message);
        }

        /*check if package can be found as resource in the FraSCAtiClassloaders*/
        String packagePath=packageName.replace(".", "/");
        URL packageURL=this.getResource(packagePath, frascatiClassLoaders);
        if(packageURL==null)
        {
            String message="["+this.getClass().getSimpleName()+"] The package : "+packageName+" can't be find ";
            throw new ProcessorException(element, message);
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.assembly.factory.api.ImportExportProcessor#instantiate(java.lang.Object, org.ow2.frascati.util.ImportExportFrascatiClassLoader)
     */
    public void instantiate(EObjectType element, ImportExportFrascatiClassLoader importExportFrascatiClassLoader) throws ProcessorException
    {
        String packageName=this.getPackageName(element);
        /*Add package as valid package in the ImportExportFrascatiClassLoader*/
        importExportFrascatiClassLoader.addPackage(packageName);
        String resourceStylePackageName=packageName.replace(".", "/");
        /*Add package path as valid resource in the ImportExportFrascatiClassLoader*/
        importExportFrascatiClassLoader.addResource(resourceStylePackageName);
    }
    
    /**
     * Get package name define in the element
     * 
     * @param element the package name container
     * @return
     */
    protected abstract String getPackageName(EObjectType element);
}
