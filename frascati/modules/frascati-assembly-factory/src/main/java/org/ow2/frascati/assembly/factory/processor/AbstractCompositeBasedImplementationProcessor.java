/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.Implementation;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.InterfaceType;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.component.factory.api.FactoryException;

/**
 * OW2 FraSCAti Assembly Factory abstract composite-based implementation processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractCompositeBasedImplementationProcessor<ImplementationType extends Implementation, ContentType>
              extends AbstractComponentFactoryBasedImplementationProcessor<ImplementationType> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected void doGenerate(ImplementationType implementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    try {
      getComponentFactory().generateMembrane(
        processingContext,
        getFractalComponentType(implementation, processingContext),
  	    getMembrane(),
  	    null);
    } catch (FactoryException te) {
  	  severe(new ProcessorException(implementation, "Error while generating " + toString(implementation), te));
  	}
  }

  /**
   * Get the SCA implementation composite membrane to use.
   */
  protected String getMembrane() {
    return "scaComposite";  
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  protected final Component doInstantiate(ImplementationType implementation, ProcessingContext processingContext, ContentType content)
      throws ProcessorException
  {
    // Create the SCA composite for the implementation.
    Component component = createFractalComposite(implementation, processingContext);

    // Connect the Fractal composite to the content.
    connectFractalComposite(implementation, processingContext, content);

    return component;
  }

  /**
   * Create the Fractal composite.
   */
  protected final Component createFractalComposite(ImplementationType implementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Create the SCA composite for the implementation.
    Component component;
    try {
      component = getComponentFactory().createComponent(
        processingContext,
        getFractalComponentType(implementation, processingContext),
        getMembrane(),
        null);
    } catch (FactoryException te) {
      severe(new ProcessorException(implementation, "Error while creating " + toString(implementation), te));
      return null;
    }

    // Store the created composite into the processing context.
    processingContext.putData(implementation, Component.class, component);

    return component;
  }

  /**
   * Connect the Fractal composite.
   */
  protected final void connectFractalComposite(ImplementationType implementation, ProcessingContext processingContext, ContentType content)
      throws ProcessorException
  {
    // Get the binding and content controller of the component.
    Component component = getFractalComponent(implementation, processingContext);
    BindingController bindingController = getFractalBindingController(component);
    ContentController contentController = getFractalContentController(component);
    
    // Traverse all the interface types of the component type.
    for (InterfaceType interfaceType : getFractalComponentType(implementation, processingContext).getFcInterfaceTypes()) {
      String interfaceName = interfaceType.getFcItfName();
      String interfaceSignature = interfaceType.getFcItfSignature();
      Class<?> interfaze;
      try {
        interfaze = processingContext.loadClass(interfaceSignature);
      } catch(ClassNotFoundException cnfe) {
        severe(new ProcessorException(implementation, "Java interface '" + interfaceSignature + "' not found", cnfe));
        return;
      }
	  if (!interfaceType.isFcClientItf()) {
	    // When this is a server interface type.
        // Get a service instance.
	    Object service = null;
	    try {
	      service = getService(content, interfaceName, interfaze);
	    } catch(Exception exc) {
	      severe(new ProcessorException(implementation, "Can not obtain the SCA service '" + interfaceName + "'", exc));
	      return;  	
	    }
        // Bind the SCA composite to the service instance.
	    bindFractalComponent(bindingController, interfaceName, service);
      } else {
        // When this is a client interface type.
    	Object itf = getFractalInternalInterface(contentController, interfaceName);
        try {
          setReference(content, interfaceName, itf, interfaze);
  	    } catch(Exception exc) {
  	      severe(new ProcessorException(implementation, "Can not set the SCA reference '" + interfaceName + "'", exc));
  	      return;  	
  	    }
      }
    }
  }

  /**
   * Get an SCA service.
   */
  protected abstract Object getService(ContentType content, String name, Class<?> interfaze) throws Exception;

  /**
   * Set an SCA reference.
   */
  protected abstract void setReference(ContentType content, String name, Object delegate, Class<?> interfaze) throws Exception;

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
