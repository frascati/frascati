/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 * 
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.lang.annotation.ElementType;
import java.lang.reflect.Proxy;

import org.eclipse.stp.sca.SCABinding;
import org.eclipse.stp.sca.ScaPackage;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory SCA Binding processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @author Philippe Merle - INRIA
 * @version 1.3
 */
public class ScaBindingScaProcessor
     extends AbstractBindingProcessor<SCABinding> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Reference to the composite domain manager.
   */
  @Reference(name = "composite-manager")
  private CompositeManager compositeManager;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(SCABinding scaBinding, StringBuilder sb) {
    sb.append("sca:binding.sca");
    super.toStringBuilder(scaBinding, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SCABinding scaBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    String uri = scaBinding.getUri();
	if(hasBaseService(scaBinding)) {
	  // The parent of this binding is a <service>.
	  if(uri != null) {
        warning(processingContext, scaBinding, "The attribute 'uri' on a SCA service is not supported by OW2 FraSCAti");
	  }
	} else {
	  // The parent of this binding is a <reference>.
	  if(isNullOrEmpty(uri)) {
        error(processingContext, scaBinding, "The attribute 'uri' must be 'compositeName/componentName*/serviceName'");
	  } else {
        String[] parts = uri.split("/");
        if(parts.length < 2) {
          error(processingContext, scaBinding, "The attribute 'uri' must be 'compositeName/componentName*/serviceName'");
        } else {
          // Obtain the composite.
          String compositeName = parts[0];
          if(compositeName.equals(processingContext.getRootComposite().getName())) {
            // The searched composite is the currently processed root SCA composite.

        	// TODO Check if the SCA service exists.

          } else {
            // The searched composite is another composite loaded into the domain, so get it.
            Component component = null;
            try {
              component = compositeManager.getComposite(compositeName);
            } catch(ManagerException me) {
              error(processingContext, scaBinding, "Composite '", compositeName, "' not found");
            }
            if(component != null) {
              // Find the service from the uri.
              Object service = findService(scaBinding, processingContext, component, parts);
              if(service != null) {
                // Store the found service into the processing context.
                processingContext.putData(scaBinding, Object.class, service);
              }
            }
          }
        }
	  }
	}

	// Check other attributes of the binding.
    checkBinding(scaBinding, processingContext);
  }

  /**
   * Find a service according to an uri.
   */
  protected final Object findService(SCABinding scaBinding, ProcessingContext processingContext, Component initialComponent, String[] parts)
    throws ProcessorException
  {
    Component component = initialComponent;
    boolean found = true;
    // Traverse the componentName parts of the uri.
    for(int i=1; i<parts.length-1; i++) {
      found = false;
      // Search the sub component having the searched component name.
      for(Component subComponent : getFractalSubComponents(component)) {
        if(parts[i].equals(getFractalComponentName(subComponent))) {
          component = subComponent;
          found = true;
          break; // the for loop.
        }
      }
      if(!found) {
        error(processingContext, scaBinding, "Component '", parts[i], "' not found");
        return null;
      }
    }
    if(found) {
      // Get the service of the last found component.
      String serviceName = parts[parts.length - 1];
      try {
        return component.getFcInterface(serviceName);
      } catch(NoSuchInterfaceException nsie) {
        error(processingContext, scaBinding, "Service '", serviceName, "' not found");
        return null;
      }
    }
    return null;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(SCABinding scaBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    if(hasBaseService(scaBinding))
    {
      // Nothing to do.
      return;
    }

    // Obtain the service computed during doCheck()
    Object service = processingContext.getData(scaBinding, Object.class);

    if(service == null) {
       // The uri should refer to a service of the currently processed SCA composite.
       String[] parts = scaBinding.getUri().split("/");
       if(parts[0].equals(processingContext.getRootComposite().getName())) {
         Component component = getFractalComponent(processingContext.getRootComposite(), processingContext);
         service = findService(scaBinding, processingContext, component, parts);
       }
       if(service == null) {
         // This case should never happen because it should be checked by doCheck()
    	 severe(new ProcessorException(scaBinding, "Should never happen!!!"));
    	 return;
       } 
    }
    
    // Create a dynamic proxy invocation handler.
    ScaBindingScaInvocationHandler bsih = new ScaBindingScaInvocationHandler();
    // Its delegates to the service found.
    bsih.setDelegate(service);
    //It set the binding uri
    bsih.setBindingURI(scaBinding.getUri());
    
    
    // Create a dynamic proxy with the created invocation handler.
    Object proxy = Proxy.newProxyInstance(
                       processingContext.getClassLoader(),
                       new Class<?>[] { getBaseReferenceJavaInterface(scaBinding, processingContext) },
                       bsih);
   
    // Bind the proxy to the component owning this <binding.sca>.
    bindFractalComponent(
        getFractalComponent(scaBinding, processingContext),
        getBaseReference(scaBinding).getName(),
        proxy);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.SCA_BINDING);
  }
  
}
