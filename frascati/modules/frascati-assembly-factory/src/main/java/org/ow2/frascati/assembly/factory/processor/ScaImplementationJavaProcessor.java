/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor: Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.ScaPackage;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;

/**
 * OW2 FraSCAti Assembly Factory implementation Java processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class ScaImplementationJavaProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<JavaImplementation> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(JavaImplementation javaImplementation, StringBuilder sb) {
    sb.append("sca:implementation.java");
    append(sb, "class", javaImplementation.getClass_());
    super.toStringBuilder(javaImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(JavaImplementation javaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Test if the Java class is present in the current classloader.
    try {
      processingContext.loadClass(javaImplementation.getClass_());
    } catch (ClassNotFoundException cnfe) {
      error(processingContext, javaImplementation, "class '",
          javaImplementation.getClass_(), "' not found");
	  return;
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(javaImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(JavaImplementation javaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate a FraSCAti SCA primitive component.
    generateScaPrimitiveComponent(javaImplementation, processingContext, javaImplementation.getClass_());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(JavaImplementation javaImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate a FraSCAti SCA primitive component.
    instantiateScaPrimitiveComponent(javaImplementation, processingContext, javaImplementation.getClass_());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.processor.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.JAVA_IMPLEMENTATION);
  }

}
