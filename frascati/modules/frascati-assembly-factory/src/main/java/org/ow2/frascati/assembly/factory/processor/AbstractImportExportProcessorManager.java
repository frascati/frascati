/**
 * OW2 FraSCAti Assembly Factory
 *
 * Copyright (c) 2011-2013 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.assembly.factory.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.osoa.sca.annotations.Init;
import org.ow2.frascati.assembly.factory.api.ImportExportProcessor;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.ImportExportFrascatiClassLoader;

/**
 *
 */
public abstract class AbstractImportExportProcessorManager <ElementType extends EObject> extends AbstractImportExportProcessor<ElementType>
{

  //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * List of processors.
     */
    private List<ImportExportProcessor<ElementType>> processors = null;

    /**
     * Map indexing processors by their processor ID.
     */
    private Map<String, ImportExportProcessor<ElementType>> processorsByID = null;

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * Set the list of processors.
     */
    protected final void setProcessors(List<ImportExportProcessor<ElementType>> exportProcessors) 
    {
            this.processors = exportProcessors;
            this.processorsByID = null;
    }

    /**
     * Get the processor associated to a given EObject.
     */
    protected final ImportExportProcessor<ElementType> getProcessor(ElementType eObj)
        throws ProcessorException
    {
      if(processorsByID == null) {
        initializeProcessorsByID();
      }

      ImportExportProcessor<ElementType> processor = processorsByID.get(getID(eObj));
      if(processor == null)
      {
        return null;
      }
      return processor;
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
     */
    public void check(ElementType element,FrascatiClassLoader ... frascatiClassLoader) throws ProcessorException
    {
      getProcessor(element).check(element, frascatiClassLoader);
    }
    
    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
     */
    public void instantiate(ElementType element,  ImportExportFrascatiClassLoader importExportFrascatiClassLoader) throws ProcessorException
    {
      getProcessor(element).instantiate(element, importExportFrascatiClassLoader);
    }
    
    /**
     * Initialize the map of processors indexed by their id.
     */
    @Init
    public final void initializeProcessorsByID()
    {
          this.processorsByID =new HashMap<String, ImportExportProcessor<ElementType>>();
          for (ImportExportProcessor<ElementType> processor : this.processors)
          {
              this.processorsByID.put(processor.getProcessorID(), processor);
          }
    }
}
