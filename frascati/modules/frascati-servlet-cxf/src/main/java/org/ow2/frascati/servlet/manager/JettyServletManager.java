/**
 * OW2 FraSCAti: Servlet Manager
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s): Christophe Munilla
 *
 */

package org.ow2.frascati.servlet.manager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashSet;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.BusFactory;
import org.apache.cxf.transport.http_jetty.JettyHTTPHandler;
import org.apache.cxf.transport.http_jetty.JettyHTTPServerEngine;
import org.apache.cxf.transport.http_jetty.JettyHTTPServerEngineFactory;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.ContextHandler;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.servlet.FraSCAtiServlet;
import org.ow2.frascati.servlet.api.ServletManager;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Implementation of the {@link ServletManager} interface.
 *
 * @author Philippe Merle
 */

@Scope("COMPOSITE")
@Service(ServletManager.class)
public class JettyServletManager
     extends AbstractLoggeable
  implements ServletManager
{
    /**
     * Set of Apache Jetty server engine ports.
     */
    protected HashSet<Integer> ports = new HashSet<Integer>();

    /**
     * @see ServletManager#registerServlet(String, Servlet)
     */
    public final synchronized void registerServlet(final String uri, final Servlet servlet)
    {
        // If the uri is relative and a FraSCAti servlet is started
        if(uri.startsWith("/") && FraSCAtiServlet.singleton != null) {
          // then delegate the registration to it.
          FraSCAtiServlet.singleton.registerServlet(uri, servlet);
          return;
        }

        log.info("RegisterServlet(uri=" + uri + ") called.");

        // Create a URL instance from the 'aUri' string.
        URL url;
        try {
            url = new URL(uri);
        } catch (MalformedURLException mue) {
            throw new RuntimeException(mue);
        }

        // Obtain the Apache CXF Jetty server factory.
        JettyHTTPServerEngineFactory jettyFactory =
            BusFactory.getDefaultBus().getExtension(JettyHTTPServerEngineFactory.class);

        // Create an Apache CXF Jetty server.
        JettyHTTPServerEngine jettyServer;
        try {
          jettyServer = jettyFactory.createJettyHTTPServerEngine(url.getHost(), url.getPort(), "http");
          // keep the port of this Apache CXF Jetty server engine
          // in order to be able to destroy it when shutdowning.
          this.ports.add(url.getPort());
        } catch(Exception exc) {
            throw new RuntimeException(exc);
        }

        jettyServer.setSessionSupport(true);
        
        log.info("Adding a Servlet with path '" + url.getPath() +
						"' on the Jetty server on port " + url.getPort() + " ...");
        jettyServer.addServant(url, new ServletHandler(servlet));

        log.info("RegisterServlet(uri=" + uri + ") done.");
	}

    /**
     * @see ServletManager#unregisterServlet(String)
     */
    public final void unregisterServlet(final String uri) {
    	// If the uri is relative and a FraSCAti servlet is started
        if(uri.startsWith("/") && FraSCAtiServlet.singleton != null) {
          // then delegate the unregistration to it.
          FraSCAtiServlet.singleton.unregisterServlet(uri);
          return;
        }
        log.info("UnregisterServlet(uri=" + uri + ") called.");
        
        URI servletURI = URI.create(uri);
        int port = servletURI.getPort();
        
        // Obtain the Apache CXF Jetty server factory.
	    JettyHTTPServerEngineFactory jettyFactory =
	            BusFactory.getDefaultBus().getExtension(JettyHTTPServerEngineFactory.class);
	    //Obtain the Jetty server engine for the specific port
	    JettyHTTPServerEngine engine = jettyFactory.retrieveJettyHTTPServerEngine(port);
	    
	    try {	 
	    	//Retrieve the ContextHandler associated to the URL of the Servlet
			ContextHandler contextHandler = engine.getContextHandler(servletURI.toURL());
			//If not stopped, do it
			if(!contextHandler.getState().equals(ContextHandler.STOPPED)){
				contextHandler.stop();
			}
			//Destroy the context attached to the Servlet
			contextHandler.destroy();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}        
        log.info("UnregisterServlet(uri=" + uri + ") done.");
    }

    /**
     * @see ServletManager#shutdown()
     *
     * Added for issue http://jira.ow2.org/browse/FRASCATI-105
     */
    public void shutdown()
    {
        // Obtain the Apache CXF Jetty server factory.
        JettyHTTPServerEngineFactory jettyFactory =
            BusFactory.getDefaultBus().getExtension(JettyHTTPServerEngineFactory.class);

        // Shutdown all started Apache CXF Jetty server engines.
        log.info("Shutdown all started Apache CXF Jetty server engines.");
        for(int port : this.ports) {
          log.info("Shutdown Apache CXF Jetty server engine on port " + port + ".");
          jettyFactory.destroyForPort(port);
        }
    }
}

class ServletHandler extends JettyHTTPHandler
{
    /**
     * Servlet to delegate.
     */
    private Servlet servlet;

    /**
     * Constructor.
     */
    public ServletHandler(Servlet servlet)
    {
        super(null, false);
        this.servlet = servlet;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
    	// Check if this request is for this handler.
    	if(target.startsWith(getName())) {
          // Remove the path from the path info of this request.
          String old_path_info = baseRequest.getPathInfo();
          baseRequest.setPathInfo(old_path_info.substring(getName().length()));
          // Dispatch the request to the servlet.
          this.servlet.service(request, response);
          // Restore the previous path info.
          baseRequest.setPathInfo(old_path_info);
          // This request was handled.
          baseRequest.setHandled(true);
    	}
    }
}
