/**
 * OW2 FraSCAti: Servlet Manager
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.servlet.api;

import javax.servlet.Servlet;

public interface ServletManager
{
  /**
   * Register a servlet.
   *
   * @param uri the URI of the servlet.
   * @param servlet the servlet to register.
   */
  void registerServlet(final String uri, final Servlet servlet);

  /**
   * Unregister a servlet.
   *
   * @param uri the URI of the servlet to unregister.
   */
  void unregisterServlet(final String uri);

  /**
   * Shutdown the servlet manager.
   *
   * Added for issue http://jira.ow2.org/browse/FRASCATI-105
   */
  void shutdown();
}
