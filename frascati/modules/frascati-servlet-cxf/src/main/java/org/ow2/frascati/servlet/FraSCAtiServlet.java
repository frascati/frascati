/**
 * OW2 FraSCAti Servlet CXF
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.servlet;

import java.io.InputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.servlet.api.ServletManager;

/**
 * OW2 FraSCAti Servlet extending the Apache CXF Servlet ({@link CXFServlet}).
 *
 * @author Philippe Merle
 *
 */
public class FraSCAtiServlet
     extends CXFNonSpringServlet
  implements ServletManager
{
  // --------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The singleton instance.
   */
  public static FraSCAtiServlet singleton = null;

  /**
   * Logger.
   */
  private Logger log = Logger.getLogger(this.getClass().getCanonicalName());

  /**
   * The FraSCAti runtime.
   */
  private FraSCAti frascati;

  /**
   * Launched SCA composites.
   */
  private Collection<Component> launchedComposites = new ArrayList<Component>();

  /**
   * Deployed servlets.
   */
  private Map<String, Servlet> servlets = new HashMap<String, Servlet>();

  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  protected void launch(String[] composites, Properties contextualProperties)
  {
    for(String compositeName : composites) {
      try {
        log.fine("OW2 FraSCAti Servlet - Launching SCA composite '" + compositeName + "'...");
        // Create and init the processing context.
        ProcessingContext processingContext = frascati.newProcessingContext();
        if(contextualProperties != null) {
          for(String propertyName : contextualProperties.stringPropertyNames()) {
            processingContext.setContextualProperty(propertyName, contextualProperties.getProperty(propertyName));
          }
        }
        // Process the composite.
        Component launchedComposite = frascati.processComposite(compositeName, processingContext);
        this.launchedComposites.add(launchedComposite);
        log.fine("OW2 FraSCAti Servlet - '" + compositeName + "' launched.");
      } catch (FrascatiException e) {
        log.severe("Cannot launch SCA composite '" + compositeName + "'!");
      }
    }
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initialization of the servlet and launches SCA composites if any.
   */
  @Override
  public  void init(ServletConfig servletConfig) throws ServletException
  {
    log.fine("OW2 FraSCAti Servlet - Initialization...");
    // Init the CXF servlet.
    super.init(servletConfig);

    // Keep this as the FraSCAtiServlet singleton instance.
    singleton = this;

    String frascatiBootstrap = servletConfig.getInitParameter(FraSCAti.FRASCATI_BOOTSTRAP_PROPERTY_NAME);
    if(frascatiBootstrap != null) {
      System.setProperty(FraSCAti.FRASCATI_BOOTSTRAP_PROPERTY_NAME, frascatiBootstrap);
    }

    // Set that no prefix is added to <sca:binding uri>.
	AbstractBindingProcessor.setEmptyBindingURIBase();

    try {
      frascati = FraSCAti.newFraSCAti();
    } catch (FrascatiException e) {
      log.severe("Cannot instanciate FraSCAti!");
      return;
    }

    // Get contextual properties.
    String contextualPropertiesParameter = servletConfig.getInitParameter("contextual-properties");
    Properties contextualProperties = null;
    if(contextualPropertiesParameter != null) {
      InputStream is = this.getClass().getClassLoader().getResourceAsStream(contextualPropertiesParameter);
      if(is == null) {
        log.warning(contextualPropertiesParameter + " not found!");    	  
      } else {
        try {
          Properties cp = new Properties();
          cp.load(is);
          contextualProperties = cp;
        } catch(IOException ioe) {
          log.warning("IOException when loading " + contextualPropertiesParameter);
          ioe.printStackTrace();
        }
      }
    }

    // Get the list of composites to launch.
    String composite = servletConfig.getInitParameter("composite");
    if(composite == null) {
      log.warning("OW2 FraSCAti Servlet - No SCA composites to launch.");
    } else {
      // SCA composites are separated by spaces.
      String[] composites = composite.split("\\s+");
      launch(composites, contextualProperties);
    }
  }

  @Override
  public void destroy()
  {
    // Stop all started SCA composites.
    for (Component launcherComposite : this.launchedComposites) {
      try {
        frascati.close(launcherComposite);
      } catch(Exception exc) {
        exc.printStackTrace();
      }
    }
    this.launchedComposites.clear();

    // Added for issue http://jira.ow2.org/browse/FRASCATI-105
    // Stop all started Apache CXF Jetty server engines.
    try {
      ServletManager servletManager = frascati.getService(frascati.getFrascatiComposite(), "servlet-manager", ServletManager.class);
      servletManager.shutdown();
    } catch(Exception exc) {
      exc.printStackTrace();
    }

    // Stop FraSCAti.
    try {
      frascati.close();
    } catch(Exception exc) {
      exc.printStackTrace();
    }
    
    super.destroy();
  }

  @Override
  public void service(ServletRequest request, ServletResponse response)
    throws IOException, ServletException
  {
	HttpServletRequest httpRequest = (HttpServletRequest)request;

	String pathInfo = httpRequest.getPathInfo();

    if(pathInfo != null) {
      // Search a deployed servlet that could handle this request.
      for(Entry<String, Servlet> entry : servlets.entrySet()) {
	    if(pathInfo.startsWith(entry.getKey())) {
	      entry.getValue().service(new MyHttpServletRequestWrapper(httpRequest,
        		                          pathInfo.substring(entry.getKey().length())),
        		                   response);
          return;
	    }
      }
	}

    // If no deployed servlet for this request then dispatch this request via Apache CXF.
    super.service(request, response);
  }

  /**
   * @see ServletManager#registerServlet(String, Servlet)
   */
  public final void registerServlet(String uri, final Servlet servlet)
  {
    log.info("RegisterServlet(uri=" + uri + ") called.");

    // Register both uri and servlet.
    synchronized(servlets) {
      servlets.put(uri, servlet);
    }

    log.info("RegisterServlet(uri=" + uri + ") done.");
  }

  /**
   * @see ServletManager#unregisterServlet(String)
   */
  public final void unregisterServlet(String uri)
  {
    log.info("UnregisterServlet(uri=" + uri + ") called.");

    // Unregister both uri and servlet.
    synchronized(servlets) {
      servlets.remove(uri);
    }

    log.info("UnregisterServlet(uri=" + uri + ") done.");
  }

  /**
   * @see ServletManager#shutdown()
   *
   * Added for issue http://jira.ow2.org/browse/FRASCATI-105
   */
  public void shutdown()
  {
     // Nothing to do.
  }
}

class MyHttpServletRequestWrapper extends HttpServletRequestWrapper
{
  private String pathInfo;

  public MyHttpServletRequestWrapper(HttpServletRequest request, String pathInfo)
  {
	super(request);
	this.pathInfo = pathInfo;
  }

  @Override
  public String getPathInfo()
  {
    return this.pathInfo;
  }
}
