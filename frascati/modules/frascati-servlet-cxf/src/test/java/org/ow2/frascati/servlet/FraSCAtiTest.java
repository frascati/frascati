/**
 * OW2 FraSCAti: Test Servlet with Apache CXF
 * Copyright (C) 2010 - 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Munilla
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.servlet.api.ServletManager;

/**
 * JUnit test case for JettyServletManager class. 
 */
public class FraSCAtiTest {

	private static int PORT = 8765;
	private static String URL_PREFIX = "http://localhost:" + PORT + '/';

	private String message1 = "hello from s1";
	private String message2 = "hello from s2";
	private URL u1;
	private URL u2;
	private ServletManager manager;
	private FraSCAti frascati;
	
	@Test   
    public void newFraSCAti() throws Exception {
      //init FraSCAti
      frascati = FraSCAti.newFraSCAti();
      //Found the ServletManager service
      manager = frascati.getService(frascati.getFrascatiComposite(), "servlet-manager", ServletManager.class);

      //Create a new Servlet
      Servlet s1 = new ServletTest(message1);
      Servlet s2 = new ServletTest(message2);
      
      //Create new URLs
      u1 = new URL(URL_PREFIX + "myServlet");
      u2 = new URL(URL_PREFIX + "myOtherServlet");
      
      //Register, Unregister and Register it again
      manager.registerServlet(URL_PREFIX + "myServlet", s1);
      manager.registerServlet(URL_PREFIX + "myOtherServlet", s2);
      
      assertEquals(message1,callServlet(u1));
      assertEquals(message2,callServlet(u2));
      
      manager.unregisterServlet(URL_PREFIX + "myServlet");
      try{
      assertEquals(message1,callServlet(u1));
      fail();
      } catch(IOException e){
    	  //expected
      }
      assertEquals(message2,callServlet(u2));
      
      manager.registerServlet(URL_PREFIX + "myServlet", s1); 
      assertEquals(message1,callServlet(u1));
      assertEquals(message2,callServlet(u2));

      manager.shutdown();
      frascati.close();
	}    

    private String callServlet(URL u) throws IOException
    {
    	BufferedReader in = null; 
		in = new BufferedReader(new InputStreamReader(
				 u.openConnection().getInputStream()));
	    String message = in.readLine();
	    System.out.println(message);
	    return message;	      
    }
    
    /**
     * A simple Helloworld Servlet
     */
    @SuppressWarnings("serial")
	private class ServletTest extends HttpServlet
	{
    	String message;
    	
    	public ServletTest(String message)
    	{
    		this.message = message;
    	}
    	
    	public void doGet(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException
        {
    		PrintWriter out = response.getWriter();
    		out.println(message);
    	}
    }
}
