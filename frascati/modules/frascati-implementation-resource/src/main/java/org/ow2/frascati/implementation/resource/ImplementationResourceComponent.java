/**
 * OW2 FraSCAti: SCA Implementation Resource
 * Copyright (C) 2010-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.implementation.resource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.util.Stream;

/**
 * OW2 FraSCAti implementation resource component class.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
@Scope("COMPOSITE")
@Service(Servlet.class)
public class ImplementationResourceComponent
     extends HttpServlet
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Mapping between file extensions and MIME types.
   */
  private static Properties extensions2mimeTypes = new Properties();
  static {
    // Load mapping between file extensions and MIME types.
    try {
      extensions2mimeTypes.load(
        ImplementationResourceComponent.class.getClassLoader().getResourceAsStream(
          ImplementationResourceComponent.class.getPackage().getName().replace('.', '/') + "/extensions2mimeTypes.properties"
        )
      );
    } catch(IOException ioe) {
      throw new Error(ioe);
    }
  }

  /**
   * Classloader where resources are searched.
   */
  @Property(name = "classloader")
  private ClassLoader classloader;

  /**
   * Location where resources are searched.
   */
  @Property(name = "location")
  private String location;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
	// The requested resource.
    String pathInfo = request.getPathInfo();
    if(pathInfo == null) {
      pathInfo = "";
    }
    int idx = pathInfo.lastIndexOf('.');
    String extension = (idx != -1) ? pathInfo.substring(idx) : "";

    // Search the requested resource into the class loader.
    InputStream is = this.classloader.getResourceAsStream(this.location + pathInfo);

    if(is == null) {
      // Requested resource not found.
      super.doGet(request, response);
      return;
    }

    // Requested resource found.
    response.setStatus(HttpServletResponse.SC_OK);
    String mimeType = extensions2mimeTypes.getProperty(extension);
    if(mimeType == null) {
      mimeType = "text/plain";
    }
    response.setContentType(mimeType);

    // Copy the resource stream to the servlet output stream.
    Stream.copy(is, response.getOutputStream());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
