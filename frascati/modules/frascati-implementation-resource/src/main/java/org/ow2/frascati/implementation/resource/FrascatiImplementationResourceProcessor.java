/**
 * OW2 FraSCAti: SCA Implementation Resource
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.implementation.resource;

import java.util.List;
import javax.servlet.Servlet;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.domainmodel.tuscany.ResourceImplementation;
import org.eclipse.stp.sca.domainmodel.tuscany.TuscanyPackage;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * OW2 FraSCAti Assembly Factory implementation resource processor class.
 *
 * @author Philippe Merle - INRIA
 * @version 1.4
 */
public class FrascatiImplementationResourceProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<ResourceImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(ResourceImplementation resourceImplementation, StringBuilder sb)
  {
    sb.append("tuscany:implementation.resource");
    append(sb, "location", resourceImplementation.getLocation());
    super.toStringBuilder(resourceImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(ResourceImplementation resourceImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    String location = resourceImplementation.getLocation();

    checkAttributeMustBeSet(resourceImplementation, "location", location, processingContext);

    // Check that location is present in the class path.
    if(!isNullOrEmpty(location) && processingContext.getClassLoader().getResourceAsStream(location) == null) {
      // Location not found.
      error(processingContext, resourceImplementation, "Location '" + location + "' not found");
    }

    // Get the enclosing SCA component.
    Component component = getParent(resourceImplementation, Component.class);

    if(component.getProperty().size() != 0) {
      error(processingContext, resourceImplementation, "<implementation.resource> can't have SCA properties");
    }

    if(component.getReference().size() != 0) {
      error(processingContext, resourceImplementation, "<implementation.resource> can't have SCA references");
    }

    List<ComponentService> services = component.getService();
    if(services.size() > 1) {
      error(processingContext, resourceImplementation, "<implementation.resource> can't have more than one SCA service");
    } else {
      ComponentService service = null;
      if(services.size() == 0) {
        // The component has zero service then add a service to the component.
        service = ScaFactory.eINSTANCE.createComponentService();
        service.setName("Resource");
        services.add(service);
      } else {
    	// The component has one service.
        service = services.get(0);
      }
      // Get the service interface.
      Interface itf = service.getInterface();
      if(itf == null) {
        // The component service has no interface than add a Java Servlet interface.
        JavaInterface javaInterface = ScaFactory.eINSTANCE.createJavaInterface();
        javaInterface.setInterface(Servlet.class.getName());
        service.setInterface(javaInterface);
      } else {
    	// Check if this is a Java Servlet interface.
        boolean isServletInterface = (itf instanceof JavaInterface)
                                   ? Servlet.class.getName().equals(((JavaInterface)itf).getInterface())
                                   : false;
    	if(!isServletInterface) {
          error(processingContext, resourceImplementation, "<service name=\"" + service.getName() + "\"> must have an <interface.java interface=\"" + Servlet.class.getName() + "\">");
    	} 
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(resourceImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(ResourceImplementation resourceImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Generate a FraSCAti SCA primitive component.
    generateScaPrimitiveComponent(resourceImplementation, processingContext, ImplementationResourceComponent.class.getName());
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(ResourceImplementation resourceImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate a FraSCAti SCA primitive component.
    org.objectweb.fractal.api.Component component =
      instantiateScaPrimitiveComponent(resourceImplementation, processingContext, ImplementationResourceComponent.class.getName());

    // Retrieve the SCA property controller of this Fractal component.
    SCAPropertyController propertyController =  (SCAPropertyController)getFractalInterface(component, SCAPropertyController.NAME);

    // Set the classloader property.
    propertyController.setValue("classloader", processingContext.getClassLoader());

    // Set the location property.
    propertyController.setValue("location", resourceImplementation.getLocation());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.processor.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(TuscanyPackage.Literals.RESOURCE_IMPLEMENTATION);
  }

}
