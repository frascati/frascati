/**
 * OW2 FraSCAti: SCA Interface WSDL
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.util.Collection;
import java.util.List;
import javax.wsdl.Definition;
import javax.wsdl.Import;
import javax.wsdl.PortType;
import javax.xml.namespace.QName;

/**
 * Helper functions related to WSDL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
public class WsdlHelper
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Search a port type into a WSDL definition and all its imported WSDL recursively.
   *
   * @param definition The WSDL definition.
   * @param portTypeName The name of the searched WSDL port type.
   * @return The found port type or null if not found.
   */
  public static PortType getPortTypeIntoDefinitionAndAllItsImportsRecursively(Definition definition, String portTypeName)
  {
    // Search the requested port type.
    PortType portType = definition.getPortType(new QName(definition.getTargetNamespace(), portTypeName));
    if(portType == null) {
      // If the portType was not found then search it into all WSDL import recursively.
      for(List<Import> importList : ((Collection<List<Import>>)(definition.getImports().values()))) {
        for(Import importElement : importList) {
          portType = getPortTypeIntoDefinitionAndAllItsImportsRecursively(importElement.getDefinition(), portTypeName);
          if(portType != null) {
            break; // portType was found.
          }
        }
      }
    }
    // return the found portType or null if not found.
    return portType;
  }
}
