/**
 * OW2 FraSCAti: SCA Interface WSDL
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Gwenael Cattez
 *
 */

package org.ow2.frascati.wsdl;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.wsdl.Definition;
import javax.wsdl.WSDLException;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;
import org.apache.cxf.tools.common.ToolContext;
import org.apache.cxf.tools.wsdlto.WSDLToJava;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.FrascatiException;
import com.sun.xml.bind.api.impl.NameConverter;

/**
 * OW2 FraSCAti Assembly Factory WSDL compiler with Apache CXF.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public class WsdlCompilerCXF
     extends AbstractLoggeable
  implements WsdlCompiler
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Options for the Apache CXF WSDL2Java compiler.
   */
  @Property(name = "wsdl2java-options")
  private String wsdl2javaOptions = "-xjc-XtoString";
  
  /**
   * Target directory.
   */
  @Property(name = "target-directory")
  private String targetDirectory = "wsdl2java";

  @Property(name = "binding-file-name")
  private String bindingFileName = "jaxb-global-bindings.xml";
  
  /**
   * Reader of WSDL descriptions.
   */
  private WSDLReader wsdlReader;

  /**
   * Already compiled WSDL files.
   */
  private Map<String, String> alreadyCompiledWsdlFiles = new HashMap<String, String>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initialize the component.
   */
  @Init
  public final void initialize() throws FrascatiException
  {
    // Initialize the WSDL reader.
	try {
      WSDLFactory wsdlFactory = WSDLFactory.newInstance();
      this.wsdlReader = wsdlFactory.newWSDLReader();
      this.wsdlReader.setFeature("javax.wsdl.verbose", false);
      this.wsdlReader.setFeature("javax.wsdl.importDocuments", true);
    } catch (WSDLException we) {
      severe(new FrascatiException("Could not initialize WSDLReader", we));
    }
  }

  /**
   * @see WsdlCompiler#readWSDL(String)
   */
  public final Definition readWSDL(String wsdlUri) throws WSDLException
  {
    return this.wsdlReader.readWSDL(wsdlUri);
  }

  /**
   * @see WsdlCompiler#compileWSDL(String,JaxWsBindingList,ProcessingContext)
   */
  public final void compileWSDL(String wsdlUri, JaxWsBindingList bindings, ProcessingContext processingContext) throws Exception
  {
	// Check if the given WSDL file was already compiled.
    if(this.alreadyCompiledWsdlFiles.get(wsdlUri) != null) {
      log.info("WSDL '" + wsdlUri + "' already compiled.");
      return;
    }

    // Keep that this wsdl file is compiled to avoid to recompile it several times.
    this.alreadyCompiledWsdlFiles.put(wsdlUri, wsdlUri);

    try {
      // Read the WSDL definition.
      Definition definition = readWSDL(wsdlUri);
      // Try to load the ObjectFactory class of the Java package corresponding to the WSDL targetNamespace.
      String objectFactoryClassName = NameConverter.standard.toPackageName(definition.getTargetNamespace()) + ".ObjectFactory";
      processingContext.loadClass(objectFactoryClassName);
      // If found then the WSDL file was already compiled.
      log.info("WSDL '" + wsdlUri + "' already compiled.");
      return;
    } catch(ClassNotFoundException cnfe) {
      // ObjectFactory class not found then compile the WSDL file.
    }

    // TODO: Could be optimized to avoid to read the WSDL file twice.
    // Require to study the Apache CXF WSDL2Java class to find a more appropriate entry point.

    // Create WSDL compiler parameters.
    ArrayList<String> paramList = new ArrayList<String>();
    // Add wsdl2java options.
    paramList.add(this.wsdl2javaOptions);
    // Add the target directory.
    String outputDirectory = processingContext.getOutputDirectory() + '/' + targetDirectory;
    paramList.add("-d");
    paramList.add(outputDirectory);
    // Add -b option if the global JAX-WS binding file is present in the classpath.
    URL globalJaxWsBindingURL = processingContext.getResource(this.bindingFileName);
    if(globalJaxWsBindingURL != null) {
      paramList.add("-b");
      paramList.add(globalJaxWsBindingURL.toExternalForm());
    }
    // Add bindings passed as argument.
    for(URL bindingURL : bindings) {
      paramList.add("-b");
      paramList.add(bindingURL.toExternalForm());
    }
    // Add the WSDL to compile.
    paramList.add(wsdlUri);

    String[] params = paramList.toArray(new String[paramList.size()]);

    log.info("Compiling WSDL '" + wsdlUri + "' into '" + outputDirectory + "'...");    
    
    try {
      // Compile the WSDL description with Apache CXF WSDL2Java Tool.
      new WSDLToJava(params).run(new ToolContext(), System.out);
    } catch (Exception exc) {
      log.warning("Impossible to compile WSDL '" + wsdlUri + "'.");
      throw exc;
    }
	log.info("WSDL '" + wsdlUri + "' compiled.");
  
	// Add WSDL2Java output directory to the Juliac compiler at the first time needed.
    processingContext.addJavaSourceDirectoryToCompile(outputDirectory);    
  }
}
