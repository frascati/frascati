/**
 * OW2 FraSCAti: SCA Interface WSDL
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import com.sun.xml.bind.api.impl.NameConverter;

import java.net.URL;
import javax.wsdl.Definition;
import javax.wsdl.PortType;
import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.WSDLPortType;
import org.eclipse.stp.sca.ScaPackage;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractInterfaceProcessor;

/**
 * OW2 FraSCAti Assembly Factory SCA interface WSDL processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaInterfaceWsdlProcessor
     extends AbstractInterfaceProcessor<WSDLPortType>
{
  //---------------------------------------------------------------------------
  // Constants.
  // --------------------------------------------------------------------------

  /**
   * Attribute 'requires'.
   */
  static final String REQUIRES = "requires";

  /**
   * Location where JAXB bindings are stored.
   */
  static final String LOCATION_OF_BINDINGS = "org/ow2/frascati/wsdl/bindings/";

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Reference to the WSDL compiler.
   */
  @Reference(name="wsdl-compiler")
  private WsdlCompiler wsdlCompiler;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(WSDLPortType wsdlPortType, StringBuilder sb) {
    sb.append("sca:interface.wsdl");
    append(sb, "interface", wsdlPortType.getInterface());
    super.toStringBuilder(wsdlPortType, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(WSDLPortType wsdlPortType, ProcessingContext processingContext)
      throws ProcessorException
  {
	// Check the attribute 'interface'.
	String wsdlInterface = wsdlPortType.getInterface();
	if(wsdlInterface == null || wsdlInterface.equals("") ) {
      error(processingContext, wsdlPortType, "The attribute 'interface' must be set");
	} else {
      PortType portType = getPortType(wsdlPortType, processingContext, "interface", wsdlInterface);
      if(portType != null) {
        QName qname = portType.getQName();

        // Compute the Java interface name for the WSDL port type.
        String localPart = qname.getLocalPart();
        log.info("The interface QName is " + qname);
        String javaInterface = NameConverter.standard.toPackageName(qname.getNamespaceURI()) + '.' + NameConverter.standard.toClassName(localPart);
        log.info("The Java interface for '" + wsdlInterface + "' is " + javaInterface);

        // Load the Java interface.
        Class<?> clazz = null;
        try {
          clazz = processingContext.loadClass(javaInterface);
        } catch (ClassNotFoundException cnfe) {
          // If the Java interface is not found then this requires to compile WSDL to Java.
        }

        // TODO: check that this is a Java interface and not a Java class.

        // Store the Java class into the processing context.
        storeJavaInterface(wsdlPortType, processingContext, javaInterface, clazz);
      }
    }

    // Check the attribute 'callbackInterface'.
    wsdlInterface = wsdlPortType.getCallbackInterface();
    if(wsdlInterface != null) {
      getPortType(wsdlPortType, processingContext, "callback interface", wsdlInterface);
    }

    // Create a list of JAX-WS binding files.
    JaxWsBindingList bindings = new JaxWsBindingList();

    // Store the list of JAX-WS binding files.
    processingContext.putData(wsdlPortType, JaxWsBindingList.class, bindings);

    // Check the any attributes.
    for(FeatureMap.Entry entry : wsdlPortType.getAnyAttribute()) {
      String attributeName = entry.getEStructuralFeature().getName();
      // Check the attribute 'requires', aka intents.
      if(attributeName.equals(REQUIRES)) {
        // Iterate over the list of intents.
        for(String intent : ((String)entry.getValue()).split(" ")) {
          // Search the JAXB binding file for the current intent.
          URL bindingURL = processingContext.getResource(LOCATION_OF_BINDINGS + intent + ".xml");
          if(bindingURL == null) {
            // if not found then this is an error.
            error(processingContext, wsdlPortType, "The intent '" + intent + "' is not supported");              
          } else {
            // else store it.
            bindings.add(bindingURL);
          }
        }
      } else {
        error(processingContext, wsdlPortType, "The attribute '" + attributeName + "' is not supported");
      }
    }
  }

  /**
   * Compute the port type of an SCA WSDL interface.
   */
  private PortType getPortType(WSDLPortType wsdlPortType, ProcessingContext processingContext, String what, String scaWsdlInterface)
      throws ProcessorException
  {
    logDo(processingContext, wsdlPortType, "check the WSDL " + what + " " + scaWsdlInterface);

    PortType portType = null;

    // Compute uri and portTypeName from the SCA WSDL interface.
    String wsdlUri = null;
    String portTypeName = null;

    int idx = scaWsdlInterface.indexOf("#wsdl.interface(");
    if(idx != -1 && scaWsdlInterface.endsWith(")")) {
      wsdlUri = scaWsdlInterface.substring(0, idx);
      portTypeName = scaWsdlInterface.substring(idx + ("#wsdl.interface(").length(), scaWsdlInterface.length() - 1);
    }

    if(wsdlUri == null || portTypeName == null) {
      error(processingContext, wsdlPortType, "Invalid 'interface' value, must be uri#wsdl.interface(portType)");
    } else {

      // Search the WSDL file from the processing context's class loader.
      URL url = processingContext.getResource(wsdlUri);
      if(url != null) {
        wsdlUri = url.toString();
      }

      // Read the requested WSDL definition.
      Definition definition = null;
      try {
        definition = this.wsdlCompiler.readWSDL(wsdlUri);
      } catch(WSDLException we) {
  	    processingContext.error(toString(wsdlPortType) + " " + wsdlUri + ": " + we.getMessage());
  	    return null;
      }

      // Search the requested port type.
      portType = WsdlHelper.getPortTypeIntoDefinitionAndAllItsImportsRecursively(definition, portTypeName);
      if(portType == null) {
        error(processingContext, wsdlPortType, "Unknown port type name '", portTypeName, "'");
        return null;
      }
    }

    logDone(processingContext, wsdlPortType, "check the WSDL " + what + " " + scaWsdlInterface);
    return portType;
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(WSDLPortType wsdlPortType, ProcessingContext processingContext)
      throws ProcessorException
  {
    // If no Java interface computed during doCheck() then compile the WDSL file.
    if(getClass(wsdlPortType, processingContext) == null) {
      String scaWsdlInterface = wsdlPortType.getInterface();
      int idx = scaWsdlInterface.indexOf("#wsdl.interface(");
  	  String wsdlUri = scaWsdlInterface.substring(0, idx);

      // Search the WSDL file from the processing context's class loader.
      URL url = processingContext.getResource(wsdlUri);
      if(url != null) {
        wsdlUri = url.toString();
      }

      // Get the list of JAX-WS binding files.
      JaxWsBindingList bindings = processingContext.getData(wsdlPortType, JaxWsBindingList.class);

      // Compile the WSDL description.
      try {
        this.wsdlCompiler.compileWSDL(wsdlUri, bindings, processingContext);
      } catch(Exception exc) {
        severe(new ProcessorException("Error when compiling WSDL", exc));
        return;
      }
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.WSDL_PORT_TYPE);
  }

}
