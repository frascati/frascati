/**
 * OW2 FraSCAti: SCA Implementation BPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.test;

import javax.xml.ws.Holder;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

import org.ow2.frascati.implementation.bpel.test.factorial.FactorialDocumentPT;
import org.ow2.frascati.implementation.bpel.test.echo.Echo;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest {

	/**
	 * FraSCAti.
	 */
    static FraSCAti frascati;

    @BeforeClass
    public static void initFraSCAti() throws Exception {
      frascati = FraSCAti.newFraSCAti();
    }

    /**
     * Check errors produced during the checking phase.
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processCheckingErrorsWarningsComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        frascati.processComposite("CheckingErrorsWarnings", processingContext);
      } catch(ManagerException me) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrorsWarnings.composite'
    	assertEquals("The number of checking errors", 2, processingContext.getErrors());
        // Let's note that the file 'CheckingErrorsWarnings.composite' produces 2 warnings,
        // one warning is also produced by the OW2 FraSCAti Parser (EMF diagnostics).
        assertEquals("The number of checking warnings", 3, processingContext.getWarnings());
      }
    }

    @Test
    public void getEchoComposite() throws Exception {
      Component echoComposite = frascati.getComposite("Echo/Echo.composite");
      Echo echo = frascati.getService(echoComposite, "echo", Echo.class);
      String echoRequest = "Philippe Merle";
      String echoResponse = echo.process(echoRequest);
      assertEquals("echo.process(" + echoRequest + ")", echoRequest, echoResponse);
      frascati.close(echoComposite);
    }

    @Test
    public void getFactorialComposite() throws Exception {
      Component factorialComposite = frascati.getComposite("Factorial/Factorial.composite");
      FactorialDocumentPT factorial = frascati.getService(factorialComposite, "receiver", FactorialDocumentPT.class);
      Holder<Double> request = new Holder(new Double(5));
      factorial.factorial(request);
      System.out.println("factorial(5) ==> " + request.value);
      assertEquals("factorial(5) == 120", request.value, new Double(120));
      frascati.close(factorialComposite);
    }

}
