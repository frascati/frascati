/**
 * OW2 FraSCAti: SCA Implementation BPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.test;

import static org.junit.Assert.assertEquals;

import javax.xml.ws.Holder;

import org.junit.Test;
import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkInput;
import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkOutput;
import org.ow2.frascati.implementation.bpel.api.BPELProcess;
import org.ow2.frascati.implementation.bpel.easybpel.EasyBpelEngine;
import org.ow2.frascati.implementation.bpel.test.echo.Echo;
import org.ow2.frascati.implementation.bpel.test.factorial.FactorialDocumentPT;

/**
 * JUnit test case for EasyBpelEngine class. 
 *
 * @author Philippe Merle.
 */
public class EasyBpelEngineTest {

    @Test
    public void test_factorial() throws Exception
    {
      // Create a FraSCAti EasyBPEL engine and initialize it.
      EasyBpelEngine easyBpel = new EasyBpelEngine();
      easyBpel.initialize();

      // Create a new BPEL process and deploy it.
      BPELProcess bpelProcess = easyBpel.newBPELProcess("./src/test/resources/Factorial/factorial.bpel");
      bpelProcess.deploy();

      // Get the BPEL partner links.
      BPELPartnerLinkInput receiver = bpelProcess.getBPELPartnerLinkInput("receiver");
      BPELPartnerLinkOutput invoker = bpelProcess.getBPELPartnerLinkOutput("invoker");

      // Get the typed proxy for partner link 'receiver'.
      FactorialDocumentPT factorial = receiver.getProxy(FactorialDocumentPT.class);

      // Create the recursive binding for 'invoker' to 'receiver'.
      invoker.setDelegate(factorial, FactorialDocumentPT.class);

      // Do several Java invocations.
      Holder<Double> request = new Holder(new Double(5));    
      factorial.factorial(request);
      System.out.println("factorial(5) ==> " + request.value);
      assertEquals("factorial(5) == 120", request.value, new Double(120));
    }

    @Test
    public void test_echo() throws Exception
    {
      // Create a FraSCAti EasyBPEL engine and initialize it.
      EasyBpelEngine easyBpel = new EasyBpelEngine();
      easyBpel.initialize();

      // Create a new BPEL process and deploy it.
      BPELProcess bpelProcess = easyBpel.newBPELProcess("./src/test/resources/Echo/Echo.bpel");
      bpelProcess.deploy();

      // Get the BPEL partner link 'client'.
      BPELPartnerLinkInput client = bpelProcess.getBPELPartnerLinkInput("client");

      // Create a typed proxy to the client partner link.
      Echo echo = client.getProxy(Echo.class);

      // Do several Java invocations.
      echo.process ("Philippe Merle");
      echo.process ("Merle Philippe");
      echo.process ("Everybody");
   }

}
