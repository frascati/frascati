/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Lionel Seinturier
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicComponentType;

import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Implements a FraSCAti composite where getFcSubComponents() could be redefined by subclasses.
 *
 * @author Philippe Merle - INRIA
 */
public abstract class AbstractFrascatiContainer
              extends AbstractLoggeable
           implements Component,
                      ContentController,
                      NameController,
                      SCAPropertyController
{
  //---------------------------------------------------------------------------
  //
  // Internal state.
  //
  //---------------------------------------------------------------------------

  /**
   * Map of Fractal interfaces.
   */
  private Map<String, Object> interfaces = new HashMap<String, Object>();

  /**
   * Name of the component.
   */
  private String componentName;

  /**
   * Default constructor.
   */
  protected AbstractFrascatiContainer()
  {
    interfaces.put("component", this);
    interfaces.put("content-controller", this);
    interfaces.put("name-controller", this);
    interfaces.put("sca-component-controller", this);
    interfaces.put("sca-property-controller", this);
  }

  //---------------------------------------------------------------------------
  //
  // Implementation for interface Component.
  //
  //---------------------------------------------------------------------------

  /**
   * @see Component#getFcType()
   */
  public Type getFcType()
  {
    log.finest("getFcType() called");
    try {
      return new BasicComponentType(new InterfaceType[0]);
    } catch(InstantiationException ie) {
      throw new Error(ie);
    }
  }

  /**
   * @see Component#getFcInterfaces()
   */
  public Object[] getFcInterfaces()
  {
    log.finest("getFcInterfaces() called");
    return interfaces.values().toArray(new Object[interfaces.size()]);
  }

  /**
   * @see Component#getFcInterface(String)
   */
  public Object getFcInterface(String interfaceName)
    throws NoSuchInterfaceException
  {
    log.finest("getFcInterface(\"" + interfaceName + "\") called");
    Object result = interfaces.get(interfaceName);
    if(result == null) {
      throw new NoSuchInterfaceException(interfaceName);
    }
    return result;
  }

  //---------------------------------------------------------------------------
  //
  // Implementation for interface ContentController.
  //
  //---------------------------------------------------------------------------

  /**
   * @see ContentController#getFcInternalInterfaces()
   */
  public Object[] getFcInternalInterfaces()
  {
    log.finest("getFcInternalInterfaces() called");
    return new Object[0];
  }

  /**
   * @see ContentController#getFcInternalInterface(String)
   */
  public Object getFcInternalInterface(String interfaceName)
    throws NoSuchInterfaceException
  {
    log.finest("getFcInternalInterface(\"" + interfaceName + "\") called");
    throw new NoSuchInterfaceException(interfaceName);
  }

  /**
   * @see ContentController#addFcSubComponent(Component)
   */
  public void addFcSubComponent(Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    log.finest("addFcSubComponent() called");
    throw new IllegalContentException("Not supported");
  }

  /**
   * @see ContentController#removeFcSubComponent(Component)
   */
  public void removeFcSubComponent(Component subComponent)
    throws IllegalContentException, IllegalLifeCycleException
  {
    log.finest("removeFcSubComponent() called");
    throw new IllegalContentException("Not supported");
  }

  //---------------------------------------------------------------------------
  //
  // Implementation for interface NameController.
  //
  //---------------------------------------------------------------------------

  /**
   * @see NameController#getFcName()
   */
  public String getFcName()
  {
    log.finest("getFcName() called");
    return this.componentName;
  }

  /**
   * @see NameController#setFcName(String)
   */
  public void setFcName(String componentName)
  {
    log.finest("setFcName(\"" + componentName + "\") called");
    this.componentName = componentName;
  }

  //---------------------------------------------------------------------------
  //
  // Implementation for interface SCAPropertyController.
  //
  //---------------------------------------------------------------------------

  /**
   * @see SCAPropertyController#setType(String, Class)
   */
  public void setType(String name, Class<?> type)
  {
    log.finest("setType(\"" + name + "\", " + type + ") called");
  }

  /**
   * @see SCAPropertyController#setValue(String, Object)
   */
  public void setValue(String name, Object value)
    throws IllegalArgumentException
  {
    log.finest("setValue(\"" + name + "\", " + value + ") called");
  }

  /**
   * @see SCAPropertyController#getType(String)
   */
  public Class<?> getType(String name)
  {
    log.finest("getType(\"" + name + "\") called");
    return null;
  }

  /**
   * @see SCAPropertyController#getValue(String)
   */
  public Object getValue(String name)
  {
    log.finest("getValue(\"" + name + "\") called");
    return null;
  }

  /**
   * @see SCAPropertyController#containsPropertyName(String)
   */
  public boolean containsPropertyName(String name)
  {
    log.finest("containsPropertyName(\"" + name + "\") called");
    return false;
  }

  /**
   * @see SCAPropertyController#getPropertyNames()
   */
  public String[] getPropertyNames()
  {
    log.finest("getPropertyNames() called");
    return new String[0];
  }

  /**
   * @see SCAPropertyController#setPromoter(String, SCAPropertyController)
   */
  public void setPromoter(String name, SCAPropertyController promoter)
    throws IllegalPromoterException
  {
    log.finest("setPromoter(\"" + name + "\") called");
  }

  /**
   * @see SCAPropertyController#getPromoter(String)
   */
  public SCAPropertyController getPromoter(String name)
  {
    log.finest("getPromoter(\"" + name + "\") called");
    return null;
  }

  /**
   * @see SCAPropertyController#setPromoter(String, SCAPropertyController, String)
   */
  public void setPromoter(
		String name, SCAPropertyController promoter,
		String promoterPropertyName)
    throws IllegalPromoterException
  {
    log.finest("setPromoter(\"" + name + "\", \"" + promoterPropertyName + "\") called");
  }

  /**
   * @see SCAPropertyController#getPromoterPropertyName(String)
   */
  public String getPromoterPropertyName(String name)
  {
    log.finest("getPromoterPropertyName(\"" + name + "\") called");
    return null;
  }

  /**
   * @see SCAPropertyController#isDeclared(String)
   */
  public boolean isDeclared(String name)
  {
    log.finest("isDeclared(\"" + name + "\") called");
    return false;
  }
  
  /**
   * @see SCAPropertyController#hasBeenSet(String)
   */
  public boolean hasBeenSet(String name)
  {
    log.finest("hasBeenSet(\"" + name + "\") called");
    return false;
  }

  /**
   * @see SCAPropertyController#removePromoter(String)
   */
  public void removePromoter( String name ) {
    log.finest("removePromoter(\"" + name + "\") called");	  
  }
}
