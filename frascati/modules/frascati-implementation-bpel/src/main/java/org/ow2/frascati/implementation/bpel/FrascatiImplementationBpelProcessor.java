/**
 * OW2 FraSCAti: SCA Implementation BPEL
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel;

import java.net.URL;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.BpelImplementation;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractCompositeBasedImplementationProcessor;
import org.ow2.frascati.implementation.bpel.api.BPELEngine;
// import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkInput;
// import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkOutput;
import org.ow2.frascati.implementation.bpel.api.BPELProcess;
import org.ow2.frascati.jaxb.JAXB;
import org.ow2.frascati.wsdl.JaxWsBindingList;
import org.ow2.frascati.wsdl.WsdlCompiler;
import org.ow2.frascati.util.FrascatiException;

/**
 * OW2 FraSCAti SCA Implementation BPEL processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class FrascatiImplementationBpelProcessor
     extends AbstractCompositeBasedImplementationProcessor<BpelImplementation, BPELProcess>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Classpath namespace.
   */
  public static final String CLASSPATH_NS = "classpath:";

  /**
   * The EasyBPEL engine.
   */
  @Reference(name="bpel-engine")
  private BPELEngine bpelEngine;

  /**
   * Reference to the WSDL compiler.
   */
  @Reference(name="wsdl-compiler")
  private WsdlCompiler wsdlCompiler;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(BpelImplementation bpelImplementation, StringBuilder sb) {
    sb.append("sca:implementation.bpel");
    append(sb, "process", bpelImplementation.getProcess());
    super.toStringBuilder(bpelImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(BpelImplementation bpelImplementation, ProcessingContext processingContext)
	      throws ProcessorException
  {
    // Check the 'process' attribute.
    QName bpelImplementationProcess = bpelImplementation.getProcess();
    if(bpelImplementationProcess == null) {
      error(processingContext, bpelImplementation, "The attribute 'process' must be set");
    } else {
      String processNamespace = bpelImplementationProcess.getNamespaceURI();
      String processUri = null;
      // When the process namespace starts by "classpath:"
      if(processNamespace.startsWith(CLASSPATH_NS)) {
        StringBuffer sb = new StringBuffer();
        sb.append(processNamespace.substring(CLASSPATH_NS.length()));
        if(sb.length() > 0) {
          sb.append('/');
        }
        sb.append(bpelImplementationProcess.getLocalPart());
        // Search the process file into the processing context's class loader.
        URL resource = processingContext.getResource(sb.toString());
        if(resource != null) {
          processUri = resource.toString();
        }
      }
      if(processUri == null) {
        processUri = processNamespace + '/' + bpelImplementationProcess.getLocalPart();
      }
      try {
        BPELProcess bpelProcess = bpelEngine.newBPELProcess(processUri);

        // Register of JAXB Object Factories.
        JAXB.registerObjectFactoryFromClassLoader(processingContext.getClassLoader());

//        System.out.println(bpelProcess);

        // Store the created BPELProcess into the processing context.
        processingContext.putData(bpelImplementation, BPELProcess.class, bpelProcess);
      } catch(FrascatiException exc) {
//        exc.printStackTrace();
        error(processingContext, bpelImplementation, "Can't read BPEL process ", bpelImplementationProcess.toString());
      } catch(Exception exc) {
        severe(new ProcessorException(bpelImplementation, "Can not read BPEL process " + bpelImplementationProcess, exc));
        return;
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(bpelImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(BpelImplementation bpelImplementation, ProcessingContext processingContext)
	      throws ProcessorException
  {
    // Obtain the BPELProcess stored during doCheck()
    BPELProcess bpelProcess = processingContext.getData(bpelImplementation, BPELProcess.class);

    // Compile all WSDL imported by the BPEL process.
    for(String wsdlUri : bpelProcess.getAllImportedWsdlLocations()) {
      // Check if the WSDL file is present in the processing context's class loader.
      URL url = processingContext.getResource(wsdlUri);
      if(url != null) {
        wsdlUri = url.toString();
      }
      // Compile the WSDL file.
      try {
        this.wsdlCompiler.compileWSDL(wsdlUri, new JaxWsBindingList(), processingContext);
      } catch(Exception exc) {
        error(processingContext, bpelImplementation, "Can't compile WSDL '", wsdlUri, "'");
      }
    }

    super.doGenerate(bpelImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(BpelImplementation bpelImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    Component containerOfProcesses = null;
    // Obtain the BPELProcess stored during doCheck()
    BPELProcess bpelProcess = processingContext.getData(bpelImplementation, BPELProcess.class);
    try {
      containerOfProcesses = bpelProcess.deploy();
    } catch(Exception e) {
      warning(new ProcessorException("Error during deployment of the BPEL process '" + bpelImplementation.getProcess() + "'", e));
      return;
    }

    // Instantiate the SCA composite containing the process.
    doInstantiate(bpelImplementation, processingContext, bpelProcess);

    // Set the name of the EasyBPEL container component.
    setFractalComponentName(containerOfProcesses, "implementation.bpel");

    // Add the EasyBPEL process component to the SCA BPEL composite.
    addFractalSubComponent(
      getFractalComponent(bpelImplementation, processingContext),
      containerOfProcesses);
  }

  /**
   * Get an SCA service.
   */
  @Override
  protected final Object getService(BPELProcess bpelProcess, String name, Class<?> interfaze)
  {
    return bpelProcess.getBPELPartnerLinkInput(name).getProxy(interfaze);
  }

  /**
   * Set an SCA reference.
   */
  @Override
  protected final void setReference(BPELProcess bpelProcess, String name, Object delegate, Class<?> interfaze)
  {
    bpelProcess.getBPELPartnerLinkOutput(name).setDelegate(delegate, interfaze);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.BPEL_IMPLEMENTATION);
  }

}
