/**
 * OW2 FraSCAti: SCA Implementation BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.api;

import java.util.List;

import org.objectweb.fractal.api.Component;

/**
 * OW2 FraSCAti BPEL Process interface.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public interface BPELProcess {

  /**
   * Get all partner link inputs.
   */
//  List<BPELPartnerLink> getBPELPartnerLinkInputs();

  /**
   * Get all partner link outputs.
   */
//  List<BPELPartnerLink> getBPELPartnerLinkOutputs();

  /**
   * Get all imported WSDL locations.
   */
  List<String> getAllImportedWsdlLocations();

  /**
   * Get an input partner link.
   *
   * @param name the name of the input partner link.
   */
  BPELPartnerLinkInput getBPELPartnerLinkInput(String name);

  /**
   * Get an output partner link.
   *
   * @param name the name of the output partner link.
   */
  BPELPartnerLinkOutput getBPELPartnerLinkOutput(String name);

  /**
   * Deploy the process.
   */
  Component deploy() throws Exception;

}
