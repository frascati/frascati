/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.lang.reflect.Method;

import com.ebmwebsourcing.easyviper.core.api.soa.message.Message;

import org.jdom.Element;

import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkOutput;
import org.ow2.frascati.wsdl.WsdlDelegate;
import org.ow2.frascati.wsdl.WsdlDelegateFactory;

/**
 * OW2 FraSCAti BPEL PartnerLink Output implementation with EasyBPEL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class EasyBpelPartnerLinkOutput
extends EasyBpelPartnerLink
implements BPELPartnerLinkOutput
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Delegate.
   */
  private WsdlDelegate delegate = null;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Delegate BPEL message to the delegate instance.
   */
  protected final Message invoke(Message message)
  {
    log.fine("Invoke Java delegate of the BPEL partner link output '" + easyBpelPartnerLink.getName() + "'");
    log.fine("  message.endpoint=" + message.getEndpoint());
    log.fine("  message.operationName=" + message.getOperationName());
    log.fine("  message.qname=" + message.getQName());
    log.fine("  message.service=" + message.getService());        
    log.fine("  message.content=" + message);

    Method method = this.delegate.getMethod(message.getOperationName());
    log.fine("Target Java method is " + method);
    try {
      Element response = this.delegate.invoke(method, message.getBody().getPayload());
      // TODO: Cr�er un nouveau InternalMessage plutot que d'utiliser le message en entr�e
      message.getBody().setPayload(response);

    } catch(Exception exc) {
      // TODO marshall exception to an XML message.
      exc.printStackTrace();
      throw new Error(exc);
    }
    
    return message;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see BPELPartnerLink#setDelegate(Object)
   */
  public final void setDelegate(Object delegate, Class<?> interfaze)
  {
    this.delegate = WsdlDelegateFactory.newWsdlDelegate(delegate, interfaze, delegate.getClass().getClassLoader());
  }

}
