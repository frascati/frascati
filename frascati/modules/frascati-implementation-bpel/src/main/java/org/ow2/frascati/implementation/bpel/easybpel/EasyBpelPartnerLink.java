/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import com.ebmwebsourcing.easybpel.model.bpel.api.partnerLink.PartnerLink;

import org.petalslink.abslayer.service.api.Interface;

import org.ow2.frascati.util.AbstractLoggeable;

/**
 * OW2 FraSCAti BPEL PartnerLink implementation with EasyBPEL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class EasyBpelPartnerLink
extends AbstractLoggeable
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  private static final String NL = "'\n";

  /**
   * The OW2 FraSCAti EasyBPEL process.
   */
  protected EasyBpelProcess easyBpelProcess;

  /**
   * The EasyBPEL partner link.
   */
  protected PartnerLink easyBpelPartnerLink;

  /**
   * Associated WSDL interface type.
   */
  protected Interface roleInterfaceType;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Initialization.
   */
  protected final void init()
  {
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see Object#toString()
   */
  @Override
  public final String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("EasyBpelPartnerLink\n");
    sb.append("\t - name = '" + easyBpelPartnerLink.getName() + NL);
    sb.append("\t - myRole = '" + easyBpelPartnerLink.getMyRole() + NL);
    sb.append("\t - partnerRole = '" + easyBpelPartnerLink.getPartnerRole() + NL);
    sb.append("\t - initializePartnerRole = '" + easyBpelPartnerLink.getInitializePartnerRole() + NL);
    sb.append("\t - partnerLinkType = '" + easyBpelPartnerLink.getPartnerLinkType() + NL);
    sb.append("\t - role.interface.qname = '" + (roleInterfaceType == null ? null : roleInterfaceType.getQName()) + NL);
    return sb.toString();
  }

}
