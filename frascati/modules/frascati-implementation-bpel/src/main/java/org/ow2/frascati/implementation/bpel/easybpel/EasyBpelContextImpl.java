/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import com.ebmwebsourcing.easyviper.core.api.engine.handler.TerminationHandler;
import com.ebmwebsourcing.easyviper.core.api.env.ExternalContext;
import com.ebmwebsourcing.easyviper.core.api.soa.message.Message;

import org.ow2.frascati.util.AbstractLoggeable;

/**
 * @author Philippe Merle - INRIA
 */
class EasyBpelContextImpl
extends AbstractLoggeable
implements ExternalContext
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * EasyViper termination handler.
   */
  private TerminationHandler th = null;

  /**
   * The current OW2 FraSCAti EasyBPEL partner link input.
   */
  protected EasyBpelPartnerLinkInput easyBpelPartnerLinkInput;

  /**
   * The BPEL message reply.
   */
  private Message reply = null;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Wait the BPEL reply message.
   */
  protected final synchronized Message waitReply()
      throws InterruptedException
  {
    while(this.reply == null) {
      this.wait();
    }
    return this.reply;
  }

  /**
   * Notify the BPEL reply message.
   */
  protected final synchronized void notifyReply(Message reply)
  {
    this.reply = reply;
    this.notifyAll();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see ExternalContext#getTerminationHandler()
   */
  public final TerminationHandler getTerminationHandler() {
    return this.th;
  }

  /**
   * @see ExternalContext#setTerminationHandler(TerminationHandler)
   */
  public final void setTerminationHandler(TerminationHandler th) {
    this.th = th;
  }

}
