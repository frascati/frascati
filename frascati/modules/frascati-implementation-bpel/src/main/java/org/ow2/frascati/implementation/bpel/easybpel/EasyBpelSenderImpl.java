/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import com.ebmwebsourcing.easyviper.core.api.CoreException;
import com.ebmwebsourcing.easyviper.core.api.env.ExternalContext;
import com.ebmwebsourcing.easyviper.core.api.env.Sender;
import com.ebmwebsourcing.easyviper.core.api.soa.Partner;
import com.ebmwebsourcing.easyviper.core.api.soa.message.Message;
import com.ebmwebsourcing.easyviper.core.impl.env.AbstractSenderImpl;
import com.ebmwebsourcing.easyviper.core.impl.engine.ProcessImpl;

import java.util.logging.Logger;
import java.util.Map;

/**
 * @author Philippe Merle - INRIA
 */
@org.osoa.sca.annotations.Service(Sender.class)
public class EasyBpelSenderImpl
     extends AbstractSenderImpl
  implements Sender {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private Logger log = Logger.getLogger(this.getClass().getCanonicalName());

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see Sender#sendTo(InternalMessage, Endpoint, ExternalContext)
   */
  public final void sendTo(final Message message, final String address,
                     final Map<Partner, Map<String,ExternalContext>> context, boolean isReply) throws CoreException
  {
    // TODO: Is there a more simple way for this?
    EasyBpelContextImpl myContext = (EasyBpelContextImpl) ProcessImpl.getFirstExternalContext(context);

    log.fine("message.QName=" + message.getQName());
    log.fine("message.interface=" + message.getInterface());
    log.fine("message.endpoint=" + message.getEndpoint());
    log.fine("message.service=" + message.getService());
    log.fine("message.operationName=" + message.getOperationName());
    log.fine("message.body.payloadAstring=" + message.getBody().getPayloadAstring());
    log.fine("address=" + address);
  
    // Here is needed to differentiate BPEL replies and BPEL asynchronous sends.
    if(isReply) {
      // This is a reply to a client request.
      myContext.notifyReply(message);
    } else {
      // This is an asynchronous invocation of a partner link.
      final EasyBpelPartnerLinkOutput plo = myContext.easyBpelPartnerLinkInput.easyBpelProcess.getPartnerLinkByInterfaceName(message.getInterface());
      // Create an asynchronous thread to execute the invocation.
      new Thread() { public void run() {
    	// Asynchronous invocation.
        plo.invoke(message);
      }}.start();
    }
  }

  /**
   * @see Sender#sendSyncTo(InternalMessage, Endpoint, ExternalContext)
   */
  public final Message sendSyncTo(final Message message,
      final String address, final Map<Partner, Map<String,ExternalContext>> context) throws CoreException
  {
    // TODO: Is there a more simple way for this?
    EasyBpelContextImpl myContext = (EasyBpelContextImpl) ProcessImpl.getFirstExternalContext(context);

    EasyBpelPartnerLinkOutput plo = myContext.easyBpelPartnerLinkInput.easyBpelProcess.getPartnerLinkByInterfaceName(message.getInterface());
    return plo.invoke(message);
  }

  /**
   * @see Sender#sendTo(CoreException, ExternalContext)
   */
  public final void sendTo(CoreException e, Map<Partner, Map<String,ExternalContext>> context) throws CoreException
  {
    // TODO: Must be implemented.
    log.severe("EasyBpelSenderImpl sendTo CoreException=" + e + " NOT IMPLEMENTED");
    e.printStackTrace();
  }

}
