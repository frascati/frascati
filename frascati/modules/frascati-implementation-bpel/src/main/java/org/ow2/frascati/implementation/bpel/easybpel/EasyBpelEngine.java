/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.net.URI;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.implementation.bpel.api.BPELEngine;
import org.ow2.frascati.implementation.bpel.api.BPELProcess;
import org.ow2.frascati.util.AbstractLoggeable;
import org.ow2.frascati.util.FrascatiException;

import com.ebmwebsourcing.easybpel.model.bpel.api.BPELException;
import com.ebmwebsourcing.easybpel.model.bpel.api.BPELFactory;
import com.ebmwebsourcing.easybpel.model.bpel.api.inout.BPELReader;
import com.ebmwebsourcing.easybpel.model.bpel.impl.BPELFactoryImpl;
import com.ebmwebsourcing.easyviper.core.api.Core;
import com.ebmwebsourcing.easyviper.core.api.engine.configuration.ConfigurationEngine;
import com.ebmwebsourcing.easyviper.core.impl.engine.configuration.ConfigurationEngineImpl;

/**
 * OW2 FraSCAti BPEL engine with EasyBPEL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public class EasyBpelEngine
     extends AbstractLoggeable
  implements BPELEngine
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * SCA property to configure EasyBPEL with Explorer or not.
   */
  @Property(name="explorer")
  private boolean withExplorer = false;

  /**
   * An EasyBPEL BPELFactory.
   */
  private BPELReader bpelReader;

  /**
   * An EasyViper core.
   */
  private Core core;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initializes the EasyBPEL engine.
   */
  @Init
  public final void initialize() throws Exception
  {
    // Set the transformer factory used by EasyBPEL.
    System.setProperty("javax.xml.transform.TransformerFactory",
        "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");

    log.fine("OW2 FraSCAti EasyBPEL initialization...");
    BPELFactory bpelFactory = BPELFactoryImpl.getInstance();
    this.bpelReader = bpelFactory.newBPELReader();

    log.fine("EasyBPEL Core initialization...");

    // set up the EasyBPEL core configuration.
    ConfigurationEngine conf = new ConfigurationEngineImpl();
    boolean easyBpelExplorer = Boolean.getBoolean("easybpel.explorer");
    conf.setExplorer(withExplorer || easyBpelExplorer);

    // create the EasyBPEL core.
    // TODO: why 10 ? or another value ?
    this.core = bpelFactory.newBPELEngine(conf, 10,
        EasyBpelReceiverImpl.class, 10, EasyBpelSenderImpl.class, log);
  }

  /**
   * @see BPELEngine#neBPELProcess(String)
   */
  public final BPELProcess newBPELProcess(String processUri) throws Exception
  {
    // EasyBPEL waits for URIs, i.e., where spaces are encoded by %20.
    processUri = processUri.replaceAll(" ", "%20");

    log.fine("Create an EasyBPEL BPELProcess for '" + processUri + "'...");
    com.ebmwebsourcing.easybpel.model.bpel.api.BPELProcess bpelProcess = null;
    try {
      bpelProcess = bpelReader.readBPEL(new URI(processUri));
    } catch(BPELException bexc) {
      throw new FrascatiException("EasyBPEL can not read the BPEL process '" + processUri + "'", bexc);
    }
    EasyBpelProcess easyBpelProcess = new EasyBpelProcess(this.core, processUri, bpelProcess);
    log.fine(easyBpelProcess.toString());
    return easyBpelProcess;
  }

}
