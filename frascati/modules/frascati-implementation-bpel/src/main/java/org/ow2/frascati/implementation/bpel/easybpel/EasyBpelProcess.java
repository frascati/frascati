/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.objectweb.fractal.api.Component;
// import org.objectweb.fractal.api.control.ContentController;
import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkInput;
import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkOutput;
import org.ow2.frascati.implementation.bpel.api.BPELProcess;
import org.ow2.frascati.util.AbstractLoggeable;
import org.petalslink.abslayer.service.api.Endpoint;
import org.petalslink.abslayer.service.api.Interface;
import org.petalslink.abslayer.service.api.PartnerLinkType;
import org.petalslink.abslayer.service.api.Role;
import org.petalslink.abslayer.service.api.Service;

import com.ebmwebsourcing.easybpel.model.bpel.api.partnerLink.PartnerLink;
import com.ebmwebsourcing.easybpel.model.bpel.api.wsdlImports.Import;
import com.ebmwebsourcing.easyviper.core.api.Core;
import com.ebmwebsourcing.easyviper.core.api.engine.Process;
import com.ebmwebsourcing.easyviper.core.impl.model.registry.ProcessContextDefinitionImpl;
import com.ebmwebsourcing.easyviper.core.impl.model.registry.ProcessKeyImpl;

/**
 * OW2 FraSCAti BPEL Process implementation with EasyBPEL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class EasyBpelProcess
extends AbstractLoggeable
implements BPELProcess
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * New line constant for method toString().
   */
  private static final String NL = "'\n";

  /**
   * WSDL namespace.
   */
  private static final String WSDL_NS = "http://schemas.xmlsoap.org/wsdl/";
	  
  /**
   * An EasyViper core.
   */
  private Core core;

  /**
   * The URI to the .bpel file.
   */
  private String processUri;

  /**
   * The EasyBPEL process.
   */
  private com.ebmwebsourcing.easybpel.model.bpel.api.BPELProcess bpelProcess;

  /**
   * Map of BPEL partner link inputs.
   */
  private Map<String, EasyBpelPartnerLinkInput> partnerLinkInputs;

  /**
   * Map of BPEL partner link outputs.
   */
  private Map<String, EasyBpelPartnerLinkOutput> partnerLinkOutputs;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs.
   */
  protected EasyBpelProcess(Core core, String uri, com.ebmwebsourcing.easybpel.model.bpel.api.BPELProcess bpelProcess)
      throws Exception
  {
    this.core = core;
    this.processUri = uri;
    this.bpelProcess = bpelProcess;

	this.partnerLinkInputs = new HashMap<String, EasyBpelPartnerLinkInput>();
    this.partnerLinkOutputs = new HashMap<String, EasyBpelPartnerLinkOutput>();
    for(PartnerLink partnerLink : bpelProcess.getPartnerLinks()) {
      EasyBpelPartnerLink pl = null;
      PartnerLinkType partnerLinkType = bpelProcess.getImports().getPartnerLinkType(partnerLink.getPartnerLinkType());
      if(partnerLink.getMyRole() != null) {
        EasyBpelPartnerLinkInput pli = new EasyBpelPartnerLinkInput();
        partnerLinkInputs.put(partnerLink.getName(), pli);
        pl = pli;
        Role myRole = partnerLinkType.getRole(partnerLink.getMyRole());
        Interface myRoleInterfaceType = bpelProcess.getImports().findInterface(myRole.getInterfaceQName());
        for(Service service : bpelProcess.getImports().getServices()) {
          for(Endpoint endpoint : service.getEndpoints()) {
            if(endpoint.getBinding().getInterface().getQName().equals(myRoleInterfaceType.getQName())) {
              pli.roleInterfaceType = myRoleInterfaceType;
              pli.service = service.getQName();
              pli.endpoint = endpoint.getName();
              break;
            }
          }
        }
      }
      if(partnerLink.getPartnerRole() != null) {
        EasyBpelPartnerLinkOutput plo = new EasyBpelPartnerLinkOutput();
        partnerLinkOutputs.put(partnerLink.getName(), plo);
        pl = plo;
        Role partnerRole = partnerLinkType.getRole(partnerLink.getPartnerRole());
        pl.roleInterfaceType = bpelProcess.getImports().findInterface(partnerRole.getInterfaceQName());
      }
      pl.easyBpelProcess = this;
      pl.easyBpelPartnerLink = partnerLink;
      pl.init();
    }
  }

  protected final EasyBpelPartnerLinkOutput getPartnerLinkByInterfaceName(QName qname)
  {
    log.fine("Get partner link for " + qname);
    EasyBpelPartnerLinkOutput result = null;
    for(EasyBpelPartnerLinkOutput plo : partnerLinkOutputs.values()) {
      if(plo.roleInterfaceType.getQName().equals(qname)) {
        result = plo;
        break;
      }
    }
    log.fine("Return partner link " + result);
    return result;
  }

  protected final Core getCore()
  {
    return this.core;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see BPELProcess#getAllImportedWsdlLocations()
   */
  public final List<String> getAllImportedWsdlLocations()
  {
    List<String> result = new ArrayList<String>();
    for(Import i : this.bpelProcess.getImports().getBPELImports()) {
      // If the BPEL import is a WSDL definition then add it to the result.
      if(WSDL_NS.equals(i.getImportType().toString())) {
        result.add(i.getLocation().toString());
      }
    }
    return result;
  }

  /**
   * @see BPELProcess#getBPELPartnerLinkInput(String)
   */
  public final BPELPartnerLinkInput getBPELPartnerLinkInput(String name)
  {
    log.fine("Get the BPEL partner link input '" + name + "' of the BPEL process '" + processUri + "'");
    return partnerLinkInputs.get(name);
  }

  /**
   * @see BPELProcess#getBPELPartnerLinkOutput(String)
   */
  public final BPELPartnerLinkOutput getBPELPartnerLinkOutput(String name)
  {
    log.fine("Get the BPEL partner link output '" + name + "' of the BPEL process '" + processUri + "'");
    return partnerLinkOutputs.get(name);
  }

  /**
   * @see BPELProcess#deploy()
   */
  public final Component deploy() throws Exception
  {
    log.fine("Deploy an EasyBPEL process '" + processUri + "'");
    ProcessContextDefinitionImpl context = new ProcessContextDefinitionImpl();
	this.core.getModel().getRegistry().storeProcessDefinition(new URI(this.processUri), context);
    return new BpelProcessContainer();
  }

  class BpelProcessContainer extends AbstractFrascatiContainer
  {
    /**
   	 * @see ContentController#getFcSubComponents()
     */
    public Component[] getFcSubComponents()
   	{
      log.finest("getFcSubComponents() called");
      // Get the first input partner link.
   	  EasyBpelPartnerLinkInput ebpli = (EasyBpelPartnerLinkInput)partnerLinkInputs.values().iterator().next();
      // Create a process key for this partner link.
   	  ProcessKeyImpl pk = new ProcessKeyImpl(ebpli.roleInterfaceType.getQName(), ebpli.service, ebpli.endpoint);
      List<Process> processes = null;
   	  try {
   	    // Get all the processes matching the process key.
   	    processes = getCore().getEngine().getProcessInstanceRegistry().getProcessInstances(pk);
      } catch(/*Core*/Exception ce) {
   	    return new Component[0];
      }
      // Return all the process components.
   	  Component[] result = new Component[processes.size()];
   	  int i = 0;
   	  for(Process process : processes) {
   	    result[i] = ((org.objectweb.fractal.api.Interface)process).getFcItfOwner();
        i++;
   	  }
   	  return result;
    }
  }
  
  /**
   * @see Object#toString()
   */
  @Override
  public final String toString()
  {
    StringBuffer sb = new StringBuffer();
    sb.append("EasyBpelProcess\n");
    sb.append("\t * processUrl='" + this.processUri + NL);
    sb.append("\t * partnerLinks\n");
    List<PartnerLink> partnerLinks = bpelProcess.getPartnerLinks();
    for(PartnerLink partnerLink : partnerLinks) {
      sb.append("\t\t * partnerLink name = '" + partnerLink.getName() + NL);
      sb.append("\t\t   - myRole = '" + partnerLink.getMyRole() + NL);
      sb.append("\t\t   - partnerRole = '" + partnerLink.getPartnerRole() + NL);
      sb.append("\t\t   - initializePartnerRole = '" + partnerLink.getInitializePartnerRole() + NL);
      sb.append("\t\t   - partnerLinkType = '" + partnerLink.getPartnerLinkType() + NL);
      try {
        PartnerLinkType partnerLinkType = bpelProcess.getImports().getPartnerLinkType(partnerLink.getPartnerLinkType());
        sb.append("\t\t\t - qname = '" + partnerLinkType.getQName() + NL);
        sb.append("\t\t\t - roles\n");
        try {
          for(Role role : partnerLinkType.getRoles()) {
            sb.append("\t\t\t\t - role name = '" + role.getName() + NL);
            sb.append("\t\t\t\t - interface\n");
            Interface itf = bpelProcess.getImports().findInterface(role.getInterfaceQName());
            sb.append("\t\t\t\t   - qname = '" + itf.getQName() + NL);
            sb.append("\t\t\t\t   - operations\n");

/*            
            for(Operation o : itf.getOperations()) {
                sb.append("\t\t\t\t     - operation name = '" + o.getQName() + NL);
                sb.append("\t\t\t\t       - input.messageName = '" + o.getInput().getMessageName() + NL);
                if(o.getOutput() != null) {
                  sb.append("\t\t\t\t       - output.messageName = '" + o.getOutput().getMessageName() + NL);
                }
            }
*/
          }

          if(partnerLink.getMyRole() != null) {
            Role myRole = partnerLinkType.getRole(partnerLink.getMyRole());
            sb.append("\t\t   - myRole.name = '" + myRole.getName() + NL);
            Interface myRoleInterfaceType = bpelProcess.getImports().findInterface(myRole.getInterfaceQName());
            sb.append("\t\t   - myRole.interface = '" + myRoleInterfaceType.getQName() + NL);

            // Search the Service and Port associated to this partnerLink.
            Service myRoleService = null;
            Endpoint myRoleEndpoint = null;
            for(Service service : bpelProcess.getImports().getServices()) {
              for(Endpoint endpoint : service.getEndpoints()) {
                if(endpoint.getBinding().getInterface() == myRoleInterfaceType) {
                  myRoleService = service;
                  myRoleEndpoint = endpoint;
                  break;
                }
              }
            }
            if(myRoleService != null) {
              sb.append("\t\t   - service = '" + myRoleService.getQName() + NL);
              sb.append("\t\t   - endpoint = '" + myRoleEndpoint.getName() + NL);
            }
          }

          if(partnerLink.getPartnerRole() != null) {
              Role partnerRole = partnerLinkType.getRole(partnerLink.getPartnerRole());
              sb.append("\t\t   - partnerRole.name = '" + partnerRole.getName() + NL);
              Interface partnerRoleInterfaceType = bpelProcess.getImports().findInterface(partnerRole.getInterfaceQName());
              sb.append("\t\t   - partnerRole.interface = '" + partnerRoleInterfaceType.getQName() + NL);
          }
        } catch(Exception e) {
          sb.append(e);
        }
      } catch(Exception e) {
        sb.append(e);
      }
    }
    return sb.toString();
  }
}
