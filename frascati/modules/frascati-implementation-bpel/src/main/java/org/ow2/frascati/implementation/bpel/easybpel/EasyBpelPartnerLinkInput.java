/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import javax.jws.Oneway;
import javax.xml.namespace.QName;

import com.ebmwebsourcing.easyviper.core.api.soa.message.Message;
import com.ebmwebsourcing.easyviper.core.impl.soa.message.MessageImpl;

// import org.petalslink.abslayer.service.api.Operation;

import org.ow2.frascati.implementation.bpel.api.BPELPartnerLinkInput;
import org.ow2.frascati.jdom.JDOM;
import org.ow2.frascati.wsdl.AbstractWsdlInvocationHandler;

/**
 * OW2 FraSCAti BPEL PartnerLink Input implementation with EasyBPEL.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class EasyBpelPartnerLinkInput
extends EasyBpelPartnerLink
implements BPELPartnerLinkInput
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Associated WSDL Service.
   */
  protected QName service;

  /**
   * Associated WSDL Endpoint (or port).
   */
  protected String endpoint;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see BPELPartnerLink#getProxy(Class)
   */
  @SuppressWarnings("unchecked")
  public final <T> T getProxy(Class<T> interfaze)
  {
    MyInvocationHandler ih = new MyInvocationHandler();
    ih.easyBpelPartnerLinkInput = this;
    return (T)Proxy.newProxyInstance(
                        interfaze.getClassLoader(),
                        new Class<?>[] { interfaze },
                        ih);
  }

  /**
   * Dynamic proxy invocation handler.
   *
   * @author Philippe Merle - INRIA
   */
  private static class MyInvocationHandler
               extends AbstractWsdlInvocationHandler
  {
    EasyBpelPartnerLinkInput easyBpelPartnerLinkInput;

    public final Object invoke(Object proxy, Method method, Object[] args)
      throws Throwable
    {
      log.fine("Invoke the BPEL partner link input '" + easyBpelPartnerLinkInput.easyBpelPartnerLink.getName() + "' with method " + method);

      // Marshall invocation, i.e.: method and arguments.
      String inputXmlMessage = marshallInvocation(method, args);
      log.fine("Input XML message " + inputXmlMessage);

      // Create an EasyBPEL internal message.
      log.fine("Create an EasyBPEL Message");
      Message bpelMessage = new MessageImpl(method.getName());
      bpelMessage.setService(easyBpelPartnerLinkInput.service);
      bpelMessage.setEndpoint(easyBpelPartnerLinkInput.endpoint);
// TODO: following seems to be not required with EasyBPEL 1.6-SNAPSHOT
//      bpelMessage.setInterface(easyBpelPartnerLinkInput.roleInterfaceType.getQName());
//      Operation op = easyBpelPartnerLinkInput.roleInterfaceType.getOperation(new QName(easyBpelPartnerLinkInput.roleInterfaceType.getQName().getNamespaceURI(), method.getName()));
//  	  bpelMessage.setQName(op.getInput().getMessageName());
      bpelMessage.getBody().setPayload(JDOM.toElement(inputXmlMessage));
      
      // Invoke the BPEL partner link.
      log.fine("Invoke EasyBPEL");
      log.fine("  message.qname=" + bpelMessage.getQName());
      log.fine("  message.content=" + bpelMessage.toString());
      EasyBpelContextImpl context = new EasyBpelContextImpl();
      context.easyBpelPartnerLinkInput = easyBpelPartnerLinkInput;
      easyBpelPartnerLinkInput.easyBpelProcess.getCore().getEngine().accept(bpelMessage, context);

      if(method.getAnnotation(Oneway.class) != null) {
    	// When oneway invocation return nothing immediately.
        return null;
      } else {
        // When twoway invocation then wait for a reply.
        Message replyBpelMsg = context.waitReply();
        log.fine("Output XML message " + replyBpelMsg.toString());

        // Unmarshall the output XML message.
        return unmarshallResult(method, args, replyBpelMsg.getBody().getPayload());
      }

    }
  }

}
