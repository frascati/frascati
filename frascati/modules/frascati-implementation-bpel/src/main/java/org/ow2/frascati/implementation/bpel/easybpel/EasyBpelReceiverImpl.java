/**
 * OW2 FraSCAti: SCA Implementation BPEL with EasyBPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import com.ebmwebsourcing.easyviper.core.api.CoreException;
import com.ebmwebsourcing.easyviper.core.api.env.ExternalContext;
import com.ebmwebsourcing.easyviper.core.api.env.Receiver;
import com.ebmwebsourcing.easyviper.core.api.soa.message.Message;
import com.ebmwebsourcing.easyviper.core.impl.env.AbstractReceiverImpl;

/**
 * @author Philippe Merle - INRIA
 */
@org.osoa.sca.annotations.Service(Receiver.class)
public class EasyBpelReceiverImpl
     extends AbstractReceiverImpl
  implements Receiver {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see Receiver#accept(ExternalMessage, ExternalContext)
   */
  public final void accept(final Message externalMessage, final ExternalContext context) throws CoreException
  {
    java.util.logging.Logger.getLogger(this.getClass().getCanonicalName()).warning("Throw new UnsupportedOperationException()");
    throw new UnsupportedOperationException();
  }

}
