/***
 * Julia: France Telecom's implementation of the Fractal API
 * Copyright (C) 2001-2002 France Telecom R&D
 * Copyright (C) 2010 INRIA, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal[at]objectweb.org
 *
 * Author: Eric Bruneton
 * Contributors: Romain Rouvoy, Philippe Merle
 */

package org.ow2.frascati.implementation.bpel.easybpel;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.objectweb.fractal.julia.Julia;
import org.objectweb.fractal.julia.factory.BasicGenericFactoryMixin;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Loader;
import org.objectweb.fractal.julia.type.BasicTypeFactoryMixin;

/**
 * This version of Julia uses the current thread class loader to search the Julia loader class.
 */
public class MyJulia extends Julia {

  /**
   * @see Julia#newFcInstance()
   */
  @Override
  public final Component newFcInstance () throws InstantiationException {
    return newFcInstance(new HashMap<String, Object>());
  }

  /**
   * @see Julia#newFcInstance (Type, Object, Object)
   */
  @Override
  @SuppressWarnings("unchecked")
  public final Component newFcInstance (
    final Type type, 
    final Object controllerDesc, 
    final Object contentDesc) throws InstantiationException 
  {
    Map<String, Object> context;
    if (contentDesc instanceof Map) {
      context = (Map<String, Object>)contentDesc;
    } else {
      context = new HashMap<String, Object>();
    }
    return newFcInstance(context);
  }

  protected final Component newFcInstance (final Map<String, Object> context) 
    throws InstantiationException 
  {
    Component bootstrapComponent = null;

    // The field 'bootstrapComponent' of class Julia is private
    // so we use Java reflection to get and set it.
    Field fieldJuliaBootstrapComponent = null;
    try {
      fieldJuliaBootstrapComponent = Julia.class.getDeclaredField("bootstrapComponent");
      fieldJuliaBootstrapComponent.setAccessible(true);
//      bootstrapComponent = (Component)fieldJuliaBootstrapComponent.get(null);
    } catch(Exception e) {
      InstantiationException instantiationException = new InstantiationException("");
      instantiationException.initCause(e);
      throw instantiationException;
    }

//    if (bootstrapComponent == null) {
      String boot = (String)context.get("julia.loader");
      if (boot == null) {
        boot = System.getProperty("julia.loader");
      } 
      if (boot == null) {
        boot = DEFAULT_LOADER;
      }

      // creates the pre bootstrap controller components
      Loader loader;
      try {
        loader = (Loader)Thread.currentThread().getContextClassLoader().loadClass(boot).newInstance();
        loader.init(context);
      } catch (Exception e) {
        // chain the exception thrown
        InstantiationException instantiationException = new InstantiationException(
          "Cannot find or instantiate the '" + boot +
          "' class specified in the julia.loader [system] property");
        instantiationException.initCause(e);
        throw instantiationException;
      }
      BasicTypeFactoryMixin typeFactory = new BasicTypeFactoryMixin();
      BasicGenericFactoryMixin genericFactory = new BasicGenericFactoryMixin();
      genericFactory._this_weaveableL = loader;
      genericFactory._this_weaveableTF = typeFactory;

      // use the pre bootstrap component to create the real bootstrap component
      ComponentType t = typeFactory.createFcType(new InterfaceType[0]);
      try {
        bootstrapComponent = genericFactory.newFcInstance(t, "bootstrap", null);
        try {
          ((Loader)bootstrapComponent.getFcInterface("loader")).init(context);
        } catch (NoSuchInterfaceException ignored) {
        }
      } catch (Exception e) {
        throw new ChainedInstantiationException(
          e, null, "Cannot create the bootstrap component");
      }

      try {
        fieldJuliaBootstrapComponent.set(null, bootstrapComponent);
      } catch(Exception e) {
        throw new Error(e);
      }
//    }

    return bootstrapComponent;
  }

}
