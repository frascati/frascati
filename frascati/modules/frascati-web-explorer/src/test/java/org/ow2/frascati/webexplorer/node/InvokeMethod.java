/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.node;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class InvokeMethod
{
    @BeforeClass
    public static void startTest()
    {
        System.out.println("-- Start Invoke Method Component Tests");
    }

    @Test
    public void testInvokeMethod() throws JSONException
    {
        JSONObject defaultDomain = NodeTestSuite.defaultDomain;
        JSONObject alarmComposite = WebExplorerAllTest.getItem(defaultDomain, "text", "alarm");
        JSONObject alarmControl = WebExplorerAllTest.getItem(alarmComposite, "text", "alarmControl");
        assertNotNull(alarmControl);
        JSONObject alarmControlInterface = WebExplorerAllTest.getItem(alarmControl, "type", "interface");
        assertNotNull(alarmControlInterface);
        JSONObject getStatusMethod = WebExplorerAllTest.getItem(alarmControlInterface, "value", "getStatus");
        assertNotNull(getStatusMethod);
        JSONObject onMethod = WebExplorerAllTest.getItem(alarmControlInterface, "value", "on");
        assertNotNull(onMethod);
        JSONObject offMethod = WebExplorerAllTest.getItem(alarmControlInterface, "value", "off");
        assertNotNull(offMethod);

        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        String methodId = getStatusMethod.getString("id");
        params.add("treeId", methodId);
        String methodReturn = WebExplorerAllTest.webExplorer.invokeMethod(params);
        assertEquals("false", methodReturn);

        params.clear();
        methodId = onMethod.getString("id");
        params.add("treeId", methodId);
        methodReturn = WebExplorerAllTest.webExplorer.invokeMethod(params);
        assertEquals("true", methodReturn);

        params.clear();
        methodId = getStatusMethod.getString("id");
        params.add("treeId", methodId);
        methodReturn = WebExplorerAllTest.webExplorer.invokeMethod(params);
        assertEquals("true", methodReturn);

        params.clear();
        methodId = offMethod.getString("id");
        params.add("treeId", methodId);
        methodReturn =WebExplorerAllTest. webExplorer.invokeMethod(params);
        assertEquals("true", methodReturn);

        params.clear();
        methodId = getStatusMethod.getString("id");
        params.add("treeId", methodId);
        methodReturn = WebExplorerAllTest.webExplorer.invokeMethod(params);
        assertEquals("false", methodReturn);
    }
}
