/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.node;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class SetProperty
{
    @BeforeClass
    public static void startTest()
    {
        System.out.println("-- Start Set Property Tests");
    }

    @Test
    public void testSetProperty() throws JSONException
    {
        JSONObject defaultDomain = NodeTestSuite.defaultDomain;
        JSONObject alarmComposite = WebExplorerAllTest.getItem(defaultDomain, "text", "alarm");
        JSONObject alarmComponent = WebExplorerAllTest.getItem(alarmComposite, "text", "alarm");
        assertNotNull(alarmComponent);
        JSONObject frameTitleProperty = WebExplorerAllTest.getItem(alarmComponent, "text", "frameTitle");
        assertNotNull(frameTitleProperty);
        String frameTitlePropertyId = frameTitleProperty.getString("id");
        String isSet = WebExplorerAllTest.webExplorer.setProperty(frameTitlePropertyId, "testValue");
        assertEquals(isSet, "true");
        String frameTitlePropertyString = WebExplorerAllTest.webExplorer.refreshTreeNode(frameTitlePropertyId);
        frameTitleProperty = new JSONObject(frameTitlePropertyString);
        String frameTitlePropertyValue = frameTitleProperty.getString("value");
        assertEquals(frameTitlePropertyValue, "testValue");
    }
}
