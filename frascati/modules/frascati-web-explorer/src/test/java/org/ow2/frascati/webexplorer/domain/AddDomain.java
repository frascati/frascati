/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.WebApplicationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class AddDomain
{
    private static JSONObject defaultDomain;
    
    private JSONObject testDomain;
    
    @BeforeClass
    public static void startTest() throws JSONException
    {
        System.out.println("-- Start Add Domain Tests");
        String treeString=WebExplorerAllTest.webExplorer.getTree();
        JSONObject tree=new JSONObject(treeString);
        JSONObject rootDomain=WebExplorerAllTest.getItem(tree,"text","Root");
        JSONArray domains=rootDomain.getJSONArray("item");
        AddDomain.defaultDomain=domains.getJSONObject(0);
    }
    
    @Test
    public void addDomainTest() throws JSONException
    {
        
        String defaultDomainURI=defaultDomain.getString("domainURI");
        String testDomainString=WebExplorerAllTest.webExplorer.addDomain("test", defaultDomainURI, "FF0000");
        this.testDomain=new JSONObject(testDomainString);
        assertNotNull(testDomain);
        String treeString=WebExplorerAllTest.webExplorer.getTree();
        JSONObject tree=new JSONObject(treeString);
        JSONObject rootDomain=WebExplorerAllTest.getItem(tree,"text","Root");
        JSONArray domains=rootDomain.getJSONArray("item");
        assertEquals(domains.length(),2);
    }
    
    @Test(expected=WebApplicationException.class)
    public void wrongURITest()
    {
       WebExplorerAllTest.webExplorer.addDomain("test", "wrongURI", "FF0000");
    }
    
    @After
    public void removeDomain() throws JSONException
    {
        try
        {
            String testDomainId=testDomain.getString("id");
            String treeString=WebExplorerAllTest.webExplorer.removeDomain(testDomainId);
            JSONObject tree=new JSONObject(treeString);
            JSONObject rootDomain=WebExplorerAllTest.getItem(tree,"text","Root");
            JSONArray domains=rootDomain.getJSONArray("item");
            assertEquals(domains.length(),1);

        }
        catch(Exception exception){}
    }
    
}
