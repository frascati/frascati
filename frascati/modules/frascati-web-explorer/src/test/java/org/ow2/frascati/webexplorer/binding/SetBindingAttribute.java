/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.jdom.JDOMException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class SetBindingAttribute
{
    @BeforeClass
    public static void startTest() throws JSONException, JDOMException, IOException
    {
        System.out.println("-- Start Set Binding Attribute Tests");
        String runServiceId = BindingTestSuite.runService.getString("id");
        String runServiceString = WebExplorerAllTest.webExplorer.refreshTreeNode(runServiceId);
        JSONObject runService = new JSONObject(runServiceString);
        JSONObject binding = WebExplorerAllTest.getItem(runService, "text", "WS");
        if(binding==null)
        {
            MultivaluedMap<String, String> params = new MetadataMap<String, String>();
            runServiceId = BindingTestSuite.runService.getString("id");
            params.add("treeId", runServiceId);
            params.add("kind", "WS");
            params.add("uri", "http://localhost:9100/r");
            WebExplorerAllTest.webExplorer.addBinding(params);
        }
    }

    @Test
    public void testSetBindingAttribute() throws JSONException
    {
      String runServiceId = BindingTestSuite.runService.getString("id");
      String  runServiceString=WebExplorerAllTest.webExplorer.setBindingAttribute(runServiceId, "0", "uri","http://localhost:9100/run");
      JSONObject runService=new JSONObject(runServiceString);
      assertNotNull(runService);
      JSONObject binding=WebExplorerAllTest.getItem(runService, "text", "WS");
      assertNotNull(binding);
      JSONObject uriAttribute=WebExplorerAllTest.getObject(binding, "attributes", "name", "uri");
      assertNotNull(uriAttribute);
      String uriAttributeValue=uriAttribute.getString("value");
      assertEquals(uriAttributeValue, "http://localhost:9100/run");
    }
    
    @Test(expected=WebApplicationException.class)
    public void wrongAttributeName() throws JSONException
    {
        String runServiceId = BindingTestSuite.runService.getString("id");
        WebExplorerAllTest.webExplorer.setBindingAttribute(runServiceId, "0", "BadAttribute","BadValue");
    }
    
    @Test(expected=WebApplicationException.class)
    public void noTreeIdTest()
    {
        WebExplorerAllTest.webExplorer.setBindingAttribute("BADID", "0", "BadAttribute","BadValue");
    }
    
    @AfterClass
    public static void removeBinding() throws JSONException
    {
        System.out.println("-- Start Set Binding Attribute Tests");
        String runServiceId = BindingTestSuite.runService.getString("id");
        String runServiceString = WebExplorerAllTest.webExplorer.refreshTreeNode(runServiceId);
        JSONObject runService = new JSONObject(runServiceString);
        JSONObject binding = WebExplorerAllTest.getItem(runService, "text", "WS");
        if(binding!=null)
        {
            WebExplorerAllTest.webExplorer.removeBinding(runServiceId, "0");
        }
    }
}
