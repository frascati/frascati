/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.jdom.JDOMException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class AddBinding
{
    @BeforeClass
    public static void startTest() throws JSONException, JDOMException, IOException
    {
        System.out.println("-- Start Add Binding Tests");
    }

    @Test
    public void testAddBinding() throws JSONException
    {
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        String runServiceId = BindingTestSuite.runService.getString("id");
        params.add("treeId", runServiceId);
        params.add("kind", "WS");
        params.add("uri", "http://localhost:9100/r");
        String runServiceString = WebExplorerAllTest.webExplorer.addBinding(params);
        JSONObject runService = new JSONObject(runServiceString);
        assertNotNull(runService);
        JSONObject binding = WebExplorerAllTest.getItem(runService, "text", "WS");
        assertNotNull(binding);
        JSONObject uriAttribute = WebExplorerAllTest.getObject(binding, "attributes", "name", "uri");
        assertNotNull(uriAttribute);
        String uriAttributeValue = uriAttribute.getString("value");
        assertEquals(uriAttributeValue, "http://localhost:9100/r");
    }

    @Test(expected=WebApplicationException.class)
    public void noTreeIdTest()
    {
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        params.add("kind", "WS");
        params.add("uri", "http://localhost:9100/r");
        WebExplorerAllTest.webExplorer.addBinding(params);
    }
    
    @Test(expected=WebApplicationException.class)
    public void badBindingKindTest() throws JSONException
    {
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        String runServiceId = BindingTestSuite.runService.getString("id");
        params.add("treeId", runServiceId);
        params.add("kind", "BADKIND");
        params.add("uri", "http://localhost:9100/r");
        WebExplorerAllTest.webExplorer.addBinding(params);
    }
    
    @Test
    public void missingURITest() throws JSONException
    {
        System.out.println();
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        String runServiceId = BindingTestSuite.runService.getString("id");
        params.add("treeId", runServiceId);
        params.add("kind", "WS");
        String runServiceString = WebExplorerAllTest.webExplorer.addBinding(params);
        JSONObject runService = new JSONObject(runServiceString);
        assertNotNull(runService);
        JSONObject binding = WebExplorerAllTest.getItem(runService, "text", "WS");
        assertNotNull(binding);
        JSONObject uriAttribute = WebExplorerAllTest.getObject(binding, "attributes", "name", "uri");
        assertNotNull(uriAttribute);
        String uriAttributeValue = uriAttribute.getString("value");
        /*default value of URI if parameter is not found*/
        assertEquals(uriAttributeValue, "http://localhost:8080/Service");
    }
    
    @After
    public void removeBinding() throws JSONException
    {
        String runServiceId = BindingTestSuite.runService.getString("id");
        try
        {
            WebExplorerAllTest.webExplorer.removeBinding(runServiceId, "0");
        }
        catch(WebApplicationException webApplicationException)
        {
            System.out.println(webApplicationException.getMessage());
        }
        finally
        {
            String runServiceString= WebExplorerAllTest.webExplorer.refreshTreeNode(runServiceId);
            System.out.println(runServiceString);
            JSONObject runService = new JSONObject(runServiceString);
            JSONObject binding = WebExplorerAllTest.getItem(runService, "text", "WS");
            assertNull(binding);
        }
    }
}
