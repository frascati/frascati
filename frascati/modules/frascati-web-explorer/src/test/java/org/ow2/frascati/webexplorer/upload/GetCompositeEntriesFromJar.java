/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.upload;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
public class GetCompositeEntriesFromJar
{
    private static JSONObject defaultDomain;

    @BeforeClass
    public static void startTest() throws JSONException
    {
        System.out.println("-- Start GetCompositeEntriesFromJar Tests");
        String treeString = WebExplorerAllTest.webExplorer.getTree();
        JSONObject tree = new JSONObject(treeString);
        JSONObject rootDomain = WebExplorerAllTest.getItem(tree, "text", "Root");
        JSONArray domains = rootDomain.getJSONArray("item");
        GetCompositeEntriesFromJar.defaultDomain = domains.getJSONObject(0);
    }

    @Test
    public void testDeployComposite() throws URISyntaxException, IOException, JSONException
    {
        URL url = this.getClass().getClassLoader().getResource("helloworld-pojo-1.5-SNAPSHOT.jar");
        File compositeFile = new File(url.toURI());
        String defaultDomainId = defaultDomain.getString("id");
        String encodedComposite = FileUtil.getStringFromFile(compositeFile);
        String jarEntries = WebExplorerAllTest.uploader.getCompositeEntriesFromJar(defaultDomainId, encodedComposite);
        assertEquals(jarEntries, "helloworld-pojo");
    }
}
