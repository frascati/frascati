/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.julia.Julia;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.webexplorer.binding.BindingTestSuite;
import org.ow2.frascati.webexplorer.domain.DomainTestSuite;
import org.ow2.frascati.webexplorer.node.NodeTestSuite;
import org.ow2.frascati.webexplorer.service.WebExplorerItf;
import org.ow2.frascati.webexplorer.service.upload.UploadServiceItf;
import org.ow2.frascati.webexplorer.upload.UploadTestSuite;

import static org.junit.Assert.assertNotNull;
/**
 *
 */
@RunWith(Suite.class)
//@SuiteClasses({DomainTestSuite.class,UploadTestSuite.class,NodeTestSuite.class,BindingTestSuite.class})
@SuiteClasses({DomainTestSuite.class})
public class WebExplorerAllTest
{      
    public static WebExplorerItf webExplorer;
    public static UploadServiceItf uploader;
    
    /**
     * Create a new FraSCAti platform and initialize the domain
     * 
     * @throws FrascatiException
     */
    @BeforeClass
    public static void init() throws FrascatiException
    {
        System.setProperty("fractal.provider", Julia.class.getCanonicalName());
        System.setProperty("org.ow2.frascati.binding.uri.base", "http://localhost:9875");
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiWebExplorer");
        FraSCAti frascati=FraSCAti.newFraSCAti();
        Component frascatiComponent=frascati.getComposite("org.ow2.frascati.FraSCAti");
        webExplorer=frascati.getService(frascatiComponent, "webexplorer", WebExplorerItf.class);
        assertNotNull(webExplorer);
        uploader=frascati.getService(frascatiComponent, "webexplorerUpload", UploadServiceItf.class);
        assertNotNull(uploader);
    }
    
    public static JSONObject getItem(JSONObject parent, String searchedKey, Object searchValue) throws JSONException
    {
        JSONArray items=parent.getJSONArray("item");
        JSONObject tmpItem,item=null;
        Object tmpValue;
        if(items!=null)
        {
            for(int i=0; i<items.length();i++)
            {
                tmpItem=items.getJSONObject(i);
                tmpValue=tmpItem.get(searchedKey);
                if(searchValue.equals(tmpValue))
                {
                    item=tmpItem;
                }
            }
        }
        return item;
    }
    
    public static JSONObject getObject(JSONObject parent,String arrayKey, String searchedKey, Object searchValue) throws JSONException
    {
        JSONArray objects=parent.getJSONArray(arrayKey);
        JSONObject tmpObject;
        String tmpValue;
        if(objects!=null)
        {
            for(int i=0;i<objects.length();i++)
            {
                tmpObject=objects.getJSONObject(i);
                tmpValue=tmpObject.getString(searchedKey);
                if(searchValue.equals(tmpValue))
                {
                    return tmpObject;
                }
            }
        }
        return null;
    }
}
