/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.node;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
@RunWith(Suite.class)
@SuiteClasses({RefreshTreeNode.class,StartComponent.class,StopComponent.class,InvokeMethod.class, SetProperty.class})
public class NodeTestSuite
{
    public static JSONObject defaultDomain;
    
    private static JSONObject alarmComposite;
    
    @BeforeClass
    public static void deployComposite() throws JSONException, IOException
    {
        System.out.println("- Node TestSuite");
        System.out.println("--- Deploy alarm composite");
        String treeString = WebExplorerAllTest.webExplorer.getTree();
        JSONObject tree = new JSONObject(treeString);
        JSONObject rootDomain = WebExplorerAllTest.getItem(tree, "text", "Root");
        JSONArray domains = rootDomain.getJSONArray("item");
        defaultDomain = domains.getJSONObject(0);
        /*Use hard coded path because of the static context*/
        File contributionFile=new File("src/test/resources/alarm.zip");
        String encodedContribution=FileUtil.getStringFromFile(contributionFile);
        String defaultDomainId=defaultDomain.getString("id");        
        String domainString=WebExplorerAllTest.uploader.deployContribution(defaultDomainId, encodedContribution);
        JSONObject domain=new JSONObject(domainString);
        NodeTestSuite.alarmComposite=WebExplorerAllTest.getItem(domain, "text", "alarm");
        assertNotNull(alarmComposite);
    }
    
    @AfterClass
    public static void undeployComposite() throws JSONException
    {
        System.out.println("--- Undeploy alarm composite");
        String alarmCompositeId=alarmComposite.getString("id");
        String domainString=WebExplorerAllTest.uploader.undeployComposite(alarmCompositeId);
        JSONObject domain=new JSONObject(domainString);
        JSONObject alarmComposite=WebExplorerAllTest.getItem(domain, "text", "alarm");
        assertNull(alarmComposite);
    }
    
}
