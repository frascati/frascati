/**
 * OW2 FraSCAti Web Explorer
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.webexplorer.WebExplorerAllTest;

/**
 *
 */
@RunWith(Suite.class)
@SuiteClasses({AddBinding.class,RemoveBinding.class,SetBindingAttribute.class})
public class BindingTestSuite
{
    private static JSONObject helloComposite;
    
    public static JSONObject runService;
    
    @BeforeClass
    public static void deployComposite() throws JSONException, IOException
    {
        System.out.println("- Binding TestSuite");
        System.out.println("--- Deploy hello-world composite");
        String treeString = WebExplorerAllTest.webExplorer.getTree();
        JSONObject tree = new JSONObject(treeString);
        JSONObject rootDomain = WebExplorerAllTest.getItem(tree, "text", "Root");
        JSONArray domains = rootDomain.getJSONArray("item");
        JSONObject defaultDomain = domains.getJSONObject(0);
        String defaultDomainId=defaultDomain.getString("id");
        File compositeFile = new File("src/test/resources/helloworld-pojo-1.5-SNAPSHOT.jar");
        String encodedComposite = FileUtil.getStringFromFile(compositeFile);
        String jarEntries = WebExplorerAllTest.uploader.getCompositeEntriesFromJar(defaultDomainId, encodedComposite);
        assertEquals(jarEntries, "helloworld-pojo");
        String domainString = WebExplorerAllTest.uploader.deployComposite(defaultDomainId, jarEntries, encodedComposite);
        defaultDomain = new JSONObject(domainString);
        helloComposite = WebExplorerAllTest.getItem(defaultDomain, "text", "helloworld-pojo");
        assertNotNull(helloComposite);
        runService=WebExplorerAllTest.getItem(helloComposite, "text", "r");
        assertNotNull(runService);
    }
    
    @AfterClass
    public static void undeployComposite() throws JSONException
    {
        String alarmCompositeId = helloComposite.getString("id");
        String domainString = WebExplorerAllTest.uploader.undeployComposite(alarmCompositeId);
        JSONObject domain = new JSONObject(domainString);
        JSONObject alarmComposite = WebExplorerAllTest.getItem(domain, "text", "helloworld-pojo");
        assertNull(alarmComposite);
    }
}
