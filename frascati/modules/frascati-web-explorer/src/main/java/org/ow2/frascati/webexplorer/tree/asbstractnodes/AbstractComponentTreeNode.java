/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.asbstractnodes;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;
import org.ow2.frascati.webexplorer.tree.nodes.ComponentTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.PropertyTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ReferenceTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ServiceTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;

/**
 *
 */
public abstract class AbstractComponentTreeNode extends AbstractIndexableTreeNode
{
    private static final String STATUS_STARTED="STARTED";
    private static final String STATUS_STOPPED="STOPPED";
    
    protected String status;

    protected List<String> components;
    protected List<String> services;
    protected List<String> references;
    protected List<String> properties;
    
    public AbstractComponentTreeNode(AbstractTreeNode parentNode, String image, String type, int positionComponent, Component component)
    {
        super(parentNode, component.getName(), image, type, positionComponent);

        this.components=new ArrayList<String>();
        this.services=new ArrayList<String>();
        this.references=new ArrayList<String>();
        this.properties=new ArrayList<String>();
        this.status=component.getStatus().toString();

        int position;

        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain=root.getDomainByURI(this.getDomainURI());
        RemoteScaDomain introspection=domain.getintrospectionClient();
        
        position=0;
        for(Component subComponent : component.getComponents())
        {
            ComponentTreeNode componentNode = new ComponentTreeNode(this, position, introspection.getComponent(this.getFrascatiPath()+'/'+subComponent.getName()));
            position++;
            this.addTreeNode(componentNode);
            this.components.add(componentNode.getId());
        }

        position=0;
        for(Port service : component.getServices())
        {
            ServiceTreeNode serviceNode=new ServiceTreeNode(this, position, service);
            position++;
            this.addTreeNode(serviceNode);
            this.services.add(serviceNode.getId());
        }
        
        position=0;
        for(Port reference : component.getReferences())
        {
            ReferenceTreeNode referenceNode=new ReferenceTreeNode(this, position, reference);
            position++;
            this.addTreeNode(referenceNode);
            this.references.add(referenceNode.getId());
        }
        
        position=0;
        for(Property property : component.getProperties())
        {
        	PropertyTreeNode propertyNode=new PropertyTreeNode(this, position, property);
            position++;
            this.addTreeNode(propertyNode);
            this.properties.add(propertyNode.getId());
        }
    }
    
    private boolean startStop(String method)
    {
        try
        {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomainByURI(this.getDomainURI());
            RemoteScaDomain introspection=domain.getintrospectionClient();
            if(method.equals("start"))
            {
                introspection.startComponent(this.getFrascatiPath());
            }
            else if(method.equals("stop"))
            {
                introspection.stopComponent(this.getFrascatiPath());
            }
            else
            {
                return false;
            }
            return true;
        }
        catch(Exception exception)
        {
            return false;
        }
    }
    
    public boolean start()
    {
        boolean isStarted=startStop("start");
        if(isStarted)
        {
            this.status=STATUS_STARTED;
        }
        return isStarted;
    }
    
    public boolean stop()
    {
        boolean isStopped=startStop("stop");
        if(isStopped)
        {
            this.status=STATUS_STOPPED;
        }
        return isStopped;
    }
    
    @Override
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject=super.toJSON();
            for(String componentId : components)
            {
                jsonObject.append("components", componentId);
            }
            for(String serviceId : services)
            {
                jsonObject.append("services", serviceId);
            }
            for(String referenceId : references)
            {
                jsonObject.append("references", referenceId);
            }
            for(String propertyId : properties)
            {
                jsonObject.append("properties", propertyId);
            }
            return jsonObject;
        }
        catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500,"Cannot marshall "+this.getText()+" node to JSON Object");
        }
    }
    
    public String getStatus()
    {
        return this.status;
    }
}
