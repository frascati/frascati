/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.nodes;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Interface;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;

/**
 *
 */
public class InterfaceTreeNode extends AbstractTreeNode
{
    public final static String INTERFACE_TYPE="interface";
    public final static String INTERFACE_IMG="icone_i.png";
    
    private List<String> methods;
    
    public InterfaceTreeNode(AbstractTreeNode parentNode, Interface itf)
    {
        super(parentNode, itf.getClazz(), INTERFACE_IMG, INTERFACE_TYPE, null);

        this.methods=new ArrayList<String>();
        for(Method method : itf.getMethods())
        {
            MethodTreeNode methodNode = new MethodTreeNode(this, method);
            this.addTreeNode(methodNode);
            this.methods.add(methodNode.getId());
        }
    }
    
    @Override
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject=super.toJSON();
            for(String bindingId : methods)
            {
                jsonObject.append("methods", bindingId);
            }
            return jsonObject;   
        }
        catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500,"Cannot marshall "+this.getText()+" node to JSON Object");
        }
    }
}
