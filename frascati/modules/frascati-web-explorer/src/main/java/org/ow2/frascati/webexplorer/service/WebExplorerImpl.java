/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012-2013 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.webexplorer.service;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONObject;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.webexplorer.tree.AbstractNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractComponentTreeNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractServiceReferenceTreeNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ComponentTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.CompositeTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.MethodTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.PropertyTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ReferenceTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ServiceTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;
import org.ow2.frascati.webexplorer.tree.utils.TreeUtils;

/**
 * FraSCAti Web Explorer.
 */
@Scope("COMPOSITE")
public class WebExplorerImpl implements WebExplorerItf
{
    public String getTree()
    {
        RootTreeNode root = RootTreeNode.getInstance();
        root.refreshNode();
        JSONObject object = root.toJSON();
        return object.toString();
    }

    public String getDomain(String domainId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain = root.getDomain(domainId);
        JSONObject domainObject = domain.toJSON();
        return domainObject.toString();
    }

    public String addDomain(String name, String domainURI, String color)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain = new DomainTreeNode(root, name, domainURI, color);
        root.addDomain(domain);
        try {
          domain.loadIt();
        } catch(MyWebApplicationException exc) {
          root.removeDomain(domain.getId());
          throw exc;
        }
        JSONObject object = domain.toJSON();
        return object.toString();
    }

    public String removeDomain(String domainId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        root.removeDomain(domainId);
        JSONObject object = root.toJSON();
        return object.toString();
    }

    public String refreshTreeNode(String treeId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractNode node = root.getNode(treeId);
        node = node.refreshNode();
        JSONObject object = node.toJSON();
        return object.toString();
    }

    public String startComponent(String treeId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() == ComponentTreeNode.COMPONENT_TYPE || node.getType() == CompositeTreeNode.COMPOSITE_TYPE)
        {
            AbstractComponentTreeNode abstractComponentTreeNode = (AbstractComponentTreeNode) node;
            abstractComponentTreeNode.start();
            AbstractNode abstractNode = abstractComponentTreeNode.refreshNode();
            JSONObject object = abstractNode.toJSON();
            return object.toString();
        }
        throw new MyWebApplicationException("startComponent method can't be apply on a " + node.getType() + " node");
    }

    public String stopComponent(String treeId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() == ComponentTreeNode.COMPONENT_TYPE || node.getType() == CompositeTreeNode.COMPOSITE_TYPE)
        {
            AbstractComponentTreeNode abstractComponentTreeNode = (AbstractComponentTreeNode) node;
            abstractComponentTreeNode.stop();
            AbstractNode abstractNode = abstractComponentTreeNode.refreshNode();
            JSONObject object = abstractNode.toJSON();
            return object.toString();
        }
        throw new MyWebApplicationException("stopComponent method can't be invoke on a " + node.getType() + " node");
    }

    public String invokeMethod(MultivaluedMap<String, String> params)
    {
        String treeId = params.getFirst("treeId");
        if (treeId == null)
        {
            throw new MyWebApplicationException("treeId params is compulsory");
        }

        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() != MethodTreeNode.METHOD_TYPE)
        {
            throw new MyWebApplicationException("invokeMethod method can't be apply on a " + node.getType() + " node");
        }

        MethodTreeNode methodTreeNode = (MethodTreeNode) node;
        String nodeId = TreeUtils.getParentId(methodTreeNode.getId());
        node = root.getNode(nodeId);
        nodeId = TreeUtils.getParentId(node.getId());
        node = root.getNode(nodeId);
        AbstractServiceReferenceTreeNode abstractServiceReferenceTreeNode = (AbstractServiceReferenceTreeNode) node;
        return methodTreeNode.invoke(abstractServiceReferenceTreeNode, params);
    }

    public String setProperty(String treeId, String value)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() != PropertyTreeNode.PROPERTY_TYPE)
        {
            throw new MyWebApplicationException("setProperty method can't be apply on a " + node.getType() + " node");
        }
        PropertyTreeNode propertyTreeNode = (PropertyTreeNode) node;
        Boolean isSet = propertyTreeNode.setPropertyValue(value);
        return isSet.toString();
    }

    public String addBinding(MultivaluedMap<String, String> params)
    {
        String treeId = params.getFirst("treeId");
        if (treeId == null)
        {
            throw new MyWebApplicationException("treeId params is compulsory");
        }

        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() != ServiceTreeNode.SERVICE_TYPE && node.getType() != ReferenceTreeNode.REFERENCE_TYPE)
        {
            throw new MyWebApplicationException("addBinding method can't be apply on a " + node.getType() + " node");
        }
        AbstractServiceReferenceTreeNode abstractServiceReferenceTreeNode = (AbstractServiceReferenceTreeNode) node;
        abstractServiceReferenceTreeNode.addBinding(params);
        AbstractNode refreshedNode = abstractServiceReferenceTreeNode.refreshNode();
        JSONObject object = refreshedNode.toJSON();
        return object.toString();
    }

    public String removeBinding(String treeId, String position)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() != ServiceTreeNode.SERVICE_TYPE && node.getType() != ReferenceTreeNode.REFERENCE_TYPE)
        {
            throw new MyWebApplicationException("removeBinding method can't be apply on a " + node.getType() + " node");
        }

        AbstractServiceReferenceTreeNode abstractServiceReferenceTreeNode = (AbstractServiceReferenceTreeNode) node;
        abstractServiceReferenceTreeNode.removeBinding(position);
        AbstractNode refreshedNode = abstractServiceReferenceTreeNode.refreshNode();
        JSONObject object = refreshedNode.toJSON();
        return object.toString();
    }

    public String setBindingAttribute(String treeId, String position, String attribute, String newValue)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node = root.getNode(treeId);
        if (node.getType() != ServiceTreeNode.SERVICE_TYPE && node.getType() != ReferenceTreeNode.REFERENCE_TYPE)
        {
            throw new MyWebApplicationException("setBindingAttribute method can't be apply on a " + node.getType() + " node");
        }
        AbstractServiceReferenceTreeNode abstractServiceReferenceTreeNode = (AbstractServiceReferenceTreeNode) node;
        abstractServiceReferenceTreeNode.setBindingAttribute(position, attribute, newValue);
        AbstractNode refreshedNode = abstractServiceReferenceTreeNode.refreshNode();
        JSONObject object = refreshedNode.toJSON();
        return object.toString();

    }
}
