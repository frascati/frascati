/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.nodes;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.remote.introspection.resources.Parameter;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractServiceReferenceTreeNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;

/**
 *
 */
public class MethodTreeNode extends AbstractTreeNode
{
    public final static String METHOD_TYPE="method";
    public final static String METHOD_IMG="icone_m.png";
    
    private String signature;
    private String value;
    private List<String> parameters;
    
    public MethodTreeNode(AbstractTreeNode parentNode, Method method)
    {
        super(parentNode, method.getName(), METHOD_IMG, METHOD_TYPE, null);

        this.parameters=new ArrayList<String>();
        this.value=method.getName();
        StringBuilder sb = new StringBuilder(method.getName());
        char c = '(';
        for(Parameter parameter : method.getParameters())
        {
            String parameterType = parameter.getType();
            this.parameters.add(parameterType);
            parameterType=formatParameterType(parameterType);
            sb.append(c);
            sb.append(parameterType);
            c = ',';
        }
        sb.append(")");
        this.signature=sb.toString();
    }
    
    public String invoke(AbstractServiceReferenceTreeNode abstractServiceReferenceTreeNode, MultivaluedMap<String, String> params)
    {
        if(params==null) params=new MetadataMap<String, String>();
        params.add("methodName",this.value);
        try
        {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomainByURI(this.getDomainURI());
            RemoteScaDomain introspection=domain.getintrospectionClient();
            String result=introspection.invokeMethod(abstractServiceReferenceTreeNode.getFrascatiPath(), params);
            return result;
        }
        catch(Exception exception)
        {
            return "";
        }
    }
    
    private String formatParameterType(String type)
    {
        int index=type.lastIndexOf(".");
        if(index==-1)   return type;
        else                    return type.substring(index+1);
    }
    
    public String getText()
    {
        return signature;
    }

    public String getSignature()
    {
        return signature;
    }

    public String getValue()
    {
        return value;
    }
    
    @Override
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject=super.toJSON();
            for(String parameter : parameters)
            {
                jsonObject.append("parameters", parameter);
            }
            return jsonObject;
        }
        catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500,"Cannot marshall "+this.getText()+" node to JSON Object");
        }
        
    }
}
