/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.nodes.binding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Attribute;
import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;

/**
 *
 */
public class BindingTreeNode extends AbstractTreeNode
{
    public final static String BINDING_TYPE="binding";
    public final static String BINDING_IMG="scaBindingDefault.png";
    
    public String kind;
    private List<BindingAttribute> attributes;
    
    public BindingTreeNode(AbstractTreeNode parentNode, Binding binding)
    {
        super(parentNode, binding.getKind().toString(), BINDING_IMG, BINDING_TYPE, null);

        this.kind=binding.getKind().toString();
        String img;
        if(kind.equalsIgnoreCase("REST"))               img="scaBindingREST.png";
        else if(kind.equalsIgnoreCase("WS"))            img="scaBindingWS.png";
        else if(kind.equalsIgnoreCase("JSON_RPC"))      img="scaJsonRpcBinding.png";
        else if(kind.equalsIgnoreCase("RMI"))           img="scaRmiBinding.png";
        else                                            img="scaBindingDefault.png";
        this.setImage(img);

        this.attributes=new ArrayList<BindingAttribute>();
        for(Attribute attribute : binding.getAttributes())
        {
        	try {
              BindingAttribute bindingAttribute = new BindingAttribute(attribute.getName(), attribute.getType(), attribute.getValue());
              this.attributes.add(bindingAttribute);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    
    public String getKind()
    {
        return this.kind;
    }
    
    @Override
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject=super.toJSON();
            for(BindingAttribute attribute : this.attributes)
            {
                jsonObject.append("attributes", attribute);
            }
            return jsonObject;
        }
        catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500,"Cannot marshall "+this.getText()+" node to JSON Object");
        }
    }
}
