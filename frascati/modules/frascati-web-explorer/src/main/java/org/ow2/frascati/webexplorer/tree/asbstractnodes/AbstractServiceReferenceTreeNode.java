/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.asbstractnodes;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.webexplorer.tree.nodes.InterfaceTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.binding.BindingTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;

/**
 *
 */
public abstract class AbstractServiceReferenceTreeNode extends AbstractIndexableTreeNode
{
    private String interfaceId;
    private List<String> bindings;

    public AbstractServiceReferenceTreeNode(AbstractTreeNode parentNode, String image, String type, int position, Port port)
    {
        super(parentNode, port.getName(), image, type, position);

        InterfaceTreeNode interfaceNode = new InterfaceTreeNode(this, port.getImplementedInterface());
        this.addTreeNode(interfaceNode);
        this.interfaceId = interfaceNode.getId();

        this.bindings = new ArrayList<String>();
        for (Binding binding : port.getBindings())
        {
        	BindingTreeNode bindingNode = new BindingTreeNode(this, binding);
            this.addTreeNode(bindingNode);
            this.bindings.add(bindingNode.getId());
        }
    }

    @Override
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject = super.toJSON();
            for (String bindingId : bindings)
            {
                jsonObject.append("bindings", bindingId);
            }
            return jsonObject;
        } catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500, "Cannot marshall " + this.getText() + " node to JSON Object");
        }
    }

    public String getInterfaceId()
    {
        return interfaceId;
    }

    /**
     * @param params
     * @return
     */
    public void addBinding(MultivaluedMap<String, String> params)
    {
        System.out.println("add binding" + params);
        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain = root.getDomainByURI(this.getDomainURI());
        RemoteScaDomain introspection = domain.getintrospectionClient();
        introspection.addBinding(this.getFrascatiPath(), params);
    }

    /**
     * @param position
     * @return
     */
    public void removeBinding(String position)
    {
        System.out.println("Remove binding of " + this.getFrascatiPath() + " at position : " + position);
        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain = root.getDomainByURI(this.getDomainURI());
        RemoteScaDomain introspection = domain.getintrospectionClient();
        introspection.removeBinding(this.getFrascatiPath(), position);
    }

    /**
     * @param position
     * @param attribute
     * @param newValue
     * @return
     */
    public void setBindingAttribute(String position, String attribute, String newValue)
    {

        RootTreeNode root = RootTreeNode.getInstance();
        DomainTreeNode domain = root.getDomainByURI(this.getDomainURI());
        RemoteScaDomain introspection = domain.getintrospectionClient();
        introspection.setBindingAttribute(this.getFrascatiPath(), position, attribute, newValue);
    }
}
