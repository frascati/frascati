/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.service.upload;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;

/**
 *
 */
@Service
public interface UploadServiceItf
{
    @POST
    @Path("/getCompositesName")
    @Produces({"text/plain"})
    public String getCompositeEntriesFromJar(@FormParam("domainId") String domainId,@FormParam("jar") String encodedComposite);
    
    @POST
    @Path("/contribution")
    @Produces("application/json")
    public String deployContribution(@FormParam("domainId") String domainId, @FormParam("contribution") String encodedContribution);
    
    @POST
    @Path("/composite")
    @Produces("application/json")
    public String deployComposite(@FormParam("domainId") String domainId, @FormParam("compositeName") String compositeName, @FormParam("jar") String encodedComposite);
    
    @DELETE
    @Path("/composite")
    @Produces("application/json")
    public String undeployComposite(@FormParam("treeId") String treeId) ;
}
