/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012-2013 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.webexplorer.tree.nodes.domain;

import java.util.List;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.webexplorer.tree.AbstractNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.CompositeTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;

/**
 *
 */
public class DomainTreeNode extends AbstractTreeNode
{
    public final static String DOMAIN_TYPE="domain";
    public final static String DOMAIN_IMG="domain.png";

    private Deployment deployment;
    private RemoteScaDomain introspection;
    
    private String domainName;
    private String color;
    
    public DomainTreeNode(RootTreeNode parentNode, String text, String domainURI, String color, Deployment deployment, RemoteScaDomain introspection)
    {
        super(parentNode, text, DOMAIN_IMG, DOMAIN_TYPE,domainURI);
        this.domainName=text;
        this.color=color;
        this.deployment=deployment;
        this.introspection=introspection;
    }

    public DomainTreeNode(RootTreeNode parentNode, String text, String domainURI, String color)
    {
        this(parentNode, text, domainURI, color,
          JAXRSClientFactory.create(domainURI+"/deploy", Deployment.class),
          JAXRSClientFactory.create(domainURI+"/introspection", RemoteScaDomain.class)
        );
    }

    public String getText()
    {
        return this.domainName+" ("+this.getDomainURI()+")";
    }
    
    public String getFrascatiPath()
    {
        return "";
    }
    
    public String getColor()
    {
        return color;
    }

    /*Lower case to avoid JSON marshalling*/
    public Deployment getdeploymentClient()
    {
        return deployment;
    }

    /*Lower case to avoid JSON marshalling*/
    public RemoteScaDomain getintrospectionClient()
    {
        return introspection;
    }

    public void loadIt() 
    {
    	try {
          this.item.clear();
          int position=0;
          for(Component composite : this.introspection.getDomainComposites()) {
            CompositeTreeNode compositeNode = new CompositeTreeNode(this, position, composite);
            this.addTreeNode(compositeNode);          
            position++;
    	  }
    	} catch (Exception exc) {
          throw new MyWebApplicationException("Cannot loadIt()");
    	}
    }
}
