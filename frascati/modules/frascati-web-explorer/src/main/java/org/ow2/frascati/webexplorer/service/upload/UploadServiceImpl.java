/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012-2013 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.webexplorer.service.upload;

import org.json.JSONObject;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.ComponentTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.CompositeTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;

/**
 *
 */
@Scope("COMPOSITE")
public class UploadServiceImpl implements UploadServiceItf
{
    public String getCompositeEntriesFromJar(String domainId,String encodedComposite)
    {
        try
        {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomain(domainId);
            Deployment deployment=domain.getdeploymentClient();
            String entries=deployment.getCompositeEntriesFromJar(encodedComposite);
            return entries;
        }
        catch(Exception exception)
        {
            return "";
        }
    }

    public String deployContribution(String domainId,String encodedContribution)
    {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomain(domainId);
            Deployment deployment=domain.getdeploymentClient();
            deployment.deployContribution(encodedContribution);
            domain.loadIt();
            JSONObject object = domain.toJSON();
            return object.toString();
    }

    public String deployComposite(String domainId,String compositeName, String encodedComposite)
    {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomain(domainId);
            Deployment deployment=domain.getdeploymentClient();
            deployment.deployComposite(compositeName, encodedComposite);
            domain.loadIt();
            JSONObject object = domain.toJSON();
            return object.toString();
    }

    public String undeployComposite(String treeId)
    {
        RootTreeNode root = RootTreeNode.getInstance();
        AbstractTreeNode node=root.getNode(treeId);
        
        if(node.getType()==ComponentTreeNode.COMPONENT_TYPE || node.getType()==CompositeTreeNode.COMPOSITE_TYPE)
        {
                String domainURI=node.getDomainURI();
                DomainTreeNode domain=root.getDomainByURI(domainURI);
                Deployment deployment=domain.getdeploymentClient();
                String nodePath=node.getFrascatiPath();
                deployment.undeployComposite(nodePath);
                domain.loadIt();
                JSONObject object = domain.toJSON();
                return object.toString();
        }
        throw new MyWebApplicationException("undeployComposite method can't be apply on a " + node.getType() + " node");
    }
}
