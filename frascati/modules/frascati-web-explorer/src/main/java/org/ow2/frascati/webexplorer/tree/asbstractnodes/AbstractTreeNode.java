/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012-2013 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.webexplorer.tree.asbstractnodes;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jdom.JDOMException;
// import org.jdom.input.SAXBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.webexplorer.tree.AbstractNode;

/**
 *
 */
public abstract class AbstractTreeNode implements AbstractNode
{
    private String id;
    private String text;
    private String image;
    private String type;
    private String domainURI;
    private String frascatiPath;
    public String parentId;
    
    protected AbstractNode parentNode;
    protected List<AbstractTreeNode> item;

    public AbstractTreeNode(AbstractNode parentNode, String text, String image, String type, String domainURI)
    {
       this.id=parentNode.getnextId();
       this.text=text;
       this.image=image;
       this.type=type;
       this.domainURI=parentNode.getDomainURI();
       this.frascatiPath=parentNode.getFrascatiPath()+"/"+this.text;
       this.item=new ArrayList<AbstractTreeNode>();
       this.parentNode=parentNode;
       if(domainURI!=null && domainURI!="") {
         this.domainURI=domainURI;
       }
    }
    
    public AbstractNode refreshNode() 
    {
//        String path=this.getRequestPath();
//        if(path==null)
//        {
//            return this.parentNode.refreshNode();
//        }
/*
        String urlString = null;
        try
        {
            this.item.clear();
            SAXBuilder sxb=new SAXBuilder();
            urlString=this.domainURI+path;
            URL url=new URL(urlString);
            Document document=sxb.build(url);
            Element element=document.getRootElement();
            this.buildNode(element);
*/
        return this;
/*
        }
        catch(MalformedURLException malformedURLException)
        {
            throw new MyWebApplicationException("The provided URL : "+urlString+" is not well formed");
        }
        catch (JDOMException jdomException)
        {
            throw new MyWebApplicationException("Cannot parse XML document retrieved from "+urlString);
        }
        catch (IOException ioException)
        {
            throw new MyWebApplicationException("Cannot parse XML document retrieved from "+urlString);
        }
*/
    }
        
    public JSONObject toJSON()
    {
        try
        {
            JSONObject jsonObject=new JSONObject(this);
            jsonObject.remove("class");
            
            if(!this.item.isEmpty())
            {
                JSONObject tempJSONItem;
                for(AbstractTreeNode tempNode : this.item)
                {
                    tempJSONItem=tempNode.toJSON();
                    jsonObject.append("item",tempJSONItem);
                }
            }
            return jsonObject;
        }
        catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500,"Cannot marshall "+this.text+" node to JSON Object");
        }
    }
    
    public void addTreeNode(AbstractTreeNode node)
    {
        String id=this.id+"_"+item.size();
        node.setId(id);
        this.item.add(node);
    }
    
    /*minor case to avoid JSON marshalling*/
    public String getnextId()
    {
        String nextId=this.getId();
        nextId+="_"+this.item.size();
        return nextId;
    }
    
    public AbstractTreeNode getTreeNode(int index) throws IndexOutOfBoundsException
    {
        try
        {
            return item.get(index);
        }
        catch(IndexOutOfBoundsException indexOutOfBoundsException)
        {
            throw new MyWebApplicationException("Node "+this.text+" as no child at position "+index);
        }
    }
    
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id=id;
    }
    
    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text=text;
    }
    
    public String getIm0()
    {
        return image;
    }
    
    public String getIm1()
    {
        return image;
    }
    
    public String getIm2()
    {
        return image;
    }
    
    public void setImage(String image)
    {
        this.image=image;
    }
    
    public String getTooltip()
    {
        return  type+" : "+text+ "("+id+")";
    }

    public String getType()
    {
        return type;
    }

    public String getDomainURI()
    {
        return domainURI;
    }
    
    public void setdomainURI(String domainURI)
    {
        this.domainURI=domainURI;
    }
    
    public String getFrascatiPath()
    {
        return frascatiPath;
    }
}
