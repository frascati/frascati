/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree;

/**
 *
 */
public enum AbstractTreeNodeType
{
    ROOT("root"),
    DOMAIN("domain","domain.png"),
    COMPOSITE("composite","scaCompositetStarted.png","scaCompositeStopped.png"),
    COMPONENT("composite","scaComponentStarted.png","scaComponentStopped.png"),
    SERVICE("service","scaService.png"),
    REFERENCE("reference","scaReference.png"),
    INTERFACE("interface","icone_i.png"),
    METHOD("method","icone_m.png"),
    BINDING("binding","scaBindingDefault.png");
    
    private String type;
    private String image;
    private String image2;
    
    private AbstractTreeNodeType(String type)
    {
        this.type=type;
    }
    
    private AbstractTreeNodeType(String type,String image)
    {
        this.type=type;
        this.image=image;
    }
    
    private AbstractTreeNodeType(String type,String image, String image2)
    {
        this.type=type;
        this.image=image;
        this.image2=image2;
    }

    public String getType()
    {
        return type;
    }

    public String getImage()
    {
        return image;
    }

    public String getImage2()
    {
        return image2;
    }
}
