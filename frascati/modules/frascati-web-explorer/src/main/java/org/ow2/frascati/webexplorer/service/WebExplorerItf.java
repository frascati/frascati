/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;

import org.osoa.sca.annotations.Service;

/**
 *
 */
@Service
public interface WebExplorerItf
{
    @GET
    @Path("/tree")
    @Produces("application/json")
    public String getTree();
    
    @GET
    @Path("/domain")
    @Produces("application/json")
    public String getDomain(@FormParam("id") String domainId);
    
    @POST
    @Path("/domain")
    @Produces("application/json")
    public String addDomain(@FormParam("name") String name, @FormParam("domainURI") String domainURI, @FormParam("color") String hexaColor);
    
    @DELETE
    @Path("/domain")
    @Produces("application/json")
    public String removeDomain(@FormParam("id") String domainId);

    @GET
    @Path("/node")
    @Produces("application/json")
    public String refreshTreeNode(@FormParam("treeId") String treeId);
    
    @POST
    @Path("/component/start")
    @Produces("application/json")
    public String startComponent(@FormParam("treeId") String treeId) ;
    
    @POST
    @Path("/component/stop")
    @Produces("application/json")
    public String stopComponent(@FormParam("treeId") String treeId) ;
    
    @POST
    @Path("/port")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public String invokeMethod(MultivaluedMap<String, String> params);
    
    @POST
    @Path("/property")
    @Produces({"text/plain"})
    public String setProperty(@FormParam("treeId") String treeId, @FormParam("value") String value);
    
    @POST
    @Path("/binding")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public String addBinding(MultivaluedMap<String, String> params) ;
    
    @DELETE
    @Path("/binding")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    public String removeBinding(@FormParam("treeId") String treeId, @FormParam("position") String position);
    
    @PUT
    @Path("/binding")
    @Produces("application/json")
    public String setBindingAttribute(@FormParam("treeId") String treeId,@FormParam("position") String position,@FormParam("attribute") String attribute,@FormParam("newValue") String newValue) ;
}
