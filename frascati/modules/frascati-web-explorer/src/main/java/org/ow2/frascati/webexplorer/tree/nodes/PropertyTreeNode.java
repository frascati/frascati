/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.nodes;

import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Property;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractIndexableTreeNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.root.RootTreeNode;
import org.ow2.frascati.webexplorer.tree.utils.TreeUtils;

/**
 *
 */
public class PropertyTreeNode extends AbstractIndexableTreeNode
{
    public final static String PROPERTY_TYPE="property";
    public final static String PROPERTY_IMG="scaProperty.png";

    private String valueType;
    private String value;
    
    public PropertyTreeNode(AbstractTreeNode parentNode, int position, Property property)
    {
        super(parentNode, property.getName(), PROPERTY_IMG, PROPERTY_TYPE, position);
        this.valueType=property.getType();
        this.value=property.getValue();
    }

    public boolean setPropertyValue(String newValue)
    {
        try
        {
            RootTreeNode root = RootTreeNode.getInstance();
            DomainTreeNode domain=root.getDomainByURI(this.getDomainURI());
            RemoteScaDomain introspection=domain.getintrospectionClient();

            String parentId= TreeUtils.getParentId(this.getId());
            AbstractTreeNode parentNode=root.getNode(parentId);
            introspection.setProperty(parentNode.getFrascatiPath(),this.getText(), newValue);
            return true;
        }
        catch(Exception exception)
        {
           return false;
        }
    }
    
    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getValueType()
    {
        return valueType;
    }
}
