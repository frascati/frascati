/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012-2013 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.webexplorer.tree.root;

import java.util.ArrayList;
import java.util.List;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Reference;

import org.json.JSONException;
import org.json.JSONObject;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.webexplorer.tree.AbstractNode;
import org.ow2.frascati.webexplorer.tree.asbstractnodes.AbstractTreeNode;
import org.ow2.frascati.webexplorer.tree.nodes.domain.DomainTreeNode;
import org.ow2.frascati.webexplorer.tree.utils.TreeIdIterator;

/**
 *
 */
@Scope("COMPOSITE")
@EagerInit // in order to force the instantiation of this class at the composite deployment time.
public class RootTreeNode implements AbstractNode
{
    public final static String ROOT_TYPE = "root";

    private static RootTreeNode instance;
    private static boolean isInitialized;
    private List<DomainTreeNode> domainList;

	@Reference(name="introspection")
	org.ow2.frascati.remote.introspection.RemoteScaDomain introspection;
	
	@Reference(name="deployment")
	org.ow2.frascati.remote.introspection.Deployment deployment;
	
    public RootTreeNode()
    {
        this.domainList = new ArrayList<DomainTreeNode>();
        instance = this;
    }

    protected void init()
    {
        String bindingURIProperty = System.getProperty(AbstractBindingFactoryProcessor.BINDING_URI_BASE_PROPERTY_NAME);
        if (bindingURIProperty == null)
        {
            bindingURIProperty = AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;
        }
        //
        // Don't remove following this.deployment and this.introspection parameters
        // as they are required when the FraSCAti Web Explorer is packaged into a WAR
        // and deployed into a Web container (e.g., Tomcat).
        //
        DomainTreeNode defaultDomain = new DomainTreeNode(this, "Default Domain", bindingURIProperty, "FFFFFF", this.deployment, this.introspection);
        this.domainList.add(defaultDomain);  
        defaultDomain.loadIt();
    }

    public static RootTreeNode getInstance()
    {
        if (!isInitialized)
        {
            isInitialized = true;
            instance.init();
        }
        return instance;
    }

    public JSONObject toJSON()
    {
        try
        {
            JSONObject root = new JSONObject();
            root.put("id", 0);

            JSONObject rootDomain = new JSONObject();
            rootDomain.put("id", "0_");
            rootDomain.put("text", "Root");
            rootDomain.put("type", ROOT_TYPE);
            rootDomain.put("im0", "rootDomain.png");
            rootDomain.put("im1", "rootDomain.png");
            rootDomain.put("im2", "rootDomain.png");
            JSONObject jsonDomainNode;
            for (DomainTreeNode tmpDomainNode : domainList)
            {
                jsonDomainNode = tmpDomainNode.toJSON();
                rootDomain.append("item", jsonDomainNode);
            }

            root.append("item", rootDomain);
            return root;
        } catch (JSONException jsonException)
        {
            throw new MyWebApplicationException(500, "Cannot marshall Root node to JSON object");
        }
    }

    /* minor case to avoid JSON marshalling */
    public String getnextId()
    {
        String nextId = "0_" + domainList.size();
        return nextId;
    }

    public DomainTreeNode getDomain(String domainId)
    {
        String tmpDomainId;
        for (DomainTreeNode tmpDomain : domainList)
        {
            tmpDomainId = tmpDomain.getId();
            if (tmpDomainId.equals(domainId))
            {
                return tmpDomain;
            }
        }
        throw new MyWebApplicationException("Cannot find FraSCAti domain for id : " + domainId);
    }

    public DomainTreeNode getDomainByURI(String domainURI)
    {
        String tmpDomainURI;
        for (DomainTreeNode tmpDomain : domainList)
        {
            tmpDomainURI = tmpDomain.getDomainURI();
            if (tmpDomainURI.equals(domainURI))
            {
                return tmpDomain;
            }
        }
        return null;
    }

    public void addDomain(DomainTreeNode domain)
    {
        String id = "0_" + domainList.size();
        domain.setId(id);
        domainList.add(domain);
    }

    public boolean removeDomain(String domainId)
    {
        DomainTreeNode domain = this.getDomain(domainId);
        boolean isRemoved = domainList.remove(domain);
        if (isRemoved)
        {
            DomainTreeNode domainTreeNode;
            for (int i = 0; i < domainList.size(); i++)
            {
                domainTreeNode = domainList.get(i);
                domainTreeNode.setId("0_" + i);
            }
        }
        return isRemoved;
    }

    public AbstractTreeNode getNode(String treeId)
    {
        TreeIdIterator treeIdIterator = new TreeIdIterator(treeId);
        int nodeId = treeIdIterator.getNextId();
        if (nodeId != 0)
            return null;

        AbstractTreeNode node;
        int treeNodeId = treeIdIterator.getNextId();
        node = this.domainList.get(treeNodeId);

        while (treeIdIterator.hasMoreId())
        {
            treeNodeId = treeIdIterator.getNextId();
            node = node.getTreeNode(treeNodeId);
        }

        return node;
    }

    public AbstractNode refreshNode()
    {
        for (DomainTreeNode tmpDomain : domainList)
        {
            tmpDomain.loadIt();
        }
        return this;
    }

    public String getFrascatiPath()
    {
        return "";
    }

    public String getDomainURI()
    {
        return "";
    }
}
