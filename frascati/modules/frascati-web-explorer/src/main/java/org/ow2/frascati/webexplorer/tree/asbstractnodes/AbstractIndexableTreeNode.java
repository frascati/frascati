/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.asbstractnodes;

import org.ow2.frascati.webexplorer.tree.AbstractNode;

/**
 *
 */
public class AbstractIndexableTreeNode extends AbstractTreeNode
{
    private int position;
    
    public AbstractIndexableTreeNode(AbstractNode parentNode, String text, String image, String type, int position)
    {
        super(parentNode, text, image, type, null);
        this.position=position;
    }

    public int getPosition()
    {
        return this.position;
    }
}
