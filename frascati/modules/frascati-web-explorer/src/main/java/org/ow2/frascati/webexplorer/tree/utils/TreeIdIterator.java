/**
 * OW2 FraSCAti Web Explorer
 * Copyright (C) 2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.webexplorer.tree.utils;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;

/**
 *
 */
public class TreeIdIterator
{
    
    private ArrayList<Integer> nodeIds;
    private int currentIndex;
    
    public TreeIdIterator(String treeId)
    {
        nodeIds=new ArrayList<Integer>();
        StringTokenizer tokenizer=new StringTokenizer(treeId, "_");
        String token;
        int intToken;
        while(tokenizer.hasMoreTokens())
        {
            token=tokenizer.nextToken();
            try
            {
                intToken=Integer.valueOf(token);
                nodeIds.add(intToken);
            }
            catch (NumberFormatException numberFormatException)
            {
                throw new MyWebApplicationException("Invalid tree Id : "+treeId);
            }
        }
        currentIndex=0;
    }
    
    public int getNextId()
    {
        if(currentIndex!=nodeIds.size())
        {
            int currentNodeId= nodeIds.get(currentIndex);
            currentIndex++;
            return currentNodeId;
        }
        return -1;
    }
    
    public boolean hasMoreId()
    {
        return currentIndex!=nodeIds.size();
    }
    
}
