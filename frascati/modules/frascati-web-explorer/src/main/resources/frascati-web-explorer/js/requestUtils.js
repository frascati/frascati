/**
 * OW2 FraSCAti Introspection
 * Copyright (C) 2008-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */	

	function getTree()
	{
		var response=getRequest("/tree","get");
		if(response!=null)
		{
			eval("tree=("+response+")");
			parent.refreshDHTMLXTree(tree);
		}
	}

	function addDomain(domainName, domainURI, color)
	{
		var params="name="+encodeURIComponent(domainName);
		params+="&domainURI="+encodeURIComponent(domainURI);
		params+="&color="+encodeURIComponent(color);
		var response=getRequest("/domain","post",params);
		if(response!=null)
		{
			eval("domain=("+response+")");
			parent.refreshTreeItem(domain);
			parent.document.bgColor = domain.color;
		}
	}
	
	function removeDomain(domainId)
	{
		var params="domainId="+encodeURIComponent(domainId);
		var response=getRequest("/domain","delete",params);
		if(response!=null)
		{
			eval("tree=("+response+")");
			parent.refreshDHTMLXTree(tree);
			return tree;
		}
	}
	
	function refreshTreeNode(treeId)
	{
		var params="treeId="+encodeURIComponent(treeId);
		var response=getRequest("/node","get",params);
		if(response!=null)
		{
			eval("node=("+response+")");
			parent.refreshTreeItemDatas(node);
			var domain=getDomainNode(node.id);
			parent.document.bgColor = domain.color;
		}
	}
	
	function getCompositesName(domainId,encodedJar)
	{
		var params="domainId="+encodeURIComponent(domainId);
		params+="&jar="+encodedJar;
		var response=getRequest("/upload/getCompositesName","post",params);
		if(response!=null)
		{
			return response;
		}
	}

	function uploadContribution(domainId,encodedContribution)
	{
		var params="domainId="+encodeURIComponent(domainId);
		params+="&contribution="+encodedContribution;
		var response=getRequest("/upload/contribution","post",params);
		if(response!=null)
		{
			eval("domain=("+response+")");
			parent.refreshTreeItem(domain);
		}
	}
	
	function uploadComposite(domainId,compositeName,encodedJar)
	{
		var params="domainId="+encodeURIComponent(domainId);
		params+="&compositeName="+encodeURIComponent(compositeName);
		params+="&jar="+encodedJar;
		var response=getRequest("/upload/composite","post",params);
		if(response!=null)
		{
			eval("domain=("+response+")");
			parent.refreshTreeItem(domain);
		}
	}
	
	function startStopComponent(treeId,method)
	{
		var params="treeId="+encodeURIComponent(treeId);
		var response=getRequest("/component/"+method,"post",params);
		if(response!=null)
		{
			eval("abstractComponent=("+response+")");
			refreshTreeItem(abstractComponent);
		}
	}
	
	function setProperty(treeId, newValue)
	{
		var params="treeId="+encodeURIComponent(treeId);
		params+="&value="+encodeURIComponent(newValue);
		var response=getRequest("/property","post",params);
		if(response!=null)
		{
			eval("isSet=("+response+")");
			return isSet;
		}
	}
	
	function removeBinding(treeId,position)
	{
		var params="treeId="+encodeURIComponent(treeId);
		params+="&position="+encodeURIComponent(position);
		var response=getRequest("/binding","delete",params);
		if(response!=null)
		{
			eval("serviceOrReference=("+response+")");
			parent.refreshTreeItemDatas(serviceOrReference);
		}
	}

	function setBindingAttribute(treeId,position,attribute,newValue)
	{
		var params="treeId="+encodeURIComponent(treeId);
		params+="&position="+encodeURIComponent(position);
		params+="&attribute="+encodeURIComponent(attribute);
		params+="&newValue="+encodeURIComponent(newValue);
		var response=getRequest("/binding","put",params);
		if(response!=null)
		{
			eval("serviceOrReference=("+response+")");
			parent.refreshTreeItemDatas(serviceOrReference);
		}
	}
	
	function addBindingRequest(treeId,kind,parameters)
	{
		var params="treeId="+encodeURIComponent(treeId);
		params+="&kind="+encodeURIComponent(kind);
		var parameter;
		if(parameters!=undefined)
		{
			for(var i=0;i<parameters.length;i++)
			{
				parameter=parameters[i];
				params+="&"+parameter.name+"="+encodeURIComponent(parameter.value);
			}
		}
		var response=getRequest("/binding","post",params);
		if(response!=null)
		{
			eval("serviceOrReference=("+response+")");
			parent.refreshTreeItem(serviceOrReference);
		}
	}
	
	function invokeMethodRequest(treeId,parameters)
	{
		var params="treeId="+encodeURIComponent(treeId);
		var parameter;
		if(parameters!=undefined)
		{
			for(var i=0;i<parameters.length;i++)
			{
				parameter=parameters[i];
				params+="&"+parameter.name+"="+encodeURIComponent(parameter.value);
			}
		}
		var response=getRequest("/port","post",params);
		if(response!=null)
		{
			return response;
		}
	}

	function getRequest(path,method,params)
	{
		var textResponse;
		var myRequest=new Request({
		    url: "../WebExplorerService"+path,
		    method : method,
		    async : false,
		    emulation: false,
		    onSuccess : function(responseText,responseXML)
		    {
		    	textResponse=responseText;
		    },
			onFailure : function(xhr)
			{
				var failureException=new Object();
				failureException.name="Ajax Request error";
				failureException.message="Status : "+xhr.status;
			}
		});
		
		if(params!=undefined && params!="")
		{
			myRequest.setHeader("Content-Type","application/x-www-form-urlencoded");
			myRequest.send(params);
		}
		else
		{
			myRequest.send();
		}
		
		if(textResponse!=undefined && textResponse!="")
		{
			return textResponse;
		}
	}
	