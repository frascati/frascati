
	function fillComponentsPanel()
	{
		if(currentComponentTreeNode.components==undefined || currentComponentTreeNode.components.length==0)
		{
			var noCom=document.createElement("h2");
			panel_content.appendChild(noCom);
			noCom.innerHTML="No components";
		}
		else
		{
			var accordion=document.createElement("div");
			panel_content.appendChild(accordion);
			accordion.id='accordion';
			
			for(var i=0;i<currentComponentTreeNode.components.length;i++)
			{
				var titleDiv=document.createElement("div");
				accordion.appendChild(titleDiv);
				titleDiv.className="title"; 
				
				var image=document.createElement("img");
				titleDiv.appendChild(image);
				image.src="images/scaComponent.png";
				image.style.cssFloat="left";
				image.style.marginRight="5px";
				
				var title=document.createElement("h2");
				titleDiv.appendChild(title);
				var componentNode=getJSONTreeNode(currentComponentTreeNode.components[i]);
				title.innerHTML=componentNode.text;
				
				var content=document.createElement("div");
				accordion.appendChild(content);
				content.className="content";
			}
		}
	}
	
	function fillServicesPanel()
	{
		if(currentComponentTreeNode.services==undefined || currentComponentTreeNode.services.length==0)
		{
			var noSer=document.createElement("h2");
			panel_content.appendChild(noSer);
			noSer.innerHTML="No services";
		}
		else
		{
			var accordion=document.createElement("div");
			panel_content.appendChild(accordion);
			accordion.id='accordion';
			
			var service;
			for(var i=0;i<currentComponentTreeNode.services.length;i++)
			{
				service=getJSONTreeNode(currentComponentTreeNode.services[i]);
				
				var titleDiv=document.createElement("div");
				accordion.appendChild(titleDiv);
				titleDiv.className="title"; 
				
				var image=document.createElement("img");
				titleDiv.appendChild(image);
				image.src="images/scaService.png";
				image.style.cssFloat="left";
				image.style.marginRight="5px";
				
				var title=document.createElement("h2");
				titleDiv.appendChild(title);
				title.innerHTML=service.text;
				
				var content=document.createElement("div");
				accordion.appendChild(content);
				content.className="content";

				var interfaceNode=getJSONTreeNode(service.interfaceId);
				fillMethodInvocationPanel(content,interfaceNode);
				fillBindingsPanel(content,service);
			}
		}
	}
	
	function fillReferencesPanel()
	{
		if(currentComponentTreeNode.references==undefined || currentComponentTreeNode.references.length==0)
		{
			var noRef=document.createElement("h2");
			panel_content.appendChild(noRef);
			noRef.innerHTML="No references";
		}
		else
		{
			var accordion=document.createElement("div");
			panel_content.appendChild(accordion);
			accordion.id='accordion';
			
			var reference;
			for(var i=0;i<currentComponentTreeNode.references.length;i++)
			{
				reference=getJSONTreeNode(currentComponentTreeNode.references[i]);
				
				var titleDiv=document.createElement("div");
				accordion.appendChild(titleDiv);
				titleDiv.className="title"; 
				
				var image=document.createElement("img");
				titleDiv.appendChild(image);
				image.src="images/scaReference.png";
				image.style.cssFloat="left";
				image.style.marginRight="5px";
				
				var title=document.createElement("h2");
				titleDiv.appendChild(title);
				title.innerHTML=reference.text;
				
				var content=document.createElement("div");
				accordion.appendChild(content);
				content.className="content";

				var interfaceNode=getJSONTreeNode(reference.interfaceId);
				fillMethodInvocationPanel(content,interfaceNode);		
				fillBindingsPanel(content,reference);
			}
		}
	}
	
	function fillPropertiesPanel()
	{
		if(currentComponentTreeNode.properties==undefined || currentComponentTreeNode.properties.length==0)
		{
			var noProp=document.createElement("h2");
			panel_content.appendChild(noProp);
			noProp.innerHTML="No properties";
		}
		else
		{
			var accordion=document.createElement("div");
			panel_content.appendChild(accordion);
			accordion.id='accordion';
			
			var property;
			for(var i=0;i<currentComponentTreeNode.properties.length;i++)
			{
				property=getJSONTreeNode(currentComponentTreeNode.properties[i]);
				
				var titleDiv=document.createElement("div");
				accordion.appendChild(titleDiv);
				titleDiv.className="title"; 
				
				var image=document.createElement("img");
				titleDiv.appendChild(image);
				image.src="images/scaProperty.png";
				image.style.cssFloat="left";
				image.style.marginRight="5px";
				
				var title=document.createElement("h2");
				titleDiv.appendChild(title);
				title.innerHTML=property.text;/**/
				
				var content=document.createElement("div");
				accordion.appendChild(content);
				content.className="content";
				
				var form=document.createElement("form");
				content.appendChild(form);
				
				var currentValue=document.createElement("h4");
				form.appendChild(currentValue);
				form.appendChild(document.createElement("br"));
				currentValue.id="currentValue";
				currentValue.innerHTML=property.valueType+" : "+property.value;			
							
				var text=document.createElement("input");
				form.appendChild(text);
				text.type="text";
				text.name="newValue";
				text.title=property.value;
				
				var treeId=document.createElement("input");
				form.appendChild(treeId);
				treeId.type="hidden";
				treeId.name="treeId";
				treeId.value=property.id;
				
				var button=document.createElement("input");
				form.appendChild(button);
				button.type="button";
				button.value="set value";
				button.onclick=function()
				{
					var newValue=this.form.newValue.value;
					var treeId=this.form.treeId.value;
					var isSet=setProperty(treeId,newValue);
					this.form.newValue.value="";
					if(isSet==true)
					{
						this.form.getElementById("currentValue").innerHTML="currentValue :"+newValue;
					}
				};
			}
		}
	}
	
	/*******************************************Method Invocation Panel************************************************************/  
	
	function fillMethodInvocationPanel(content,interfaceNode)
	{	
		var container=document.createElement("fieldset");
		container.className="fieldsetContainer";
		content.appendChild(container);
		
		var legend=document.createElement("legend");
		container.appendChild(legend);
		legend.className="containerlegend";
		legend.innerHTML="interface : "+interfaceNode.text;
		
		var image=document.createElement("img");
		legend.appendChild(image);
		image.src="images/icone_i.png";
		image.style.cssFloat="left";
		image.style.marginRight="5px";
		
		var method;
		
		for(var i=0;i<interfaceNode.methods.length;i++)
		{
			method=getJSONTreeNode(interfaceNode.methods[i]);
			
			var form=document.createElement("form");
			container.appendChild(form);
			
			var methodName=document.createElement("input");
			form.appendChild(methodName);
			methodName.type="hidden";
			methodName.name="methodName";
			methodName.value=method.value;
			
			var treeId=document.createElement("input");
			form.appendChild(treeId);
			treeId.type="hidden";
			treeId.name="treeId";
			treeId.value=method.id;
			
			var fieldset=document.createElement("fieldset");
			fieldset.className="fieldsetform";
			form.appendChild(fieldset);
			
			var legend=document.createElement("legend");
			fieldset.appendChild(legend);
			legend.className="formlegend";
			legend.innerHTML=method.signature;

			var lab,input;
			if(method.parameters!=undefined)
			{
				for(var j=0;j<method.parameters.length;j++)
				{
					var parameter=method.parameters[j];
					
					lab=document.createElement("label");
					fieldset.appendChild(lab);
					lab.className="desc";
					lab.innerHTML=parameter+" : ";
					
					input=document.createElement("input");
					fieldset.appendChild(input);
					input.type="text";
					input.name="Parameter"+j;
					input.className="formField";
					input.size=70;
					
					fieldset.appendChild(document.createElement("br"));
				}

			}
			
			var image=document.createElement("img");
			legend.appendChild(image);
			image.src="images/icone_m.png";
			image.style.cssFloat="left";
			image.style.marginRight="5px";
									
			var invokeButton=document.createElement("input");
			fieldset.appendChild(invokeButton);
			invokeButton.type="button";
			invokeButton.id="invokeButton";
			invokeButton.value="invoke method";
			invokeButton.className="button";
			invokeButton.onclick=function(){invokeMethod(this.form)};
			
		}
	}
	
	function invokeMethod(form)
	{
		var treeId=form.treeId.value;
		
		var inputs=form.getElementsByTagName("input");
		var parameters=new Array();
		var input,parameter;
		for(var i=0;i<inputs.length;i++)
		{
			input=inputs[i];
			if(input.type=="text")
			{
				parameter=new Object();
				parameter.name=input.name;
				parameter.value=input.value;
				parameters.push(parameter);
				input.value="";
			}
		}
		
		var response=invokeMethodRequest(treeId,parameters);
		
		var responseArea=form.getElementById("responseArea");
		if(responseArea==undefined || responseArea==null)
		{
			responseArea=document.createTextNode("");
			responseArea=document.createElement("h4");
			responseArea.id="responseArea";
			form.appendChild(responseArea);
		}	
		
		if(response==undefined || response==null || response=="")	responseArea.innerHTML="no response to display";
		else														responseArea.innerHTML=response;
	}
	
	/*******************************************Bindings Panel************************************************************/  
	
	var bindingsList=["WS","REST","RMI","JSON_RPC","JMS","UPnP","SCA"/*,"JGroups","JNA"*/];
	var bindingsAttributesList={"WS": {"service" : ["uri"], "reference":["wsdlLocation","wsdlElement","uri"]},
								"REST":	["uri"],
								"RMI":	["host","serviceName","port"],
								"JSON_RPC":["uri"],
								"SCA":["uri"],
								//"JNA":		["library"],
								"JMS":		["jndiInitialContextFactory","jndiURL","jndiDestinationName","destinationType"],
								//"JGroups":	["cluster"],
								"UPnP":	[]
							   };
	
	
	function fillBindingsPanel(content,element)
	{	
		
		var container=document.createElement("fieldset");
		content.appendChild(container);
		container.className="fieldsetContainer";
		
		var legend=document.createElement("legend");
		container.appendChild(legend);
		legend.className="containerlegend";
		legend.innerHTML="Bindings";

		if(element.bindings!=undefined)
		{
			var binding;
			for(var i=0;i<element.bindings.length;i++)
			{
				var attribteName,attributeValue,node,binding;
				binding=getJSONTreeNode(element.bindings[i]);
				
				var form=document.createElement("form");
				container.appendChild(form);
				
				var position=document.createElement("input");
				form.appendChild(position);
				position.type="hidden";
				position.name="position";
				position.value=i;
				
				var treeId=document.createElement("input");
				form.appendChild(treeId);
				treeId.type="hidden";
				treeId.name="treeId";
				treeId.value=element.id;
				
				var fieldset=document.createElement("fieldset");
				form.appendChild(fieldset);
				fieldset.className="fieldsetform";
				
				var legend=document.createElement("legend");
				fieldset.appendChild(legend);
				legend.className="formlegend";
				legend.innerHTML="Binding : "+binding.kind;
				
				var image=document.createElement("img");
				legend.appendChild(image);
				image.src="images/"+binding.im0;
				image.style.cssFloat="left";
				image.style.marginRight="5px";
				
				var attribute;
				var lab1,lab2;
				for(var j=0;j<binding.attributes.length;j++)
				{
					attribute=binding.attributes[j];
					
					lab1=document.createElement("label");
					fieldset.appendChild(lab1);
					lab1.className="desc";
					lab1.innerHTML=attribute.name+" : ";
					
					lab2=document.createElement("label");
					fieldset.appendChild(lab2);
					lab2.className="value";
					lab2.name=attribute.name;
					lab2.innerHTML=attribute.value;

					fieldset.appendChild(document.createElement("br"));
				}
				
				fieldset.appendChild(document.createElement("br"));
				
				var deleteButton=document.createElement("input");
				fieldset.appendChild(deleteButton);
				deleteButton.type="button";
				deleteButton.id="deleteButton";
				deleteButton.value="delete binding";
				deleteButton.className="button";
				deleteButton.onclick=function(){deleteBinding(this.form)};
		
				var editButton=document.createElement("input");
				fieldset.appendChild(editButton);
				editButton.type="button";
				editButton.id="editButton";
				editButton.value="edit binding";
				editButton.className="button";
				editButton.onclick=function(){editBinding(this.form)};		
			}
		}
		
		var form=document.createElement("form");
		container.appendChild(form);
		
		var position=document.createElement("input");
		form.appendChild(position);
		position.type="hidden";
		position.name="position";
		position.value=i;
		
		var treeId=document.createElement("input");
		form.appendChild(treeId);
		treeId.type="hidden";
		treeId.name="treeId";
		treeId.value=element.id;
		
		var fieldset=document.createElement("fieldset");
		form.appendChild(fieldset);
		fieldset.className="fieldsetform";
		fieldset.id="form_fieldset";
		
		var legend=document.createElement("legend");
		fieldset.appendChild(legend);
		legend.className="formlegend";
		legend.innerHTML="Create New Binding";
		
		var selectKind=document.createElement("select");
		fieldset.appendChild(selectKind);
		selectKind.id="kindlist";
		selectKind.name="kind";
		selectKind.onchange=function(){createAddBindingForm(this.form)};
		
		var option;
		option=document.createElement("option");		
		option.value="NO KIND";
		option.innerHTML="Select kind of binding";
		selectKind.appendChild(option);
		
		for(var i=0;i<bindingsList.length;i++)
		{
			option=document.createElement("option");
			option.value=bindingsList[i];
			option.innerHTML=bindingsList[i];
			selectKind.appendChild(option);
		}		
		fieldset.appendChild(document.createElement("br"));
	}
	
	function deleteBinding(form)
	{
		var position=form.position.value;
		var treeId=form.treeId.value;
		new Fx.Morph(form).set({'opacity': 0.4});
		removeBinding(treeId,position);
		new Fx.Slide(form,{mode:'vertical'}).slideOut();
	}
	
	function editBinding(form)
	{
		var fieldset=form.getElementsByTagName('fieldset')[0];	
		var labels=form.getElementsByTagName("label");
		
		var toReplace=new Array();
		var replacers=new Array();
		var lab,input;
		for(var i=0;i<labels.length;i++)
		{
			lab=labels[i];
			if(lab.className=="value")
			{
				toReplace.push(lab);
				input=document.createElement("input");
				input.type="text";
				input.name=lab.name;
				input.className="formField";
				input.value=lab.innerHTML;
				input.size=70;
				replacers.push(input);
			}
		}
		
		for(var i=0;i<toReplace.length;i++)	fieldset.replaceChild(replacers[i],toReplace[i]);
		
		var editButton=form.getElementById("editButton");
		editButton.id="modifyButton";
		editButton.value="modify binding";
		editButton.onclick=function(){modifyBinding(form)};
		
		var cancelEditButton=document.createElement("input");
		fieldset.appendChild(cancelEditButton);
		cancelEditButton.type="button";
		cancelEditButton.id="cancelEditButton";
		cancelEditButton.value="cancel editing";
		cancelEditButton.className="button";
		cancelEditButton.onclick=function(){cancelEditing(this.form)};
	}
	
	function modifyBinding(form)
	{
		var treeId=form.treeId.value;
		var JSONObject=getJSONTreeNode(treeId);
				
		var position=form.position.value;
		var bindingId=JSONObject.bindings[position];
		var binding=getJSONTreeNode(bindingId);
		var fieldset=form.getElementsByTagName('fieldset')[0];
		var inputs=form.getElementsByTagName("input");
		var inputsFields=form.getElementsByTagName("input");
		
		var toReplace=new Array();
		var replacers=new Array();
		var lab,input,field,attribute;
		var isModify=false;
		for(var i=0;i<inputs.length;i++)
		{
			input=inputs[i];
			if(input.type=="text")
			{
				for( var j=0; j<binding.attributes.length; j++)
				{
					attribute=binding.attributes[j];
					if(input.name==attribute.name && input.value!=attribute.value)
					{
						setBindingAttribute(treeId, position, input.name, input.value);
						attribute.value=input.value;
						modify=true;
					}
				}
				
				toReplace.push(input);
				lab=document.createElement("label");
				lab.className="value";
				lab.name=input.name;
				lab.innerHTML=input.value;
				replacers.push(lab);
			}
		}
		
		for(var i=0;i<toReplace.length;i++)	fieldset.replaceChild(replacers[i],toReplace[i]);
		
		var modifyButton=form.getElementById("modifyButton");
		modifyButton.id="editButton";
		modifyButton.value="edit binding";
		modifyButton.onclick=function(){editBinding(form)};
		
		var cancelEditButton=fieldset.getElementById("cancelEditButton");
		fieldset.removeChild(cancelEditButton);
		
//		if(isModify==true)	JSONObject.reload();
	}
	
	function cancelEditing(form)
	{
		var treeId=form.treeId.value;
		var JSONObject=getJSONTreeNode(treeId);
				
		var position=form.position.value;
		var binding=JSONObject.bindings[position];
		
		var fieldset=form.getElementsByTagName('fieldset')[0];
		var inputs=form.getElementsByTagName("input");

		var toReplace=new Array();
		var replacers=new Array();
		var lab,input,field,attribute;
		for(var i=0;i<inputs.length;i++)
		{
			input=inputs[i];
			if(input.type=="text")
			{
				attribute=binding.getAttribute(input.name);
				toReplace.push(input);
				lab=document.createElement("label");
				lab.className="value";
				lab.name=input.name;
				lab.innerHTML=attribute.value;
				replacers.push(lab);
			}
		}
		
		for(var i=0;i<toReplace.length;i++)	fieldset.replaceChild(replacers[i],toReplace[i]);
		
		var modifyButton=form.getElementById("modifyButton");
		modifyButton.id="editButton";
		modifyButton.value="edit binding";
		modifyButton.onclick=function(){editBinding(form)};
		
		var cancelEditButton=fieldset.getElementById("cancelEditButton");
		fieldset.removeChild(cancelEditButton);	
	}
	
	var currentBindingKind=null;
	
	function createAddBindingForm(form)
	{
		var selectKind=form.getElementById("kindlist");
		var kind=selectKind.options[selectKind.selectedIndex].value;

		if(currentBindingKind!=null && currentBindingKind==kind)	return;
		currentBindingKind=kind;

		var fieldset=form.getElementById("form_fieldset");
		var container=fieldset.getElementById("form_container");
		if(container!=undefined)	fieldset.removeChild(container);
		
		container=document.createElement("div");
		fieldset.appendChild(container);
		container.id="form_container";
						
		var fields;
		eval("fields=bindingsAttributesList."+kind+";");
		
		if(fields.service!=undefined && fields.reference!=undefined)
		{
			var treeId=form.treeId.value;
			var JSONObject=getJSONTreeNode(treeId);
			if(JSONObject.type=="reference")			fields=fields.reference;
			else if(JSONObject.type=="service")			fields=fields.service;
		}
		
		if(fields.length==0)
		{
			var noProp=document.createTextNode("no properties needed");
			container.appendChild(noProp);
			container.appendChild(document.createElement("br"));
		}
		else
		{
			var lab,input;
			for(var i=0;i<fields.length;i++)
			{
				var lab=document.createElement("label");
				container.appendChild(lab);
				lab.className="desc";
				lab.innerHTML=fields[i]+" : ";
				
				input=document.createElement("input");
				container.appendChild(input);
				input.type="text";
				input.name=fields[i];
				input.size=70;
				
				container.appendChild(document.createElement("br"));
			}
		}
		
		var addButton=document.createElement("input");
		container.appendChild(addButton);
		addButton.type="button";
		addButton.value="add binding";
		addButton.className="button";
		addButton.onclick=function(){addBinding(this.form);};	
	}

	function addBinding(form)
	{
		var selectKind=form.getElementById("kindlist");
		var kind=selectKind.options[selectKind.selectedIndex].value;
		
		var inputs=form.getElementsByTagName("input");
		var parameters=new Array();
		
		var input,objectParameter;
		for(var i=0;i<inputs.length;i++)
		{
			input=inputs[i];
			if(input.type=="text" && input.value!="")
			{
				objectParameter=new Object();
				objectParameter.name=input.name;
				objectParameter.value=input.value;
				parameters.push(objectParameter);
			}
		}
		
		var treeId=form.treeId.value;
		addBindingRequest(treeId,kind,parameters);
		
//		JSONObject.reload();
//		refreshPanelContent();
	}
