/**
 * OW2 FraSCAti Introspection
 * Copyright (C) 2008-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
	include("js/requestUtils.js");
	include("js/uploadUtils.js");
	include("js/htmlBuilder.js");
	
	var DHTMLXLayout;
	var DHTMLXTree;
	var mainPanel;
	
	function doOnLoadExplorer()
	{
		DHTMLXLayout = new dhtmlXLayoutObject(document.body, "2U");
		DHTMLXLayout.attachHeader("header");
		DHTMLXLayout.attachFooter("footer");
		DHTMLXLayout.cont.obj._offsetTop = 150;
		DHTMLXLayout.cont.obj._offsetHeight = -300;
		DHTMLXLayout.cont.obj._offsetLeft = 100;
		DHTMLXLayout.cont.obj._offsetWidth = -200;
		DHTMLXLayout.setSizes();
		
	    DHTMLXLayout.cells("a").setWidth(400);
	    DHTMLXLayout.cells("a").setText("SCA Domain");
	    DHTMLXTree = DHTMLXLayout.cells("a").attachTree();
		DHTMLXTree.setImagePath("images/");
		DHTMLXTree.setDataMode("json");
	    DHTMLXTree.attachEvent("onClick", onClickListener);
	    
	    DHTMLXLayout.cells("b").setText("About the WebExplorer");
	    DHTMLXLayout.cells("b").attachURL("rootDomain.html");
	    mainPanel=DHTMLXLayout.cells("b");
	
	    DHTMLXLayout.cells("a").progressOn();
	    var tree=getTree();
	    DHTMLXLayout.cells("a").progressOff();
	}
	
	function onClickListener(nodeID)
    {
		if(nodeID=="0_")
		{
			mainPanel.setText("About the WebExplorer");	    
	    	mainPanel.attachURL("rootDomain.html");
	    	return;
		}
		
		var isDomain=isDomainNode(nodeID);
		if(isDomain==true)
		{
			mainPanel.setText("Managing Domain");
	    	mainPanel.attachURL("domain.html");
		}
		else
		{
			mainPanel.attachURL("mainPanel.html");
		}
    }
	
	var JSONTree;
	function refreshDHTMLXTree(tree)
	{ 
		var dhtmlxTree=DHTMLXTree;
		if(dhtmlxTree==undefined)
		{
			dhtmlxTree=parent.DHTMLXTree;
		}

		dhtmlxTree.deleteChildItems(0);
		console.log(tree);
		dhtmlxTree.loadJSONObject(tree);
		JSONTree=tree;
		dhtmlxTree.openItem("0_");
		dhtmlxTree.setItemCloseable("0_",0);
	}
	
	function refreshTreeItem(item)
	{ 
		var selectedItemId=parent.DHTMLXTree.getSelectedItemId();
		parent.DHTMLXTree.deleteItem(item.id,true);
		var parentItem=refreshTreeItemDatas(item);
		parent.DHTMLXTree.insertNewChild(parentItem.id,item.id,item.text,0,item.im0,item.im1,item.im2);
		addItems(item);
		parent.DHTMLXTree.selectItem(selectedItemId,true);
	}

	function refreshTreeItemDatas(item)
	{
		var parentItem=getJSONParentTreeNode(item.id);
		var itemIndex=getItemIndex(item.id);
		parentItem.item[itemIndex]=item;
		return parentItem;
	}
	
	function addItems(JSONObject)
	{
		if(JSONObject.item!=undefined)
		{
			var tmpItem;
			for(var i=0; i<JSONObject.item.length; i++)
			{
				tmpItem=JSONObject.item[i];
				parent.DHTMLXTree.insertNewChild(JSONObject.id,tmpItem.id,tmpItem.text,0,tmpItem.im0,tmpItem.im1,tmpItem.im2);
				addItems(tmpItem);
				parent.DHTMLXTree.setItemText(tmpItem.id,tmpItem.text,tmpItem.tooltip);
				parent.DHTMLXTree.closeAllItems(tmpItem.id);
			}
		}
		parent.DHTMLXTree.setItemText(JSONObject.id,JSONObject.text,JSONObject.tooltip);
		parent.DHTMLXTree.closeAllItems(JSONObject.id);
	}
	
	function addSCADomain()
	{
		var SCADomainName=document.getElementById("SCADomainName").value;
		var SCADomainBaseURI=document.getElementById("SCADomainBaseURI").value;
		var color=document.getElementById("color").value;
		addDomain(SCADomainName,SCADomainBaseURI,color);

	}
	
	function removeSCADomain()
	{
		var selectedItemId=parent.DHTMLXTree.getSelectedItemId();
		removeDomain(selectedItemId);
	}
	
	/*******************************************MainPanel************************************************************/
	
	
	var selectedTreeNode,currentComponentTreeNode,currentSelectedMenuTreeNode;
	var currentMenu;
	var slide;
	
	function doOnLoadMainPanel()
	{	
		var selectedItemId=parent.DHTMLXTree.getSelectedItemId();
		refreshTreeNode(selectedItemId);
		selectedTreeNode=getJSONTreeNode(selectedItemId);
		currentComponentTreeNode=getJSONComponentTreeNode(selectedItemId);
		currentSelectedMenuTreeNode=getSelectedMenuTreeNode();
		
		parent.mainPanel.setText("  "+currentComponentTreeNode.frascatiPath);
		
		var componentTreeNodeDom=document.getElementById("component_name");
		componentTreeNodeDom.innerHTML=" "+currentComponentTreeNode.text;
		
		var type;
		if(currentComponentTreeNode.type=="composite")	type="composite";
		else											type="composant";
		var statusButton=document.getElementById("startstop_component");
		if(currentComponentTreeNode.status=="STARTED")	statusButton.value="stop "+type;
		else											statusButton.value="start "+type;

		slide=new Fx.Slide('panel',{mode:'horizontal',resetHeight:true});
		panel=document.getElementById("panel");
		panel_content=document.getElementById("panel_content");
		currentMenu=currentSelectedMenuTreeNode.menu;
		refreshPanelContent();
	}
	
	function setPanelContent(selectedMenu)
	{
		if(selectedMenu==currentMenu)	return;
		modifyPanelContent(selectedMenu);
	}
	
	function refreshPanelContent()
	{
		modifyPanelContent(currentMenu);
	}
	
	function setComponentStatus()
	{
//	    parent.DHTMLXLayout.cells("b").progressOn();
	    var statusButton=document.getElementById("startstop_component");
		var method,newValue;
		if(statusButton.value==("start "+currentComponentTreeNode.type))
		{
			method="start";
			newValue="stop";
		}
		else
		{
			method="stop";
			newValue="start";
		}
		
		startStopComponent(currentComponentTreeNode.id,method);
//		parent.DHTMLXLayout.cells("b").progressOff();
	}
	
	function modifyPanelContent(selectedMenu)
	{
		if(currentMenu!=null)	document.getElementById(currentMenu).style.borderColor="#ccc";

		slide.slideOut().chain(function()
		{
			panel.removeChild(panel_content);
			panel_content=document.createElement("div");
			panel_content.id="panel_content";
			panel.appendChild(panel_content);
			
			if(currentMenu=="component")		fillComponentsPanel();
			else if(currentMenu=="service")		fillServicesPanel();
			else if(currentMenu=="reference")	fillReferencesPanel();
			else if(currentMenu=="property")	fillPropertiesPanel();
			new Fx.Accordion($('accordion'), '#accordion .title', '#accordion .content',{alwaysHide:true,show : currentSelectedMenuTreeNode.position});
			slide.slideIn();
		});
		
		currentMenu=selectedMenu;
		document.getElementById(currentMenu).style.borderColor="#646464";
	}
	
	/*******************************************Utils************************************************************/
	
	function getJSONTreeNode(treeId)
    {
    	var index,lastIndex;
		var JSONObject;

		JSONObject=parent.JSONTree;
		lastIndex=0;
		
    	while((index=treeId.indexOf("_",lastIndex))!=-1)
    	{
    		JSONObject=JSONObject.item[parseInt(treeId.substring(lastIndex,index))];
    		lastIndex=index+1;
    	}
    	
    	if(lastIndex != treeId.length)
    	{
    		JSONObject=JSONObject.item[parseInt(treeId.substring(lastIndex,treeId.length))];
    	}
    	
    	return JSONObject;
    }
	
	function getJSONParentTreeNode(treeId)
	{
		var JSONObject=parent.JSONTree;
		var lastIndex=0;
    	while((index=treeId.indexOf("_",lastIndex))!=-1)
    	{
    		JSONObject=JSONObject.item[parseInt(treeId.substring(lastIndex,index))];
    		lastIndex=index+1;
    	}
    	
    	return JSONObject;
	}
	
	function getItemIndex(treeId)
	{
		var index=treeId.lastIndexOf("_");
		if(index!=-1)
		{
			return treeId.substring(index+1,treeId.length);
		}
	}
	
	function getDomainNode(treeId)
	{
		var index=treeId.indexOf("_",0);
        if(index==-1)   return;
        index=treeId.indexOf("_",index+1);
        if(index==-1)   return;
        var domainId=treeId.substring(0, index);
        return getJSONTreeNode(domainId);
	}
	
	function getJSONComponentTreeNode(treeId)
    {
   		var index,lastIndex;
		var JSONObject,JSONObjectTmp;

		JSONObject=parent.JSONTree;
		lastIndex=0;
				
    	while((index=treeId.indexOf("_",lastIndex))!=-1)
    	{
    		JSONObjectTmp=JSONObject.item[parseInt(treeId.substring(lastIndex,index))];
    		if(JSONObjectTmp.type=="root" || JSONObjectTmp.type=="domain" || JSONObjectTmp.type=="composite" || JSONObjectTmp.type=="component")	JSONObject=JSONObjectTmp;
    		else																										return JSONObject;
    		lastIndex=index+1;
    	}
    	JSONObjectTmp=JSONObject.item[parseInt(treeId.substring(lastIndex,treeId.length))];
   		if(JSONObjectTmp.type=="root" || JSONObjectTmp.type=="domain" || JSONObjectTmp.type=="composite" || JSONObjectTmp.type=="component")		JSONObject=JSONObjectTmp;
    	
    	return JSONObject;
    }
	
	function getSelectedMenuTreeNode()
	{
		var currentTreeNode=selectedTreeNode;
		var type=currentTreeNode.type;
		var currentTreeId;
		while(type!="composite" && type!="component" && type!="reference"&& type!="service"&& type!="property")
		{
			currentTreeId=currentTreeNode.id;
			currentTreeNode=getJSONParentTreeNode(currentTreeId);
			type=currentTreeNode.type;
		}
		
		if(type=="composite")	currentTreeNode.menu="component";
		else					currentTreeNode.menu=currentTreeNode.type;
		return currentTreeNode;
	}
	
	/*A domain node just have one '_' in is id*/
	function isDomainNode(nodeId)
	{
		if(nodeId=="0_")	return false;
		var index=nodeId.indexOf("_",0);
		if(index==-1)		return false;
		index=nodeId.indexOf("_",index+1);
		if(index!=-1)		return false;
		return true;
	}
	
	function include(filename)
	{
	    var head = document.getElementsByTagName('head')[0];
	    var script = document.createElement('script');
	    script.src = filename;
	    script.type = 'text/javascript';
	    head.appendChild(script)
	}


	