 var encodedComposite,isJar,isZip;
    function fileChange()
    {
		isJar=isZip=false;
		
    	var toUpload = document.getElementById("toUploadFile");
 		var file = toUpload.files[0];

 		var extension=file.name.substring(file.name.lastIndexOf(".")+1, file.name.length).toLowerCase();
		if(extension!="jar" && extension!="zip")
		{
			alert("The file must be a jar or a zip");
			file.name="";
			return;
		}
		
		reader = new FileReader();
	    reader.readAsDataURL(file);
	    reader.onload = function(evt)
	    {
	    	var toFormat=evt.target.result;
	    	var index=toFormat.indexOf(",",toFormat);
	    	var toReturn=toFormat.substring(index+1,toFormat.length);
	    	encodedComposite=encodeURIComponent(toReturn);
	    	
	    	if(extension=="jar")
	    	{
	    		var domainId=parent.DHTMLXTree.getSelectedItemId();
	    		var compositesName=getCompositesName(domainId,encodedComposite);
		    	if(compositesName=="")
		    	{
		    		alert("The jar you provide doesn't contain composite file");
					return;
		    	}
				isJar=true;		    	
		    	var namesTab=compositesName.split("%S%");
				var compositeNameSelect=document.getElementById("compositeName")
				compositeNameSelect.options.length=0;
				for(var i=0;i<namesTab.length;i++)
				{
					compositeNameSelect.options[i]=new Option(namesTab[i],namesTab[i],false,false);
				}
				
				var compositeNameDiv=document.getElementById("compositeNameDiv");
				compositeNameDiv.style.visibility = "visible";
	    	}
	    	else
	    	{
	    		isZip=true;
	    	}
	    }
    }
    
    function upload()
    {
    	if(isJar==true || isZip==true)
    	{
		    var domainId=parent.DHTMLXTree.getSelectedItemId();
		    var newTree;
	    	if(isJar==true)
	    	{
	    		var compositeNameSelect=document.getElementById("compositeName");
			    var compositeName=compositeNameSelect.options[compositeNameSelect.selectedIndex].text;
			    uploadComposite(domainId,compositeName,encodedComposite);
	
				var compositeNameDiv=document.getElementById("compositeNameDiv");
				compositeNameDiv.style.visibility = "hidden";
	    	}
	    	else
	    	{
	    		uploadContribution(domainId,encodedComposite);
	    	}
    	}
	    return false;
    };
