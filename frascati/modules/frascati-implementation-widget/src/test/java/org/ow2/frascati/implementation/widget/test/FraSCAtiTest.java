/**
 * OW2 FraSCAti: SCA Implementation Widget
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.widget.test;

import java.net.URL;
import javax.xml.namespace.QName;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest {

    FraSCAti frascati;
    CompositeManager compositeManager;

    @Before
    public void initFraSCAti() throws Exception {
      frascati = FraSCAti.newFraSCAti();
      compositeManager = frascati.getCompositeManager();
    }

    /**
     * Check errors produced during the checking phase.
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processCheckingErrorsWarningsComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        compositeManager.processComposite(new QName("CheckingErrorsWarnings"), processingContext);
      } catch(ManagerException me) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrorsWarnings.composite'
    	assertEquals("The number of checking errors", 6, processingContext.getErrors());
        // Let's note that the file 'CheckingErrorsWarnings.composite' produces 2 warnings.
        assertEquals("The number of checking warnings", 2, processingContext.getWarnings());
      }
    }

    /**
     * Test an example.
     */
    @Test
    public void processExample1Composite() throws Exception
    {
      // Get the example1 composite.
      frascati.getComposite("example1");

      Thread.sleep(1000);

      // Try to access an implementation widget.
      new URL("http://localhost:18000/pages/index.html").openConnection();
    }
}
