/**
 * OW2 FraSCAti SCA Implementation Widget
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.widget.binding.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 *
 */
public class JaxRsUtils
{
    private static Logger logger = Logger.getLogger(JaxRsUtils.class.getName());
    
    public static boolean isMethodAnnotation(Class<? extends Annotation> annotationType)
    {
        return annotationType.isAssignableFrom(GET.class) || annotationType.isAssignableFrom(POST.class) ||annotationType.isAssignableFrom(DELETE.class) ||annotationType.isAssignableFrom(PUT.class);
    }
    
    public static int getNbParametersType(Method method,Class<? extends Annotation> parameterClass)
    {
        int nbParametersType=0;
        Annotation[][] paramsAnnotations=method.getParameterAnnotations();
        Annotation[] paramAnnotations = null;
        logger.fine("getParameter "+method.getName()+" parameterclass "+parameterClass);
        for(int i=0;i<paramsAnnotations.length; i++)
        {
            paramAnnotations=paramsAnnotations[i];
            logger.fine("parameter"+i+" nb parameters : "+paramAnnotations.length);
            for(Annotation annotation : paramAnnotations)
            {
                logger.fine(annotation.toString());
                if(annotation.annotationType().isAssignableFrom(parameterClass))
                {
                    nbParametersType++;
                }
            }
        }
        return nbParametersType;
    }
    
    public static<T extends Annotation> T isParameterType(Method method, int index, Class<T> annotationType)
    {
        Annotation[][] paramsAnnotations=method.getParameterAnnotations();
        Annotation[] paramAnnotations = paramsAnnotations[index];
        for(Annotation annotation : paramAnnotations)
        {
            logger.fine("annotation.annotationType() "+annotation.annotationType().getName());
            if(annotation.annotationType().isAssignableFrom(annotationType))
            {
                return annotationType.cast(annotation);
            }
        }
        return null;
    }
    
    public static int getPathParamIndex(Method method, String pathParameterValue)
    {
        Annotation[][] paramsAnnotations=method.getParameterAnnotations();
        Annotation[] paramAnnotations = null;
        PathParam pathParam;
        logger.fine("getParameter "+method.getName());
        for(int i=0;i<paramsAnnotations.length; i++)
        {
            paramAnnotations=paramsAnnotations[i];
            logger.fine("parameter"+i+" nb parameters : "+paramAnnotations.length);
            for(Annotation annotation : paramAnnotations)
            {
                logger.fine(annotation.toString());
                if(annotation.annotationType().isAssignableFrom(PathParam.class))
                {
                    pathParam=(PathParam) annotation;
                    if(pathParam.value().equals(pathParameterValue))
                    {
                        return i;
                    }
                }
            }
        }
        return -1;
    }
}
