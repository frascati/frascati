/**
 * OW2 FraSCAti: SCA Implementation Widget
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor: Gwenael Cattez
 *
 */

package org.ow2.frascati.implementation.widget;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.annotation.ElementType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import org.apache.cxf.service.invoker.Factory;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.ScaFactory;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.assembly.factory.processor.AbstractInterfaceProcessor;
import org.ow2.frascati.implementation.widget.binding.WidgetBindingProcessorItf;
import org.ow2.frascati.metamodel.web.WebPackage;
import org.ow2.frascati.metamodel.web.WidgetImplementation;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.util.reflection.ReflectionUtil;

/**
 * OW2 FraSCAti Assembly Factory implementation widget processor class.
 *
 * @author Philippe Merle - INRIA
 * @version 1.5
 */
public class FrascatiImplementationWidgetProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<WidgetImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Package where content classes are generated.
   */
  @Property(name = "package")
  private String packageGeneration;

  private List<WidgetBindingProcessorItf> widgetBindingProcessors;
  
  public List<WidgetBindingProcessorItf> getWidgetBindingProcessors()
  {
      return widgetBindingProcessors;
  }

  @Reference(name = "widgetBindingProcessors")
  public void setWidgetBindingProcessors(List<WidgetBindingProcessorItf> widgetBindingProcessors)
  {
      this.widgetBindingProcessors = widgetBindingProcessors;
  }
  
  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

/**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(WidgetImplementation widgetImplementation, StringBuilder sb)
  {
    sb.append("web:implementation.widget");
    append(sb, "location", widgetImplementation.getLocation());
    append(sb, "default", widgetImplementation.getDefault());
    super.toStringBuilder(widgetImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType, ProcessingContext)
   */
    @Override
    protected final void doCheck(WidgetImplementation widgetImplementation, ProcessingContext processingContext) throws ProcessorException
    {
        String location = widgetImplementation.getLocation();
        checkAttributeMustBeSet(widgetImplementation, "location", location, processingContext);

        boolean checkDefaultInClassLoader = true;

        // Check that location is present in the class path.
        if (!isNullOrEmpty(location) && processingContext.getClassLoader().getResourceAsStream(location) == null)
        {
            // Location not found.
            error(processingContext, widgetImplementation, "Location '" + location + "' not found");
            checkDefaultInClassLoader = false;
        }

        String default_ = widgetImplementation.getDefault();
        // checkAttributeMustBeSet(widgetImplementation, "default", default_,
        // processingContext);

        // Check that default is present in the class path.
        if (checkDefaultInClassLoader && !isNullOrEmpty(default_) && processingContext.getClassLoader().getResourceAsStream(location + '/' + default_) == null)
        {
            // Default not found.
            error(processingContext, widgetImplementation, "Default '" + default_ + "' not found");
        }

        // Get the enclosing SCA component.
        Component parentComponent = getParent(widgetImplementation, Component.class);
        List<ComponentService> services = parentComponent.getService();
        
        if (services.size() > 1)
        {
            //if component has more than one service
            // it must have at least one service with an explicit
            // Servlet interface
            boolean componentHasServletService=false;
            List<String> implementedInterfaces=new ArrayList<String>();
            JavaInterface componentServiceJavaItf;
            for(ComponentService componentService : services)
            {
                if(componentService.getInterface()!=null && (componentService.getInterface() instanceof JavaInterface))
                {
                    componentServiceJavaItf = (JavaInterface) componentService.getInterface();
                    if(Servlet.class.getName().equals(componentServiceJavaItf.getInterface()))
                    {
                        componentHasServletService=true;
                        log.fine(parentComponent.getName()+" has a Servlet service");
                    }
                    else
                    {
                        implementedInterfaces.add(componentServiceJavaItf.getInterface());
                        log.fine(parentComponent.getName()+" has a "+componentServiceJavaItf.getInterface()+" service");
                    }
                }
            }
            
            if(!componentHasServletService)
            {
                error(processingContext, widgetImplementation, "<implementation.widget> must have at least one "+Servlet.class.getName()+" service");
            }
            processingContext.putData(widgetImplementation, List.class, implementedInterfaces);
            
        } else
        {
            ComponentService service = null;
            if (services.size() == 0)
            {
                // The component has zero service then add a service to the
                // component.
                service = ScaFactory.eINSTANCE.createComponentService();
                service.setName("Widget");
                services.add(service);
            } else
            {
                // The component has one service.
                service = services.get(0);
            }
            // Get the service interface.
            Interface itf = service.getInterface();
            if (itf == null)
            {
                // The component service has no interface than add a Java
                // Servlet interface.
                JavaInterface javaInterface = ScaFactory.eINSTANCE.createJavaInterface();
                javaInterface.setInterface(Servlet.class.getName());
                service.setInterface(javaInterface);
            } else
            {
                // Check if this is a Java Servlet interface.
                boolean isServletInterface = (itf instanceof JavaInterface) ? Servlet.class.getName().equals(((JavaInterface) itf).getInterface()) : false;
                if (!isServletInterface)
                {
                    error(processingContext, widgetImplementation, "<service name=\"" + service.getName() + "\"> must have an <interface.java interface=\""
                            + Servlet.class.getName() + "\">");
                }
            }
        }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(widgetImplementation, processingContext);
  }

  
    
    
  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(WidgetImplementation widgetImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Get the parent component.
    Component component = getParent(widgetImplementation, Component.class);

    // Name of the content class.
    String contentFullClassName = null;

    if(component.getProperty().size() == 0 && component.getReference().size() == 0) {
      // if no property and no reference then use default ImplementationVelocity class.
      contentFullClassName = ImplementationWidgetComponent.class.getName();
    } else {
      String basename = widgetImplementation.getLocation() + widgetImplementation.getDefault();
      int hashCode = basename.hashCode();
      if(hashCode < 0) {
        hashCode = -hashCode;
      }

      // if some property or some reference then generate an ImplementationWidgetComponent class.
	  String contentClassName = ImplementationWidgetComponent.class.getSimpleName() + hashCode;
      // Add the package to the content class name.
      contentFullClassName = this.packageGeneration + '.' + contentClassName;

      try {
        processingContext.loadClass(contentFullClassName);
      } catch(ClassNotFoundException cnfe) {
        // If the class can not be found then generate it.
        log.info(packageGeneration + '.' + contentClassName);

        // Create the output directory for generation.
        String outputDirectory = processingContext.getOutputDirectory() + "/generated-frascati-sources";
        
        // Add the output directory to the Java compilation process.
        processingContext.addJavaSourceDirectoryToCompile(outputDirectory);

        try { // Generate a sub class of ImplementationVelocity declaring SCA references and properties.
          File packageDirectory = new File(outputDirectory + '/'
                    + packageGeneration.replace('.', '/'));
          packageDirectory.mkdirs();

          PrintStream file = new PrintStream(new FileOutputStream(new File(
                  packageDirectory, contentClassName + ".java")));

          file.println("package " + packageGeneration + ";\n");
          file.println("public class " + contentClassName);
          file.println("extends " + ImplementationWidgetComponent.class.getName());
          
          List<String> implementedInterfaces=processingContext.getData(widgetImplementation, List.class);
          if(implementedInterfaces != null && !implementedInterfaces.isEmpty())
          {
              file.print("implements ");
              for(int i=0;i<implementedInterfaces.size(); i++)
              {
                  if(i!=0)
                  {
                      file.print(", ");
                  }
                  file.print(implementedInterfaces.get(i));
              }
              file.println();
          }
          
          file.println("{");
          int index = 0;
          for (PropertyValue propertyValue : component.getProperty()) {
            // Get the property value and class.
            Object propertyValueObject = processingContext.getData(propertyValue, Object.class);
            Class<?> propertyValueClass = (propertyValueObject != null) ? propertyValueObject
                        .getClass() : String.class;
            file.println("    @" + Property.class.getName() + "(name = \""+ propertyValue.getName() + "\")");
            file.println("    protected " + propertyValueClass.getName() + " property" + index + ";");
            index++;
          }
          index = 0;
          for (ComponentReference componentReference : component.getReference()) {
            file.println("    @" + Reference.class.getName() + "(name = \""+ componentReference.getName() + "\")");
            file.println("    protected Object reference" + index + ";");
            index++;
          }

          if(implementedInterfaces != null && !implementedInterfaces.isEmpty())
          {
              file.println();
              Class<?> implementedInterfaceClass = null;
              String implementedMethodDeclaration;
              for(String implementedInterface : implementedInterfaces)
              {
                  try
                  {
                      implementedInterfaceClass = processingContext.loadClass(implementedInterface);
                  } catch (ClassNotFoundException e)
                  {
                      error(processingContext, widgetImplementation,"Interface "+implementedInterface+" can not be found");
                  }
                  
                  for(Method implementedMethod : implementedInterfaceClass.getMethods())
                  {
                      file.print("    ");
                      implementedMethodDeclaration=ReflectionUtil.generateMethodDeclaration(implementedMethod);
                      file.println(implementedMethodDeclaration);
                      file.println("    {");
                      file.println("    //TODO generated by FrascatiImplementationWidgetProcessor");
                      file.println("    }");
                      file.println();
                  }
              }
          }
          file.println("}");
          file.flush();
          file.close();
        } catch(FileNotFoundException fnfe) {
          severe(new ProcessorException(widgetImplementation, fnfe));
        }
      }
    }

    // Generate a FraSCAti SCA primitive component.
    generateScaPrimitiveComponent(widgetImplementation, processingContext, contentFullClassName);

    // Store the content class name to retrieve it from next doInstantiate method.
    processingContext.putData(widgetImplementation, String.class, contentFullClassName);  
  }

  
  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(WidgetImplementation widgetImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate a FraSCAti SCA primitive component.
    org.objectweb.fractal.api.Component component =
      instantiateScaPrimitiveComponent(widgetImplementation, processingContext,
    		      processingContext.getData(widgetImplementation, String.class));

    // Retrieve the SCA property controller of this Fractal component.
    SCAPropertyController propertyController =  (SCAPropertyController)getFractalInterface(component, SCAPropertyController.NAME);

    // Set the classloader property.
    propertyController.setValue("classloader", processingContext.getClassLoader());

    // Set the location property.
    propertyController.setValue("location", widgetImplementation.getLocation());

    // Set the default property.
    propertyController.setValue("default", widgetImplementation.getDefault());
    
  }

  private WidgetBindingProcessorItf getWidgetBindingProcessor(Binding binding)
  {
      String bindingId=binding.eClass().getName();
      for(WidgetBindingProcessorItf widgetBindingProcessor : this.widgetBindingProcessors)
      {
          if(widgetBindingProcessor.getBindingId().equals(bindingId))
          {
              return widgetBindingProcessor;
          }
      }
      return null;
  }

    /**
     * Get the first binding found for a componentReference Looks for binding
     * definition in componentReference target or composite promoter service
     * 
     * @param componentReference
     * @return Binding of the reference, null if not found
     */
    private Binding getReferenceBinding(Composite parentComposite, ComponentReference componentReference)
    {
        List<Binding> bindings;
        // look for binding definition in the componentReference bindings
        bindings = componentReference.getBinding();
        if (!bindings.isEmpty())
        {
            return bindings.get(0);
        }

        // look for binding definition in the componentReference target bindings
        bindings = componentReference.getTarget2().getBinding();
        if (!bindings.isEmpty())
        {
            return bindings.get(0);
        }

        for (org.eclipse.stp.sca.Service compositeService : parentComposite.getService())
        {
            if (componentReference.getTarget().equals(compositeService.getPromote()) && !compositeService.getBinding().isEmpty())
            {
                return compositeService.getBinding().get(0);
            }
        }
        return null;
    }
  
  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#complete(ElementType, ProcessingContext)
   */
    @Override
    protected final void doComplete(WidgetImplementation widgetImplementation, ProcessingContext processingContext) throws ProcessorException
    {
        Component parentComponent = getParent(widgetImplementation, Component.class);
        Composite parentComposite = (Composite) parentComponent.eContainer();
        List<String> javascriptClients = new ArrayList<String>();

        Binding referenceBinding;
        WidgetBindingProcessorItf referenceWidgetBindingProcessor;

        String referenceName;
        Class<?> referenceInterface;
        String referenceUri;
        String referenceJavascriptClient;

        // Traverse all references of the enclosing component of this widget
        // implementation.
        for (ComponentReference componentReference : parentComponent.getReference())
        {
            referenceBinding=this.getReferenceBinding(parentComposite, componentReference);
            if(referenceBinding==null)
            {
                processingContext.warning("No binding found for reference " + componentReference.getName());
                continue;
            }

            referenceWidgetBindingProcessor = this.getWidgetBindingProcessor(referenceBinding);
            if (referenceWidgetBindingProcessor == null)
            {
                processingContext.warning("No widgetBindingProcessor for binding " + referenceBinding.eClass().getName());
                continue;
            }

            referenceName = componentReference.getName();
            referenceInterface = AbstractInterfaceProcessor.getClass(componentReference.getInterface(), processingContext);
            referenceUri=referenceBinding.getUri();
            referenceUri = this.completeBindingURI(referenceBinding.getUri());
            referenceJavascriptClient = referenceWidgetBindingProcessor.generateJavaScriptClient(referenceName, referenceUri, referenceInterface);
            javascriptClients.add(referenceJavascriptClient);
        }

        
        
        // Retrieve the FraSCAti SCA primitive component.
        org.objectweb.fractal.api.Component component = processingContext.getData(widgetImplementation, org.objectweb.fractal.api.Component.class);

        // Retrieve the SCA property controller of this Fractal component.
        SCAPropertyController propertyController = (SCAPropertyController) getFractalInterface(component, SCAPropertyController.NAME);

        // Set the bindingInfos property.
        propertyController.setValue("javascriptClients", javascriptClients);
    }
  
  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.processor.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(WebPackage.Literals.WIDGET_IMPLEMENTATION);
  }
  
  /**
   * Complete binding URI with binding URI base.
   */
  private final String completeBindingURI(String uri)
  {
    String completedUri=uri;
    if(completedUri != null && completedUri.startsWith("/"))
    {
        completedUri = System.getProperty(AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingProcessor.BINDING_URI_BASE_DEFAULT_VALUE) + uri;
    }
    return completedUri;
  }
}
