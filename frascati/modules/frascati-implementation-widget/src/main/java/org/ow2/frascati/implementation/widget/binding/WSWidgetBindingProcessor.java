/**
 * OW2 FraSCAti SCA Implementation Widget
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.widget.binding;


/**
 *
 */
public class WSWidgetBindingProcessor extends AbstractWidgetBindingProcessor
{
    /* (non-Javadoc)
     * @see org.ow2.frascati.implementation.widget.jsclient.JavascriptWidgetBindingProcessorItf#getBindingId()
     */
    public String getBindingId()
    {
        //TODO Use org.eclipse.stp.sca.domainmodel.frascati.RestBinding
        return "WebServiceBinding";
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.implementation.widget.jsclient.AbstractJavascriptWidgetBindingProcessor#generateJavascriptClient(java.lang.StringBuilder)
     */
    @Override
    public void generateJavascriptClient(StringBuilder javascriptClientBuilder)
    {
        javascriptClientBuilder.append("<!-- the object to talk to the " + referenceName + " Web service. -->\n");
        javascriptClientBuilder.append("var " + referenceName + ";\n");
        javascriptClientBuilder.append("<!-- retrieve the JavaScript proxy client for the " + referenceName + " Web service. -->\n");
        javascriptClientBuilder.append("includeJs(\"" + uri + "?js\",\n");
        javascriptClientBuilder.append("  function() {\n");
        
        // compute the Apache CXF JavaScript proxy name.
        String simpleName = javaInterface.getSimpleName();
        // compute a prefix for the enclosing packages of this Java interface.
        String[] packages = javaInterface.getPackage().getName().split("\\.");
        StringBuilder sb = new StringBuilder();
        for (int i = packages.length - 1; i >= 0; i--)
        {
            sb.append(packages[i]);
            sb.append('_');
        }
        String prefix = sb.toString();
        String apacheCxfProxyName = prefix + '_' + simpleName + "PortType_" + prefix + '_' + simpleName + "Port";

        javascriptClientBuilder.append("    " + referenceName + " = new " + apacheCxfProxyName + "()");
        javascriptClientBuilder.append("  }\n");
        javascriptClientBuilder.append(")\n");
    }
}
