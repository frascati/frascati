/**
 * OW2 FraSCAti: SCA Implementation Widget
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.implementation.widget;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.implementation.widget.proxy.HttpProxy;
import org.ow2.frascati.util.Stream;

/**
 * OW2 FraSCAti implementation widget component class.
 *
 * @author Philippe Merle - INRIA
 * @version 1.5
 */
@Scope("COMPOSITE")
@Service(Servlet.class)
public class ImplementationWidgetComponent extends HttpServlet
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  private static final Logger logger = Logger.getLogger(ImplementationWidgetComponent.class.getName());  
  
  private static final int DEFAULT_WEBSOCKET_PORT=7766;
  
  /**
   * Mapping between file extensions and MIME types.
   */
  private static Properties extensions2mimeTypes = new Properties();
  static {
    // Load mapping between file extensions and MIME types.
    try {
      extensions2mimeTypes.load(
        ImplementationWidgetComponent.class.getClassLoader().getResourceAsStream(
          ImplementationWidgetComponent.class.getPackage().getName().replace('.', '/') + "/extensions2mimeTypes.properties"
        )
      );
    } catch(IOException ioe) {
      throw new Error(ioe);
    }
  }

  /**
   * Classloader where resources are searched.
   */
  @Property(name = "classloader")
  private ClassLoader classloader;

  /**
   * Location where resources are searched.
   */
  @Property(name = "location")
  private String location;

  /**
   * Default resource.
   */
  @Property(name = "default")
  private String defaultResource;

  /**
   * Javascript clients.
   */
  @Property(name = "javascriptClients")
  private List<String> javascriptClients;

  /**
   * Port for WebSocket communication
   */
  @Property(name = "webSocketPort")
  private int webSocketPort;
  
  /**
   * The name of the enclosing SCA component.
   */
  @ComponentName
  private String componentName;

  
  //---------------------------------------------------------------------------
  // WebSocket management methods.
  // --------------------------------------------------------------------------

    private boolean isWebSocketServerInit = false;
    private WebSocketBrokerServlet webSocketBroker;

    private void initWebSocketServer()
    {
        if (!isWebSocketServerInit)
        {
            int webSocketPort = this.webSocketPort;
            if (webSocketPort == 0)
            {
                webSocketPort = DEFAULT_WEBSOCKET_PORT;
            }
            // Run server on localhost:webSocketPort
            final Server server = new Server(webSocketPort);
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);
            webSocketBroker = new WebSocketBrokerServlet();
            context.addServlet(new ServletHolder(webSocketBroker), "/*");
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        server.start();
                        server.join();
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }).start();
            isWebSocketServerInit = true;
        }
    }
  
  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  @Override
  protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      String pathInfo = request.getPathInfo();
      if(pathInfo!=null && pathInfo.startsWith("/proxy"))
      {
          doProxyRequest(request, response);
          return;
      }
      else
      {
          super.service(request, response);
      }
  }
  
  protected void doProxyRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
  {
      try
      {
          String method=request.getMethod();
          //get parameters
          String url=request.getParameter("url");
          String contentType=request.getParameter("contentType");
          String data=request.getParameter("data");
          logger.fine("url : "+url+", method : "+method+", contentType : "+contentType+", data : "+data);
          //send request
          
          HttpProxy.sendRequest(response, url, method, contentType, data);
      }
      catch (Exception exception)
      {
          exception.printStackTrace();
          response.sendError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), exception.getMessage());
      }
  }
  
  /**
   * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
	// The requested resource.
    String pathInfo = request.getPathInfo();
    if(pathInfo == null) {
      pathInfo = defaultResource;
    }
    
    // When the JavaScript for the enclosing SCA component is requested then generate it. 
    if(('/' + componentName + ".js").equals(pathInfo)) {
      generateJavaScriptForEnclosingComponent(response);
      return;
    }

    // Search the requested resource into the class loader.
    InputStream is = this.classloader.getResourceAsStream(this.location + pathInfo);

    if(is == null) {
      // Requested resource not found.
      super.doGet(request, response);
      return;
    }

    // Requested resource found.
    response.setStatus(HttpServletResponse.SC_OK);

    // set the content type.
    int idx = pathInfo.lastIndexOf('.');
    String extension = (idx != -1) ? pathInfo.substring(idx) : "";
    String mimeType = extensions2mimeTypes.getProperty(extension);
    if(mimeType == null) {
      mimeType = "text/plain";
    }
    response.setContentType(mimeType);

    // Copy the resource stream to the servlet output stream.
    Stream.copy(is, response.getOutputStream());
  }
    
   /**
   * Generate the JavaScript for the enclosing SCA component.
   */
  protected void generateJavaScriptForEnclosingComponent(HttpServletResponse response)
    throws IOException
  {
    response.setStatus(HttpServletResponse.SC_OK);
    response.setContentType("text/javascript");
    PrintWriter pw = response.getWriter();
    
    pw.println("function includeJs(jsFilePath, callback) {");
    pw.println("  var js = document.createElement(\"script\");");
    pw.println("  js.type = \"text/javascript\";");
    pw.println("  js.onload = callback");
    pw.println("  js.src = jsFilePath;");
    pw.println("  document.head.appendChild(js);");
    pw.println("};");
    pw.println();
    
    for(String javascriptClient : this.javascriptClients)
    {
        pw.println(javascriptClient);
    }
  }
  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
