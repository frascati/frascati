/**
 * OW2 FraSCAti SCA Implementation Widget
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.widget.binding;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.ow2.frascati.implementation.widget.binding.util.JaxRsUtils;

/**
 *
 */
public class RestWidgetBindingRESTProcessor extends AbstractWidgetBindingProcessor
{
    private final static Logger logger=Logger.getLogger(RestWidgetBindingRESTProcessor.class.getName());
    private final static String REGEX_PATHPARAM_REGEX="\\{(.+):(.+)\\}";
    private final static String PATHPARAM_REGEX="\\{(.+)\\}";
    
    private String baseUri;
    
    /**
     * @see org.ow2.frascati.implementation.widget.jsclient.JavascriptWidgetBindingProcessorItf#getBindingId()
     */
    public String getBindingId()
    {
        //TODO Use org.eclipse.stp.sca.domainmodel.frascati.RestBinding
        return "RestBinding";
    }

    /**
     * @see org.ow2.frascati.implementation.widget.jsclient.AbstractJavascriptWidgetBindingProcessor#generateJavascriptClient(java.lang.StringBuilder)
     */
    @Override
    public void generateJavascriptClient(StringBuilder javascriptClientBuilder)
    {
        javascriptClientBuilder.append("<!-- the object to talk to the " + referenceName + " REST web service. -->\n");
        javascriptClientBuilder.append("var "+referenceName+" = new Object();\n");
        javascriptClientBuilder.append("includeJs(\"http://code.jquery.com/jquery-1.9.1.min.js\",function(){\n");
        
        this.baseUri=this.uri;
        Path javaInterfacePath=javaInterface.getAnnotation(Path.class);
        if(javaInterfacePath!=null)
        {
            this.baseUri+=((Path)javaInterfacePath).value();
        }

        /**proceed methods*/
        for(Method method : this.javaInterface.getMethods())
        {
            logger.fine("method "+method.getName());
            proceedJavaMethod(javascriptClientBuilder, method);
        }
        
        javascriptClientBuilder.append("});\n");
    }

    public void proceedJavaMethod(StringBuilder js, Method method)
    {
        logger.fine("proceed java method "+method.getName());

        String httpMethod = null;
        for(Annotation annotation : method.getAnnotations())
        {
            if(JaxRsUtils.isMethodAnnotation(annotation.annotationType()))
            {
                httpMethod=annotation.annotationType().getSimpleName();
                logger.fine("httpMethod "+httpMethod);
            }
        }
        
        if(httpMethod==null)
        {
            logger.warning("No JAX-RS method found for method "+method.getName());
            return;
        }
        
        String methodName=referenceName+"."+method.getName();
        js.append(methodName+"=function(");
        int nbParameters=method.getParameterTypes().length;
        logger.fine("nbParameters "+nbParameters);
        String consoleLog="console.log(";
        if(nbParameters>0)
        {
            js.append("param"+0);
            consoleLog+="param"+0;
            for(int i=1;i<method.getParameterTypes().length;i++)
            {
                js.append(",param"+i);
                consoleLog+=",param"+i;
            }
        }
        js.append(")\n{\n");
        consoleLog+=");";
        /**add HTTP method of the java method*/
        js.append("\t"+consoleLog+"\n");
        js.append("\tvar JQueryRequest=jQuery.ajax({\n");
        js.append("\t\t type : \""+httpMethod+"\",\n");
        js.append("\t\t url : \"proxy\",\n");
        js.append("\t\t data : {\n");
        
        js.append("\t\t\t url : ");
        proceedMethodURL(js, method);
        js.append(",\n");
        
        js.append("\t\t\t contentType : ");
        proceedMethodContentType(js, method);
        js.append(",\n");
        
        js.append("\t\t\t data : ");
        proceedMethodFormParams(js, method);
        js.append(",\n");
        js.append("\t\t\t}\n");
        js.append("\t });\n");

        js.append("\t JQueryRequest.done(function(data, textStatus, jqXHR) {\n");
        js.append("\t\t if (typeof("+methodName+"Done) != \"undefined\"){\n");
        js.append("\t\t\t "+methodName+"Done(data);\n");
        js.append("\t\t } else {\n");
        js.append("\t\t\t console.log(\"no callback function found for method "+methodName+" done\");\n");
        js.append("\t\t }\n");
        js.append("\t});\n");
        
        js.append("\t JQueryRequest.fail(function(jqXHR, textStatus, errorThrown) {\n");
        js.append("\t\t if (typeof("+methodName+"Fail) != \"undefined\"){\n");
        js.append("\t\t\t "+methodName+"Fail(errorThrown);\n");
        js.append("\t\t } else {\n");
        js.append("\t\t\t console.log(\"no callback function found for method "+methodName+" fail\");\n");
        js.append("\t\t }\n");
        js.append("\t});\n");
        
        js.append("};\n");
    }
    
    public void proceedMethodURL(StringBuilder js, Method method)
    {
        UriBuilder uriBuilder=UriBuilder.fromUri(this.baseUri);
        
        QueryParam queryParamAnnotation;
        for(int i=0;i<method.getParameterTypes().length; i++)
        {
            queryParamAnnotation =JaxRsUtils.isParameterType(method, i, QueryParam.class);
            if(queryParamAnnotation!=null)
            {
                uriBuilder.queryParam(queryParamAnnotation.value(), "\"+param"+i+"+\"");
            }
        }

        List<String> pathParams = new ArrayList<String>();
        Path path = method.getAnnotation(Path.class);
        if (path != null)
        {
            uriBuilder.path(path.value());
            String pathParamValue, pathParamRegex, pathParamReplacement;
            int pathParamIndex;
            for (String pathElement : path.value().split("/"))
            {
                if (pathElement.matches(REGEX_PATHPARAM_REGEX))
                {
                    //we found a path param defined as a regex
                    pathParamValue = pathElement.replaceAll(REGEX_PATHPARAM_REGEX, "$1");
                    pathParamValue=pathParamValue.trim();
                    logger.fine("pathParamValue "+pathParamValue);
                    pathParamIndex = JaxRsUtils.getPathParamIndex(method, pathParamValue);
                    pathParamRegex = pathElement.replaceAll(REGEX_PATHPARAM_REGEX, "$2");
                    logger.fine("pathParamRegex "+pathParamRegex);
                    pathParamReplacement = pathParamRegex.trim();
                    logger.fine("pathParamReplacement "+pathParamReplacement);
                    pathParamReplacement = pathParamReplacement.replaceAll("\\\\", "");
                    logger.fine("pathParamReplacement \\"+pathParamReplacement);
                    //TODO improve regex replacemement
                    pathParamReplacement = pathParamReplacement.replaceFirst("\\(.+\\)", "\"+param" + pathParamIndex + "+\"");
                    logger.fine("pathParamReplacement replace "+pathParamReplacement);
                    pathParams.add(pathParamReplacement);
                }
                else if (pathElement.matches(PATHPARAM_REGEX))
                {
                    // we found a path param
                    pathParamValue = pathElement.replaceAll(PATHPARAM_REGEX, "$1");
                    pathParamValue=pathParamValue.trim();
                    logger.fine("pathParamValue "+pathParamValue);
                    pathParamIndex = JaxRsUtils.getPathParamIndex(method, pathParamValue);
                    pathParamReplacement = "\"+param" + pathParamIndex + "+\"";
                    pathParams.add(pathParamReplacement);
                }
            }
        }
  
        URI methodURI=uriBuilder.build(pathParams.toArray());
        String methodURIString="\"";
        methodURIString += methodURI.toASCIIString();
        methodURIString=methodURIString.replaceAll("%22", "\"");
        methodURIString=methodURIString.replaceAll("%2B", "+");
        if(methodURIString.endsWith("+\""))
        {
            methodURIString=methodURIString.substring(0, methodURIString.length()-2);
        }
        else
        {
            methodURIString+="\"";
        }
        js.append(methodURIString);

    }
    
    public void proceedMethodContentType(StringBuilder js, Method method)
    {
        Consumes consumesAnnotation=method.getAnnotation(Consumes.class);
        String contentType=MediaType.TEXT_PLAIN;
        if(consumesAnnotation!=null)
        {
            contentType=consumesAnnotation.value()[0];
        }
        js.append("\""+contentType+"\"");
    }
    
    public void proceedMethodFormParams(StringBuilder js, Method method)
    {
        FormParam formParamAnnotation;
        int nbParams = 0;
        for (int i = 0; i < method.getParameterTypes().length; i++)
        {
            formParamAnnotation = JaxRsUtils.isParameterType(method, i, FormParam.class);
            if (formParamAnnotation != null)
            {
                if (nbParams == 0)
                {
                    js.append("\"");
                } else
                {
                    js.append("+\"&");
                }

                js.append(formParamAnnotation.value() + " = \"+param" + i);
                nbParams++;
            }
        }

        if (nbParams == 0)
        {
            js.append("\"\"");
        }
    }
}
