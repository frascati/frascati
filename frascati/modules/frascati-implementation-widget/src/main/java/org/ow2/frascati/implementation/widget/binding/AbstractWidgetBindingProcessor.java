/**
 * OW2 FraSCAti SCA Implementation Widget
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.widget.binding;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 *
 */
public abstract class AbstractWidgetBindingProcessor implements WidgetBindingProcessorItf
{
    /**
    * Name of the SCA reference.
    */
   protected String referenceName;

   /**
    * Java interface of the SCA reference.
    */
   protected Class<?> javaInterface;

   /**
    * URI where the SCA service is bound.
    */
   protected String uri;
    
    public String generateJavaScriptClient(String referenceName, String uri, Class<?> javaInterface)
    {
        this.referenceName=referenceName;
        this.uri=uri;
        this.javaInterface=javaInterface;
        StringBuilder javascriptClientBuilder=new StringBuilder();

        javascriptClientBuilder.append("<!-- OW2 FraSCAti SCA Implementation Widget -->\n");
        javascriptClientBuilder.append("<!-- Copyright (C) 2013 Inria, University of Lille 1 -->\n");
        javascriptClientBuilder.append("<!-- Generated "+this.getBindingId()+" JavaScript Client for SCA reference "+this.referenceName+", uri="+this.uri+"-->\n\n");

        this.generateJavascriptClient(javascriptClientBuilder);
        
        javascriptClientBuilder.append("\n<!---------------------------------->\n\n");
        return javascriptClientBuilder.toString();
        
    }
    
    public abstract void generateJavascriptClient(StringBuilder javascriptClientBuilder);
    
    protected static String getBindingId(EObject bindingObject)
    {
        EClass eclass=bindingObject.eClass();
        return eclass.getEPackage().getNsURI() + "#" + eclass.getName();
    }
}
