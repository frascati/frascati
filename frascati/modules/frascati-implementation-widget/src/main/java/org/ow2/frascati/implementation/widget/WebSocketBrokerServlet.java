/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.widget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jetty.websocket.WebSocket;
import org.eclipse.jetty.websocket.WebSocketServlet;

/**
 *
 */
public class WebSocketBrokerServlet extends WebSocketServlet
{
    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(WebSocketBrokerServlet.class.getName());  
    
    private List<FraSCAtiServiceSocket> members=new ArrayList<FraSCAtiServiceSocket>();
    
    /**
     * @see org.eclipse.jetty.websocket.WebSocketFactory.Acceptor#doWebSocketConnect(javax.servlet.http.HttpServletRequest, java.lang.String)
     */
    public WebSocket doWebSocketConnect(HttpServletRequest arg0, String arg1)
    {
        return new FraSCAtiServiceSocket();
    }

    public void notify(Object objectToSend)
    {
        for(FraSCAtiServiceSocket fraSCAtiServiceSocket : this.members)
        {
            try
            {
                fraSCAtiServiceSocket.sendMessage(objectToSend.toString());
            } catch (IOException e)
            {
                logger.warning("IOException when writing on a socket");
            }
        }
    }
    
    public class FraSCAtiServiceSocket implements WebSocket.OnTextMessage
    {
        private Connection currentConnection;

        public void onClose(int closeCode, String message)
        {
            members.remove(this);
        }

        public void sendMessage(String data) throws IOException
        {
            currentConnection.sendMessage(data);
        }

        public void onMessage(String data)
        {
            logger.info("Received: " + data);
        }

        public boolean isOpen()
        {
            return currentConnection.isOpen();
        }

        public void onOpen(Connection connection)
        {
            members.add(this);
            currentConnection = connection;
            try
            {
                connection.sendMessage("Server received Web Socket upgrade and added it to Receiver List.");
                logger.info("Server received Web Socket upgrade and added it to Receiver List.");
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
