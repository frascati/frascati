/**
 * OW2 FraSCAti Component Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle, Christophe Demarey
 *
 */

package org.ow2.frascati.component.factory.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.util.Fractal;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext.MembraneDescription;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.component.factory.api.MembraneProvider;
import org.ow2.frascati.component.factory.api.TypeFactory;
import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * OW2 FraSCAti Component Factory component acting as proxy between
 * OW2 FraSCAti Assembly Factory and Fractal providers.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class ComponentFactoryImpl
     extends AbstractScopeCompositeLoggeable
  implements ComponentFactory,
             MembraneGeneration,
             TypeFactory
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Property for the name of the scaPrimitive membrane.
   */
  @Property(name = "sca-primitive-membrane")
  private String scaPrimitiveMembrane = "scaPrimitive";

  /**
   * Property for the name of the scaComposite membrane.
   */
  @Property(name = "sca-composite-membrane")
  private String scaCompositeMembrane = "scaComposite";

  /**
   * Property for the name of the scaContainer membrane.
   */
  @Property(name = "sca-container-membrane")
  private String scaContainerMembrane = "scaContainer";

  /**
   * Reference to Fractal bootstrap providers. 
   */
  @Reference(name = "fractal-bootstrap-class-providers")
  private List<MembraneProvider> fractalBootstrapClassProviders = new ArrayList<MembraneProvider>();

  /**
   * Reference to the membrane generation plugin.
   */
  @Reference(name = "delegate-membrane-generation", required=false)
  private MembraneGeneration membraneGeneration;

  /**
   * Used Fractal Type Factory interface.
   */
  private org.objectweb.fractal.api.type.TypeFactory typeFactory;

  /**
   * All used Generic Factories.
   */
  private Map<String, GenericFactory> genericFactories = new HashMap<String, GenericFactory>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Initialization.
   */
  @Init
  public final void initialization() throws FactoryException
  {
    logDo("initialization");

    for(MembraneProvider membraneProvider : fractalBootstrapClassProviders) {
  	  String membraneDescription = membraneProvider.getMembraneDescription();

  	  Component fractalBootstrapComponent;
      String msg = "create the " + membraneDescription + " bootstrap component";
      try {
    	Class<?> membraneClass = membraneProvider.getMembraneClass();
        HashMap<String, Object> hints = new HashMap<String, Object>();
        hints.put("fractal.provider", membraneClass.getCanonicalName());
        // use the current thread's context class loader where FraSCAti is loaded
        // to load Fractal bootstrap classes instead of the class loader
        // where Fractal API was loaded.
        hints.put("classloader", FrascatiClassLoader.getCurrentThreadContextClassLoader());
  	    logDo(msg);
        fractalBootstrapComponent = Fractal.getBootstrapComponent(hints);
        logDone(msg);
      } catch (InstantiationException ie) {
        severe(new FactoryException("Cannot " + msg, ie));
        return;
      }

      // FIXME: If TypeFactory is null or genericFactories.size() > 0
      // then a NullPointerException will be thrown later.
      // Thanks to Re�mi Melisson for pointing this issue out.

      GenericFactory genericFactory = null;
      try {
          genericFactory = Fractal.getGenericFactory(fractalBootstrapComponent);
      } catch(NoSuchInterfaceException nsie) {
        severe(new FactoryException("Cannot get the GenericFactory interface of the " + membraneDescription + " Fractal bootstrap component", nsie));
        return;
      }

      // Associate the generic factory to all membrane names supported by this membrane provider.
      for(String membraneName : membraneProvider.getMembraneNames()) {
        this.genericFactories.put(membraneName, genericFactory);
      }

      if(this.typeFactory == null) {
        try {
          // Get Type Factory which allows to create Fractal interface and component types.
          this.typeFactory = Fractal.getTypeFactory(fractalBootstrapComponent);
        } catch(NoSuchInterfaceException nsie) {
          severe(new FactoryException("Cannot get the TypeFactory interface of the " + membraneDescription + " bootstrap component", nsie));
          return;
        }
      }
    }

	logDone("initialization");
  }

  /**
   * @see ComponentFactory#generateMembrane(ComponentFactoryContext, ComponentType, String, String)
   */
  public final void generateMembrane(ComponentFactoryContext context, ComponentType componentType, String membraneDesc,
      String contentClass) throws FactoryException
  {
	String logMessage = "generating membrane componentType='" + componentType + "' membraneDesc='" + membraneDesc + "' contentClass='" + contentClass + "'";
	logDo(logMessage);

    if(componentType == null) {
      componentType = createComponentType(new InterfaceType[0]);
    }

	MembraneDescription md = new MembraneDescription();
    md.componentType = componentType;
    md.membraneDesc = membraneDesc;
    md.contentDesc = contentClass;
    context.addMembraneToGenerate(md);

	logDone(logMessage);
  }

  /**
   * @see ComponentFactory#generateScaPrimitiveMembrane(ComponentFactoryContext, ComponentType, String)
   */
  public final void generateScaPrimitiveMembrane(ComponentFactoryContext context, ComponentType componentType, String classname)
      throws FactoryException
  {
    generateMembrane(context, componentType, this.scaPrimitiveMembrane, classname);
  }

  /**
   * @see ComponentFactory#generateScaCompositeMembrane(ComponentFactoryContext, ComponentType)
   */
  public final void generateScaCompositeMembrane(ComponentFactoryContext context, ComponentType componentType)
      throws FactoryException
  {
    generateMembrane(context, componentType, this.scaCompositeMembrane, null);
  }

  /**
   * @see ComponentFactory#createComponent(ComponentFactoryContext, ComponentType, String, Object)
   */
  public final Component createComponent(ComponentFactoryContext context, ComponentType componentType, String membraneDesc,
      Object contentClass) throws FactoryException
  {
    if(componentType == null) {
	  componentType = createComponentType(new InterfaceType[0]);
	}

    String logMessage = "create component componentType='" + componentType + "' membraneDesc='" + membraneDesc + "' contentClass='" + contentClass + "'";
    logDo(logMessage);

    GenericFactory genericFactory = this.genericFactories.get(membraneDesc);
    if(genericFactory == null) {
      severe(new FactoryException("No generic factory can " + logMessage));
      return null;
    }

    Component component = null;
    try {
      if("scaPrimitive".equals(membraneDesc) && contentClass instanceof String) {
        String className = (String)contentClass;
        // Load the content class.
        Class<?> clazz = null;
        try {
          clazz = context.loadClass(className);
        } catch(ClassNotFoundException cnfe) {
          severe(new FactoryException("Cannot load class '" + className + "'", cnfe));
        }
        // Is this content class annotated with @org.objectweb.fractal.fraclet.extensions.Membrane?
        Membrane membrane = clazz.getAnnotation(Membrane.class);
        if(membrane != null) {
          // The content class is annotated with @Membrane.
          // Is its 'controllerDesc' attribute defined?
          Class<?> controllerDesc = membrane.controllerDesc();
          if( ! controllerDesc.equals(Constants.class) ) {
        	// Yes, then this controlledDesc is annotated with @Membrane.
            membrane = controllerDesc.getAnnotation(Membrane.class);
          }
          // Change the membraneDesc to use to instantiate the component.
          membraneDesc = membrane.desc();
        }
      }

      // Instantiate the component.
      Object[] controllerDesc = new Object[] { context.getClassLoader(), membraneDesc };
      component = genericFactory.newFcInstance(componentType, controllerDesc, contentClass);
    } catch (InstantiationException ie) {
      throw new FactoryException("Error when " + logMessage, ie);
    }

    logDone(logMessage);
    return component;
  }

  /**
   * @see ComponentFactory#createScaPrimitiveComponent(ComponentFactoryContext, ComponentType, String)
   */
  public final Component createScaPrimitiveComponent(ComponentFactoryContext context, ComponentType componentType, String classname)
      throws FactoryException
  {
    return createComponent(context, componentType, this.scaPrimitiveMembrane, classname);
  }

  /**
   * @see ComponentFactory#createScaCompositeComponent(ComponentFactoryContext, ComponentType)
   */
  public final Component createScaCompositeComponent(ComponentFactoryContext context, ComponentType componentType)
      throws FactoryException
  {
    return createComponent(context, componentType, this.scaCompositeMembrane, null);
  }

  /**
   * @see ComponentFactory#createScaContainer(ComponentFactoryContext)
   */
  public final Component createScaContainer(ComponentFactoryContext context)
      throws FactoryException
  {
    return createComponent(context, null, this.scaContainerMembrane, null);
  }

  /**
   * @see MembraneGeneration#open(ComponentFactoryContext)
   */
  public final void open(ComponentFactoryContext context) throws FactoryException
  {
	logDo("Open a generation phase");
	// Delegate to the plugged membrane generation if bound.
	if(membraneGeneration != null) {
      membraneGeneration.open(context);
    }
	logDone("Open a generation phase");
  }

  /**
   * @see MembraneGeneration#close(ComponentFactoryContext)
   */
  public final void close(ComponentFactoryContext context) throws FactoryException
  {
	logDo("Close a generation phase");
	// Delegate to the plugged membrane generation if bound.
    if(membraneGeneration != null) {
      membraneGeneration.close(context);
    }
	logDone("Close a generation phase");
  }

  /**
   * @see TypeFactory#createInterfaceType(String, String, boolean, boolean, boolean)
   */
  public final InterfaceType createInterfaceType(String arg0, String arg1, boolean arg2,
      boolean arg3, boolean arg4) throws FactoryException
  {
    try {
      logDo("Create interface type");
      InterfaceType interfaceType = this.typeFactory.createFcItfType(arg0, arg1, arg2, arg3, arg4);
      logDone("Create interface type");
      return interfaceType;
    } catch (InstantiationException ie) {
      severe(new FactoryException("Error while creating a Fractal interface type", ie));
      return null;
    }
  }

  /**
   * @see TypeFactory#createComponentType(InterfaceType)
   */
  public final ComponentType createComponentType(InterfaceType[] arg0)
      throws FactoryException
  {
    try {
      logDo("Create component type");
      ComponentType componentType = this.typeFactory.createFcType(arg0);
      logDone("Create component type");
      return componentType;
    } catch (InstantiationException ie) {
      severe(new FactoryException("Error while creating a Fractal component type", ie));
      return null;
    }
  }

}
