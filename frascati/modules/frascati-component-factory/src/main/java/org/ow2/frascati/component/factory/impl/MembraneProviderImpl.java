/**
 * OW2 FraSCAti Component Factory
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.component.factory.impl;

import org.osoa.sca.annotations.Property;

import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;
import org.ow2.frascati.component.factory.api.MembraneProvider;

/**
 * OW2 FraSCAti membrane provider implemementation class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.4
 */
public class MembraneProviderImpl
     extends AbstractScopeCompositeLoggeable
  implements MembraneProvider
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Property for membrane description.
   */
  @Property(name = "membrane-description")
  private String membraneDescription;

  /**
   * Property for membrane names.
   */
  @Property(name = "membrane-names")
  private String membraneNames;

  /**
   * Property for membrane class.
   */
  @Property(name = "membrane-class")
  private Class<?> membraneClass;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see MembraneProvider#getMembraneNames()
   */
  public String[] getMembraneNames()
  {
    return this.membraneNames.split(":");
  }

  /**
   * @see MembraneProvider#getMembraneDescription()
   */
  public String getMembraneDescription()
  {
    return this.membraneDescription;
  }

  /**
   * @see MembraneProvider#getMembraneClass()
   */
  public Class<?> getMembraneClass()
  {
    return this.membraneClass;
  }
}
