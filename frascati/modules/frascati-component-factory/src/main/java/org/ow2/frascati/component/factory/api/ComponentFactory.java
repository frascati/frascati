/**
 * OW2 FraSCAti: Component Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.component.factory.api;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;

import org.osoa.sca.annotations.Service;

/**
 * OW2 FraSCAti Component Factory interface.
 * 
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
@Service
public interface ComponentFactory
{
  /**
   * Generates a component membrane.
   *
   * @throws FactoryException
   * @since 1.1
   */
  void generateMembrane(ComponentFactoryContext context, ComponentType componentType, String membraneDesc,
      String contentClass) throws FactoryException;

  /**
   * Generates an SCA primitive component membrane.
   *
   * @throws FactoryException
   * @since 1.3
   */
  void generateScaPrimitiveMembrane(ComponentFactoryContext context, ComponentType componentType, String classname)
      throws FactoryException;

  /**
   * Generates an SCA composite instance.
   *
   * @throws FactoryException
   * @since 1.3
   */
  void generateScaCompositeMembrane(ComponentFactoryContext context, ComponentType componentType)
      throws FactoryException;

  /**
   * Creates a component instance.
   *
   * @return created component instance.
   * @throws FactoryException
   * @since 1.1
   */
  Component createComponent(ComponentFactoryContext context, ComponentType componentType, String membraneDesc,
      Object contentClass) throws FactoryException;

  /**
   * Creates an SCA primitive component instance.
   *
   * @return created primitive component instance.
   * @throws FactoryException
   * @since 1.3
   */
  Component createScaPrimitiveComponent(ComponentFactoryContext context, ComponentType componentType, String classname)
      throws FactoryException;

  /**
   * Creates an SCA composite component instance.
   *
   * @return created composite component instance.
   * @throws FactoryException
   * @since 1.3
   */
  Component createScaCompositeComponent(ComponentFactoryContext context, ComponentType componentType)
      throws FactoryException;

  /**
   * Generates an SCA container instance.
   *
   * @throws FactoryException
   * @since 1.4
   */
  Component createScaContainer(ComponentFactoryContext context)
      throws FactoryException;

}
