/**
 * OW2 FraSCAti Component Factory
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.component.factory.impl;

import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.util.context.ClassLoaderContextImpl;

/**
 * OW2 FraSCAti Component Factory context implementation class.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class ComponentFactoryContextImpl
     extends ClassLoaderContextImpl
  implements ComponentFactoryContext
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Output directory.
   */
  private String outputDirectory;

  /**
   * List of Java source directories to compile.
   */
  private List<String> javaSourceDirectoryToCompile = new ArrayList<String>();

  /**
   * List of membranes to generate.
   */
  private List<MembraneDescription> membranesToGenerate = new ArrayList<MembraneDescription>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs with a new default class loader.
   */
  public ComponentFactoryContextImpl()
  {
    super();
  }

  /**
   * Constructs with a class loader.
   *
   * @param classLoader the class loader of the parsing context.
   */
  public ComponentFactoryContextImpl(ClassLoader classLoader)
  {
    super(classLoader);
  }

  /**
   * @see ComponentFactoryContext#getOutputDirectory()
   */
  public String getOutputDirectory()
  {
    return this.outputDirectory;
  }

  /**
   * @see ComponentFactoryContext#setOutputDirectory(String)
   */
  public void setOutputDirectory(String path)
  {
    this.outputDirectory = path;
  }

  /**
   * @see ComponentFactoryContext#addJavaSourceDirectoryToCompile(String)
   */
  public void addJavaSourceDirectoryToCompile(String path)
  {
    // Only store Java source path not already registered.
    if(!this.javaSourceDirectoryToCompile.contains(path)) {
      this.javaSourceDirectoryToCompile.add(path);
    }	  
  }

  /**
   * @see ComponentFactoryContext#getJavaSourceDirectoryToCompile()
   */
  public String[] getJavaSourceDirectoryToCompile()
  {
    return this.javaSourceDirectoryToCompile.toArray(new String[this.javaSourceDirectoryToCompile.size()]);
  }

  /**
   * @see ComponentFactoryContext#addMembraneToGenerate(MembraneDescription)
   */
  public void addMembraneToGenerate(MembraneDescription md)
  {
    this.membranesToGenerate.add(md);
  }

  /**
   * @see ComponentFactoryContext#getMembraneToGenerate()
   */
  public MembraneDescription[] getMembraneToGenerate()
  {
    return this.membranesToGenerate.toArray(new MembraneDescription[this.membranesToGenerate.size()]);
  }
}
