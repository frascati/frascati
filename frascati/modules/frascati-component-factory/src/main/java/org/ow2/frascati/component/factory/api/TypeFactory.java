/**
 * OW2 FraSCAti Component Factory
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.component.factory.api;

import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;

import org.osoa.sca.annotations.Service;

/**
 * OW2 FraSCAti type factory interface.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
@Service
public interface TypeFactory
{
  /**
   * Create a Fractal InterfaceType.
   * @return created component interface type.
   * @throws FactoryException
   * @since 1.1
   */
  InterfaceType createInterfaceType(String arg0, String arg1, boolean arg2,
      boolean arg3, boolean arg4) throws FactoryException;

  /**
   * Create a Fractal ComponentType.
   * @return created component type.
   * @throws FactoryException
   * @since 1.1
   */
  ComponentType createComponentType(InterfaceType[] arg0) throws FactoryException;

}
