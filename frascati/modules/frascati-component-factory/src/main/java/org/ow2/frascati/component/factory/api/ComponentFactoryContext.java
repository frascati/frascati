/**
 * OW2 FraSCAti: Component Factory
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.component.factory.api;

import org.ow2.frascati.util.context.ClassLoaderContext;

/**
 * OW2 FraSCAti Component Factory context interface.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public interface ComponentFactoryContext extends ClassLoaderContext
{
    /**
     * Get the output directory.
     *
     * @return The output directory.
     */
    String getOutputDirectory();

    /**
     * Set the output directory.
     *
     * @param path The output directory.
     */
    void setOutputDirectory(String path);

    /**
     * Add a Java source directory to compile.
     *
     * @param path The Java source directory to compile.
     */
    void addJavaSourceDirectoryToCompile(String path);

    /**
     * Get all Java source directories to compile.
     *
     * @return All Java source directories to compile.
     */
    String[] getJavaSourceDirectoryToCompile();

    /**
     * Add a membrane to generate.
     *
     * @param md The membrane to generate.
     */
    void addMembraneToGenerate(MembraneDescription md);

    /**
     * Get all membranes to generate.
     *
     * @return All membranes to generate.
     */
    MembraneDescription[] getMembraneToGenerate();

    /**
     * Description of a membrane.
     *
     * @author Philippe Merle at Inria
     * @since 1.6
     */
    class MembraneDescription
    {
        public org.objectweb.fractal.api.type.ComponentType componentType;
        public String membraneDesc;
        public Object contentDesc;
    }
}
