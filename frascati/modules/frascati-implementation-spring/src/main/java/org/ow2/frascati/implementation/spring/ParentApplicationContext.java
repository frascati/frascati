/**
 * OW2 FraSCAti: SCA Implementation Spring
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.spring;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Locale;
import java.util.Map;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.util.AbstractFractalLoggeable;
import org.ow2.frascati.util.FrascatiException;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.core.io.Resource;

/**
 * Is a Spring {@link ApplicationContext} where beans are obtained by
 * requesting internal interfaces of a Fractal component.
 *
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 */

public class ParentApplicationContext
     extends AbstractFractalLoggeable<FrascatiException>
  implements ApplicationContext
{
  // ======================================================================
  //
  // Internal state.
  //
  // ======================================================================

  /**
   * The Fractal component where to search Spring beans.
   */
  private Component component;

  // ======================================================================
  //
  // Internal method.
  //
  // ======================================================================

  /**
   * @see AbstractFractalLoggeable#newException(String)
   */
  @Override
  protected final FrascatiException newException(String message) {
    return new FrascatiException(message);
  }

  // ======================================================================
  //
  // Constructor.
  //
  // ======================================================================

  /**
   * Creates a {@link ParentApplicationContext} instance using a Fractal {@link Component}.
   *
   * @param component The Fractal {@link Component} to use.
   */
  public ParentApplicationContext(final Component component)
  {
    this.component = component;
  }

  // ======================================================================
  //
  // Implementation of org.springframework.context.ApplicationContext
  //
  // ======================================================================

  /**
   * @see ApplicationContext#getId()
   */
  public final String getId ()
  {
    log.finer("Spring parent context - getId called");
    return null;
  }

  /**
   * @see ApplicationContext#getBean(String)
   */
  public final Object getBean (final String name)
  {
    return getBean(name, (Class)null);
  }

  /**
   * @see ApplicationContext#getBean(String,Object[])
   */
  public final Object getBean (final String name, final Object[] args)
  {
    return getBean(name, (Class)null);
  }

  /**
   * @see ApplicationContext#getBean(String,Class)
   */
  public final Object getBean (final String name, final Class requiredType)
  {
    log.finer("Spring parent context - getBean called for name: " + name);

    // Try to find the requested Spring bean as an internal interface of the Fractal component.
    try {
      Object bean = getFractalInternalInterface(component, name);
      // TODO: Check if bean is instance of requiredType!
      return bean;
    } catch(FrascatiException fe) {
    }

    // TODO: The requested bean is perhaps a property from the SCA composite file.

    // When not found then throw an exception.
    throw new NoSuchBeanDefinitionException("Unable to find Bean with name " + name);
  }

  /**
   * @see ApplicationContext#containsBean(String)
   */
  public final boolean containsBean (final String name)
  {
    log.finer("Spring parent context - containsBean called for name: " + name);
    return false;
  }

  /**
   * @see ApplicationContext#isSingleton(String)
   */
  public final boolean isSingleton (final String name)
  {
    log.finer("Spring parent context - isSingleton called for name: " + name);
    return false;
  }

  /**
   * @see ApplicationContext#isTpeMatch(String,Class)
   */
  public final boolean isTypeMatch (final String name, final Class targetType)
  {
    log.finer("Spring parent context - isTypeMatch called for name: " + name);
    throw new UnsupportedOperationException();
  }

  /**
   * @see ApplicationContext#getType(String)
   */
  public final Class getType (final String name)
  {
    log.finer("Spring parent context - getType called for name: " + name);
    return null;
  }

  /**
   * @see ApplicationContext#getAliases(String)
   */
  public final String[] getAliases (final String name)
  {
    log.finer("Spring parent context - getAliases called for name: " + name);
    return new String[0];
  }

  /**
   * @see ApplicationContext#getParent()
   */
  public final ApplicationContext getParent ()
  {
    log.finer("Spring parent context - getParent called");
    return null;
  }

  /**
   * @see ApplicationContext#getAutowireCapableBeanFactory()
   */
  public final AutowireCapableBeanFactory getAutowireCapableBeanFactory ()
  {
    log.finer("Spring parent context - getAutowireCapableBeanFactory called");
    return null;
  }

  /**
   * @see ApplicationContext#getDisplayName()
   */
  public final String getDisplayName ()
  {
    log.finer("Spring parent context - getDisplayName called");
    return "NoDisplayName";
  }

  /**
   * @see ApplicationContext#getStartupDate()
   */
  public final long getStartupDate ()
  {
    log.finer("Spring parent context - getStartupDate called");
    return 0;
  }

  /**
   * @see ApplicationContext#containsBeanDefinition(String)
   */
  public final boolean containsBeanDefinition (final String beanName)
  {
    log.finer("Spring parent context - containsBeanDefinition called for name: " + beanName);
    return false;
  }

  /**
   * @see ApplicationContext#getBeanDefinitionCount()
   */
  public final int getBeanDefinitionCount ()
  {
    log.finer("Spring parent context - getBeanDefinitionCount called");
    return 0;
  }

  /**
   * @see ApplicationContext#getBeanDefinitionNames()
   */
  public final String[] getBeanDefinitionNames ()
  {
    log.finer("Spring parent context - getBeanDefinitionNames called");
    return new String[0];
  }

  /**
   * @see ApplicationContext#getBeanNamesForType(Class)
   */
  public final String[] getBeanNamesForType (final Class type)
  {
    log.finer("Spring parent context - getBeanNamesForType called");
    return new String[0];
  }

  /**
   * @see ApplicationContext#getBeanNamesForType(Class,boolean,boolean)
   */
  public final String[] getBeanNamesForType (final Class type, final boolean includePrototypes, final boolean includeFactoryBeans)
  {
    log.finer("Spring parent context - getBeanNamesForType called");
    return new String[0];
  }

  /**
   * @see ApplicationContext#getBeansOfType(Class)
   */
  public final Map getBeansOfType (final Class type)
  {
    log.finer("Spring parent context - getBeansOfType called");
    return null;
  }

  /**
   * @see ApplicationContext#getBeansOfType(Class,boolean,boolean)
   */
  public final Map getBeansOfType (final Class type, final boolean includePrototypes, final boolean includeFactoryBeans)
  {
    log.finer("Spring parent context - getBeansOfType called");
    return null;
  }

  /**
   * @see ApplicationContext#isPrototype(String)
   */
  public final boolean isPrototype (final String theString)
  {
    log.finer("Spring parent context - isPrototype called");
    return false;
  }

  /**
   * @see ApplicationContext#getParentBeanFactory()
   */
  public final BeanFactory getParentBeanFactory ()
  {
    log.finer("Spring parent context - getParentBeanFactory called");
    return null;
  }

  /**
   * @see ApplicationContext#containsLocalBean(String)
   */
  public final boolean containsLocalBean (final String name)
  {
    log.finer("Spring parent context - containsLocalBean called for name: " + name);
    return false;
  }

  /**
   * @see ApplicationContext#getMessage(String,Object[],String,Locale)
   */
  public final String getMessage (final String code, final Object[] args, final String defaultMessage, final Locale locale)
  {
    log.finer("Spring parent context - getMessage called");
    return null;
  }

  /**
   * @see ApplicationContext#getMessage(String,Object[],Locale)
   */
  public final String getMessage (final String code, final Object[] args, final Locale locale)
  {
    log.finer("Spring parent context - getMessage called");
    return null;
  }

  /**
   * @see ApplicationContext#getMessage(MessageSourceResolvable,Locale)
   */
  public final String getMessage (final MessageSourceResolvable resolvable, final Locale locale)
  {
    log.finer("Spring parent context - getMessage called");
    return null;
  }

  /**
   * @see ApplicationContext#publishEvent(ApplicationEvent)
   */
  public final void publishEvent (final ApplicationEvent event)
  {
    log.finer("Spring parent context - publishEvent called");
  }

  /**
   * @see ApplicationContext#getResources(String)
   */
  public final Resource[] getResources (final String locationPattern)
    throws IOException
  {
    log.finer("Spring parent context - getResources called");
    return new Resource[0];
  }

  /**
   * @see ApplicationContext#getResource(String)
   */
  public final Resource getResource (final String location)
  {
    log.finer("Spring parent context - getResource called");
    return null;
  }

  /**
   * @see ApplicationContext#getClassLoader()
   */
  public final ClassLoader getClassLoader ()
  {
    log.finer("Spring parent context - getClassLoader called");
    return this.getClass().getClassLoader();
  }

  /**
   * @see ApplicationContext#findAnnotationOnBean(String, Class)
   */
  public final <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType)
  {
    log.finer("Spring parent context - findAnnotationOnBean called");
    return null;
  }

  /**
   * @see ApplicationContext#getBean(Class)
   */
  public final <T> T getBean(Class<T> requiredType)
  {
    log.finer("Spring parent context - getBean called");
    return null;
  }
  
  /**
   * @see ApplicationContext#getBeansWithAnnotation(Class)
   */
  public final Map<String,Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType)
  {
    log.finer("Spring parent context - getBeansWithAnnotation called");
    return null;
  }

}
