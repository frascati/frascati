/**
 * OW2 FraSCAti: SCA Implementation Spring
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Damien Fournier
 *
 */

package org.ow2.frascati.implementation.spring;

import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.SpringImplementation;

import org.objectweb.fractal.api.Component;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractCompositeBasedImplementationProcessor;

/**
 * OW2 FraSCAti SCA Implementation Spring Framework processor class.
 *
 * @author <a href="mailto:Philippe.Merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaImplementationSpringProcessor
     extends AbstractCompositeBasedImplementationProcessor<SpringImplementation, ClassPathXmlApplicationContext> {

  // ======================================================================
  // Internal state.
  // ======================================================================

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(SpringImplementation springImplementation, StringBuilder sb) {
    sb.append("sca:implementation.spring");
    append(sb, "location", springImplementation.getLocation());
    super.toStringBuilder(springImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SpringImplementation springImplementation, ProcessingContext processingContext)
	      throws ProcessorException
  {
    String springImplementationLocation = springImplementation.getLocation();
    if(springImplementationLocation == null || springImplementationLocation.equals("")) {
      error(processingContext, springImplementation, "The attribute 'location' must be set");
    } else {
      if(processingContext.getResource(springImplementationLocation) == null) {
        error(processingContext, springImplementation, "Location '", springImplementationLocation, "' not found");
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(springImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(SpringImplementation springImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Get the Spring location.
    String springLocation = springImplementation.getLocation();

    log.finer("Create an SCA component with the Spring implementation "
            + springLocation + " and the Fractal component type "
            + getFractalComponentType(springImplementation, processingContext).toString());

   	// TODO: Must handle location as described in the SCA specification.
    // location could be a Jar file or a directory.
    // Currently, location is considered to refer a Spring XML file in the class path.

    // Create the SCA composite.
    Component component = createFractalComposite(springImplementation, processingContext);

    // Create the Spring application context.
    // TODO: Must use another ApplicationContext to read XML files from JAR
    // files or directories.
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
        new String[] { springLocation }, new ParentApplicationContext(component));
    // It must use the current thread class loader to load Java classes.
    context.setClassLoader(processingContext.getClassLoader());

    // Connect the Fractal composite to Spring beans.
    connectFractalComposite(springImplementation, processingContext, context);
  }

  /**
   * AbstractCompositeBasedImplementationProcessor#getService(ContentType, String, Class<?>)
   */
  @Override
  protected final Object getService(ClassPathXmlApplicationContext context, String name, Class<?> interfaze) throws Exception
  {
    // Get the Spring bean having the same name as the interface name.
    return context.getBean(name);
  }

  /**
   * AbstractCompositeBasedImplementationProcessor#setReference(ContentType, String, Object, Class<?>)
   */
  @Override
  protected final void setReference(ClassPathXmlApplicationContext context, String name, Object delegate, Class<?> interfaze) throws Exception
  {
    // Nothing to do as the context will obtain the references by calling the ContentController directly.
  }

  // ======================================================================
  // Public methods.
  // ======================================================================

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.SPRING_IMPLEMENTATION);
  }

}
