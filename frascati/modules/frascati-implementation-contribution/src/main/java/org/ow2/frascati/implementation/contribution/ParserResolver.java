/**
 * OW2 FraSCAti: FraSCAti Implementation Contribution
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.contribution;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaFactory;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;

import org.ow2.frascati.metamodel.frascati.implementation.contribution.ContributionImplementation;
import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * OW2 FraSCAti parser resolver for <implementation.contribution>.
 * 
 * for each component of loaded composites
 *   if this component has a <implementation.contribution> then
 *     for each service of this component
 *       if this service has no interface then
 *         set its interface to the EmptyInterface Java interface.
 *       end
 *     end
 *     for each reference of this component
 *       if this reference has no interface then
 *         set its interface to the EmptyInterface Java interface.
 *       end
 *     end
 *   end
 * end
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
@Scope("COMPOSITE")
@Service(Resolver.class)
public class ParserResolver
  implements Resolver<EObject>
{
  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  public final EObject resolve(EObject element, ParsingContext parsingContext)
  {
	// If OSOA document
    if(element instanceof DocumentRoot) {
      Composite composite = ((DocumentRoot)element).getComposite();
      if(composite != null) {
        for(Component component : composite.getComponent()) {
          if(component.getImplementation() instanceof ContributionImplementation) {
            for(ComponentService cs : component.getService()) {
              if(cs.getInterface() == null) {
                cs.setInterface( newEmptyJavaInterface() );
              }
            }
            for(ComponentReference cr : component.getReference()) {
              if(cr.getInterface() == null) {
                cr.setInterface( newEmptyJavaInterface() );
              }
            }
          }
        }
      }
    }
    return element;
  }

  private JavaInterface newEmptyJavaInterface()
  {
    JavaInterface javaInterface = ScaFactory.eINSTANCE.createJavaInterface();
    javaInterface.setInterface(EmptyInterface.class.getName());
    return javaInterface;
  }
}
