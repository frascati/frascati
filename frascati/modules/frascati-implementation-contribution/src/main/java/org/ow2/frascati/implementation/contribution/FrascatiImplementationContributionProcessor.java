/**
 * OW2 FraSCAti: FraSCAti Implementation Contribution
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.contribution;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;

import org.oasisopen.sca.annotation.Reference;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.metamodel.frascati.implementation.contribution.ContributionImplementation;
import org.ow2.frascati.metamodel.frascati.implementation.contribution.ImplementationContributionPackage;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;

/**
 * OW2 FraSCAti Implementation Contribution processor class.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class FrascatiImplementationContributionProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<ContributionImplementation>
{
  //---------------------------------------------------------------------------
  // SCA configuration.
  // --------------------------------------------------------------------------

  @Reference(name="composite-manager")
  private CompositeManager compositeManager;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(ContributionImplementation contributionImplementation, StringBuilder sb)
  {
    sb.append("fic:implementation.contribution");
    append(sb, "contribution", contributionImplementation.getContribution());
    if(!isNullOrEmpty(contributionImplementation.getRemoteFraSCAti())) {
      append(sb, "remoteFraSCAti", contributionImplementation.getRemoteFraSCAti());
    }
    super.toStringBuilder(contributionImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(ContributionImplementation contributionImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the attribute 'contribution'.
    String contribution = contributionImplementation.getContribution();
    if(isNullOrEmpty(contribution)) {
      error(processingContext, contributionImplementation, "The attribute 'contribution' must be set with a non empty value");
    } else {
      if(processingContext.getResource(contribution) == null) {
        error(processingContext, contributionImplementation, contribution + " not found");
      }
    }

    // TODO: Check the attribute 'remoteFraSCAti'.

    // check attributes 'policySets' and 'requires'.
    checkImplementation(contributionImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(ContributionImplementation contributionImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
	// Generate the composite membrane for this contribution implementation.
    try {
      getComponentFactory().generateScaCompositeMembrane(processingContext, getFractalComponentType(contributionImplementation, processingContext));
    } catch (FactoryException fe) {
      severe(new ProcessorException(contributionImplementation, "Error when generating composite membrane", fe));
    }

	// Generate the component membrane for this contribution implementation.
    try {
      getComponentFactory().generateScaPrimitiveMembrane(processingContext, null, ImplementationContribution.class.getName());
    } catch (FactoryException fe) {
      severe(new ProcessorException(contributionImplementation, "Error when generating component membrane", fe));
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(ContributionImplementation contributionImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Create the component associated to this contribution implementation.
    Component contributionComponent;
    try {
      contributionComponent = getComponentFactory().createScaPrimitiveComponent(processingContext, null, ImplementationContribution.class.getName());
    } catch (FactoryException te) {
      severe(new ProcessorException(contributionImplementation, "Error while creating contribution component instance", te));
      return;
    }

    // Set the name of the component associated to this contribution implementation.
    setFractalComponentName(contributionComponent, "implementation.contribution");

    // Retrieve the SCA property controller of the component associated to this contribution implementation.
    SCAPropertyController propertyController = (SCAPropertyController)getFractalInterface(contributionComponent, SCAPropertyController.NAME);

    // Set properties of the component associated to this contribution implementation.
    String contribution = contributionImplementation.getContribution();
    propertyController.setValue("contribution", contribution);
    String remoteFraSCAti = contributionImplementation.getRemoteFraSCAti();
    propertyController.setValue("remoteFraSCAti", remoteFraSCAti);
    
    // Create the composite associated to this contribution implementation.
    Component contributionComposite;
    try {
      contributionComposite = getComponentFactory().createScaCompositeComponent(processingContext, getFractalComponentType(contributionImplementation, processingContext));
    } catch (FactoryException te) {
      severe(new ProcessorException(contributionImplementation, "Error while creating contribution composite instance", te));
      return;
    }

    // Add the component into the composite associated to this contribution implementation.
    addFractalSubComponent(contributionComposite, contributionComponent);

    // Retrieve where the contribution is stored into the processing context class loader.
    URL contributionURL = processingContext.getResource(contribution);
    if(contributionURL != null) {
      String contributionURLtoString = contributionURL.toString();
      if(contributionURLtoString.startsWith("file:")) {
        contribution = contributionURLtoString.substring("file:".length());
      }
    }

    if(isNullOrEmpty(remoteFraSCAti)) {
      //
      // Deploy this contribution on the local FraSCAti instance.
      //
      try {
    	// Deply the contribution and iterate over all composites contained into this contribution.
        for(Component cc : this.compositeManager.getContribution(contribution)) {
          // Add each composite contained into this contribution to the composite representing this contribution implementation.
          addFractalSubComponent(contributionComposite, cc);
        }
      } catch(ManagerException me) {
        severe(new ProcessorException(contributionImplementation, "Error while deploying contribution '" + contribution + "'", me));    
      }
    } else {
      //
      // Deploy this contribution on a remote FraSCAti instance.
      //

      //
      // Read the contribution file and store it into a string.
      //
      String contributionAsString = null;
      try {
        contributionAsString = FileUtil.getStringFromFile(new File(contribution));
      } catch (IOException ioe) {
        severe(new ProcessorException(contributionImplementation, "Error while reading the contribution file", ioe));
      }

      //
      // Create an Apache CXF stub to the remote FraSCAti instance.
      //
      Deployment deployment = JAXRSClientFactory.create(remoteFraSCAti + "/deploy", Deployment.class);

      //
      // Deploy the contribution onto the remote FraSCAti instance.
      //
      StringList deployedCompositeStringList = deployment.deployContribution(contributionAsString);

      //
      // Create a proxy composite for each composite deployed remotely.
      //
      for(String name : deployedCompositeStringList.getStringList()) {
        try {
    	  Component composite = getComponentFactory().createScaCompositeComponent(processingContext, null);
          setFractalComponentName(composite, name);
          addFractalSubComponent(contributionComposite, composite);
    	} catch (FactoryException te) {
    	  severe(new ProcessorException(contributionImplementation, "Error while creating proxy composite for " + name, te));
          return;
    	}
      }
    }

    // Store the contribution implementation composite into the processing context.
    processingContext.putData(contributionImplementation, org.objectweb.fractal.api.Component.class, contributionComposite);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(ImplementationContributionPackage.Literals.CONTRIBUTION_IMPLEMENTATION);
  }
}
