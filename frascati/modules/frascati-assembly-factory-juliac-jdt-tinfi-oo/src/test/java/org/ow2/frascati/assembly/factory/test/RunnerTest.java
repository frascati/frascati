/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.test;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;

public class RunnerTest
{
    @Test
    public void runnerAutowire() throws Exception {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component composite = frascati.getComposite("Runner-autowire.composite");
      Runnable runnable = frascati.getService(composite, "Runnable", Runnable.class);
      runnable.run();
      frascati.close(composite);
      frascati.close();
    }

    @Test
    public void runnerWire() throws Exception {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component composite = frascati.getComposite("Runner-wire.composite");
      Runnable runnable = frascati.getService(composite, "Runnable", Runnable.class);
      runnable.run();
      frascati.close(composite);
      frascati.close();
    }

    @Test
    public void runnerTarget() throws Exception {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component composite = frascati.getComposite("Runner-target.composite");
      Runnable runnable = frascati.getService(composite, "Runnable", Runnable.class);
      runnable.run();
      frascati.close(composite);
      frascati.close();
    }
}
