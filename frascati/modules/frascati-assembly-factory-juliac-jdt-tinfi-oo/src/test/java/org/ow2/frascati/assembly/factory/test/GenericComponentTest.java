/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Jonathan Labejof
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.test;

import org.junit.Test;

import org.osoa.sca.annotations.Service;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;

/**
 * @author Jonathan Labejof
 *
 */
@Service(GenericInterface.class)
public class GenericComponentTest implements GenericInterface<String>
{

  public void test() {
  }

  public String getT() {
    return null;
  }

  @Test
  public void testIt() throws Exception {
    FraSCAti frascati = FraSCAti.newFraSCAti();
    Component component = frascati.getComposite("GenericComponent.composite");
    GenericInterface<String> genericInterface = (GenericInterface<String>)frascati.getService(component, "GenericInterface", GenericInterface.class);
    genericInterface.test();
    genericInterface.getT();
    frascati.close(component);
  }

}
