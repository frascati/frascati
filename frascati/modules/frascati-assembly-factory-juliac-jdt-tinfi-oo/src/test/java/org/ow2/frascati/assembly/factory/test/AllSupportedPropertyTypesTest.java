/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

import org.junit.Test;

import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Property;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;

/**
 * SCA component implementations with all property types supported by the OW2 FraSCAti Assembly Factory. 
 *
 * @author Philippe Merle.
 */
@Scope("COMPOSITE")
@Service(Runnable.class)
public class AllSupportedPropertyTypesTest implements Runnable {

  @Test
  public void loadIt() throws Exception {
    FraSCAti frascati = FraSCAti.newFraSCAti();
    Component component = frascati.getComposite("AllSupportedPropertyTypes.composite");
    Runnable runnable = frascati.getService(component, "Runnable", Runnable.class);
    runnable.run();
    frascati.close(component);
  }
	
  @Property(name = "java-BigDecimal")
  protected BigDecimal bigDecimalObject;

  @Property(name = "java-BigInteger")
  protected BigInteger bigIntegerObject;

  @Property(name = "java-boolean")
  protected boolean booleanPrimitive;
	
  @Property(name = "java-Boolean")
  protected Boolean booleanObject;
	
  @Property(name = "java-byte")
  protected byte bytePrimitive;
	
  @Property(name = "java-Byte")
  protected Byte byteObject;
	
  @Property(name = "java-Class")
  protected Class classObject;

  @Property(name = "java-char")
  protected char charPrimitive;
	
  @Property(name = "java-Character")
  protected Character characterObject;

  @Property(name = "java-double")
  protected double doublePrimitive;
	
  @Property(name = "java-Double")
  protected Double doubleObject;

  @Property(name = "java-float")
  protected float floatPrimitive;
	
  @Property(name = "java-Float")
  protected Float floatObject;
	
  @Property(name = "java-int")
  protected int intPrimitive;
	
  @Property(name = "java-Integer")
  protected Integer integerObject;

  @Property(name = "java-long")
  protected long longPrimitive;
	
  @Property(name = "java-Long")
  protected Long longObject;
	
  @Property(name = "java-short")
  protected short shortPrimitive;
	
  @Property(name = "java-Short")
  protected Short shortObject;
	
  @Property(name = "java-String")
  protected String stringObject;

  @Property(name = "java-URI")
  protected URI uriObject;

  @Property(name = "java-URL")
  protected URL urlObject;

  @Property(name = "java-QName")
  protected QName qnameObject;

  @Property(name = "xsd-anyURI")
  protected URI xsdAnyUri;

  @Property(name = "xsd-anySimpleType")
  protected String xsdAnySimpleType;

  @Property(name = "xsd-base64Binary")
  protected byte[] xsdBase64Binary;

  @Property(name = "xsd-boolean")
  protected boolean xsdBoolean;

  @Property(name = "xsd-byte")
  protected byte xsdByte;

  @Property(name = "xsd-date")
  protected XMLGregorianCalendar xsdDate;

  @Property(name = "xsd-dateTime")
  protected XMLGregorianCalendar xsdDateTime;

  @Property(name = "xsd-decimal")
  protected BigDecimal xsdDecimal;

  @Property(name = "xsd-double")
  protected double xsdDouble;

  @Property(name = "xsd-duration")
  protected Duration xsdDuration;

  @Property(name = "xsd-float")
  protected float xsdFloat;

  @Property(name = "xsd-g")
  protected XMLGregorianCalendar xsdG;

  @Property(name = "xsd-hexBinary")
  protected byte[] xsdHexBinary;

  @Property(name = "xsd-int")
  protected int xsdInt;

  @Property(name = "xsd-integer")
  protected long xsdInteger;

  @Property(name = "xsd-long")
  protected long xsdLong;

  @Property(name = "xsd-negativeInteger")
  protected long xsdNegativeInteger;

  @Property(name = "xsd-nonNegativeInteger")
  protected long xsdNonNegativeInteger;

  @Property(name = "xsd-nonPositiveInteger")
  protected long xsdNonPositiveInteger;

  @Property(name = "xsd-notation")
  protected QName xsdNotation;

  @Property(name = "xsd-unsignedByte")
  protected short xsdUnsignedByte;

  @Property(name = "xsd-unsignedInt")
  protected long xsdUnsignedInt;

  @Property(name = "xsd-unsignedLong")
  protected long xsdUnsignedLong;

  @Property(name = "xsd-unsignedShort")
  protected int xsdUnsignedShort;

  @Property(name = "xsd-time")
  protected XMLGregorianCalendar xsdTime;

  @Property(name = "xsd-positiveInteger")
  protected BigInteger xsdPositiveInteger;

  @Property(name = "xsd-short")
  protected short xsdShort;

  @Property(name = "xsd-string")
  protected String xsdString;

  @Property(name = "xsd-QName")
  protected QName xsdQName;

  public void run()
  {
    System.out.println("property 'java-BigDecimal' has value: " + bigDecimalObject);
    System.out.println("property 'java-BigInteger' has value: " + bigIntegerObject);
    System.out.println("property 'java-Class' has value: " + classObject);
    System.out.println("property 'java-boolean' has value: " + booleanPrimitive);
    System.out.println("property 'java-Boolean' has value: " + booleanObject);
    System.out.println("property 'java-byte' has value: " + bytePrimitive);
    System.out.println("property 'java-Byte' has value: " + byteObject);
    System.out.println("property 'java-char' has value: " + charPrimitive);
    System.out.println("property 'java-Character' has value: " + characterObject);
    System.out.println("property 'java-double' has value: " + doublePrimitive);
    System.out.println("property 'java-Double' has value: " + doubleObject);
    System.out.println("property 'java-float' has value: " + floatPrimitive);
    System.out.println("property 'java-Float' has value: " + floatObject);
    System.out.println("property 'java-int' has value: " + intPrimitive);
    System.out.println("property 'java-Integer' has value: " + integerObject);
    System.out.println("property 'java-long' has value: " + longPrimitive);
    System.out.println("property 'java-Long' has value: " + longObject);
    System.out.println("property 'java-short' has value: " + shortPrimitive);
    System.out.println("property 'java-Short' has value: " + shortObject);
    System.out.println("property 'java-String' has value: " + stringObject);
    System.out.println("property 'java-URI' has value: " + uriObject);
    System.out.println("property 'java-URL' has value: " + urlObject);
    System.out.println("property 'java-QName' has value: " + qnameObject);
    System.out.println("property 'xsd-anyURI' has value: " + xsdAnyUri);
    System.out.println("property 'xsd-anySimpleType' has value: " + xsdAnySimpleType);
    System.out.println("property 'xsd-base64Binary' has value: " + xsdBase64Binary);
    System.out.println("property 'xsd-boolean' has value: " + xsdBoolean);
    System.out.println("property 'xsd-byte' has value: " + xsdByte);
    System.out.println("property 'xsd-date' has value: " + xsdDate);
    System.out.println("property 'xsd-dateTime' has value: " + xsdDateTime);
    System.out.println("property 'xsd-decimal' has value: " + xsdDecimal);
    System.out.println("property 'xsd-double' has value: " + xsdDouble);
    System.out.println("property 'xsd-duration' has value: " + xsdDuration);
    System.out.println("property 'xsd-float' has value: " + xsdFloat);
    System.out.println("property 'xsd-g' has value: " + xsdG);
    System.out.println("property 'xsd-hexBinary' has value: " + xsdHexBinary);
    System.out.println("property 'xsd-int' has value: " + xsdInt);
    System.out.println("property 'xsd-integer' has value: " + xsdInteger);
    System.out.println("property 'xsd-long' has value: " + xsdLong);
    System.out.println("property 'xsd-negativeInteger' has value: " + xsdNegativeInteger);
    System.out.println("property 'xsd-nonNegativeInteger' has value: " + xsdNonNegativeInteger);
    System.out.println("property 'xsd-nonPositiveInteger' has value: " + xsdNonPositiveInteger);
    System.out.println("property 'xsd-notation' has value: " + xsdNotation);
    System.out.println("property 'xsd-positiveInteger' has value: " + xsdPositiveInteger);
    System.out.println("property 'xsd-unsignedByte' has value: " + xsdUnsignedByte);
    System.out.println("property 'xsd-unsignedInt' has value: " + xsdUnsignedInt);
    System.out.println("property 'xsd-unsignedLong' has value: " + xsdUnsignedLong);
    System.out.println("property 'xsd-unsignedShort' has value: " + xsdUnsignedShort);
    System.out.println("property 'xsd-short' has value: " + xsdShort);
    System.out.println("property 'xsd-string' has value: " + xsdString);
    System.out.println("property 'xsd-time' has value: " + xsdTime);
    System.out.println("property 'xsd-QName' has value: " + xsdQName);
  }

}
