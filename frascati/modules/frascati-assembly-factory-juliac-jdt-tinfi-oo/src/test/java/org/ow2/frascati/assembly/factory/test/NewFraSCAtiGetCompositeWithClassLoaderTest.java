/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.test;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import org.junit.Test;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;

/**
 * JUnit test case for testing methods @link{FraSCAti#newFraSCAti(ClassLoader)} and @link{FraSCAti#getComposite(String,ClassLoader)}. 
 *
 * @author Philippe Merle.
 */
public class NewFraSCAtiGetCompositeWithClassLoaderTest
{
    private final static String M2_REPO = System.getProperty("user.home") + "/.m2/repository/";
    private final static String FRASCATI_VERSION = "1.5-SNAPSHOT";
    private final static String HELLOWORLD_POJO = computeArtifactPath("org/ow2/frascati/examples/helloworld-pojo", "helloworld-pojo");

    private static String computeArtifactPath(String groupId, String artifactId)
    {
      return M2_REPO + groupId + '/' + FRASCATI_VERSION + '/' + artifactId + '-' + FRASCATI_VERSION + ".jar";

    }

    @Test
    public void testHelloWorldPojo() throws Exception
    {
/*
      // Defines the class loader used to instantiate OW2 FraSCAti.
      ClassLoader classLoaderForNewFraSCAti =
          new URLClassLoader(
              new URL[] {
              }
          );
      // Instantiates OW2 FraSCAti.
      FraSCAti frascati = FraSCAti.newFraSCAti(classLoaderForNewFraSCAti);

      // Checks the existence of the helloworld-pojo.jar
      File helloWorldPojoJar = new File(HELLOWORLD_POJO);
      System.out.println("\n\n\nDoes " + HELLOWORLD_POJO + " exist? " + helloWorldPojoJar.exists() + "\n\n\n");
      if(!helloWorldPojoJar.exists()) {
        throw new Exception(HELLOWORLD_POJO + " does not exist!");
      }

      // Defines the class loader used to get an SCA composite.
      ClassLoader classLoaderForGetComposite =
          new URLClassLoader(
              new URL[] {
            		  helloWorldPojoJar.toURI().toURL()
              }
          );
      
      // Gets the SCA composite.
      Component component = frascati.getComposite("helloworld-pojo", classLoaderForGetComposite);

      // Gets an SCA service of the SCA composite.
      Runnable runnable = frascati.getService(component, "r", Runnable.class);

      // Invokes this SCA service.
      runnable.run();

      // Closes the SCA composite.
      frascati.close(component);
*/
    }
}
