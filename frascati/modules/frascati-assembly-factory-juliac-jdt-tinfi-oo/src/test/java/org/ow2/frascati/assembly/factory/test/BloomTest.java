/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Jonathan Labejof
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.assembly.factory.test;

import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.annotations.Service;

import org.ow2.frascati.FraSCAti;

/**
 * @author bejof
 */
@Service(interfaces={Runnable.class})
public class BloomTest implements Runnable {

  @Test
  public void testIt() throws Exception {
    FraSCAti frascati = FraSCAti.newFraSCAti();
    Component component = frascati.getComposite("Blooms.composite");
    frascati.close(component);
  }

  public void run() {}

}
