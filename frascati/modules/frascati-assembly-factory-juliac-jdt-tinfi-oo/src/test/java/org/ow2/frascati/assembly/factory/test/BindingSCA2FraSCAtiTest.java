/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.assembly.factory.test;

import static org.junit.Assert.*;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;

/**
 * @author Philippe Merle - INRIA
 */
@Service(interfaces={Runnable.class})
public class BindingSCA2FraSCAtiTest implements Runnable {

  @Test
  public void testIt() throws Exception {
    FraSCAti frascati = FraSCAti.newFraSCAti();
    Component component = frascati.getComposite("BindingSCA2FraSCAti.composite");
    Runnable runnable = frascati.getService(component, "run", Runnable.class);
    runnable.run();
    frascati.close(component);
  }

  public void run()
  {
    String[] names = this.compositeManager.getCompositeNames();
    assertEquals("names.length", 2, names.length);
    assertEquals("names[0]", "org.ow2.frascati.FraSCAti", names[0]);
    assertEquals("names[1]", "BindingSCA2FraSCAti", names[1]);
  }

  @Reference(name = "composite-manager")
  protected CompositeManager compositeManager;

}
