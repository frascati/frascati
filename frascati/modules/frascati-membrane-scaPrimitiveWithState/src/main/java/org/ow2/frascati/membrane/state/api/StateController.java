/**
 * OW2 FraSCAti SCA Membrane with State Controller
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 */

package org.ow2.frascati.membrane.state.api;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;

/**
 * Controller to manage the state of a component.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public interface StateController
{
    /**
     * The interface name of the state controller.
     */
    String NAME = "state-controller";

    /**
     * Gets the state of a component.
     *
     * @return The state of a component.
     * @throws IllegalLifeCycleException Thrown when the component is not stopped.
     * @throws ContentInstantiationException Thrown when the content can not be instantiated.
     */
    State getState() throws IllegalLifeCycleException, ContentInstantiationException;

    /**
     * Sets the state of a component.
     *
     * @param state The state of a component.
     * @throws IllegalLifeCycleException Thrown when the component is not stopped.
     * @throws ContentInstantiationException Thrown when the content can not be instantiated.
     */
    void setState(State state) throws IllegalLifeCycleException, ContentInstantiationException;
}
