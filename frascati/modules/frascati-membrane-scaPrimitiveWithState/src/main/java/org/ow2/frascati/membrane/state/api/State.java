/**
 * OW2 FraSCAti SCA Membrane with State Controller
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 */

package org.ow2.frascati.membrane.state.api;

/**
 * A state is a collection of state fields.
 * Each state field has a name and a value.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public interface State
{
    /**
     * Gets all state field names.
     *
     * @return All state field names.
     */
    String[] getFields();

    /**
     * Gets the value of a state field.
     *
     * @param fieldName The name of the state field.
     * @param type The type of the state value.
     * @return The value of the state field.
     */
    <T> T getField(String fieldName, Class<T> type);

    /**
     * Sets the value of a state field.
     *
     * @param fieldName The name of the state field.
     * @param value The value of the state field.
     */
    <T> void setField(String fieldName, T value);
}
