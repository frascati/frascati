/**
 * OW2 FraSCAti SCA Membrane with State Controller
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 */

package org.ow2.frascati.membrane.state.juliac;

import java.util.HashMap;
import java.util.Map;

import org.ow2.frascati.membrane.state.api.State;

/**
 * Implementation of the State interface.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class StateImpl implements State
{
    /**
     * A map to store state fields, i.e., tuples of name and value.
     */
    private Map<String, Object> fields = new HashMap<String, Object>();

    /**
     * @see State#getFields()
     */
    public String[] getFields()
    {
        return this.fields.keySet().toArray(new String[this.fields.size()]);
    }

    /**
     * @see State#getField(String, Class)
     */
    @SuppressWarnings("unchecked")
    public <T> T getField(String fieldName, Class<T> type)
    {
        return (T)this.fields.get(fieldName);
    }

    /**
     * @see State#setField(String, T)
     */
    public <T> void setField(String fieldName, T value)
    {
    	this.fields.put(fieldName, value);
    }

    /**
     * Obtains a string representation of this state.
     *
     * @return A string representation of this state.
     */
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[State");
        for(Map.Entry<String,Object> entry : this.fields.entrySet() ) {
            sb.append(" (");
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(entry.getValue());
            sb.append(')');
        }
        sb.append(']');
        return sb.toString();
    }
}
