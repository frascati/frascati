/**
 * OW2 FraSCAti SCA Membrane with State Controller
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 */

package org.ow2.frascati.membrane.state.juliac;

import java.lang.reflect.Field;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;

import org.ow2.frascati.membrane.state.annotation.StateField;
import org.ow2.frascati.membrane.state.api.State;
import org.ow2.frascati.membrane.state.api.StateController;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Implementation of the state controller.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class StateControllerMixin implements StateController, Controller
{
    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------

    /**
     * Called at the initialisation of the controller.
     */
    public void initFcController(final InitializationContext ic)
    throws InstantiationException {
    	// Obtain the life cycle controller of the enclosing component.
        lifeCycleController = (LifeCycleController)ic.interfaces.get("lifecycle-controller");
    	// Obtain the SCA content controller of the enclosing component.
        scaContentController = (SCAContentController)ic.interfaces.get(SCAContentController.NAME);
    }

    // -------------------------------------------------------------------------
    // Internals related to the life cycle management
    // -------------------------------------------------------------------------

    /**
     * Reference to the life cycle controller.
     */
    private LifeCycleController lifeCycleController;

    /**
     * Checks that the enclosing component is stopped.
     *
     * @throws IllegalLifeCycleException Thrown when the enclosing component is not stopped.
     */
    private void checkFcStopped() throws IllegalLifeCycleException
    {
    	// If the component has a lifecycle controller
        if( lifeCycleController != null ) {
        	// then the life cycle state must be equals to STOPPED
            if( ! LifeCycleController.STOPPED.equals(lifeCycleController.getFcState()) ) {
            	// else throws an IllegalLifeCycleException.
            	throw new IllegalLifeCycleException("Component is not stopped");
            }
        }
    }

    // -------------------------------------------------------------------------
    // Internals related to the content management
    // -------------------------------------------------------------------------

    /**
     * Reference to the SCA content controller.
     */
    private SCAContentController scaContentController;

    // -------------------------------------------------------------------------
    // Implementation of the StateController interface
    // -------------------------------------------------------------------------

    /**
     * @see StateController#getState()
     */
    public State getState() throws IllegalLifeCycleException, ContentInstantiationException
    {
    	// Check if the component is stopped.
        checkFcStopped();

        // Get the current content.
        Object content = scaContentController.getFcContent();

        // Create the state to return.
        State state = new StateImpl();

        // Iterate over all fields of the content instance.
        for(Field field : content.getClass().getDeclaredFields()) {
        	// Is it annotated with @StateField?
            StateField stateField = field.getAnnotation(StateField.class);
            if(stateField != null) {
            	// Obtain the field name:
            	// 1) the name attribute of @StateField, else
            	// 2) the name property of the Field.
                String fieldName = stateField.name();
                if(fieldName.equals("")) {
                  fieldName = field.getName();
                }
                // Obtain the field value.
                Object fieldValue;
                try {
                	// allow to get private fields.
                    field.setAccessible(true);
                    // get the value of the field.
                    fieldValue = field.get(content);
                } catch(IllegalAccessException iae) {
                	// This exception should never happen.
                    throw new RuntimeException("Should never happen", iae);
                }
                // Add a state field to the state.
                state.setField(fieldName, fieldValue);
            }
        }

        System.out.println("StateController: getState called on " + content);
        System.out.println("  returned state is " + state);

        return state;
    }

    /**
     * @see StateController#setState(State)
     */
    public void setState(State state) throws IllegalLifeCycleException, ContentInstantiationException
    {
    	// Check if the component is stopped.
        checkFcStopped();

        // Get the current content.
        Object content = scaContentController.getFcContent();

        System.out.println("StateController: setState called on " + content + " with " + state);

        // Iterate over all fields of the content instance.
        for(Field field : content.getClass().getDeclaredFields()) {
            // Is this field annotated with @StateField?
            StateField stateField = field.getAnnotation(StateField.class);
            if(stateField != null) {
            	// Obtain the field name:
            	// 1) the name attribute of @StateField, else
            	// 2) the name property of the Field.
                String fieldName = stateField.name();
                if(fieldName.equals("")) {
                  fieldName = field.getName();
                }
                // Is a value defined for this state field?
                Object fieldValue = state.getField(fieldName, Object.class);
                if(fieldValue != null) {
                    try {
                        // allow to set private fields.
                        field.setAccessible(true);
                        // Set the field with the state field value.
                        field.set(content, fieldValue);
                    } catch(IllegalAccessException iae) {
                        // This exception should never happen.
                        throw new RuntimeException("Should never happen", iae);
                    }
                }
            }
        }
    }
}
