/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.impl;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.ow2.frascati.model.DocumentRoot;
import org.ow2.frascati.model.JsonRpcBinding;
import org.ow2.frascati.model.ModelPackage;
import org.ow2.frascati.model.OsgiImplementation;
import org.ow2.frascati.model.RMIBinding;
import org.ow2.frascati.model.RestBinding;
import org.ow2.frascati.model.ScriptImplementation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.impl.DocumentRootImpl#getImplementationOsgi <em>Implementation Osgi</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.DocumentRootImpl#getImplementationScript <em>Implementation Script</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.DocumentRootImpl#getBindingRest <em>Binding Rest</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.DocumentRootImpl#getBindingRmi <em>Binding Rmi</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.DocumentRootImpl#getBindingJsonRpc <em>Binding Json Rpc</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DocumentRootImpl extends org.eclipse.stp.sca.impl.DocumentRootImpl implements DocumentRoot {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DocumentRootImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return ModelPackage.Literals.DOCUMENT_ROOT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OsgiImplementation getImplementationOsgi() {
    return (OsgiImplementation)getMixed().get(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetImplementationOsgi(OsgiImplementation newImplementationOsgi, NotificationChain msgs) {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, newImplementationOsgi, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImplementationOsgi(OsgiImplementation newImplementationOsgi) {
    ((FeatureMap.Internal)getMixed()).set(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_OSGI, newImplementationOsgi);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ScriptImplementation getImplementationScript() {
    return (ScriptImplementation)getMixed().get(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetImplementationScript(ScriptImplementation newImplementationScript, NotificationChain msgs) {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT, newImplementationScript, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImplementationScript(ScriptImplementation newImplementationScript) {
    ((FeatureMap.Internal)getMixed()).set(ModelPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT, newImplementationScript);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RestBinding getBindingRest() {
    return (RestBinding)getMixed().get(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_REST, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBindingRest(RestBinding newBindingRest, NotificationChain msgs) {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_REST, newBindingRest, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBindingRest(RestBinding newBindingRest) {
    ((FeatureMap.Internal)getMixed()).set(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_REST, newBindingRest);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RMIBinding getBindingRmi() {
    return (RMIBinding)getMixed().get(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_RMI, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBindingRmi(RMIBinding newBindingRmi, NotificationChain msgs) {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_RMI, newBindingRmi, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBindingRmi(RMIBinding newBindingRmi) {
    ((FeatureMap.Internal)getMixed()).set(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_RMI, newBindingRmi);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public JsonRpcBinding getBindingJsonRpc() {
    return (JsonRpcBinding)getMixed().get(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_JSON_RPC, true);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetBindingJsonRpc(JsonRpcBinding newBindingJsonRpc, NotificationChain msgs) {
    return ((FeatureMap.Internal)getMixed()).basicAdd(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_JSON_RPC, newBindingJsonRpc, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBindingJsonRpc(JsonRpcBinding newBindingJsonRpc) {
    ((FeatureMap.Internal)getMixed()).set(ModelPackage.Literals.DOCUMENT_ROOT__BINDING_JSON_RPC, newBindingJsonRpc);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
        return basicSetImplementationOsgi(null, msgs);
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT:
        return basicSetImplementationScript(null, msgs);
      case ModelPackage.DOCUMENT_ROOT__BINDING_REST:
        return basicSetBindingRest(null, msgs);
      case ModelPackage.DOCUMENT_ROOT__BINDING_RMI:
        return basicSetBindingRmi(null, msgs);
      case ModelPackage.DOCUMENT_ROOT__BINDING_JSON_RPC:
        return basicSetBindingJsonRpc(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
        return getImplementationOsgi();
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT:
        return getImplementationScript();
      case ModelPackage.DOCUMENT_ROOT__BINDING_REST:
        return getBindingRest();
      case ModelPackage.DOCUMENT_ROOT__BINDING_RMI:
        return getBindingRmi();
      case ModelPackage.DOCUMENT_ROOT__BINDING_JSON_RPC:
        return getBindingJsonRpc();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
        setImplementationOsgi((OsgiImplementation)newValue);
        return;
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT:
        setImplementationScript((ScriptImplementation)newValue);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_REST:
        setBindingRest((RestBinding)newValue);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_RMI:
        setBindingRmi((RMIBinding)newValue);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_JSON_RPC:
        setBindingJsonRpc((JsonRpcBinding)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
        setImplementationOsgi((OsgiImplementation)null);
        return;
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT:
        setImplementationScript((ScriptImplementation)null);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_REST:
        setBindingRest((RestBinding)null);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_RMI:
        setBindingRmi((RMIBinding)null);
        return;
      case ModelPackage.DOCUMENT_ROOT__BINDING_JSON_RPC:
        setBindingJsonRpc((JsonRpcBinding)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_OSGI:
        return getImplementationOsgi() != null;
      case ModelPackage.DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT:
        return getImplementationScript() != null;
      case ModelPackage.DOCUMENT_ROOT__BINDING_REST:
        return getBindingRest() != null;
      case ModelPackage.DOCUMENT_ROOT__BINDING_RMI:
        return getBindingRmi() != null;
      case ModelPackage.DOCUMENT_ROOT__BINDING_JSON_RPC:
        return getBindingJsonRpc() != null;
    }
    return super.eIsSet(featureID);
  }

} //DocumentRootImpl
