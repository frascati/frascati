/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.Implementation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Osgi Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.OsgiImplementation#getBundle <em>Bundle</em>}</li>
 *   <li>{@link org.ow2.frascati.model.OsgiImplementation#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.OsgiImplementation#getGroup <em>Group</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.ModelPackage#getOsgiImplementation()
 * @model extendedMetaData="name='OsgiImplementation' kind='elementOnly'"
 * @generated
 */
public interface OsgiImplementation extends Implementation {
  /**
   * Returns the value of the '<em><b>Bundle</b></em>' attribute.
   * The default value is <code>""</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Bundle</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Bundle</em>' attribute.
   * @see #setBundle(String)
   * @see org.ow2.frascati.model.ModelPackage#getOsgiImplementation_Bundle()
   * @model default="" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getBundle();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.OsgiImplementation#getBundle <em>Bundle</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Bundle</em>' attribute.
   * @see #getBundle()
   * @generated
   */
  void setBundle(String value);

  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getOsgiImplementation_AnyAttribute()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getOsgiImplementation_Group()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:sca:osgiimplementation'"
   * @generated
   */
  FeatureMap getGroup();

} // OsgiImplementation
