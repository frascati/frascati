/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.stp.sca.impl.ImplementationImpl;

import org.ow2.frascati.model.ModelPackage;
import org.ow2.frascati.model.ScriptImplementation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Script Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.impl.ScriptImplementationImpl#getScript <em>Script</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.ScriptImplementationImpl#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.ScriptImplementationImpl#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.ScriptImplementationImpl#getGroup <em>Group</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScriptImplementationImpl extends ImplementationImpl implements ScriptImplementation {
  /**
   * The default value of the '{@link #getScript() <em>Script</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScript()
   * @generated
   * @ordered
   */
  protected static final String SCRIPT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getScript() <em>Script</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScript()
   * @generated
   * @ordered
   */
  protected String script = SCRIPT_EDEFAULT;

  /**
   * The default value of the '{@link #getLanguage() <em>Language</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLanguage()
   * @generated
   * @ordered
   */
  protected static final String LANGUAGE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLanguage() <em>Language</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLanguage()
   * @generated
   * @ordered
   */
  protected String language = LANGUAGE_EDEFAULT;

  /**
   * The cached value of the '{@link #getAnyAttribute() <em>Any Attribute</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnyAttribute()
   * @generated
   * @ordered
   */
  protected FeatureMap anyAttribute;

  /**
   * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroup()
   * @generated
   * @ordered
   */
  protected FeatureMap group;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ScriptImplementationImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return ModelPackage.Literals.SCRIPT_IMPLEMENTATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getScript() {
    return script;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setScript(String newScript) {
    String oldScript = script;
    script = newScript;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SCRIPT_IMPLEMENTATION__SCRIPT, oldScript, script));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLanguage() {
    return language;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLanguage(String newLanguage) {
    String oldLanguage = language;
    language = newLanguage;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SCRIPT_IMPLEMENTATION__LANGUAGE, oldLanguage, language));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FeatureMap getAnyAttribute() {
    if (anyAttribute == null) {
      anyAttribute = new BasicFeatureMap(this, ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE);
    }
    return anyAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FeatureMap getGroup() {
    if (group == null) {
      group = new BasicFeatureMap(this, ModelPackage.SCRIPT_IMPLEMENTATION__GROUP);
    }
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE:
        return ((InternalEList<?>)getAnyAttribute()).basicRemove(otherEnd, msgs);
      case ModelPackage.SCRIPT_IMPLEMENTATION__GROUP:
        return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case ModelPackage.SCRIPT_IMPLEMENTATION__SCRIPT:
        return getScript();
      case ModelPackage.SCRIPT_IMPLEMENTATION__LANGUAGE:
        return getLanguage();
      case ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE:
        if (coreType) return getAnyAttribute();
        return ((FeatureMap.Internal)getAnyAttribute()).getWrapper();
      case ModelPackage.SCRIPT_IMPLEMENTATION__GROUP:
        if (coreType) return getGroup();
        return ((FeatureMap.Internal)getGroup()).getWrapper();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case ModelPackage.SCRIPT_IMPLEMENTATION__SCRIPT:
        setScript((String)newValue);
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__LANGUAGE:
        setLanguage((String)newValue);
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE:
        ((FeatureMap.Internal)getAnyAttribute()).set(newValue);
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__GROUP:
        ((FeatureMap.Internal)getGroup()).set(newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case ModelPackage.SCRIPT_IMPLEMENTATION__SCRIPT:
        setScript(SCRIPT_EDEFAULT);
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__LANGUAGE:
        setLanguage(LANGUAGE_EDEFAULT);
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE:
        getAnyAttribute().clear();
        return;
      case ModelPackage.SCRIPT_IMPLEMENTATION__GROUP:
        getGroup().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case ModelPackage.SCRIPT_IMPLEMENTATION__SCRIPT:
        return SCRIPT_EDEFAULT == null ? script != null : !SCRIPT_EDEFAULT.equals(script);
      case ModelPackage.SCRIPT_IMPLEMENTATION__LANGUAGE:
        return LANGUAGE_EDEFAULT == null ? language != null : !LANGUAGE_EDEFAULT.equals(language);
      case ModelPackage.SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE:
        return anyAttribute != null && !anyAttribute.isEmpty();
      case ModelPackage.SCRIPT_IMPLEMENTATION__GROUP:
        return group != null && !group.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (script: ");
    result.append(script);
    result.append(", language: ");
    result.append(language);
    result.append(", anyAttribute: ");
    result.append(anyAttribute);
    result.append(", group: ");
    result.append(group);
    result.append(')');
    return result.toString();
  }

} //ScriptImplementationImpl
