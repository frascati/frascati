/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.Binding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RMI Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.RMIBinding#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.RMIBinding#getGroup <em>Group</em>}</li>
 *   <li>{@link org.ow2.frascati.model.RMIBinding#getHost <em>Host</em>}</li>
 *   <li>{@link org.ow2.frascati.model.RMIBinding#getServiceName <em>Service Name</em>}</li>
 *   <li>{@link org.ow2.frascati.model.RMIBinding#getPort <em>Port</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.ModelPackage#getRMIBinding()
 * @model extendedMetaData="name='RMIBinding' kind='elementOnly'"
 * @generated
 */
public interface RMIBinding extends Binding {
  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getRMIBinding_AnyAttribute()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getRMIBinding_Group()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:sca:rmibinding'"
   * @generated
   */
  FeatureMap getGroup();

  /**
   * Returns the value of the '<em><b>Host</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Host</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Host</em>' attribute.
   * @see #setHost(String)
   * @see org.ow2.frascati.model.ModelPackage#getRMIBinding_Host()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getHost();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.RMIBinding#getHost <em>Host</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Host</em>' attribute.
   * @see #getHost()
   * @generated
   */
  void setHost(String value);

  /**
   * Returns the value of the '<em><b>Service Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Service Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Service Name</em>' attribute.
   * @see #setServiceName(String)
   * @see org.ow2.frascati.model.ModelPackage#getRMIBinding_ServiceName()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getServiceName();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.RMIBinding#getServiceName <em>Service Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Service Name</em>' attribute.
   * @see #getServiceName()
   * @generated
   */
  void setServiceName(String value);

  /**
   * Returns the value of the '<em><b>Port</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Port</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Port</em>' attribute.
   * @see #setPort(String)
   * @see org.ow2.frascati.model.ModelPackage#getRMIBinding_Port()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   * @generated
   */
  String getPort();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.RMIBinding#getPort <em>Port</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Port</em>' attribute.
   * @see #getPort()
   * @generated
   */
  void setPort(String value);

} // RMIBinding
