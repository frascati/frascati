/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.stp.sca.impl.ImplementationImpl;

import org.ow2.frascati.model.ModelPackage;
import org.ow2.frascati.model.OsgiImplementation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Osgi Implementation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.impl.OsgiImplementationImpl#getBundle <em>Bundle</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.OsgiImplementationImpl#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.impl.OsgiImplementationImpl#getGroup <em>Group</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OsgiImplementationImpl extends ImplementationImpl implements OsgiImplementation {
  /**
   * The default value of the '{@link #getBundle() <em>Bundle</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBundle()
   * @generated
   * @ordered
   */
  protected static final String BUNDLE_EDEFAULT = "";

  /**
   * The cached value of the '{@link #getBundle() <em>Bundle</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getBundle()
   * @generated
   * @ordered
   */
  protected String bundle = BUNDLE_EDEFAULT;

  /**
   * The cached value of the '{@link #getAnyAttribute() <em>Any Attribute</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAnyAttribute()
   * @generated
   * @ordered
   */
  protected FeatureMap anyAttribute;

  /**
   * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getGroup()
   * @generated
   * @ordered
   */
  protected FeatureMap group;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OsgiImplementationImpl() {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass() {
    return ModelPackage.Literals.OSGI_IMPLEMENTATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getBundle() {
    return bundle;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setBundle(String newBundle) {
    String oldBundle = bundle;
    bundle = newBundle;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OSGI_IMPLEMENTATION__BUNDLE, oldBundle, bundle));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FeatureMap getAnyAttribute() {
    if (anyAttribute == null) {
      anyAttribute = new BasicFeatureMap(this, ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE);
    }
    return anyAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public FeatureMap getGroup() {
    if (group == null) {
      group = new BasicFeatureMap(this, ModelPackage.OSGI_IMPLEMENTATION__GROUP);
    }
    return group;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
    switch (featureID) {
      case ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE:
        return ((InternalEList<?>)getAnyAttribute()).basicRemove(otherEnd, msgs);
      case ModelPackage.OSGI_IMPLEMENTATION__GROUP:
        return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType) {
    switch (featureID) {
      case ModelPackage.OSGI_IMPLEMENTATION__BUNDLE:
        return getBundle();
      case ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE:
        if (coreType) return getAnyAttribute();
        return ((FeatureMap.Internal)getAnyAttribute()).getWrapper();
      case ModelPackage.OSGI_IMPLEMENTATION__GROUP:
        if (coreType) return getGroup();
        return ((FeatureMap.Internal)getGroup()).getWrapper();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue) {
    switch (featureID) {
      case ModelPackage.OSGI_IMPLEMENTATION__BUNDLE:
        setBundle((String)newValue);
        return;
      case ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE:
        ((FeatureMap.Internal)getAnyAttribute()).set(newValue);
        return;
      case ModelPackage.OSGI_IMPLEMENTATION__GROUP:
        ((FeatureMap.Internal)getGroup()).set(newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID) {
    switch (featureID) {
      case ModelPackage.OSGI_IMPLEMENTATION__BUNDLE:
        setBundle(BUNDLE_EDEFAULT);
        return;
      case ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE:
        getAnyAttribute().clear();
        return;
      case ModelPackage.OSGI_IMPLEMENTATION__GROUP:
        getGroup().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID) {
    switch (featureID) {
      case ModelPackage.OSGI_IMPLEMENTATION__BUNDLE:
        return BUNDLE_EDEFAULT == null ? bundle != null : !BUNDLE_EDEFAULT.equals(bundle);
      case ModelPackage.OSGI_IMPLEMENTATION__ANY_ATTRIBUTE:
        return anyAttribute != null && !anyAttribute.isEmpty();
      case ModelPackage.OSGI_IMPLEMENTATION__GROUP:
        return group != null && !group.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString() {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (bundle: ");
    result.append(bundle);
    result.append(", anyAttribute: ");
    result.append(anyAttribute);
    result.append(", group: ");
    result.append(group);
    result.append(')');
    return result.toString();
  }

} //OsgiImplementationImpl
