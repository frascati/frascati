/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.stp.sca.ScaPackage;

import org.eclipse.stp.sca.instance.InstancePackage;

import org.eclipse.stp.sca.policy.PolicyPackage;

import org.ow2.frascati.model.DocumentRoot;
import org.ow2.frascati.model.JsonRpcBinding;
import org.ow2.frascati.model.ModelFactory;
import org.ow2.frascati.model.ModelPackage;
import org.ow2.frascati.model.OsgiImplementation;
import org.ow2.frascati.model.RMIBinding;
import org.ow2.frascati.model.RestBinding;
import org.ow2.frascati.model.ScriptImplementation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelPackageImpl extends EPackageImpl implements ModelPackage {
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass osgiImplementationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass scriptImplementationEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass restBindingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass rmiBindingEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass jsonRpcBindingEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see org.ow2.frascati.model.ModelPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private ModelPackageImpl() {
    super(eNS_URI, ModelFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link ModelPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static ModelPackage init() {
    if (isInited) return (ModelPackage)EPackage.Registry.INSTANCE.getEPackage(ModelPackage.eNS_URI);

    // Obtain or create and register package
    ModelPackageImpl theModelPackage = (ModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ModelPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    ScaPackage.eINSTANCE.eClass();
    PolicyPackage.eINSTANCE.eClass();
    InstancePackage.eINSTANCE.eClass();
    XMLTypePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theModelPackage.createPackageContents();

    // Initialize created meta-data
    theModelPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theModelPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(ModelPackage.eNS_URI, theModelPackage);
    return theModelPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot() {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ImplementationOsgi() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_ImplementationScript() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_BindingRest() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_BindingRmi() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_BindingJsonRpc() {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOsgiImplementation() {
    return osgiImplementationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_Bundle() {
    return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_AnyAttribute() {
    return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOsgiImplementation_Group() {
    return (EAttribute)osgiImplementationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getScriptImplementation() {
    return scriptImplementationEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getScriptImplementation_Script() {
    return (EAttribute)scriptImplementationEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getScriptImplementation_Language() {
    return (EAttribute)scriptImplementationEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getScriptImplementation_AnyAttribute() {
    return (EAttribute)scriptImplementationEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getScriptImplementation_Group() {
    return (EAttribute)scriptImplementationEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRestBinding() {
    return restBindingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRestBinding_AnyAttribute() {
    return (EAttribute)restBindingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRestBinding_Group() {
    return (EAttribute)restBindingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRMIBinding() {
    return rmiBindingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRMIBinding_AnyAttribute() {
    return (EAttribute)rmiBindingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRMIBinding_Group() {
    return (EAttribute)rmiBindingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRMIBinding_Host() {
    return (EAttribute)rmiBindingEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRMIBinding_ServiceName() {
    return (EAttribute)rmiBindingEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRMIBinding_Port() {
    return (EAttribute)rmiBindingEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getJsonRpcBinding() {
    return jsonRpcBindingEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJsonRpcBinding_AnyAttribute() {
    return (EAttribute)jsonRpcBindingEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getJsonRpcBinding_Group() {
    return (EAttribute)jsonRpcBindingEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ModelFactory getModelFactory() {
    return (ModelFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents() {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEReference(documentRootEClass, DOCUMENT_ROOT__IMPLEMENTATION_OSGI);
    createEReference(documentRootEClass, DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT);
    createEReference(documentRootEClass, DOCUMENT_ROOT__BINDING_REST);
    createEReference(documentRootEClass, DOCUMENT_ROOT__BINDING_RMI);
    createEReference(documentRootEClass, DOCUMENT_ROOT__BINDING_JSON_RPC);

    osgiImplementationEClass = createEClass(OSGI_IMPLEMENTATION);
    createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__BUNDLE);
    createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__ANY_ATTRIBUTE);
    createEAttribute(osgiImplementationEClass, OSGI_IMPLEMENTATION__GROUP);

    scriptImplementationEClass = createEClass(SCRIPT_IMPLEMENTATION);
    createEAttribute(scriptImplementationEClass, SCRIPT_IMPLEMENTATION__SCRIPT);
    createEAttribute(scriptImplementationEClass, SCRIPT_IMPLEMENTATION__LANGUAGE);
    createEAttribute(scriptImplementationEClass, SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE);
    createEAttribute(scriptImplementationEClass, SCRIPT_IMPLEMENTATION__GROUP);

    restBindingEClass = createEClass(REST_BINDING);
    createEAttribute(restBindingEClass, REST_BINDING__ANY_ATTRIBUTE);
    createEAttribute(restBindingEClass, REST_BINDING__GROUP);

    rmiBindingEClass = createEClass(RMI_BINDING);
    createEAttribute(rmiBindingEClass, RMI_BINDING__ANY_ATTRIBUTE);
    createEAttribute(rmiBindingEClass, RMI_BINDING__GROUP);
    createEAttribute(rmiBindingEClass, RMI_BINDING__HOST);
    createEAttribute(rmiBindingEClass, RMI_BINDING__SERVICE_NAME);
    createEAttribute(rmiBindingEClass, RMI_BINDING__PORT);

    jsonRpcBindingEClass = createEClass(JSON_RPC_BINDING);
    createEAttribute(jsonRpcBindingEClass, JSON_RPC_BINDING__ANY_ATTRIBUTE);
    createEAttribute(jsonRpcBindingEClass, JSON_RPC_BINDING__GROUP);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents() {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    ScaPackage theScaPackage = (ScaPackage)EPackage.Registry.INSTANCE.getEPackage(ScaPackage.eNS_URI);
    XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    documentRootEClass.getESuperTypes().add(theScaPackage.getDocumentRoot());
    osgiImplementationEClass.getESuperTypes().add(theScaPackage.getImplementation());
    scriptImplementationEClass.getESuperTypes().add(theScaPackage.getImplementation());
    restBindingEClass.getESuperTypes().add(theScaPackage.getBinding());
    rmiBindingEClass.getESuperTypes().add(theScaPackage.getBinding());
    jsonRpcBindingEClass.getESuperTypes().add(theScaPackage.getBinding());

    // Initialize classes and features; add operations and parameters
    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getDocumentRoot_ImplementationOsgi(), this.getOsgiImplementation(), null, "implementationOsgi", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_ImplementationScript(), this.getScriptImplementation(), null, "implementationScript", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_BindingRest(), this.getRestBinding(), null, "bindingRest", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_BindingRmi(), this.getRMIBinding(), null, "bindingRmi", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_BindingJsonRpc(), this.getJsonRpcBinding(), null, "bindingJsonRpc", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(osgiImplementationEClass, OsgiImplementation.class, "OsgiImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getOsgiImplementation_Bundle(), theXMLTypePackage.getString(), "bundle", "", 1, 1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOsgiImplementation_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOsgiImplementation_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, OsgiImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(scriptImplementationEClass, ScriptImplementation.class, "ScriptImplementation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getScriptImplementation_Script(), theXMLTypePackage.getString(), "script", null, 0, 1, ScriptImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getScriptImplementation_Language(), theXMLTypePackage.getString(), "language", null, 0, 1, ScriptImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getScriptImplementation_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, ScriptImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getScriptImplementation_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, ScriptImplementation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(restBindingEClass, RestBinding.class, "RestBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRestBinding_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, RestBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRestBinding_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, RestBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(rmiBindingEClass, RMIBinding.class, "RMIBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getRMIBinding_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, RMIBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRMIBinding_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, RMIBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRMIBinding_Host(), theXMLTypePackage.getString(), "host", null, 1, 1, RMIBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRMIBinding_ServiceName(), theXMLTypePackage.getString(), "serviceName", null, 1, 1, RMIBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRMIBinding_Port(), theXMLTypePackage.getString(), "port", null, 1, 1, RMIBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(jsonRpcBindingEClass, JsonRpcBinding.class, "JsonRpcBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getJsonRpcBinding_AnyAttribute(), ecorePackage.getEFeatureMapEntry(), "anyAttribute", null, 0, -1, JsonRpcBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getJsonRpcBinding_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, JsonRpcBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations() {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] {
       "name", "",
       "kind", "mixed"
       });		
    addAnnotation
      (getDocumentRoot_ImplementationOsgi(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "implementation.osgi",
       "namespace", "##targetNamespace",
       "affiliation", "http://www.osoa.org/xmlns/sca/1.0#implementation"
       });		
    addAnnotation
      (getDocumentRoot_ImplementationScript(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "implementation.script",
       "namespace", "##targetNamespace",
       "affiliation", "http://www.osoa.org/xmlns/sca/1.0#implementation"
       });		
    addAnnotation
      (getDocumentRoot_BindingRest(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "binding.rest",
       "namespace", "##targetNamespace",
       "affiliation", "http://www.osoa.org/xmlns/sca/1.0#binding"
       });		
    addAnnotation
      (getDocumentRoot_BindingRmi(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "binding.rmi",
       "namespace", "##targetNamespace",
       "affiliation", "http://www.osoa.org/xmlns/sca/1.0#binding"
       });		
    addAnnotation
      (getDocumentRoot_BindingJsonRpc(), 
       source, 
       new String[] {
       "kind", "element",
       "name", "binding.jsonrpc",
       "namespace", "##targetNamespace",
       "affiliation", "http://www.osoa.org/xmlns/sca/1.0#binding"
       });		
    addAnnotation
      (osgiImplementationEClass, 
       source, 
       new String[] {
       "name", "OsgiImplementation",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getOsgiImplementation_AnyAttribute(), 
       source, 
       new String[] {
       "kind", "attributeWildcard",
       "wildcards", "##any",
       "name", ":3",
       "processing", "lax"
       });		
    addAnnotation
      (getOsgiImplementation_Group(), 
       source, 
       new String[] {
       "kind", "group",
       "name", "group:sca:osgiimplementation"
       });		
    addAnnotation
      (scriptImplementationEClass, 
       source, 
       new String[] {
       "name", "ScriptImplementation",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getScriptImplementation_AnyAttribute(), 
       source, 
       new String[] {
       "kind", "attributeWildcard",
       "wildcards", "##any",
       "name", ":3",
       "processing", "lax"
       });		
    addAnnotation
      (getScriptImplementation_Group(), 
       source, 
       new String[] {
       "kind", "group",
       "name", "group:sca:scriptimplementation"
       });		
    addAnnotation
      (restBindingEClass, 
       source, 
       new String[] {
       "name", "RestBinding",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getRestBinding_AnyAttribute(), 
       source, 
       new String[] {
       "kind", "attributeWildcard",
       "wildcards", "##any",
       "name", ":3",
       "processing", "lax"
       });		
    addAnnotation
      (getRestBinding_Group(), 
       source, 
       new String[] {
       "kind", "group",
       "name", "group:sca:restbinding"
       });		
    addAnnotation
      (rmiBindingEClass, 
       source, 
       new String[] {
       "name", "RMIBinding",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getRMIBinding_AnyAttribute(), 
       source, 
       new String[] {
       "kind", "attributeWildcard",
       "wildcards", "##any",
       "name", ":3",
       "processing", "lax"
       });		
    addAnnotation
      (getRMIBinding_Group(), 
       source, 
       new String[] {
       "kind", "group",
       "name", "group:sca:rmibinding"
       });		
    addAnnotation
      (jsonRpcBindingEClass, 
       source, 
       new String[] {
       "name", "JsonRpcBinding",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getJsonRpcBinding_AnyAttribute(), 
       source, 
       new String[] {
       "kind", "attributeWildcard",
       "wildcards", "##any",
       "name", ":3",
       "processing", "lax"
       });		
    addAnnotation
      (getJsonRpcBinding_Group(), 
       source, 
       new String[] {
       "kind", "group",
       "name", "group:sca:jsonrpcbinding"
       });
  }

} //ModelPackageImpl
