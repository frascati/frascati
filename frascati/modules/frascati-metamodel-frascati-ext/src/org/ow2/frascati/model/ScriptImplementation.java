/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.Implementation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Script Implementation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.ScriptImplementation#getScript <em>Script</em>}</li>
 *   <li>{@link org.ow2.frascati.model.ScriptImplementation#getLanguage <em>Language</em>}</li>
 *   <li>{@link org.ow2.frascati.model.ScriptImplementation#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.ScriptImplementation#getGroup <em>Group</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.ModelPackage#getScriptImplementation()
 * @model extendedMetaData="name='ScriptImplementation' kind='elementOnly'"
 * @generated
 */
public interface ScriptImplementation extends Implementation {
  /**
   * Returns the value of the '<em><b>Script</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Script</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Script</em>' attribute.
   * @see #setScript(String)
   * @see org.ow2.frascati.model.ModelPackage#getScriptImplementation_Script()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String"
   * @generated
   */
  String getScript();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.ScriptImplementation#getScript <em>Script</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Script</em>' attribute.
   * @see #getScript()
   * @generated
   */
  void setScript(String value);

  /**
   * Returns the value of the '<em><b>Language</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Language</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Language</em>' attribute.
   * @see #setLanguage(String)
   * @see org.ow2.frascati.model.ModelPackage#getScriptImplementation_Language()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String"
   * @generated
   */
  String getLanguage();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.ScriptImplementation#getLanguage <em>Language</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Language</em>' attribute.
   * @see #getLanguage()
   * @generated
   */
  void setLanguage(String value);

  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getScriptImplementation_AnyAttribute()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getScriptImplementation_Group()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:sca:scriptimplementation'"
   * @generated
   */
  FeatureMap getGroup();

} // ScriptImplementation
