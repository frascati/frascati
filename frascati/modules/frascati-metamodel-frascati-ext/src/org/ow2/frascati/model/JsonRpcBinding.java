/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;

import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.Binding;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Json Rpc Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.JsonRpcBinding#getAnyAttribute <em>Any Attribute</em>}</li>
 *   <li>{@link org.ow2.frascati.model.JsonRpcBinding#getGroup <em>Group</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.ModelPackage#getJsonRpcBinding()
 * @model extendedMetaData="name='JsonRpcBinding' kind='elementOnly'"
 * @generated
 */
public interface JsonRpcBinding extends Binding {
  /**
   * Returns the value of the '<em><b>Any Attribute</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Any Attribute</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Any Attribute</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getJsonRpcBinding_AnyAttribute()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='attributeWildcard' wildcards='##any' name=':3' processing='lax'"
   * @generated
   */
  FeatureMap getAnyAttribute();

  /**
   * Returns the value of the '<em><b>Group</b></em>' attribute list.
   * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Group</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Group</em>' attribute list.
   * @see org.ow2.frascati.model.ModelPackage#getJsonRpcBinding_Group()
   * @model dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
   *        extendedMetaData="kind='group' name='group:sca:jsonrpcbinding'"
   * @generated
   */
  FeatureMap getGroup();

} // JsonRpcBinding
