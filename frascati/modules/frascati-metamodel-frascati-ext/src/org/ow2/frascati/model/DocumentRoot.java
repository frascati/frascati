/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}</li>
 *   <li>{@link org.ow2.frascati.model.DocumentRoot#getImplementationScript <em>Implementation Script</em>}</li>
 *   <li>{@link org.ow2.frascati.model.DocumentRoot#getBindingRest <em>Binding Rest</em>}</li>
 *   <li>{@link org.ow2.frascati.model.DocumentRoot#getBindingRmi <em>Binding Rmi</em>}</li>
 *   <li>{@link org.ow2.frascati.model.DocumentRoot#getBindingJsonRpc <em>Binding Json Rpc</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends org.eclipse.stp.sca.DocumentRoot {
  /**
   * Returns the value of the '<em><b>Implementation Osgi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Implementation Osgi</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Implementation Osgi</em>' containment reference.
   * @see #setImplementationOsgi(OsgiImplementation)
   * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot_ImplementationOsgi()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='implementation.osgi' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#implementation'"
   * @generated
   */
  OsgiImplementation getImplementationOsgi();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Implementation Osgi</em>' containment reference.
   * @see #getImplementationOsgi()
   * @generated
   */
  void setImplementationOsgi(OsgiImplementation value);

  /**
   * Returns the value of the '<em><b>Implementation Script</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Implementation Script</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Implementation Script</em>' containment reference.
   * @see #setImplementationScript(ScriptImplementation)
   * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot_ImplementationScript()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='implementation.script' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#implementation'"
   * @generated
   */
  ScriptImplementation getImplementationScript();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.DocumentRoot#getImplementationScript <em>Implementation Script</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Implementation Script</em>' containment reference.
   * @see #getImplementationScript()
   * @generated
   */
  void setImplementationScript(ScriptImplementation value);

  /**
   * Returns the value of the '<em><b>Binding Rest</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Binding Rest</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Binding Rest</em>' containment reference.
   * @see #setBindingRest(RestBinding)
   * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot_BindingRest()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='binding.rest' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#binding'"
   * @generated
   */
  RestBinding getBindingRest();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.DocumentRoot#getBindingRest <em>Binding Rest</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Binding Rest</em>' containment reference.
   * @see #getBindingRest()
   * @generated
   */
  void setBindingRest(RestBinding value);

  /**
   * Returns the value of the '<em><b>Binding Rmi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Binding Rmi</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Binding Rmi</em>' containment reference.
   * @see #setBindingRmi(RMIBinding)
   * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot_BindingRmi()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='binding.rmi' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#binding'"
   * @generated
   */
  RMIBinding getBindingRmi();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.DocumentRoot#getBindingRmi <em>Binding Rmi</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Binding Rmi</em>' containment reference.
   * @see #getBindingRmi()
   * @generated
   */
  void setBindingRmi(RMIBinding value);

  /**
   * Returns the value of the '<em><b>Binding Json Rpc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Binding Json Rpc</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Binding Json Rpc</em>' containment reference.
   * @see #setBindingJsonRpc(JsonRpcBinding)
   * @see org.ow2.frascati.model.ModelPackage#getDocumentRoot_BindingJsonRpc()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='binding.jsonrpc' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#binding'"
   * @generated
   */
  JsonRpcBinding getBindingJsonRpc();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.DocumentRoot#getBindingJsonRpc <em>Binding Json Rpc</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Binding Json Rpc</em>' containment reference.
   * @see #getBindingJsonRpc()
   * @generated
   */
  void setBindingJsonRpc(JsonRpcBinding value);

} // DocumentRoot
