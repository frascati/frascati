/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.stp.sca.ScaPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.ow2.frascati.model.ModelFactory
 * @model kind="package"
 * @generated
 */
public interface ModelPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "model";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://frascati.ow2.org/xmlns/sca/1.1";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "frascati-model";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  ModelPackage eINSTANCE = org.ow2.frascati.model.impl.ModelPackageImpl.init();

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.DocumentRootImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 0;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = ScaPackage.DOCUMENT_ROOT__MIXED;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = ScaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = ScaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION;

  /**
   * The feature id for the '<em><b>Allow</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ALLOW = ScaPackage.DOCUMENT_ROOT__ALLOW;

  /**
   * The feature id for the '<em><b>Base Export</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BASE_EXPORT = ScaPackage.DOCUMENT_ROOT__BASE_EXPORT;

  /**
   * The feature id for the '<em><b>Base Import</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BASE_IMPORT = ScaPackage.DOCUMENT_ROOT__BASE_IMPORT;

  /**
   * The feature id for the '<em><b>Binding</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING = ScaPackage.DOCUMENT_ROOT__BINDING;

  /**
   * The feature id for the '<em><b>Binding Ejb</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_EJB = ScaPackage.DOCUMENT_ROOT__BINDING_EJB;

  /**
   * The feature id for the '<em><b>Binding Jms</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_JMS = ScaPackage.DOCUMENT_ROOT__BINDING_JMS;

  /**
   * The feature id for the '<em><b>Binding Sca</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_SCA = ScaPackage.DOCUMENT_ROOT__BINDING_SCA;

  /**
   * The feature id for the '<em><b>Binding Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_WS = ScaPackage.DOCUMENT_ROOT__BINDING_WS;

  /**
   * The feature id for the '<em><b>Binding Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_TYPE = ScaPackage.DOCUMENT_ROOT__BINDING_TYPE;

  /**
   * The feature id for the '<em><b>Callback</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CALLBACK = ScaPackage.DOCUMENT_ROOT__CALLBACK;

  /**
   * The feature id for the '<em><b>Component Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT_TYPE = ScaPackage.DOCUMENT_ROOT__COMPONENT_TYPE;

  /**
   * The feature id for the '<em><b>Composite</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPOSITE = ScaPackage.DOCUMENT_ROOT__COMPOSITE;

  /**
   * The feature id for the '<em><b>Constraining Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CONSTRAINING_TYPE = ScaPackage.DOCUMENT_ROOT__CONSTRAINING_TYPE;

  /**
   * The feature id for the '<em><b>Contribution</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CONTRIBUTION = ScaPackage.DOCUMENT_ROOT__CONTRIBUTION;

  /**
   * The feature id for the '<em><b>Definitions</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__DEFINITIONS = ScaPackage.DOCUMENT_ROOT__DEFINITIONS;

  /**
   * The feature id for the '<em><b>Deny All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__DENY_ALL = ScaPackage.DOCUMENT_ROOT__DENY_ALL;

  /**
   * The feature id for the '<em><b>Export</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT = ScaPackage.DOCUMENT_ROOT__EXPORT;

  /**
   * The feature id for the '<em><b>Export Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT_JAVA = ScaPackage.DOCUMENT_ROOT__EXPORT_JAVA;

  /**
   * The feature id for the '<em><b>Export Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT_RESOURCE = ScaPackage.DOCUMENT_ROOT__EXPORT_RESOURCE;

  /**
   * The feature id for the '<em><b>Implementation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION;

  /**
   * The feature id for the '<em><b>Implementation Bpel</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_BPEL = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_BPEL;

  /**
   * The feature id for the '<em><b>Implementation Composite</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE;

  /**
   * The feature id for the '<em><b>Implementation Cpp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_CPP = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_CPP;

  /**
   * The feature id for the '<em><b>Implementation Ejb</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_EJB = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_EJB;

  /**
   * The feature id for the '<em><b>Implementation Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_JAVA = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_JAVA;

  /**
   * The feature id for the '<em><b>Implementation Spring</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_SPRING = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_SPRING;

  /**
   * The feature id for the '<em><b>Implementation Web</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_WEB = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_WEB;

  /**
   * The feature id for the '<em><b>Implementation Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_TYPE = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_TYPE;

  /**
   * The feature id for the '<em><b>Import</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT = ScaPackage.DOCUMENT_ROOT__IMPORT;

  /**
   * The feature id for the '<em><b>Import Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT_JAVA = ScaPackage.DOCUMENT_ROOT__IMPORT_JAVA;

  /**
   * The feature id for the '<em><b>Import Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT_RESOURCE = ScaPackage.DOCUMENT_ROOT__IMPORT_RESOURCE;

  /**
   * The feature id for the '<em><b>Include</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INCLUDE = ScaPackage.DOCUMENT_ROOT__INCLUDE;

  /**
   * The feature id for the '<em><b>Intent</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTENT = ScaPackage.DOCUMENT_ROOT__INTENT;

  /**
   * The feature id for the '<em><b>Interface</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE = ScaPackage.DOCUMENT_ROOT__INTERFACE;

  /**
   * The feature id for the '<em><b>Interface Cpp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_CPP = ScaPackage.DOCUMENT_ROOT__INTERFACE_CPP;

  /**
   * The feature id for the '<em><b>Interface Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_JAVA = ScaPackage.DOCUMENT_ROOT__INTERFACE_JAVA;

  /**
   * The feature id for the '<em><b>Interface Partner Link Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_PARTNER_LINK_TYPE = ScaPackage.DOCUMENT_ROOT__INTERFACE_PARTNER_LINK_TYPE;

  /**
   * The feature id for the '<em><b>Interface Wsdl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_WSDL = ScaPackage.DOCUMENT_ROOT__INTERFACE_WSDL;

  /**
   * The feature id for the '<em><b>Permit All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__PERMIT_ALL = ScaPackage.DOCUMENT_ROOT__PERMIT_ALL;

  /**
   * The feature id for the '<em><b>Policy Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__POLICY_SET = ScaPackage.DOCUMENT_ROOT__POLICY_SET;

  /**
   * The feature id for the '<em><b>Run As</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__RUN_AS = ScaPackage.DOCUMENT_ROOT__RUN_AS;

  /**
   * The feature id for the '<em><b>Ends Conversation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ENDS_CONVERSATION = ScaPackage.DOCUMENT_ROOT__ENDS_CONVERSATION;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__REQUIRES = ScaPackage.DOCUMENT_ROOT__REQUIRES;

  /**
   * The feature id for the '<em><b>Anyextension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ANYEXTENSION = ScaPackage.DOCUMENT_ROOT__ANYEXTENSION;

  /**
   * The feature id for the '<em><b>Implementation Osgi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_OSGI = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Implementation Script</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Binding Rest</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_REST = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Binding Rmi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_RMI = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Binding Json Rpc</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_JSON_RPC = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.OsgiImplementationImpl <em>Osgi Implementation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.OsgiImplementationImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getOsgiImplementation()
   * @generated
   */
  int OSGI_IMPLEMENTATION = 1;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__POLICY_SETS = ScaPackage.IMPLEMENTATION__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__REQUIRES = ScaPackage.IMPLEMENTATION__REQUIRES;

  /**
   * The feature id for the '<em><b>Bundle</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__BUNDLE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__ANY_ATTRIBUTE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__GROUP = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Osgi Implementation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION_FEATURE_COUNT = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.ScriptImplementationImpl <em>Script Implementation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.ScriptImplementationImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getScriptImplementation()
   * @generated
   */
  int SCRIPT_IMPLEMENTATION = 2;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__POLICY_SETS = ScaPackage.IMPLEMENTATION__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__REQUIRES = ScaPackage.IMPLEMENTATION__REQUIRES;

  /**
   * The feature id for the '<em><b>Script</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__SCRIPT = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Language</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__LANGUAGE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION__GROUP = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Script Implementation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SCRIPT_IMPLEMENTATION_FEATURE_COUNT = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.RestBindingImpl <em>Rest Binding</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.RestBindingImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getRestBinding()
   * @generated
   */
  int REST_BINDING = 3;

  /**
   * The feature id for the '<em><b>Operation</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__OPERATION = ScaPackage.BINDING__OPERATION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__NAME = ScaPackage.BINDING__NAME;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__POLICY_SETS = ScaPackage.BINDING__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__REQUIRES = ScaPackage.BINDING__REQUIRES;

  /**
   * The feature id for the '<em><b>Uri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__URI = ScaPackage.BINDING__URI;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__ANY_ATTRIBUTE = ScaPackage.BINDING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING__GROUP = ScaPackage.BINDING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Rest Binding</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REST_BINDING_FEATURE_COUNT = ScaPackage.BINDING_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.RMIBindingImpl <em>RMI Binding</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.RMIBindingImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getRMIBinding()
   * @generated
   */
  int RMI_BINDING = 4;

  /**
   * The feature id for the '<em><b>Operation</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__OPERATION = ScaPackage.BINDING__OPERATION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__NAME = ScaPackage.BINDING__NAME;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__POLICY_SETS = ScaPackage.BINDING__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__REQUIRES = ScaPackage.BINDING__REQUIRES;

  /**
   * The feature id for the '<em><b>Uri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__URI = ScaPackage.BINDING__URI;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__ANY_ATTRIBUTE = ScaPackage.BINDING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__GROUP = ScaPackage.BINDING_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Host</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__HOST = ScaPackage.BINDING_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Service Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__SERVICE_NAME = ScaPackage.BINDING_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Port</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING__PORT = ScaPackage.BINDING_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>RMI Binding</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RMI_BINDING_FEATURE_COUNT = ScaPackage.BINDING_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.impl.JsonRpcBindingImpl <em>Json Rpc Binding</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.impl.JsonRpcBindingImpl
   * @see org.ow2.frascati.model.impl.ModelPackageImpl#getJsonRpcBinding()
   * @generated
   */
  int JSON_RPC_BINDING = 5;

  /**
   * The feature id for the '<em><b>Operation</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__OPERATION = ScaPackage.BINDING__OPERATION;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__NAME = ScaPackage.BINDING__NAME;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__POLICY_SETS = ScaPackage.BINDING__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__REQUIRES = ScaPackage.BINDING__REQUIRES;

  /**
   * The feature id for the '<em><b>Uri</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__URI = ScaPackage.BINDING__URI;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__ANY_ATTRIBUTE = ScaPackage.BINDING_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING__GROUP = ScaPackage.BINDING_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Json Rpc Binding</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int JSON_RPC_BINDING_FEATURE_COUNT = ScaPackage.BINDING_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see org.ow2.frascati.model.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Implementation Osgi</em>'.
   * @see org.ow2.frascati.model.DocumentRoot#getImplementationOsgi()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ImplementationOsgi();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.DocumentRoot#getImplementationScript <em>Implementation Script</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Implementation Script</em>'.
   * @see org.ow2.frascati.model.DocumentRoot#getImplementationScript()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ImplementationScript();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.DocumentRoot#getBindingRest <em>Binding Rest</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Binding Rest</em>'.
   * @see org.ow2.frascati.model.DocumentRoot#getBindingRest()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_BindingRest();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.DocumentRoot#getBindingRmi <em>Binding Rmi</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Binding Rmi</em>'.
   * @see org.ow2.frascati.model.DocumentRoot#getBindingRmi()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_BindingRmi();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.DocumentRoot#getBindingJsonRpc <em>Binding Json Rpc</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Binding Json Rpc</em>'.
   * @see org.ow2.frascati.model.DocumentRoot#getBindingJsonRpc()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_BindingJsonRpc();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.OsgiImplementation <em>Osgi Implementation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Osgi Implementation</em>'.
   * @see org.ow2.frascati.model.OsgiImplementation
   * @generated
   */
  EClass getOsgiImplementation();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.OsgiImplementation#getBundle <em>Bundle</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bundle</em>'.
   * @see org.ow2.frascati.model.OsgiImplementation#getBundle()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_Bundle();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.OsgiImplementation#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.OsgiImplementation#getAnyAttribute()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.OsgiImplementation#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.OsgiImplementation#getGroup()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_Group();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.ScriptImplementation <em>Script Implementation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Script Implementation</em>'.
   * @see org.ow2.frascati.model.ScriptImplementation
   * @generated
   */
  EClass getScriptImplementation();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.ScriptImplementation#getScript <em>Script</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Script</em>'.
   * @see org.ow2.frascati.model.ScriptImplementation#getScript()
   * @see #getScriptImplementation()
   * @generated
   */
  EAttribute getScriptImplementation_Script();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.ScriptImplementation#getLanguage <em>Language</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Language</em>'.
   * @see org.ow2.frascati.model.ScriptImplementation#getLanguage()
   * @see #getScriptImplementation()
   * @generated
   */
  EAttribute getScriptImplementation_Language();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.ScriptImplementation#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.ScriptImplementation#getAnyAttribute()
   * @see #getScriptImplementation()
   * @generated
   */
  EAttribute getScriptImplementation_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.ScriptImplementation#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.ScriptImplementation#getGroup()
   * @see #getScriptImplementation()
   * @generated
   */
  EAttribute getScriptImplementation_Group();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.RestBinding <em>Rest Binding</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Rest Binding</em>'.
   * @see org.ow2.frascati.model.RestBinding
   * @generated
   */
  EClass getRestBinding();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.RestBinding#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.RestBinding#getAnyAttribute()
   * @see #getRestBinding()
   * @generated
   */
  EAttribute getRestBinding_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.RestBinding#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.RestBinding#getGroup()
   * @see #getRestBinding()
   * @generated
   */
  EAttribute getRestBinding_Group();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.RMIBinding <em>RMI Binding</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>RMI Binding</em>'.
   * @see org.ow2.frascati.model.RMIBinding
   * @generated
   */
  EClass getRMIBinding();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.RMIBinding#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.RMIBinding#getAnyAttribute()
   * @see #getRMIBinding()
   * @generated
   */
  EAttribute getRMIBinding_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.RMIBinding#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.RMIBinding#getGroup()
   * @see #getRMIBinding()
   * @generated
   */
  EAttribute getRMIBinding_Group();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.RMIBinding#getHost <em>Host</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Host</em>'.
   * @see org.ow2.frascati.model.RMIBinding#getHost()
   * @see #getRMIBinding()
   * @generated
   */
  EAttribute getRMIBinding_Host();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.RMIBinding#getServiceName <em>Service Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Service Name</em>'.
   * @see org.ow2.frascati.model.RMIBinding#getServiceName()
   * @see #getRMIBinding()
   * @generated
   */
  EAttribute getRMIBinding_ServiceName();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.RMIBinding#getPort <em>Port</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Port</em>'.
   * @see org.ow2.frascati.model.RMIBinding#getPort()
   * @see #getRMIBinding()
   * @generated
   */
  EAttribute getRMIBinding_Port();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.JsonRpcBinding <em>Json Rpc Binding</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Json Rpc Binding</em>'.
   * @see org.ow2.frascati.model.JsonRpcBinding
   * @generated
   */
  EClass getJsonRpcBinding();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.JsonRpcBinding#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.JsonRpcBinding#getAnyAttribute()
   * @see #getJsonRpcBinding()
   * @generated
   */
  EAttribute getJsonRpcBinding_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.JsonRpcBinding#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.JsonRpcBinding#getGroup()
   * @see #getJsonRpcBinding()
   * @generated
   */
  EAttribute getJsonRpcBinding_Group();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  ModelFactory getModelFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.DocumentRootImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Implementation Osgi</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__IMPLEMENTATION_OSGI = eINSTANCE.getDocumentRoot_ImplementationOsgi();

    /**
     * The meta object literal for the '<em><b>Implementation Script</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__IMPLEMENTATION_SCRIPT = eINSTANCE.getDocumentRoot_ImplementationScript();

    /**
     * The meta object literal for the '<em><b>Binding Rest</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__BINDING_REST = eINSTANCE.getDocumentRoot_BindingRest();

    /**
     * The meta object literal for the '<em><b>Binding Rmi</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__BINDING_RMI = eINSTANCE.getDocumentRoot_BindingRmi();

    /**
     * The meta object literal for the '<em><b>Binding Json Rpc</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__BINDING_JSON_RPC = eINSTANCE.getDocumentRoot_BindingJsonRpc();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.OsgiImplementationImpl <em>Osgi Implementation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.OsgiImplementationImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getOsgiImplementation()
     * @generated
     */
    EClass OSGI_IMPLEMENTATION = eINSTANCE.getOsgiImplementation();

    /**
     * The meta object literal for the '<em><b>Bundle</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__BUNDLE = eINSTANCE.getOsgiImplementation_Bundle();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__ANY_ATTRIBUTE = eINSTANCE.getOsgiImplementation_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__GROUP = eINSTANCE.getOsgiImplementation_Group();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.ScriptImplementationImpl <em>Script Implementation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.ScriptImplementationImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getScriptImplementation()
     * @generated
     */
    EClass SCRIPT_IMPLEMENTATION = eINSTANCE.getScriptImplementation();

    /**
     * The meta object literal for the '<em><b>Script</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCRIPT_IMPLEMENTATION__SCRIPT = eINSTANCE.getScriptImplementation_Script();

    /**
     * The meta object literal for the '<em><b>Language</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCRIPT_IMPLEMENTATION__LANGUAGE = eINSTANCE.getScriptImplementation_Language();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCRIPT_IMPLEMENTATION__ANY_ATTRIBUTE = eINSTANCE.getScriptImplementation_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SCRIPT_IMPLEMENTATION__GROUP = eINSTANCE.getScriptImplementation_Group();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.RestBindingImpl <em>Rest Binding</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.RestBindingImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getRestBinding()
     * @generated
     */
    EClass REST_BINDING = eINSTANCE.getRestBinding();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REST_BINDING__ANY_ATTRIBUTE = eINSTANCE.getRestBinding_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REST_BINDING__GROUP = eINSTANCE.getRestBinding_Group();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.RMIBindingImpl <em>RMI Binding</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.RMIBindingImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getRMIBinding()
     * @generated
     */
    EClass RMI_BINDING = eINSTANCE.getRMIBinding();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RMI_BINDING__ANY_ATTRIBUTE = eINSTANCE.getRMIBinding_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RMI_BINDING__GROUP = eINSTANCE.getRMIBinding_Group();

    /**
     * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RMI_BINDING__HOST = eINSTANCE.getRMIBinding_Host();

    /**
     * The meta object literal for the '<em><b>Service Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RMI_BINDING__SERVICE_NAME = eINSTANCE.getRMIBinding_ServiceName();

    /**
     * The meta object literal for the '<em><b>Port</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute RMI_BINDING__PORT = eINSTANCE.getRMIBinding_Port();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.impl.JsonRpcBindingImpl <em>Json Rpc Binding</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.impl.JsonRpcBindingImpl
     * @see org.ow2.frascati.model.impl.ModelPackageImpl#getJsonRpcBinding()
     * @generated
     */
    EClass JSON_RPC_BINDING = eINSTANCE.getJsonRpcBinding();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JSON_RPC_BINDING__ANY_ATTRIBUTE = eINSTANCE.getJsonRpcBinding_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute JSON_RPC_BINDING__GROUP = eINSTANCE.getJsonRpcBinding_Group();

  }

} //ModelPackage
