/**
 * OW2 FraSCAti: SCA Implementation EJB
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.ejb;

import java.io.File;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.stp.sca.EJBImplementation;

import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.resolver.AbstractRecursiveResolver;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * Resolve to add EJB jars to the FraSCAti parsing context class loader.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
public class EjbResolver
     extends AbstractRecursiveResolver
{
  @Override
  protected void doResolve(EObject eobject, ParsingContext parsingContext)
  {
    if (eobject instanceof EJBImplementation) {
      // When the EMF object is an EJBImplementation instance
      // add the EJB jar to the parsing context class loader.
      String ejbImplementationEjbLink = ((EJBImplementation)eobject).getEjbLink();
      if(ejbImplementationEjbLink != null) {
        String[] tmp = ejbImplementationEjbLink.split("#");
        if(tmp.length > 0) {
  	      File ejbJar = new File(tmp[0]);
  	      if(ejbJar.exists()) {
  	        try {
              ((FrascatiClassLoader)parsingContext.getClassLoader()).addUrl(ejbJar.toURI().toURL());
  	        } catch(java.net.MalformedURLException mue) {
              mue.printStackTrace(System.err);
            }
          }
        }
      }
    }
  }
}
