/**
 * OW2 FraSCAti: SCA Implementation EJB
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.ejb;

import java.io.File;
import java.util.Properties;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;

/**
 * Info for deploying an EJB instance.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
public class EjbInfo
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  String ejbJar;

  File ejbJarFile;

  String ejbName;

  EJBContainer ejbContainer;

  Object ejbInstance;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Construct an EjbInfo instance.
   */
  public EjbInfo(String ejbJar, String ejbName)
  {
    this.ejbJar = ejbJar;
    this.ejbJarFile = new File(ejbJar);
    this.ejbName = ejbName;
  }

  /**
   * Create the EJB instance.
   */
  public void createEjbInstance()
  {
    // Create an EJB container.
	Properties properties = new Properties();
    properties.put(EJBContainer.MODULES, this.ejbJarFile);
    properties.put(EJBContainer.APP_NAME, "FraSCAti");
    this.ejbContainer = EJBContainer.createEJBContainer(properties);

    // Lookup the EJB instance.
    Context context = this.ejbContainer.getContext();
    try {
      this.ejbInstance = context.lookup("java:global/FraSCAti/" + this.ejbJarFile.getName().split("\\.")[0] + "/" + this.ejbName);
    } catch(NamingException ne) {
      throw new RuntimeException("JNDI lookup failed", ne);
    }
  }
}
