/**
 * OW2 FraSCAti: SCA Implementation EJB
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.ejb;

import org.eclipse.stp.sca.EJBImplementation;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.ScaPackage;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.component.factory.api.FactoryException;

/**
 * OW2 FraSCAti SCA Implementation EJB processor class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
public class FrascatiImplementationEjbProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<EJBImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(EJBImplementation ejbImplementation, StringBuilder sb) {
    sb.append("sca:implementation.ejb");
    append(sb, "ejb-link", ejbImplementation.getEjbLink());
    super.toStringBuilder(ejbImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(EJBImplementation ejbImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Check the attribute 'ejb-link'.
    String ejbImplementationEjbLink = ejbImplementation.getEjbLink();
    if(isNullOrEmpty(ejbImplementationEjbLink)) {
      error(processingContext, ejbImplementation, "The attribute 'ejb-link' must be set");
    } else {
      String[] tmp = ejbImplementationEjbLink.split("#");
      if(tmp.length != 2 || isNullOrEmpty(tmp[0]) || isNullOrEmpty(tmp[1])) {
        error(processingContext, ejbImplementation, "Invalid value for the attribute 'ejb-link', must be <ejb-jar>#<ejb-name>");
      } else {
        // Create an EjbInfo instance.
        EjbInfo ejbInfo = new EjbInfo(tmp[0], tmp[1]);

        // Check that the EJB jar is available.
        if(!ejbInfo.ejbJarFile.exists()) {
          error(processingContext, ejbImplementation, "'" + ejbInfo.ejbJar + "' not found");
        }

        // Store the EjbInfo instance into the processing context as it is used by next methods.
        processingContext.putData(ejbImplementation, EjbInfo.class, ejbInfo);
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(ejbImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(EJBImplementation ejbImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    EjbInfo ejbInfo = processingContext.getData(ejbImplementation, EjbInfo.class);

    // Create the SCA membrane for this component.
    try {
      getComponentFactory().generateMembrane(processingContext, getFractalComponentType(ejbImplementation, processingContext),
	      "scaComposite", null);
    } catch (FactoryException te) {
      severe(new ProcessorException(ejbImplementation, "Error while generating an EJB component instance", te));
      return;
    }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(EJBImplementation ejbImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    EjbInfo ejbInfo = processingContext.getData(ejbImplementation, EjbInfo.class);

    // Create the EJB instance.
    ejbInfo.createEjbInstance();

    // Instantiate an EJB composite component.
    Component component;
    try {
      component = getComponentFactory().createComponent(processingContext, getFractalComponentType(ejbImplementation, processingContext),
                "scaComposite", null);
    } catch (FactoryException te) {
      severe(new ProcessorException(ejbImplementation, "Error while creating an EJB component instance", te));
      return;
    }

    // Etablish a Fractal binding between the 1st composite service and the EJB instance.
    org.eclipse.stp.sca.Component scaComponent = getParent(ejbImplementation, org.eclipse.stp.sca.Component.class);
    ComponentService componentService = scaComponent.getService().get(0);
    try {
      bindFractalComponent(
          component,
          componentService.getName(),
    	  ejbInfo.ejbInstance);
    } catch (Exception e) {
      severe(new ProcessorException(ejbImplementation, "Can't connect the EJB composite to the EJB instance", e));
      return ;
    }

    // Store the created component into the processing context.
    processingContext.putData(ejbImplementation, Component.class, component);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.EJB_IMPLEMENTATION);
  }
}
