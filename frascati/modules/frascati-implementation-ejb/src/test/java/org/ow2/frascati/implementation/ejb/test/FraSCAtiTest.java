/**
 * OW2 FraSCAti: SCA Implementation EJB
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.ejb.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

public class FraSCAtiTest
{
    /**
     * Check errors produced during the checking phase.
     */
    @Test
    public void processCheckingErrorsComposite() throws Exception
    {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        frascati.processComposite("CheckingErrors", processingContext);
      } catch(FrascatiException fe) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrors.composite'
    	assertEquals("The number of checking errors", 6, processingContext.getErrors());
        // Let's note that one warning is produced by the OW2 FraSCAti Parser (EMF diagnostics).
        assertEquals("The number of checking warnings", 1, processingContext.getWarnings());
      }
    }
}
