/**
 * OW2 FraSCAti Fractal Binding Factory
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributors: Christophe Demarey, Philippe Merle
 *
 */

package org.ow2.frascati.binding.factory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.Binding;
import org.eclipse.stp.sca.Composite;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.common.BindingIntentController;
import org.objectweb.fractal.bf.connectors.common.BindingIntentHandler;
import org.objectweb.fractal.julia.Julia;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.parser.api.MultiplicityHelper;

/**
 * Abstract class for remote bindings implemented with the Fractal {@link BindingFactory}.
 *
 * @author <a href="mailto:nicolas.dolet@inria.fr">Nicolas Dolet</a>
 * @version 1.1
 */
public abstract class AbstractBindingFactoryProcessor<BindingType extends Binding>
              extends AbstractBindingProcessor<BindingType> {

  static
  {
	// OW2 Fractal Binding Factory requires at least the Julia Fractal bootstrap provider.
    System.setProperty("fractal.provider", Julia.class.getCanonicalName());
  }

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * The required binding factory.
   */
  @Reference(name = "binding-factory")
  private BindingFactory bindingFactory;

  /**
   * Used to get intent composites by calling processComposite(QName, ProcessingContext).
   */
  @Reference(name = "intent-loader")
  private CompositeManager intentLoader;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Create hints for the Fractal {@link BindingFactory}.
   */
  @SuppressWarnings("unchecked")
  private Map<String, Object> createBindingFactoryHints(Binding binding, ClassLoader classLoader) {
    Map<String, Object> hints = new HashMap<String, Object>();
    // set the binding protocol used by the Fractal Binding Factory.
    hints.put(BindingFactorySCAImpl.PLUGIN_ID, getBindingFactoryPluginId());
    // set the class loader used by the Fractal Binding Factory.
    hints.put(BindingFactorySCAImpl.CLASS_LOADER, classLoader);
    // don't start components created by the Fractal Binding Factory.
    hints.put(BindingFactorySCAImpl.DONT_START_COMPONENT, true);    
    // initialize hints specific to the binding type.
    initializeBindingHints((BindingType)binding, hints);
    return hints;
  }

  /**
   * Get the Fractal {@link BindingFactory} plugin to use.
   *
   * @return the Fractal {@link BindingFactory} plugin to use.
   */
  protected abstract String getBindingFactoryPluginId();

  /**
   * Initialize hints for a concrete {@link BindingType}.
   * @param b the binding
   * @param hints a {@link Map} representing hints to initialize with
   *   <ul>
   *     <li> key: the Object id represented with a {@link String}
   *     <li> value: the {@link Object}
   *   </ul>
   */
  protected abstract void initializeBindingHints(BindingType binding,
                                              Map<String, Object> hints);

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected void doCheck(BindingType binding, ProcessingContext processingContext)
      throws ProcessorException
  {
    checkBinding(binding, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected void doComplete(BindingType binding, ProcessingContext processingContext)
      throws ProcessorException
  {
    Component component = getFractalComponent(binding, processingContext);
    Map<String, Object> hints = createBindingFactoryHints(binding, processingContext.getClassLoader());

    Component proxyComponent = null;

    if(hasBaseReference(binding)) {
      BaseReference reference = getBaseReference(binding);
      String referenceName = reference.getName();

      // Following deal with references having a 0..N or 1..N multiplicity and several bindings.
      if(MultiplicityHelper.is0Nor1N(reference.getMultiplicity())) {
        // Add a unique suffix to the reference name.
        int index = reference.getBinding().indexOf(binding);
        referenceName = referenceName + '-' + index;
      }

      try {
        log.fine("Calling binding factory\n bind: "
            + getFractalComponentName(component) + " -> "
            + referenceName);
        proxyComponent = bindingFactory.bind(component, referenceName, hints);
      } catch (BindingFactoryException bfe) {
        severe(new ProcessorException(binding, "Error while binding reference: " + referenceName, bfe));
      }
    }

    if(hasBaseService(binding)) {
      BaseService service = getBaseService(binding);
      String serviceName = service.getName();
      try {
        log.fine("Calling binding factory\n export : "
            + getFractalComponentName(component) + " -> "
            + serviceName);
        proxyComponent = bindingFactory.export(component, serviceName, hints);
      } catch (BindingFactoryException bfe) {
        severe(new ProcessorException("Error while binding service: " + serviceName, bfe));
      }
    }

    logDo(processingContext, binding, "complete required intents");

    List<QName> requires = binding.getRequires();
	if(requires != null && requires.size() > 0) {
      // Get the binding intent controller of the proxy component.
      BindingIntentController bindingIntentController;
      try {
        bindingIntentController = (BindingIntentController)proxyComponent.getFcInterface("attribute-controller");
      } catch (NoSuchInterfaceException nsie) {
        severe(new ProcessorException(binding, "Cannot get to the binding intent controller", nsie));
        return;
      }

      for (QName require : requires) {
        // The binding intent component.
        Component bindingIntentComponent = null;

        // Try to retrieve the intent as a component of the composite containing this binding,
        // aka find a local intent.

        // Find the enclosing composite.
        EObject container = binding.eContainer();
        while(!(container instanceof Composite)) {
          container = container.eContainer();
        }
        Composite composite = (Composite)container;

        // Find a component named as the required intent.
        for(org.eclipse.stp.sca.Component comp : composite.getComponent()) {
          if(comp.getName().equals(require.getLocalPart())) {
            bindingIntentComponent = getFractalComponent(comp, processingContext);
            break; // the foor loop.
          }
        }

        // if no component found then
        if(bindingIntentComponent == null) {
          // Try to retrieve the intent as a global composite.
          try {
            bindingIntentComponent = intentLoader.processComposite(require, processingContext);
          } catch(ManagerException me) {
            warning(new ProcessorException(binding, "Error while getting intent '" + require
                    + "'", me));
            continue;
          }
        }

        BindingIntentHandler bindingIntentHandler;
        try {
          // Get the intent handler of the intent composite.
          bindingIntentHandler = (BindingIntentHandler)bindingIntentComponent.getFcInterface("intent");
        } catch (NoSuchInterfaceException nsie) {
          warning(new ProcessorException(binding, "Intent '" + require
              + "' does not provide an 'intent' service", nsie));
          continue;
        } catch (ClassCastException cce) {
          warning(new ProcessorException(binding, "Intent '" + require
                  + "' does not provide an 'intent' service of interface " +
                  BindingIntentHandler.class.getCanonicalName(), cce));
          continue;
        }

        try {
          // Add the intent handler.
          logDo(processingContext, binding, "adding intent handler");
          bindingIntentController.getIntentHandlers().add(bindingIntentHandler);
          logDone(processingContext, binding, "adding intent handler");
        } catch (Exception e) {
          severe(new ProcessorException(binding, "Intent '" + require + "' cannot be added", e));
          continue;
        }
      }
	}

	logDone(processingContext, binding, "complete required intents");
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
