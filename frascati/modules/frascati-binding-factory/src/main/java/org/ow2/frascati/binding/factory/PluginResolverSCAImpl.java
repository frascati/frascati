/***
 * OW2 FraSCAti Fractal Binding Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.binding.factory;

import java.util.List;

import org.objectweb.fractal.bf.BindingFactoryPlugin;
import org.objectweb.fractal.bf.PluginResolverImpl;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * SCA implementation of the PluginResolver component from the Binding Factory.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
@Scope("COMPOSITE")
public class PluginResolverSCAImpl extends PluginResolverImpl {

    @Reference(name="plugins")
    public final void setPlugins( List<BindingFactoryPlugin<?,?>> plugins ) {
    	/*
    	 * We are not supposed to use this method from SCA since the bindings on
    	 * the 'plugins' collection interface are performed by the inherited
    	 * resolvePlugin method.
    	 * 
    	 * This method is provided in order for the component to be
    	 * SCA-consistent.
    	 */
    	if( plugins.size() != 0 ) {
        	throw new UnsupportedOperationException();
    	}
    }
}
