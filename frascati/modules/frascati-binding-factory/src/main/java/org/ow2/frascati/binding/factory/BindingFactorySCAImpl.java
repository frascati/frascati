/***
 * OW2 FraSCAti Fractal Binding Factory
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 *
 * Contributor(s): Philippe Merle
 *				   Christophe Demarey
 */

package org.ow2.frascati.binding.factory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactoryImpl;
import org.objectweb.fractal.bf.PluginResolver;
import org.objectweb.fractal.util.Fractal;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.tinfi.TinfiRuntimeException;

@Scope("COMPOSITE")
public class BindingFactorySCAImpl extends BindingFactoryImpl {

	/**
	 * Setter for plugin resolvers.
	 */
	@Reference(name=PluginResolver.INTERFACE_NAME)
	public final void setPluginResolver( PluginResolver pluginResolver ) {
		// TODO implement this method by calling  super.bindingFc()
		try {
			bindFc(PluginResolver.INTERFACE_NAME,pluginResolver);
		}
		catch (NoSuchInterfaceException e) {
			throw new TinfiRuntimeException(e);
		}
		catch (IllegalBindingException e) {
			throw new TinfiRuntimeException(e);
		}
		catch (IllegalLifeCycleException e) {
			throw new TinfiRuntimeException(e);
		}
	}

	/**
	 * Return the parent container of the specified component.
	 * A container is a component with the "-container" suffix.
	 * 
	 * @param comp - A binding component
	 * @return the parent container of the given component, null if not found.
	 * @throws NoSuchInterfaceException
	 *             if the comp does not provide the {@link SuperController}
	 *             interface.
	 */
	@Override
	protected final Component getParentComponent(Component comp)
       throws NoSuchInterfaceException
    {
      Component[] parents = Fractal.getSuperController(comp).getFcSuperComponents();
      for (Component parent: parents) {
        if (Fractal.getNameController(parent).getFcName().endsWith("-container")) {
          return parent;
        }
      }
      return parents[0]; // No container found, keep the previous behavior: return the first parent
    }

}
