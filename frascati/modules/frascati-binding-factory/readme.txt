Fractal Binding Factory 
-----------------------
09.04.2009 - 0.5.1 stable released
17.02.2009 - 0.5 stable released
13.11.2008 - 0.4 stable released
22.07.2008 - 0.3 stable released
03.06.2008 - 0.3-SNAPSHOT under development
03.06.2008 - 0.2 stable released
21.05.2008 - 0.2-SNAPSHOT under development
21.05.2008 - 0.1 stable released
06.03.2008 - 0.1-SNAPSHOT released

How to import fractal-bf libraries into your Maven-based project
-----------------------
Add the following to your pom.xml:
<project>...
	
		<properties>
		   <fractal.bf>0.4</fractal.bf>
		</properties>
		<dependency>
			<groupId>org.objectweb.fractal.bf</groupId>
			<artifactId>fractal-bf-core</artifactId>
			<version>${fractal.bf}</version>
		</dependency>
		<dependency>
			<groupId>
				org.objectweb.fractal.bf.connectors
			</groupId>
			<artifactId>fractal-bf-connectors-soap-cxf</artifactId>
			<version>${fractal.bf}</version>
		</dependency>
		
		
	<!--OW2 repositories -->
	<repositories>
		<repository>
			<id>objectweb-release</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>objectweb-snapshot</id>
			<name>ObjectWeb Maven Repository</name>
			<url>http://maven.objectweb.org/maven2-snapshot</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
	
</project>