/**
 * OW2 FraSCAti: SCA Binding HTTP
 * Copyright (C) 2010-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.binding.http;

import javax.servlet.Servlet;

import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.domainmodel.tuscany.TuscanyPackage;
import org.eclipse.stp.sca.domainmodel.tuscany.HTTPBinding;

import org.osoa.sca.annotations.Reference;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;

import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.servlet.api.ServletManager;

/**
 * Bind components using HTTP bindings.
 *
 * @author Philippe Merle
 * @version 1.3
 */
public class FrascatiBindingHttpProcessor
     extends AbstractBindingProcessor<HTTPBinding>
{
  // --------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  @Reference(name = "servlet-manager")
  private ServletManager servletManager;

  @Reference(name = "composite-manager")
  private CompositeManager compositeManager;

  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(HTTPBinding httpBinding, StringBuilder sb) {
    sb.append("tuscany:binding.http");
    super.toStringBuilder(httpBinding, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(HTTPBinding httpBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    checkAttributeMustBeSet(httpBinding, "uri", httpBinding.getUri(), processingContext);

    checkAttributeNotSupported(httpBinding, "name", httpBinding.getName() != null, processingContext);

    if(!hasBaseService(httpBinding)) {
      error(processingContext, httpBinding, "Can only be child of <sca:service>");
    } else {
      // Get the Java class of the interface of the service.
      Class<?> interfaceClass = getBaseServiceJavaInterface(httpBinding, processingContext);
      if(interfaceClass != null) {
        if(!Servlet.class.isAssignableFrom(interfaceClass)) {
          error(processingContext, httpBinding, "The service's interface '",
              interfaceClass.getCanonicalName(), "' must be compatible with '",
              Servlet.class.getCanonicalName(), "'");
        }
      }
    }
    
    checkBinding(httpBinding, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(HTTPBinding httpBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, httpBinding, "nothing to generate");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(HTTPBinding httpBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    // The following is safe as it was checked in the method check().
	BaseService service = getBaseService(httpBinding);

    Servlet servlet;
    try {
      // Get the component from the processing context.
      Component componentToBind = getFractalComponent(httpBinding, processingContext);
      // The following cast is safe as it was checked in the method check().
      servlet = (Servlet)componentToBind.getFcInterface(service.getName());
	} catch(NoSuchInterfaceException nsie) {
	  // Should not happen!
	  severe(new ProcessorException(httpBinding, "Internal Fractal error!", nsie));
	  return;
    }

	// TODO Here use Tinfi or Assembly Factory to create the binding component.

	final HttpBinding bindingHttp = new HttpBinding();
    bindingHttp.uri = completeBindingURI(httpBinding);
    bindingHttp.servlet = servlet;
    bindingHttp.servletManager = servletManager;

    // TODO: Following could be moved into complete() or @Init method of the binding component.
//
// It seems to be not needed to run a Thread to initialize the new binding.
// Moreover Google App Engine does not allow us to create threads.
//
//    new Thread() {
//       public void run() {
            try {
	            bindingHttp.initialize();
	        } catch(Exception exc) {
	        	log.throwing("", "", exc);
	        }
//	    }
//	}.start();
	logFine(processingContext, httpBinding, "exporting done");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
  protected final void doComplete(HTTPBinding httpBinding, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, httpBinding, "nothing to complete");
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(TuscanyPackage.Literals.HTTP_BINDING);
  }

}
