/**
 * OW2 FraSCAti: SCA Binding HTTP
 * Copyright (C) 2010-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.binding.http;

import javax.servlet.Servlet;

import org.osoa.sca.annotations.Destroy;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.servlet.api.ServletManager;

@Scope("COMPOSITE")
@Service(Runnable.class)
public class HttpBinding
{
	/** URI of this HTTP binding. */
    @Property
    protected String uri;

    /** The servlet exposed by this HTTP binding. */
    @Reference
    protected Servlet servlet;

    /** The servlet manager. */
    @Reference
    protected ServletManager servletManager;

    @Init
    public final void initialize() throws Exception {
        servletManager.registerServlet(uri, servlet);
    }

    @Destroy
    public final void destroy() {
    	servletManager.unregisterServlet(uri);    
    }

}
