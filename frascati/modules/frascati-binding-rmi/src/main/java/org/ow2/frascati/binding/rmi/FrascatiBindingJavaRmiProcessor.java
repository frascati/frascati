/**
 * OW2 FraSCAti SCA Binding Java RMI
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.binding.rmi;

import java.net.URL;
import java.rmi.RMISecurityManager;
import java.util.Map;

import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.RMIBinding;

import org.objectweb.fractal.bf.connectors.rmi.RmiConnectorConstants;

import org.osoa.sca.annotations.Destroy;
import org.osoa.sca.annotations.Init;

import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;

/**
 * Bind components using Java RMI Binding.
 * 
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class FrascatiBindingJavaRmiProcessor
     extends AbstractBindingFactoryProcessor<RMIBinding>
{
  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * The Java Security Manager previously installed.
   */
  private SecurityManager previousSecurityManager;

  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(RMIBinding rmiBinding, StringBuilder sb) {
    sb.append("frascati:binding.rmi");
    append(sb, "host", rmiBinding.getHost());
    append(sb, "port", rmiBinding.getPort());
    append(sb, "serviceName", rmiBinding.getServiceName());
    super.toStringBuilder(rmiBinding, sb);
  }

  /**
   * @see AbstractBindingFactoryProcessor#getBindingFactoryPluginId()
   */
  @Override
  protected final String getBindingFactoryPluginId() {
    return "rmi";
  }

  /**
   * @see AbstractBindingFactoryProcessor#initializeBindingHints(EObjectType, Map)
   */
  @Override
  protected final void initializeBindingHints(RMIBinding rmiBinding, Map<String, Object> hints) {
    // set protocol specific parameters
    hints.put(RmiConnectorConstants.SERVICE_NAME, rmiBinding.getServiceName());
    hints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_PORT, rmiBinding.getPort());
    hints.put(RmiConnectorConstants.JAVA_RMI_REGISTRY_HOST_ADDRESS, rmiBinding.getHost());
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Set the Java Security Manager when starting this Java RMI plugin.
   */
  @SuppressWarnings("unused")
  @Init
  public final void init() {
	// Do nothing if Security Manager is not required.
    if("no".equals(System.getProperty("org.ow2.frascati.binding.rmi.security-manager"))) {
      return;
    }

    // TODO FraSCAti Java RMI policy property.
    URL policyURL = this.getClass().getClassLoader().getResource("frascati.policy");
    if (policyURL == null ) {
      log.warning("Java policy file not found for OW2 FraSCAti: using default Java policy!");
      previousSecurityManager = null;
    } else {
      System.setProperty("java.security.policy", policyURL.toString());
      previousSecurityManager = System.getSecurityManager();
      if (previousSecurityManager == null) {
        log.info("Set the Java security manager with Java policy " + policyURL);
        System.setSecurityManager(new RMISecurityManager());
      }
    }
  }

  /**
   * Reset the Java Security Manager when stopping this Java RMI plugin.
   */
  @Destroy
  public void destroy() {
    log.info("Reset the Java security manager.");
    System.setSecurityManager(previousSecurityManager);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(FrascatiPackage.Literals.RMI_BINDING);
  }

}
