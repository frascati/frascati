
-- FraSCAti JMS binding

For the JMS binding to work, you need to have an external JMS provider
and a JNDI server running. Tests have been configured to run with JORAM,
using the following configuration :

<config>
  <property name="Transaction" value="fr.dyade.aaa.util.NullTransaction"/>

  <server id="0" name="S0" hostname="localhost">
    <service class="org.objectweb.joram.mom.proxies.ConnectionManager" args="root root"/>
    <service class="org.objectweb.joram.mom.proxies.tcp.TcpProxyService" args="16010"/>
    <service class="fr.dyade.aaa.jndi2.server.JndiServer" args="16500"/>
  </server>
  
</config>

Furthermore, destinations and connection factories used in the tests must
have been previously created and registered in the JNDI server.
This includes :
- a queue named 'queue'
- a topic named 'topic'
- a connection factory named 'cf'
