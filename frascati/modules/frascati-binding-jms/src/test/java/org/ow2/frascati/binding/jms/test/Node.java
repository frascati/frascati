/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.binding.jms.test;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Reference;

/**
 * SCA Node component implementation.
 *
 * @author Philippe Merle.
 */
@Service(Runnable.class)
public class Node implements Runnable {

	public void run() {
      System.out.println("Node(name=" + name + ") run()...");
      if(out != null) {
        out.run();
      }
	}

	@Reference(name = "out", required=false)
	protected Runnable out;

	@ComponentName
	protected String name;

}
