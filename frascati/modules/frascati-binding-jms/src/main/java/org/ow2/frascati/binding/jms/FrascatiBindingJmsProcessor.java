/**
 * OW2 FraSCAti SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Guillaume Surrel
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.binding.jms;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.stp.sca.CorrelationSchemeType;
import org.eclipse.stp.sca.CreateResource;
import org.eclipse.stp.sca.JMSBinding;
import org.eclipse.stp.sca.Response;
import org.eclipse.stp.sca.ScaPackage;
import org.objectweb.fractal.bf.connectors.jms.JmsConnectorConstants;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;

/**
 * Bind SCA components using JMS Binding.
 */
public class FrascatiBindingJmsProcessor extends AbstractBindingFactoryProcessor<JMSBinding> {

  // The pattern used to check JMS URI.
  private static Pattern JMS_URI_PATTERN;

  static {
    // Build the JMS URI pattern, respecting http://tools.ietf.org/html/draft-merrick-jms-uri-07
    // There are 3 capturing groups for:
    // - the jms-variant
    // - the jms-dest
    // - the params
    String hexdig = "[a-fA-F0-9]";
    String pctencoded = "(?:%" + hexdig + hexdig + ")";
    String unreserved = "[a-zA-Z0-9-._~]";
    String subdelims = "[!$&'()*+,;=]";
    String segment = "(?:" + unreserved + '|' + subdelims + '|' + pctencoded + '|' + "[@:])*";
    String segmentnz = "(?:" + unreserved + '|' + subdelims + '|' + pctencoded + '|' + "[@:])+";
    String segmentnznc = "(?:" + unreserved + '|' + subdelims + '|' + pctencoded + '|' + "[@])+";
    String pathrootless = segmentnz + "(?:/" + segment + ")*";
    String paramname = "(?:" + unreserved + '|' + pctencoded + ")+";
    String paramvalue = "(?:" + unreserved + '|' + pctencoded + ")*";
    String param = paramname + "=" + paramvalue;

    String prefix = "jms:";
    String delimeter = ":";

    String jmsuri = prefix + '(' + segmentnznc + ')' + delimeter + '(' + pathrootless + ')' + "(?:\\?("
        + param + "(?:&" + param + ")*))?";

    JMS_URI_PATTERN = Pattern.compile(jmsuri);
  }

  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(JMSBinding jmsBinding, StringBuilder sb) {
    sb.append("sca:binding.jms");
    append(sb, "correlationScheme", jmsBinding.getCorrelationScheme());
    append(sb, "initialContextFactory", jmsBinding.getInitialContextFactory());
    append(sb, "jndiURL", jmsBinding.getJndiURL());
    super.toStringBuilder(jmsBinding, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(JMSBinding jmsBinding, ProcessingContext processingContext)
      throws ProcessorException {

    // Check if getUri() is well-formed.
    if (jmsBinding.getUri() != null && !jmsBinding.getUri().equals("")) {
      Matcher matcher = JMS_URI_PATTERN.matcher(jmsBinding.getUri());
      if (!matcher.matches()) {
        throw new ProcessorException(jmsBinding, "JMS URI is not well formed.");
      }

      String jmsvariant = matcher.group(1);
      try {
        jmsvariant = URLDecoder.decode(jmsvariant, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        log.severe("UTF-8 format not supported: can't decode JMS URI.");
      }
      if (!jmsvariant.equals("jndi")) {
        throw new ProcessorException(jmsBinding, "Only jms:jndi: variant is supported.");
      }

      // When the @uri attribute is specified, the destination element
      // MUST NOT be present
      if (jmsBinding.getDestination() != null) {
        throw new ProcessorException(jmsBinding, "Binding can't have both a JMS URI and a destination element.");
      }
    }

	// Default checking done on any SCA binding.
	super.doCheck(jmsBinding, processingContext);
  }

  /**
   * @see AbstractBindingFactoryProcessor#getBindingFactoryPluginId()
   */
  @Override
  protected final String getBindingFactoryPluginId() {
    return "jms";
  }

  /**
   * @see AbstractBindingFactoryProcessor#initializeBindingHints(EObjectType, Map)
   */
  @Override
  protected final void initializeBindingHints(JMSBinding jmsBinding, Map<String, Object> hints) {
    // set protocol specific parameters
    hints.put(JmsConnectorConstants.URI, jmsBinding.getUri());

    hints.put(JmsConnectorConstants.JNDI_URL, jmsBinding.getJndiURL());
    hints.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, jmsBinding.getInitialContextFactory());
    if (jmsBinding.getCorrelationScheme() != null) {
      if (jmsBinding.getCorrelationScheme().equals(CorrelationSchemeType.REQUEST_CORREL_ID_TO_CORREL_ID)) {
        hints.put(JmsConnectorConstants.JMS_CORRELATION_SCHEME, JmsConnectorConstants.CORRELATION_ID_SCHEME);
      } else if (jmsBinding.getCorrelationScheme().equals(CorrelationSchemeType.REQUEST_MSG_ID_TO_CORREL_ID)) {
        hints.put(JmsConnectorConstants.JMS_CORRELATION_SCHEME, JmsConnectorConstants.MESSAGE_ID_SCHEME);
      }
    }
    if (jmsBinding.getHeaders() != null) {
      hints.put(JmsConnectorConstants.JMS_DELIVERY_MODE, jmsBinding.getHeaders().getJMSDeliveryMode());
      hints.put(JmsConnectorConstants.JMS_TTL, String.valueOf(jmsBinding.getHeaders().getJMSTimeToLive()));
      hints.put(JmsConnectorConstants.JMS_PRIORITY, String.valueOf(jmsBinding.getHeaders().getJMSPriority()));
    }
    if (jmsBinding.getDestination() != null) {
      hints.put(JmsConnectorConstants.JNDI_DESTINATION_NAME, jmsBinding.getDestination().getName());
      if (jmsBinding.getDestination().getCreate() == null) {
        hints.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, JmsConnectorConstants.CREATE_DEFAULT);
      } else if (jmsBinding.getDestination().getCreate().equals(CreateResource.ALWAYS)) {
        hints.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, JmsConnectorConstants.CREATE_ALWAYS);
      } else if (jmsBinding.getDestination().getCreate().equals(CreateResource.NEVER)) {
        hints.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, JmsConnectorConstants.CREATE_NEVER);
      } else if (jmsBinding.getDestination().getCreate().equals(CreateResource.IFNOTEXIST)) {
        hints.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, JmsConnectorConstants.CREATE_IFNOTEXIST);
      }
      if (jmsBinding.getDestination().getType() != null) {
        hints.put(JmsConnectorConstants.DESTINATION_TYPE, jmsBinding.getDestination().getType().getName());
      } else {
        hints.put(JmsConnectorConstants.DESTINATION_TYPE, JmsConnectorConstants.DESTINATION_TYPE_DEFAULT);
      }
    }
    if (jmsBinding.getResponse() != null) {
      Response response = jmsBinding.getResponse();
      if (response.getDestination() != null) {
        hints.put(JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME, response.getDestination().getName());
      }
    }

    // Added in 1.1, not present in STP model for now
    // if (jmsBinding.getMessageSelection() != null) {
    //   hints.put(JmsConnectorConstants.JMS_SELECTOR,
    //   jmsBinding.getMessageSelection().getSelector());
    // }

    // Override with information extracted from URI
    if (jmsBinding.getUri() != null && !jmsBinding.getUri().equals("")) {

      Matcher matcher = JMS_URI_PATTERN.matcher(jmsBinding.getUri());
      // we know it will match as we have previously checked the uri
      matcher.matches();

      String jmsdest = matcher.group(2);
      try {
        jmsdest = URLDecoder.decode(jmsdest, "UTF-8");
      } catch (UnsupportedEncodingException e) {
        log.severe("UTF-8 format not supported: can't decode JMS URI.");
      }
      hints.put(JmsConnectorConstants.JNDI_DESTINATION_NAME, jmsdest);

      // There is an implicit @create="never" for the resources referred to
      // in the @uri attribute.
      hints.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, JmsConnectorConstants.CREATE_NEVER);

      String optionGroup = matcher.group(3);
      if (optionGroup != null) {
        String[] options = optionGroup.split("&");
        for (int i = 0; i < options.length; i++) {
          String option = options[i];
          String[] paramAndValue = option.split("=");
          String param = paramAndValue[0];
          try {
            URLDecoder.decode(param, "UTF-8");
          } catch (UnsupportedEncodingException e) {
            log.severe("UTF-8 format not supported: can't decode JMS URI.");
          }
          String value = "";
          if (paramAndValue.length > 1) {
            value = paramAndValue[1];
            try {
              value = URLDecoder.decode(value, "UTF-8");
            } catch (UnsupportedEncodingException e) {
              log.severe("UTF-8 format not supported: can't decode JMS URI.");
            }
          }
          if (param.equals(JmsConnectorConstants.JNDI_URL)) {
            hints.put(JmsConnectorConstants.JNDI_URL, value);
          } else if (param.equals(JmsConnectorConstants.JNDI_INITCONTEXTFACT)) {
            hints.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, value);
          } else if (param.equals(JmsConnectorConstants.JNDI_CONNFACTNAME)) {
            hints.put(JmsConnectorConstants.JNDI_CONNFACTNAME, value);
          } else if (param.equals(JmsConnectorConstants.JMS_DELIVERY_MODE)) {
            hints.put(JmsConnectorConstants.JMS_DELIVERY_MODE, value);
          } else if (param.equals(JmsConnectorConstants.JMS_TTL)) {
            hints.put(JmsConnectorConstants.JMS_TTL, value);
          } else if (param.equals(JmsConnectorConstants.JMS_PRIORITY)) {
            hints.put(JmsConnectorConstants.JMS_PRIORITY, value);
          } else if (param.equals(JmsConnectorConstants.JMS_SELECTOR)) {
            hints.put(JmsConnectorConstants.JMS_SELECTOR, value);
          }
        }
      }
    }

    if (jmsBinding.getResponse() != null) {
      if (jmsBinding.getResponse().getDestination() != null) {
        hints.put(JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME, jmsBinding.getResponse()
            .getDestination().getName());
      }
    }

  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.JMS_BINDING);
  }

}
