/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Guillaume Surrel
 *
 * Contributor(s):
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.net.ConnectException;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

/**
 * The JMS module is used by the stub and the skeleton to retrieve various JMS objects on
 * the JNDI and to initiate the JMS sessions necessary to interact with them.
 */
public class JmsModule {

  /** Logger for this object. */
  protected Logger log = Logger.getLogger(this.getClass().getCanonicalName());

  /** All the jms attributes used by the binding. */
  private JmsAttributes jmsAttributes;

  /** The JMS connection used by the binding. */
  private Connection jmsCnx;

  /** The JMS session used to send messages produced by the binding. */
  private Session session;

  /** The destination used by the binding. */
  private Destination destination;

  /** The session used to send or receive response messages. */
  private Session responseSession;

  /** The destination used to send or receive response messages. */
  private Destination responseDestination;

  public JmsModule(JmsAttributes jmsAttributes) {
    this.jmsAttributes = jmsAttributes;
  }

  public Connection getJmsCnx() {
    return jmsCnx;
  }

  public Session getSession() {
    return session;
  }

  public Destination getDestination() {
    return destination;
  }

  public Session getResponseSession() {
    return responseSession;
  }

  public Destination getResponseDestination() {
    return responseDestination;
  }

  /**
   * Retrieves or creates JMS objects on the JNDI and starts a JMS connection and two
   * sessions to interact with them.
   */
  public void start() throws JMSException, NamingException {

    log.info("******************************");
    log.info("correlationScheme: " + jmsAttributes.getCorrelationScheme());
    log.info("JndiURL: " + jmsAttributes.getJndiURL());
    log.info("ConnFactName: " + jmsAttributes.getJndiConnectionFactoryName());
    log.info("InitialContextFactory: " + jmsAttributes.getJndiInitialContextFactory());
    log.info("DestinationName: " + jmsAttributes.getJndiDestinationName());
    log.info("CreateDestinationMode: " + jmsAttributes.getCreateDestinationMode());
    log.info("DestinationType: " + jmsAttributes.getDestinationType());
    log.info("priority: " + jmsAttributes.getPriority());
    log.info("ttl: " + jmsAttributes.getTimeToLive());
    log.info("selector: " + jmsAttributes.getSelector());
    log.info("persistent: " + jmsAttributes.getPersistent());
    log.info("******************************");

    // Retrieve initial context.
    Hashtable environment = new Hashtable();
    if (!jmsAttributes.getJndiInitialContextFactory().equals(JmsConnectorConstants.NO_JNDI_INITCONTEXTFACT)) {
      environment.put(Context.INITIAL_CONTEXT_FACTORY, jmsAttributes.getJndiInitialContextFactory());
    }
    if (!jmsAttributes.getJndiURL().equals(JmsConnectorConstants.NO_JNDI_URL)) {
      environment.put(Context.PROVIDER_URL, jmsAttributes.getJndiURL());
    }
    Context ictx = new InitialContext(environment);

    // Lookup for elements in the JNDI.
    ConnectionFactory cf;
    try {
      cf = (ConnectionFactory) ictx.lookup(jmsAttributes.getJndiConnectionFactoryName());
    } catch (NamingException exc) {
      if (exc.getCause() instanceof ConnectException) {
        log.log(Level.WARNING,
                "ConnectException while trying to reach JNDI server -> launching a collocated JORAM server instead.");
        JoramServer.start(jmsAttributes.getJndiURL());
        cf = (ConnectionFactory) ictx.lookup(jmsAttributes.getJndiConnectionFactoryName());
      } else {
        throw exc;
      }
    }

    try {
      destination = (Destination) ictx.lookup(jmsAttributes.getJndiDestinationName());
    } catch (NameNotFoundException nnfe) {
      if (jmsAttributes.getCreateDestinationMode().equals(JmsConnectorConstants.CREATE_NEVER)) {
        throw new IllegalStateException("Destination " + jmsAttributes.getJndiDestinationName()
            + " not found in JNDI.");
      }
    }

    // Create the connection
    jmsCnx = cf.createConnection();

    session = jmsCnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    responseSession = jmsCnx.createSession(false, Session.AUTO_ACKNOWLEDGE);
    jmsCnx.start();

    if (destination == null) {

      // Create destination if it doesn't exist in JNDI
      if (jmsAttributes.getDestinationType().equals(JmsConnectorConstants.TYPE_QUEUE)) {
        destination = session.createQueue(jmsAttributes.getJndiDestinationName());
        ictx.bind(jmsAttributes.getJndiDestinationName(), destination);
      } else if (jmsAttributes.getDestinationType().equals(JmsConnectorConstants.TYPE_TOPIC)) {
        destination = session.createTopic(jmsAttributes.getJndiDestinationName());
        ictx.bind(jmsAttributes.getJndiDestinationName(), destination);
      } else {
        throw new IllegalStateException("Unknown destination type: " + jmsAttributes.getDestinationType());
      }

    } else {

      // Check that the object found in JNDI is correct
      if (jmsAttributes.getCreateDestinationMode().equals(JmsConnectorConstants.CREATE_ALWAYS)) {
        throw new IllegalStateException("Destination " + jmsAttributes.getJndiDestinationName()
            + " already exists in JNDI.");
      }
      if (jmsAttributes.getDestinationType().equals(JmsConnectorConstants.TYPE_QUEUE)
          && !(destination instanceof javax.jms.Queue)) {
        throw new IllegalStateException("Object found in JNDI " + jmsAttributes.getJndiDestinationName()
            + " does not match declared type 'queue'.");
      }
      if (jmsAttributes.getDestinationType().equals(JmsConnectorConstants.TYPE_TOPIC)
          && !(destination instanceof javax.jms.Topic)) {
        throw new IllegalStateException("Object found in JNDI " + jmsAttributes.getJndiDestinationName()
            + " does not match declared type 'topic'.");
      }
    }

    try {
      responseDestination = (Destination) ictx.lookup(jmsAttributes.getJndiResponseDestinationName());
    } catch (NameNotFoundException nnfe) {
      responseDestination = responseSession.createQueue(jmsAttributes.getJndiResponseDestinationName());
      ictx.bind(jmsAttributes.getJndiResponseDestinationName(), responseDestination);
    }

    ictx.close();

  }

  public void close() throws JMSException {
    if (jmsCnx != null) {
      jmsCnx.close();
    }
  }

}
