/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.util.Map;

import org.objectweb.fractal.bf.connectors.common.GenericBindHints;
import org.objectweb.fractal.bf.connectors.common.GenericConnector;
import org.objectweb.fractal.bf.connectors.common.GenericExportHints;

/**
 * A JMS-Connector can export and bind Fractal interfaces via JMS.
 */
public class JmsConnector extends GenericConnector {

  /**
   * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getPluginIdentifier()
   */
  public final String getPluginIdentifier() {
    return "jms";
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getSkeletonAdl()
   */
  @Override
  protected final String getSkeletonAdl() {
    return "org.objectweb.fractal.bf.connectors.jms.JmsSkeleton";
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initSkeletonAdlContext(EH,
   *      Map)
   */
  @Override
  protected final void initSkeletonAdlContext(GenericExportHints exportHints, Map<String, Object> context) {
    String uri = (String) exportHints.get(JmsConnectorConstants.URI, JmsConnectorConstants.NO_URI);
    context.put(JmsConnectorConstants.URI, uri);

    String correlationScheme = (String) exportHints.get(JmsConnectorConstants.JMS_CORRELATION_SCHEME,
        JmsConnectorConstants.JMS_CORRELATION_SCHEME_DEFAULT);
    context.put(JmsConnectorConstants.JMS_CORRELATION_SCHEME, correlationScheme);

    String jndiURL = (String) exportHints.get(JmsConnectorConstants.JNDI_URL,
        JmsConnectorConstants.NO_JNDI_URL);
    context.put(JmsConnectorConstants.JNDI_URL, jndiURL);

    String jndiInitialContextFactory = (String) exportHints.get(JmsConnectorConstants.JNDI_INITCONTEXTFACT,
        JmsConnectorConstants.NO_JNDI_INITCONTEXTFACT);
    context.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, jndiInitialContextFactory);

    String jndiConnFactName = (String) exportHints.get(JmsConnectorConstants.JNDI_CONNFACTNAME,
        JmsConnectorConstants.JNDI_CONNFACTNAME_DEFAULT);
    context.put(JmsConnectorConstants.JNDI_CONNFACTNAME, jndiConnFactName);

    String jndiDestName = (String) exportHints.get(JmsConnectorConstants.JNDI_DESTINATION_NAME,
        JmsConnectorConstants.NO_JNDI_URL);
    context.put(JmsConnectorConstants.JNDI_DESTINATION_NAME, jndiDestName);

    String destType = (String) exportHints.get(JmsConnectorConstants.DESTINATION_TYPE,
        JmsConnectorConstants.NO_TYPE);
    context.put(JmsConnectorConstants.DESTINATION_TYPE, destType);

    String createDest = (String) exportHints.get(JmsConnectorConstants.CREATE_DESTINATION_MODE,
        JmsConnectorConstants.CREATE_DEFAULT);
    context.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, createDest);

    String jmsPriority = (String) exportHints.get(JmsConnectorConstants.JMS_PRIORITY,
        JmsConnectorConstants.JMS_PRIORITY_DEFAULT);
    context.put(JmsConnectorConstants.JMS_PRIORITY, jmsPriority);

    String timeToLive = (String) exportHints.get(JmsConnectorConstants.JMS_TTL,
        JmsConnectorConstants.JMS_TTL_DEFAULT);
    context.put(JmsConnectorConstants.JMS_TTL, timeToLive);

    String selector = (String) exportHints.get(JmsConnectorConstants.JMS_SELECTOR,
        JmsConnectorConstants.JMS_SELECTOR_DEFAULT);
    context.put(JmsConnectorConstants.JMS_SELECTOR, selector);

    String deliveryMode = (String) exportHints.get(JmsConnectorConstants.JMS_DELIVERY_MODE,
        JmsConnectorConstants.JMS_DELIVERY_MODE_DEFAULT);
    if (deliveryMode.equalsIgnoreCase(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT)) {
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "true");
    } else if (deliveryMode.equalsIgnoreCase(JmsConnectorConstants.JMS_DELIVERY_MODE_NONPERSISTENT)) {
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "false");
    } else {
      log.warning("Unrecognized delivery mode, set to persistent");
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "true");
    }

    String jndiResponseDestName = (String) exportHints.get(
        JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME,
        JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME_DEFAULT);
    context.put(JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME, jndiResponseDestName);
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#getStubAdl()
   */
  @Override
  protected final String getStubAdl() {
    return "org.objectweb.fractal.bf.connectors.jms.JmsStub";
  }

  /**
   * @see org.objectweb.fractal.bf.connectors.common.AbstractConnector#initStubAdlContext(BH,
   *      Map)
   */
  @Override
  protected final void initStubAdlContext(GenericBindHints bindHints, Map<String, Object> context) {
    generateStubContentClass(JmsStubContent.class, context);
    String uri = (String) bindHints.get(JmsConnectorConstants.URI, JmsConnectorConstants.NO_URI);
    context.put(JmsConnectorConstants.URI, uri);

    String correlationScheme = (String) bindHints.get(JmsConnectorConstants.JMS_CORRELATION_SCHEME,
        JmsConnectorConstants.JMS_CORRELATION_SCHEME_DEFAULT);
    context.put(JmsConnectorConstants.JMS_CORRELATION_SCHEME, correlationScheme);

    String jndiURL = (String) bindHints
        .get(JmsConnectorConstants.JNDI_URL, JmsConnectorConstants.NO_JNDI_URL);
    context.put(JmsConnectorConstants.JNDI_URL, jndiURL);

    String jndiInitialContextFactory = (String) bindHints.get(JmsConnectorConstants.JNDI_INITCONTEXTFACT,
        JmsConnectorConstants.NO_JNDI_INITCONTEXTFACT);
    context.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, jndiInitialContextFactory);

    String jndiConnFactName = (String) bindHints.get(JmsConnectorConstants.JNDI_CONNFACTNAME,
        JmsConnectorConstants.JNDI_CONNFACTNAME_DEFAULT);
    context.put(JmsConnectorConstants.JNDI_CONNFACTNAME, jndiConnFactName);

    String jndiDestName = (String) bindHints.get(JmsConnectorConstants.JNDI_DESTINATION_NAME,
        JmsConnectorConstants.NO_JNDI_URL);
    context.put(JmsConnectorConstants.JNDI_DESTINATION_NAME, jndiDestName);

    String createDest = (String) bindHints.get(JmsConnectorConstants.CREATE_DESTINATION_MODE,
        JmsConnectorConstants.CREATE_DEFAULT);
    context.put(JmsConnectorConstants.CREATE_DESTINATION_MODE, createDest);

    String destType = (String) bindHints.get(JmsConnectorConstants.DESTINATION_TYPE,
        JmsConnectorConstants.NO_TYPE);
    context.put(JmsConnectorConstants.DESTINATION_TYPE, destType);

    String jmsPriority = (String) bindHints.get(JmsConnectorConstants.JMS_PRIORITY,
        JmsConnectorConstants.JMS_PRIORITY_DEFAULT);
    context.put(JmsConnectorConstants.JMS_PRIORITY, jmsPriority);

    String timeToLive = (String) bindHints.get(JmsConnectorConstants.JMS_TTL,
        JmsConnectorConstants.JMS_TTL_DEFAULT);
    context.put(JmsConnectorConstants.JMS_TTL, timeToLive);

    String selector = (String) bindHints.get(JmsConnectorConstants.JMS_SELECTOR,
        JmsConnectorConstants.JMS_SELECTOR_DEFAULT);
    context.put(JmsConnectorConstants.JMS_SELECTOR, selector);

    String deliveryMode = (String) bindHints.get(JmsConnectorConstants.JMS_DELIVERY_MODE,
        JmsConnectorConstants.JMS_DELIVERY_MODE_DEFAULT);
    if (deliveryMode.equalsIgnoreCase(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT)) {
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "true");
    } else if (deliveryMode.equalsIgnoreCase(JmsConnectorConstants.JMS_DELIVERY_MODE_NONPERSISTENT)) {
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "false");
    } else {
      log.warning("Unrecognized delivery mode, set to persistent");
      context.put(JmsConnectorConstants.JMS_DELIVERY_MODE_PERSISTENT, "true");
    }

    String jndiResponseDestName = (String) bindHints.get(
        JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME,
        JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME_DEFAULT);
    context.put(JmsConnectorConstants.JNDI_RESPONSE_DESTINATION_NAME, jndiResponseDestName);
  }

}
