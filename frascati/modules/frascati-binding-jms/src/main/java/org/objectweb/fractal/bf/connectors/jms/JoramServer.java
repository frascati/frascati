/**
 * OW2 FraSCAti SCA Binding JMS
 * Copyright (C) 2010-2012 ScalAgent Distributed Technologies, Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Guillaume Surrel
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.net.URI;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.objectweb.joram.client.jms.Queue;
import org.objectweb.joram.client.jms.Topic;
import org.objectweb.joram.client.jms.admin.AdminModule;
import org.objectweb.joram.client.jms.admin.User;
import org.objectweb.joram.client.jms.tcp.TcpConnectionFactory;
import fr.dyade.aaa.agent.AgentServer;

/**
 * This class is used to launch a collocated JORAM server in case the external
 * JMS server does not respond.
 */
public class JoramServer
{
	public static int TCP_CONNECTION_FACTORY_PORT = 16010;

	private static boolean started = false;

	/** Logger for this object. */
	protected static Logger log = Logger.getLogger(JoramServer.class
			.getCanonicalName());

	/**
	 * Starts a collocated JORAM server.
	 */
	public static void start()
	{

		if (started)
		{
			System.out.println("JORAM server has already been started.");
			return;
		}

		System.out.println("Starting JORAM...");

		try
		{
			AgentServer.init(new String[] { "0", "target/s0" });
			AgentServer.start();

			AdminModule.connect("root", "root", 60);

			Queue queue = Queue.create("queue");
			Topic topic = Topic.create("topic");

			User.create("anonymous", "anonymous");

			queue.setFreeReading();
			topic.setFreeReading();
			queue.setFreeWriting();
			topic.setFreeWriting();

			ConnectionFactory cf = TcpConnectionFactory.create("localhost",
					TCP_CONNECTION_FACTORY_PORT);

			Context jndiCtx = new InitialContext();
			jndiCtx.bind("cf", cf);
			jndiCtx.bind("queue", queue);
			jndiCtx.bind("topic", topic);
			jndiCtx.close();

			AdminModule.disconnect();

			started = true;
		} catch (Exception e)
		{
			throw new IllegalStateException(
					"JORAM server could not be started properly: "
							+ e.getMessage());
		}
	}

	public static void start(String CorrelationScheme)
	{

		if (started)
		{
			System.out.println("JORAM server has already been started.");
			return;
		}

		System.out.println("Starting JORAM...");

		try
		{
			AgentServer.init(new String[] { "0", "target/s0" });
			AgentServer.start();

			AdminModule.connect("root", "root", 60);

			Queue queue = Queue.create("queue");
			Topic topic = Topic.create("topic");

			User.create("anonymous", "anonymous");

			queue.setFreeReading();
			topic.setFreeReading();
			queue.setFreeWriting();
			topic.setFreeWriting();
			// Innitaliase the connectionfactory
			ConnectionFactory cf = null;
			
			try
			{
				URI uri=URI.create(CorrelationScheme);
				cf = TcpConnectionFactory.create(uri.getHost(), TCP_CONNECTION_FACTORY_PORT);
			}
			catch(IllegalArgumentException e)
			{
				cf = TcpConnectionFactory.create("localhost", TCP_CONNECTION_FACTORY_PORT);
			}
			
			Context jndiCtx = new InitialContext();
			jndiCtx.bind("cf", cf);
			jndiCtx.bind("queue", queue);
			jndiCtx.bind("topic", topic);
			jndiCtx.close();

			AdminModule.disconnect();

			started = true;
		} catch (Exception e)
		{
			e.printStackTrace();
			throw new IllegalStateException(
					"JORAM server could not be started properly: "
							+ e.getMessage());
		}
	}

	/** Stops the collocated JORAM server. */
	public static void stop() throws Exception
	{
		AgentServer.stop();
	}
}
