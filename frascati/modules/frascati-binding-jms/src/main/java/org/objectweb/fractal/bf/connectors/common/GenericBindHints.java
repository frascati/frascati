/**
 * OW2 Fractal Binding Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: fractal@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import java.util.Map;

import org.objectweb.fractal.bf.BindHints;

/**
 * The generic bind hints class.
 *
 * TODO: Should be moved into the OW2 Fractal Binding Factory SVN.
 */
public class GenericBindHints implements BindHints {

	private Map<String, Object> hints;

	public GenericBindHints(Map<String, Object> hints) {
		this.hints = hints;
	}

	public final Object get(String key, Object defaultValue) {
		Object result = hints.get(key);
		return result != null ? result : defaultValue;
	}

	/**
	 * @see Object#toString()
	 */
	@Override
	public final String toString() {
		StringBuffer sb = new StringBuffer("[");
		sb.append(hints);
		return sb.toString();
	}

}
