/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

/**
 * JMS specific attributes.
 */
public interface JmsAttributes {

  /**
   * Get the jndiURL used to configure the stub/skeleton.
   * 
   * @return the jndiURL used to configure the stub/skeleton.
   */
  public String getJndiURL();

  /**
   * Set the jndiURL used to configure the stub/skeleton.
   * 
   * @param jndiURL
   */
  public void setJndiURL(String jndiURL);

  /**
   * Gets the JNDI name of the destination to use.
   * 
   * @return the JNDI name of the destination to use.
   */
  public String getJndiDestinationName();

  /**
   * Sets the JNDI name of the destination to use.
   * 
   * @param jndiDestinationName
   *          the JNDI name of the destination.
   */
  public void setJndiDestinationName(String jndiDestinationName);

  /**
   * Gets the JNDI initial context factory.
   * 
   * @return the JNDI initial context factory.
   */
  public String getJndiInitialContextFactory();

  /**
   * Sets the JNDI initial context factory.
   * 
   * @param jndiInitialContextFactory
   *          the JNDI initial context factory.
   */
  public void setJndiInitialContextFactory(String jndiInitialContextFactory);

  /**
   * Gets the JNDI name of the connection factory to use.
   * 
   * @return the JNDI name of the connection factory to use.
   */
  public String getJndiConnectionFactoryName();

  /**
   * Sets the JNDI name of the connection factory to use.
   * 
   * @param jndiConnFactName
   *          the JNDI name of the connection factory.
   */
  public void setJndiConnectionFactoryName(String jndiConnFactName);

  /**
   * Gets the priority of messages sent by the stub/skeleton.
   * 
   * @return the priority of messages sent by the stub/skeleton.
   */
  public int getPriority();

  /**
   * Sets the priority of messages sent by the stub/skeleton.
   * 
   * @param priority
   *          the priority of messages sent by the stub/skeleton.
   */
  public void setPriority(int priority);

  /**
   * Gets the living time of messages sent by the stub/skeleton.
   * 
   * @return the living time of messages sent by the stub/skeleton.
   */
  public long getTimeToLive();

  /**
   * Sets the living time of messages sent by the stub/skeleton.
   * 
   * @param timeToLive
   *          the living time of messages sent by the stub/skeleton.
   */
  public void setTimeToLive(long timeToLive);

  /**
   * Gets the selector used to consume messages.
   * 
   * @return the selector used when consume messages.
   */
  public String getSelector();

  /**
   * Sets the selector used to consume messages.
   * 
   * @param selector
   *          the selector used to consume messages.
   */
  public void setSelector(String selector);

  /**
   * Gets if messages sent by the stub/skeleton are persistent.
   * 
   * @return true if messages sent by the stub/skeleton are persistent.
   */
  public boolean getPersistent();

  /**
   * Sets the persistence of messages sent by the stub/skeleton.
   * 
   * @param persistent
   *          true if messages sent by the stub/skeleton must be persistent.
   */
  public void setPersistent(boolean persistent);

  /**
   * Gets the mode used to create destination: always, never, or ifNotExist
   * 
   * @return the mode used to create destination.
   * @see JmsConnectorConstants#CREATE_ALWAYS
   * @see JmsConnectorConstants#CREATE_NEVER
   * @see JmsConnectorConstants#CREATE_IFNOTEXIST
   */
  public String getCreateDestinationMode();

  /**
   * Sets the mode used to create destination always, never, or ifNotExist
   * 
   * @param mode
   *          the mode used to create destination.
   * @see JmsConnectorConstants#CREATE_ALWAYS
   * @see JmsConnectorConstants#CREATE_NEVER
   * @see JmsConnectorConstants#CREATE_IFNOTEXIST
   */
  public void setCreateDestinationMode(String mode);

  /**
   * Gets the type of the created destination: queue or topic.
   * 
   * @return the type of the created destination.
   * @see JmsConnectorConstants#TYPE_QUEUE
   * @see JmsConnectorConstants#TYPE_TOPIC
   */
  public String getDestinationType();

  /**
   * Sets the type of the created destination: queue or topic.
   * 
   * @param type
   *          the type of the created destination.
   * @see JmsConnectorConstants#TYPE_QUEUE
   * @see JmsConnectorConstants#TYPE_TOPIC
   */
  public void setDestinationType(String type);

  /**
   * Gets the JNDI name of the destination on which responses will be send.
   * 
   * @return the JNDI name of the response destination
   */
  public String getJndiResponseDestinationName();

  /**
   * Sets the JNDI name of the destination on which responses will be send.
   * 
   * @param name
   *          the JNDI name of the response destination
   */
  public void setJndiResponseDestinationName(String name);

  /**
   * Gets the correlation scheme used by the stub/skeleton: messageID, correlationID or
   * none
   * 
   * @return the correlation scheme
   * @see JmsConnectorConstants#MESSAGE_ID_SCHEME
   * @see JmsConnectorConstants#CORRELATION_ID_SCHEME
   */
  public String getCorrelationScheme();

  /**
   * Sets the correlation scheme used by the stub/skeleton: messageID, correlationID or
   * none
   * 
   * @param scheme
   *          the correlation scheme
   * @see JmsConnectorConstants#MESSAGE_ID_SCHEME
   * @see JmsConnectorConstants#CORRELATION_ID_SCHEME
   */
  public void setCorrelationScheme(String scheme);

}
