/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010-2012 ScalAgent Distributed Technologies, Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ow2.frascati.wsdl.AbstractWsdlInvocationHandler;
import org.ow2.frascati.wsdl.WsdlDelegate;
import org.ow2.frascati.wsdl.WsdlDelegateFactory;

/**
 * These classes simulate a very basic and poor JMS broker.
 *
 * TODO: Uses JORAM instead of simulating it.
 *
 * @author Philippe Merle - Inria
 *
 */
public final class JmsBroker
{
  /** Map of all <uri, queue> tuples. */
  private static Map<String, JmsQueue> uri2queue = new HashMap<String, JmsQueue>();

  /** Map of all <servant, listener> tuples. */
  private static Map<Object, JmsListener> servant2listener = new HashMap<Object, JmsListener>();

  /**
   * Private constructor to avoid to instantiate this utility class.
   */
  private JmsBroker() {}

  /**
   * Called by JmsSkeletonContent to register a servant instance at a given uri.
   */
  public static synchronized void registerSkeleton(String uri, Object servant, Class<?> interfaze)
  {
    // Get the JmsQueue instance for the given uri.
    JmsQueue queue = getJmsQueue(uri);
    // Create a JmsListener instance for the given servant instance, and connected to the queue.
    JmsListener listener = new JmsListener(servant, interfaze, queue);
    // Keep the association <servant, listener>.
    servant2listener.put(servant, listener);
  }

  /**
   * Called by JmsSkeletonContent to unregister a servant instance at a given uri.
   */
  public static synchronized void unregisterSkeleton(String uri, Object servant)
  {
    // Forget the association <servant, listener>
    JmsListener listener = servant2listener.remove(servant);
    // Remove the listener from its queue.
    listener.queue.listeners.remove(listener);
  }

  /**
   * Called by JmsStubContent to create a typed stub to send messages.
   */
  public static Object createStub(final String uri, Class<?> clazz)
  {
    // Create a Jms invocation handler.
    JmsStubInvocationHandler2 ih = new JmsStubInvocationHandler2();
    // Connect it to the queue at a given uri.
    ih.queue = getJmsQueue(uri);
    // Create a Java dynamic proxy with the created invocation handler.
    return Proxy.newProxyInstance(
        clazz.getClassLoader(),
        new Class<?>[] { clazz },
        ih);
  }

  /**
   * Get the queue associated to a given uri.
   */
  private static synchronized JmsQueue getJmsQueue(String uri)
  {
    // Get the queue from the map of <uri, queue>.
    JmsQueue queue = uri2queue.get(uri);
    if(queue == null) {
      // If no queue then create a new one and put it into the map.
      queue = new JmsQueue();
      uri2queue.put(uri, queue);
    }
    return queue;
  }

}

/**
 * JMS stub invocation handler inherits from a generic WSDL invocation handler.
 *
 * @author Philippe Merle - INRIA
 */
class JmsStubInvocationHandler2 extends AbstractWsdlInvocationHandler {

  /**
   * The queue where messages are sent.
   */
  protected JmsQueue queue;

  /**
   * #see java.lang.reflect.InvocationHandler#invoke(Object, Method, Object[])
   */
  public final Object invoke(Object proxy, Method method, Object[] args)
    throws Throwable
  {
    // Create a JMS message.
    JmsMessage message = new JmsMessage();
    // Store the invoked method name into the message.
    message.methodName = method.getName();

    if(args == null || args.length == 0) {
      message.xmlMessage = null;
    } else {
      // If args then marshall them as an XML message.
      message.xmlMessage = marshallInvocation(method, args);
    }
    log.fine("Invoked method is '" + message.methodName + "'");
    log.fine("Marshalled XML message is " + message.xmlMessage);
    // Send the message to the queue.
    queue.send(message);
  	return null;
  }

}

/**
 * Very basic JMS message.
 *
 * @author Philippe Merle - INRIA
 */
class JmsMessage
{
  protected String methodName;
  protected String xmlMessage;
}

/**
 * A very basic JMS queue.
 *
 * @author Philippe Merle - INRIA
 */
class JmsQueue
{
  /**
   * List of JMS listeners receiving JMS messages sent to this JMS queue.
   */
  List<JmsListener> listeners = new ArrayList<JmsListener>();

  /**
   * Send a JMS message to this queue.
   */
  protected final void send(JmsMessage msg) throws Exception
  {
    // Push the given message to each listener of this queue.
	for(JmsListener listener : this.listeners) {
	  listener.receive(msg);
	}
  }

}

/**
 * Very simple JMS listener.
 *
 * @author Philippe Merle - INRIA
 */
class JmsListener
{
  /**
   * The listen queue.
   */
  JmsQueue queue;

  /**
   * Encapsulation of the servant receiving Java invocations.
   */
  WsdlDelegate delegate;

  /**
   * Constructs a new JmsListener instance.
   */
  protected JmsListener(Object servant, Class<?> interfaze, JmsQueue queue)
  {
    // Store the listen queue.
    this.queue = queue;
    // Add this listener to listeners of the queue.
    queue.listeners.add(this);
    // Create a WSDL delegate that decodes XML messages and calls the given Java servant.
    this.delegate = WsdlDelegateFactory.newWsdlDelegate(servant, interfaze, servant.getClass().getClassLoader());
  }

  /**
   * Receive a JMS message from the listen JMS queue.
   */
  protected final void receive(JmsMessage msg) throws Exception
  {
    Method method = this.delegate.getMethod(msg.methodName);
	if(msg.xmlMessage == null) {
      // If no XML message then invoke the servant directly.
      method.invoke(this.delegate.getDelegate(), null);
	} else {
      // If an XML message then uses the WSDL delegate to decode the XML message and invoke the servant.
	  this.delegate.invoke(method, msg.xmlMessage);
	}
  }

}
