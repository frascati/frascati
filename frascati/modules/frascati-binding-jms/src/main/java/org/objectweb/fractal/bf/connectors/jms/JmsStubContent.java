/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.lang.reflect.Proxy;

import javax.jms.JMSException;
import javax.jms.MessageProducer;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.connectors.common.uri.UriStubContent;

/**
 * A super class for JMS stubs. Concrete classes are generated on the fly.
 */
public abstract class JmsStubContent extends UriStubContent implements JmsStubContentAttributes {

  /** The correlation scheme used: messageID, correlationID or none. */
  private String correlationScheme;

  /** The jndiURL to configure the binding. */
  private String jndiURL;

  /** The JNDI name of the destination used by the binding. */
  private String jndiDestinationName;

  /** The mode used to create the destination: always, never or ifNotExist. */
  private String createDestinationMode;

  /** The destination type: queue or topic. */
  private String destinationType;

  /** The initial context factory used by the binding. */
  private String jndiInitialContextFactory;

  /** The JNDI connection factory name. */
  private String jndiConnFactName;

  /** True if messages produced by the binding are persistent. */
  private boolean persistent;

  /** The time to live of messages produced by the binding. */
  private long timeToLive;

  /** The priority of messages produced by the binding. */
  private int priority;

  /** The selector used by the binding to select received messages. */
  private String selector;

  /** The message producer used to send messages produced by the binding. */
  private MessageProducer producer;

  /** The JNDI name of the response destination. */
  private String jndiResponseDestinationName;

  /** The JMS module containing JMS objects used to send and receive messages. */
  private JmsModule jmsModule;

  /**
   * @see JmsStubContentAttributes#getCorrelationScheme()
   */
  public String getCorrelationScheme() {
    return correlationScheme;
  }

  /**
   * @see JmsStubContentAttributes#setCorrelationScheme(String)
   */
  public void setCorrelationScheme(String correlationScheme) {
    this.correlationScheme = correlationScheme;
  }

  /**
   * @see JmsStubContentAttributes#getJndiDestinationName()
   */
  public String getJndiDestinationName() {
    return jndiDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiDestinationName(String)
   */
  public void setJndiDestinationName(String jndiDestinationName) {
    this.jndiDestinationName = jndiDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#getCreateDestinationMode()
   */
  public String getCreateDestinationMode() {
    return createDestinationMode;
  }

  /**
   * @see JmsStubContentAttributes#setCreateDestinationMode(String)
   */
  public void setCreateDestinationMode(String createDestinationMode) {
    this.createDestinationMode = createDestinationMode;
  }

  /**
   * @see JmsStubContentAttributes#getDestinationType()
   */
  public String getDestinationType() {
    return destinationType;
  }

  /**
   * @see JmsStubContentAttributes#setDestinationType(String)
   */
  public void setDestinationType(String destinationType) {
    this.destinationType = destinationType;
  }

  /**
   * @see JmsStubContentAttributes#getJndiInitialContextFactory()
   */
  public String getJndiInitialContextFactory() {
    return jndiInitialContextFactory;
  }

  /**
   * @see JmsStubContentAttributes#setJndiInitialContextFactory(String)
   */
  public void setJndiInitialContextFactory(String jndiInitialContextFactory) {
    this.jndiInitialContextFactory = jndiInitialContextFactory;
  }

  /**
   * @see JmsStubContentAttributes#getJndiConnectionFactoryName()
   */
  public String getJndiConnectionFactoryName() {
    return jndiConnFactName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiConnectionFactoryName(String)
   */
  public void setJndiConnectionFactoryName(String jndiConnFactName) {
    this.jndiConnFactName = jndiConnFactName;
  }

  /**
   * @see JmsStubContentAttributes#getPriority()
   */
  public int getPriority() {
    return priority;
  }

  /**
   * @see JmsStubContentAttributes#setPriority(int)
   */
  public void setPriority(int priority) {
    this.priority = priority;
  }

  /**
   * @see JmsStubContentAttributes#getTimeToLive()
   */
  public long getTimeToLive() {
    return timeToLive;
  }

  /**
   * @see JmsStubContentAttributes#setTimeToLive(long)
   */
  public void setTimeToLive(long timeToLive) {
    this.timeToLive = timeToLive;
  }

  /**
   * @see JmsStubContentAttributes#getSelector()
   */
  public String getSelector() {
    return selector;
  }

  /**
   * @see JmsStubContentAttributes#setSelector(String)
   */
  public void setSelector(String selector) {
    this.selector = selector;
  }

  /**
   * @see JmsStubContentAttributes#getPersistent()
   */
  public boolean getPersistent() {
    return persistent;
  }

  /**
   * @see JmsStubContentAttributes#setPersistent(boolean)
   */
  public void setPersistent(boolean persistent) {
    this.persistent = persistent;
  }

  /**
   * @see JmsStubContentAttributes#getJndiURL()
   */
  public String getJndiURL() {
    return this.jndiURL;
  }

  /**
   * @see JmsStubContentAttributes#setJndiURL(String)
   */
  public void setJndiURL(String jndiURL) {
    this.jndiURL = jndiURL;
  }

  /**
   * @see JmsStubContentAttributes#getJndiResponseDestinationName()
   */
  public String getJndiResponseDestinationName() {
    return jndiResponseDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiResponseDestinationName(String)
   */
  public void setJndiResponseDestinationName(String jndiResponseDestinationName) {
    this.jndiResponseDestinationName = jndiResponseDestinationName;
  }

  /**
   * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
   */
  @Override
  public void startFc() throws IllegalLifeCycleException {

    try {

      jmsModule = new JmsModule(this);
      jmsModule.start();

      producer = jmsModule.getSession().createProducer(jmsModule.getDestination());

    } catch (Exception exc) {
      throw new IllegalStateException("Error starting JMS Stub -> " + exc.getMessage(), exc);
    }

    super.startFc();
  }

  /**
   * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
   */
  @Override
  public void stopFc() {
    try {
      jmsModule.close();
    } catch (JMSException exc) {
      log.severe("stopFc -> " + exc.getMessage());
    }
    super.stopFc();
  }

  /**
   * Build a JMS client.
   * 
   * @return the object instantiated.
   */
  protected final Object resolveReference() {
    // Create a Jms invocation handler.
    JmsStubInvocationHandler ih = new JmsStubInvocationHandler(jmsModule, getPersistent(), getPriority(),
        getTimeToLive(), producer);

    // Create a Java dynamic proxy with the created invocation handler.
    return Proxy.newProxyInstance(getServiceClass().getClassLoader(), new Class<?>[] { getServiceClass() }, ih);

  }

}
