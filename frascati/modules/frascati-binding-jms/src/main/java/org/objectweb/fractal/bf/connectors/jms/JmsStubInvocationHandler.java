/**
 * OW2 FraSCAti SCA Binding JMS
 * Copyright (C) 2010 - 2011 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Guillaume Surrel
 *
 * Contributor(s):
 *
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.jms.DeliveryMode;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;

import org.ow2.frascati.jaxb.JAXB;
import org.ow2.frascati.wsdl.AbstractWsdlInvocationHandler;

/**
 * The invocation handler for the JMS binding.
 */
public class JmsStubInvocationHandler extends AbstractWsdlInvocationHandler {

  /** True if messages produced are persistent. */
  private boolean persistent;

  /** The time to live of produced messages. */
  private long timeToLive;

  /** The priority of produced messages. */
  private int priority;

  /** The message producer used to send messages. */
  private MessageProducer producer;

  /** The JMS module containing JMS objects used to send a message and receive a response. */
  private JmsModule jmsModule;

  public JmsStubInvocationHandler(JmsModule jmsModule, boolean persistent, int priority, long ttl,
      MessageProducer producer) {
    super();
    this.jmsModule = jmsModule;
    this.persistent = persistent;
    this.priority = priority;
    this.timeToLive = ttl;
    this.producer = producer;
  }

  /**
   * Sends a message in order to call the given method on the remote JMS service.
   */
  public Object invoke(Object proxy, Method m, Object[] args) throws Throwable {

    log.fine("Send message");

    TextMessage msg = jmsModule.getSession().createTextMessage();

    msg.setStringProperty(JmsConnectorConstants.OPERATION_SELECTION_PROPERTY, m.getName());
    msg.setJMSReplyTo(jmsModule.getResponseDestination());
    if (args != null) {
      msg.setText(marshallInvocation(m, args));
    }

    producer.send(msg, persistent ? DeliveryMode.PERSISTENT : DeliveryMode.NON_PERSISTENT, priority,
        timeToLive);

    // Return if no response is expected
    if (m.getReturnType().equals(void.class) && m.getExceptionTypes().length == 0) {
      return null;
    }

    String selector = "JMSCorrelationID = '" + msg.getJMSMessageID() + "'";

    Session respSession = jmsModule.getJmsCnx().createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer responseConsumer = respSession.createConsumer(jmsModule.getResponseDestination(), selector);
    Message responseMsg = responseConsumer.receive();

    responseConsumer.close();
    respSession.close();

    log.fine("Response received. " + ((TextMessage) responseMsg).getText());

    // TODO use a WSDLDelegate to unmarshall response
    Object response = JAXB
        .unmarshall(getQNameOfFirstArgument(m), ((TextMessage) responseMsg).getText(), null);

    return response;
  }

  /**
   * Get the targetNamespace of the @WebService annotating the class of the given method.
   */
  protected static String getTargetNamespace(Method method) {
    WebService ws = method.getDeclaringClass().getAnnotation(WebService.class);
    return ws.targetNamespace();
  }

  /**
   * Compute the qname of a given @WebParam.
   */
  protected static QName toQName(WebParam wp, Method method) {
    String targetNamespace = wp.targetNamespace();
    if (targetNamespace == null || targetNamespace.equals("")) {
      targetNamespace = getTargetNamespace(method);
    }
    return new QName(targetNamespace, wp.name());
  }

  protected static QName getQNameOfFirstArgument(Method method) throws Exception {
    // Compute the qname of the @WebParam attached to the first method parameter.
    Annotation[][] pa = method.getParameterAnnotations();
    WebParam wp = (WebParam) pa[0][0];
    return toQName(wp, method);
  }
}
