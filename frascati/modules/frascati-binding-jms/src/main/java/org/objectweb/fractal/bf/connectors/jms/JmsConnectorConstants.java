/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Guillaume Surrel
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.objectweb.fractal.bf.connectors.jms;

import org.objectweb.fractal.bf.connectors.common.uri.UriConnectorConstants;

/**
 * Constants to configure JMS bindings.
 */
public interface JmsConnectorConstants extends UriConnectorConstants {

  public static final String NO_URI = "no_uri";

  public static final String JNDI_URL = "jndiURL";

  public static final String NO_JNDI_URL = "no_jndiURL";

  public static final String JNDI_DESTINATION_NAME = "jndiDestinationName";

  public static final String CREATE_DESTINATION_MODE = "createDestinationMode";

  public static final String CREATE_ALWAYS = "always";

  public static final String CREATE_NEVER = "never";

  public static final String CREATE_IFNOTEXIST = "ifNotExist";

  public static final String CREATE_DEFAULT = CREATE_IFNOTEXIST;

  public static final String DESTINATION_TYPE = "destinationType";

  public static final String TYPE_QUEUE = "queue";

  public static final String TYPE_TOPIC = "topic";

  public static final String NO_TYPE = "no_type";

  public static final String DESTINATION_TYPE_DEFAULT = TYPE_QUEUE;

  public static final String JNDI_INITCONTEXTFACT = "jndiInitialContextFactory";

  public static final String NO_JNDI_INITCONTEXTFACT = "no_ContextFactory";

  public static final String JNDI_CONNFACTNAME = "jndiConnectionFactoryName";

  public static final String JNDI_CONNFACTNAME_DEFAULT = "cf";

  public static final String JMS_DELIVERY_MODE = "deliveryMode";

  public static final String JMS_DELIVERY_MODE_PERSISTENT = "persistent";

  public static final String JMS_DELIVERY_MODE_NONPERSISTENT = "nonpersistent";

  public static final String JMS_DELIVERY_MODE_DEFAULT = JMS_DELIVERY_MODE_PERSISTENT;

  public static final String JMS_TTL = "timeToLive";

  public static final String JMS_TTL_DEFAULT = "0";

  public static final String JMS_PRIORITY = "priority";

  public static final String JMS_PRIORITY_DEFAULT = "4";

  public static final String JMS_SELECTOR = "selector";

  public static final String JMS_SELECTOR_DEFAULT = "";

  public static final String JNDI_RESPONSE_DESTINATION_NAME = "jndiResponseDestinationName";

  public static final String JNDI_RESPONSE_DESTINATION_NAME_DEFAULT = "scaResponseDestination";

  public static final String OPERATION_SELECTION_PROPERTY = "scaOperationName";

  public static final String JMS_CORRELATION_SCHEME = "correlationScheme";

  public static final String CORRELATION_ID_SCHEME = "correlationID";

  public static final String MESSAGE_ID_SCHEME = "messageID";

  public static final String JMS_CORRELATION_SCHEME_DEFAULT = MESSAGE_ID_SCHEME;

}
