/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010-2012 ScalAgent Distributed Technologies, Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 * 
 */
package org.objectweb.fractal.bf.connectors.jms;

import java.lang.reflect.Method;
import java.util.logging.Level;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import org.objectweb.fractal.bf.connectors.common.uri.UriSkeletonContent;
import org.ow2.frascati.wsdl.WsdlDelegate;
import org.ow2.frascati.wsdl.WsdlDelegateFactory;

/**
 * The content implementation of JMS skeleton components.
 */
public class JmsSkeletonContent extends UriSkeletonContent implements JmsSkeletonContentAttributes,
    MessageListener {

  /** The correlation scheme used: messageID, correlationID or none. */
  private String correlationScheme;

  /** The jndiURL to configure the binding. */
  private String jndiURL;

  /** The JNDI name of the destination used by the binding. */
  private String jndiDestinationName;

  /** The mode used to create the destination: always, never or ifNotExist. */
  private String createDestinationMode;

  /** The destination type: queue or topic. */
  private String destinationType;

  /** The initial context factory used by the binding. */
  private String jndiInitialContextFactory;

  /** The JNDI connection factory name. */
  private String jndiConnFactName;

  /** True if messages produced by the binding are persistent. */
  private boolean persistent;

  /** The time to live of messages produced by the binding. */
  private long timeToLive;

  /** The priority of messages produced by the binding. */
  private int priority;

  /** The selector used by the binding to select received messages. */
  private String selector;

  /** A WSDL delegate that decodes XML messages and calls the given Java servant. */
  private WsdlDelegate delegate;

  /** The JNDI name of the response destination. */
  private String jndiResponseDestinationName;

  /** The JMS module containing JMS objects used to send and receive messages. */
  private JmsModule jmsModule;

  /**
   * @see JmsStubContentAttributes#getCorrelationScheme()
   */
  public String getCorrelationScheme() {
    return correlationScheme;
  }

  /**
   * @see JmsStubContentAttributes#setCorrelationScheme(String)
   */
  public void setCorrelationScheme(String correlationScheme) {
    this.correlationScheme = correlationScheme;
  }

  /**
   * @see JmsStubContentAttributes#getJndiDestinationName()
   */
  public String getJndiDestinationName() {
    return jndiDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiDestinationName(String)
   */
  public void setJndiDestinationName(String jndiDestinationName) {
    this.jndiDestinationName = jndiDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#getCreateDestinationMode()
   */
  public String getCreateDestinationMode() {
    return createDestinationMode;
  }

  /**
   * @see JmsStubContentAttributes#setCreateDestinationMode(String)
   */
  public void setCreateDestinationMode(String createDestinationMode) {
    this.createDestinationMode = createDestinationMode;
  }

  /**
   * @see JmsStubContentAttributes#getDestinationType()
   */
  public String getDestinationType() {
    return destinationType;
  }

  /**
   * @see JmsStubContentAttributes#setDestinationType(String)
   */
  public void setDestinationType(String destinationType) {
    this.destinationType = destinationType;
  }

  /**
   * @see JmsStubContentAttributes#getJndiInitialContextFactory()
   */
  public String getJndiInitialContextFactory() {
    return jndiInitialContextFactory;
  }

  /**
   * @see JmsStubContentAttributes#setJndiInitialContextFactory(String)
   */
  public void setJndiInitialContextFactory(String jndiInitialContextFactory) {
    this.jndiInitialContextFactory = jndiInitialContextFactory;
  }

  /**
   * @see JmsStubContentAttributes#getJndiConnectionFactoryName()
   */
  public String getJndiConnectionFactoryName() {
    return jndiConnFactName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiConnectionFactoryName(String)
   */
  public void setJndiConnectionFactoryName(String jndiConnFactName) {
    this.jndiConnFactName = jndiConnFactName;
  }

  /**
   * @see JmsStubContentAttributes#getPriority()
   */
  public int getPriority() {
    return priority;
  }

  /**
   * @see JmsStubContentAttributes#setPriority(int)
   */
  public void setPriority(int priority) {
    this.priority = priority;
  }

  /**
   * @see JmsStubContentAttributes#getTimeToLive()
   */
  public long getTimeToLive() {
    return timeToLive;
  }

  /**
   * @see JmsStubContentAttributes#setTimeToLive(long)
   */
  public void setTimeToLive(long timeToLive) {
    this.timeToLive = timeToLive;
  }

  /**
   * @see JmsStubContentAttributes#getSelector()
   */
  public String getSelector() {
    return selector;
  }

  /**
   * @see JmsStubContentAttributes#setSelector(String)
   */
  public void setSelector(String selector) {
    this.selector = selector;
  }

  /**
   * @see JmsStubContentAttributes#getPersistent()
   */
  public boolean getPersistent() {
    return persistent;
  }

  /**
   * @see JmsStubContentAttributes#setPersistent(boolean)
   */
  public void setPersistent(boolean persistent) {
    this.persistent = persistent;
  }

  /**
   * @see JmsStubContentAttributes#getJndiURL()
   */
  public String getJndiURL() {
    return this.jndiURL;
  }

  /**
   * @see JmsStubContentAttributes#setJndiURL(String)
   */
  public void setJndiURL(String jndiURL) {
    this.jndiURL = jndiURL;
  }

  /**
   * @see JmsStubContentAttributes#getJndiResponseDestinationName()
   */
  public String getJndiResponseDestinationName() {
    return jndiResponseDestinationName;
  }

  /**
   * @see JmsStubContentAttributes#setJndiResponseDestinationName(String)
   */
  public void setJndiResponseDestinationName(String jndiResponseDestinationName) {
    this.jndiResponseDestinationName = jndiResponseDestinationName;
  }

  /**
   * @see org.objectweb.fractal.api.control.LifeCycleController#startFc()
   */
  @Override
  public final void startFc() {

    delegate = WsdlDelegateFactory.newWsdlDelegate(getServant(), getServiceClass(), getClass().getClassLoader());

    try {

      jmsModule = new JmsModule(this);
      jmsModule.start();

      // start listening to incoming messages
      MessageConsumer consumer = jmsModule.getSession().createConsumer(jmsModule.getDestination(),
          getSelector());
      consumer.setMessageListener(this);

    } catch (Exception exc) {
      throw new IllegalStateException("Error starting JMS skeleton -> " + exc.getMessage(), exc);
    }

    super.startFc();
  }

  /**
   * @see org.objectweb.fractal.api.control.LifeCycleController#stopFc()
   */
  @Override
  public final void stopFc() {
    try {
      jmsModule.close();
    } catch (JMSException exc) {
      log.severe("JmsSkeletonContent.stopFc() -> " + exc.getMessage());
    }
    super.stopFc();
  }

  /**
   * Handles messages received by the binding.
   */
  public void onMessage(Message msg) {
    log.fine("onMessage: " + msg);

    try {

      Object servant = getServant();
      String operation = msg.getStringProperty(JmsConnectorConstants.OPERATION_SELECTION_PROPERTY);
      Method[] methods = servant.getClass().getDeclaredMethods();

      if (methods.length == 1) {
        /* If there is only one operation on the service's interface, then that operation
         * is the selected operation name. */
        log.fine("Servant unique operation called.");
        invokeMethod(methods[0], msg);
        return;

      } else if (operation != null) {
        /* Otherwise, if the JMS user property "scaOperationName" is present, then the
         * value of that user property is used as the selected operation name;*/
        log.fine("Looking for '" + operation + "' operation.");
        for (int i = 0; i < methods.length; i++) {
          Method m = methods[i];
          if (m.getName().equals(operation)) {
            invokeMethod(methods[i], msg);
            return;
          }
        }
        log.severe("JmsSkeletonContent.onMessage() -> Operation not found");

      } else {
        /* � Otherwise, if the message is a JMS text or bytes message containing XML, then
         * the selected operation name is the local name of the root element of the XML
         * payload; � Otherwise, the selected operation name is "onMessage".*/
        log.warning("Not yet implemented");
      }
    } catch (Throwable exc) {
      log.log(Level.SEVERE, "JmsSkeletonContent.onMessage() -> " + exc.getMessage(), exc);
    }
  }

  private void invokeMethod(Method method, Message msg) throws Exception {

    log.fine("invokeMethod " + method);
    Class<?>[] parameterTypes = method.getParameterTypes();

    Object response = null;

    if (parameterTypes.length == 0) {
      response = method.invoke(getServant(), (Object[]) null);

    } else if (parameterTypes.length == 1 && Message.class.isAssignableFrom(parameterTypes[0])) {

      /* If there is a single parameter that is a JMSMessage, then the JMSMessage is
       * passed as is.*/
      log.fine("Pass the JMSMessage as is.");
      response = method.invoke(getServant(), msg);

    } else {

      if (msg instanceof BytesMessage) {
        BytesMessage bytesMsg = (BytesMessage) msg;
        byte[] data = new byte[(int) bytesMsg.getBodyLength()];
        bytesMsg.readBytes(data);

        Method m = delegate.getMethod(method.getName());
        response = delegate.invoke(m, new String(data));

      } else if (msg instanceof TextMessage) {
        TextMessage txtMsg = (TextMessage) msg;
        Method m = delegate.getMethod(method.getName());
        response = delegate.invoke(m, txtMsg.getText());

      } else {
        /* Otherwise, if the JMSMessage is not a JMS text message or bytes message
         * containing XML, it is invalid. */
        log.severe("Received message is invalid.");
      }
    }

    if (method.getReturnType().equals(void.class) && method.getExceptionTypes().length == 0) {
      // One-way message exchange: nothing to do
      log.fine("One-way message exchange");

    } else {
      // Request/response message exchange
      log.fine("Request/response message exchange");

      TextMessage responseMsg = jmsModule.getResponseSession().createTextMessage();
      if (getCorrelationScheme().equals(JmsConnectorConstants.CORRELATION_ID_SCHEME)) {
        responseMsg.setJMSCorrelationID(msg.getJMSCorrelationID());
      } else if (getCorrelationScheme().equals(JmsConnectorConstants.MESSAGE_ID_SCHEME)) {
        responseMsg.setJMSCorrelationID(msg.getJMSMessageID());
      }

      if (responseMsg != null) {
        responseMsg.setText(response.toString());
      }

      // the request message included a non-null JMSReplyTo destination, the SCA runtime
      // MUST send the response message to that destination
      Destination responseDest = msg.getJMSReplyTo();

      log.fine("ReplyTo field: " + responseDest);
      if (responseDest == null) {
        // the JMS binding includes a response/destination element the SCA runtime MUST
        // send the response message to that destination
        responseDest = jmsModule.getResponseDestination();
      }
      if (responseDest == null) {
        throw new Exception("No response destination found.");
      }

      MessageProducer responseProducer = jmsModule.getResponseSession().createProducer(responseDest);
      responseProducer.send(responseMsg);

      responseProducer.close();

    }

    log.fine("invokeMethod done");
  }

}
