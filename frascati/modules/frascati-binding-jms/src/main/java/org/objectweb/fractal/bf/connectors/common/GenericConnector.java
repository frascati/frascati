/**
 * OW2 Fractal Binding Factory
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */
package org.objectweb.fractal.bf.connectors.common;

import java.util.Map;

import org.objectweb.fractal.bf.connectors.common.AbstractConnector;

/**
 * A generic connector can export and bind Fractal interfaces.
 *
 * TODO: Should be moved into the OW2 Fractal Binding Factory SVN.
 */
public abstract class GenericConnector extends AbstractConnector<GenericExportHints, GenericBindHints> {

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getExportHints(Map)
	 */
	public final GenericExportHints getExportHints(Map<String, Object> initialHints) {
		log.fine("Raw hints: " + initialHints);
		GenericExportHints genericExportHints = new GenericExportHints(initialHints);
		log.fine("GenericExportHints: " + genericExportHints);
		return genericExportHints;
	}

	/**
	 * @see org.objectweb.fractal.bf.BindingFactoryPlugin#getBindHints(Map)
	 */
	public final GenericBindHints getBindHints(Map<String, Object> initialHints) {
		log.fine("Raw hints: " + initialHints);
		GenericBindHints genericBindHints = new GenericBindHints(initialHints);
		log.fine("GenericBindHints: " + genericBindHints);
		return genericBindHints;
	}

}
