/**
 * OW2 FraSCAti: SCA Binding JMS
 * Copyright (C) 2010 ScalAgent Distributed Technologies, INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Guillaume Surrel
 *
 */
package org.ow2.frascati.examples.jms.sender;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

/**
 * SCA Node component implementation.
 *
 * @author Philippe Merle.
 */
@Service(interfaces={Runnable.class, Sender.class})
public class Node implements Runnable, Sender {

	public final void run() {
      System.out.println("Node(name=" + componentName + ") run()...");
      if(out != null) {
    	Message msg = new Message();
    	msg.setFrom(componentName);
    	msg.setTo("all");
    	msg.setTitle("HelloWorld");
    	msg.setBody("Best Regards.");
        System.out.println("Node(name=" + componentName + ") send " + toString(msg) + "...");
        System.out.println("** Node(name=" + componentName + ") Response: " + out.send(msg).body);
      }
	}

  public final ResponseMessage send(Message msg) {
      System.out.println("Node(name=" + componentName + ") receive " + toString(msg));
      if(out != null) {
    	msg.setFrom(componentName);
        System.out.println("Node(name=" + componentName + ") send " + toString(msg));
        System.out.println("** Node(name=" + componentName + ") Response: " + out.send(msg).body);
      }
    ResponseMessage response = new ResponseMessage();
    response.setBody("Response from " + componentName);
    return response;
	}

	@Reference(name = "out", required=false)
	private Sender out;

	@ComponentName
	private String componentName;

	private String toString(Message msg) {
      StringBuffer sb = new StringBuffer();
      sb.append("Message(to='");
      sb.append(msg.getTo());
      sb.append("', from='");
      sb.append(msg.getFrom());
      sb.append("', title='");
      sb.append(msg.getTitle());
      sb.append("', body='");
      sb.append(msg.getBody());
      sb.append("')");
      return sb.toString();
	}
}
