/**
 * OW2 FraSCAti SCA Binding RESTful
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.binding.rest;

import java.util.Map;

import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.RestBinding;

import org.objectweb.fractal.bf.connectors.rest.RestConnectorConstants;

import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;

/**
 * Bind components using a RESTful Binding.
 *
 * @author Nicolas Dolet
 * @version 1.1
 */
public class FrascatiBindingRestProcessor
     extends AbstractBindingFactoryProcessor<RestBinding>
{
  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(RestBinding restBinding, StringBuilder sb)
  {
    sb.append("frascati:binding.rest");
    super.toStringBuilder(restBinding, sb);
  }

  /**
   * @see AbstractBindingFactoryProcessor#getBindingFactoryPluginId()
   */
  @Override
  protected final String getBindingFactoryPluginId()
  {
    return "rest";
  }

  /**
   * @see AbstractBindingFactoryProcessor#initializeBindingHints(EObjectType, Map)
   */
  @Override
  @SuppressWarnings("static-access")
  protected final void initializeBindingHints(RestBinding restBinding, Map<String, Object> hints)
  {
    // set protocol specific parameter
    hints.put(RestConnectorConstants.URI, completeBindingURI(restBinding));

    // TODO: Here attribute 'name' is used until a new attribute will be added to the metamodel.
//    if(restBinding.getName() != null) {
//      hints.put(RestConnectorConstants.MODEL_REF, restBinding.getName());
//    }
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(FrascatiPackage.Literals.REST_BINDING);
  }
}
