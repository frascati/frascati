/**
 * OW2 FraSCAti: SCA Implementation Fractal
 * Copyright (C) 2008-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.implementation.fractal;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.FractalImplementation;

import org.objectweb.fractal.adl.ADLException;
import org.objectweb.fractal.adl.FactoryFactory;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.Factory;

import org.objectweb.fractal.julia.Julia;                                                                                                                               

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractImplementationProcessor;

/**
 * OW2 FraSCAti SCA Implementation Fractal processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class FrascatiImplementationFractalProcessor
     extends AbstractImplementationProcessor<FractalImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  static {
    System.setProperty("fractal.provider", Julia.class.getCanonicalName());
  }

  /**
   * The Julia bootstrap component to use with Fractal ADL factory.
   */
  private Component juliaBootstrapComponent = null;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(FractalImplementation fractalImplementation, StringBuilder sb)
  {
    sb.append("frascati:implementation.fractal");
    append(sb, "definition", fractalImplementation.getDefinition());
    super.toStringBuilder(fractalImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(FractalImplementation fractalImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    String fractalImplementationDefinition = fractalImplementation.getDefinition();
    if(isNullOrEmpty(fractalImplementationDefinition)) {
      error(processingContext, fractalImplementation, "The attribute 'definition' must be set");
    } else {
      int index = fractalImplementationDefinition.indexOf('(');
      String fractalFile = (index == -1)
                         ? fractalImplementationDefinition
                         : fractalImplementationDefinition.substring(0, index);
      if(processingContext.getResource(fractalFile.replace(".", "/") + ".fractal") == null) {
        error(processingContext, fractalImplementation, "Fractal definition '", fractalFile, "' not found");
      }
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(fractalImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(FractalImplementation fractalImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    logFine(processingContext, fractalImplementation, "nothing to generate");
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(FractalImplementation fractalImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    String definition = fractalImplementation.getDefinition();

    Component component;
    try {
      // Try loading from definition generated with Juliac
      Class<?> javaClass = processingContext.loadClass(definition);
      Factory object = (Factory) javaClass.newInstance();

      // Create the component instance.
      component = object.newFcInstance();

    } catch (ClassNotFoundException e) {

      // Create the Julia bootstrap component the first time is needed.
      if(juliaBootstrapComponent == null) {
        try {
          juliaBootstrapComponent = new MyJulia().newFcInstance();
        } catch (org.objectweb.fractal.api.factory.InstantiationException ie) {
          // Error on MyJulia
          severe(new ProcessorException(fractalImplementation, "Error into the Fractal implementation : "
                + definition, ie));
          return;
        }
      }

      // Generated class not found try with fractal definition and Fractal ADL

      try {

        // init a Fractal ADL factory.
        org.objectweb.fractal.adl.Factory fractalAdlFactory = FactoryFactory.getFactory(FactoryFactory.FRACTAL_BACKEND);

        // init a Fractal ADL context.
        Map<Object, Object> context = new HashMap<Object, Object>();
        // ask the Fractal ADL factory to the Julia Fractal bootstrap.
        context.put("bootstrap", juliaBootstrapComponent);
        // ask the Fractal ADL factory to use the current FraSCAti processing context classloader.
        context.put("classloader", processingContext.getClassLoader());

        // load the Fractal ADL definition and create the associated Fractal component.
        component = (Component) fractalAdlFactory.newComponent(definition, context);

      } catch (ADLException ae) {
        // Error with Fractal ADL
        severe(new ProcessorException(fractalImplementation, "Error into the Fractal implementation : "
            + definition, ae));
        return;
      }
    } catch (InstantiationException ie) {
      severe(new ProcessorException(fractalImplementation,
          "Error when building instance for class " + definition, ie));
      return;
    } catch (IllegalAccessException iae) {
      severe(new ProcessorException(fractalImplementation, "Can't access class "
          + definition, iae));
      return;
    } catch (org.objectweb.fractal.api.factory.InstantiationException ie) {
      severe(new ProcessorException(fractalImplementation,
          "Error when building component instance: " + definition, ie));
      return;
    }
 
    // Store the created component into the processing context.
    processingContext.putData(fractalImplementation, Component.class, component);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(FrascatiPackage.Literals.FRACTAL_IMPLEMENTATION);
  }

}
