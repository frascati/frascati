/**
 * OW2 FraSCAti: SCA Implementation Fractal
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.fractal.test;

import javax.xml.namespace.QName;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.processor.ProcessingContextImpl;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest {

	FraSCAti frascati;
	CompositeManager compositeManager;

	@Before
    public void initFraSCAti() throws Exception {
      frascati = FraSCAti.newFraSCAti();
      compositeManager = frascati.getCompositeManager();
    }

    /**
     * Check errors produced during the checking phase.
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processCheckingErrorsWarningsComposite() throws Exception {
      ProcessingContext processingContext = new ProcessingContextImpl();
      try {
        compositeManager.processComposite(new QName("CheckingErrorsWarnings"), processingContext);
      } catch(ManagerException me) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrorsWarnings.composite'
    	assertEquals("The number of checking errors", 3, processingContext.getErrors());
        // Let's note that the file 'CheckingErrorsWarnings.composite' produces 2 warnings,
        // one warning is also produced by the OW2 FraSCAti Parser (EMF diagnostics)..
        assertEquals("The number of checking warnings", 3, processingContext.getWarnings());
      }
    }

}
