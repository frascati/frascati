/**
 * OW2 FraSCAti SCA Binding UPnP
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Remi Melisson
 *
 * Contributor(s): Gwenael Cattez, Philippe Merle
 *
 */

package org.ow2.frascati.upnp.binding;

import java.util.Map;

import org.objectweb.fractal.bf.connectors.upnp.UPnPConnectorConstants;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.upnp.UPnPBinding;
import org.ow2.frascati.upnp.UpnpPackage;

/**
 * Bind components using a UPnP Binding.
 *
 * @author R�mi M�lisson
 * @version 1.1
 */
public class FrascatiBindingUPnPProcessor
     extends AbstractBindingFactoryProcessor<UPnPBinding>
{
  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(UPnPBinding upnpBinding, StringBuilder sb) {
    sb.append("upnp:binding.upnp");
    sb.append(UPnPConnectorConstants.SERVICE_ID + ":");
    sb.append(upnpBinding.getServiceId());
    sb.append(UPnPConnectorConstants.SERVICE_TYPE + ":");
    sb.append(upnpBinding.getServiceType());
    sb.append(UPnPConnectorConstants.DEVICE_ID + ":");
    sb.append(upnpBinding.getDeviceId());
    sb.append(UPnPConnectorConstants.DEVICE_TYPE + ":");
    sb.append(upnpBinding.getDeviceType());
    sb.append(UPnPConnectorConstants.DEVICE_VERSION + ":");
    sb.append(upnpBinding.getDeviceVersion());
    sb.append(UPnPConnectorConstants.DEVICE_ICON + ":");
    sb.append(upnpBinding.getDeviceIcon());
    sb.append("]");
    super.toStringBuilder(upnpBinding, sb);
  }

  @Override
  protected final String getBindingFactoryPluginId() {
    return "upnp";
  }

  @Override
  @SuppressWarnings("static-access")
  protected final void initializeBindingHints(UPnPBinding upnpBinding, Map<String, Object> hints)
  {
      hints.put(UPnPConnectorConstants.DEVICE_ID, upnpBinding.getDeviceId());
      hints.put(UPnPConnectorConstants.DEVICE_TYPE, upnpBinding.getDeviceType());
      hints.put(UPnPConnectorConstants.SERVICE_ID, upnpBinding.getServiceId());
      hints.put(UPnPConnectorConstants.SERVICE_TYPE, upnpBinding.getServiceType());
      hints.put(UPnPConnectorConstants.DEVICE_VERSION, upnpBinding.getDeviceVersion());
      hints.put(UPnPConnectorConstants.DEVICE_ICON, upnpBinding.getDeviceIcon());
  }
  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
	  return getID(UpnpPackage.Literals.UPN_PBINDING);
  }
}
