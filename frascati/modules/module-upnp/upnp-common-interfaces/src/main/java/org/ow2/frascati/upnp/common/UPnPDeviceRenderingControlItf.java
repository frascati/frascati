package org.ow2.frascati.upnp.common;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu Schepens
 *
 * Contributor(s): Gwenael Cattez
 *
 */


/**
 *
 */
@UPnPService(type="RenderingControl")
public interface UPnPDeviceRenderingControlItf
{
    @GET
    @Path("/preset/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("ListPresets")
    public ListPresets listPresets(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID);
	
    @POST
    @Path("/preset/{instanceID}/{name}")
    @UPnPAction("SelectPresets")
    public void selectPresets(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("PresetName") @PathParam("name") String PresetName);

    @GET
    @Path("/mute/{instanceID}/{channel}")
    @Produces({"text/plain"})
    @UPnPAction("GetMute")
    public boolean getMute(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("Channel") @PathParam("channel") String Channel);
    
    @POST
    @Path("/mute/{instanceID}/{channel}/{value}")
    @UPnPAction("SetMute")
    public void setMute(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("Channel") @PathParam("channel") String Channel, @UPnPVariable("DesiredMute") @PathParam("value") String DesiredMute);

    @GET
    @Path("/volume/{instanceID}/{channel}")
    @Produces({"text/plain"})
    @UPnPAction("GetVolume")
    public int getVolume(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("Channel") @PathParam("channel") String Channel);
    
    @POST
    @Path("/volume/{instanceID}/{channel}/{value}")
    @UPnPAction("SetVolume")
    public void setVolume(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("Channel") @PathParam("channel") String Channel, @UPnPVariable("DesiredVolume") @PathParam("value") String DesiredVolume);
}
