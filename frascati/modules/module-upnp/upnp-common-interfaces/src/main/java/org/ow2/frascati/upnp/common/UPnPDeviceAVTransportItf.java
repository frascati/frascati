package org.ow2.frascati.upnp.common;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu Schepens
 *
 * Contributor(s): Gwenael Cattez
 *
 */


/**
 *
 */
@UPnPService(type="AVTransport")
public interface UPnPDeviceAVTransportItf
{
    @POST
    @Path("/play/{instanceID}/{speed}")
    @UPnPAction("Play")
    public void play(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID, @UPnPVariable("Speed") @PathParam("speed") String Speed);

    @POST
    @Path("/pause/{instanceID}")
    @UPnPAction("Pause")
    public void pause(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @POST
    @Path("/stop/{instanceID}")
    @UPnPAction("Stop")
    public int stop(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @POST
    @Path("/seek/{instanceID}/{unit}/{target}")
    @UPnPAction("Seek")
    public void seek(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID, @UPnPVariable("Unit") @PathParam("unit") String Unit, @UPnPVariable("Target") @PathParam("target") String Target);

    @POST
    @Path("/next/{instanceID}")
    @UPnPAction("Next")
    public void next(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);

    @POST
    @Path("/previous/{instanceID}")
    @UPnPAction("Previous")
    public void previous(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);

    
    @GET
    @Path("/transportInfo/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetTransportInfo")
    public GetTransportInfo getTransportInfo(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @GET
    @Path("/mediaInfo/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetMediaInfo")
    public GetMediaInfo getMediaInfo(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @GET
    @Path("/deviceCapabilities/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetDeviceCapabilities")
    public GetDeviceCapabilities getDeviceCapabilities(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @GET
    @Path("/currentTransportActions/{instanceID}")
    @Produces({"text/plain"})
    @UPnPAction("GetCurrentTransportActions")
    public String getCurrentTransportActions(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @GET
    @Path("/positionInfo/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetPositionInfo")
    public GetPositionInfo getPositionInfo(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @GET
    @Path("/transportSettings/{instanceID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetTransportSettings")
    public GetTransportSettings getTransportSettings(@UPnPVariable("InstanceID") @PathParam("instanceID")String InstanceID);
    
    @POST
    @Path("/avtTransportURI/{instanceID}/{currentURI}/{currentURIMetaData}")
    @UPnPAction("SetAVTransportURI")
    public void setAVTransportURI(@UPnPVariable("InstanceID") @PathParam("instanceID") String InstanceID, @UPnPVariable("CurrentURI") @PathParam("currentURI") String CurrentURI, @UPnPVariable("CurrentURIMetaData") @PathParam("currentURIMetaData") String CurrentURIMetaData);
}
