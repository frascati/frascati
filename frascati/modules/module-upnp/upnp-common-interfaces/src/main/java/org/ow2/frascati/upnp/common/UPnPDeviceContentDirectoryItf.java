/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu Schepens
 *
 * Contributor(s): Gwenael Cattez
 *
 */

package org.ow2.frascati.upnp.common;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;

/**
 *
 */
@UPnPService(type="ContentDirectory")
public interface UPnPDeviceContentDirectoryItf
{
    @GET
    @Path("/searchCapabilities")
    @UPnPAction("GetSearchCapabilities")
    public String getSearchCapabilities();
    
    @GET
    @Path("/sortCapabilities")
    @UPnPAction("GetSortCapabilities")
    public String getSortCapabilities();
    
    @GET
    @Path("/systemUpdateID")
    @UPnPAction("GetSystemUpdateID")
    public String getSystemUpdateID();
    
    @GET
    @Path("/brows/{objectID}/{browseFlag}/[filter}/{startingIndex}/{requestedCount}/{sortCriteria}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("Browse")
    public Browse browse(@UPnPVariable("ObjectID") @QueryParam("objectId") String ObjectID, @UPnPVariable("BrowseFlag") @QueryParam("browseFlag") String BrowseFlag, @UPnPVariable("Filter") @QueryParam("filter") String Filter, @UPnPVariable("StartingIndex") @QueryParam("startingIndex") int StartingIndex, @UPnPVariable("RequestedCount") @QueryParam("requestedCount") int RequestedCount, @UPnPVariable("SortCriteria") @QueryParam("sortCriteria") String SortCriteria) throws Exception;
}
