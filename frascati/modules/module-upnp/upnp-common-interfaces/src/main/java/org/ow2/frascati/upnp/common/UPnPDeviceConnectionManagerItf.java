package org.ow2.frascati.upnp.common;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.ow2.fractal.upnp.annotation.UPnPAction;
import org.ow2.fractal.upnp.annotation.UPnPService;
import org.ow2.fractal.upnp.annotation.UPnPVariable;


/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu Schepens
 *
 * Contributor(s): Gwenael Cattez
 *
 */


/**
 *
 */
@UPnPService(type="ConnectionManager")
public interface UPnPDeviceConnectionManagerItf
{
    @GET
    @Path("/currentConnectionInfo/{connectionID}")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetCurrentConnectionInfo")
    public GetCurrentConnectionInfo getCurrentConnectionInfo(@UPnPVariable("ConnectionID") @PathParam("connectionId") String ConnectionID);

    @GET
    @Path("/protocolInfo")
    @Produces({"application/xml", "application/json"})
    @UPnPAction("GetProtocolInfo")
    public GetProtocolInfo getProtocolInfo();
    
    @GET
    @Path("/currentConnectionIDs")
    @Produces({"text/plain"})
    @UPnPAction("GetCurrentConnectionIDs")
    public String getCurrentConnectionIDs();
}
