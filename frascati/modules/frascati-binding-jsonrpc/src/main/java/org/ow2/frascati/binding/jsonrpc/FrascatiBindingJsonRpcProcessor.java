/**
 * OW2 FraSCAti SCA Binding JSON-RPC
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 * 
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.binding.jsonrpc;

import java.util.Map;

import org.eclipse.stp.sca.domainmodel.frascati.JsonRpcBinding;
import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;

import org.objectweb.fractal.bf.connectors.jsonrpc.JsonRpcConnectorConstants;

import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;

/**
 * Bind components using a Json RPC Binding.
 *
 * @author Nicolas Dolet
 * @since 1.1
 */
public class FrascatiBindingJsonRpcProcessor
     extends AbstractBindingFactoryProcessor<JsonRpcBinding>
{
  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(JsonRpcBinding jsonRpcBinding, StringBuilder sb) {
    sb.append("frascati:binding.jsonrpc");
    super.toStringBuilder(jsonRpcBinding, sb);
  }

  /**
   * @see AbstractBindingFactoryProcessor#getBindingFactoryPluginId()
   */
  @Override
  public final String getBindingFactoryPluginId() {
    return "jsonrpc";
  }

  /**
   * @see AbstractBindingFactoryProcessor#initializeBindingHints(EObjectType, Map)
   */
  @Override
  @SuppressWarnings("static-access")
  public final void initializeBindingHints(JsonRpcBinding jsonRpcBinding, Map<String, Object> hints) {
    // set protocol specific parameter
    hints.put(JsonRpcConnectorConstants.URI, completeBindingURI(jsonRpcBinding));
  }
	
  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(FrascatiPackage.Literals.JSON_RPC_BINDING);
  }

}
