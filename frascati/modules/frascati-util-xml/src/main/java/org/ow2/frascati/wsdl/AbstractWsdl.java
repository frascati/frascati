/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import javax.jws.WebService;
import javax.jws.WebParam;
import javax.xml.namespace.QName;

import org.ow2.frascati.util.AbstractLoggeable;

/**
 * OW2 FraSCAti WSDL abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
abstract class AbstractWsdl
       extends AbstractLoggeable
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Get the targetNamespace of the @WebService annotating the class of the given method.
   */
  protected static String getTargetNamespace(Method method)
  {
    WebService ws = method.getDeclaringClass().getAnnotation(WebService.class);
    return ws.targetNamespace();
  }

  /**
   * Compute the qname of a given @WebParam.
   */
  protected static QName toQName(WebParam wp, Method method)
  {
    String targetNamespace = wp.targetNamespace();
    if(targetNamespace == null || targetNamespace.equals("")) {
      targetNamespace = getTargetNamespace(method);
    }
    return new QName(targetNamespace, wp.name());
  }

  protected static QName getQNameOfFirstArgument(Method method) throws Exception
  {
    // Compute the qname of the @WebParam attached to the first method parameter.
    Annotation[][] pa = method.getParameterAnnotations();
    WebParam wp = (WebParam)pa[0][0];
    return toQName(wp, method);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
