/**
 * OW2 FraSCAti: JAXB Helper
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.jaxb;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;

import com.sun.xml.bind.api.impl.NameConverter;

/**
 * OW2 FraSCAti JAXB wrapper.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class JAXB
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Map to find XSD types associated to Java primitive types.
   */
  private static final Map<Class<?>, String> JAVA2XSD = new HashMap<Class<?>, String>()
  {
    {
      put(Boolean.class, "xsd:boolean");
      put(Integer.class, "xsd:int");
      put(String.class, "xsd:string");
      put(Double.class, "xsd:double");
// TODO: add other primitive Java types.
    }
  };

  /**
   * Map of already created JAXBContext instances.
   */
  private static Map<String, JAXBContext> jaxbContexts = new HashMap<String, JAXBContext>();

  /**
   * Map of already created JAXBMarshaller instances.
   */
  private static Map<String, Marshaller> jaxbMarshallers = new HashMap<String, Marshaller>();

  /**
   * Map of already created JAXBUnmarshaller instances.
   */
  private static Map<String, Unmarshaller> jaxbUnmarshallers = new HashMap<String, Unmarshaller>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Private constructor to avoid to instantiate this utility class.
   */
  private JAXB() {}

  /**
   * Is a primitive type object.
   */
  private static boolean isPrimitiveTypeObject(Object value)
  {
    return JAVA2XSD.containsKey(value.getClass());
  }

  /**
   * Marshall primitive types.
   */
  private static String marshallPrimitiveTypeObject(QName qname, Object value)
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<");
    sb.append(qname.getLocalPart());
    sb.append(" xmlns=\"");
    sb.append(qname.getNamespaceURI());
    sb.append("\"");
    sb.append(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"");
    sb.append(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"");
    sb.append(" xsi:type=\"");
    sb.append(JAVA2XSD.get(value.getClass()));
    sb.append("\"");
    sb.append(">");
    sb.append(value.toString());
    sb.append("</");
    sb.append(qname.getLocalPart());
    sb.append(">");
    return sb.toString();
  }

  private static JAXBContext getJAXBContext(String javaPackageName) throws JAXBException
  {
    JAXBContext jc = jaxbContexts.get(javaPackageName);
    if(jc == null) {
      jc = JAXBContext.newInstance(javaPackageName);
      jaxbContexts.put(javaPackageName, jc);
    }
    return jc;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Get the Java package name associated to an URI.
   */
  public static String getJavaPackageName(String uri)
  {
   	return NameConverter.standard.toPackageName(uri);
  }

  /**
   * Get the XSD type associated to a Java primitive type.
   */
  public static String toXsdType(Class<?> clazz)
  {
    return JAVA2XSD.get(clazz);
  }

  /**
   * Marshall a Java object to an XML message.
   */
  public static String marshall(QName qname, Object object) throws JAXBException
  {
    Object value = object;
    if(value instanceof Holder) {
      value = ((Holder)value).value;
    }

    if(isPrimitiveTypeObject(value)) {
      return marshallPrimitiveTypeObject(qname, value);
    } else {
      String javaPackageName = getJavaPackageName(qname.getNamespaceURI());

      Marshaller marshaller = jaxbMarshallers.get(javaPackageName);
      if(marshaller == null) {
        JAXBContext jc = getJAXBContext(javaPackageName);
        marshaller = jc.createMarshaller();
        jaxbMarshallers.put(javaPackageName, marshaller);
      }
      StringWriter sw = new StringWriter();

      // JAXB marshaller is not thread-safe.
      synchronized(marshaller) {
        if(value.getClass().getAnnotation(XmlRootElement.class) != null) {
          // Marshall the value which is annotated with @XmlRootElement.
          marshaller.marshal(value, sw);
        } else {
          // Marshall the value which is not annotated with @XmlRootElement.
          marshaller.marshal(new JAXBElement(qname, value.getClass(), value ), sw);
        }
      }
      return sw.toString();
    }
  }

  /**
   * Unmarshall an XML message to a Java object.
   */
  public static Object unmarshall(QName qname, String xml, Object holder) throws JAXBException
  {
    String javaPackageName = getJavaPackageName(qname.getNamespaceURI());

    Unmarshaller unmarshaller = jaxbUnmarshallers.get(javaPackageName);
    if(unmarshaller == null) {
      JAXBContext jc = getJAXBContext(javaPackageName);
      unmarshaller = jc.createUnmarshaller();
      jaxbUnmarshallers.put(javaPackageName, unmarshaller);
    }

    // Unmarshall the XML message.
    Object result = null;
    // JAXB unmarshaller is not thread-safe.
    synchronized(unmarshaller) {
      result = unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes()));
    }

    if(result.getClass() == JAXBElement.class) {
      JAXBElement jaxbe = (JAXBElement)result;
      result = jaxbe.getValue();
    }

    if(holder instanceof Holder) {
      ((Holder)holder).value = result;
      return holder;
    }

    return result;
  }

  /**
     * Look for ObjectFactory classes in the ClassLoader passed on on as
     * parameter. If an ObjectFactory class is found a new JAXBContext is
     * created by using the package name of the class and the ClassLoader. Then
     * it is stored in the jaxbContexts map
     * 
     * @param classLoader
     *            The used ClassLoader to look for ObjectFactory classes
     */
    public static void registerObjectFactoryFromClassLoader(
            ClassLoader classLoader) {

        Enumeration<URL> factoriesURL = null;
        try {
            factoriesURL = classLoader.getResources("ObjectFactory.class");
        } catch (IOException e) {
           // e.printStackTrace();
        }
        if (factoriesURL != null && factoriesURL.hasMoreElements()) {

            while (factoriesURL.hasMoreElements()) {

                // extract the package name from the URL
                URL factoryURL = factoriesURL.nextElement();

                // get the file part only
                String factoryURLFilePart = factoryURL.getFile();

                // if in a jar : cut after the jar delimiter
                // if not : cut after the first '/' character
                String factoryPackageName = factoryURLFilePart
                    .substring(factoryURLFilePart.lastIndexOf("!/") + 2);

                // replace file separator with package separator
                factoryPackageName = factoryPackageName.replace('/', '.');

                // then remove the class name
                String factoryClassName = factoryPackageName.substring(0,
                        factoryPackageName.lastIndexOf('.'));
                
                factoryPackageName = factoryClassName.substring(0,
                        factoryClassName.lastIndexOf('.'));
                
                // Try to find an existing JAXBContext for the package name
                JAXBContext jc = jaxbContexts.get(factoryPackageName);
                if (jc == null) {
                    // if it doesn't exist, it is created...
                    try {
                        jc = JAXBContext.newInstance(factoryPackageName,
                                classLoader);
                        // ...and stored in the map
                        jaxbContexts.put(factoryPackageName, jc);
                    } catch (javax.xml.bind.JAXBException jaxbe) {
                        System.out.println(
                            "WARNING : No JAXBContext created for '"
                                + factoryPackageName + "' package");
                    }
                }
            }
        } 
    }

}
