/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import javax.jws.WebResult;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.Holder;
import javax.xml.namespace.QName;

import org.jdom.Element;

import org.ow2.frascati.jaxb.JAXB;
import org.ow2.frascati.jdom.JDOM;

/**
 * OW2 FraSCAti WSDL dynamic proxy invocation handler.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractWsdlInvocationHandler
              extends AbstractWsdl
           implements InvocationHandler
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Marshall invocation to an XML message.
   */
  protected final String marshallInvocation(Method method, Object[] args) throws Exception
  {
	// TODO: Support for zero and several arguments must be added.
    if(args.length != 1) {
      throw new IllegalArgumentException("Only methods with one argument are supported, given method is " + method + " and argument length is " + args.length);
    }

    // Compute the qname of the first parameter.
    QName qname = getQNameOfFirstArgument(method);

    // Marshall the first argument into as an XML message.
    return JAXB.marshall(qname, args[0]);
  }

  /**
   * Unmarshall an XML message.
   */
  protected final Object unmarshallResult(Method method, Object[] args, Element element) throws Exception
  {
	// Unmarshall the given XML message.
    SOAPBinding soapBinding = method.getDeclaringClass().getAnnotation(SOAPBinding.class);
    if(soapBinding != null) {
      log.fine("@SOAPBinding for " + method);
      if(soapBinding.style() == SOAPBinding.Style.DOCUMENT) {
        log.fine("@SOAPBinding(style=SOAPBinding.Style.DOCUMENT) for " + method);
        if(soapBinding.parameterStyle() == SOAPBinding.ParameterStyle.BARE) {
          log.fine("@SOAPBinding(parameterStyle=SOAPBinding.ParameterStyle.BARE) for " + method);

          WebResult wr = method.getAnnotation(WebResult.class);
          if(wr != null) {
            Object result = JDOM.unmarshallContent0(element, method.getReturnType());
            if(result != null) {
            	return result;
            }
            QName qname = new QName(wr.targetNamespace(), wr.name());
            String xmlMessage = JDOM.toString(element);
            return JAXB.unmarshall(qname, xmlMessage, null);
          }
          if(args[0].getClass() == Holder.class) {
            QName qname = getQNameOfFirstArgument(method);
            String xmlMessage = JDOM.toString(element);
            Object result = JAXB.unmarshall(qname, xmlMessage, args[0]);
            return null;
          }
        }

        log.fine("@SOAPBinding(parameterStyle=SOAPBinding.ParameterStyle.WRAPPED) for " + method);

        // Compute the qname of the @WebResult attached to the method.
        String targetNamespace = null;
        String name = "";
        WebResult wr = method.getAnnotation(WebResult.class);
        if(wr != null) {
          log.fine("@WebResult for " + method);
          targetNamespace = wr.targetNamespace();
          name = wr.name();
        }
        if(targetNamespace == null || targetNamespace.equals("")) {
          targetNamespace = getTargetNamespace(method);
        }
        String xmlMessage = JDOM.toString(element);
        log.fine("xmlMessage = " + xmlMessage);
        return JAXB.unmarshall(new QName(targetNamespace, name), xmlMessage, args[0]);
      }

      if(soapBinding.style() == SOAPBinding.Style.RPC) {
        log.fine("@SOAPBinding(style=SOAPBinding.Style.RPC) for " + method);
        return JDOM.unmarshallElement0(element, method.getReturnType());
      }
    }

    return null;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
