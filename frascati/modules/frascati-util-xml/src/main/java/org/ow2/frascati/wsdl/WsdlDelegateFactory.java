/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import javax.jws.soap.SOAPBinding;

/**
 * OW2 FraSCAti WSDL Delegate Factory class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class WsdlDelegateFactory
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Private constructor to avoid to instantiate this utility class.
   */
  private WsdlDelegateFactory() {}

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Create a WsdlDelegate instance.
   */
  public static WsdlDelegate newWsdlDelegate(Object delegate, Class<?> interfaze, ClassLoader classLoader)
  {
    AbstractWsdlDelegate result = null;
    SOAPBinding soapBinding = interfaze.getAnnotation(SOAPBinding.class);
    if(soapBinding != null) {
      if(soapBinding.style() == SOAPBinding.Style.DOCUMENT) {
        result = new DocumentWsdlDelegate(classLoader);
      }
      if(soapBinding.style() == SOAPBinding.Style.RPC) {
        result = new RpcWsdlDelegate();
      }
    } else {
      // When no @SOAPBinding then create a Document-based WSDL Delegate instance.
      result = new DocumentWsdlDelegate(classLoader);
    }
    result.delegate = delegate;
    result.interfaze = interfaze;
    return result;
  }

}
