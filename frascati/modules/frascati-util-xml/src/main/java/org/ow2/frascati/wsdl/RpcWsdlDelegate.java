/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.lang.reflect.Method;

import org.jdom.Element;

import org.ow2.frascati.jdom.JDOM;

/**
 * OW2 FraSCAti WSDL RPC-based Delegate class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class RpcWsdlDelegate
extends AbstractWsdlDelegate
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see WsdlDelegate#invoke(Method, String)
   */
  public String invoke(Method method, String xmlMessage) throws Exception
  {
    Element request = JDOM.toElement(xmlMessage);
    Element response = invoke(method, request);
    return JDOM.toString(response);
  }

  /**
   * @see WsdlDelegate#invoke(Method, Element)
   */
  public Element invoke(Method method, Element element) throws Exception
  {
    Object arg = JDOM.unmarshallElement0(element, method.getParameterTypes()[0]);
    
    Object result = method.invoke(delegate, arg);
    log.fine("result = " + result);
    
    String name = element.getName();
    Element response = new Element(name);
    response.setNamespace(element.getNamespace());
    Element child = new Element(name + "Response");
    child.setText(result.toString());
    response.addContent(child);

    return response;
  }

}
