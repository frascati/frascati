/**
 * OW2 FraSCAti: JDOM Helper
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.jdom;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.xml.namespace.QName;

import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Text;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import org.ow2.frascati.jaxb.JAXB;

/**
 * OW2 FraSCAti JDOM Helper class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class JDOM
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Private constructor to avoid to instantiate this utility class.
   */
  private JDOM() {}

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  public static Element toElement(String xmlMessage)
  {
    try {
      final ByteArrayInputStream bais = new ByteArrayInputStream(xmlMessage.getBytes());
// TODO : the SAXBuilder can be create once and reuse?
      final SAXBuilder saxBuilder = new SAXBuilder();
      final Document jdomDocument = saxBuilder.build(bais);
      return jdomDocument.getRootElement();
    } catch(JDOMException je) {
      throw new Error("Should not happen on the XML message " + xmlMessage, je);
    } catch(IOException ioe) {
      throw new Error("Should not happen on the XML message " + xmlMessage, ioe);
    }
  }

  public static String toString(Element element)
  {
// TODO : the XML outputter can be create once and reuse?
    final XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
	return outputter.outputString(element);
  }

  public static Object unmarshallElement0(final Element element, Class<?> type) throws Exception
  {
    Element element0 = (Element)element.getContent().get(0);
    return unmarshallContent0(element0, type);
  }

  public static Object unmarshallContent0(final Element element, Class<?> type) throws Exception
  {
    Content content0 = (Content)element.getContent().get(0);
    if(content0 instanceof Element) {
      QName qname = new QName(element.getNamespace().getURI(), element.getName());
      return JAXB.unmarshall(qname, toString(element), null);
    }
    if(content0 instanceof Text) {
      String value = ((Text)content0).getText();
      if(type.equals(String.class)) {
        return value;
      } else if(type.equals(int.class)) {
        return Integer.valueOf(value);
      } else if(type.equals(boolean.class)) {
        return Boolean.valueOf(value);
      }
      // TODO manage other data types
    }
    return null;
  }
}
