/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.lang.reflect.Method;

/**
 * OW2 FraSCAti WSDL Delegate abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
abstract class AbstractWsdlDelegate
       extends AbstractWsdl
    implements WsdlDelegate
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Delegate.
   */
  protected Object delegate;
  
  /**
   * Delegate interface.
   */
  protected Class<?> interfaze;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   *
   */
  protected static Method getMethod(Class<?> clazz, String methodName)
  {
    for(Method method : clazz.getMethods()) {
      if(method.getName().equals(methodName)) {
        return method;
      }
    }
    return null;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see WsdlDelegate#getDelegate()
   */
  public final Object getDelegate() {
    return this.delegate;
  }

  /**
   * @see WsdlDelegate#getMethod(String)
   */
  public final Method getMethod(String methodName)
  {
    // Lower case the first character of the method name.
	methodName = Character.toLowerCase(methodName.charAt(0)) + methodName.substring(1);
    return getMethod(this.interfaze, methodName);
  }

}
