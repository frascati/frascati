/**
 * OW2 FraSCAti: WSDL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.wsdl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import javax.jws.Oneway;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.namespace.QName;
import javax.xml.ws.Holder;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

import org.jdom.Element;

import org.ow2.frascati.jaxb.JAXB;
import org.ow2.frascati.jdom.JDOM;

/**
 * OW2 FraSCAti Document-based WSDL Delegate class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
class DocumentWsdlDelegate
extends AbstractWsdlDelegate
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Class loader to load @ResponseWrapper classes.
   */
  ClassLoader classLoader;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  protected static String firstCharacterUpperCase(String value)
  {
    return Character.toUpperCase(value.charAt(0)) + value.substring(1);
  }

  protected static Object invokeGetter(Object object, String getterName) throws Exception
  {
    Method getter = object.getClass().getMethod("get" + firstCharacterUpperCase(getterName), (Class<?>[])null);
    return getter.invoke(object);
  }

  protected static void invokeSetter(Object object, String setterName, Object value) throws Exception
  {
    Method setter = getMethod(object.getClass(), "set" + firstCharacterUpperCase(setterName));
    setter.invoke(object, value);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Public constructor.
   */
  public DocumentWsdlDelegate(ClassLoader cl)
  {
    classLoader = cl;
  }

  /**
   * @see WsdlDelegate#invoke(Method, String)
   */
  public String invoke(Method method, String xmlMessage) throws Exception
  {
    log.fine("xmlMessage = " + xmlMessage);

    Class<?>[] methodParameterTypes = method.getParameterTypes();
    Annotation[][] pa = method.getParameterAnnotations();
    Object[] parameters = new Object[methodParameterTypes.length];

    RequestWrapper rw = method.getAnnotation(RequestWrapper.class);
    if(rw != null) {
      log.fine("@RequestWrapper for " + method);
      QName qname = new QName(rw.targetNamespace(), rw.localName());
      Object request = JAXB.unmarshall(qname, xmlMessage, null);
      log.fine("request = " + request);
      for(int i=0; i<methodParameterTypes.length; i++) {
        WebParam wp = (WebParam)pa[i][0];
        WebParam.Mode wpMode = wp.mode();
        if(wpMode == WebParam.Mode.IN) {
          parameters[i] = invokeGetter(request,  wp.name());
        } else if(wpMode == WebParam.Mode.OUT) {
          parameters[i] = new Holder();
        } else if(wpMode == WebParam.Mode.INOUT) {
          Holder<Object> holder = new Holder<Object>();
          holder.value = invokeGetter(request,  wp.name());
          parameters[i] = holder;
        }
      }
    } else {
      log.fine("no @RequestWrapper for " + method);

      QName qname = getQNameOfFirstArgument(method);

      if(methodParameterTypes[0] == Holder.class) {
        parameters[0] = new Holder();
      }
      parameters[0] = JAXB.unmarshall(qname, xmlMessage, parameters[0]);
    }

    Object result = method.invoke(this.delegate, parameters);
    log.fine("result = " + result);
    
    if(method.getAnnotation(Oneway.class) != null) {
      log.fine("@Oneway for " + method);
      return null;
    }
    
    ResponseWrapper rrw = method.getAnnotation(ResponseWrapper.class);
    if(rrw != null) {
      log.fine("@ResponseWrapper for " + method);
      Class<?> responseClass = classLoader.loadClass(rrw.className());
      Object response = responseClass.newInstance();
      WebResult wr = method.getAnnotation(WebResult.class);
      if(wr != null) {
        log.fine("@WebResult(name=" + wr.name() + ") for " + method);
        invokeSetter(response, wr.name(), result);        
      }
      for(int i=0; i<methodParameterTypes.length; i++) {
        WebParam wp = (WebParam)pa[i][0];
        // if (wpMode == WebParam.Mode.IN) then nothing to do.
        if(wp.mode() != WebParam.Mode.IN) { // i.e. OUT or INOUT
          log.fine("@WebParam(mode=" + wp.mode() + ") for " + method);
          Object value = ((Holder)parameters[i]).value;
          invokeSetter(response, wp.name(), value);
        }
      }
      log.fine("response = " + response);
      String reply = JAXB.marshall(new QName(rrw.targetNamespace(), rrw.localName()), response);
      log.fine("reply = " + reply);
      return reply;
    } else {
      log.fine("No @ResponseWrapper for " + method);

      QName qname = null;

      WebResult wr = method.getAnnotation(WebResult.class);
      if(wr != null) {
        qname = new QName(wr.targetNamespace(), wr.name());
      } else {
        if(methodParameterTypes[0] == Holder.class) {
          qname = getQNameOfFirstArgument(method);
          result = ((Holder)parameters[0]).value;
        }
      }

      if(qname != null) {
        String reply = JAXB.marshall(qname, result);
        log.fine("reply = " + reply);
        return reply;
      }
      return null;
    }
  }

  /**
   * @see WsdlDelegate#invoke(Method, Element)
   */
  public Element invoke(Method method, Element element) throws Exception
  {
    String request = JDOM.toString(element);
	String response = invoke(method, request);
	return (response != null) ? JDOM.toElement(response) : null;
  }

}
