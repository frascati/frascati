/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Pierre-Charles David
 *
 * Contributor(s): Christophe Demarey
 *                 Philippe Merle
 *
 */

package org.ow2.frascati.fscript.console.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.script.ScriptContext;

import org.objectweb.fractal.fscript.model.Node;

/**
 * This command prints the list of global variables.
 */
public class VarsCommand
     extends AbstractCommand
{
    public final void execute(String args) throws Exception
    {
        List<String> names = new ArrayList<String>(engine.getBindings(ScriptContext.ENGINE_SCOPE).keySet());
        for (String key: names) {
            // Ignore special variables
            if (key.startsWith("*")) {
                names.remove(key);
            }        	
        }        
        
        Collections.sort(names);
        String[][] data = new String[names.size() + 1][2];
        data[0][0] = "Name";
        data[0][1] = "Value";
        for (int i = 0; i < names.size(); i++) {
            String var = names.get(i);
            Object val = engine.get(var);
            data[i+1][0] = var;
            data[i+1][1] = printString(val);
        }
        showTitle("Global variables");
        showTable(data);
    }

    @SuppressWarnings("unchecked")
    private String printString(Object val)
    {
        if (val == null) {
            return "null";
        } else if (val instanceof String) {
            return "\"" + ((String) val).replaceAll("\\\"", "\\\\\\\"") + "\"";
        } else if (val instanceof Set<?>) {
            Set<Node> nodes = (Set<Node>) val;
            return "node-set (" + nodes.size() + " elements)";
        } else {
            return String.valueOf(val);
        }
    }
}
