/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe MErle
 *
 */
package org.ow2.frascati.fscript.console.commands;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fscript.model.fractal.FractalModelAttributes;
import org.objectweb.fractal.util.Fractal;

/**
 * This command adds an URL to the class-path used by FraSCAtiScript (especially the
 * embedded SCA Assembly Factory).
 * 
 * @author Christophe Demarey
 */
public class ClassPathAddCommand 
     extends AbstractCommand
{
  /**
   * Adds an URL at the class-path used by FraSCAtiScript.
   * 
   * @param arg An URL defining a valid path or jar file.
   */
  public final void execute(String name) throws Exception
  {
    URL url = null;
    try {
        url = new URL(name);
    } catch (MalformedURLException e) {
        showError("Invalid URL (" + name + "): " + e.getMessage());
        return;
    }
    Map<String, Object> ctx = getInstanciationContext();
    ClassLoader parent = null;
    if (ctx.containsKey("classloader")) {
        parent = (ClassLoader) ctx.get("classloader");
    } else {
        parent = Thread.currentThread().getContextClassLoader();
    }
    URL[] urls = new URL[] { url };
    ClassLoader cl = new URLClassLoader(urls, parent);
    ctx.put("classloader", cl);
    
    fscript.getClassLoaderManager().loadLibraries(urls);
    
    urls = ((URLClassLoader) cl).getURLs();
    for(URL u : urls) {
    	showMessage(u.toString());
    }
    showMessage("Classpath updated.");
  }

  protected final Map<String, Object> getInstanciationContext() throws Exception
  {
      ContentController cc = Fractal.getContentController( fscript.getFraSCAtiScriptComposite() );
      
      for (Component child : cc.getFcSubComponents()) {
          try {
              NameController nc = Fractal.getNameController(child);
              if ("model".equals(nc.getFcName())) {
                  FractalModelAttributes fma = (FractalModelAttributes) Fractal.getAttributeController(child);
                  return fma.getInstanciationContext();
              }
          } catch (NoSuchInterfaceException nsie) {
              continue;
          }
      }
      return null;
  }
}
