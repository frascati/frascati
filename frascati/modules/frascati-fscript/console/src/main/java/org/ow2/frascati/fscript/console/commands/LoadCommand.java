/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Pierre-Charles David
 *
 * Contributor(s): Christophe Demarey
 *                 Philippe Merle
 *
 */

package org.ow2.frascati.fscript.console.commands;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This command loads new FScript procedures definitions from a file.
 */
public class LoadCommand
     extends AbstractCommand
{
    public final void execute(String args) throws Exception
    {
        Reader reader = null;
        if (args.startsWith("classpath:")) {
            reader = getResourceReader(args.substring("classpath:".length()));
        } else if (args.matches("^[a-z]+:.*")) {
            reader = getURLReader(args);
        } else {
            reader = getFileReader(args);
        }
        if (reader == null) {
            return;
        }
        engine.eval(reader);
    }

    private Reader getResourceReader(String resource)
    {
        ClassLoader cl = fscript.getClassLoaderManager().getClassLoader();
        InputStream is = cl.getResourceAsStream(resource);
        if (is != null) {
            return new InputStreamReader(is);
        } else {
            showError("Not such resource in the classpath: " + resource + ".");
            return null;
        }
    }

    private Reader getURLReader(String url)
    {
        try {
            InputStream is = new URL(url).openStream();
            return new InputStreamReader(is);
        } catch (MalformedURLException e) {
            showError("Invalid URL (" + url + "): " + e.getMessage());
            return null;
        } catch (IOException e) {
            showError("Unable to open a connection on this URL (" + url + "): "
                    + e.getMessage());
            return null;
        }
    }

    private Reader getFileReader(String fileName)
    {
        try {
            return new FileReader(fileName);
        } catch (FileNotFoundException e) {
            showError("Unable to open file " + e.getMessage());
            return null;
        }
    }
}
