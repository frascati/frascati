/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Pierre-Charles David
 *
 * Contributor(s): Christophe Demarey
 *                 Philippe Merle
 *
 */
package org.ow2.frascati.fscript.console.commands;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.fscript.FScript;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;

/**
 * This command invokes a <code>Runnable</code> service interface from a component. in a separate
 * thread.
 */
public class RunCommand
     extends AbstractCommand
{
    public final void execute(String args) throws Exception
    {
        Object result = engine.eval(args);

        InterfaceNode itfNode = (InterfaceNode) FScript.getSingleNode(result);
        if (itfNode == null) {
            showError("Invalid expression value. Should return a Runnable interface node.");
            showResult(result);
            return;
        }

        Interface itf = itfNode.getInterface();
        if (!(itf instanceof Runnable)) {
            showError("This interface node is not a Runnable.");
            showResult(result);
            showMessage("Interface signature: " + itfNode.getSignature());
            return;
        }

        ensureComponentIsStarted(((InterfaceNode) itfNode).getInterface().getFcItfOwner());
        showMessage("Launching interface " + result + ".");
        Thread th = new Thread((Runnable) itf, "Service " + itfNode);
        th.setDaemon(true);
        th.start();
    }
}
