/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Pierre-Charles David
 *         Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.fscript.console.commands;

import javax.script.Bindings;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.fscript.console.Command;
import org.objectweb.fractal.fscript.console.Session;
import org.objectweb.fractal.fscript.diagnostics.DiagnosticListener;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.ow2.frascati.fscript.console.TextConsole;

import com.google.common.base.Preconditions;

/**
 * Common abstract base class for all the console's special commands.
 */
public abstract class AbstractCommand
           implements Command, Session
{
    /** The command's name. */
    private String name;

    /** A short (one-line) description of the command. */
    private String shortDescription;

    /** A long (multiple lines) description of the command. */
    private String longDescription;

    /** The session to use to interact with the user. */
    protected Session session;

    /** The FScript implementation component to use. */
    protected FraSCAtiFScript fscript;

    /** The <code>FScriptEngine</code> interface of the <code>fscript</code>
     * implementation component. */
    protected InvocableScriptEngine engine;

    // ========================================================================
    // Private methods
    // ========================================================================

    protected final void ensureComponentIsStarted(Component comp)
            throws IllegalLifeCycleException
    {
        try {
            LifeCycleController lcc = Fractal.getLifeCycleController(comp);
            if (!"STARTED".equals(lcc.getFcState())) {
                showMessage("Starting the component.");
                lcc.startFc();
            } else {
                showMessage("Component already started.");
            }
        } catch (NoSuchInterfaceException e) {
            showWarning("The component does not have a 'lifecycle-controller'.");
            showWarning("Assuming it is ready to use.");
        }
    }
    
    /**
     * Get the command context that will be used to execute this command.
     * @return the command context.
     */
    protected final Bindings getContext()
    {
    	return ((TextConsole) session).getContext();
    }

    // ========================================================================
	// Implementation of the Command API
	// ========================================================================

    public String getName()
    {
        return name;
    }

    public final void setName(String name)
    {
        this.name = name;
    }

    public final void setShortDescription(String desc)
    {
        if (desc != null) {
            this.shortDescription = desc;
        } else {
            this.shortDescription = "(no description available)";
        }
    }

    public String getShortDescription()
    {
        return this.shortDescription;
    }

    public final void setLongDescription(String desc)
    {
        if (desc != null) {
            this.longDescription = desc;
        } else {
            this.longDescription = "(no description available)";
        }
    }

    public String getLongDescription()
    {
        return longDescription;
    }

    public final void setFScriptEngine(Component newFscript)
    {
    	// parameter not used but need to be compliant with FScript API
    	this.fscript = FraSCAtiFScript.getSingleton();
    	this.engine = fscript.getScriptEngine();
    }

    // ========================================================================
	// Implementation of the Session API
	// ========================================================================

    public final void setSession(Session session)
    {
        Preconditions.checkNotNull(session, "Null session");
        this.session = session;
    }

    @Override
    public final String toString()
    {
        return ":" + getName();
    }

    // Delegate Session implementation to the current session

    public final void setSessionInterpreter(Component fscript)
    {
        session.setSessionInterpreter(fscript);
    }

    public final void showMessage(String message)
    {
        session.showMessage(message);
    }

    public final void showResult(Object result)
    {
        session.showResult(result);
    }

    public final void showWarning(String warning)
    {
        session.showWarning(warning);
    }

    public final void showError(String error)
    {
        session.showError(error);
    }

    public final void showError(String error, Throwable cause)
    {
        session.showError(error, cause);
    }

    public final void showTitle(String title)
    {
        session.showTitle(title);
    }

    public final void newline()
    {
        session.newline();
    }

    public final void showTable(String[][] table)
    {
        session.showTable(table);
    }

    public final DiagnosticListener getDiagnosticListener()
    {
        return session.getDiagnosticListener();
    }
}
