/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.console;

import javax.script.ScriptContext;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory;

/**
 * Main class to launch the FraSCAtiScript console.
 *
 * @author Christophe Demarey.
 */
public class Main
     extends org.objectweb.fractal.fscript.console.Main
{
	public static void main(String[] args) throws Exception
	{
		FraSCAtiScriptEngineFactory factory =  new FraSCAtiScriptEngineFactory();
		// Instantiate the engine
		Component fscriptCmpt = ((Interface) factory.getScriptEngine()).getFcItfOwner();
		// Update the engine context
		InvocableScriptEngine engine = FraSCAtiFScript.getSingleton().getScriptEngine();		
		factory.addDomainToContext( engine.getBindings(ScriptContext.GLOBAL_SCOPE) );
		// Launch the console
		new TextConsole( FraSCAtiFScript.getSingleton().getFraSCAtiScriptComposite() ).run();
		System.exit(0);
	}
}
