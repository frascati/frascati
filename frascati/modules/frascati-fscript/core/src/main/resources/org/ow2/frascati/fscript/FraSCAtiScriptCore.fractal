<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE definition PUBLIC "-//objectweb.org//DTD Fractal ADL 2.0//EN" "classpath://org/objectweb/fractal/adl/xml/standard.dtd">
<!--
 * ====================================================================
 *
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2013 Inria
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 * ====================================================================
-->

<!-- This is the main, top-level component implementing the default -->
<!-- FraSCAti FScript configuration. -->
<!-- The explorer has to be bound to a FraSCAti domain / runtime. -->

<definition name="org.ow2.frascati.fscript.FraSCAtiScriptCore"
            extends="org.objectweb.fractal.fscript.Jsr223FScript">
  <!-- External interfaces -->
  <interface name="sca-node-factory"
             role="server"
             signature="org.ow2.frascati.fscript.model.NodeFactory" />
  
  <interface name="domain-composite-manager" role="client" contingency="optional"
             signature="org.ow2.frascati.assembly.factory.api.CompositeManager" />
             
  <interface name="binding-factory" role="client" contingency="optional"
             signature="org.objectweb.fractal.bf.BindingFactory" />

  <!-- The model to use -->
  <component name="model"
             definition="org.ow2.frascati.fscript.model.FraSCAtiModel"/>
             
  <!-- Override the Script Engine implementation -->
  <component name="back-end">
    <component name="driver">
        <content class="org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngine" />
    </component>
  </component>
             
  <!-- Promote the sca-node-factory interface -->        
  <binding client="this.sca-node-factory"  server="model.sca-node-factory"/>
  
  <!-- Promote client interfaces -->
  <binding client="model.domain-composite-manager"      server="this.domain-composite-manager"/>
  <binding client="model.binding-factory" 				server="this.binding-factory"/>  
  
</definition>
 
