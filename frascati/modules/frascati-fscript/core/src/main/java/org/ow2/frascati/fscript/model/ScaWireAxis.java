/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.BindingAxis;

/**
 * Implements the <code>scawire</code> axis in FPath. This axis connects client
 * interfaces to the server interface(s) they are bound to. The
 * {@link #connect(Node, Node)} and {@link #disconnect(Node, Node)} operations on this
 * axis are used to create and destroy wires (fractal bindings).
 * 
 * @author Christophe Demarey
 */
public class ScaWireAxis
     extends BindingAxis
{
  /**
   * Default constructor
   * 
   * @param model The model referencing this axis.
   */
  public ScaWireAxis(FraSCAtiModel model)
  {
    super(model, "scawire", "scareference", "scaservice");
  }

  protected Node getServerInterface(BindingController bc, String clItfName)
  {
      try {
          Interface serverItf = (Interface) bc.lookupFc(clItfName);
          if (serverItf != null) {
              NodeFactory nf = (NodeFactory) model;
              return nf.createScaServiceNode(serverItf);
          } else {
              return null;
          }
      } catch (NoSuchInterfaceException e) {
          throw new AssertionError("Node references non-existing interface.");
      }
  }
}
