/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;
import org.objectweb.fractal.fscript.types.UnionType;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Implements the <code>scaproperty</code> axis in FPath. This axis connects a component
 * to its SCA properties (if any), as defined by its <code>sca-property-controller</code>
 * interface. This axis is not modifiable.
 * 
 * @author Christophe Demarey
 */
public class ScaPropertyAxis
     extends AbstractAxis
{
  public ScaPropertyAxis(FraSCAtiModel model)
  {
    super(model, "scaproperty",
          new UnionType(model.getNodeKind("scacomponent"), model.getNodeKind("scareference"), model.getNodeKind("scaservice")),
          model.getNodeKind("scaproperty"));
  }

  public boolean isPrimitive()
  {
    return true;
  }

  public boolean isModifiable()
  {
    return false;
  }

  public Set<Node> selectFrom(Node source)
  {
    Component comp = null;
    if (source instanceof ComponentNode) {
      comp = ((ComponentNode) source).getComponent();
    } else if (source instanceof InterfaceNode) {
      comp = ((InterfaceNode) source).getInterface().getFcItfOwner();
    } else {
      throw new IllegalArgumentException("Invalid source node kind " + source.getKind());
    }
    
    SCAPropertyController ctl = null;
    Set<Node> result = new HashSet<Node>();
    try {
      ctl = (SCAPropertyController) comp.getFcInterface(SCAPropertyController.NAME);
      for (String name : ctl.getPropertyNames() ) {
        Node node = new ScaPropertyNode((FraSCAtiModel) model, comp, ctl, name);
        result.add(node);
      }
    } catch (NoSuchInterfaceException e) {
      // Not an SCA component
      String compName = null;
      try {
        compName = Fractal.getNameController(comp).getFcName();
      } catch (NoSuchInterfaceException e1) {
        // Should not happen
        e1.printStackTrace();
      }
      throw new IllegalArgumentException("Invalid source node kind: "+ compName + " is not an SCA component!");
    }
    
    return result;
  }
  
}
