/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import static org.objectweb.fractal.fscript.types.PrimitiveType.BOOLEAN;
import static org.objectweb.fractal.fscript.types.PrimitiveType.OBJECT;
import static org.objectweb.fractal.fscript.types.PrimitiveType.STRING;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.fscript.model.ComposedAxis;
import org.objectweb.fractal.fscript.model.Property;
import org.objectweb.fractal.fscript.model.ReflectiveAxis;
import org.objectweb.fractal.fscript.model.TransitiveAxis;
import org.objectweb.fractal.fscript.model.fractal.FractalModel;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.fscript.procedures.AddRestBinding;
import org.ow2.frascati.fscript.procedures.AddWsBinding;
import org.ow2.frascati.fscript.procedures.ScaNativeProcedure;

/**
 * This class represents the SCA Fractal component model in terms of the
 * {@link Model} APIs. It describes which kinds of nodes are present in a SCA
 * architecture, what properties these nodes have, and what axes can connect these nodes
 * together to form a complete SCA architecture.
 * 
 * @author Christophe Demarey.
 */
public class FraSCAtiModel 
     extends FractalModel 
  implements NodeFactory
{
  /** Names of bindings */
  public static final String COMPOSITE_MANAGER_ITF = "domain-composite-manager";
  public static final String BINDING_FACTORY_ITF = "binding-factory";
  public static final String NAME = "name";
  /** The FraSCAti domain composite Manager. */
  private CompositeManager compositeManager;
  /** The Binding Factory. */
  private BindingFactory bindingFactory;
 
  /**
   * Contributes the different kinds of nodes used to represent Fractal architectures.
   */
  @Override
  protected void createNodeKinds()
  {
    super.createNodeKinds();
    addKind( "scacomponent", 
             new Property(NAME, STRING, true),
             new Property("state", STRING, true) );
    addKind( "scaservice", 
             new Property(NAME, STRING, false),
             new Property("signature", STRING, false),
             new Property("collection", BOOLEAN, false) );
    addKind( "scareference", 
            new Property(NAME, STRING, false),
            new Property("signature", STRING, false),
            new Property("collection", BOOLEAN, false) );
    addKind( "scaproperty",
             new Property(NAME, STRING, true),
             new Property("value", OBJECT, true) );
    addKind( "scabinding",
             new Property(NAME, STRING, true),
             new Property("state", STRING, true) );
    addKind( "scaintent",
            new Property(NAME, STRING, true),
            new Property("state", STRING, true) );
  }

  /**
   * Contributes the axes which can be used to connect Fractal nodes in order to
   * represent the structure of a Fractal architecture.
   */
  @Override
  protected void createAxes()
  {
    super.createAxes();
    // Base axes
    addAxis(new ScaChildAxis(this));
    addAxis(new ScaParentAxis(this));
//    addAxis(new ComponentAxis(this));
    addAxis(new ScaServiceAxis(this));
    addAxis(new ScaReferenceAxis(this));
    addAxis(new ScaPropertyAxis(this));
    addAxis(new ScaWireAxis(this));
    addAxis(new ScaBindingAxis(this));
    addAxis(new ScaIntentAxis(this));
    // Transitive axes
    addAxis(new TransitiveAxis(getAxis("scachild"), "scadescendant"));
    addAxis(new TransitiveAxis(getAxis("scaparent"), "scaancestor"));
    // Composed axes
    addAxis(new ComposedAxis("scasibling", getAxis("scaparent"), getAxis("scachild")));
    // Reflective axes
    addAxis(new ReflectiveAxis(getAxis("scachild")));
    addAxis(new ReflectiveAxis(getAxis("scaparent")));
    addAxis(new ReflectiveAxis(getAxis("scadescendant")));
    addAxis(new ReflectiveAxis(getAxis("scaancestor")));
    addAxis(new ReflectiveAxis(getAxis("scasibling")));
  }

  /**
   * Contributes a few custom procedures to manipulate Fractal architecture which can
   * not be described and generated in the framework of the {@link Model} APIs.
   */
  @Override
  protected void createAdditionalProcedures()
  {
    super.createAdditionalProcedures();
      
    List<ScaNativeProcedure> procedures = new ArrayList<ScaNativeProcedure>();
    procedures.add( new ScaNewAction() );
    procedures.add( new ScaRemoveAction() );
    procedures.add( new AddWsBinding() );
    procedures.add( new AddRestBinding() );

    for (ScaNativeProcedure proc : procedures) {
        try {
            proc.bindFc(proc.MODEL_ITF_NAME, this);
        } catch (Exception e) {
            throw new AssertionError("Internal inconsistency with " + proc.getName() + " procedure!");
        }
        addProcedure(proc);
    }
  }
  
  /**
   * Create a new ScaComponentNode instance.
   * 
   * @param comp The SCA component to encapsulate.
   * @return the ScaComponentNode instance
   */
  public ScaComponentNode createScaComponentNode(Component comp)
  {
    return new ScaComponentNode(this, comp);
  }

  /**
   * Create a new ScaServiceNode instance.
   * 
   * @param comp The SCA service to encapsulate.
   * @return the ScaServiceNode instance
   */
  public ScaServiceNode createScaServiceNode(Interface itf)
  {
    return new ScaServiceNode(this, itf);
  }
  
  /**
   * Create a new ScaReferenceNode instance.
   * 
   * @param comp The SCA reference to encapsulate.
   * @return the ScaReferenceNode instance
   */
  public ScaReferenceNode createScaReferenceNode(Interface itf)
  {
    return new ScaReferenceNode(this, itf);
  }
  
  /**
   * Print this model.
   */
  @Override
  public String toString()
  {
    return "FraSCAti model";
  }
  
  public String[] listFc()
  {
    String[] tmp = super.listFc();
    String[] result = new String[tmp.length + 2];
    int idx = tmp.length;
    System.arraycopy(tmp, 0, result, 0, idx);
    
    tmp = new String[] { COMPOSITE_MANAGER_ITF, BINDING_FACTORY_ITF };
    System.arraycopy(tmp, 0, result, idx, tmp.length);
    return result;
  }

  public Object lookupFc(String clItfName) throws NoSuchInterfaceException
  {
    if (COMPOSITE_MANAGER_ITF.equals(clItfName)) {
        return this.compositeManager;
    } else if (BINDING_FACTORY_ITF.equals(clItfName)) {
        return this.bindingFactory;
    } else {
      return super.lookupFc(clItfName);
    }
  }

  public void bindFc(String clItfName, Object srvItf) throws NoSuchInterfaceException
  {
    if  (COMPOSITE_MANAGER_ITF.equals(clItfName)) {
        this.compositeManager = (CompositeManager) srvItf;
    } else if (BINDING_FACTORY_ITF.equals(clItfName)) {
        this.bindingFactory = (BindingFactory) srvItf;
    } else {
      super.bindFc(clItfName, srvItf);
    }
  }

  public void unbindFc(String clItfName) throws NoSuchInterfaceException
  {
    if (COMPOSITE_MANAGER_ITF.equals(clItfName)) {
        this.compositeManager = null;
    } else if (BINDING_FACTORY_ITF.equals(clItfName)) {
        this.bindingFactory = null;
    } else {
      super.unbindFc(clItfName);
    }
  }

}
