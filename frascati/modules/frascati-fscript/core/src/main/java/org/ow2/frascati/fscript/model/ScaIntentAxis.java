/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;
import org.objectweb.fractal.fscript.types.UnionType;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * Implements the <code>scaintent</code> axis in FPath. This axis connects SCA
 * intents to the interface(s) they are bound to. The
 * {@link #connect(Node, Node)} and {@link #disconnect(Node, Node)} operations on this
 * axis are used to create and destroy intents.
 * 
 * @author Christophe Demarey
 */
public class ScaIntentAxis
     extends AbstractAxis
{
  private Logger log = Logger.getLogger(this.getClass().getCanonicalName());

  /**
   * Default constructor
   * 
   * @param model The model referencing this axis.
   */
  public ScaIntentAxis(FraSCAtiModel model)
  {
    super( model, "scaintent",
           new UnionType( model.getNodeKind("scaservice"), model.getNodeKind("scareference") ),
           model.getNodeKind("scacomponent") );
  }

  public boolean isPrimitive()
  {
      return true;
  }

  public boolean isModifiable()
  {
      return true;
  }

  /**
   * Get a reference to the SCA component implementing the SCA intent (WS, RMI, etc.).
   * 
   * @param source
   *            the source node from which to select the binding component.
   * @return the binding component node the given source node is connected to.
   */
  public Set<Node> selectFrom(Node source)
  {
      Component comp = null;
      Interface itf = null;
      SCABasicIntentController ic = null;
      
      try {
          comp = ((ComponentNode) source).getComponent();  
      } catch (ClassCastException e1) {
          try {
              itf = ((InterfaceNode) source).getInterface();
              comp = itf.getFcItfOwner();  
          } catch (ClassCastException e2) {
              throw new IllegalArgumentException("Invalid source node kind " + source.getKind());
          }
      }

      Set<Node> result = new HashSet<Node>();
      
      try {
          ic = (SCABasicIntentController) comp.getFcInterface(SCABasicIntentController.NAME);
      } catch (NoSuchInterfaceException e) {
          // No intent controller => no intents on this component
          return Collections.emptySet();
      }
      
      try {
          if (itf == null) {
              for (Object anItf : comp.getFcInterfaces() ) {
                  result.addAll( getIntentNodes(ic, (Interface) anItf) );
              }
          } else {
              result.addAll( getIntentNodes(ic, (Interface) itf) );
          }
      } catch (NoSuchInterfaceException e) {
          log.warning("One interface cannot be retrieved on " + comp + "!");
          e.printStackTrace();
      }

      return result;
  }
  
  /**
   * Get the list of intent nodes for a given business interface.
   * 
   * @param ic - The intent controller of the interface owner (the component under introspection).
   * @param itf - The interface to search intents on.
   * @return The list of intents found for this interface (can be empty!).
   * @throws NoSuchInterfaceException if an interface is not found.
   */
  private List<ScaIntentNode> getIntentNodes(SCABasicIntentController ic, Interface itf)
    throws NoSuchInterfaceException
  {
      List<ScaIntentNode> nodes = new ArrayList<ScaIntentNode>();
      String itfName = itf.getFcItfName();

      if ( !itfName.endsWith("-controller") && !itfName.equals("component")) { // no intents on controllers
          List<IntentHandler> intents = ic.listFcIntentHandler( itf.getFcItfName() );
          
          for (IntentHandler intent : intents) {
              Component impl = ((Interface) intent).getFcItfOwner();
              String name = Fractal.getNameController(impl).getFcName();
              nodes.add( new ScaIntentNode((FraSCAtiModel) this.model, ic, name, impl, itf) );
          }
      }
      return nodes;
  }
  
  @Override
  public void connect(Node source, Node dest)
  {
      Interface itf = ((InterfaceNode) source).getInterface();
      Component intent = ((ScaComponentNode) dest).getComponent(),
                itfOwner = null;
      SCABasicIntentController ic = null;
      String intentName = null;

      // Get the interface owner
      try {
          itfOwner = itf.getFcItfOwner();
      } catch (ClassCastException cce) {
          log.log(Level.WARNING, "An intent can only be applied on components, services and references!");
          return;
      }      
      
      // Get intent controller
      try {
          ic = (SCABasicIntentController) itfOwner.getFcInterface(SCABasicIntentController.NAME);
      } catch (NoSuchInterfaceException nsie) {
          log.log(Level.SEVERE, "Cannot access to intent controller", nsie);
          return;
      }

      if (intent != null) {
          String itfName = null;
          
          try {
            intentName = Fractal.getNameController(intent).getFcName();
          } catch (NoSuchInterfaceException nsie) {
              log.log(Level.SEVERE, "Cannot find the Name Controller!", nsie);
          }
    
          try {
              // Stop owner component
              Fractal.getLifeCycleController(itfOwner).stopFc();
              // Get intent handler
              IntentHandler h = TinfiDomain.getService(intent, IntentHandler.class, "intent");
              // Add intent
              if ( itf != null ) {
                  itfName = itf.getFcItfName();
                  ic.addFcIntentHandler(h, itfName);
              } else {
                  ic.addFcIntentHandler(h);
              }
              // Start owner component
              Fractal.getLifeCycleController(itfOwner).startFc();
          } catch (Exception e1) {
              String errorMsg = (itf != null) ? "interface " + itfName : "component"; 
              log.log(Level.SEVERE, "Intent '" + intentName
                      + "' cannot be added to " + errorMsg, e1);
          }
      }
  }
  
  @Override
  public void disconnect(Node source, Node dest)
  {
      Interface itf = ((InterfaceNode) source).getInterface();
      Component intent = ((ScaComponentNode) dest).getComponent(),
                itfOwner = itf.getFcItfOwner();
      SCABasicIntentController ic = null;

      // Get intent controller
      try {
          ic = (SCABasicIntentController) itf.getFcItfOwner().getFcInterface(SCABasicIntentController.NAME);
      } catch (NoSuchInterfaceException nsie) {
          log.log(Level.SEVERE, "Cannot access to intent controller", nsie);
      }

      // Remove intent
      try {
          // Stop owner component
          Fractal.getLifeCycleController(itfOwner).stopFc();
          // Get intent handler
          IntentHandler h = TinfiDomain.getService(intent, IntentHandler.class, "intent");
          // remove intent
          ic.removeFcIntentHandler(h, itf.getFcItfName());
          // Start owner component
          Fractal.getLifeCycleController(itfOwner).startFc();
      } catch (NoSuchInterfaceException nsie) {
          log.log(Level.SEVERE, "Cannot retrieve the interface name!", nsie);
      } catch (IllegalLifeCycleException ilce) {
          log.log(Level.SEVERE, "Illegal life cycle Exception!", ilce);
      }
          
  }
}
