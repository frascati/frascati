/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.procedures;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.rmi.RmiRegistryCreationException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.fscript.model.FraSCAtiModel;

/**
 * Abstract class allowing to add bindings 
 * to an SCA service/reference.
 *
 * @author Christophe Demarey
 */
public abstract class AddBinding
           implements ScaNativeProcedure
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------
    public static Logger logger = Logger.getLogger("org.ow2.frascati.fscript.procedures.AddBinding");

    /** Classloader constant for Fractal Binding Factory */
    public static final String CLASSLOADER = "classloader";
    /** plug-in id constant for Fractal Binding Factory */
    public static final String PLUGIN_ID = "plugin.id";
    
    /** The FraSCAti model component */
    protected FraSCAtiModel model; // for the binding

    // --------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------

    /**
     * Default constructor.
     */
    public AddBinding()
    {
    }

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * Export an SCA service or Unbind an SCA reference with the Binding Factory.
     * Callback method used by the NewBindingForm GUI to create the binding.
     *
     * @param itfName The interface to add binding on.
     * @param owner The component owning the interface to add binding on.
     * @param isScaReference Is the interface to bind an SCA reference?
     * @param hints A map with entries needed by the Binding Factory.
     */
    public void createBinding( String itfName,
                               Component owner,
                               boolean isScaReference,
                               Map<String, Object> hints)
    {
        LifeCycleController lcc = null;
        BindingFactory bf  = null;

        // Set the class loader
        try {
          hints.put(CLASSLOADER, owner.getFcInterface(itfName).getClass().getClassLoader());
        } catch (NoSuchInterfaceException nsie) {
            logger.log(Level.SEVERE, "Cannot retrieve interface " + itfName, nsie);
            return;
        }

        try {
            lcc = Fractal.getLifeCycleController(owner);
            lcc.stopFc();
        } catch (NoSuchInterfaceException nsie) {
            logger.log(Level.SEVERE, "Error while getting component life cycle controller", nsie);
            return;
        } catch (IllegalLifeCycleException ilce) {
            logger.log(Level.SEVERE, "Cannot stop the component!", ilce);
            return;
        }
        
        try {
            bf = (BindingFactory) model.lookupFc(FraSCAtiModel.BINDING_FACTORY_ITF);
        } catch (NoSuchInterfaceException nsie) {
            logger.log(Level.SEVERE, "Cannot retrieve the Binding Factory!", nsie);
            return;
        }
        
        try {
            logger.fine("Calling binding factory\n bind: "
                    + Fractal.getNameController(owner).getFcName() + " -> "
                    + itfName);
            if( isScaReference ) {
                bf.bind(owner, itfName, hints);
            } else {
                bf.export(owner, itfName, hints);
            }
        } catch (BindingFactoryException bfe) {
        	logger.log(Level.SEVERE, "Error while binding "
                       + (isScaReference ? "reference" : "service") 
                       + ": " + itfName, bfe);
            return;
        } catch (NoSuchInterfaceException nsie) {
        	logger.log(Level.SEVERE, "Error while getting component name controller", nsie);
            return;
        } catch (RmiRegistryCreationException rmie) {
            String msg = rmie.getMessage() + "\nAddress already in use?";
            JOptionPane.showMessageDialog(null, msg);
            logger.log(Level.SEVERE, msg); //, rmie);
            return;
        }
        
        try {
            lcc.startFc();
        } catch (IllegalLifeCycleException ilce) {
        	logger.log(Level.SEVERE,"Cannot start the component!", ilce);
        }
    }
    
    public String[] listFc()
    {
        return new String[] { MODEL_ITF_NAME };
    }

    public void bindFc(String itfName, Object srvItf) throws NoSuchInterfaceException
    {
        if (MODEL_ITF_NAME.equals(itfName)) {
            this.model = (FraSCAtiModel) srvItf;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }

    public Object lookupFc(String itfName) throws NoSuchInterfaceException
    {
        if (MODEL_ITF_NAME.equals(itfName)) {
            return this.model;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }

    public void unbindFc(String itfName) throws NoSuchInterfaceException
    {
        if (MODEL_ITF_NAME.equals(itfName)) {
            this.model = null;
        } else {
            throw new NoSuchInterfaceException(itfName);
        }
    }
}
