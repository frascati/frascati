/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;

/**
 * A <em>factory</em> for SCA-specific {@linkplain Node nodes}. This interface can
 * be used by clients to wrap objects from the standard SCA API in {@link Node}s
 * appropriate for FPath to manipulate in a uniform way.
 * 
 * @author Christophe Demarey
 */
public interface NodeFactory
{
    /**
     * Create a new ScaComponentNode instance.
     * 
     * @param comp The SCA component to encapsulate.
     * @return the ScaComponentNode instance
     */
    ScaComponentNode createScaComponentNode(Component comp);

    /**
     * Create a new ScaServiceNode instance.
     * 
     * @param comp The SCA service to encapsulate.
     * @return the ScaServiceNode instance
     */
    ScaServiceNode createScaServiceNode(Interface itf);
    
    /**
     * Create a new ScaReferenceNode instance.
     * 
     * @param comp The SCA reference to encapsulate.
     * @return the ScaReferenceNode instance
     */
    ScaReferenceNode createScaReferenceNode(Interface itf);  
}