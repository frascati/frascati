/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */
package org.ow2.frascati.fscript.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.SuperController;
import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.objectweb.fractal.util.Fractal;

/**
 * Implements the <code>scaparent</code> axis in FPath. This axis connects SCA
 * components to their direct parents (if any). This axis is not modifiable 
 * directly (although changes to the <code>child</code> axis will reflect on
 * this one).
 *
 * @author Christophe Demarey.
 */
public class ScaParentAxis
     extends AbstractAxis
{
  /** The logger */
  private static Logger logger = Logger.getLogger("org.ow2.frascati.fscript.model.ScaParentAxis");

  /**
   * Default constructor.
   * 
   * @param model The model referencing this axis.
   */
  public ScaParentAxis(FraSCAtiModel model)
  {
    super(model, "scaparent", "scacomponent", "scacomponent");
  }
  
  public boolean isPrimitive()
  {
      return true;
  }

  public boolean isModifiable()
  {
      return false;
  }

  /**
   * Get nodes for this axis from the specified source node.
   * 
   * @param source The source node to introspect.
   * @return A set of nodes.
   */
  public Set<Node> selectFrom(Node source)
  {
      Component comp = ((ComponentNode) source).getComponent();
      String compName = null;
      
      try {
          compName = Fractal.getNameController(comp).getFcName();
      } catch (NoSuchInterfaceException e1) {
          logger.warning(comp + "should have a Name Controller!");
      }
      
      try {
          SuperController cc = Fractal.getSuperController(comp);
          Set<Node> result = new HashSet<Node>();
          for (Component parent : cc.getFcSuperComponents()) {
            try {
              parent.getFcInterface(ScaBindingAxis.SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
              ScaComponentNode node = ((NodeFactory) model).createScaComponentNode(parent);
              // Check that parent is not an hidden frascati container
              if (compName != null) {
                  String parentName = null;
                  try {
                      parentName = Fractal.getNameController(parent).getFcName();
                  } catch (NoSuchInterfaceException e1) {
                      logger.warning(parent + "should have a Name Controller!");
                  }
                  if ( (parentName != null) && (parentName.compareTo(compName+"-container") != 0)) {
                      result.add(node);
                  }
              }
            } catch (NoSuchInterfaceException e) {
              /* The sca-component-controller cannot be found!
                 This component is not a SCA component => Nothing to do! */
            }
          }
          return result; // Collections.unmodifiableSet(result);
      } catch (NoSuchInterfaceException e) {
          // Not a composite, no children.
          return Collections.emptySet();
      }
  }
  
}
