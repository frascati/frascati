/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.model;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;

/**
 * A {@link Node} which represents a FraSCAti component. Note that in FPath, component
 * nodes are distinct from interface nodes representing the <code>component</code>
 * interface.
 * 
 * @author Christophe Demarey
 */
public class ScaComponentNode
     extends ComponentNode
{
  /**
   * Creates a new <code>ScaComponentNode</code>.
   * 
   * @param model
   *            the FraSCAti model this node is part of.
   * @param component
   *            the component the new node will represent.
   */
  protected ScaComponentNode(FraSCAtiModel model, Component component)
  {
      super( model, component, "scacomponent");
  }

  @Override
  public final String toString()
  {
      return "#<scacomponent: " + getName() + ">";
  }
}
