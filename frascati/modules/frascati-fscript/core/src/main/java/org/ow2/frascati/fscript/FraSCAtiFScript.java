/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Christophe Demarey
 */

package org.ow2.frascati.fscript;

import javax.script.ScriptContext;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.FScript;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory;
import org.ow2.frascati.fscript.model.NodeFactory;
import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;
import org.ow2.frascati.util.AbstractActiveComponent;

/**
 * Implementation of the OW2 FraSCAti FScript component.
 * 
 * @author Philippe Merle.
 */
public class FraSCAtiFScript
     extends AbstractActiveComponent
{
	/** The factory singleton */
	private static FraSCAtiFScript singleton;

	/** Reference to the OW2 FraSCAti FScript engine. */
	@Reference(name = "engine")
	private InvocableScriptEngine engine;

	/** Reference to the OW2 FraSCAti FScript SCA node factory. */
	@Reference(name = "sca-node-factory")
	private NodeFactory nodeFactory;

	/** Reference to the OW2 FraSCAti composite manager. */
	@Reference(name = "domain-composite-manager")
	private CompositeManager compositeManager;
	
	/** Reference to the OW2 FraSCAti class loader manager. */
	@Reference(name = "domain-classloader-manager")
	private ClassLoaderManager classLoaderManager;

	/**
	 * Default constructor
	 */
	public FraSCAtiFScript()
	{
		if (singleton == null) {
			singleton = this;
		}
	}
		
	/**
	 * Get the FraSCAti FScript singleton. 
	 * Used to deal with object <-> components world. 
	 * @return A reference to the  FraSCAti FScript singleton.
	 */
	public static FraSCAtiFScript getSingleton()
	{
		return singleton;
	}
	
	/**
	 * Body of the component.
	 */
	public final void run()
	{
		FraSCAtiScriptEngineFactory factory =  new FraSCAtiScriptEngineFactory();
		
		engine.createBindings();
		factory.addDomainToContext( engine.getBindings(ScriptContext.GLOBAL_SCOPE) );

        // Load the FScript standard library.
        try {
          FScript.loadStandardLibrary(getFraSCAtiScriptComposite());
        } catch(Exception exc) {
          log.warning("Can't load FScript standard library");
        }
    }
	
	/**
	 * Get a reference to the FraSCAti FScript engine.
	 * @return the reference to the FraSCAti FScript engine, null if the SCA composite is not instantiated.
	 */
	public final InvocableScriptEngine getScriptEngine()
	{
		return engine;
	}

	/**
	 * Get a reference to the FraSCAti SCA node factory.
	 * @return the reference to the FraSCAti FScript SCA node factory, null if the SCA composite is not instantiated.
	 */
	public final NodeFactory getNodeFactory()
	{
		return nodeFactory;
	}

	/**
	 * Get a reference to the FraSCAti composite manager.
	 * @return the reference to the FraSCAti composite manager, null if the SCA composite is not instantiated.
	 */
	public final CompositeManager getCompositeManager()
	{
		return compositeManager;
	}

	/**
	 * Get a reference to the FraSCAti class loader manager.
	 * @return the reference to the FraSCAti class loader manager, null if the SCA composite is not instantiated.
	 */
	public final ClassLoaderManager getClassLoaderManager()
	{
		return classLoaderManager;
	}
	
	/**
	 * Get the FraSCAti FScript composite, i.e. the fractal component
	 * implementing the whole FraSCAti FScript engine;
	 * 
	 * @return the FraSCAti FScript composite.
	 */
	public final Component getFraSCAtiScriptComposite()
	{
		ServiceReferenceImpl<?> engineItf = (ServiceReferenceImpl<?>) engine;
		Component fscriptComp = ((Interface) engineItf._getDelegate()).getFcItfOwner();
		Interface engineSrvItf = null;
		
		try {
			engineSrvItf = (Interface) Fractal.getBindingController(fscriptComp).lookupFc("engine");
			return engineSrvItf.getFcItfOwner();
		} catch (NoSuchInterfaceException e) {
			// should not happen
			e.printStackTrace();
			return null;
		}
	}

}
