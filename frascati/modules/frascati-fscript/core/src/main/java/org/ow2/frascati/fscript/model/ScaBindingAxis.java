/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.adl.util.ContentControllerHelper;
import org.objectweb.fractal.adl.util.NoSuchComponentException;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.fscript.model.AbstractAxis;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;
import org.objectweb.fractal.fscript.types.UnionType;
import org.objectweb.fractal.util.BindingControllerHelper;
import org.objectweb.fractal.util.Fractal;

/**
 * Implements the <code>scabinding</code> axis in FPath. This axis connects client
 * interfaces to the server interface(s) they are bound to. The
 * {@link #connect(Node, Node)} and {@link #disconnect(Node, Node)} operations on this
 * axis are used to create and destroy bindings.
 * 
 * @author Christophe Demarey
 */
public class ScaBindingAxis
     extends AbstractAxis
{
	
	final static String SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME = "sca-component-controller";
	
  /** The logger */
  private static Logger logger = Logger.getLogger("org.ow2.frascati.fscript.model.ScaBindingAxis");
    
  /**
   * Default constructor
   * 
   * @param model The model referencing this axis.
   */
  public ScaBindingAxis(FraSCAtiModel model)
  {
    super( model, "scabinding", 
           new UnionType( model.getNodeKind("scaservice"), model.getNodeKind("scareference") ),
           model.getNodeKind("scabinding") );
  }

  public boolean isPrimitive()
  {
      return true;
  }

  public boolean isModifiable() 
  {
      return true;
  }

  /**
   * Get a reference to the component implementing the SCA binding (WS, RMI, etc.).
   * 
   * @param source
   *            the source node from which to select the binding component.
   * @return the binding component node the given source node is connected to.
   */
  public Set<Node> selectFrom(Node source)
  {
    Set<Node> result = new HashSet<Node>(); 
    InterfaceNode itfNode = null;
    Interface itf = null;

    try {
        itfNode = (InterfaceNode) source;
        itf = itfNode.getInterface();
    } catch (ClassCastException e) {
        logger.info("The scabinding axis is only available for Interface nodes!");
        return Collections.emptySet();
    }
    
    if (itfNode.isClient()) { // Display stub components
        BindingController bc;
        ScaBindingNode bindingNode = null;
        Interface serverItf = null;
    	Component itfOwner = itf.getFcItfOwner();
    	try {
            bc = Fractal.getBindingController(itfOwner);
            serverItf = (Interface) bc.lookupFc( itf.getFcItfName() );
            // TODO: remove this block as soon as the RMI connector is refactored
            try {
                String boundInterfaceOwnerName = Fractal.getNameController( serverItf.getFcItfOwner() ).getFcName();
                if ( boundInterfaceOwnerName.contains("-rmi-stub") ) {
                    try {
                        Component stub = ContentControllerHelper.getSubComponentByName(serverItf.getFcItfOwner(), "rmi-stub-primitive");
                        bindingNode = new ScaBindingNode((FraSCAtiModel) this.model, stub); 
                        result.add( bindingNode );
                        return result;
                    } catch (NoSuchComponentException e) {
                        logger.severe("Cannot found rmi-stub-primitive component in the " + boundInterfaceOwnerName + "composite!");
                    }
                }
            } catch (NoSuchInterfaceException e1) {
                e1.printStackTrace();
                return Collections.emptySet();
            }
        } catch (NoSuchInterfaceException e) {
            logger.fine( e.toString() );
            return Collections.emptySet();
        }
    	bindingNode = new ScaBindingNode((FraSCAtiModel) this.model, serverItf.getFcItfOwner()); 
    	result.add( bindingNode );
    } else { // Display skeleton components
        ScaBindingNode bindingNode;
        Set<Interface> clientItfs = BindingControllerHelper.getFcClientItfsBoundTo(itf);
        
        for (Interface clientItf : clientItfs) {
            Component clientItfOwner = clientItf.getFcItfOwner();
            
            try {
                clientItfOwner.getFcInterface(SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
                // Nothing to do : we don't want to display wires
            } catch (NoSuchInterfaceException e) {
                // Not an SCA component => it's a binding component
                bindingNode = new ScaBindingNode((FraSCAtiModel) this.model, clientItfOwner);
                result.add( bindingNode );
            } 
        }
        
        // TODO: remove this block as soon as the RMI connector is refactored
        Node rmiBinding = getRmiBinding(itf);
        if (rmiBinding != null) {
        	result.add(rmiBinding);          
        }
    }
    
    return result;
  }
  
  /**
   * Retrieve RMI bindings for the given interface.
   * TODO: remove this method as soon as the RMI connector is refactored 
   *       to comply with the connectors common API. 
   * 
   * @param itf The interface to introspect.
   * @return a Binding node if an RMI binding is found, else null.
   */
  private Node getRmiBinding(Interface itf)
  {
    Component itfOwner = itf.getFcItfOwner(); 
      
    try {
        Component[] parents = Fractal.getSuperController(itfOwner).getFcSuperComponents();
        for (Component parent : parents) {
            Component[] children = Fractal.getContentController(parent).getFcSubComponents();
            for (Component child : children) {            
                String childName = Fractal.getNameController(child).getFcName();
                if ( childName.contains("-rmi-skeleton") ) {
                    try {
                        Component skeleton = ContentControllerHelper.getSubComponentByName(child, "org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonContent");
                        return new ScaBindingNode((FraSCAtiModel) this.model, skeleton); 
                    } catch (NoSuchComponentException e) {
                        logger.severe("Cannot found org.objectweb.fractal.bf.connectors.rmi.RmiSkeletonContent component in the " + childName + "composite!");
                    }
                }
            }
        }
    } catch (NoSuchInterfaceException e) {
        // components may not have a super controller
        logger.fine( e.toString() );
        return null;
    }
    return null; // No RMI binding found! 
  }

  /**
   * Remove an SCA binding (dest) from an SCA service or reference (source). 
   */
  @Override
  public void disconnect(Node source, Node dest)
  {
      Interface itf = ((InterfaceNode) source).getInterface(); // The SCA interface
      Component bindingComponent = ((ScaBindingNode) dest).getComponent(); // the BF stub or skeleton      
      boolean isScaReference = source instanceof ScaReferenceNode;
      final String itfName = itf.getFcItfName(); // The SCA interface name
      Component owner = itf.getFcItfOwner(); // The SCA interface owner component
      FraSCAtiModel model = (FraSCAtiModel) this.model;       
      BindingFactory bf = null;
      
      try {
        bf = (BindingFactory) model.lookupFc(FraSCAtiModel.BINDING_FACTORY_ITF);
      } catch (NoSuchInterfaceException nsie) {
          logger.log(Level.SEVERE, "Cannot retrieve the Binding Factory!", nsie);
          return;
      }
      
      try {
          if ( isScaReference ) {
              bf.unbind(owner, itfName, bindingComponent);
          } else {
              bf.unexport(owner, bindingComponent, new HashMap<String, Object>());
          }
      } catch (BindingFactoryException bfe) {
          logger.log(Level.SEVERE, "Cannot unbind/unexport this service/reference!", bfe);
      }
  }
  
}
