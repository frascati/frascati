/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.model.AbstractNode;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * A {@link Node} which represents a SCA property.
 * 
 * @author Christophe Demarey
 */
public class ScaPropertyNode
     extends AbstractNode
{
  /** The name of this property. */
  private final String propName;
  /** The owner of this property. */
  private final Component owner;
  /** The controller managing SCA properties for the owner. */
  private final SCAPropertyController controller;
  /** The logger */
  private static Logger log = Logger.getLogger("org.ow2.frascati.fscript.model.ScaPropertyNode");

  /**
   * Creates a new <code>ScaPropertyNode</code>.
   * 
   * @param model
   *            the FraSCAti model this node is part of.
   * @param ctl
   *            the SCA property controller holding this attribute.
   * @param propName
   *            the name of the property.
   */
  protected ScaPropertyNode( FraSCAtiModel model, 
                             Component owner,
                             SCAPropertyController ctl,
                             String propName)
  {
      super( model.getNodeKind("scaproperty") );
      checkNotNull(propName);
      this.propName = propName;
      checkNotNull(owner);
      this.owner = owner;
      checkNotNull(ctl);
      this.controller = ctl;
  }

  /**
   * Returns the name of this property.
   * 
   * @return the name of this property.
   */
  public final String getName()
  {
      return propName;
  }

  /**
   * Returns the current value of this property.
   * 
   * @return the current value of this property.
   */
  public final Object getValue()
  {
    return controller.getValue(propName);
  }

  /**
   * Sets the value of this property.
   * 
   * @param value
   *            the new value to set for this property.
   */
  public final void setValue(Object value)
  {
    controller.setValue(propName, value);
  }

  /**
   * Returns the current value of one of this node's properties.
   * 
   * @param name
   *            the name of the property to access.
   * @return the current value of the named property for this node.
   * @throws IllegalArgumentException
   *             if this node does not have a property of the given name.
   */
  public final Object getProperty(String name)
  {
    if ("name".equals(name)) {
      return getName();
//    } else if ("type".equals(name)) {
//      return getType();
    } else if ("value".equals(name)) {
      return getValue();
    } else {
      throw new IllegalArgumentException("Invalid property name '" + name + "'.");
    }
  }

  /**
   * Changes the value of one of this node's properties.
   * 
   * @param name
   *            the name of the property to change.
   * @param value
   *            the new value to set for the property.
   * @throws NoSuchElementException
   *             if this node does not have a property of the given name.
   */
  public final void setProperty(String name, Object value)
  {
    checkSetRequest(name, value);
    if ("value".equals(name)) {
      setValue(value);
    } else {
      throw new NoSuchElementException(name);
    }
  }

  @Override
  public final String toString()
  {
      String ownerName = "<unnamed>";
      try {
          ownerName = Fractal.getNameController(owner).getFcName();
      } catch (NoSuchInterfaceException nsie) {
          // Ignore.
      }
      return "#<scaproperty: " + ownerName + "." + getName() + ">";
  }
}
