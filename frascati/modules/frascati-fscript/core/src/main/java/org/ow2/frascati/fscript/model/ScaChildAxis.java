/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */
package org.ow2.frascati.fscript.model;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.ChildAxis;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.objectweb.fractal.util.Fractal;

/**
 * Implements the <code>scachild</code> axis in FPath. This axis connects composite
 * components to their direct subcomponents. The {@link #connect(Node, Node)} and
 * {@link #disconnect(Node, Node)} operations map to the addition and removal of
 * sub-components from composites.
 *
 * @author Christophe Demarey.
 */
public class ScaChildAxis
     extends ChildAxis
{
  /**
   * Default constructor.
   * 
   * @param model The model referencing this axis.
   */
  public ScaChildAxis(FraSCAtiModel model)
  {
    super(model, "scachild", "scacomponent", "scacomponent");
  }
  
  /**
   * Get nodes for this axis from the specified source node.
   * 
   * @param source The source node to introspect.
   * @return A set of nodes.
   */
  @Override
  public Set<Node> selectFrom(Node source)
  {
      Component parent = ((ComponentNode) source).getComponent();
      try {
          ContentController cc = Fractal.getContentController(parent);
          Set<Node> result = new HashSet<Node>();
          for (Component child : cc.getFcSubComponents()) {
            try {
              child.getFcInterface(ScaBindingAxis.SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
              ComponentNode node = ((NodeFactory) model).createScaComponentNode(child);
              result.add(node);
            } catch (NoSuchInterfaceException e) {
              /* The sca-component-controller cannot be found!
                 This component is not a SCA component => Nothing to do! */
            }
          }
          return result;
      } catch (NoSuchInterfaceException e) {
          // Not a composite, no children.
          return Collections.emptySet();
      }
  }
}
