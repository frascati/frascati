/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.procedures;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.bf.connectors.rest.RestConnectorConstants;
import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.interpreter.Context;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;
import org.objectweb.fractal.fscript.types.PrimitiveType;
import org.objectweb.fractal.fscript.types.Signature;
import org.objectweb.fractal.fscript.types.UnionType;
import org.objectweb.fractal.fscript.types.VoidType;

/**
 * Implements the <code>add-rest-binding()</code> function which adds
 * a RestFul binding to the SCA service/reference given as argument.
 * The second argument is the Rest URI.
 *
 * @author Christophe Demarey
 */
public class AddRestBinding
     extends AddBinding
{
    /**
     * Default constructor.
     */
    public AddRestBinding()
    {
    }

    /**
     * Returns the name of the procedure.
     * 
     * @return the name of the procedure.
     */
    public String getName()
    {
      return "add-rest-binding";
    }

    /**
     * Returns the signature of this procedure.
     * 
     * @return the signature of this procedure.
     */
    public Signature getSignature()
    {
      return new Signature( VoidType.VOID_TYPE, 
                            new UnionType( model.getNodeKind("scaservice"), model.getNodeKind("scareference") ),
                            PrimitiveType.STRING );
    }

    /**
     * Tests whether this procedure is a pure function (i.e. without side-effect), or an
     * action (with potential side-effects).
     * 
     * @return <code>true</code> if this procedure is a pure function.
     */
    public boolean isPureFunction()
    {
      return false;
    }

    /**
     * Parse arguments and call createBinding()
     */
    public Object apply(List<Object> args, Context ctx)
            throws ScriptExecutionError
    {
        Map<String, Object> hints = new HashMap<String, Object>();
        InterfaceNode itf = (InterfaceNode) args.get(0);
        String uri = (String) args.get(1);

        hints.put(PLUGIN_ID, "rest");
        hints.put(RestConnectorConstants.URI, uri);
        this.createBinding(itf.getName(), itf.getInterface().getFcItfOwner(), itf.isClient(), hints);

        return null;
    }

}
