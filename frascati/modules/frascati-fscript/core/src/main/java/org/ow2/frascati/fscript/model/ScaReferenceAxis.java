/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.model;

import java.util.HashSet;
import java.util.Set;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.objectweb.fractal.fscript.model.fractal.InterfaceAxis;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;

/**
 * Implements the <code>scareference</code> axis in FPath. This axis connects SCA reference
 * interfaces to the implementation component.
 * 
 * @author Christophe Demarey
 */
public class ScaReferenceAxis
     extends InterfaceAxis
{
  /**
   * Default constructor
   * 
   * @param model The model referencing this axis.
   */
  public ScaReferenceAxis(FraSCAtiModel model)
  {
    super(model, false, "scareference", "scacomponent", "scareference");
  }

  @Override
  public Set<Node> selectFrom(Node source)
  {
      Component comp = ((ComponentNode) source).getComponent();
      ContentController cc = null;
      Set<Node> result = new HashSet<Node>();
      Object[] rawItfs = comp.getFcInterfaces();
      
      addInterfaces(rawItfs, result);
      addPrototypeInterfaces(comp, cc, result);
      
      return result;
  }

  @Override
  protected void addInterfaces(Object[] rawItfs, Set<Node> result)
  {
      NodeFactory nf = (NodeFactory) model;
      for (Object rawItf : rawItfs) {
          Interface itf = (Interface) rawItf;
          if ( itf instanceof TinfiComponentOutInterface ) {
              result.add(nf.createScaReferenceNode(itf));
          }
      }
  }
}
