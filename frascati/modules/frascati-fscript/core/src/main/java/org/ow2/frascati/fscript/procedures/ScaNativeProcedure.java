/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.fscript.procedures;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fscript.procedures.NativeProcedure;

/**
 * An SCA native procedure is a fractal component that needs to be bound to the 
 * model component
 */
public interface ScaNativeProcedure
         extends NativeProcedure, BindingController
{
    /** The model client interface name. */
    String MODEL_ITF_NAME = "frascati-model";    
}
