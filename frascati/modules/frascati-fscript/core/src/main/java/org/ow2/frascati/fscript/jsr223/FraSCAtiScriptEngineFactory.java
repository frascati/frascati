/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.jsr223;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.script.Bindings;
import javax.script.ScriptEngine;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.objectweb.fractal.fscript.model.Node;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.ow2.frascati.fscript.model.NodeFactory;
import org.ow2.frascati.util.FrascatiException;

public class FraSCAtiScriptEngineFactory
     extends org.objectweb.fractal.fscript.jsr223.FScriptEngineFactory
{
	/** Script extensions managed by the engine */
	private static List<String> extensions = new ArrayList<String>() {{ add("fscript"); }};

    /** The logger */
    private static Logger log = Logger.getLogger("org.ow2.frascati.fscript.FraSCAtiFScript");

    /**
	 * Default constructor.
	 */
	public FraSCAtiScriptEngineFactory()
	{
		super();
	}
	
	public String getEngineName()
	{
		return "OW2 FraSCAti FScript";
	}

	public String getEngineVersion()
	{
		return "1.0";
	}

	public final String getLanguageName()
	{
		return "FScript";
	}

	public final String getLanguageVersion()
	{
		return "2.1";
	}	

	public final List<String> getMimeTypes()
	{
		return new ArrayList<String>() {{ add("application/fscript"); add("text/fscript"); }};
	}

	public final List<String> getNames()
	{
		return new ArrayList<String>() {{ add("fscript"); }};
	}

	public final ScriptEngine getScriptEngine()
	{
		ScriptEngine engine = null;
		FraSCAtiFScript singleton = FraSCAtiFScript.getSingleton();

		if (singleton != null) {
			engine = singleton.getScriptEngine();
		}

		// engine != null => The SCA reference is set, we will only use one engine!
		if (engine == null) {
			try {
				// Instantiate a new FraSCAti FScript engine AND a FraSCAti runtime
				if(System.getProperty("org.ow2.frascati.bootstrap") == null) {
 				  System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiFractal");
				}
				System.setProperty("org.ow2.frascati.class", FraSCAti.class.getCanonicalName());
				System.setProperty("org.ow2.frascati.composite", FraSCAti.class.getCanonicalName());
				
				FraSCAti frascati = FraSCAti.newFraSCAti();
			    Component comp = frascati.getComposite(FraSCAti.class.getCanonicalName());
			    // Retrieve the FraSCAti FScript engine
			    comp = ContentControllerHelper.getSubComponentByName(comp, "fscript");
			    comp = ContentControllerHelper.getSubComponentByName(comp, "fscript-engine");
			    engine = frascati.getService(comp, "engine", InvocableScriptEngine.class);
			} catch (FrascatiException e) {
				e.printStackTrace();
			}
		}
		
		return engine;
	}

    /**
	 * Update the FraSCAti SCript context by adding the FraSCAti domain.
	 */
	public final void addDomainToContext(Bindings ctx)
	{
		Component domain = FraSCAtiFScript.getSingleton().getCompositeManager().getTopLevelDomainComposite();
		updateContext(ctx, "domain", domain);
	}

	/**
	 * Update FraSCAti SCript context by setting the varName variable 
	 * with the given component.
	 * 
	 * @param varName The variable name to set.
	 * @param comp A fractal component that will be registered in the 'varName' variable.
	 */
	public void updateContext(Bindings ctx, String varName, Component comp)
	{
		NodeFactory nf = FraSCAtiFScript.getSingleton().getNodeFactory();
		Node rootNode = nf.createScaComponentNode(comp);
		ctx.put(varName, rootNode);
	}
}
