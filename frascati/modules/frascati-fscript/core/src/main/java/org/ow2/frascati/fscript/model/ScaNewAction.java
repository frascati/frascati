/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.model;

import static org.objectweb.fractal.fscript.diagnostics.Severity.ERROR;

import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.ScriptExecutionError;
import org.objectweb.fractal.fscript.diagnostics.Diagnostic;
import org.objectweb.fractal.fscript.interpreter.Context;
import org.objectweb.fractal.fscript.types.PrimitiveType;
import org.objectweb.fractal.fscript.types.Signature;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.fscript.procedures.ScaNativeProcedure;

/**
 * Implements the <code>sca-new()</code> action to instantiate SCA composites
 * using FraSCAti Assembly Factory.
 *
 * @author Christophe Demarey.
 */
public class ScaNewAction
  implements ScaNativeProcedure
{
  // --------------------------------------------------------------------------
  // Internal state
  // --------------------------------------------------------------------------
  /** The model client interface name. */
  public static final String MODEL_ITF_NAME = "frascati-model";
  
  /** The FraSCAti model component */
  private FraSCAtiModel model; // for the binding

  // --------------------------------------------------------------------------
  // Constructor
  // --------------------------------------------------------------------------
  /**
   * Default constructor.
   */
  public ScaNewAction()
  {
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Returns the name of the procedure.
   * 
   * @return the name of the procedure.
   */
  public final String getName()
  {
    return "sca-new";
  }

  /**
   * Returns the signature of this procedure.
   * 
   * @return the signature of this procedure.
   */
  public Signature getSignature()
  {
    return new Signature(model.getNodeKind("scacomponent"), PrimitiveType.STRING);
  }

  /**
   * Tests whether this procedure is a pure function (i.e. without side-effect), or an
   * action (with potential side-effects).
   * 
   * @return <code>true</code> if this procedure is a pure function.
   */
  public boolean isPureFunction()
  {
    return false;
  }

  /**
   * Get the SCA Domain allowing us to instanciate SCA composites.
   * 
   * @return The FraSCAti Domain.
   */
  private CompositeManager getDomain()
  {
    try {
        return (CompositeManager) model.lookupFc(FraSCAtiModel.COMPOSITE_MANAGER_ITF);
    } catch (NoSuchInterfaceException e) {
        throw new AssertionError("Invalid FractalModel component.");
    }
  }

  /**
   * Apply this procedure to the specified arguments.
   * 
   * @param args
   *            the arguments of the procedure call.
   * @param ctx
   *            the execution context in which to execute the procedure.
   * @return the value returned by the procedure.
   * @throws ScriptExecutionError
   *             if any error occurred during the execution of the procedure.
   */
  public Object apply(List<Object> args, Context ctx)
      throws ScriptExecutionError
  {
    String compositeName = (String) args.get(0);
    CompositeManager domain = getDomain();
    Object newComponent;
    try {
        newComponent = domain.getComposite(compositeName);
    } catch (ManagerException e) {
        Diagnostic err = new Diagnostic(ERROR, "Unable to instanciate composite " + compositeName);
        throw new ScriptExecutionError(err, e);
    }
    return model.createScaComponentNode((Component) newComponent);
  }

  public String[] listFc()
  {
      return new String[] { MODEL_ITF_NAME };
  }

  public void bindFc(String itfName, Object srvItf) throws NoSuchInterfaceException
  {
      if (MODEL_ITF_NAME.equals(itfName)) {
          this.model = (FraSCAtiModel) srvItf;
      } else {
          throw new NoSuchInterfaceException(itfName);
      }
  }

  public Object lookupFc(String itfName) throws NoSuchInterfaceException
  {
      if (MODEL_ITF_NAME.equals(itfName)) {
          return this.model;
      } else {
          throw new NoSuchInterfaceException(itfName);
      }
  }

  public void unbindFc(String itfName) throws NoSuchInterfaceException
  {
      if (MODEL_ITF_NAME.equals(itfName)) {
          this.model = null;
      } else {
          throw new NoSuchInterfaceException(itfName);
      }
  }
}
