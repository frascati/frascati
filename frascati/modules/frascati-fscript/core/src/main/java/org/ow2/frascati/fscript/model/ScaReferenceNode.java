/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.model;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.fscript.model.fractal.InterfaceNode;
import org.objectweb.fractal.util.Fractal;

/**
 * A {@link Node} which represents an SCA reference of a FraSCAti component.
 *
 * @author Christophe Demarey.
 */
public class ScaReferenceNode
     extends InterfaceNode
{
  /**
   * Creates a new SCA reference node.
   * 
   * @param model
   *            the FraSCAti model the node is part of.
   * @param itf
   *            the interface the new node will represent.
   */
  protected ScaReferenceNode(FraSCAtiModel model, Interface itf)
  {
    super(model, itf, "scareference");
  }

  @Override
  public final boolean isInternal()
  {
      return false;
  }

  @Override
  public final boolean isClient()
  {
      return true;
  }

  @Override
  public final String toString()
  {
      Component owner = itf.getFcItfOwner();
      String ownerName = "<unnamed>";
      try {
          ownerName = Fractal.getNameController(owner).getFcName();
      } catch (NoSuchInterfaceException nsie) {
          // Ignore.
      }
      return "#<scareference: " + ownerName + "." + getName() + ">";
  }
}
