/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.model;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.fscript.model.fractal.ComponentNode;
import org.ow2.frascati.tinfi.api.control.SCABasicIntentController;

/**
 * A {@link Node} which represents an SCA intent.
 * This node inherits from the component node as an SCA intent is 
 * implemented as an SCA component in FraSCAti.
 * 
 * @author Christophe Demarey
 */
public class ScaIntentNode
     extends ComponentNode
{
    /** The intent controller managing this intent. */
    private final SCABasicIntentController ic; // TODO: use the ic to disable intents ic.remove()
    /** The owner of this intent. */
    private final Interface owner;
    /** The name of this property. */
    private final String name;

    /**
     * Creates a new <code>ScaIntentNode</code>.
     * 
     * @param model
     *            the FraSCAti model this node is part of.
     * @param ic
     *            the intent controller managing this intent.
     * @param name
     *            the name of the intent.
     * @param impl
     *            the intent implementation (an SCA component).
     * @param itf
     *            this intent is attached to itf.
     */
    public ScaIntentNode( FraSCAtiModel model,
                          SCABasicIntentController ic,
                          String name,
                          Component impl,
                          Interface itf )
    {
        super(model, impl, "scaintent");
        this.ic = ic;
        this.owner = itf;
        this.name = name;
    }

    /**
     * Returns the name of this intent.
     * 
     * @return the name of this intent.
     */
    public final String getName()
    {
        return this.name;
    }
    
    /**
     * Get the attachment of this intent, i.e. the interface, 
     * the component on which the policy will be apply to.
     * 
     * @return the interface (service or reference) for which the
     * intent has been defined.
     */
    public final Interface getAttachment()
    {
        return owner;
    }

    /**
     * Returns the component implementing this intent.
     * 
     * @return the component implementing this intent.
     */
    public final Component getImplementation()
    {
        return getComponent();
    }

    @Override
    public final String toString()
    {
        return "#<scaintent: " + getName() + " on " + getAttachment().getFcItfName() + ">";
    }
}
