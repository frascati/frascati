/**
 * OW2 FraSCAti FScript
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: fractal@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.fscript.jsr223;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.fscript.jsr223.FScriptEngine;
import org.ow2.frascati.fscript.FraSCAtiFScript;
import org.ow2.frascati.fscript.model.NodeFactory;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;

/**
 * The FraSCAti FScript JSR223 Script Engine.
 * It overrides the put method in order to create FScript nodes when possible.
 */
public class FraSCAtiScriptEngine
     extends FScriptEngine
{
	@Override
	public void put(String key, Object value)
	{
		NodeFactory nf = FraSCAtiFScript.getSingleton().getNodeFactory();
		
		if (value instanceof Component) { // SCA component
			value = nf.createScaComponentNode((Component) value);
		} else if (value instanceof TinfiComponentOutInterface) { // SCA reference
			value = nf.createScaReferenceNode((Interface) value);
		} else if (value instanceof Interface) { // SCA service
			value = nf.createScaServiceNode((Interface) value);
		}
		super.put(key, value);
	}
}
