/***
 * OW2 FraSCAti fscript
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */
package org.ow2.frascati.fscript.model;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import org.objectweb.fractal.fscript.model.Node;

/**
 * JUnit test case for the scabinding axis. 
 *
 * @author Christophe Demarey.
 */
public class ScabindingAxisTest extends HelloWorldBasedTest {

    @Test
    public void testBindings() throws Exception {
                
        String[] requests = { "$root/scachild::client/scareference::printService/scabinding::*;",
                              "$root/scachild::server/scaservice::s/scabinding::*;" };
        
        for (String request : requests) {
            Set<Node> result = (Set<Node>) engine.eval(request);
            for (Node node : result) {
                assertTrue(node+" is not an instance of ScaBindingNode", node instanceof ScaBindingNode );
            }
            assertTrue("The following query expect only one binding as result!\n"
                       + request, result.size() == 1);
        }
    }

}
