/***
 * OW2 FraSCAti fscript
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s):
 */
package org.ow2.frascati.fscript;

import static org.junit.Assert.assertNotNull;

import javax.script.ScriptEngineManager;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.fscript.jsr223.FScriptEngineFactoryProxy;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;

public class FraSCAtiFScriptTest {
    
    @Test
    public void testGetFraSCAtiScriptComposite() throws Exception {
        
        System.setProperty(FScriptEngineFactoryProxy.SCRIPT_ENGINE_FACTORY_PROPERTY_NAME,
        "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory");
        
        ScriptEngineManager manager = new ScriptEngineManager();
        
        InvocableScriptEngine engine = (InvocableScriptEngine) manager.getEngineByExtension("fscript"); // A reference to the FScript engine
        Component fscriptEngineFractalComp = FraSCAtiFScript.getSingleton().getFraSCAtiScriptComposite();
        
        assertNotNull(fscriptEngineFractalComp);
    }

}
