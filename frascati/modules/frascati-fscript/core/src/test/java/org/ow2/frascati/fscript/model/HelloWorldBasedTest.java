/***
 * OW2 FraSCAti FScript
 * Copyright (C) 2009-2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 */
package org.ow2.frascati.fscript.model;

import javax.script.ScriptEngineManager;

import org.junit.Before;
import org.objectweb.fractal.fscript.jsr223.FScriptEngineFactoryProxy;
import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;

/**
 * Based class useful for tests running on top of the HelloWorld example. 
 *
 * @author Christophe Demarey.
 */
public abstract class HelloWorldBasedTest {
        
    /** A reference to the root composite. */
    ScaComponentNode helloWorldRoot = null;
    /** A reference to the FScript engine. */
    InvocableScriptEngine engine = null;

    @Before
    public void setUp() throws Exception
    {
        System.setProperty(FScriptEngineFactoryProxy.SCRIPT_ENGINE_FACTORY_PROPERTY_NAME, 
                "org.ow2.frascati.fscript.jsr223.FraSCAtiScriptEngineFactory");
        ScriptEngineManager manager = new ScriptEngineManager();
        
        engine = (InvocableScriptEngine) manager.getEngineByExtension("fscript");
        
        this.helloWorldRoot = (ScaComponentNode) engine.eval("root = sca-new(\"helloworld-rmi-standalone\");");
        engine.eval("client = $root/scachild::client;");
    }
    
    // TODO: Add a tearDown() method used to unload loaded SCA composite(s) 
}
