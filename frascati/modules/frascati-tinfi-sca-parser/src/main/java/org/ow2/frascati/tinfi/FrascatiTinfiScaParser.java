/**
 * OW2 FraSCAti: Tinfi SCA Parser
 * Copyright (C) 2010-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Lionel Seinturier
 *
 */

package org.ow2.frascati.tinfi;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Composite;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.core.ParsingContextImpl;
import org.ow2.frascati.tinfi.emf.EMFParserSupportImpl;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * OW2 FraSCAti Tinfi SCA Parser.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class FrascatiTinfiScaParser
     extends EMFParserSupportImpl
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Get the OW2 FraSCAti SCA composite parser.
   */
  private Parser<Composite> getCompositeParser() throws Exception
  {
    // Create a factory of SCA parsers.
    org.ow2.frascati.parser.Parser parserFactory = new org.ow2.frascati.parser.Parser();
    // Create a parser component.
    Component parser = parserFactory.newFcInstance();
    // Start the parser component.
    Fractal.getLifeCycleController(parser).startFc();
    // Get the composite parser interface.
    return (Parser<Composite>)parser.getFcInterface("composite-parser");
  }

  /**
   * New OW2 FraSCAti SCA parsing context.
   */
  private ParsingContext newParsingContext()
  {
    // Obtain the Juliac class loader.
    URLClassLoader juliacClassLoader = (URLClassLoader)this.jc.getJuliacConfig().getClassLoader();
    // Obtain all URLs of the Juliac class loader.
    List<URL> juliacClassLoaderUrls = Arrays.asList(juliacClassLoader.getURLs());
    // Obtain the parent of the Juliac class loader.
    ClassLoader parentClassLoader = juliacClassLoader.getParent();
    // Obtain all the URLs of all the parents of the Juliac class loader.
    List<URL> parentClassLoaderUrls = new ArrayList<URL>();

    // Iterate over the chain of class loaders.
    {
      ClassLoader classLoader = parentClassLoader;
      while(classLoader != null) {
        if(classLoader instanceof URLClassLoader) {
          // store URLs of each URL class loader.
          parentClassLoaderUrls.addAll(Arrays.asList(((URLClassLoader)classLoader).getURLs()));
        }
        classLoader = classLoader.getParent();
      }
    }

    // Compute juliacClassLoaderUrls - parentClassLoaderUrls, i.e., all the urls of
    // juliacClassLoaderUrls that are not present in parentClassLoaderUrls.
    List<URL> urls = new ArrayList<URL>();
    boolean frascatiScaParserJarNotAdded = true;
    for(URL url : juliacClassLoaderUrls) {
      String urlAsString = url.toString();
      if(!parentClassLoaderUrls.contains(url)
        // Exclude src/main/resources/ as resource files were copied into target/classes/ before executing this plugin.
        && !urlAsString.endsWith("src/main/resources/")
      ) {
      	// Exclude frascati-sca-parser-<version>.jar because sometimes this jar is present twice into the Juliac class loader with different urls.
      	if(urlAsString.contains("frascati-sca-parser-") && urlAsString.endsWith(".jar")) {
      	  if(frascatiScaParserJarNotAdded) {
            urls.add(url);
            frascatiScaParserJarNotAdded = false;
      	  }
      	} else {
          urls.add(url);
        }
      }
	}

    System.out.println("Parent class loader:");
    for(URL url : parentClassLoaderUrls) {
      System.out.println("* " + url);
    }
    System.out.println("FraSCAti class loader:");
    for(URL url : urls) {
      System.out.println("* " + url);
    }

    // Create a new class loader with urls and parentClassLoader.
    ClassLoader classLoader = new FrascatiClassLoader(urls.toArray(new URL[urls.size()]), parentClassLoader);

    // Create a new parsing context using the new class loader.
    return new ParsingContextImpl(classLoader);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see EMFParserSupportImpl#parse(String)
   */
  @Override
  protected Composite innerparse( String adl, Map<Object,Object> context ) throws IOException
  {
    try {
      Parser<Composite> compositeParser = getCompositeParser();
      ParsingContext parsingContext = newParsingContext();
      QName qname = new QName(adl.replace('.','/'));
      return compositeParser.parse(qname, parsingContext);
    } catch (Exception e) {
      throw new IOException("Can not parse " + adl, e);
    }
  }

}
