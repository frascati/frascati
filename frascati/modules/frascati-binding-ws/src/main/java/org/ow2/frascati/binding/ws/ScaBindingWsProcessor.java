/***
 * OW2 FraSCAti SCA Binding Web Service
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Valerio Schiavoni, Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.binding.ws;

import java.util.Map;

import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.WebServiceBinding;

import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;

import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;

/**
 * Bind components using Web Service Binding.
 * 
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.1
 */
public class ScaBindingWsProcessor
     extends AbstractBindingFactoryProcessor<WebServiceBinding>
{
  // --------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(WebServiceBinding wsBinding, StringBuilder sb) {
    sb.append("sca:binding.ws");
    append(sb, "wsdlElement", wsBinding.getWsdlElement());
    append(sb, "wsdlLocation", wsBinding.getWsdlLocation());
    super.toStringBuilder(wsBinding, sb);
  }

  /**
   * @see AbstractBindingFactoryProcessor#getBindingFactoryPluginId()
   */
  @Override
  protected final String getBindingFactoryPluginId() {
    return "ws";
  }

  /**
   * @see AbstractBindingFactoryProcessor#initializeBindingHints(EObjectType, Map)
   */
  @Override
  @SuppressWarnings("static-access")
  protected final void initializeBindingHints(WebServiceBinding wsBinding, Map<String, Object> hints) {
    // set protocol specific parameter
    hints.put(WsConnectorConstants.URI, completeBindingURI(wsBinding));
    hints.put(WsConnectorConstants.WSDL_ELEMENT, wsBinding.getWsdlElement());
    hints.put(WsConnectorConstants.WSDL_LOCATION, wsBinding.getWsdlLocation());
  }

  // --------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(ScaPackage.Literals.WEB_SERVICE_BINDING);
  }

}
