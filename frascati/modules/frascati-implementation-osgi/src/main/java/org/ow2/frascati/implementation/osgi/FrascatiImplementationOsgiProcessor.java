/**
 * OW2 FraSCAti: SCA OSGi Implementation
 * Copyright (C) 2009-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.implementation.osgi;

import org.eclipse.stp.sca.domainmodel.frascati.FrascatiPackage;
import org.eclipse.stp.sca.domainmodel.frascati.OsgiImplementation;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.component.factory.api.FactoryException;

/**
 * OW2 FraSCAti SCA Implementation OSGi processor class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class FrascatiImplementationOsgiProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<OsgiImplementation> {

  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(OsgiImplementation osgiImplementation, StringBuilder sb) {
    sb.append("frascati:implementation.osgi");
    append(sb, "bundle", osgiImplementation.getBundle());
    super.toStringBuilder(osgiImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(OsgiImplementation osgiImplementation, ProcessingContext processingContext)
	      throws ProcessorException
  {
    String osgiImplementationBundle = osgiImplementation.getBundle();
    if(isNullOrEmpty(osgiImplementationBundle)) {
      error(processingContext, osgiImplementation, "The attribute 'bundle' must be set");
    } else {
      // TODO check that bundles are available?
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(osgiImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(OsgiImplementation osgiImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    try {
      getComponentFactory().generateMembrane(processingContext, getFractalComponentType(osgiImplementation, processingContext),
  	      "osgiPrimitive", osgiImplementation.getBundle());
    } catch (FactoryException te) {
  	  severe(new ProcessorException(osgiImplementation, "Error while creating OSGI component instance", te));
  	}
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(OsgiImplementation osgiImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // get instance of an OSGI primitive component
    Component component;
    try {
      component = getComponentFactory().createComponent(processingContext, getFractalComponentType(osgiImplementation, processingContext),
    	            "osgiPrimitive", osgiImplementation.getBundle());
    } catch (FactoryException te) {
      severe(new ProcessorException(osgiImplementation, "Error while creating OSGI component instance", te));
      return;
    }

    // Store the created component into the processing context.
    processingContext.putData(osgiImplementation, Component.class, component);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
   */
  public final String getProcessorID() {
    return getID(FrascatiPackage.Literals.OSGI_IMPLEMENTATION);
  }

}
