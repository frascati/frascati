/***
 * OW2 FraSCAti Introspection
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Christophe Demarey
 */
package org.ow2.frascati.remote.introspection;

import java.util.Collection;
import javax.script.ScriptException;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.remote.introspection.resources.Node;

/**
 * This interface is used to reconfigure / introspect SCA applications remotely with the help of FraSCAti FScript.
 */
@Service
public interface Reconfiguration {
	/**
	 * Execute a FraSCAti Script statement.
	 */
	@GET
    @Path("/eval/{query:.*}")
    @Produces({"application/xml", "application/json"})
	public Collection<Node> eval(@PathParam("query") String script) throws ScriptException;

	/**
	 * Load procedures into the FraSCAti Script engine.
	 */
	@POST
    @Path("/register/")
	public String register(@FormParam("script") String script) throws ScriptException;	
}
