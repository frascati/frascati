/**
 * OW2 FraSCAti Introspection
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributors: Philippe Merle
 */
package org.ow2.frascati.remote.introspection;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.script.ScriptException;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.remote.introspection.resources.Node;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.util.FrascatiException;

public class IntrospectionTest {
	/** REST binding URI */
	private static final String BINDING_URI = AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;
	/** The FraSCAti platform */
	private static FraSCAti frascati;

	/** The SCA composite to test */
	private static org.objectweb.fractal.api.Component scaComposite;

	private static Reconfiguration reconfig;
    private static Deployment deployment;

	/**
	 * Instantiate FraSCAti and retrieve services.
	 * @throws InterruptedException 
	 */
	@BeforeClass
	public static void loadFrascati() throws FrascatiException, InterruptedException {
		System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTFractalRest");
		frascati = FraSCAti.newFraSCAti();
		scaComposite = frascati.getComposite("org.ow2.frascati.FraSCAti");
		
		// Get services by using the Rest binding
		reconfig = JAXRSClientFactory.create(BINDING_URI+"/reconfig", Reconfiguration.class);
		deployment = JAXRSClientFactory.create(BINDING_URI+"/deploy", Deployment.class);
		
        // Wait for 1 second to be sure that the FScript engine is initialized.
		Thread.sleep(1000);
	}

	@Test
	public final void testFScriptStatementReturningNode() throws ScriptException {
		System.out.println("** Running FraSCAti FScript query: $domain/scadescendant::assembly-factory ...");
		Collection<Node> result = reconfig.eval("$domain/scadescendant::assembly-factory;");
		System.out.println(result);
	}

	@Test
	public final void testFScriptStatementReturningNodes() throws ScriptException {
		System.out.println("** Running FraSCAti FScript query: $domain/scadescendant::component-factory/scaservice::*; ...");
		Collection<Node> result = reconfig.eval("$domain/scadescendant::component-factory/scaservice::*;");
		System.out.println(result);
	}

	@Test
	public final void testFScriptReconfigurationAction() throws ScriptException {
	    Collection<Node> result;
		
		System.out.println("** Trying to stop the /org.ow2.frascati.FraSCAti/sca-parser component ...");
		
		System.out.println("   Before reconfiguration:");
		result = reconfig.eval("$domain/scadescendant::sca-parser;");
		System.out.println(result);
		
		reconfig.eval("set-state($domain/scadescendant::sca-parser, \"STOPPED\");");
		
		System.out.println("   After reconfiguration:");
		result = reconfig.eval("$domain/scadescendant::sca-parser;");
        System.out.println(result);

		reconfig.eval("set-state($domain/scadescendant::sca-parser, \"STARTED\");");
	}

	@Test
	public final void testIncrementalDeployment() {

		String contrib = null;
	    
		//First deployment
        try {
            contrib = FileUtil.getStringFromFile(new File("src/test/resources/helloworld1.zip"));
        } catch (IOException e) {
            System.err.println("Cannot read the contribution!");
            e.printStackTrace();
            fail();
        }
        
        System.out.println("** Trying to deploy 1st contribution ...");
        deployment.deployContribution(contrib);
        System.out.println("** Contribution deployed!");
        
        //Second deployment
        try {
            contrib = FileUtil.getStringFromFile(new File("src/test/resources/helloworld2.zip"));
        } catch (IOException e) {
            System.err.println("Cannot read the contribution!");
            e.printStackTrace();
            fail();
        }
        
        System.out.println("** Trying to deploy 2nd contribution ...");
        deployment.deployContribution(contrib);
        System.out.println("** Contribution deployed!");
        
        //Reconfiguration
    	File file = new File( "src/test/resources/reconfigFScript.txt");

		try {
			reconfig.register(new String( FileUtil.getBytesFromFile(file) ));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		System.out.println("** Trying to reconfigure application ...");
		try{
			Collection<Node> result = reconfig.eval("update()");
		} catch (ScriptException e){
			e.printStackTrace();
			fail();
		}
		System.out.println("** Application reconfigured!");
		
		// undeploy composites contained by contributions.
		deployment.undeployComposite("helloworld");
	}

	@Test
	public final void testIncrementalDeployment1()
	{		    
	    // First deployment
		String contrib = null;
        try {
            contrib = FileUtil.getStringFromFile(new File("src/test/resources/counter-contribution.zip"));
	    } catch (IOException e) {
	        System.err.println("Cannot read the contribution!");
	        e.printStackTrace();
	        fail();
	    }

        System.out.println("** Trying to deploy 1st contribution ...");
        deployment.deployContribution(contrib);
        System.out.println("** Contribution deployed!");
	}

	@Test
	public final void testIncrementalDeployment2()
	{
		// Second deployment
		String contrib = null;
        try {
	        contrib = FileUtil.getStringFromFile(new File("src/test/resources/helloworld1.zip"));
        } catch (IOException e) {
            System.err.println("Cannot read the contribution!");
            e.printStackTrace();
            fail();
        }

        System.out.println("** Trying to deploy 2nd contribution ...");
        deployment.deployContribution(contrib);
        System.out.println("** Contribution deployed!");
	}

	@Test
	public final void testIncrementalDeployment3()
	{
		// Thrid deployment
		String contrib = null;
        try {
            contrib = FileUtil.getStringFromFile(new File("src/test/resources/helloworld2.zip"));
        } catch (IOException e) {
            System.err.println("Cannot read the contribution!");
            e.printStackTrace();
            fail();
        }

        System.out.println("** Trying to deploy 3st contribution ...");
        deployment.deployContribution(contrib);
        System.out.println("** Contribution deployed!");
	}
}
