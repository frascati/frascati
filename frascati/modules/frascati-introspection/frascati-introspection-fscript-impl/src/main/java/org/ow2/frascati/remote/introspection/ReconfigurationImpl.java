/**
 * OW2 FraSCAti Introspection
 * Copyright (C) 2008-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.remote.introspection;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Logger;

import javax.script.ScriptException;

import org.objectweb.fractal.fscript.jsr223.InvocableScriptEngine;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.fscript.model.ScaComponentNode;
import org.ow2.frascati.fscript.model.ScaReferenceNode;
import org.ow2.frascati.fscript.model.ScaServiceNode;
import org.ow2.frascati.remote.introspection.resources.Node;
import org.ow2.frascati.remote.introspection.util.ResourceUtilItf;

/**
 * This SCA component is used to enable remote introspection and reconfiguration
 * with FScript.
 */
@Scope("COMPOSITE")
public class ReconfigurationImpl implements Reconfiguration
{
    /** A reference to the FraSCAti FScript engine */
    @Reference(name = "fscript-engine")
    protected InvocableScriptEngine engine;
    
    /** A reference to the remote ResourceUtil service */
    @Reference(name = "resourceUtil")
    protected ResourceUtilItf resourceUtil;
    
    /** The logger */
    public final static Logger log = Logger.getLogger(ReconfigurationImpl.class.getCanonicalName());

    /** Default constructor. */
    public ReconfigurationImpl()
    {
    }

    // Private methods

    /**
     * Map FraSCAti Script nodes to serializable resources.
     * 
     * @param obj The result of an FScript evaluation : nothing, a node or a
     *        list of nodes
     * @return A serializable resource if the type is known, else the parameter
     *         itself.
     */
    protected Node getResourceFor(Object obj)
    {
        if (obj instanceof ScaComponentNode)
        {
            return resourceUtil.getFullComponentResource(((ScaComponentNode) obj).getComponent());
        }
        if (obj instanceof ScaServiceNode)
        {
            return resourceUtil.getPortResource(((ScaServiceNode) obj).getInterface());
        }
        if (obj instanceof ScaReferenceNode)
        {
            return resourceUtil.getPortResource(((ScaReferenceNode) obj).getInterface());
        }

        Node node = new Node();
        if (obj != null)
        {
            node.setName(obj.toString());
        }
        return node;
    }

    // Public Methods

    // Implementation of the Reconfiguration interface

    /**
     * Evaluate a FraSCAti FScript statement.
     * 
     * @param script A FraSCAti FScript statement.
     * @return a serializable resource, else an FScript node (or nodes list).
     */
    public Collection<Node> eval(String script) throws ScriptException
    {
        Object evalResults = engine.eval(script);
        Collection<Node> results = new ArrayList<Node>();

        if (evalResults instanceof Collection)
        {
            Collection<?> objects = (Collection<?>) evalResults;

            for (Object obj : objects)
            {
                Node resource = getResourceFor(obj);
                results.add(resource);
            }
        } else
        {
            Node resource = getResourceFor(evalResults);
            results.add(resource);
        }
        return results;
    }

    /**
     * Register a new script into the FraSCAti FScript engine.
     */
    public String register(String script) throws ScriptException
    {
        Reader reader = new StringReader(script.trim());

        @SuppressWarnings("unchecked")
        Set<String> loadedProc = (Set<String>) engine.eval(reader);
        return loadedProc.toString();
    }

}
