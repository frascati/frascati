19/03/2013
Refactoring of the FraSCAti Introspection module
- remove hard coded binding search, remove all static access to ResourceUtil
+ SCA architecture, uniform way to bind and unbind component, doc and comments, improve tests