/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;

import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCAImplementation;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

/**
 *
 */
public class SCAModelUtilImpl implements SCAModelUtilItf
{
    @org.osoa.sca.annotations.Reference(name = "compositeManager")
    protected CompositeManager compositeManager;
    
    @org.osoa.sca.annotations.Reference(name="fractalUtil")
    protected FractalUtilItf fractalUtil;
    
    /** The logger */
    public final Logger logger = Logger.getLogger(SCAModelUtilImpl.class.getName());

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getTopLevelCompositeName(java.lang.String)
     */
    public String getTopLevelCompositeName(String path)
    {
        logger.fine("getTopLevelCompositeName " + path);
        if (path == null)
        {
            return null;
        }

        String toSplitPath = path;
        if (path.startsWith("/"))
        {
            toSplitPath = path.substring(1, path.length());
        }

        String[] pathSplitted = toSplitPath.split("/");
        logger.fine("TopLevelCompositeName : " + pathSplitted[0]);
        return pathSplitted[0];
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getProcessingContext(java.lang.String)
     */
    public ProcessingContext getProcessingContext(String path)
    {
        String topLevelCompositeName = this.getTopLevelCompositeName(path);
        ProcessingContext processingContext = compositeManager.getProcessingContext(topLevelCompositeName);
        return processingContext;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getTopLevelComposite(java.lang.String)
     */
    public Composite getTopLevelComposite(String path)
    {
        logger.fine("getTopLevelComposite " + path);
        String topLevelCompositeName=this.getTopLevelCompositeName(path);
        if(topLevelCompositeName==null)
        {
            logger.warning("getTopLevelComposite, no topLevelCompositeName found for path :" + path);
            return null;
        }
        
        ProcessingContext processingContext = this.getProcessingContext(path);
        if(processingContext==null)
        {
            logger.warning("getTopLevelComposite, no ProcessingContext found for composite :" + topLevelCompositeName);
            return null;
        }
        
        Composite composite=processingContext.getProcessedComposite(topLevelCompositeName);
        if(composite==null)
        {
            StringBuilder logSeverBuilder=new StringBuilder("Can't find SCA composite name ");
            logSeverBuilder.append(topLevelCompositeName);
            logSeverBuilder.append(", the processingContext contains : ");
            for(Composite processedComposite : processingContext.getProcessedComposite())
            {
                logSeverBuilder.append(processedComposite.getName()+" ");
            }
            logger.warning(logSeverBuilder.toString());
            return null;
        }
        
        return composite;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findReference(java.util.List, java.lang.String)
     */
    public Reference findReference(List<Reference> references, String referenceName)
    {
        for (Reference tmpReference : references)
        {
            if (tmpReference.getName().equals(referenceName))
            {
                return tmpReference;
            }
        }
        return null;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findReference(org.eclipse.stp.sca.Composite, java.lang.String)
     */
    public Reference findReference(Composite composite, String referenceName)
    {
        return findReference(composite.getReference(), referenceName);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findComponent(java.util.List, java.lang.String)
     */
    public Component findComponent(List<Component> components, String componentName)

    {
        for (Component tmpComponent : components)
        {
            if (tmpComponent.getName().equals(componentName))
            {
                return tmpComponent;
            }
        }
        return null;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findComponent(org.eclipse.stp.sca.Composite, java.lang.String)
     */
    public Component findComponent(Composite composite, String componentName)
    {
        return findComponent(composite.getComponent(), componentName);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findComponentReference(java.util.List, java.lang.String)
     */
    public ComponentReference findComponentReference(List<ComponentReference> componentReferences, String referenceName)
    {
        for (ComponentReference tmpComponentReference : componentReferences)
        {
            if (tmpComponentReference.getName().equals(referenceName))
            {
                return tmpComponentReference;
            }
        }
        return null;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#findComponentReference(org.eclipse.stp.sca.Component, java.lang.String)
     */
    public ComponentReference findComponentReference(Component component, String referenceName)
    {
        return findComponentReference(component.getReference(), referenceName);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getComposite(java.lang.String)
     */
    public Composite getComposite(String path)
    {
        logger.fine("getLastComposite "+path);
        ProcessingContext processingContext=this.getProcessingContext(path);
        Composite currentComposite=this.getTopLevelComposite(path);
        if(processingContext==null || currentComposite==null)
        {
            logger.warning("getLastComposite Can't find top level Composite for path : "+path);
            return null;
        }
        
        StringTokenizer pathTokenizer = new StringTokenizer(path, "/");
        /**skip the name of the top level composite*/
        pathTokenizer.nextToken();
        String nextToken;
        while (pathTokenizer.hasMoreTokens())
        {
            nextToken = pathTokenizer.nextToken();
            /** we look for the next subComponent */
            Component subComponent = findComponent(currentComposite, nextToken);
            if (subComponent == null || !(subComponent.getImplementation() instanceof SCAImplementation))
            {
                logger.warning("getLastComposite can't find Composite named "+nextToken+" in the composite "+currentComposite.getName());
                return null;
            }
            currentComposite = processingContext.getData(subComponent.getImplementation(), Composite.class);
        }
        return currentComposite;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getLastComposite(java.lang.String)
     */
    public Composite getLastComposite(String path)
    {
        logger.fine("getLastComposite "+path);
        ProcessingContext processingContext=this.getProcessingContext(path);
        Composite currentComposite=this.getTopLevelComposite(path);
        if(processingContext==null || currentComposite==null)
        {
            logger.warning("getLastComposite Can't find top level Composite for path : "+path);
            return null;
        }
        
        StringTokenizer pathTokenizer = new StringTokenizer(path, "/");
        /**skip the name of the top level composite*/
        pathTokenizer.nextToken();
        String nextToken;
        while (pathTokenizer.hasMoreTokens())
        {
            nextToken = pathTokenizer.nextToken();
            /** we look for the next subComponent */
            Component subComponent = findComponent(currentComposite, nextToken);
            if (subComponent == null || !(subComponent.getImplementation() instanceof SCAImplementation))
            {
                return currentComposite;
            }
            currentComposite = processingContext.getData(subComponent.getImplementation(), Composite.class);
        }
        return currentComposite;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getComponent(java.lang.String)
     */
    public Component getComponent(String path)
    {
        Component lastComponent=this.getLastComponent(path);
        if(lastComponent==null)
        {
            logger.fine("Can't find component in the path " + path);
            return null;
        }
        
        String nextTokenInThePath = nextTokenInThePath(path, lastComponent.getName());
        if(nextTokenInThePath!=null)
        {
            logger.fine("Can't find component for path " + path);
            return null;
        }
        
        return lastComponent;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.SCAModelUtilItf#getLastComponent(java.lang.String)
     */
    public Component getLastComponent(String path)
    {
        Composite parentComposite = getLastComposite(path);
        if (parentComposite == null)
        {
            logger.fine("Can't find composite in the path " + path);
            return null;
        }

        String componentName = nextTokenInThePath(path, parentComposite.getName());
        if (componentName == null)
        {
            logger.fine("Path : " + path + " do not contain component");
            return null;
        }
        Component component = findComponent(parentComposite.getComponent(), componentName);
        if (component == null)
        {
            logger.fine("Can't find component " + componentName + " in composite " + parentComposite.getName());
            return null;
        }
        return component;
    }
    
    private String nextTokenInThePath(String path, String fromToken)
    {
        int indexOfLastToken = path.lastIndexOf(fromToken);
        if (indexOfLastToken == -1)
        {
            logger.severe("[nextTokenInThePath] : " + fromToken + " can not be found in " + path);
            return null;
        }
        int startIndexOfNextToken = path.indexOf("/", indexOfLastToken);
        if (startIndexOfNextToken == -1)
        {
            logger.severe("[nextTokenInThePath] " + fromToken + " is the last token of the path: " + path);
            return null;
        }

        startIndexOfNextToken++;
        int endIndexOfNextToken = path.indexOf("/", startIndexOfNextToken);
        if (endIndexOfNextToken == -1)
        {
            /* next token is the last token */
            return path.substring(startIndexOfNextToken);
        } else
        {
            return path.substring(startIndexOfNextToken, endIndexOfNextToken);
        }
    }

    
}
