/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Author: Christophe Demarey
 * 
 * Contributor(s) : Gwenael Cattez.
 */
package org.ow2.frascati.remote.introspection.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.apache.cxf.common.util.Base64Exception;
import org.apache.cxf.common.util.Base64Utility;

/**
 * Utility class providing convenient ways to handle Files.
 */
public class FileUtil
{
    private static final Logger logger=Logger.getLogger(FileUtil.class.getName());

    public final static String JAR_ENTRIES_SEPARATOR="%S%";
    
    /**
     * Reads the contents of a file into a byte array.
     * 
     * @param file - the file to read, must not be null
     * @return the file contents
     * @throws IOException - in case of an I/O error
     */
    public static byte[] getBytesFromFile(File file) throws IOException
    {
        InputStream inputStream = new FileInputStream(file);
        
        try
        {
         // Get the size of the file
            long length = file.length();
            
            if (length > Integer.MAX_VALUE)
            {
                throw new IOException("The file you are trying to read is too large, length :"+length+", length max : "+Integer.MAX_VALUE);
            }

            // Create the byte array to hold the data
            byte[] bytes = new byte[(int) length];
            int offset = 0;
            int numRead = inputStream.read(bytes, offset, bytes.length - offset);
            while (offset < bytes.length && numRead >= 0)
            {
                offset += numRead;
                numRead = inputStream.read(bytes, offset, bytes.length - offset);
            }
            
            // Ensure all the bytes have been read in
            if (offset < bytes.length)
            {
                throw new IOException("Could not completely read file " + file.getName());
            }
            
            return bytes;
        }
        finally
        {
         // Close the input stream and return bytes
            inputStream.close();
        }
    }

    /**
     * Reads the contents of a file into a Base64 encoded String.
     * 
     * @param file - the file to read, must not be null
     * @return the file contents as a Base64 encoded String
     * @throws IOException - in case of an I/O error
     */
    public static String getStringFromFile(File file) throws IOException
    {
        return Base64Utility.encode(getBytesFromFile(file));
    }

    /**
     * decode a Base64 encoded file and copy it to a temporary file
     * 
     * @param encodedFile the base64 encoded file
     * @param extension the extension of the created temporary file
     * @return
     * @throws Base64Exception
     * @throws IOException
     */
    public static File decodeFile(String encodedFile, String extension) throws Base64Exception, IOException
    {
        File destFile = null;
        BufferedOutputStream bos = null;
        byte[] content = Base64Utility.decode(encodedFile);

        String tempFileName = "";
        if (extension != null && !"".equals(extension))
        {
            tempFileName = "." + extension;
        }

        destFile = File.createTempFile("deploy", tempFileName);
        
        try
        {
            bos = new BufferedOutputStream(new FileOutputStream(destFile));
            bos.write(content);
            bos.flush();
        }
        finally
        {
            bos.close();
        }

        return destFile;
    }

    /**
     * Unzip a zip file to a directory with the same name
     * 
     * @param toUnzip the file to extract
     * @return the directory created to extract
     * @throws IOException
     * @throws ZipException
     */
    public static File unZipHere(File toUnzip) throws ZipException, IOException
    {
        String destFilePath = toUnzip.getAbsolutePath();
        String destDirPath = destFilePath.substring(0, destFilePath.lastIndexOf('.'));
        File destDir = new File(destDirPath);
        boolean isDirMade=destDir.mkdirs();
        if(isDirMade)
        {
            logger.fine("build directory for file "+destDirPath);
        }
        
        ZipFile zfile = new ZipFile(toUnzip);

        Enumeration<? extends ZipEntry> entries = zfile.entries();
        while (entries.hasMoreElements())
        {
            ZipEntry entry = entries.nextElement();
            File file = new File(destDir, entry.getName());
            
            
            if (entry.isDirectory())
            {
                isDirMade=file.mkdirs();
                if(isDirMade)
                {
                    logger.fine("build directory for zip entry "+entry.getName());
                }
                
            } else
            {
                isDirMade=file.getParentFile().mkdirs();
                if(isDirMade)
                {
                    logger.fine("build parent directory for zip entry "+entry.getName());
                }
                InputStream inputStream;
                inputStream = zfile.getInputStream(entry);
                try
                {
                    copy(inputStream, file);
                }finally
                {
                    inputStream.close();
                }
            }
        }
        return destDir;
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) throws IOException
    {
        byte[] buffer = new byte[1024];
        while (true)
        {
            int readCount = inputStream.read(buffer);
            if (readCount < 0)
            {
                break;
            }
            outputStream.write(buffer, 0, readCount);
        }
    }

    public static void copy(File file, OutputStream out) throws IOException
    {
        InputStream inputStream = new FileInputStream(file);
        try
        {
            copy(inputStream, out);
        } finally
        {
            inputStream.close();
        }
    }

    public static void copy(InputStream inputStream, File file) throws IOException
    {
        OutputStream out = new FileOutputStream(file);
        try
        {
            copy(inputStream, out);
        } finally
        {
            out.close();
        }
    }

    public static boolean deleteDirectory(File path)
    {
        boolean isDeleted;
        if (path.exists())
        {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++)
            {
                if (files[i].isDirectory())
                {
                    deleteDirectory(files[i]);
                } else
                {
                    isDeleted=files[i].delete();
                    if(isDeleted)
                    {
                        logger.fine("delete file "+files[i].getPath());
                    }
                }
            }
        }
        return (path.delete());
    }
    
    public static File copyToTempFile(InputStream toCopyStream, String tempPrefix, String tempSuffix)
    {
        try
        {
            File tempFile = File.createTempFile(tempPrefix, tempSuffix);
            OutputStream destinationStream = new FileOutputStream(tempFile);
            copy(toCopyStream, destinationStream);
            destinationStream.close();
            return tempFile;
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }
    
    private static final String TMP_PREFIX="frascati_introspection_prefix";
    private static final String TMP_SUFFIX="frascati_introspection_suffix";
    
    public static File copyToTempFile(InputStream toCopyStream)
    {
        return copyToTempFile(toCopyStream, TMP_PREFIX,TMP_SUFFIX);
    }
}
