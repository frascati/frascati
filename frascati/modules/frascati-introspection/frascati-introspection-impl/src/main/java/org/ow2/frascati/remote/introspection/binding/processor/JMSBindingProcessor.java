/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.remote.introspection.binding.processor;

import java.util.HashMap;
import java.util.Map;

import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.bf.connectors.jms.JmsConnectorConstants;
import org.objectweb.fractal.bf.connectors.jms.JmsSkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.jms.JmsStubContentAttributes;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 * JMS BindingProcessor
 */
@Scope("COMPOSITE")
public class JMSBindingProcessor extends AbstractBindingProcessor
{

    public JMSBindingProcessor()
    {
        super(BindingKind.JMS,"-jms-skeleton","-jms-stub",JmsSkeletonContentAttributes.class, JmsStubContentAttributes.class);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.hints.HintsBindingProcessorItf#extractHints(javax.ws.rs.core.MultivaluedMap)
     */
    public Map<String, Object> extractHints(Map<String, String> params)
    {
        Map<String, Object> hints = new HashMap<String, Object>();
        
        String jndiICF = params.get("jndiInitialContextFactory");
        if (jndiICF != null)
        {
            hints.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, jndiICF);
        }

        String jndiURL = params.get("jndiInitialContextFactory");
        if (jndiURL != null)
        {
            hints.put(JmsConnectorConstants.JNDI_URL, jndiURL);
        }

        String jndiDestinationName = params.get("jndiDestinationName");
        if (jndiDestinationName != null)
        {
            hints.put(JmsConnectorConstants.JNDI_DESTINATION_NAME, jndiDestinationName);
        }

        String jndiDestinationType = params.get("destinationType");
        if (jndiDestinationType != null)
        {
            hints.put(JmsConnectorConstants.JNDI_INITCONTEXTFACT, jndiDestinationType);
        }

        return hints;
    }
}
