/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.remote.introspection.binding.processor;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.bf.connectors.ws.WsConnectorConstants;
import org.objectweb.fractal.bf.connectors.ws.WsSkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.ws.WsStubContentAttributes;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 * WS BindingProcessor
 */
public class WSBindingProcessor extends AbstractBindingProcessor
{

    /**
     * @param bindingKind
     */
    public WSBindingProcessor()
    {
        super(BindingKind.WS,"-ws-skeleton","-ws-stub",WsSkeletonContentAttributes.class,WsStubContentAttributes.class);
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.AbstractBindingProcessor#extractHints(java.util.Map)
     */
    protected Map<String, Object> extractHints(Map<String, String> params)
    {
        Map<String, Object> hints = new HashMap<String, Object>();

        String wsdlLocation = params.get("wsdlLocation");
        if (wsdlLocation != null)
        {
            hints.put(WsConnectorConstants.WSDL_LOCATION, wsdlLocation);
        }

        String wsdlElement = params.get("wsdlElement");
        if (wsdlElement != null)
        {
            hints.put(WsConnectorConstants.WSDL_ELEMENT, wsdlElement);
        }

        String uri = params.get("uri");
        if (uri != null)
        {
            hints.put(WsConnectorConstants.URI, uri);
        }

        return hints;
    }
}
