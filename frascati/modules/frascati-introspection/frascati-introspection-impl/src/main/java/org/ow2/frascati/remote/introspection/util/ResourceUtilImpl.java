/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.remote.introspection.util;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.binding.BindingBundle;
import org.ow2.frascati.remote.introspection.binding.BindingManagerItf;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf;
import org.ow2.frascati.remote.introspection.resources.Attribute;
import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.ComponentStatus;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;
import org.ow2.frascati.remote.introspection.resources.Parameter;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Utility class used to convert objects from Fractal API to REST (SCA)
 * resources.
 * 
 * @author Gwenael Cattez
 */
public class ResourceUtilImpl implements ResourceUtilItf
{
    /** The logger */
    private final Logger logger = Logger.getLogger(ResourceUtilImpl.class.getName());

    @Reference(name = "fractalUtil")
    protected FractalUtilItf fractalUtil;

    @Reference(name = "bindingManager")
    private BindingManagerItf bindingManager;
   
    @Reference(name= "scaModelUtil")
    private SCAModelUtilItf scaModelUtil;
    
    
    public Component getComponentResource(String componentPath, org.objectweb.fractal.api.Component comp)
    {
        logger.fine("getComponentResource "+componentPath+" "+fractalUtil.getComponentName(comp));
        Component compResource = new Component();
        String componentName=fractalUtil.getComponentName(comp);
        compResource.setName(componentName);
        compResource.setPath(componentPath);
        String componentStatus=fractalUtil.getComponentStatus(comp);
        if (componentStatus.equals(ComponentStatus.STARTED.value()))
        {
            compResource.setStatus(ComponentStatus.STARTED);
        } else
        {
            compResource.setStatus(ComponentStatus.STOPPED);
        }
        return compResource;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.ResourceUtilItf#getComponentResource(org.objectweb.fractal.api.Component)
     */
    public Component getComponentResource(org.objectweb.fractal.api.Component comp)
    {
        String componentPath=fractalUtil.getComponentName(comp);
        return getComponentResource(componentPath,comp);
    }
    
    
    public Component getComponentResource(Component parentResource, org.objectweb.fractal.api.Component comp)
    {
        String componentPath=parentResource.getPath()+"/"+fractalUtil.getComponentName(comp);
        return getComponentResource(componentPath, comp);
    }

    public Component getFullComponentResource(String path, org.objectweb.fractal.api.Component comp)
    {
        if(comp==null)
        {
            logger.fine("getFullComponentResource null component");
            return null;
        }
        
        Component compResource = this.getComponentResource(path,comp);
        List<org.objectweb.fractal.api.Component> subComponents=fractalUtil.getSubComponents(comp);
        for(org.objectweb.fractal.api.Component subComponent : subComponents)
        {
            if(fractalUtil.isSCAComponent(subComponent))
            {
                compResource.getComponents().add(getComponentResource(compResource,subComponent));
            }
        }
        
        // Get SCA Services // Get SCA References
        this.addPortResources(comp, compResource);

        // Get SCA properties
        this.addPropertiesResource(comp, compResource.getProperties());

        return compResource;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.ResourceUtilItf#getFullComponentResource(org.objectweb.fractal.api.Component)
     */
    public Component getFullComponentResource(org.objectweb.fractal.api.Component comp)
    {
        String componentPath=fractalUtil.getComponentName(comp);
        return getFullComponentResource(componentPath,comp);
    }

    private void addPortResources(org.objectweb.fractal.api.Component comp, Component compResource)
    {
        logger.fine("addPortResources "+comp);
        Interface itf= null;
        InterfaceType itfType=null;
        
        Port port;
        for(Object itfObject : comp.getFcInterfaces())
        {
            if(comp==itfObject)
            {
                logger.fine("continue "+itfObject);
                continue;
            }
            
            itf = (Interface) itfObject;
            itfType = (InterfaceType) itf.getFcItfType();
            logger.fine("***FcInterface "+itf.getFcItfName() +" "+itfType.isFcCollectionItf());
            if(itfType.isFcCollectionItf())
            {
                Object serverObject=fractalUtil.getServerInterface(itf);
                if(serverObject==null)
                {
                    /**if the fractal interface is multiple but not bound it's the multiple reference fractal definition*/
                    logger.fine("interface "+itf.getFcItfName()+" is not bounded collection");
                    continue;
                }
            }
            
            //TODO remove
            port=getPortResource(compResource,itf);
            
            if(itfType.isFcClientItf())
            {
                logger.fine("add interface "+itf.getFcItfName()+" to reference resources");
                compResource.getReferences().add(port);
            } else
            {
                if(!fractalUtil.isFractalControllerInterface(itf))
                {
                    logger.fine("add interface "+itf.getFcItfName()+" to service resources");
                    compResource.getServices().add(port);
                }
            }
        }
        
        
        // Add the not bound collection interfaces.
        ComponentType compType = (ComponentType) comp.getFcType();
        boolean referenceAlreadyFound;
        for (InterfaceType compInterfaceType : compType.getFcInterfaceTypes())
        {
            logger.fine("compInterfaceType "+compInterfaceType.getFcItfName());
            if (compInterfaceType.isFcCollectionItf() && compInterfaceType.isFcClientItf())
            {
                logger.fine("collection client "+compInterfaceType.getFcItfName());
                // Check the collection is not already in the references
                referenceAlreadyFound = false;
                for (Port reference : compResource.getReferences())
                {
                    if (reference.getName().equals(compInterfaceType.getFcItfName()))
                    {
                        referenceAlreadyFound = true;
                    }
                }
                
                logger.fine("referenceAlreadyFound "+referenceAlreadyFound);
                if (!referenceAlreadyFound)
                {
                    port=getPortResource(compResource,compInterfaceType);
                    compResource.getReferences().add(port);
                }
            }
        }
        
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.ResourceUtilItf#getPortResource(org.objectweb.fractal.api.Interface)
     */
    public Port getPortResource(org.objectweb.fractal.api.Interface itf)
    {
       return getPortResource(itf.getFcItfName(), itf); 
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.ResourceUtilItf#getPortResource(org.objectweb.fractal.api.Interface)
     */
    public Port getPortResource(String portPath,org.objectweb.fractal.api.Interface itf)
    {
        logger.fine("getPortResource "+fractalUtil.getComponentName(itf.getFcItfOwner())+"/"+itf.getFcItfName());
        InterfaceType itfType=(InterfaceType) itf.getFcItfType();
        Port port=this.getPortResource(portPath, itfType, itf.getClass().getClassLoader());

        // SCA bindings
        addBindingsResource(itf, port.getBindings());
        
        return port;
    }
    
    private Port getPortResource(Component compResource,org.objectweb.fractal.api.Interface itf)
    {
        String portPath=compResource.getPath()+"/"+itf.getFcItfName();
        return getPortResource(portPath, itf);
    }
    
    private Port getPortResource(String portPath, InterfaceType itfType, ClassLoader classloader)
    {
        logger.fine("getPortResource itfType: "+itfType.getFcItfName());
        Port port = new Port();
        port.setName(itfType.getFcItfName());
        port.setPath(portPath);
        String signature = itfType.getFcItfSignature();
        
        if (itfType.isFcClientItf())
        {
            if (itfType.isFcOptionalItf())
            {
                if (!itfType.isFcCollectionItf())
                {
                    port.setMutiplicity(Multiplicity._01);
                } else
                {
                    port.setMutiplicity(Multiplicity._0N);
                }
            } else
            {
                if (!itfType.isFcCollectionItf())
                {
                    port.setMutiplicity(Multiplicity._11);
                } else
                {
                    port.setMutiplicity(Multiplicity._1N);
                }
            }
            logger.fine("multiplicity "+port.getMutiplicity());
        }
        
        org.ow2.frascati.remote.introspection.resources.Interface rItf = new org.ow2.frascati.remote.introspection.resources.Interface();
        rItf.setClazz(signature);
        port.setImplementedInterface(rItf);
        addMethodsResource(signature, classloader, rItf.getMethods());
        
        return port;
    }

    private Port getPortResource(Component compResource, InterfaceType itfType)
    {
        String portPath=compResource.getPath()+"/"+itfType.getFcItfName();
        return this.getPortResource(portPath,itfType, itfType.getClass().getClassLoader());
    }
    
    /**
     * @param signature
     * @param classLoader
     * @param methods
     */
    private void addMethodsResource(String signature, ClassLoader classLoader, List<Method> methods)
    {
        Class<?> interfaceClass=null;
        try
        {
            interfaceClass = classLoader.loadClass(signature);
        }
        catch (ClassNotFoundException e)
        {
            logger.warning("can't load class for signature " + signature);
            return;
        }
        
        Method method;
        List<org.ow2.frascati.remote.introspection.resources.Parameter> parameters;
        org.ow2.frascati.remote.introspection.resources.Parameter parameter;
        int index;
        for (java.lang.reflect.Method m : interfaceClass.getMethods())
        {
            method = new Method();
            method.setName(m.getName());
            parameters = method.getParameters();
            index = 0;
            for (Class<?> param : m.getParameterTypes())
            {
                parameter = new Parameter();
                parameter.setName("Parameter" + index);
                parameter.setType(param.getName());
                parameters.add(parameter);
                index++;
            }
            methods.add(method);
        }
    }
    
    /**
     * Get SCA bindings defined on a SCA service or reference.
     * 
     * @param itf - The interface to search bindings on.
     * @param bindings - The collection of bindings to populate.
     * @throws NoSuchInterfaceException
     */
    private void addBindingsResource(org.objectweb.fractal.api.Interface itf, Collection<Binding> bindings)
    {
        logger.fine("addBindingsResource, interface "+itf.getFcItfName());
        try
        {
            BindingBundle bindingBundle = bindingManager.getBindingBundle(itf);
            logger.fine(bindingBundle.toString());
            Binding binding;
            for (BindingProcessorItf bindingProcessor : bindingBundle.getProcessors())
            {
                logger.info(bindingProcessor.getBindingKind().name());
                for (Object boundedObject : bindingBundle.getBoundedObjects(bindingProcessor))
                {
                    logger.info(boundedObject.getClass().getName());
                    binding = new Binding();
                    binding.setKind(bindingProcessor.getBindingKind());
                    addAttributesResource(bindingProcessor, boundedObject, binding.getAttributes());
                    bindings.add(binding);
                }
            }
        } catch (NoSuchInterfaceException e)
        {
            logger.info("addBindingsResource NoSuchInterfaceException for interface "+itf.getFcItfName());
        }
    }

    /**
     * Get attributes defined on a Fractal Component. This method is used to
     * retrieve SCA bindings attributes such as uri, port, etc.
     * 
     * @param comp A Fractal binding component.
     * @param attributes The collection of attributes to populate
     */
    private void addAttributesResource(BindingProcessorItf processor, Object boundedObject, Collection<Attribute> attributes)
    {
        try
        {
            List<BindingAttribute> bindingAttribues = processor.getAttributes(boundedObject);
            Attribute attribute;
            for (BindingAttribute bindingAttribute : bindingAttribues)
            {
                attribute = new Attribute();
                attribute.setName(bindingAttribute.getName());
                attribute.setType(bindingAttribute.getClazz());
                attribute.setValue(bindingAttribute.getValue());
                attributes.add(attribute);
            }
        } catch (BindingProcessorException bindingProcessorException)
        {
            logger.severe("addAttributesResource " + bindingProcessorException.getMessage());
        }
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.ResourceUtilItf#getPropertyResource(org.objectweb.fractal.api.Component, java.lang.String)
     */
    public Property getPropertyResource(org.objectweb.fractal.api.Component component, String propertyName)
    {
        logger.fine("getPropertyResource "+fractalUtil.getComponentName(component)+" "+propertyName);
        SCAPropertyController scaPropertyController=fractalUtil.getSCAPropertyController(component);
        if(scaPropertyController==null)
        {
            logger.warning("getPropertyResource Can't find SCAPropertyController for component "+fractalUtil.getComponentName(component));
            return null;
        }
        
        if(!scaPropertyController.containsPropertyName(propertyName))
        {
            logger.warning("getPropertyResource Can't find property "+propertyName+" for component "+fractalUtil.getComponentName(component));
            return null;
        }
        
        Property property = new Property();
        property.setName(propertyName);
        property.setValue(scaPropertyController.getValue(propertyName).toString());
        property.setType(scaPropertyController.getType(propertyName).getName());
        return property;
    }

    /**
     * Get the list of properties defined on an SCA component
     * 
     * @param comp The SCA component to introspect
     * @param props The collection of properties to populate
     */
    private void addPropertiesResource(org.objectweb.fractal.api.Component comp, Collection<Property> props)
    {
        NameController compNameController = null;
        try
        {
            compNameController = Fractal.getNameController(comp);
            SCAPropertyController propCtl = fractalUtil.getSCAPropertyController(comp);

            for (String propName : propCtl.getPropertyNames())
            {
                Property prop = new Property();
                prop.setName(propName);

                Object propertieValue = propCtl.getValue(propName);
                if (propertieValue == null)
                {
                    prop.setValue("undefined value");
                } else
                {
                    prop.setValue(propertieValue.toString());
                }

                prop.setType(propCtl.getType(propName).getName());
                props.add(prop);
            }
        } catch (NoSuchInterfaceException e)
        {
            // No properties
            if (compNameController != null)
            {
                logger.info("component " + compNameController.getFcName() + " has no properties");
            }
        }
    }
}
