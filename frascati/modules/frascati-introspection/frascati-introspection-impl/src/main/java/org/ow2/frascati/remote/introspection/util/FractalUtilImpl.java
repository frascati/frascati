/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.juliac.desc.NoSuchControllerDescriptorException;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Utility class for Fractal API
 * 
 */
@Scope("COMPOSITE")
public class FractalUtilImpl implements FractalUtilItf
{
    public static final String SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME = "sca-component-controller";

    /** A reference to the FraSCAti Domain */
    @Reference(name = "compositeManager")
    protected CompositeManager compositeManager;
    
    /** The logger */
    private final Logger logger = Logger.getLogger(FractalUtilImpl.class.getName());

    /**
     * Wrapper for java.util.logging.Logger.entering
     * 
     * @param method
     * @param params
     */
    private void logEntering(String method, Object... params)
    {
        logger.entering(FractalUtilImpl.class.getName(), method, params);
    }

    /**
     * Wrapper for java.util.logging.Logger.exiting
     * 
     * @param method
     * @param params
     */
    private void logExiting(String method, Object... params)
    {
        logger.exiting(FractalUtilImpl.class.getName(), method, params);
    }


    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#displayComponent(org.objectweb.fractal.api.Component)
     */
    public void displayComponent(org.objectweb.fractal.api.Component component)
    {
        displayComponent(Level.FINE, component);
    }


    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#displayComponent(java.util.logging.Level, org.objectweb.fractal.api.Component)
     */
    public void displayComponent(java.util.logging.Level level, org.objectweb.fractal.api.Component component)
    {
        String componentName = "";
        logger.log(level, "**************************************");
        try
        {
            NameController nameController = Fractal.getNameController(component);
            componentName = nameController.getFcName();
            logger.log(level, "Print component " + componentName);
        } catch (NoSuchInterfaceException e)
        {
            logger.log(level, "No NameController for component " + component);
        }

        try
        {
            BindingController bindingController = Fractal.getBindingController(component);
            Interface requiredInterface;
            logger.log(level, "required interfaces (server)");
            for (String requiredInterfaceName : bindingController.listFc())
            {
                Object requiredInterfaceObject = bindingController.lookupFc(requiredInterfaceName);
                if (requiredInterfaceObject == null)
                {
                    logger.log(level, "\t" + requiredInterfaceName + " not bound");
                } else if (requiredInterfaceObject instanceof Interface)
                {
                    requiredInterface = (Interface) requiredInterfaceObject;
                    logger.log(level, "\t" + requiredInterfaceName + " bound to " + requiredInterface.getFcItfName());
                } else
                {
                    logger.log(level, "\t" + requiredInterfaceName + " bound to " + requiredInterfaceObject.getClass().getName() + " object");
                }

            }
        } catch (NoSuchInterfaceException e)
        {
            logger.log(level, "No BindingController for component " + componentName);
        }

        try
        {
            ContentController contentController = Fractal.getContentController(component);
            logger.log(level, "internal interfaces");
            for (Object internalInterfaceObject : contentController.getFcInternalInterfaces())
            {
                logger.log(level, "\t" + ((Interface) internalInterfaceObject).getFcItfName());
            }
        } catch (NoSuchInterfaceException e)
        {
            logger.log(level, "No ContentController for component " + component + " (primitive component)");
        }

        logger.log(level, "external interface");
        for (Object externalInterfaceObject : component.getFcInterfaces())
        {
            if (externalInterfaceObject instanceof Interface)
            {
                Interface itf = (Interface) externalInterfaceObject;
                InterfaceType itfType = (InterfaceType) itf.getFcItfType();
                String role = (itfType.isFcClientItf()) ? "client" : "server";
                logger.log(level, "\t " + role + " : " + itf.getFcItfName());
            } else
            {
                logger.log(level, "\t" + externalInterfaceObject.getClass().getName());
            }
        }
        logger.log(level, "**************************************");
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getAttributeController(org.objectweb.fractal.api.Component)
     */
    public AttributeController getAttributeController(Component component)
    {
        try
        {
            return Fractal.getAttributeController(component);
        }
        catch (NoSuchInterfaceException e)
        {
            return null;
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getBindingController(org.objectweb.fractal.api.Component)
     */
    public BindingController getBindingController(Component component)
    {
        try
        {
            return Fractal.getBindingController(component);
        } catch (NoSuchInterfaceException e)
        {
            return null;
        }
       
    }
    
    /**
     * Get a reference to the fractal component designated by id.
     * 
     * @param clearedComponentId The full component id (ex: /a/b/d)
     * @return the reference to the fractal component designated by id
     * @throws NoSuchInterfaceException if the component is not found
     */
    public Component getComponent(String componentId) throws NoSuchInterfaceException
    {
        logEntering("getComponent", componentId);

        String clearedComponentId = clearFractalId(componentId);
        StringTokenizer stringTokenizer = new StringTokenizer(clearedComponentId, "/");
        Component current = null;

        if (stringTokenizer.hasMoreTokens())
        {
            String parentName = nextToken(stringTokenizer);
            try
            {
                current = this.compositeManager.getComposite(parentName);
            } catch (ManagerException e)
            {
                throw new NoSuchInterfaceException(parentName);
            }

            while (stringTokenizer.hasMoreTokens())
            {
                String name = nextToken(stringTokenizer);
                current = ContentControllerHelper.getSubComponentByName(current, name);
                if (current == null)
                {
                    throw new NoSuchInterfaceException(name);
                }
            }
        }

        logExiting("getComponent", current);
        return current;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getComponentName(org.objectweb.fractal.api.Component)
     */
    public String getComponentName(Component component)
    {
        String componentName;
        try
        {
            componentName = Fractal.getNameController(component).getFcName();
        } catch (NoSuchInterfaceException e)
        {
            componentName = "undefined";
        }
        return componentName;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getComponentStatus(org.objectweb.fractal.api.Component)
     */
    public String getComponentStatus(Component component)
    {
        String componentStatus;
        try
        {
            componentStatus = Fractal.getLifeCycleController(component).getFcState();
        } catch (NoSuchInterfaceException e)
        {
            componentStatus = "undefined";
        }
        return componentStatus;
    }
    
    /**
     * Get a reference to the fractal Component owner of the fractal Interface
     * designed by itfId
     * 
     * @param compositeManager where to search for the top level composite
     * @param itfId Id of a Fractal Interface
     * @return the Component owner of the Interface
     * @throws NoSuchInterfaceException if the component is not found
     */
    public Component findComponentByItfId(String itfId) throws NoSuchInterfaceException
    {
        logEntering("findComponentByItfId", itfId);
        String clearedItfId = clearFractalId(itfId);
        String compId = clearedItfId.substring(0, clearedItfId.lastIndexOf('/'));
        Component component = getComponent(compId);
        logExiting("findComponentByItfId", component);
        return component;
    }

    /**
     * Check if a Component have a ContentController i.e is a Primitive
     * component
     * 
     * @param component
     * @return true if the component is a Primitive component, else false
     */
    public boolean isPrimitiveComponent(Component component)
    {
        try
        {
            Fractal.getContentController(component);
            return false;
        } catch (NoSuchInterfaceException e)
        {
            return true;
        }
    }

    /**
     * Check if a fractal component is a SCa component i.e check if fractal
     * component have a interface named sca-component-controller
     * 
     * @param component
     * @return true if the component is a SCA component, else false
     */
    public boolean isSCAComponent(Component component)
    {
        try
        {
            component.getFcInterface(SCA_COMPONENT_CONTEXT_CTRL_ITF_NAME);
            return true;
        } catch (NoSuchInterfaceException e)
        {
            return false;
        }
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#isFractalControllerInterface(org.objectweb.fractal.api.Interface)
     */
    public boolean isFractalControllerInterface(Interface itf)
    {
       InterfaceType itfType=(InterfaceType) itf.getFcItfType();
       String interfaceName=itf.getFcItfName();
       return (!itfType.isFcClientItf())&&(interfaceName.endsWith("-controller")); 
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#isCollectionInterfaceInstance(org.objectweb.fractal.api.Interface)
     */
    public boolean isCollectionInterfaceInstance(Interface itf)
    {
        logger.fine("isCollectionInterfaceInstance "+itf.getFcItfName());
        String collectionInterfaceRegex = "(.*)-(0|[1-9]\\d*)";
        String interfaceName=itf.getFcItfName();
        InterfaceType itfType=(InterfaceType) itf.getFcItfType();
        if(!(interfaceName.matches(collectionInterfaceRegex) && itfType.isFcCollectionItf()))
        {
            return false;
        }
        
        Component owner=itf.getFcItfOwner();
        String baseCollectionInterfaceName=interfaceName.replaceAll(collectionInterfaceRegex, "$1");
        
        try
        {
            owner.getFcInterface(baseCollectionInterfaceName);
            return true;
        }
        catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            return false;
        }
    }
    
    /**
     * Get the SCAPropertyController related to component
     * 
     * @param component
     * @return the SCAPropertyController
     * @throws NoSuchInterfaceException if the SCAPropertyController is not
     *         found
     */
    public SCAPropertyController getSCAPropertyController(Component component)
    {
        try
        {
            return (SCAPropertyController) component.getFcInterface(SCAPropertyController.NAME);
        }
        catch (NoSuchInterfaceException e)
        {
            return null;
        }
        
       
    }

    /**
     * Get a reference to the fractal interface designed by id
     * 
     * @param compositeManager where to search for the top level composite
     * @param itfId Id of a fractal interface to look for
     * @return The fractal interface designed by itfIf
     * @throws NoSuchInterfaceException if the Interface does not exist
     */
    public Interface getInterface(String itfId) throws NoSuchInterfaceException
    {
        logEntering("getInterface", itfId);
        String clearedItfId = clearFractalId(itfId);
        Component owner = findComponentByItfId(itfId);
        String interfaceName = clearedItfId.substring(clearedItfId.lastIndexOf('/') + 1, clearedItfId.length());
        logger.fine("interfaceName : " + interfaceName);

        Interface itf = null;
        Object interfaceObject = owner.getFcInterface(interfaceName);
        if (interfaceObject != null)
        {
            try
            {
                itf = (Interface) owner.getFcInterface(interfaceName);
            } catch (ClassCastException classCastException)
            {
                logger.warning("ClassCastException cannot cast ObjectInterface of " + interfaceName + "(class: " + interfaceObject.getClass().getName()
                        + ") to org.objectweb.fractal.api.Interface");
            }
        }

        if (itf == null)
        {
            throw new NoSuchInterfaceException(interfaceName);
        }

        logExiting("getInterface", itf);
        return itf;
    }

    /**
     * Check if a fractal interface is a SCa reference
     * 
     * @param itf
     * @return true if the interface is a SCA reference, else false
     */
    public boolean isSCAReference(Interface itf)
    {
        return itf instanceof TinfiComponentOutInterface<?>;
    }
    
    /**
     * Get subComponents of a Fractal Component
     * 
     * @param owner the component to introspect
     * @return a list of all subComponents
     * @throws NoSuchInterfaceException
     */
    public List<Component> getSubComponents(Component parent)
    {
        logEntering("getSubComponents", parent);
        List<Component> subComponents=new ArrayList<Component>();
        try
        {
            ContentController parentContentController = Fractal.getContentController(parent);
            subComponents = Arrays.asList(parentContentController.getFcSubComponents());
        }
        catch(NoSuchInterfaceException noSuchInterfaceException)
        {
            logger.fine("no content controller found for component "+this.getComponentName(parent));
        }
        logExiting("getSubComponent", subComponents);
        return subComponents;
    }

    /**
     * Get a reference to the fractal Component named subComponentName owner by
     * fractal Component parent
     * 
     * @param parent the Component to introspect
     * @param subComponentName the name of the searched subComponent
     * @return the subcomponent named subComponentName or null if don't exist
     * @throws NoSuchInterfaceException if the parent component is a primitive
     *         Component, i.e it have no ContentController
     */
    public Component getSubComponent(Component parent, String subComponentName) throws NoSuchInterfaceException
    {
        logEntering("getSubComponent", parent, subComponentName);
        List<Component> subComponents = getSubComponents(parent);

        String tmpSubComponentName;
        Component subComponent = null;
        for (Component tmpSubcomponent : subComponents)
        {
            tmpSubComponentName = Fractal.getNameController(tmpSubcomponent).getFcName();
            if (tmpSubComponentName.equals(subComponentName))
            {
                subComponent = tmpSubcomponent;
                break;
            }
        }
        logExiting("getSubComponent", subComponent);
        return subComponent;
    }

    /**
     * TODO something look strange, it seems that its not the subComponent but
     * all children at the same level Get subComponents of a Fractal Component
     * 
     * @param owner the component to introspect
     * @return a list of all subComponents
     * @throws NoSuchInterfaceException
     */
    public List<Component> getChildren(Component owner) throws NoSuchInterfaceException
    {
        logEntering("getSubComponents", owner);
        List<Component> children = new ArrayList<Component>();
        Component[] parents = Fractal.getSuperController(owner).getFcSuperComponents();
        for (Component c : parents)
        {
            ContentController contentController = Fractal.getContentController(c);
            Component[] subComponents = contentController.getFcSubComponents();

            for (Component child : subComponents)
            {
                children.add(child);
            }
        }
        logExiting("getSubComponents", children);
        return children;
    }

    /**
     * Util method to start component
     * 
     * @param owner The fractal component to start
     * @throws NoSuchInterfaceException
     * @throws IllegalLifeCycleException
     */
    public void startComponent(Component owner) throws NoSuchInterfaceException, IllegalLifeCycleException
    {
        logEntering("startComponent", owner);
        LifeCycleController lcc = Fractal.getLifeCycleController(owner);
        lcc.startFc();
        logExiting("startComponent");
    }

    /**
     * Util method to stop component
     * 
     * @param owner The fractal component to stop
     * @throws NoSuchInterfaceException
     * @throws IllegalLifeCycleException
     */
    public void stopComponent(Component owner) throws NoSuchInterfaceException, IllegalLifeCycleException
    {
        logEntering("stopComponent", owner);
        LifeCycleController lcc = Fractal.getLifeCycleController(owner);
        lcc.stopFc();
        logExiting("stopComponent");
    }

    /**
     * Util to find an Fractal interface on a Fractal component and encapsulates
     * the Exception
     * 
     * @param owner The Fractal component holding the Fractal interface
     * @param itfId The name of the Fractal interface
     * @return a Fracatal interface resource
     * @throws NoSuchInterfaceException
     */
    public Interface findInterfaceById(Component owner, String itfId) throws NoSuchInterfaceException
    {
        logEntering("findInterfaceById", owner, itfId);
        String clearedItfId = clearFractalId(itfId);
        String itfName = clearedItfId.substring(clearedItfId.lastIndexOf('/') + 1);
        Interface itf = (Interface) owner.getFcInterface(itfName);
        logExiting("findInterfaceById");
        return itf;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getClientInterfaces(org.objectweb.fractal.api.Interface)
     */
    public List<Interface> getClientInterfaces(Interface itf)
    {
        String interfaceName=itf.getFcItfName();
        Component owner=itf.getFcItfOwner();
        String ownerName=this.getComponentName(owner);
        logger.fine("getPromotedInterface owner : "+ownerName+" interface : "+interfaceName+" "+itf);
        
        List<Interface> clientInterfaces=new ArrayList<Interface>();
        List<org.objectweb.fractal.api.Component> children=this.getSubComponents(owner);
        
        logger.fine(ownerName+" have "+children.size()+" children");
        BindingController childBindingController=null;
        Interface boundedOject;
        for (Component child : children)
        {
            logger.fine("child : "+this.getComponentName(child));
            try
            {
                childBindingController=Fractal.getBindingController(child);
            }
            catch (NoSuchInterfaceException noSuchInterfaceException)
            {
                logger.fine("Can't get BindingController for component "+this.getComponentName(child));
                continue;
            }
 
            for(String fc : childBindingController.listFc())
            {
                try
                {
                    boundedOject=(Interface) childBindingController.lookupFc(fc);
                    if(boundedOject!=null && boundedOject.getFcItfName().equals(itf.getFcItfName()))
                    {
                        clientInterfaces.add((Interface)child.getFcInterface(fc));
                    }
                }
                catch (NoSuchInterfaceException e)
                {
                    logger.info("It should not happen");
                }
            }
        }
        return clientInterfaces;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.util.FractalUtilItf#getServerInterface(org.objectweb.fractal.api.Interface)
     */
    public Object getServerInterface(Interface itf)
    {
        logger.fine("getServerInterface " + itf.getFcItfName());

        Component owner = itf.getFcItfOwner();
        BindingController ownerBindingController = null;

        try
        {
            ownerBindingController = Fractal.getBindingController(owner);
            
        }
        catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            logger.fine("Can't get BindingController for owner of interface : "+itf.getFcItfName());
            return null;
        }
         
        try
        {
            return ownerBindingController.lookupFc(itf.getFcItfName());
            
        }
        catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            logger.fine("No object bound to interface "+itf.getFcItfName());
            return null;
        }
    }
    
    
    /**
     * Get the name of the last interface bound to a collection Reference define
     * by collectionName i.e collectionName-(highest index found)
     * 
     * 
     * @param owner Owner of the collection
     * @param collectionName name of the collection
     * @return The last interfaceName bound to Collection
     */
    public String getLastInterfaceCollectionName(Component owner, String collectionName)
    {
        logEntering("getLastInterfaceCollectionName", owner, collectionName);
        try
        {
            logger.finest("getLastInterfaceCollectionName owner : " + Fractal.getNameController(owner).getFcName() + ", collection interface : "
                    + collectionName);
        } catch (NoSuchInterfaceException e)
        {
            logger.finest("getLastInterfaceCollectionName interface : " + collectionName);
        }

        String collectionInterfaceRegex = "(" + collectionName + "-)(0|[1-9]\\d*)";
        int highestIndex = -1, tmpIndex;
        Interface itfTemp;
        String itfTempName, itfTempId;
        for (Object itfObject : owner.getFcInterfaces())
        {
            itfTemp = (Interface) itfObject;
            itfTempName = itfTemp.getFcItfName();
            logger.finest("\t" + itfTempName);
            if (itfTempName.matches(collectionInterfaceRegex))
            {
                itfTempId = itfTempName.replaceAll(collectionInterfaceRegex, "$2");
                tmpIndex = Integer.valueOf(itfTempId);
                logger.finest("\tmatch " + itfTempName + " " + tmpIndex);
                if (tmpIndex > highestIndex)
                {
                    highestIndex = tmpIndex;
                }

            }
        }

        String lastInterfaceCollectioName = collectionName;
        if (highestIndex != -1)
        {
            lastInterfaceCollectioName += "-" + highestIndex;
        }

        logExiting("getLastInterfaceCollectionName", lastInterfaceCollectioName);
        return lastInterfaceCollectioName;
    }

    /**
     * If the interface is a collection we have to set the name of the bind
     * reference We look for other interface, in the owner component of the
     * interface, that have a name that match pattern interfaceName-number The
     * name of the new interface is interfaceName-(highest number found + 1)
     * 
     * @param owner owner Owner of the collection
     * @param collectionName name of the collection
     * @return The next interfaceName to bound to Collection
     */
    public String getNextInterfaceName(Component owner, String collectionName)
    {
        logEntering("getNextInterfaceName", owner, collectionName);
        String lastInterfaceCollectioName = getLastInterfaceCollectionName(owner, collectionName);
        String nextInterfaceName;
        if (lastInterfaceCollectioName.equals(collectionName))
        {
            nextInterfaceName = lastInterfaceCollectioName + "-0";
        } else
        {
            String collectionInterfaceRegex = "(" + collectionName + "-)(0|[1-9]\\d*)";
            String lastInterfaceCollectioIndexString = lastInterfaceCollectioName.replaceAll(collectionInterfaceRegex, "$2");
            int lastInterfaceCollectioIndex = Integer.valueOf(lastInterfaceCollectioIndexString);
            nextInterfaceName = collectionName+"-"+String.valueOf(lastInterfaceCollectioIndex+1);
        }

        logExiting("getNextInterfaceName", nextInterfaceName);
        return nextInterfaceName;
    }

    /**
     * Clear the Id send to be sure there's no '/' at the begin and end
     * 
     * @param toClearId the identifier to clean
     * @return a clean identifier
     */
    private String clearFractalId(String toClearId)
    {
        logEntering("clearFractalId", toClearId);

        String clearId = toClearId;

        // If the first character of the interface id is '/' remove it
        if (clearId.charAt(0) == '/')
        {
            clearId = clearId.substring(1, clearId.length());
        }

        // If the last character of the interface id is '/' remove it
        if (clearId.charAt(clearId.length() - 1) == '/')
        {
            clearId = clearId.substring(0, clearId.length() - 1);
        }

        logExiting("clearFractalId", clearId);
        return clearId;
    }

    /**
     * Hack because EasyBPEL component names could contain '/' characters.
     * 
     * @param stringTokenizer
     * @return
     */
    private String nextToken(StringTokenizer stringTokenizer)
    {
        logEntering("nextToken");
        StringBuilder stringBuilder = new StringBuilder();
        String tmp = stringTokenizer.nextToken();
        stringBuilder.append(tmp);
        if (tmp.contains("{"))
        {
            tmp = stringTokenizer.nextToken("}");
            stringBuilder.append(tmp);
            stringBuilder.append(stringTokenizer.nextToken("/"));
        }

        String nextToken = stringBuilder.toString();
        logExiting("nextToken", nextToken);
        return nextToken;
    }
}
