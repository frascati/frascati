/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;
import javax.xml.namespace.QName;

import org.apache.cxf.common.util.Base64Exception;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.util.context.ContextualProperties;

/**
 *
 */
public class DeploymentImpl implements Deployment
{
    /** The logger */
    public final static Logger logger = Logger.getLogger(DeploymentImpl.class.getCanonicalName());
    
    /** A reference to the FraSCAti Domain */
    @Reference(name = "frascati-domain")
    protected CompositeManager compositeManager;
    
    @Reference(name = "classloader-manager")
    protected ClassLoaderManager classLoaderManager;
    
    /**
     * @see org.ow2.frascati.remote.introspection.Deployment#deployContribution(javax.ws.rs.core.MultivaluedMap)
     */
    public StringList deployContribution(MultivaluedMap<String, String> params)
    {
        /**Remove contribution parameter from parameters*/
        /**Only contextual properties stay in the params map*/
        List<String> contributionParams = params.remove("contribution");
        if(contributionParams.isEmpty())
        {
            throw new MyWebApplicationException("contribution parameter is missing");
        }
        
        if(contributionParams.size()>1)
        {
            throw new MyWebApplicationException("contribution parameter must be defined once ("+contributionParams.size()+" contribution parameters found)");
        }
        
        String encodedContribution = contributionParams.get(0);
        return this.deployContribution(encodedContribution, params);
    }
    
    /**
     * @see Deployment#deployContribution(String)
     */
    public StringList deployContribution(String encodedContribution)
    {
        return this.deployContribution(encodedContribution, null);
    }

    /**
     * @see Deployment#deployContribution(String)
     */
    public StringList deployContribution(String encodedContribution, MultivaluedMap<String, String> contextualProperties)
    {
        File destFile = null;
        try
        {
            destFile = FileUtil.decodeFile(encodedContribution, "zip");
            FileUtil.unZipHere(destFile);
        } catch (Base64Exception b64e)
        {
            throw new MyWebApplicationException(b64e, "Cannot Base64 encode the file");
        } catch (IOException ioe)
        {
            throw new MyWebApplicationException(ioe, "IO Exception when trying to read or write contribution zip");
        }

        try
        {
            /***
             * deploy contribution, get an array of the deployed Fractal
             * components
             */
            ProcessingContext processingContext = compositeManager.newProcessingContext();
            this.processContextualProperties(contextualProperties, processingContext);
            org.objectweb.fractal.api.Component[] deployedComposites = compositeManager.processContribution(destFile.getAbsolutePath(), processingContext);
            String compositeName;
            List<String> compositeNames = new LinkedList<String>();
            for (org.objectweb.fractal.api.Component composite : deployedComposites)
            {
                compositeName = Fractal.getNameController(composite).getFcName();
                compositeNames.add(compositeName);
            }
            StringList stringList = new StringList(compositeNames);
            return stringList;

        } catch (ManagerException e)
        {
            throw new MyWebApplicationException(e, "Error while trying to deploy the zip");
        } catch (NoSuchInterfaceException e)
        {
            throw new MyWebApplicationException(e, "Erroy while deploying one of the composite");
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.Deployment#deployComposite(javax.ws.rs.core.MultivaluedMap)
     */
    public String deployComposite(MultivaluedMap<String, String> params)
    {
        /**Remove contribution parameter from parameters*/
        /**Only contextual properties stay in the params map*/
        List<String> contributionParams = params.remove("compositeName");
        if(contributionParams.isEmpty())
        {
            throw new MyWebApplicationException("compositeName parameter is missing");
        }
        
        if(contributionParams.size()>1)
        {
            throw new MyWebApplicationException("compositeName parameter must be defined once ("+contributionParams.size()+" compositeName parameters found)");
        }
        String compositeName = contributionParams.get(0);
        
        List<String> jarParams = params.remove("jar");
        if(jarParams.isEmpty())
        {
            throw new MyWebApplicationException("jar parameter is missing");
        }
        
        if(jarParams.size()>1)
        {
            throw new MyWebApplicationException("jar parameter must be defined once ("+jarParams.size()+" jar parameters found)");
        }
        String jar = jarParams.get(0);

        return this.deployComposite(compositeName, jar, params);
    }
    
    /**
     * @see Deployment#deployComposite(String, String)
     */
    public String deployComposite(String compositeName, String encodedComposite)
    {
        return this.deployComposite(compositeName, encodedComposite, null);
    }

    /**
     * @see Deployment#deployComposite(String, String)
     */
    public String deployComposite(String compositeName, String encodedComposite, MultivaluedMap<String, String> contextualProperties)
    {
        File destFile = null;
        try
        {
            destFile = FileUtil.decodeFile(encodedComposite, "jar");
            new JarFile(destFile);
        }
        catch (Base64Exception b64e)
        {
            throw new MyWebApplicationException(b64e, "Cannot Base64 encode the file");
        } catch (IOException ioe)
        {
            throw new MyWebApplicationException(ioe, "IO Exception when trying to read or write composite jar");
        }

        try
        {
            ProcessingContext processingContext = compositeManager.newProcessingContext(new URL[] {destFile.toURI().toURL()});
            this.processContextualProperties(contextualProperties, processingContext);
            QName compositeQName = new QName(compositeName);
            org.objectweb.fractal.api.Component deployedComposite = compositeManager.processComposite(compositeQName, processingContext);
            String deployedCompositeName = Fractal.getNameController(deployedComposite).getFcName();
            return deployedCompositeName;
        }
        catch (ManagerException e)
        {
            throw new MyWebApplicationException(e, "Error while trying to deploy the composite");
        } catch (MalformedURLException e)
        {
            throw new MyWebApplicationException(e, "Cannot find the jar file");
        } catch (NoSuchInterfaceException e)
        {
            throw new MyWebApplicationException(e, "Erroy while deploying the composite");
        }
    }
    
    /**
     * @see Deployment#undeployComposite(String)
     */
    public int undeployComposite(String fullCompositeId)
    {
        try
        {
            // Remove the composite of the composite manager
            compositeManager.removeComposite(fullCompositeId);
        }
        catch (ManagerException e)
        {
            throw new MyWebApplicationException(e, "Error while trying to undeploy " + fullCompositeId);
        }
        return 0;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.Deployment#getCompositeEntriesFromJar()
     */
    public String getCompositeEntriesFromJar(String encodedComposite)
    {
        logger.fine("getCompositeEntriesFromJar jar length="+encodedComposite.length());
        JarFile jarFile = null;
        try
        {
            File destFile = FileUtil.decodeFile(encodedComposite, "jar");
            jarFile = new JarFile(destFile);
        } catch (Base64Exception b64e)
        {
            throw new MyWebApplicationException(b64e, "Cannot Base64 decode the file");
        } catch (IOException ioe)
        {
            throw new MyWebApplicationException(ioe, "IO Exception when trying to read or write composite jar");
        } catch (RuntimeException runtimeException)
        {
            throw new MyWebApplicationException(runtimeException, "Runtime Exception when trying to read or write composite jar");
        }

        logger.fine("jar file decoded");

        Enumeration<JarEntry> entries = jarFile.entries();
        JarEntry entry;
        List<String> composites = new LinkedList<String>();
        String entryName, extension;
        while (entries.hasMoreElements())
        {
            entry = entries.nextElement();
            entryName = entry.getName();

            if (entryName.contains("."))
            {
                extension = entryName.substring(entryName.lastIndexOf('.') + 1);
                if ("composite".equals(extension))
                {
                    composites.add(entryName.substring(0, entryName.lastIndexOf('.')));
                }
            }
        }

        if (composites.size() == 0)
        {
            return "";
        }

        String result = "", tmpComposite;
        int index;
        for (index = 0; index < composites.size() - 1; index++)
        {
            tmpComposite = composites.get(index);
            result += tmpComposite;
            result += "%S%";
        }
        result += composites.get(index);
        return result;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.Deployment#loadLibrary(java.lang.String)
     */
    public void loadLibrary(String encodedLib)
    {
        logger.fine("loadLibrary jar length="+encodedLib.length());
        File libFile = null;
        try
        {
            libFile = FileUtil.decodeFile(encodedLib, "jar");
            /**Test File is a jar*/
            new JarFile(libFile);
            logger.fine(libFile.getPath());
        }
        catch (Base64Exception b64e)
        {
            throw new MyWebApplicationException(b64e, "Cannot Base64 encode the file");
        } catch (IOException ioe)
        {
            throw new MyWebApplicationException(ioe, "IO Exception when trying to read or write composite jar");
        }
        
        try
        {
            classLoaderManager.loadLibraries(libFile.toURI().toURL());
        }
        catch (ManagerException managerException)
        {
            throw new MyWebApplicationException(managerException);
        }
        catch (MalformedURLException malformedURLException)
        {
            throw new MyWebApplicationException(malformedURLException);
        }
    }
    
    /**
     * Extract contextual properties from contextualProperties map and set processingContext with them
     * 
     * @param contextualProperties map of the contextual properties
     * @param processingContext processing context to fill
     */
    private void processContextualProperties(MultivaluedMap<String, String> contextualProperties, ProcessingContext processingContext)
    {
        if (contextualProperties == null || contextualProperties.isEmpty())
        {
            return;
        }

        List<String> contextualPropertyValues;
        String contextualPropertyValue, contextualPropertyParentPath;
        String[] contextualPropertySplitKey;
        Map<String, Object> contextualPropertyParentValue = null, contextualPropertyParentTmpValue;

        /** Set contextual properties of the processing context */
        for (String contextualPropertyKey : contextualProperties.keySet())
        {
            contextualPropertyValues = contextualProperties.get(contextualPropertyKey);
            /** No value is defined for contextual property key */
            if (contextualPropertyValues == null || contextualPropertyValues.isEmpty())
            {
                continue;
            }
            
            contextualPropertyValue = contextualPropertyValues.get(0);
            logger.fine("Contextual property : " + contextualPropertyKey + " " + contextualPropertyValue);

            /**
             * If contextualPropertyKey contains / we must create parent
             * contextual properties
             */
            if (contextualPropertyKey.contains(ContextualProperties.PATH_SEPARATOR))
            {
                contextualPropertySplitKey = contextualPropertyKey.split(ContextualProperties.PATH_SEPARATOR);
                int i = 0;
                contextualPropertyParentPath = "";
                while(i < contextualPropertySplitKey.length - 1)
                {
                    if (i != 0)
                    {
                        contextualPropertyParentPath += ContextualProperties.PATH_SEPARATOR;
                    }
                    contextualPropertyParentPath += contextualPropertySplitKey[i];
                    i++;
                    
                    try
                    {
                        contextualPropertyParentValue = (Map<String, Object>) processingContext.getContextualProperty(contextualPropertyParentPath);
                    }
                    catch(ClassCastException classCastException)
                    {
                        logger.severe("A part of the contextual property path is invalid, "+contextualPropertyParentPath+" is already define as a final value");
                        return;
                    }
                    
                    
                    if (contextualPropertyParentValue == null)
                    {
                        contextualPropertyParentValue = new HashMap<String, Object>();
                        break;
                    }
                }
                logger.fine("contextualPropertyParentPath "+contextualPropertyParentPath+" i "+i);
                
                contextualPropertyParentTmpValue = contextualPropertyParentValue;
                while(i < contextualPropertySplitKey.length - 1)
                {
                    logger.fine("Add Map for "+contextualPropertySplitKey[i]);
                    Map<String, Object> emptyMap = new HashMap<String, Object>();
                    contextualPropertyParentTmpValue.put(contextualPropertySplitKey[i],emptyMap);
                    contextualPropertyParentTmpValue = emptyMap;
                    i++;
                }

                processingContext.setContextualProperty(contextualPropertyParentPath, contextualPropertyParentValue);
                logger.info("Add contextual property for parent "+contextualPropertyParentPath);
            }

             processingContext.setContextualProperty(contextualPropertyKey,contextualPropertyValue);
             logger.info("Add contextual property "+contextualPropertyKey+" : "+contextualPropertyValue);
        }
    }
}
