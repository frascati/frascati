/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle, Christophe Munilla, Antonio de Almeida Souza Neto, Christophe Demarey
 *
 */

package org.ow2.frascati.remote.introspection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.MultivaluedMap;

import org.mortbay.log.Log;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.bf.BindingFactory;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.processor.ScaPropertyTypeJavaProcessor;
import org.ow2.frascati.remote.introspection.binding.BindingManagerItf;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorException;
import org.ow2.frascati.remote.introspection.exception.BadParameterTypeException;
import org.ow2.frascati.remote.introspection.exception.MyWebApplicationException;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;
import org.ow2.frascati.remote.introspection.util.ResourceUtilItf;
import org.ow2.frascati.remote.introspection.util.SCAModelUtilItf;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * This SCA component is used to expose / introspect an SCA domain remotely.
 */
@Scope("COMPOSITE")
public class RemoteScaDomainImpl implements RemoteScaDomain
{
    /** The logger */
    public final static Logger logger = Logger.getLogger(RemoteScaDomainImpl.class.getCanonicalName());

    /** A reference to the FraSCAti Domain */
    @Reference(name = "frascati-domain")
    protected CompositeManager compositeManager;

    @Reference(name = "binding-factory")
    protected BindingFactory bindingFactory;

    @Reference(name = "fractalUtil")
    protected FractalUtilItf fractalUtil;

    @Reference(name = "resourceUtil")
    protected ResourceUtilItf resourceUtil;

    @Reference(name = "scaModelUtil")
    protected SCAModelUtilItf scaModelUtil;

    @Reference(name = "bindingManager")
    private BindingManagerItf bindingManager;

    /** Default constructor. */
    public RemoteScaDomainImpl()
    {
    }

    /***************************** Logger util methods ****************************/
    
    public static void logMethod(String method, Object... params)
    {
        StringBuilder messageBuilder=new StringBuilder();
        messageBuilder.append("RemoteScaDomainImpl" );
        messageBuilder.append(method);
        for(Object param : params)
        {
            messageBuilder.append(" "+param);
        }
        logger.fine(messageBuilder.toString());
    }
    
    /**
     * Wrapper for java.util.logging.Logger.entering
     * 
     * @param method
     * @param params
     */
    private void logEntering(String method, Object... params)
    {
        logMethod("entering "+method, params);
    }

    /**
     * Wrapper for java.util.logging.Logger.exiting
     * 
     * @param method
     * @param params
     */
    private void logExiting(String method, Object... params)
    {
        logMethod("exiting "+method, params);
    }

    /***************************** To remove ****************************/
    /**
     * @see org.ow2.frascati.remote.introspection.RemoteScaDomain#getFractalComponent(java.lang.String)
     * 
     *      TODO This method is not annotated JAX-RS, see to remove it without
     *      side effects
     */
    public org.objectweb.fractal.api.Component getFractalComponent(String componentId)
    {
        logEntering("getFractalComponent", componentId);

        org.objectweb.fractal.api.Component component = null;
        try
        {
            component = fractalUtil.getComponent(componentId);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            logger.severe("No component found for id " + componentId);
        }

        logExiting("getFractalComponent", component);
        return component;
    }

    /***************************** Wrappers around FractalUtil method to throw WebApplicationException ****************************/

    private org.objectweb.fractal.api.Component getFcComponent(String componentId)
    {
        logEntering("getFcComponent", componentId);
        org.objectweb.fractal.api.Component component = null;
        try
        {
            component = fractalUtil.getComponent(componentId);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new MyWebApplicationException(noSuchInterfaceException, "No component found for for id : " + componentId);
        }
        logExiting("getFractalComponent", component);
        return component;
    }

    private org.objectweb.fractal.api.Interface getFcInterface(String interfaceId)
    {
        logEntering("getFcInterface", interfaceId);
        org.objectweb.fractal.api.Interface itf = null;
        try
        {
            itf = fractalUtil.getInterface(interfaceId);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new MyWebApplicationException(noSuchInterfaceException, "No interface found for for id : " + interfaceId);
        }
        
        Object serverObject=fractalUtil.getServerInterface(itf);
        if(serverObject==null && fractalUtil.isCollectionInterfaceInstance(itf))
        {
            logger.warning("interface "+interfaceId+" is related to a fractal collection interface that is not bound");
            throw new MyWebApplicationException("No interface found for for id : " + interfaceId);
        }
        
        logExiting("getFcInterface", itf);
        return itf;
    }

    private void startFcComponent(org.objectweb.fractal.api.Component component)
    {
        logEntering("startFcComponent", component);
        String componentName = fractalUtil.getComponentName(component);
        try
        {
            fractalUtil.startComponent(component);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new MyWebApplicationException(noSuchInterfaceException, "Cannot start component : " + componentName);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new MyWebApplicationException(illegalLifeCycleException, "The component : " + componentName
                    + " is not in the appropriate phase to be started");
        }
        logExiting("startFcComponent");
    }

    private void stopFcComponent(org.objectweb.fractal.api.Component component)
    {
        logEntering("stopFcComponent", component);
        String componentName = fractalUtil.getComponentName(component);
        try
        {
            fractalUtil.stopComponent(component);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new MyWebApplicationException(noSuchInterfaceException, "Cannot stop component : " + componentName);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new MyWebApplicationException(illegalLifeCycleException, "The component : " + componentName
                    + " is not in the appropriate phase to be stopped");
        }
        logExiting("stopFcComponent");
    }

    private SCAPropertyController getFcSCAPropertyController(org.objectweb.fractal.api.Component component)
    {
        SCAPropertyController propertyController = fractalUtil.getSCAPropertyController(component);
        if (propertyController == null)
        {
            throw new MyWebApplicationException("SCA property controller not available for component " + fractalUtil.getComponentName(component));
        }
        return propertyController;
    }

    /***************************** Implementation of the RemoteScaDomain interface ****************************/

    /**
     * @see RemoteScaDomain#getComposites()
     */
    public Collection<Component> getDomainComposites()
    {
        logEntering("getDomainComposites");

        Collection<Component> components = new ArrayList<Component>();
        for (org.objectweb.fractal.api.Component composite : compositeManager.getComposites())
        {
            components.add(resourceUtil.getFullComponentResource(composite));
        }

        logExiting("getDomainComposites", components);
        return components;
    }

    /**
     * @see RemoteScaDomain#getComponent(String)
     */
    public Component getComponent(String componentId)
    {
        logEntering("getComponent", componentId);
        org.objectweb.fractal.api.Component fracatlComponent = this.getFcComponent(componentId);
        Component component = resourceUtil.getFullComponentResource(componentId,fracatlComponent);
        logExiting("getComponent", component);
        return component;
    }

    /**
     * @see RemoteScaDomain#startComponent(String)
     */
    public boolean startComponent(String componentId)
    {
        logEntering("startComponent", componentId);
        org.objectweb.fractal.api.Component component = this.getFcComponent(componentId);
        this.startFcComponent(component);
        logExiting("startComponent");
        return true;
    }

    /**
     * @see RemoteScaDomain#stopComponent(String)
     */
    public boolean stopComponent(String componentId)
    {
        logEntering("stopComponent", componentId);
        org.objectweb.fractal.api.Component component = this.getFcComponent(componentId);
        this.stopFcComponent(component);
        logExiting("stopComponent");
        return true;

    }

    /**
     * @see RemoteScaDomain#getInterface(String)
     */
    public Port getInterface(String interfaceId)
    {
        logEntering("getInterface", interfaceId);
        org.objectweb.fractal.api.Interface itf = this.getFcInterface(interfaceId);
        Port interfacePort = resourceUtil.getPortResource(interfaceId,itf);
        logExiting("getInterface", interfacePort);
        return interfacePort;
    }

    /**
     * @see RemoteScaDomain#invokeMethod(String, MultivaluedMap)
     */
    public String invokeMethod(@PathParam("id") String interfaceId, MultivaluedMap<String, String> params)
    {
        logEntering("invokeMethod", interfaceId, params);

        org.objectweb.fractal.api.Interface itf = this.getFcInterface(interfaceId);

        String methodName = params.getFirst("methodName");
        if (methodName == null)
        {
            throw new MyWebApplicationException("methodName parameter is missing ");
        }

        Method method = this.getMethod(itf, methodName);
        if (method == null)
        {
            throw new MyWebApplicationException("No method named " + methodName + " found for " + itf.getFcItfName());
        }

        Object[] arguments = null;
        try
        {
            arguments = this.getArguments(method, convertMultiValueMap(params));
        } catch (BadParameterTypeException bpte)
        {
            throw new MyWebApplicationException(bpte);
        }

        String invocationResponse=null;
        try
        {
            Object invokationReturn = method.invoke(itf, arguments);
            if (invokationReturn == null)
            {
                invocationResponse = "nothing to return";
            } else
            {
                invocationResponse = invokationReturn.toString();
            }
        } catch (InvocationTargetException invocationTargetException)
        {
            invocationTargetException.getCause().printStackTrace();
            throw new MyWebApplicationException(invocationTargetException, "Error while invoking " + method.getName());
        } catch (Exception exception)
        {
            throw new MyWebApplicationException(exception, "Error while invoking " + method.getName());
        }

        logExiting("invokeMethod", invocationResponse);
        return invocationResponse;
    }

    /**
     * @see RemoteScaDomain#getSubComponents(String)
     */
    public Collection<Component> getSubComponents(String componentId)
    {
        logEntering("getSubComponents", componentId);

        org.objectweb.fractal.api.Component parent = this.getFcComponent(componentId);
        List<org.objectweb.fractal.api.Component> subComponents = fractalUtil.getSubComponents(parent);
        Collection<Component> children = new ArrayList<Component>();
        Component child;
        for (org.objectweb.fractal.api.Component subComponent : subComponents)
        {
            if (fractalUtil.isSCAComponent(subComponent))
            {
                child = resourceUtil.getComponentResource(subComponent);
                children.add(child);
            }
        }

        logExiting("getSubComponents", children);
        return children;
    }

    /**
     * @see RemoteScaDomain#getProperty(String, String)
     */
    public Property getProperty(String componentId, String propertyName)
    {
        logEntering("getProperty", componentId, propertyName);
        org.objectweb.fractal.api.Component component = this.getFcComponent(componentId);
        Property property = resourceUtil.getPropertyResource(component, propertyName);
        logExiting("getProperty", property);
        return property;
    }

    /**
     * @see RemoteScaDomain#setProperty(String, String, String)
     */
    public void setProperty(String componentId, String propertyName, String value)
    {
        logEntering("setProperty", componentId, propertyName);

        org.objectweb.fractal.api.Component component = this.getFcComponent(componentId);
        SCAPropertyController propertyController = this.getFcSCAPropertyController(component);
        Object propertyValue = null;
        try
        {
            Class<?> propertyType = propertyController.getType(propertyName);
            propertyValue = ScaPropertyTypeJavaProcessor.stringToValue(propertyType.getCanonicalName(), value, this.getClass().getClassLoader());
        } catch (Exception exception)
        {
            throw new MyWebApplicationException(exception, "Error while creating the new Object property");
        }

        this.stopFcComponent(component);
        propertyController.setType(propertyName, propertyValue.getClass());
        propertyController.setValue(propertyName, propertyValue);
        this.startFcComponent(component);

        logExiting("setProperty");
    }

    public void addBinding(String interfaceId, MultivaluedMap<String, String> params)
    {
        org.objectweb.fractal.api.Interface itf = this.getFcInterface(interfaceId);
        Log.info("addBinding on interface : " + interfaceId);
        try
        {
            Map<String, String> stringHints = convertMultiValueMap(params);
            bindingManager.bind(itf, interfaceId, stringHints);
        } catch (NoBindingKindException noBindingKindException)
        {
            throw new MyWebApplicationException(noBindingKindException);
        } catch (UnsupportedBindingException unsupportedBindingKindException)
        {
            throw new MyWebApplicationException(unsupportedBindingKindException);
        } catch (BindingProcessorException bindingProcessorException)
        {
            throw new MyWebApplicationException(bindingProcessorException);
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.remote.introspection.RemoteScaDomain#removeBinding(java.lang.String,
     *      javax.ws.rs.core.MultivaluedMap)
     */
    public void removeBinding(String fullInterfaceId, MultivaluedMap<String, String> params)
    {
        // TODO Auto-generated method stub
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.remote.introspection.RemoteScaDomain#removeBinding(java.lang.String,
     *      java.lang.String)
     */
    public void removeBinding(String interfaceId, String position)
    {
        org.objectweb.fractal.api.Interface itf = this.getFcInterface(interfaceId);

        int inPosition;
        try
        {
            inPosition = Integer.valueOf(position);
        } catch (NumberFormatException numberFormatException)
        {
            inPosition = 0;
        }

        Log.info("removeBinding on interface : " + interfaceId);
        try
        {
            bindingManager.unbind(itf, interfaceId, inPosition);
        } catch (BindingProcessorException bindingProcessorException)
        {
            throw new MyWebApplicationException(bindingProcessorException);
        } catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
        {
            throw new MyWebApplicationException(unsupportedBindingComponentException);
        }
    }

    /**
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.remote.introspection.RemoteScaDomain#setBindingAttribute(java.lang.String,
     *      javax.ws.rs.core.MultivaluedMap, java.lang.String, java.lang.String)
     */
    public void setBindingAttribute(String fullInterfaceId, MultivaluedMap<String, String> params, String attribute, String newValue)
    {
        // TODO
    }

    /**
     * @see RemoteScaDomain#setBindingAttribute(String, int, String, String)
     */
    public void setBindingAttribute(String interfaceId, String position, String attribute, String newValue)
    {
        org.objectweb.fractal.api.Interface itf = this.getFcInterface(interfaceId);

        int inPosition;
        try
        {
            inPosition = Integer.valueOf(position);
        } catch (NumberFormatException numberFormatException)
        {
            inPosition = 0;
        }

        try
        {
            bindingManager.setBindingAttribute(itf, inPosition, attribute, newValue);
        } catch (BindingProcessorException bindingProcessorException)
        {
            throw new MyWebApplicationException(bindingProcessorException);
        } catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
        {
            throw new MyWebApplicationException(unsupportedBindingComponentException);
        }
    }

    /**
     * Convert MultivaluedMap to simple Map
     * 
     * @param multivalueMap
     * @return
     */
    private Map<String, String> convertMultiValueMap(MultivaluedMap<String, String> multivalueMap)
    {
        Map<String, String> convertedMap = new HashMap<String, String>();
        String multiValueMapValue;
        for (String multiValueMapKey : multivalueMap.keySet())
        {
            multiValueMapValue = multivalueMap.getFirst(multiValueMapKey);
            convertedMap.put(multiValueMapKey, multiValueMapValue);
        }
        return convertedMap;
    }

    private java.lang.reflect.Method getMethod(org.objectweb.fractal.api.Interface itf, String methodName)
    {
        java.lang.reflect.Method method = null;
        for (java.lang.reflect.Method m : itf.getClass().getMethods())
        {
            if (m.getName().equals(methodName))
            {
                method = m;
            }
        }
        return method;
    }

    private Object[] getArguments(java.lang.reflect.Method method, Map<String, String> params) throws BadParameterTypeException
    {
        Object[] arguments = new Object[method.getParameterTypes().length];
        int index = 0;
        String value = null;
        for (Class<?> parameterType : method.getParameterTypes())
        {
            try
            {
                value = params.get("Parameter" + index);
                arguments[index] = ScaPropertyTypeJavaProcessor.stringToValue(parameterType.getCanonicalName(), value, method.getClass().getClassLoader());
            } catch (Exception e)
            {
                throw new BadParameterTypeException(index, value, parameterType);
            }
            index++;
        }

        return arguments;
    }

}