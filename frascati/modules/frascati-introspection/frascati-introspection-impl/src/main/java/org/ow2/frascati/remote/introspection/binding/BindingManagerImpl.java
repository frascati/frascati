/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.eclipse.stp.sca.SCAImplementation;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.bf.AbstractSkeleton;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;
import org.ow2.frascati.remote.introspection.util.SCAModelUtilItf;

/**
 *
 */
public class BindingManagerImpl implements BindingManagerItf
{
    /** The logger */
    public final static Logger logger = Logger.getLogger(BindingManagerImpl.class.getCanonicalName());

    @Reference(name = "fractalUtil")
    private FractalUtilItf fractalUtil;

    @Reference(name="scaModelUtil")
    private SCAModelUtilItf scaModelUtil;
    
    @Reference(name = "processors")
    private List<BindingProcessorItf> processors;

    public List<BindingProcessorItf> getProcessors()
    {
        return processors;
    }

    public void setProcessors(List<BindingProcessorItf> processors)
    {
        this.processors = processors;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#getBindingProcessor(java.lang.String)
     */
    public BindingProcessorItf getBindingProcessor(String kind) throws UnsupportedBindingException
    {
        logger.finest(BindingManagerImpl.class.getName()+" getBindingProcessor "+kind);
        String processorKind;
        for (BindingProcessorItf tmpBindingProcessor : processors)
        {
            processorKind = tmpBindingProcessor.getBindingKind().name();
            if (processorKind.equalsIgnoreCase(kind))
            {
                return tmpBindingProcessor;
            }
        }
        logger.info("[getBindingProcessor] binding kind " + kind + " is not supported");
        throw new UnsupportedBindingException(kind);
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#getBindingProcessor(org.ow2.frascati.remote.introspection.resources.BindingKind)
     */
    public BindingProcessorItf getBindingProcessor(BindingKind bindingKing) throws UnsupportedBindingException
    {
        if(bindingKing==null)
        {
            throw new UnsupportedBindingException("null");
        }
        return getBindingProcessor(bindingKing.name());
    }
    
    private BindingProcessorItf getReferenceBindingProcessor(Object stub) throws UnsupportedBindingComponentException
    {
        logger.fine("getReferenceBindingProcessor");
        for(BindingProcessorItf processor : this.processors)
        {
            if(processor.isProcessedReference(stub))
            {
                logger.fine("processor "+processor.getBindingKind().name()+" found for stub :"+stub);
                return processor;
            }
        }
        throw new UnsupportedBindingComponentException(stub);
    }
    
    private BindingProcessorItf getServiceBindingProcessor(Object skelton) throws UnsupportedBindingComponentException
    {
        logger.fine("getServiceBindingProcessor ");
        for(BindingProcessorItf processor : this.processors)
        {
            if(processor.isProcessedService(skelton))
            {
                logger.fine("processor "+processor.getBindingKind().name()+" found for skelton :"+skelton);
                return processor;
            }
        }
        throw new UnsupportedBindingComponentException(skelton);
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#getBindingProcessor(java.lang.Object)
     */
    public BindingProcessorItf getBindingProcessor(Object boundedObject) throws UnsupportedBindingComponentException
    {
        BindingProcessorItf bindingProcessor;
        try
        {
            bindingProcessor=this.getReferenceBindingProcessor(boundedObject);
            return bindingProcessor;
        }
        catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
        {
            //nothing to do try to find a serviceBindingProcessor
        }
        
        bindingProcessor=this.getServiceBindingProcessor(boundedObject);
        return bindingProcessor;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#getBindingBundle(org.objectweb.fractal.api.Interface)
     */
    public BindingBundle getBindingBundle(Interface itf) throws NoSuchInterfaceException 
    {
        Component itfOwner=itf.getFcItfOwner();
        logger.fine("getBindingBundle owner "+fractalUtil.getComponentName(itfOwner)+" interface "+itf.getFcItfName());
        BindingController ownerBindingController;
        try
        {
            ownerBindingController = Fractal.getBindingController(itfOwner);
        } catch (NoSuchInterfaceException e1)
        {
           logger.warning("Can't find BindingController for "+fractalUtil.getComponentName(itfOwner));
           throw e1;
        }
        
        BindingBundle bindingBundle=new BindingBundle();
        Object stubObject;
        try
        {
            stubObject = ownerBindingController.lookupFc(itf.getFcItfName());
            if(stubObject!=null)
            {
                if(stubObject instanceof Interface)
                {
                    stubObject=((Interface)stubObject).getFcItfOwner();
                }
                
                try
                {
                    BindingProcessorItf referenceBindingProcessor = this.getReferenceBindingProcessor(stubObject);
                    bindingBundle.setReferenceBinding(true);
                    bindingBundle.addBinding(referenceBindingProcessor, stubObject);
                    return bindingBundle;
                }
                catch (UnsupportedBindingComponentException e)
                {
                    logger.fine("Can not find ReferenceBindingProcessor for interface "+itf.getFcItfName());
                }
            }
        }
        catch (NoSuchInterfaceException e1)
        {
            logger.fine("Can't find "+itf.getFcItfName()+" when looking up in BindingController");
        }
        
        List<org.objectweb.fractal.api.Component> children;
        try
        {
            children = fractalUtil.getChildren(itfOwner);
        } catch (NoSuchInterfaceException e1)
        {
            logger.warning("Can't getchildren of "+fractalUtil.getComponentName(itfOwner));
            throw e1;
        }
        
        BindingProcessorItf bindingProcessor;
        Interface servant;
        for (org.objectweb.fractal.api.Component child : children)
        {
            try
            {
                bindingProcessor=this.getServiceBindingProcessor(child);
                BindingController childBindingController=fractalUtil.getBindingController(child);
                if(childBindingController!=null)
                {
                    servant=(Interface) childBindingController.lookupFc(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
                    if(servant==itf)
                    {
                        bindingBundle.addBinding(bindingProcessor, child);
                        logger.fine("BindingBunlde addBinding "+bindingProcessor.getBindingKind().name()+" "+fractalUtil.getComponentName(child));
                    }
                }
                else
                {
                    logger.fine("BindingBunlde no BindingController found for child "+fractalUtil.getComponentName(child));
                }
            }
            catch (UnsupportedBindingComponentException e)
            {
                logger.fine("Can not find ServiceBindingProcessor for component "+fractalUtil.getComponentName(child));
            }
        }
        return bindingBundle;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#bind(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void bind(Interface itf, String itfPath, Map<String, String> stringHints) throws NoBindingKindException, UnsupportedBindingException, BindingProcessorException
    {
        String kind = stringHints.get("kind");
        if (kind == null || "".equals(kind))
        {
            logger.severe("[BindingManagerImpl] No kind parameter can be found");
            throw new NoBindingKindException();
        }
        logger.fine("kind parameter found : " + kind);

        /** Look for the related BindingProcessor*/
        BindingProcessorItf bindingProcessor = getBindingProcessor(kind);
        Component itfOwner = itf.getFcItfOwner();

        /** stop the fractal component to bind */
        try
        {
            logger.fine("stop component "+fractalUtil.getComponentName(itfOwner));
            fractalUtil.stopComponent(itfOwner);
            logger.fine("stopped component "+fractalUtil.getComponentName(itfOwner));
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new BindingProcessorException(noSuchInterfaceException);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new BindingProcessorException(illegalLifeCycleException);
        }

        boolean isReference = fractalUtil.isSCAReference(itf);
        logger.fine("isReference");
        if (isReference)
        {
            this.bindReference(bindingProcessor, itfOwner, itf, itfPath, stringHints);
        }
        else
        {
            this.exportService(bindingProcessor, itfOwner, itf, stringHints);
        }

        /** restrat the fractal component */
        try
        {
            logger.fine("start component "+fractalUtil.getComponentName(itfOwner));
            fractalUtil.startComponent(itfOwner);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new BindingProcessorException(noSuchInterfaceException);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new BindingProcessorException(illegalLifeCycleException);
        }
    }

    private void bindReference(BindingProcessorItf bindingProcessor,Component itfOwner, Interface itf, String itfPath, Map<String,String> stringHints) throws BindingProcessorException
    {
        logger.fine("bindReference ");
        String interfaceName=itf.getFcItfName();
        String itfOwnerName = fractalUtil.getComponentName(itfOwner);
        
        InterfaceType itfType=(InterfaceType) itf.getFcItfType();
        boolean isCollectionReference=itfType.isFcCollectionItf();
        if(isCollectionReference)
        {
            interfaceName=fractalUtil.getNextInterfaceName(itfOwner, interfaceName);
            logger.fine("Next name for collection interface : "+interfaceName);
        }
        
        logger.fine("bind reference " + interfaceName + " own by : " + itfOwnerName);
        bindingProcessor.bindReference(itf, interfaceName, stringHints);
        
        if(isCollectionReference)
        {
            try
            {
                propagateCollectionBinding(itf, itfPath);
            } catch (Exception exception)
            {
                //TODO treat each case
                throw new BindingProcessorException(exception);
            }
        }
    }
    
    private void exportService(BindingProcessorItf bindingProcessor,Component itfOwner, Interface itf, Map<String,String> stringHints) throws BindingProcessorException
    {
        String interfaceName=itf.getFcItfName();
        String itfOwnerName = fractalUtil.getComponentName(itfOwner);
        logger.fine("export service " + interfaceName + " own by : " + itfOwnerName + ", hints:");
        bindingProcessor.exportService(itf, itf.getFcItfName(), stringHints);
    }

    private void propagateCollectionBinding(Interface collectionInterface, String collectionPath) throws NoSuchInterfaceException,IllegalBindingException, IllegalLifeCycleException
    {
        ProcessingContext processingContext = scaModelUtil.getProcessingContext(collectionPath);
        if (processingContext == null)
        {
            logger.severe("No processing context found for composite " + scaModelUtil.getTopLevelCompositeName(collectionPath));
            return;
        }

        org.eclipse.stp.sca.Component collectionParentComponent = scaModelUtil.getLastComponent(collectionPath);
        if (collectionParentComponent != null)
        {
            logger.fine("The multiple reference " + collectionInterface.getFcItfName() + " is not a promoted reference, no need to propagate the binding");
            return;
        }

        org.eclipse.stp.sca.Composite collectionParentComposite = scaModelUtil.getLastComposite(collectionPath);
        if (collectionParentComposite == null)
        {
            logger.severe("Can't retrieve composite in path " + collectionPath);
            return;
        }

        org.eclipse.stp.sca.Reference collectionReference = scaModelUtil.findReference(collectionParentComposite.getReference(),collectionInterface.getFcItfName());
        propagateCollectionBinding(processingContext, collectionReference, collectionInterface.getFcItfOwner(), collectionParentComposite);
    }

    private void propagateCollectionBinding(ProcessingContext processingContext, org.eclipse.stp.sca.Reference collectionReference,
            org.objectweb.fractal.api.Component collectionReferenceOwner, org.eclipse.stp.sca.Composite collectionReferenceComposite)
            throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException
    {

        String collectionReferenceName = collectionReference.getName();
        logger.fine("collectionReferenceName : " + collectionReferenceName);
        String lastCollectionReferenceName = fractalUtil.getLastInterfaceCollectionName(collectionReferenceOwner, collectionReferenceName);
        logger.fine("lastCollectionReferenceName : " + lastCollectionReferenceName);
        String promote = collectionReference.getPromote();
        logger.fine("promote : " + promote);
        String[] promotePath = promote.split("/");
        String promotedReferenceOwnerName = promotePath[0];
        logger.fine("promotedReferenceOwnerName : " + promotedReferenceOwnerName);
        String promotedReferenceCollectionName = promotePath[1];
        logger.fine("promotedReferenceCollectionName : " + promotedReferenceCollectionName);

        org.objectweb.fractal.api.Component promotedReferenceOwner = fractalUtil.getSubComponent(collectionReferenceOwner, promotedReferenceOwnerName);
        logger.fine("promotedReferenceOwner : " + Fractal.getNameController(promotedReferenceOwner).getFcName());
        String nextPromotedReferenceName = fractalUtil.getNextInterfaceName(promotedReferenceOwner, promotedReferenceCollectionName);
        logger.fine("nextPromotedReferenceName : " + nextPromotedReferenceName);

        BindingController promotedReferenceOwnerBindingController = Fractal.getBindingController(promotedReferenceOwner);
        ContentController sourceReferenceOwnerContentController = Fractal.getContentController(collectionReferenceOwner);
        Object sourceItf = sourceReferenceOwnerContentController.getFcInternalInterface(lastCollectionReferenceName);
        promotedReferenceOwnerBindingController.bindFc(nextPromotedReferenceName, sourceItf);

        org.eclipse.stp.sca.Component promotedReferenceComponent = scaModelUtil.findComponent(collectionReferenceComposite, promotedReferenceOwnerName);
        logger.fine("promotedReferenceComponent : " + promotedReferenceComponent.getName());

        if (!(promotedReferenceComponent.getImplementation() instanceof SCAImplementation))
        {
            org.eclipse.stp.sca.ComponentReference promotedReference = scaModelUtil.findComponentReference(promotedReferenceComponent,promotedReferenceCollectionName);
            logger.fine("Binding propagation finish, owner : " + promotedReferenceComponent.getName() + ", reference : " + promotedReference.getName());
        } else
        {
            org.eclipse.stp.sca.Composite promotedReferenceComposite = processingContext.getData(promotedReferenceComponent.getImplementation(),org.eclipse.stp.sca.Composite.class);
            org.eclipse.stp.sca.Reference promotedReference = this.scaModelUtil.findReference(promotedReferenceComposite, promotedReferenceCollectionName);
            propagateCollectionBinding(processingContext, promotedReference, promotedReferenceOwner, promotedReferenceComposite);
        }

    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#unbind(org.objectweb.fractal.api.Interface, java.lang.String, int)
     */
    public void unbind(Interface itf,String itfPath, int position) throws BindingProcessorException,UnsupportedBindingComponentException
    {
        logger.fine("unbind itf "+itf.getFcItfName()+" path "+itfPath+" position "+position);
        BindingBundle interfaceBindingBundle;
        try
        {
            interfaceBindingBundle=this.getBindingBundle(itf);
        } catch (NoSuchInterfaceException e)
        {
            throw new BindingProcessorException(e);
        } 
        
        List<Object> boundedObjects=interfaceBindingBundle.getBoundedObjects();
        Object boundedObject;
        try
        {
            boundedObject=boundedObjects.get(position);
        }
        catch(ArrayIndexOutOfBoundsException aoobe)
        {
            throw new BindingProcessorException("Can not find Binding at position "+position);
        }
        
        /** stop the fractal component to bind */
        try
        {
            fractalUtil.stopComponent(itf.getFcItfOwner());
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new BindingProcessorException(noSuchInterfaceException);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new BindingProcessorException(illegalLifeCycleException);
        }
        
        BindingProcessorItf processor=this.getBindingProcessor(boundedObject);
        if(interfaceBindingBundle.isReferenceBinding())
        {
            InterfaceType itfType=(InterfaceType) itf.getFcItfType();
            if(itfType.isFcCollectionItf())
            { 
                try
                {
                    itf=propagateUnbind(itf);
                } catch (Exception e)
                {
                   throw new BindingProcessorException(e);   
                }
            }
            processor.unbindReference(itf.getFcItfOwner(), itf.getFcItfName(), boundedObject);
        }
        else
        {
            processor.unexportService(itf.getFcItfOwner(), itf.getFcItfName(), boundedObject);
        }
        
        /** restart the fractal component to bind */
        try
        {
            fractalUtil.stopComponent(itf.getFcItfOwner());
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new BindingProcessorException(noSuchInterfaceException);
        } catch (IllegalLifeCycleException illegalLifeCycleException)
        {
            throw new BindingProcessorException(illegalLifeCycleException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#unbind(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void unbind(Interface itf,String itfPath, Map<String,String> stringHints) throws NoBindingKindException, UnsupportedBindingException, BindingProcessorException
    {
        logger.warning(" unbind(Interface itf,String itfPath, Map<String,String> stringHints) not implemented yet");
    }
    
    private Interface propagateUnbind(Interface itf) throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException
    {
        logger.fine(" propagateUnbind "+itf.getFcItfName());
        Interface clientInterface=itf;
        List<Interface> clientInterfaces=fractalUtil.getClientInterfaces(itf);
        while(!clientInterfaces.isEmpty())
        {
            clientInterface=(Interface) clientInterfaces.get(0);
            clientInterfaces=fractalUtil.getClientInterfaces(clientInterface);
        }
        logger.fine("deepest clientInterface "+clientInterface.getFcItfName());
        
        Interface serverInterface = (Interface) fractalUtil.getServerInterface(clientInterface);
        org.objectweb.fractal.api.Component clientOwner;
        BindingController clientOwnerBindingController;

        while (serverInterface != null)
        {
            clientOwner = clientInterface.getFcItfOwner();
            clientOwnerBindingController = Fractal.getBindingController(clientOwner);
            fractalUtil.stopComponent(clientOwner);
            clientOwnerBindingController.unbindFc(clientInterface.getFcItfName());
            logger.info("unbind " + clientInterface.getFcItfName() + " from " + fractalUtil.getComponentName(clientOwner));
            fractalUtil.startComponent(clientOwner);
            clientInterface = serverInterface;
            serverInterface = (Interface) fractalUtil.getServerInterface(clientInterface);
            String serverBindingName=fractalUtil.getComponentName(serverInterface.getFcItfOwner())+"/"+serverInterface.getFcItfName();
            try
            {
                BindingProcessorItf processor= this.getBindingProcessor(serverInterface.getFcItfOwner());
                logger.fine(serverBindingName+" is a "+processor.getBindingKind()+" binding, break");
                break;
            } catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
            {
                logger.fine(serverBindingName+" is a fractal interface, keep going");
            }
        }
        logger.info("propagateUnbind "+clientInterface.getFcItfName());
        return clientInterface;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#setBindingAttribute(org.objectweb.fractal.api.Interface, java.lang.String, java.lang.String, java.lang.String)
     */
    public void setBindingAttribute(Interface itf, int position, String attribute, String newValue) throws BindingProcessorException,
            UnsupportedBindingComponentException
    {
        logger.info("setAttribute itf "+itf.getFcItfName()+" position "+position+" attribute "+attribute+" newValue "+newValue);
        BindingBundle interfaceBindingBundle;
        try
        {
            interfaceBindingBundle=this.getBindingBundle(itf);
        } catch (NoSuchInterfaceException e)
        {
            throw new BindingProcessorException(e);
        } 
        
        List<Object> boundedObjects=interfaceBindingBundle.getBoundedObjects();
        Object boundedObject;
        try
        {
            boundedObject=boundedObjects.get(position);
        }
        catch(ArrayIndexOutOfBoundsException aoobe)
        {
            throw new BindingProcessorException("Can not find Binding at position "+position);
        }
        
        BindingProcessorItf processor=this.getBindingProcessor(boundedObject);
        processor.setAttribute(boundedObject, attribute, newValue);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#setBindingAttribute(org.objectweb.fractal.api.Interface, java.util.Map, java.lang.String, java.lang.String)
     */
    public void setBindingAttribute(Interface itf, Map<String, String> stringHints, String attribute, String newValue) throws NoBindingKindException,
            UnsupportedBindingException, BindingProcessorException
    {
        logger.warning(" setBindingAttribute(Interface itf, Map<String, String> stringHints, String attribute, String newValue) not implemented yet");
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.BindingManagerItf#getBindingInterfaces(org.objectweb.fractal.api.Interface)
     */
    public List<Interface> getBindingInterfaces(Interface itf)
    {
        String interfaceName = itf.getFcItfName();
        Component owner = itf.getFcItfOwner();
        String ownerName = fractalUtil.getComponentName(owner);
        logger.finest("getBindingInterfaces owner : " + ownerName + " interface : " + interfaceName);

        List<Interface> clientInterfaces = new ArrayList<Interface>();
        List<org.objectweb.fractal.api.Component> children = null;
        try
        {
            children = fractalUtil.getChildren(owner);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            logger.warning("NoSuchInterfaceException when trying to get children of component : " + fractalUtil.getComponentName(owner));
            return clientInterfaces;
        }

        BindingController childBindingController = null;
        Interface servant = null, client;
        for (Component child : children)
        {
            try
            {
                childBindingController = Fractal.getBindingController(child);
            } catch (NoSuchInterfaceException noSuchInterfaceException)
            {
                logger.fine("Can't get BindingController for component " + fractalUtil.getComponentName(child));
                continue;
            }

            try
            {
                servant = (Interface) childBindingController.lookupFc(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
            } catch (NoSuchInterfaceException noSuchInterfaceException)
            {
                logger.fine("Can't find " + AbstractSkeleton.SERVANT_CLIENT_ITF_NAME + " interface on component " + fractalUtil.getComponentName(child));
                continue;
            }

            if (servant != itf)
            {
                logger.fine(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME + " interface " + servant.getFcItfName() + " on component " + fractalUtil.getComponentName(child)
                        + " is not bound to " + itf.getFcItfName());
                continue;
            }

            try
            {
                client = (Interface) child.getFcInterface(AbstractSkeleton.SERVANT_CLIENT_ITF_NAME);
                logger.fine(fractalUtil.getComponentName(client.getFcItfOwner()) + "/" + client.getFcItfName() + " is client interface for " + itf.getFcItfName());
                clientInterfaces.add(client);
            } catch (NoSuchInterfaceException noSuchInterfaceException)
            {
                logger.fine("Can't find " + AbstractSkeleton.SERVANT_CLIENT_ITF_NAME + " interface on component " + fractalUtil.getComponentName(child));
                continue;
            }
        }

        return clientInterfaces;
    }
}
