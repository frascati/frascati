/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.bf.BindingFactoryException;
import org.objectweb.fractal.bf.connectors.common.SkeletonContentAttributes;
import org.objectweb.fractal.bf.connectors.common.StubContentAttributes;
import org.objectweb.fractal.util.AttributesHelper;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;


/**
 * BindingProcessor for BindingKing that delegate binding operation to BindingFactory
 */
public abstract class AbstractBindingProcessor implements BindingProcessorItf
{
    private static final Logger logger=Logger.getLogger(AbstractBindingProcessor.class.getName());
    
    public static final String CLASSLOADER = "classloader";
    public static final String PLUGIN_ID = "plugin.id";

    @Reference(name = "binding-factory")
    protected BindingFactory bindingFactory;
    
    @Reference(name="fractalUtil")
    protected FractalUtilItf fractalUtil;
    
    /**
     * BindingKind of this processor
     */
    private BindingKind bindingKind;

    
    /**
     * The suffix a of skelton Object name proceed by this BindingProcessor
     */
    private String skeltonSuffix;
    
    /**
     * The suffix a of stub Object name proceed by this BindingProcessor
     */
    private String stubSuffix;
    
    
    /**
     * SkeletonContentAttributes class proceed by this BindingProcessor
     */
    private Class<? extends SkeletonContentAttributes> skeltonClass;
    
    /**
     * StubContentAttributes class proceed by this BindingProcessor
     */
    private Class<? extends StubContentAttributes> stubClass;
    
    /**
     * @param bindingKind BindingKind of this processor
     * @param skeltonSuffix The suffix a of skelton Object name proceed by this BindingProcessor
     * @param stubSuffix The suffix a of stub Object name proceed by this BindingProcessor
     * @param skeltonClass SkeletonContentAttributes class proceed by this BindingProcessor
     * @param stubClass StubContentAttributes class proceed by this BindingProcessor
     */
    public AbstractBindingProcessor(BindingKind bindingKind, String skeltonSuffix, String stubSuffix, Class<? extends SkeletonContentAttributes> skeltonClass, Class<? extends StubContentAttributes> stubClass)
    {
        this.bindingKind=bindingKind;
        this.skeltonSuffix=skeltonSuffix;
        this.stubSuffix=stubSuffix;
        this.skeltonClass=skeltonClass;
        this.stubClass=stubClass;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#getBindingKind()
     */
    public BindingKind getBindingKind()
    {
        return bindingKind;
    }
    
    /**
     * @return the plugin id need by the BindingFactory to perform binding
     */
    public String getPluginId()
    {
        return getBindingKind().name().toLowerCase();
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#bindReference(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void bindReference(Interface itf, String interfaceName, Map<String,String> stringHints) throws BindingProcessorException
    {
        logger.fine(this.getBindingKind().name()+" bindReference interface : "+itf.getFcItfName()+" interfaceName "+interfaceName);
        Map<String, Object> hints=processHints(itf, stringHints);
        try
        {
            bindingFactory.bind(itf.getFcItfOwner(), interfaceName, hints);
        }
        catch (BindingFactoryException bindingFactoryException)
        {
           throw new BindingProcessorException(bindingFactoryException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#exportService(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void exportService(Interface itf, String interfaceName, Map<String,String> stringHints) throws BindingProcessorException
    {
        logger.fine(this.getBindingKind().name()+" exportService interface : "+itf.getFcItfName()+" interfaceName "+interfaceName);
        Map<String, Object> hints=processHints(itf, stringHints);
        try
        {
            bindingFactory.export(itf.getFcItfOwner(), interfaceName, hints);
        }
        catch (BindingFactoryException bindingFactoryException)
        {
           throw new BindingProcessorException(bindingFactoryException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#unbindReference(org.objectweb.fractal.api.Component, java.lang.String, java.lang.Object)
     */
    public void unbindReference(Component owner, String interfaceName, Object boundedObject) throws BindingProcessorException
    {
        logger.fine(this.getBindingKind().name()+" unbindReference owner : "+fractalUtil.getComponentName(owner)+" interfaceName "+interfaceName);
        try
        {
            bindingFactory.unbind(owner, interfaceName, (Component) boundedObject);
        }
        catch (BindingFactoryException bindingFactoryException)
        {
            bindingFactoryException.printStackTrace();
           throw new BindingProcessorException(bindingFactoryException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#unexportService(org.objectweb.fractal.api.Component, java.lang.String, java.lang.Object)
     */
    public void unexportService(Component owner, String interfaceName, Object boundedObject) throws BindingProcessorException
    {
        logger.fine(this.getBindingKind().name()+" unexportService owner : "+fractalUtil.getComponentName(owner)+" interfaceName "+interfaceName);
        try
        {
            bindingFactory.unexport(owner, (Component) boundedObject, new HashMap<String, Object>());
        }
        catch (BindingFactoryException bindingFactoryException)
        {
           throw new BindingProcessorException(bindingFactoryException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#setAttribute(java.lang.Object, java.lang.String, java.lang.String)
     */
    public void setAttribute(Object boundedObject, String attribute, String newValue) throws BindingProcessorException 
    {
        try
        {
            AttributesHelper attributeHelper = new AttributesHelper((Component) boundedObject);
            attributeHelper.setAttribute(attribute, newValue);
        }
        catch(NoSuchElementException noSuchElementException)
        {
            throw new BindingProcessorException(noSuchElementException);
        }
        catch(UnsupportedOperationException unsupportedOperationException)
        {
            throw new BindingProcessorException(unsupportedOperationException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            throw new BindingProcessorException(illegalArgumentException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#getAttributes(java.lang.Object)
     */
    public List<BindingAttribute> getAttributes(Object boundedObject) throws BindingProcessorException
    {
        try
        {
            AttributesHelper attributeHelper = new AttributesHelper((Component) boundedObject);
            List<BindingAttribute> bindingAttributes=new ArrayList<BindingAttribute>();
            BindingAttribute bindingAttribute;
            String attributeName;
            Object attributeValue;
            Class<?> attributeClazz;
            for (Object attrName : attributeHelper.getAttributesNames())
            {
                attributeName=(String) attrName;
                attributeClazz=attributeHelper.getAttributeType(attributeName);
                attributeValue=attributeHelper.getAttribute(attributeName);
                bindingAttribute=new BindingAttribute(attributeName, attributeClazz, attributeValue);
                bindingAttributes.add(bindingAttribute);
            }
            return bindingAttributes;
        }
        catch(NoSuchElementException noSuchElementException)
        {
            throw new BindingProcessorException(noSuchElementException);
        }
        catch(UnsupportedOperationException unsupportedOperationException)
        {
            throw new BindingProcessorException(unsupportedOperationException);
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            throw new BindingProcessorException(illegalArgumentException);
        }
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#isProcessedService(java.lang.Object)
     */
    public boolean isProcessedService(Object stub)
    {
        return isProcessedComponent(stub, this.skeltonSuffix, this.skeltonClass);        
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#isProcessedReference(java.lang.Object)
     */
    public boolean isProcessedReference(Object skelton)
    {
        return isProcessedComponent(skelton, this.stubSuffix, this.stubClass);
    }
    
    /**
     * Return true if componentObject's AttributeController isInstance of attributeClass or if componentObject's name end with suffix
     * 
     * @param componentObject the fractal component to resolve
     * @param suffix this.skeltonSuffix or this.stubSuffix
     * @param attributeClass skeltonClass or stubClass
     * @return true if the componentObject is a fractal component that AttributeController instance of attributeClass or name end with suffix, otherwise false
     */
    private boolean isProcessedComponent(Object componentObject, String suffix, Class<?> attributeClass)
    {
        logger.finest("isProcessedComponent "+this.getBindingKind().name()+" "+suffix+" "+attributeClass);
        Component component;
        try
        {
            component=(Component) componentObject;
        }
        catch(ClassCastException classCastException)
        {
            return false;
        }
        
        String componentName=fractalUtil.getComponentName(component);
        boolean isValidName=false;
        if(suffix!=null && !"".equals(suffix))
        {
            isValidName=componentName.endsWith(suffix);
            logger.finest(componentName+ " endsWith "+suffix+" "+isValidName);
        }
        logger.finest(componentName+" is valid : "+isValidName);
        
        AttributeController attributeController = fractalUtil.getAttributeController(component);
        boolean isValidAttribute=false;
        if(attributeClass!=null && attributeController!=null)
        {
            isValidAttribute=attributeClass.isInstance(attributeController);
            logger.finest(attributeController.getClass().getName()+" is valid : "+isValidAttribute);
            
        }
        logger.finest(componentObject.getClass().getName()+" is valid :"+isValidAttribute);
        
        return isValidName || isValidAttribute;
    }
    
    /**
     * Util method to finalize hints generation
     * 
     * @param itf interface to process binding to
     * @param params string hints of the binding to proceed 
     * @return the hints to pass to BindingFactory
     */
    private Map<String, Object> processHints(Interface itf,Map<String, String> params)
    {
        Map<String, Object> hints = extractHints(params);
        hints.put(PLUGIN_ID, this.getPluginId());
        hints.put(CLASSLOADER, itf.getClass().getClassLoader());
        return hints;
    }
    
    /**
     * Extract binding hints from Map<String,String>
     * 
     * @param params string hints of the binding to proceed 
     * @return the hints to pass to BindingFactory less CLASSLOADER and PLUGIN_ID hints
     */
    protected abstract Map<String, Object> extractHints(Map<String, String> params);

}
