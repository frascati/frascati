/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.processor.ScaBindingScaInvocationHandler;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;

/**
 *
 */
public class SCABindingProcessor implements BindingProcessorItf
{
    public final static String URI="uri";
    
    @Reference(name="fractalUtil")
    protected FractalUtilItf fractalUtil;
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#getBindingKind()
     */
    public BindingKind getBindingKind()
    {
        return BindingKind.SCA;
    }
    
    /** The logger */
    private final Logger logger = Logger.getLogger(SCABindingProcessor.class.getName());

    /**
     * Wrapper for java.util.logging.Logger.entering
     * 
     * @param method
     * @param params
     */
    private void logEntering(String method, Object... params)
    {
        logger.entering(SCABindingProcessor.class.getName(), method, params);
    }

    /**
     * Wrapper for java.util.logging.Logger.exiting
     * 
     * @param method
     * @param params
     */
    private void logExiting(String method, Object... params)
    {
        logger.exiting(SCABindingProcessor.class.getName(), method, params);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#isProcessedService(org.objectweb.fractal.api.Component)
     */
    public boolean isProcessedService(Object skelton)
    {
        /**No service is proceed by SCA binding*/
        return false;
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#isProcessedReference(org.objectweb.fractal.api.Component)
     */
    public boolean isProcessedReference(Object stub)
    {
        //SCA binding are just java Proxy
        logger.fine(stub +" is SCA : "+(this.getScaBindingScaInvocationHandler(stub)!=null));
        return this.getScaBindingScaInvocationHandler(stub)!=null;
    }
    
    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#bindReference(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void bindReference(Interface reference, String referenceName, Map<String, String> stringHints) throws BindingProcessorException
    {
        logEntering("SCABindingProcessor", reference.getFcItfName(), referenceName, stringHints);

        /** Check that hints contains URI attribute */
        String serviceURI = stringHints.get(SCABindingProcessor.URI);
        if(serviceURI==null || "".equals(serviceURI))
        {
            throw new BindingProcessorException(SCABindingProcessor.class.getName()+" uri attribute is missing to bind reference : "+reference.getFcItfName());
        }

        logger.fine("[SCABindingProcessor] serviceURI : " + serviceURI);

        /** Get service Fractal interface */
        Interface service = null;
        try
        {
            service = fractalUtil.getInterface(serviceURI);
        }
        catch (NoSuchInterfaceException e)
        {
            throw new BindingProcessorException(SCABindingProcessor.class.getName()+" Can not find fractal interface for service " + serviceURI);
        }

        /** Get the name of the reference owner */
        Component referenceOwner = reference.getFcItfOwner();
        String referenceOwnerName= fractalUtil.getComponentName(referenceOwner);
        logger.fine("[SCABindingProcessor] referenceOwner : " + referenceOwnerName);

        /** Get BindingController of the reference owner */
        BindingController bindingController;
        try
        {
            bindingController = Fractal.getBindingController(referenceOwner);
        } catch (NoSuchInterfaceException noSuchInterfaceException)
        {
            throw new BindingProcessorException(SCABindingProcessor.class.getName()+" Can not get BindingController for fractal component "+ referenceOwnerName);
        }

        /** ClassLoader of the targeted service */
        ClassLoader serviceClassLoader = service.getClass().getClassLoader();
        InterfaceType referenceInterfaceType = (InterfaceType) reference.getFcItfType();
        String referenceInterfaceClassName = referenceInterfaceType.getFcItfSignature();
        /** Get Class instance of the service */
        Class<?> referenceClass;
        try
        {
            referenceClass = serviceClassLoader.loadClass(referenceInterfaceClassName);
        } catch (ClassNotFoundException e1)
        {
            throw new BindingProcessorException(SCABindingProcessor.class.getName()+" Can not load class " + referenceInterfaceClassName + " from classLoader of service : "+ service.getFcItfName());
        }

        logger.fine("[SCABindingProcessor] referenceInterfaceClassName : " + referenceInterfaceClassName);

        /** create the invocationHandler */
        ScaBindingScaInvocationHandler scaBindingScaInvocationHandler = new ScaBindingScaInvocationHandler();
        /** set the service to invoke */
        scaBindingScaInvocationHandler.setDelegate(service);
        /** set URI of the service */
        scaBindingScaInvocationHandler.setBindingURI(serviceURI);
        /** create the proxy object */
        Object proxy = Proxy.newProxyInstance(serviceClassLoader, new Class<?>[] { referenceClass }, scaBindingScaInvocationHandler);

        /** bind proxy object to the reference */
        try
        {
            bindingController.bindFc(referenceName, proxy);
        }
        catch (Exception exception)
        {
            throw new BindingProcessorException(exception);
        }
        logExiting("SCABindingProcessor");
        
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#exportService(org.objectweb.fractal.api.Interface, java.lang.String, java.util.Map)
     */
    public void exportService(Interface itf, String interfaceName, Map<String, String> stringHints) throws BindingProcessorException
    {
        throw new BindingProcessorException("Can not apply SCA binding on a service (serviceName : "+itf.getFcItfName()+")");
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#unbindReference(org.objectweb.fractal.api.Component, java.lang.String, java.lang.Object)
     */
    public void unbindReference(Component owner, String interfaceName, Object boundedObject) throws BindingProcessorException
    {
      
      if(getScaBindingScaInvocationHandler(boundedObject)==null)
      {
          throw new BindingProcessorException("Can not find SCA binding on reference "+interfaceName);
      }

      BindingController bindingController;
        try
        {
            bindingController = Fractal.getBindingController(owner);
            bindingController.unbindFc(interfaceName);
        } catch (Exception e)
        {
            throw new BindingProcessorException(e);
        }
          
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#unexportService(org.objectweb.fractal.api.Component, java.lang.String, java.lang.Object)
     */
    public void unexportService(Component owner, String interfaceName, Object boundedObject) throws BindingProcessorException
    {
        throw new BindingProcessorException("Can not apply SCA binding on a service (serviceName : "+interfaceName+")");
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#setAttribute(java.lang.Object, java.lang.String, java.lang.String)
     */
    public void setAttribute(Object boundedObject, String attribute, String newValue) throws BindingProcessorException
    {
        logger.info(SCABindingProcessor.class.getName()+" setAttribute "+attribute+" newValue "+newValue);
        ScaBindingScaInvocationHandler scaBindingScaInvocationHandler=getScaBindingScaInvocationHandler(boundedObject);
        if(scaBindingScaInvocationHandler==null)
        {
            throw new BindingProcessorException("Can not find SCA binding on reference");
        }
        
        if(!SCABindingProcessor.URI.equals(attribute))
        {
            throw new BindingProcessorException("Attribute "+attribute+" is not valid on SCA binding");
        }
        
        Interface service = null;
        try
        {
            service = fractalUtil.getInterface(newValue);
        } catch (NoSuchInterfaceException e)
        {
            throw new BindingProcessorException("Can not find service "+newValue);
        }

        scaBindingScaInvocationHandler.setBindingURI(newValue);
        scaBindingScaInvocationHandler.setDelegate(service);
    }

    /**
     * @see org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf#getAttributes(java.lang.Object)
     */
    public List<BindingAttribute> getAttributes(Object boundedObject) throws BindingProcessorException
    {
        ScaBindingScaInvocationHandler scaBindingScaInvocationHandler=getScaBindingScaInvocationHandler(boundedObject);
        if(scaBindingScaInvocationHandler==null)
        {
            throw new BindingProcessorException("Can not find SCA binding on reference");
        }
        List<BindingAttribute> bindingAttributes=new ArrayList<BindingAttribute>();
        BindingAttribute bindingAttribute=new BindingAttribute(SCABindingProcessor.URI, String.class, scaBindingScaInvocationHandler.getBindingURI());
        bindingAttributes.add(bindingAttribute);
        return bindingAttributes;
    }
    
    /**
     * Determine if boundedObject is ScaBindingScaProxy
     * 
     * @param boundedObject the Object bound to fractal interface
     * @return ScaBindingScaInvocationHandler if boundedObject is a proxy for SCa binding, null otherwise
     */
    private ScaBindingScaInvocationHandler getScaBindingScaInvocationHandler(Object boundedObject)
    {
        if(!Proxy.isProxyClass(boundedObject.getClass()) || !(Proxy.getInvocationHandler(boundedObject) instanceof ScaBindingScaInvocationHandler))
        {
            return null;
        }
        return (ScaBindingScaInvocationHandler) Proxy.getInvocationHandler(boundedObject);
    }
 
}
