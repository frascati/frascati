/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.remote;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.DeploymentTest;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.RemoteScaDomainTest;
import org.ow2.frascati.remote.introspection.binding.BindingManagerItf;
import org.ow2.frascati.remote.introspection.binding.BindingManagerTest;
import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;
import org.ow2.frascati.remote.introspection.util.FractalUtilTest;
import org.ow2.frascati.remote.introspection.util.ResourceUtilItf;
import org.ow2.frascati.remote.introspection.util.ResourceUtilTest;
import org.ow2.frascati.remote.introspection.util.SCAModelUtilItf;
import org.ow2.frascati.remote.introspection.util.SCAModelUtilTest;
import org.ow2.frascati.util.FrascatiException;

/**
 * Order of the Class in the suite depending of the dependencies
 */
@RunWith(Suite.class)
@SuiteClasses({DeploymentTest.class,RemoteScaDomainTest.class, FractalUtilTest.class,SCAModelUtilTest.class,BindingManagerTest.class,ResourceUtilTest.class})
public class IntrospectionTestSuite
{
    /** REST binding URI */
    private static final String BINDING_URI = AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;

    private static final String RESOURCES_FOLDER="target/test-classes/";
    
    private static FraSCAti frascati;
    
    /** The Frascati Domain */
    private static RemoteScaDomain domain;

    /** FraSCAti deployment module **/
    private static Deployment deployment;
    
    private static FractalUtilItf fractalUtil;
    
    private static ResourceUtilItf resourceUtil;
    
    private static SCAModelUtilItf scaModelUtil;
    
    private static BindingManagerItf bindingManager;
    
    /**
     * Create a new FraSCAti platform and initialize the domain
     * 
     * @throws FrascatiException
     */
    @BeforeClass
    public static void init() throws FrascatiException
    {
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTRest");

        // launch FraSCAti
        frascati=FraSCAti.newFraSCAti();
        
        // Get services by using the Rest binding
        domain = JAXRSClientFactory.create(BINDING_URI + "/introspection", RemoteScaDomain.class);
        deployment = JAXRSClientFactory.create(IntrospectionTestSuite.BINDING_URI + "/deploy", Deployment.class);
        
        org.objectweb.fractal.api.Component remoteComponent = getSubComponent("org.ow2.frascati.FraSCAti", "remote");
        
        fractalUtil=frascati.getService(remoteComponent, "fractalUtil", FractalUtilItf.class);
        assertNotNull(fractalUtil);
        resourceUtil=frascati.getService(remoteComponent, "resourceUtil", ResourceUtilItf.class);
        assertNotNull(resourceUtil);
        scaModelUtil=frascati.getService(remoteComponent, "scaModelUtil", SCAModelUtilItf.class);
        assertNotNull(scaModelUtil);
        bindingManager=frascati.getService(remoteComponent, "bindingManager", BindingManagerItf.class);
        assertNotNull(scaModelUtil);
    }
    
    @AfterClass
    public static void finish() throws FrascatiException
    {
        frascati.close();
    }
    
    public static RemoteScaDomain getDomain()
    {
        return domain;
    }

    public static Deployment getDeployment()
    {
        return deployment;
    }

    public static FractalUtilItf getFractalUtil()
    {
        return fractalUtil;
    }

    public static ResourceUtilItf getResourceUtil()
    {
        return resourceUtil;
    }

    public static SCAModelUtilItf getScaModelUtil()
    {
        return scaModelUtil;
    }

    public static BindingManagerItf getBindingManager()
    {
        return bindingManager;
    }

    public static File getResource(String resourceName)
    {
        String resourcePath=RESOURCES_FOLDER+resourceName;
        File resource=new File(resourcePath);
        if(!resource.exists())
        {
            resourcePath="src/test/resources/"+resourceName;
            resource=new File(resourcePath);
        }
        return resource;
    }
    
    /** Helper methods */
    public static StringList deployContribution(String contributionName)
    {
        File contribution=getResource(contributionName+".zip");
        String contrib = null;
        try
        {
            contrib = FileUtil.getStringFromFile(contribution);
        } catch (IOException e)
        {
            fail("Cannot read the contribution "+contributionName);
        }
        
        StringList deployedComponents=IntrospectionTestSuite.deployment.deployContribution(contrib);
        return deployedComponents;
    }
    
    public static String deployComposite(String compositeFinaleName, String compositeName)
    {
        File composite=getResource(compositeFinaleName+".jar");
        String compositeFile = null;
        try
        {
            compositeFile = FileUtil.getStringFromFile(composite);
        }
        catch (IOException e)
        {
            fail("Cannot read the composite "+compositeFinaleName);
        }
        
        String deployedComponent=IntrospectionTestSuite.deployment.deployComposite(compositeName,compositeFile);
        assertEquals(deployedComponent, compositeName);
        return deployedComponent;
    }
    
    public static void undeployComposite(String compositePath)
    {
        assertComponentExist(compositePath);
        deployment.undeployComposite(compositePath);
        assertComponentNotExist(compositePath);
    }
    
    public static Component assertComponentExist(String componentPath)
    {
       Component component = IntrospectionTestSuite.domain.getComponent(componentPath);
       assertNotNull(component);
       assertEquals(componentPath, component.getPath());
       return component;
    }
    
    public static void assertComponentNotExist(String componentPath)
    {
        try
        {
            IntrospectionTestSuite.domain.getComponent(componentPath);
            fail("An exception was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=getResponseMessage(exceptionResponse);
            assertEquals("No component found for for id : "+componentPath, response);
        }
    }
    
    public static Port assertPortExist(String portPath)
    {
       Port port=IntrospectionTestSuite.domain.getInterface(portPath);
       assertNotNull(port);
       assertEquals(portPath, port.getPath());
       return port;
    }
    
    public static Port assertPortExist(String portPath,Multiplicity  multiplicity)
    {
       Port port=IntrospectionTestSuite.domain.getInterface(portPath);
       assertNotNull(port);
       assertEquals(portPath, port.getPath());
       assertEquals(multiplicity, port.getMutiplicity());
       return port;
    }
    
    public static void assertPortNotExist(String portPath)
    {
        try
        {
            IntrospectionTestSuite.domain.getInterface(portPath);
            fail("An exception was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=getResponseMessage(exceptionResponse);
            assertEquals("No interface found for for id : "+portPath, response);
        }
    }
    
    private static Binding getBinding(String portPath,BindingKind bindingKind)
    {
        Port port =assertPortExist(portPath);
        List<Binding> bindings = port.getBindings();

        Binding binding=null;
        for (Binding tmpBinding : bindings)
        {
            if(tmpBinding.getKind().equals(bindingKind))
            {
                binding=tmpBinding;
            }
        }
        return binding;
    }
    
    public static Binding assertBindingExist(String portPath,BindingKind bindingKind)
    {
        Binding binding=getBinding(portPath, bindingKind);
        assertNotNull(binding);
        return binding;
    }
    
    public static void assertBindingNotExist(String portPath,BindingKind bindingKind)
    {
        Binding binding=getBinding(portPath, bindingKind);
        assertNull(binding);
    }
    
    public static String getResponseMessage(Response response)
    {
        try
        {
            return IOUtils.toString((InputStream)response.getEntity());
        }
        catch (IOException e)
        {
            return null;
        }
    }
    
    /**
     * Util method to get remote component from frascati
     * Extracted from the init method for esthetic concern
     * 
     * @param componentName
     * @param subComponentName
     * @return
     */
    private static org.objectweb.fractal.api.Component getSubComponent(String componentName,String subComponentName)
    {
        org.objectweb.fractal.api.Component component = null;
        try
        {
            component = frascati.getComposite(componentName);
        } catch (FrascatiException e)
        {
            fail("Cannot get composite named "+componentName);
        }
        assertNotNull(component);
        
        ContentController contentController = null;
        try
        {
            contentController=Fractal.getContentController(component);
        }
        catch(NoSuchInterfaceException noSuchInterfaceException)
        {
            fail("Cannot get ContentController of the component "+componentName);
        }
        assertNotNull(contentController);
        
        org.objectweb.fractal.api.Component subComponent = null;
        try
        {
            for(org.objectweb.fractal.api.Component tmpSubComponent : contentController.getFcSubComponents())
            {
                if(Fractal.getNameController(tmpSubComponent).getFcName().equals(subComponentName))
                {
                    subComponent=tmpSubComponent;
                }
            }
        }
        catch(NoSuchInterfaceException noSuchInterfaceException)
        {
            fail("Cannot get NameController of the fractal subComponent");
        }
        assertNotNull(subComponent);
        
        return subComponent;
    }
}
