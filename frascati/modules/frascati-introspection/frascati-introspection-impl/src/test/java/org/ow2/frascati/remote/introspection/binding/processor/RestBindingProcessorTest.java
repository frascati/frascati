/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 *
 */
public class RestBindingProcessorTest extends BindingProcessorTest
{

    public RestBindingProcessorTest()
    {
        super(BindingKind.REST, "counter-server/server/counter","counter-client/client/counter");
        this.addContributionToDeploy("counter");
    }

    @Override
    public void testSuite() throws BindingProcessorException, NoSuchInterfaceException, IllegalLifeCycleException, UnsupportedBindingComponentException, NoBindingKindException, UnsupportedBindingException
    {
        this.logEntering("testSuite");
        testCounter();
        
        this.removeClientBinding();
        this.removeServerBinding();

        Map<String,String> serverBindingHints=new HashMap<String, String>();
        serverBindingHints.put("uri", "http://localhost:9299/CounterServiceReconfigured");
        this.addServerBinding(serverBindingHints);

        Map<String, String> clientBindingHints = new HashMap<String, String>();
        clientBindingHints.put("uri", "http://localhost:9299/CounterServiceReconfigured");
        this.addClientBinding(clientBindingHints);
        testCounter();
        
        List<BindingAttribute> serverBindingAttributes = new ArrayList<BindingAttribute>();
        BindingAttribute uriBindingAttribute = new BindingAttribute("uri", "http://localhost:9299/CounterServiceReconfiguredAttribute");
        serverBindingAttributes.add(uriBindingAttribute);
        this.setServerAttribute(serverBindingAttributes);
        
        List<BindingAttribute> setBindingAttributesToSet=new ArrayList<BindingAttribute>();
        setBindingAttributesToSet.add(uriBindingAttribute);
        this.setClientAttribute(setBindingAttributesToSet);
        testCounter();
    }
    
    private void testCounter()
    {
        String response=this.invokeClient("getValue");
        assertNotNull(response);
        assertEquals("0", response);

        this.invokeClient("increment","3");
        response=this.invokeClient("getValue");
        assertNotNull(response);
        assertEquals("3", response);

        this.invokeClient("decrement","5");
        response=this.invokeClient("getValue");
        assertNotNull(response);
        assertEquals("-2", response);
        
        this.invokeClient("resetIt");
        response=this.invokeClient("getValue");
        assertNotNull(response);
        assertEquals("0", response);
    }
}
