/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.ow2.frascati.remote.IntrospectionTestSuite;

/**
 *
 */
public class FractalUtilTest
{
    private static FractalUtilItf fractalUtil;
    
    @BeforeClass
    public static void initTest()
    {
        System.out.println("-- FractalUtil Tests");
        fractalUtil=IntrospectionTestSuite.getFractalUtil();
        IntrospectionTestSuite.deployContribution("helloworld-multiple-reference");
    }
    
    @Test
    public void getComponent() throws NoSuchInterfaceException
    {
        Component runnerMultiple=fractalUtil.getComponent("RunnerMultipleTopLevel");
        assertNotNull(runnerMultiple);
        boolean isPrimitiveComponent=fractalUtil.isPrimitiveComponent(runnerMultiple);
        assertEquals(false, isPrimitiveComponent);
        boolean isSCAComponent=fractalUtil.isSCAComponent(runnerMultiple);
        assertEquals(true, isSCAComponent);
        BindingController bindingController=fractalUtil.getBindingController(runnerMultiple);
        assertNotNull(bindingController);
        String componentName=fractalUtil.getComponentName(runnerMultiple);
        assertEquals("RunnerMultipleTopLevel", componentName);
        
        Component runner=fractalUtil.getComponent("RunnerMultipleTopLevel/Runner");
        assertNotNull(runner);
        isPrimitiveComponent=fractalUtil.isPrimitiveComponent(runner);
        assertEquals(true, isPrimitiveComponent);
        isSCAComponent=fractalUtil.isSCAComponent(runner);
        assertEquals(true, isSCAComponent);
        componentName=fractalUtil.getComponentName(runner);
        assertEquals("Runner", componentName);
        
        List<Component> subComponents=fractalUtil.getSubComponents(runnerMultiple);
        assertNotNull(subComponents);
        assertEquals(2, subComponents.size());
        
        Component tmpRunner=fractalUtil.getSubComponent(runnerMultiple, "Runner");
        assertEquals(tmpRunner, runner);
    }
    
    @Test
    public void getInterface() throws NoSuchInterfaceException
    {
        Interface runnablesTopLevel=fractalUtil.getInterface("RunnerMultipleTopLevel/runnablesTopLevel");
        assertNotNull(runnablesTopLevel);
        assertEquals("runnablesTopLevel", runnablesTopLevel.getFcItfName());
        boolean isSCAreference = fractalUtil.isSCAReference(runnablesTopLevel);
        assertEquals(true, isSCAreference);
    }

    @Test
    public void findComponentByItfId() throws NoSuchInterfaceException
    {
        Component runnerMultiple=fractalUtil.findComponentByItfId("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/run");
        assertNotNull(runnerMultiple);
        Interface run=fractalUtil.getInterface("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/run");
        assertNotNull(run);
        assertEquals(run.getFcItfOwner(), runnerMultiple);
    }
    
    @Test
    public void startStopComponent() throws NoSuchInterfaceException, IllegalLifeCycleException
    {
        Component runnerMultiple=fractalUtil.getComponent("RunnerMultipleTopLevel");
        assertNotNull(runnerMultiple);
        String status=fractalUtil.getComponentStatus(runnerMultiple);
        assertEquals("STARTED", status);
        fractalUtil.stopComponent(runnerMultiple);
        status=fractalUtil.getComponentStatus(runnerMultiple);
        assertEquals("STOPPED", status);
        fractalUtil.startComponent(runnerMultiple);
        status=fractalUtil.getComponentStatus(runnerMultiple);
        assertEquals("STARTED", status);
    }
    
    @Test
    public void getCollectionName() throws NoSuchInterfaceException
    {
        Interface runnables=fractalUtil.getInterface("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables");
        assertNotNull(runnables);
        String runnablesName=runnables.getFcItfName();
        assertEquals("runnables", runnablesName);
        InterfaceType runnablesType=(InterfaceType) runnables.getFcItfType();
        assertNotNull(runnablesType);
        assertEquals(true, runnablesType.isFcCollectionItf());
        String lastNameforCollection=fractalUtil.getLastInterfaceCollectionName(runnables.getFcItfOwner(),runnablesName);
        assertEquals(runnablesName, lastNameforCollection);
        String nextNameForCollection=fractalUtil.getNextInterfaceName(runnables.getFcItfOwner(), runnablesName);
        assertEquals(runnablesName+"-0", nextNameForCollection);
    }
    
    @AfterClass
    public static void undeployTestResources()
    {
        IntrospectionTestSuite.undeployComposite("RunnerMultipleTopLevel");
        IntrospectionTestSuite.undeployComposite("RunnerSimple");
    }
}
