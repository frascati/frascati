/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.remote.IntrospectionTestSuite;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.binding.BindingManagerItf;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FractalUtilItf;

/**
 *
 */
public abstract class BindingProcessorTest
{
    protected Logger logger=Logger.getLogger(BindingProcessorTest.class.getName());
    
    protected FractalUtilItf fractalUtil;
    protected BindingManagerItf bindingManager;
    
    protected BindingKind bindingKind;
    
    private List<String> contributionsToDeploy;
    private Map<String,String> compositesToDeploy;
    protected List<String> deployedComposites;
    
    protected String serverInterfacePath;
    protected String serverInterfaceName;
    protected String clientInterfacePath;
    protected String clientInterfaceName;
    
    protected Interface serverInterfaceFc;
    protected Object skeltonObject;
    protected Interface skeltonInterfaceFc;
    
    protected Interface clientInterfaceFc;
    protected Object stubObject;
    protected Interface stubInterfaceFc;
    

    
    public BindingProcessorTest(BindingKind bindingKind, String serverInterfacePath, String clientInterfacePath)
    {
        this.bindingKind=bindingKind;
        this.serverInterfacePath=serverInterfacePath;
        this.clientInterfacePath=clientInterfacePath;
        this.contributionsToDeploy=new ArrayList<String>();
        this.compositesToDeploy=new HashMap<String, String>();
    }
        
    protected void addContributionToDeploy(String contributionName)
    {
        this.contributionsToDeploy.add(contributionName);
    }

    protected void addCompositeToDeploy(String compositeFileName, String compositeName)
    {
        this.compositesToDeploy.put(compositeFileName, compositeName);
    }
    
    /**
     * Deployed composites and retrieve fractal server and client interfaces based on 
     * 
     * @throws NoSuchInterfaceException if serverInterfaceFc or clientInterfaceFc can't be found serverInterfacePath and clientInterfacePath
     */
    public void startTest() throws NoSuchInterfaceException
    {
        this.logEntering("startTest");
        this.fractalUtil=IntrospectionTestSuite.getFractalUtil();
        this.bindingManager=IntrospectionTestSuite.getBindingManager();
        this.deployedComposites=new ArrayList<String>();
        
        /**deploy contributions*/
        StringList deployedContributionStringList;
        for(String contributionFileName : contributionsToDeploy)
        {
            deployedContributionStringList=IntrospectionTestSuite.deployContribution(contributionFileName);
            this.log(" contribution "+contributionFileName+" deployed");
            this.deployedComposites.addAll(deployedContributionStringList.getStringList());
        }
        
        /** deploy composites*/
        String deployedComposite;
        for(String compositeFileName : compositesToDeploy.keySet())
        {
            deployedComposite=IntrospectionTestSuite.deployComposite(compositeFileName, compositesToDeploy.get(compositeFileName));
            this.log(" contribution "+deployedComposite+" deployed");
            this.deployedComposites.add(deployedComposite);
        }
        
        /** get the fractal server interface is defined in serverInterfacePath*/
        if(this.serverInterfacePath!=null && !"".equals(this.serverInterfacePath))
        {
            this.serverInterfaceFc=fractalUtil.getInterface(this.serverInterfacePath);
            assertNotNull(this.serverInterfaceFc);
            Component serverComponentFc=this.serverInterfaceFc.getFcItfOwner();
            this.serverInterfaceName=fractalUtil.getComponentName(serverComponentFc)+"/"+this.serverInterfaceFc.getFcItfName();
            this.log("server : "+this.serverInterfaceName);
            initServer();
        }

        /** get the fractal client interface is defined in clientInterfacePath*/
        if(this.clientInterfacePath!=null && !"".equals(this.clientInterfacePath))
        {
            this.clientInterfaceFc=fractalUtil.getInterface(this.clientInterfacePath);
            Component clientComponentFc=this.clientInterfaceFc.getFcItfOwner();
            this.clientInterfaceName=fractalUtil.getComponentName(clientComponentFc)+"/"+this.clientInterfaceFc.getFcItfName();
            this.log("client : "+this.clientInterfaceName);
            assertNotNull(this.clientInterfaceFc);
            initClient();
        }
    }

    public abstract void testSuite() throws BindingProcessorException, NoSuchInterfaceException, IllegalLifeCycleException, UnsupportedBindingComponentException, NoBindingKindException, UnsupportedBindingException;
    
    public void finishTest()
    {
        this.logEntering("finishTest");
        for(String deployedComposite : this.deployedComposites)
        {
            IntrospectionTestSuite.undeployComposite(deployedComposite);
            this.log("composite "+deployedComposite+" undeloyed");
        }
    }
    
    /**
     * Init server side, get skelton objects and check if this skelton is related to the current BindingKind
     */
    private void initServer()
    {
        logEntering("initServer");
        List<Interface> clientInterfaces=bindingManager.getBindingInterfaces(this.serverInterfaceFc);
        assertEquals(1, clientInterfaces.size());
        this.skeltonObject=clientInterfaces.get(0);
        assertNotNull(this.skeltonObject);
        if(this.skeltonObject instanceof Interface)
        {
            this.skeltonInterfaceFc=(Interface) this.skeltonObject;
            this.skeltonObject=this.skeltonInterfaceFc.getFcItfOwner();
            assertNotNull(this.skeltonObject);
        }
        
        try
        {
            BindingProcessorItf bindingProcessor=bindingManager.getBindingProcessor(this.skeltonObject);
            StringBuilder inconsistantBindingKing=new StringBuilder("Inconsistent BindingProcessor : ");
            inconsistantBindingKing.append("The provided serverInterfacePath "+this.serverInterfacePath);
            inconsistantBindingKing.append(" is bound to "+bindingProcessor.getBindingKind()+" skelton");
            inconsistantBindingKing.append(", but the provided BindingKind is "+this.getBindingKind().name());                
            assertEquals(inconsistantBindingKing.toString(),this.bindingKind, bindingProcessor.getBindingKind());
        }
        catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
        {
            logger.warning("BindingProcessorTest initTest"+unsupportedBindingComponentException.getMessage());
        }
    }
    
    /**
     * Init client side, get stub objects and check if this stub is related to the current BindingKind
     */
    private void initClient()
    {
        this.stubObject=fractalUtil.getServerInterface(this.clientInterfaceFc);
        assertNotNull(this.stubObject);
        if(this.stubObject instanceof Interface)
        {
            this.stubInterfaceFc=(Interface) this.stubObject;
            this.stubObject=this.stubInterfaceFc.getFcItfOwner();
        }
        
        try
        {
            BindingProcessorItf bindingProcessor=bindingManager.getBindingProcessor(this.stubObject);
            StringBuilder inconsistantBindingKing=new StringBuilder("Inconsistent BindingProcessor : ");
            inconsistantBindingKing.append("The provided clientInterfacePath "+this.clientInterfacePath);
            inconsistantBindingKing.append(" is bound to "+bindingProcessor.getBindingKind()+" stub");
            inconsistantBindingKing.append(", but the provided BindingKind is "+this.bindingKind.name());                
            assertEquals(inconsistantBindingKing.toString(),this.bindingKind, bindingProcessor.getBindingKind());
        }
        catch (UnsupportedBindingComponentException unsupportedBindingComponentException)
        {
            logger.warning("BindingProcessorTest initTest"+unsupportedBindingComponentException.getMessage());
        }
    }
    
    public String invokeClient(String methodName, String...params)
    {
        MultivaluedMap<String, String> multiValueparams=new MetadataMap<String, String>();
        multiValueparams.add("methodName", methodName);
        for(int i=0;i<params.length;i++)
        {
            multiValueparams.add("Parameter"+i, params[i]);
        }
        RemoteScaDomain remoteScaDomain=IntrospectionTestSuite.getDomain();
        return remoteScaDomain.invokeMethod(this.clientInterfacePath, multiValueparams);
    }

    
    public void removeServerBinding() throws BindingProcessorException, UnsupportedBindingComponentException
    {
        this.logEntering("removeServerBinding",this.serverInterfaceName, this.skeltonObject);
        bindingManager.unbind(serverInterfaceFc, serverInterfacePath, 0);
        List<Interface> clientInterfaces=bindingManager.getBindingInterfaces(this.serverInterfaceFc);
        assertEquals(true, clientInterfaces.isEmpty());
        this.skeltonObject=null;
        this.skeltonInterfaceFc=null;
    }
    
    public void addServerBinding(Map<String,String> serverHints) throws BindingProcessorException, NoBindingKindException, UnsupportedBindingException
    {
        if(serverHints==null || serverHints.isEmpty())
        {
            this.log(Level.WARNING,"No hints provided to create server Binding");
            return;
        }

        this.logEntering("addServerBinding",this.serverInterfaceName);
        serverHints.put("kind", this.getBindingKind().name());
        bindingManager.bind(serverInterfaceFc, serverInterfacePath, serverHints);
        initServer();
    }
    
    public void setServerAttribute(List<BindingAttribute> serverBindingAttributes) throws BindingProcessorException, UnsupportedBindingComponentException
    {
        if(serverBindingAttributes==null || serverBindingAttributes.isEmpty())
        {
            this.log(Level.WARNING,"No attributes provided to be set on current server binding");
            return;
        }
        
        String attributeName, attributeValue;
        for(BindingAttribute bindingAttributeToSet : serverBindingAttributes)
        {
            attributeName=bindingAttributeToSet.getName();
            attributeValue=bindingAttributeToSet.getValue();
            this.logEntering("setServerAttribute",this.serverInterfaceName, attributeName, attributeValue, this.skeltonObject);
            this.bindingManager.setBindingAttribute(serverInterfaceFc, 0, attributeName, attributeValue);
        }
        

        BindingProcessorItf bindingProcessor=bindingManager.getBindingProcessor(this.skeltonObject);
        List<BindingAttribute> serverBingAttributes=bindingProcessor.getAttributes(this.skeltonObject);
        for(BindingAttribute bindingAttributeToSet : serverBindingAttributes)
        {
            assertTrue(serverBingAttributes.contains(bindingAttributeToSet));
        }
    }
    
    public void removeClientBinding() throws BindingProcessorException, UnsupportedBindingComponentException
    {
        this.logEntering("removeClientBinding",this.clientInterfaceName, this.stubObject);
        IntrospectionTestSuite.getBindingManager().unbind(clientInterfaceFc, clientInterfacePath, 0);
        Object stubObject=fractalUtil.getServerInterface(this.clientInterfaceFc);
        assertNull(stubObject);
        this.stubObject=null;
        this.stubInterfaceFc=null;
    }
    
    public void addClientBinding(Map<String,String> clientHints) throws BindingProcessorException, NoBindingKindException, UnsupportedBindingException
    {
        if(clientHints==null || clientHints.isEmpty())
        {
            this.log(Level.WARNING,"No hints provided to create client Binding");
            return;
        }
        
        this.logEntering("addClientBinding",this.clientInterfaceName);
        clientHints.put("kind", this.getBindingKind().name());
        bindingManager.bind(clientInterfaceFc, clientInterfacePath, clientHints);
        initClient();
    }
    
    public void setClientAttribute(List<BindingAttribute> clientBindingAttributes) throws BindingProcessorException, UnsupportedBindingComponentException
    {
        if(clientBindingAttributes==null || clientBindingAttributes.isEmpty())
        {
            this.log(Level.WARNING,"No attributes provided to be set on current client binding");
            return;
        }
        
        String attributeName, attributeValue;
        for(BindingAttribute bindingAttributeToSet : clientBindingAttributes)
        {
            attributeName=bindingAttributeToSet.getName();
            attributeValue=bindingAttributeToSet.getValue();
            this.logEntering("setClientAttribute",this.clientInterfaceName, attributeName, attributeValue, this.stubObject);
            bindingManager.setBindingAttribute(clientInterfaceFc, 0, attributeName, attributeValue);
        }
        
        BindingProcessorItf bindingProcessor=bindingManager.getBindingProcessor(this.skeltonObject);
        List<BindingAttribute> clientBingAttributes=bindingProcessor.getAttributes(this.stubObject);
        for(BindingAttribute bindingAttributeToSet : clientBindingAttributes)
        {
            assertTrue(clientBingAttributes.contains(bindingAttributeToSet));
        }
    }
    
    /***************************************Getter and Utils*****************************************/
    
    public BindingKind getBindingKind()
    {
        return this.bindingKind;
    }
    
    public Interface getServerInterfaceFc()
    {
        return serverInterfaceFc;
    }

    public Object getSkeltonObject()
    {
        return skeltonObject;
    }

    public Interface getClientInterfaceFc()
    {
        return clientInterfaceFc;
    }

    public Object getStubObject()
    {
        return stubObject;
    }
    
    protected void log(String message)
    {
        this.log(Level.INFO, message);
    }
    
    protected void log(Level level, String message)
    {
        this.logger.log(level, "["+this.getClass().getSimpleName()+"] "+message);
    }
    
    protected void logEntering(String method, Object... params)
    {
        this.logMethod("entering", method, params);
    }
    
    protected void logExiting(String method, Object... params)
    {
        this.logMethod("exiting", method, params);
    }
    
    private void logMethod(String access, String method, Object... params)
    {
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("["+this.getClass().getSimpleName()+"] ");
        stringBuilder.append(access+" ");
        stringBuilder.append(method);
        
        if(params.length!=0)
        {
            stringBuilder.append(", params : ");
            stringBuilder.append(params[0]);
            for(int i=1; i<params.length; i++)
            {
                stringBuilder.append(", ");
                stringBuilder.append(params[i]);
            }
        }
        logger.info(stringBuilder.toString());
    }
}
