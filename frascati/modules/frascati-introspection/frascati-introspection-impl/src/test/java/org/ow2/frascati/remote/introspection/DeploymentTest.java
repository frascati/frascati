/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.remote.IntrospectionTestSuite;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;

/**
 *
 */
public class DeploymentTest
{
    private static Deployment deployment;
    
    private static String contributionString;
    private static String compositeString;
    
    private static String contextualPropertiesComposite;
    private static String contextualPropertiesContribution;
    
    private static final String invalidJar="invalidencodedjar";
    
    @BeforeClass
    public static void initTest()
    {
        System.out.println("-- Deployment Tests");
        deployment=IntrospectionTestSuite.getDeployment();
        
        String contributionName="helloworld-multiple-reference.zip";
        File contributionFile=IntrospectionTestSuite.getResource(contributionName);
        try
        {
            contributionString = FileUtil.getStringFromFile(contributionFile);
        } catch (IOException e)
        {
            fail("Cannot read the contribution "+contributionName);
        }
        
        contributionName="frascati-contextualProperties-exemple.zip";
        contributionFile=IntrospectionTestSuite.getResource(contributionName);
        try
        {
            contextualPropertiesContribution = FileUtil.getStringFromFile(contributionFile);
        }
        catch (IOException e)
        {
            fail("Cannot read the contribution "+contributionName);
        }
        
        String compositeName="helloworld-multiple-reference.jar";
        File compositeFile=IntrospectionTestSuite.getResource(compositeName);
        try
        {
            compositeString = FileUtil.getStringFromFile(compositeFile);
        }
        catch (IOException e)
        {
            fail("Cannot read the composite "+compositeName);
        }
        
        compositeName="contextual-properties.jar";
        compositeFile=IntrospectionTestSuite.getResource(compositeName);
        try
        {
            contextualPropertiesComposite = FileUtil.getStringFromFile(compositeFile);
        }
        catch (IOException e)
        {
            fail("Cannot read the composite "+compositeName);
        }
    }

    @Test
    public void getCompositeEntriesFromJar() throws IOException
    {
        String entriesString=deployment.getCompositeEntriesFromJar(compositeString);
        String[] entries=entriesString.split(FileUtil.JAR_ENTRIES_SEPARATOR);
        assertEquals(5, entries.length);
        List<String> entriesList=Arrays.asList(entries);
        assertEquals(true, entriesList.contains("RunnerMultipleContainer"));
        assertEquals(true, entriesList.contains("RunnerMultipleLevel1"));
        assertEquals(true, entriesList.contains("RunnerMultipleLevel2"));
        assertEquals(true, entriesList.contains("RunnerMultipleTopLevel"));
        assertEquals(true, entriesList.contains("RunnerSimple"));
        
        /** Test with invalid parameter*/
        try
        {
            deployment.getCompositeEntriesFromJar(invalidJar);
            fail("WebApplicationException was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=IntrospectionTestSuite.getResponseMessage(exceptionResponse);
            assertEquals("IO Exception when trying to read or write composite jar", response);
        }
    }
    
    @Test
    public void deployContribution() throws IOException
    {
        /** Deploy contribution*/
        StringList deployedCompositeStringList = deployment.deployContribution(contributionString);
        assertNotNull(deployedCompositeStringList);
        List<String> deployedContributions=deployedCompositeStringList.getStringList();
        assertNotNull(deployedContributions);
        assertEquals(2, deployedContributions.size());
        assertEquals("RunnerMultipleTopLevel", deployedContributions.get(0));
        assertEquals("RunnerSimple", deployedContributions.get(1));
        IntrospectionTestSuite.assertComponentExist("RunnerMultipleTopLevel");
        IntrospectionTestSuite.assertComponentExist("RunnerSimple");

        /**undeploy previous composites*/
        IntrospectionTestSuite.undeployComposite("RunnerMultipleTopLevel");
        IntrospectionTestSuite.undeployComposite("RunnerSimple");
    }
    
    @Test
    public void deployInvalidContribution()
    {
        /** Test with invalid parameter*/
        try
        {
            deployment.deployContribution(invalidJar);
            fail("WebApplicationException was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=IntrospectionTestSuite.getResponseMessage(exceptionResponse);
            assertEquals("IO Exception when trying to read or write contribution zip", response);
        }
    }
    
    @Test
    public void deployContributionContextualProperties() throws IOException
    {
        /** Deploy contribution*/
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("contribution", contextualPropertiesContribution);
        params.add("component-name", "deployContributionContextualProperties");
        params.add("foo", "fooValue");
        params.add("bar/bar", "barbarValue");
        
        StringList deployedCompositeStringList = deployment.deployContribution(params);
        assertNotNull(deployedCompositeStringList);
        List<String> deployedContributions=deployedCompositeStringList.getStringList();
        assertNotNull(deployedContributions);
        assertEquals(1, deployedContributions.size());
        assertEquals("contextual-properties", deployedContributions.get(0));
        
        IntrospectionTestSuite.assertComponentExist("contextual-properties");
        IntrospectionTestSuite.assertComponentExist("contextual-properties/deployContributionContextualProperties");
        MultivaluedMap<String, String> invokationParams=new MetadataMap<String, String>();
        invokationParams.add("methodName", "run");
        IntrospectionTestSuite.getDomain().invokeMethod("contextual-properties/Runnable", invokationParams);
        
        /**undeploy previous composite*/
        IntrospectionTestSuite.undeployComposite("contextual-properties");
    }
    
    @Test
    public void deployComposite() throws IOException
    {
        /** Deploy contribution*/
        String compositeName = deployment.deployComposite("RunnerSimple", compositeString);
        assertNotNull(compositeName);
        assertEquals("RunnerSimple", compositeName);
        IntrospectionTestSuite.assertComponentExist("RunnerSimple");
        
        /**undeploy previous composite*/
        IntrospectionTestSuite.undeployComposite("RunnerSimple");
    }
    
    @Test
    public void deployInvalidComposite()
    {
        /** Test with invalid composite*/
        try
        {
            deployment.deployComposite("RunnerSimple", invalidJar);
            fail("WebApplicationException was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=IntrospectionTestSuite.getResponseMessage(exceptionResponse);
            assertEquals("IO Exception when trying to read or write composite jar", response);
        }
    }
    
    @Test
    public void deployInvalidCompositeName()
    {
        /** Test with invalid composite name*/
        try
        {
            deployment.deployComposite("InvalidName", compositeString);
            fail("WebApplicationException was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=IntrospectionTestSuite.getResponseMessage(exceptionResponse);
            assertEquals("Error while trying to deploy the composite", response);
        }
    }
    
    @Test
    public void deployCompositeContextualProperties() throws IOException
    {
        /** Deploy contribution*/
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("compositeName", "contextual-properties");
        params.add("jar", contextualPropertiesComposite);
        params.add("component-name", "deployCompositeContextualProperties");
        params.add("foo", "fooValue");
        params.add("bar/bar", "barbarValue");
        /**Not needed, just to check complex contextual property path works well*/
        params.add("x/y/z/t", "xyztValue");
        params.add("x/y/z/a/b/c", "xyzabcValue");
        
        String compositeName = deployment.deployComposite(params);
        assertNotNull(compositeName);
        assertEquals("contextual-properties", compositeName);

        IntrospectionTestSuite.assertComponentExist("contextual-properties");
        IntrospectionTestSuite.assertComponentExist("contextual-properties/deployCompositeContextualProperties");
        MultivaluedMap<String, String> invokationParams=new MetadataMap<String, String>();
        invokationParams.add("methodName", "run");
        IntrospectionTestSuite.getDomain().invokeMethod("contextual-properties/Runnable", invokationParams);
        
        /**undeploy previous composite*/
        IntrospectionTestSuite.undeployComposite("contextual-properties");
    }
    
    @Test
    public void undeployComposite() throws IOException
    {
        /** Deploy contribution*/
        IntrospectionTestSuite.deployContribution("helloworld-multiple-reference");
        /** Undeploy composites*/
        deployment.undeployComposite("RunnerMultipleTopLevel");
        IntrospectionTestSuite.assertComponentNotExist("RunnerMultipleTopLevel");
        deployment.undeployComposite("RunnerSimple");
        IntrospectionTestSuite.assertComponentNotExist("RunnerSimple");
        
        /** Test with invalid composite name*/
        try
        {
            deployment.undeployComposite("InvalidName");
            fail("WebApplicationException was excepted");
        }
        catch(WebApplicationException webApplicationException)
        {
            Response exceptionResponse=webApplicationException.getResponse();
            assertEquals(400, exceptionResponse.getStatus());
            String response=IntrospectionTestSuite.getResponseMessage(exceptionResponse);
            assertEquals("Error while trying to undeploy InvalidName", response);
        }
    }
    
    @Test
    public void loadLibrary()
    {
        File widgetManagerContribution=IntrospectionTestSuite.getResource("widget-manager.zip");
        String encodedwidgetManagerContribution = null;
        try
        {
            encodedwidgetManagerContribution = FileUtil.getStringFromFile(widgetManagerContribution);
        } catch (IOException e)
        {
            fail("Cannot read the lib "+widgetManagerContribution);
        }
        
        /**we deploy a contribution that contains only widget-manager-impl and no imports
         * because widget-manager-api is not in the main Frascati ClassLoader the contribution
         * fails to be deployed
         * */
        try
        {
            deployment.deployContribution(encodedwidgetManagerContribution);
            fail("An exception was occured");
        }
        catch(Exception exception)
        {
            /**nothing to do*/
        }

        
        File widgetManagerApiFile=IntrospectionTestSuite.getResource("widget-manager-api.jar");
        String encodedWidgetManagerApi = null;
        try
        {
            encodedWidgetManagerApi = FileUtil.getStringFromFile(widgetManagerApiFile);
        } catch (IOException e)
        {
            fail("Cannot read the lib "+widgetManagerApiFile);
        }
        /**we first load widget-manager-api that contains interface definition for widget-manager-impl*/
        deployment.loadLibrary(encodedWidgetManagerApi);
        
        /**we deploy a contribution that contains only widget-manager-impl and no imports
         * because widget-manager-api is already in the main Frascati ClassLoader the contribution can be deployed
         * */
        deployment.deployContribution(encodedwidgetManagerContribution);
    }
    
}
