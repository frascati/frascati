/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.jdom.JDOMException;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
/**
 *
 */
public class SCABindingProcessorTest extends BindingProcessorTest
{

    public SCABindingProcessorTest() throws IOException, JDOMException
    {
        super(BindingKind.SCA, "","helloworld-binding-sca/client/client/s");
        this.addContributionToDeploy("helloworld-binding-sca");
    }

    @Override
    public void testSuite() throws BindingProcessorException, NoSuchInterfaceException, IllegalLifeCycleException, UnsupportedBindingComponentException, NoBindingKindException, UnsupportedBindingException
    {
        this.logEntering("testSuite");
        this.invokeClient("print", this.getClass().getName());
        this.removeClientBinding();

        Map<String,String> clientBindingHints=new HashMap<String, String>();
        clientBindingHints.put("uri", "helloworld-binding-sca/server/server/PrintService");
        this.addClientBinding(clientBindingHints);
        this.invokeClient("print", this.getClass().getName());
    }

}
