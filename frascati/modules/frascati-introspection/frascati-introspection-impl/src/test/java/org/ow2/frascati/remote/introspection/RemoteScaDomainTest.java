/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.remote.IntrospectionTestSuite;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;

/**
 *
 */
public class RemoteScaDomainTest
{
    @Test
    public void multipleReference() throws NoSuchInterfaceException
    {
        IntrospectionTestSuite.deployContribution("helloworld-multiple-reference");
        
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/runnablesTopLevel", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/runnablesLevel2", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/runnablesLevel1", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/runnablesContainer", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables", Multiplicity._0N);

        MultivaluedMap<String, String> bindingHints = new MetadataMap<String, String>();
        bindingHints.add("kind", "WS");
        bindingHints.add("uri", "http://localhost:9000/runSimple");
        bindingHints.add("wsdlLocation", "http://localhost:9000/runSimple?wsdl");
        bindingHints.add("wsdlElement", "http://lang.java/#wsdl.port(Runnable/RunnablePort)");
        RemoteScaDomain introspection=IntrospectionTestSuite.getDomain();
        introspection.addBinding("RunnerMultipleTopLevel/runnablesTopLevel", bindingHints);
        
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/runnablesTopLevel-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/runnablesLevel2-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/runnablesLevel1-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/runnablesContainer-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables-0", Multiplicity._0N);
        
        IntrospectionTestSuite.undeployComposite("RunnerMultipleTopLevel");
        IntrospectionTestSuite.undeployComposite("RunnerSimple");
    }
}
