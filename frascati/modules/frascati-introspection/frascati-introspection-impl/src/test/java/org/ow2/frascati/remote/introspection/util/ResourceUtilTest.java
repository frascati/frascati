/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.remote.introspection.util;

import static org.junit.Assert.*;

import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.remote.IntrospectionTestSuite;
import org.ow2.frascati.remote.introspection.resources.Attribute;
import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.remote.introspection.resources.BindingKind;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.ComponentStatus;
import org.ow2.frascati.remote.introspection.resources.Interface;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;

/**
 * Utility class used to convert objects from Fractal API to REST (SCA)
 * resources.
 * 
 * @author Gwenael Cattez
 */
public class ResourceUtilTest
{
    private static FractalUtilItf fractalUtil;
    private static ResourceUtilItf resourceUtilItf;

    private static org.objectweb.fractal.api.Component helloworldFc;
    
    @BeforeClass
    public static void initTest() throws NoSuchInterfaceException
    {
        System.out.println("-- ResourceUtilTest");
        fractalUtil= IntrospectionTestSuite.getFractalUtil();
        resourceUtilItf = IntrospectionTestSuite.getResourceUtil();
        IntrospectionTestSuite.deployComposite("helloworld-pojo","helloworld-pojo");
        helloworldFc=fractalUtil.getComponent("helloworld-pojo");
        assertNotNull(helloworldFc);
    }
    
    @Test
    public void getComponentResource() throws NoSuchInterfaceException
    {
        Component helloworldComponent=resourceUtilItf.getComponentResource(helloworldFc);
        assertNotNull(helloworldComponent);
        assertEquals("helloworld-pojo", helloworldComponent.getName());
        ComponentStatus componentStatus=helloworldComponent.getStatus();
        assertEquals("STARTED", componentStatus.name());
        assertEquals(0, helloworldComponent.getComponents().size());
        assertEquals(0, helloworldComponent.getReferences().size());
        assertEquals(0, helloworldComponent.getServices().size());
        assertEquals(0, helloworldComponent.getProperties().size());
    }
    
    @Test
    public void getFullComponentResource()
    {
        Component helloworldComponent=resourceUtilItf.getFullComponentResource(helloworldFc);
        assertNotNull(helloworldComponent);
        assertEquals("helloworld-pojo", helloworldComponent.getName());
        assertEquals(ComponentStatus.STARTED, helloworldComponent.getStatus());
        assertEquals(0, helloworldComponent.getReferences().size());
        assertEquals(1, helloworldComponent.getServices().size());
        assertEquals(0, helloworldComponent.getProperties().size());
        List<Component> subComponents=helloworldComponent.getComponents();
        assertEquals(2, subComponents.size());
        
        Component client=subComponents.get(0);
        assertNotNull(client);
        assertEquals("client", client.getName());
        assertEquals(ComponentStatus.STARTED, client.getStatus());
        
        Component server=subComponents.get(1);
        assertNotNull(server);
        assertEquals("server", server.getName());
        assertEquals(ComponentStatus.STARTED, server.getStatus());
    }
    
    @Test
    public void getPortResource() throws NoSuchInterfaceException
    {
        IntrospectionTestSuite.deployContribution("counter");
        IntrospectionTestSuite.assertComponentExist("counter-client");
        IntrospectionTestSuite.assertComponentExist("counter-server");
        
        org.objectweb.fractal.api.Component component=fractalUtil.getComponent("counter-server/server");
        fractalUtil.displayComponent(Level.INFO, component);
        
        org.objectweb.fractal.api.Interface runnableClientFcItf=fractalUtil.getInterface("counter-client/client/Runnable");
        Port runnableClientPort=resourceUtilItf.getPortResource("counter-client/client/Runnable",runnableClientFcItf);
        assertNotNull(runnableClientPort);
        assertEquals("Runnable", runnableClientPort.getName());
        assertEquals("counter-client/client/Runnable", runnableClientPort.getPath());
        Interface runnableClientItf=runnableClientPort.getImplementedInterface();
        assertEquals("java.lang.Runnable", runnableClientItf.getClazz());
        assertEquals(1, runnableClientItf.getMethods().size());
        Method runClientMethod=runnableClientItf.getMethods().get(0);
        assertEquals("run", runClientMethod.getName());
        assertEquals(0, runClientMethod.getParameters().size());
        assertEquals(null, runClientMethod.getResult());
        
        org.objectweb.fractal.api.Interface counterClientFcItf=fractalUtil.getInterface("counter-client/client/counter");
        Port counterClientPort=resourceUtilItf.getPortResource("counter-client/client/counter",counterClientFcItf);
        assertNotNull(counterClientPort);
        assertEquals("counter", counterClientPort.getName());
        assertEquals("counter-client/client/counter", counterClientPort.getPath());
        assertEquals(Multiplicity._01, counterClientPort.getMutiplicity());
        Interface counterClientItf=counterClientPort.getImplementedInterface();
        assertEquals("org.ow2.frascati.examples.counter.api.CounterService", counterClientItf.getClazz());
        assertEquals(4, counterClientItf.getMethods().size());
        assertEquals(1, counterClientPort.getBindings().size());
        Binding counterClientBinding=counterClientPort.getBindings().get(0);
        assertEquals(BindingKind.REST, counterClientBinding.getKind());
        List<Attribute> counterClientBindingAttributes = counterClientBinding.getAttributes();
        assertEquals(3, counterClientBindingAttributes.size());
        Attribute counterClientServiceClass=counterClientBindingAttributes.get(0);
        assertNotNull(counterClientServiceClass);
        assertEquals("serviceClass", counterClientServiceClass.getName());
        assertEquals("java.lang.Class", counterClientServiceClass.getType());
        assertEquals("interface org.ow2.frascati.examples.counter.api.CounterService", counterClientServiceClass.getValue());
        Attribute counterClientUri=counterClientBindingAttributes.get(2);
        assertNotNull(counterClientUri);
        assertEquals("uri", counterClientUri.getName());
        assertEquals("java.lang.String", counterClientUri.getType());
        assertEquals("http://localhost:9090/CounterService", counterClientUri.getValue());
        
        org.objectweb.fractal.api.Interface counterServerFcItf=fractalUtil.getInterface("counter-server/server/counter");
        Port counterServerPort=resourceUtilItf.getPortResource("counter-server/server/counter",counterServerFcItf);
        assertNotNull(counterServerPort);
        assertEquals("counter", counterServerPort.getName());
        assertEquals("counter-server/server/counter", counterServerPort.getPath());
        Interface counterServerItf=counterServerPort.getImplementedInterface();
        assertEquals("org.ow2.frascati.examples.counter.api.CounterService", counterServerItf.getClazz());
        assertEquals(4, counterServerItf.getMethods().size());
        assertEquals(1, counterServerPort.getBindings().size());
        Binding counterServerBinding=counterClientPort.getBindings().get(0);
        assertEquals(BindingKind.REST, counterServerBinding.getKind());
        List<Attribute> counterServerBindingAttributes = counterServerBinding.getAttributes();
        assertEquals(3, counterServerBindingAttributes.size());
        Attribute counterServerServiceClass=counterServerBindingAttributes.get(0);
        assertNotNull(counterServerServiceClass);
        assertEquals("serviceClass", counterServerServiceClass.getName());
        assertEquals("java.lang.Class", counterServerServiceClass.getType());
        assertEquals("interface org.ow2.frascati.examples.counter.api.CounterService", counterServerServiceClass.getValue());
        Attribute counterServerUri=counterServerBindingAttributes.get(2);
        assertNotNull(counterServerUri);
        assertEquals("uri", counterServerUri.getName());
        assertEquals("java.lang.String", counterServerUri.getType());
        assertEquals("http://localhost:9090/CounterService", counterServerUri.getValue());
        
        IntrospectionTestSuite.undeployComposite("counter-client");
        IntrospectionTestSuite.undeployComposite("counter-server");
    }
    
    @Test
    public void getMultipleReferenceResource() throws NoSuchInterfaceException
    {
        IntrospectionTestSuite.deployContribution("helloworld-multiple-reference");
        
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/runnablesTopLevel", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/runnablesLevel2", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/runnablesLevel1", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/runnablesContainer", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables", Multiplicity._0N);
        
        MultivaluedMap<String, String> params = new MetadataMap<String, String>();
        params.add("kind", "WS");
        params.add("uri", "http://localhost:9000/runSimple");
        params.add("wsdlLocation", "http://localhost:9000/runSimple?wsdl");
        params.add("wsdlElement", "http://lang.java/#wsdl.port(Runnable/RunnablePort)");
        IntrospectionTestSuite.getDomain().addBinding("RunnerMultipleTopLevel/runnablesTopLevel", params);
        
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/runnablesTopLevel-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/runnablesLevel2-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/runnablesLevel1-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/runnablesContainer-0", Multiplicity._0N);
        IntrospectionTestSuite.assertPortExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables-0", Multiplicity._0N);
        
        IntrospectionTestSuite.getDomain().removeBinding("RunnerMultipleTopLevel/runnablesTopLevel-0", "0");
        
        IntrospectionTestSuite.assertPortNotExist("RunnerMultipleTopLevel/runnablesTopLevel-0");
        IntrospectionTestSuite.assertPortNotExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/runnablesLevel2-0");
        IntrospectionTestSuite.assertPortNotExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/runnablesLevel1-0");
        IntrospectionTestSuite.assertPortNotExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/runnablesContainer-0");
        IntrospectionTestSuite.assertPortNotExist("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables-0");
        
        IntrospectionTestSuite.undeployComposite("RunnerMultipleTopLevel");
    }
    
    @Test
    public void getPropertyResource() throws NoSuchInterfaceException
    {
        Property property=resourceUtilItf.getPropertyResource(helloworldFc, "notExiinting");
        assertNull(property);
        org.objectweb.fractal.api.Component serverFc=fractalUtil.getComponent("helloworld-pojo/server");
        assertNotNull(serverFc);
        property=resourceUtilItf.getPropertyResource(serverFc, "notExisintg");
        assertNull(property);
        property=resourceUtilItf.getPropertyResource(serverFc, "header");
        assertNotNull(property);
        assertEquals("header", property.getName());
        assertEquals("java.lang.String", property.getType());
        assertEquals("--->> ", property.getValue());
    }
    
    @AfterClass
    public static void undeployTestResources()
    {
        IntrospectionTestSuite.undeployComposite("helloworld-pojo");
    }
}