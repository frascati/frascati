/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.jdom.JDOMException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.remote.IntrospectionTestSuite;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.JMSBindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.JSONRPCBindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.RMIBindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.RestBindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.SCABindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.UPnPBindingProcessorTest;
import org.ow2.frascati.remote.introspection.binding.processor.WSBindingProcessorTest;
import org.ow2.frascati.remote.introspection.resources.BindingKind;


/**
 *
 */
public class BindingManagerTest
{
    private static BindingManagerItf bindingManager;
    
    @BeforeClass
    public static void startTest()
    {
        System.out.println("-- BindingManagerTest");
        bindingManager=IntrospectionTestSuite.getBindingManager();
    }
    
    @Test
    public void getBindingProcessorForKind() throws UnsupportedBindingException
    {
        try
        {
            bindingManager.getBindingProcessor("");
            fail("UnsupportedBindingException was expected for empty binding kind");
        }
        catch (UnsupportedBindingException unsupportedBindingException)
        {
            assertEquals("The binding  is not currently supported", unsupportedBindingException.getMessage());
        }
        
        try
        {
            bindingManager.getBindingProcessor((BindingKind)null);
            fail("UnsupportedBindingException was expected for empty binding kind");
        }
        catch (UnsupportedBindingException unsupportedBindingException)
        {
            assertEquals("The binding null is not currently supported", unsupportedBindingException.getMessage());
        }
    }
    
    public void getBindinProcessor(BindingProcessorTest bindingProcessorTest) throws UnsupportedBindingComponentException, UnsupportedBindingException
    {
        BindingProcessorItf processor=bindingManager.getBindingProcessor(bindingProcessorTest.getBindingKind());
        assertEquals(bindingProcessorTest.getBindingKind(), processor.getBindingKind());
        
        Object stubObject=bindingProcessorTest.getStubObject();
        if(stubObject!=null)
        {
            processor=bindingManager.getBindingProcessor(stubObject);
            assertEquals(bindingProcessorTest.getBindingKind(), processor.getBindingKind());
        }
        
        Object skeltonObject=bindingProcessorTest.getSkeltonObject();
        if(skeltonObject!=null)
        {
            processor=bindingManager.getBindingProcessor(skeltonObject);
            assertEquals(bindingProcessorTest.getBindingKind(), processor.getBindingKind());
        }
    }
    
    public void getClientBindingBundle(BindingProcessorTest bindingProcessorTest) throws NoSuchInterfaceException
    {
        Interface clientInterfacefc = bindingProcessorTest.getClientInterfaceFc();
        BindingBundle clientBindingBundle = bindingManager.getBindingBundle(clientInterfacefc);
        assertNotNull(clientBindingBundle);
        assertEquals(true, clientBindingBundle.isReferenceBinding());
        assertEquals(1, clientBindingBundle.getProcessors().size());
        assertEquals(bindingProcessorTest.getBindingKind(), clientBindingBundle.getProcessors().get(0).getBindingKind());
        assertEquals(1, clientBindingBundle.getBoundedObjects().size());
        Object stubObject=bindingProcessorTest.getStubObject();
        assertEquals(stubObject.getClass(), clientBindingBundle.getBoundedObjects().get(0).getClass());
    }
    
    public void getServerBindingBundle(BindingProcessorTest bindingProcessorTest) throws NoSuchInterfaceException
    {
        Interface serverInterfaceFc = bindingProcessorTest.getServerInterfaceFc();
        BindingBundle serverBindingBundle = bindingManager.getBindingBundle(serverInterfaceFc);
        assertNotNull(serverBindingBundle);
        assertEquals(false, serverBindingBundle.isReferenceBinding());
        assertEquals(1, serverBindingBundle.getProcessors().size());
        assertEquals(bindingProcessorTest.getBindingKind(), serverBindingBundle.getProcessors().get(0).getBindingKind());
        assertEquals(1, serverBindingBundle.getBoundedObjects().size());
        Object skeltonObject=bindingProcessorTest.getSkeltonObject();
        assertEquals(skeltonObject.getClass(), serverBindingBundle.getBoundedObjects().get(0).getClass());
    }
    
    @Test
    public void jmsBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException
    {
        JMSBindingProcessorTest jmsBindingProcessorTest=new JMSBindingProcessorTest();
        jmsBindingProcessorTest.startTest();
        this.getBindinProcessor(jmsBindingProcessorTest);
        this.getClientBindingBundle(jmsBindingProcessorTest);
        this.getServerBindingBundle(jmsBindingProcessorTest);
        jmsBindingProcessorTest.testSuite();
        jmsBindingProcessorTest.finishTest();
    }
    
    @Test
    public void jsonrpcBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException, IllegalLifeCycleException, NoBindingKindException
    {
        JSONRPCBindingProcessorTest jsonrpcBindingProcessorTest=new JSONRPCBindingProcessorTest();
        jsonrpcBindingProcessorTest.startTest();
        this.getBindinProcessor(jsonrpcBindingProcessorTest);
        this.getClientBindingBundle(jsonrpcBindingProcessorTest);
        this.getServerBindingBundle(jsonrpcBindingProcessorTest);
        jsonrpcBindingProcessorTest.testSuite();
        jsonrpcBindingProcessorTest.finishTest();
    }
    
    @Test
    public void restBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException, IllegalLifeCycleException, NoBindingKindException
    {
        RestBindingProcessorTest restBindingProcessorTest=new RestBindingProcessorTest();
        restBindingProcessorTest.startTest();
        this.getBindinProcessor(restBindingProcessorTest);
        this.getClientBindingBundle(restBindingProcessorTest);
        this.getServerBindingBundle(restBindingProcessorTest);
        restBindingProcessorTest.testSuite();
        restBindingProcessorTest.finishTest();
    }
    
    //FIXME
    //@Test
    public void rmiBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException
    {
        RMIBindingProcessorTest rmiBindingProcessorTest=new RMIBindingProcessorTest();
        rmiBindingProcessorTest.startTest();
        this.getBindinProcessor(rmiBindingProcessorTest);
        this.getClientBindingBundle(rmiBindingProcessorTest);
        //FIXME RMI server is not proceed
        rmiBindingProcessorTest.testSuite();
        rmiBindingProcessorTest.finishTest();
    }
  
  @Test
  public void scaBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException, IllegalLifeCycleException, NoBindingKindException, IOException, JDOMException
  {
      SCABindingProcessorTest scaBindingProcessorTest=new SCABindingProcessorTest();
      scaBindingProcessorTest.startTest();
      this.getBindinProcessor(scaBindingProcessorTest);
      this.getClientBindingBundle(scaBindingProcessorTest);
      /**SCA binding on a service is a non sense*/
      scaBindingProcessorTest.testSuite();
      scaBindingProcessorTest.finishTest();
  }
    
    @Test
    public void upnpBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException
    {
        UPnPBindingProcessorTest upnpBindingProcessorTest=new UPnPBindingProcessorTest();
        upnpBindingProcessorTest.startTest();
        this.getBindinProcessor(upnpBindingProcessorTest);
        this.getClientBindingBundle(upnpBindingProcessorTest);
        this.getServerBindingBundle(upnpBindingProcessorTest);
        upnpBindingProcessorTest.testSuite();
        upnpBindingProcessorTest.finishTest();
    }
  
  @Test
  public void wsBindingProcessor() throws NoSuchInterfaceException, UnsupportedBindingComponentException, UnsupportedBindingException, BindingProcessorException, IllegalLifeCycleException, NoBindingKindException
  {
      WSBindingProcessorTest wsBindingProcessorTest=new WSBindingProcessorTest();
      wsBindingProcessorTest.startTest();
      this.getBindinProcessor(wsBindingProcessorTest);
      this.getClientBindingBundle(wsBindingProcessorTest);
      this.getServerBindingBundle(wsBindingProcessorTest);
      wsBindingProcessorTest.testSuite();
      wsBindingProcessorTest.finishTest();
  }
    

    
    
}
