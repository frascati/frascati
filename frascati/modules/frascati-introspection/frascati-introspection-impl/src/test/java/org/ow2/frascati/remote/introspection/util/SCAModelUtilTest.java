/**
 * OW2 FraSCAti Introspection Implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Reference;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.remote.IntrospectionTestSuite;

/**
 *
 */
public class SCAModelUtilTest
{
    private static SCAModelUtilItf scaModelUtil;

    @BeforeClass
    public static void initTest()
    {
        System.out.println("-- SCAModelUtilTest");
        scaModelUtil = IntrospectionTestSuite.getScaModelUtil();
        IntrospectionTestSuite.deployContribution("helloworld-multiple-reference");
    }

    @Test
    public void getTopLevelCompositeName()
    {
        String topLevelCompositeName=scaModelUtil.getTopLevelCompositeName("");
        assertEquals("", topLevelCompositeName);
        topLevelCompositeName=scaModelUtil.getTopLevelCompositeName("componentTopLevel/path/to/sub/components");
        assertEquals("componentTopLevel", topLevelCompositeName);
        topLevelCompositeName=scaModelUtil.getTopLevelCompositeName("/componentTopLevel/path/to/sub/components");
        assertEquals("componentTopLevel", topLevelCompositeName);
        topLevelCompositeName=scaModelUtil.getTopLevelCompositeName("/componentTopLevel");
        assertEquals("componentTopLevel", topLevelCompositeName);
        topLevelCompositeName=scaModelUtil.getTopLevelCompositeName("/componentTopLevel/");
        assertEquals("componentTopLevel", topLevelCompositeName);
    }

    @Test
     public void getProcessingContext()
     {
         ProcessingContext processingContext=scaModelUtil.getProcessingContext("");
         assertNull(processingContext);
         processingContext=scaModelUtil.getProcessingContext("unexistingComponent");
         assertNull(processingContext);
         ProcessingContext runnerMultipleTopLevelprocessingContext=scaModelUtil.getProcessingContext("RunnerMultipleTopLevel");
         assertNotNull(runnerMultipleTopLevelprocessingContext);
         ProcessingContext runnerSimpleprocessingContext=scaModelUtil.getProcessingContext("RunnerSimple");
         assertNotNull(runnerSimpleprocessingContext);
         assertEquals(runnerMultipleTopLevelprocessingContext, runnerSimpleprocessingContext);
         Composite runnerMultipleTopLevelComposite=runnerMultipleTopLevelprocessingContext.getProcessedComposite("RunnerMultipleTopLevel");
         assertNotNull(runnerMultipleTopLevelComposite);
         assertEquals("RunnerMultipleTopLevel", runnerMultipleTopLevelComposite.getName());
         Composite runnerSimpleComposite=runnerSimpleprocessingContext.getProcessedComposite("RunnerSimple");
         assertNotNull(runnerSimpleComposite);
         assertEquals("RunnerSimple", runnerSimpleComposite.getName());
     }
     
    @Test
    public void getTopLevelComposite()
    {
        Composite topLevelComposite=scaModelUtil.getTopLevelComposite("");
        assertNull(topLevelComposite);
        topLevelComposite=scaModelUtil.getTopLevelComposite("unExistingComponent");
        assertNull(topLevelComposite);
        topLevelComposite=scaModelUtil.getTopLevelComposite("RunnerMultipleTopLevel");
        assertNotNull(topLevelComposite);
        assertEquals("RunnerMultipleTopLevel", topLevelComposite.getName());
        topLevelComposite=scaModelUtil.getTopLevelComposite("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple");
        assertNotNull(topLevelComposite);
        assertEquals("RunnerMultipleTopLevel", topLevelComposite.getName());
        topLevelComposite=scaModelUtil.getTopLevelComposite("RunnerMultipleTopLevel/even/if/no/valid/Component");
        assertNotNull(topLevelComposite);
        assertEquals("RunnerMultipleTopLevel", topLevelComposite.getName());
    }
    
    @Test
    public void getComposite()
    {
        Composite composite=scaModelUtil.getComposite("");
        assertNull(composite);
        composite=scaModelUtil.getComposite("unExistingComposite");
        assertNull(composite);
        composite=scaModelUtil.getComposite("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple");
        assertNull(composite);
        composite=scaModelUtil.getComposite("RunnerMultipleTopLevel");
        assertNotNull(composite);
        composite=scaModelUtil.getComposite("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer");
        assertNotNull(composite);
        assertEquals("RunnerMultipleContainer", composite.getName());

        Composite lastComposite=scaModelUtil.getLastComposite("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables");
        assertNotNull(lastComposite);
        assertEquals("RunnerMultipleContainer", lastComposite.getName());
        assertEquals(composite, lastComposite);
    }
    
    @Test
    public void getComponent()
    {
        Component component=scaModelUtil.getComponent("");
        assertNull(component);
        component=scaModelUtil.getComponent("unExistingComponent");
        assertNull(component);
        component=scaModelUtil.getComponent("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables");
        assertNull(component);
        component=scaModelUtil.getComponent("RunnerMultipleTopLevel/Runner");
        assertNotNull(component);
        assertEquals("Runner", component.getName());
        component=scaModelUtil.getComponent("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple");
        assertNotNull(component);
        assertEquals("RunnerMultiple", component.getName());
        
        Component lastComponent=scaModelUtil.getLastComponent("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple/runnables");
        assertNotNull(lastComponent);
        assertEquals("RunnerMultiple", lastComponent.getName());
        assertEquals(component, lastComponent);
    }
    
    @Test
    public void findComponent()
     {
         Composite runnerMultipleTopLevelComposite=scaModelUtil.getComposite("RunnerMultipleTopLevel");
         assertNotNull(runnerMultipleTopLevelComposite);
         
         Component runnerMultipleLevel2=scaModelUtil.findComponent(runnerMultipleTopLevelComposite, "RunnerMultipleLevel2");
         assertNotNull(runnerMultipleLevel2);
         assertEquals("RunnerMultipleLevel2",runnerMultipleLevel2.getName());
         
         Component runner=scaModelUtil.findComponent(runnerMultipleTopLevelComposite.getComponent(), "Runner");
         assertNotNull(runner);
         assertEquals("Runner",runner.getName());
     }

    @Test
    public void findReference()
    {
        ProcessingContext runnerMultipleTopLevelprocessingContext=scaModelUtil.getProcessingContext("RunnerMultipleTopLevel");
        assertNotNull(runnerMultipleTopLevelprocessingContext);
        Composite runnerMultipleTopLevelComposite=runnerMultipleTopLevelprocessingContext.getProcessedComposite("RunnerMultipleTopLevel");
        assertNotNull(runnerMultipleTopLevelComposite);
        
        Reference runnablesTopLevelReference=scaModelUtil.findReference(runnerMultipleTopLevelComposite, "runnablesTopLevel");
        assertNotNull(runnablesTopLevelReference);
        assertEquals("runnablesTopLevel",runnablesTopLevelReference.getName());
        
        Reference runnerTopLevelReference=scaModelUtil.findReference(runnerMultipleTopLevelComposite.getReference(), "runnerTopLevel");
        assertNotNull(runnerTopLevelReference);
        assertEquals("runnerTopLevel",runnerTopLevelReference.getName());
    }
    
    @Test
    public void findComponentReference()
    {
        Component runnerMultipleComponent=scaModelUtil.getComponent("RunnerMultipleTopLevel/RunnerMultipleLevel2/RunnerMultipleLevel1/RunnerMultipleContainer/RunnerMultiple");
        assertNotNull(runnerMultipleComponent);
        assertEquals("RunnerMultiple", runnerMultipleComponent.getName());
        
        ComponentReference runnablesReference=scaModelUtil.findComponentReference(runnerMultipleComponent, "runnables");
        assertNotNull(runnablesReference);
        assertEquals("runnables",runnablesReference.getName());
        
        ComponentReference runnerReference=scaModelUtil.findComponentReference(runnerMultipleComponent.getReference(), "runner");
        assertNotNull(runnerReference);
        assertEquals("runner",runnerReference.getName());
    }
    
    @AfterClass
    public static void undeployTestResources()
    {
        IntrospectionTestSuite.undeployComposite("RunnerMultipleTopLevel");
        IntrospectionTestSuite.undeployComposite("RunnerSimple");
    }
}
