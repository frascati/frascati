/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 *
 */
public class WSBindingProcessorTest extends BindingProcessorTest
{

    public WSBindingProcessorTest()
    {
        super(BindingKind.WS, "helloworld-ws-server/s","helloworld-ws-client/client/printService");
        this.addCompositeToDeploy("helloworld-ws-server", "helloworld-ws-server");
        this.addCompositeToDeploy("helloworld-ws-client", "helloworld-ws-client");
    }

    @Override
    public void startTest() throws NoSuchInterfaceException
    {
        super.startTest();
        assertWSDLIsExposed(true, "http://localhost:9000/HelloService?wsdl");
    }
    
    @Override
    public void testSuite() throws BindingProcessorException,NoSuchInterfaceException, IllegalLifeCycleException, UnsupportedBindingComponentException, NoBindingKindException, UnsupportedBindingException
    {
        this.logEntering("testSuite");
        this.invokeClient("print", this.getClass().getName());
        this.removeClientBinding();
        this.removeServerBinding();

        Map<String,String> serverBindingHints=new HashMap<String, String>();
        serverBindingHints.put("uri", "http://localhost:9299/HelloServiceReconfigured");
        this.addServerBinding(serverBindingHints);
        assertWSDLIsExposed(true,"http://localhost:9299/HelloServiceReconfigured?wsdl");

        Map<String, String> clientBindingHints = new HashMap<String, String>();
        clientBindingHints.put("uri", "http://localhost:9299/HelloServiceReconfigured");
        clientBindingHints.put("wsdlLocation", "http://localhost:9299/HelloServiceReconfigured?wsdl");
        clientBindingHints.put("wsdlElement", "http://annotated.helloworld.examples.frascati.ow2.org/#wsdl.port(HelloService/HelloServicePort)");
        this.addClientBinding(clientBindingHints);
        this.invokeClient("print", this.getClass().getName());
        
        List<BindingAttribute> serverBindingAttributes = new ArrayList<BindingAttribute>();
        BindingAttribute uriBindingAttribute = new BindingAttribute("uri", "http://localhost:9299/HelloServiceReconfiguredAttribute");
        serverBindingAttributes.add(uriBindingAttribute);
        this.setServerAttribute(serverBindingAttributes);
        //FIXME setting wsdl attribute does not work, wsdl is still expose on the previous URL
//        assertWSDLIsExposed(false,"http://localhost:9299/HelloServiceReconfigured?wsdl");
//        assertWSDLIsExposed(true,"http://localhost:9299/HelloServiceReconfiguredAttribute?wsdl");
        
        List<BindingAttribute> setBindingAttributesToSet=new ArrayList<BindingAttribute>();
        BindingAttribute wsdlLocationBindingAttribute=new BindingAttribute("wsdlLocation", "http://localhost:9299/HelloServiceReconfiguredAttribute?wsdl");
        setBindingAttributesToSet.add(wsdlLocationBindingAttribute);
        this.setClientAttribute(setBindingAttributesToSet);
        this.invokeClient("print", this.getClass().getName());
        //the client still works so the setAttribute is not proceed
        
    }
    
    /**
     * Assert assertion an hhtp request on wsdlLocation get a 200 response, ie wsdl is exposed
     * 
     * @param assertion 
     * @param wsdlLocation URL of the wsdl location
     */
    private void assertWSDLIsExposed(boolean assertion, String wsdlLocation)
    {
        URL wsdl = null;
        try
        {
            wsdl = new URL(wsdlLocation);
        } catch (MalformedURLException e)
        {
           fail("assertWSDLExist the provided wsdlLocation : "+wsdlLocation+" is not a valid URL");
        }
        
        int responseCode = 404;
        try
        {
            HttpURLConnection huc = (HttpURLConnection)wsdl.openConnection();
            huc.setRequestMethod("GET"); 
            huc.connect ();
            responseCode=huc.getResponseCode();
        } catch (IOException e)
        {
            fail("assertWSDLExist unable to connect to "+wsdl.getPath());
        } 
       
        logger.info("GET "+wsdlLocation+" HTTP status "+responseCode);
        boolean isResponseCodeOK=HttpURLConnection.HTTP_OK==responseCode;
        assertEquals(assertion,isResponseCodeOK);
    }
}
