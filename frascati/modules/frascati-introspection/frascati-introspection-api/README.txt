FraSCAti introspection module API.

Fractal components are not serializable, so we have to define Plain Old Java Objects (POJO) in order to be able to marshall it and transfer data through the network.
We use JAXB to serialize data and the best way to have correct POJOs with JAXB annotations is to generate these files from an XML schema (XSD).
Writing an XSD is not so easy and this is XML. But Eclipse will help us. By defining our Meta-Model in an ecore file, we will be able to export it to an XML Schema.
Moreover, we will design our Meta-Model with an ecore diagram file like in any other modeler. No more code!

Here are the steps to get POJOs from ou Meta-Model :
1/ With Eclipse modeling, update the sca-resources.ecorediag file
2/ The sca-resources genmodel is already created. We just have to update it: 
    * right-click on sca-resources.genmodel,
    * select reload,
    * select "Ecore model",
    * select the sca-resources.ecore file,
    * select both packages,
    * and click on finish.
3/ Our genmodel is now up to date. We can export it to an XML Schema: 
    * right-click on sca-resources.genmodel,
    * select "export model", 
    * choose "XML Schema",
    * select the destination folder: src/main/resources of the frascati-introspection-api project,
    * select both packages,
    * and click on finish.
4/ Update the sca-resources.xsd header with following information:
<xsd:schema xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" 
  xmlns:frascati-introspection="http://resources.introspection.remote.frascati.ow2.org" 
  xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
  ecore:nsPrefix="frascati-introspection" 
  ecore:package="introspection" 
  targetNamespace="http://resources.introspection.remote.frascati.ow2.org" 
  xmlns:jaxb="http://java.sun.com/xml/ns/jaxb"
  jaxb:version="1.0"
  xmlns:xjc="http://java.sun.com/xml/ns/jaxb/xjc" 
  jaxb:extensionBindingPrefixes="xjc">
  <xsd:annotation>
    <xsd:appinfo>
      <jaxb:globalBindings>
        <xjc:simple />
      </jaxb:globalBindings>
    </xsd:appinfo>
  </xsd:annotation>
  <xsd:import namespace="http://www.eclipse.org/emf/2002/Ecore" schemaLocation="Ecore.xsd"/>

There are 2 fixes in this path:
    * one to fix the path to the Ecore.xsd (replace schemaLocation="platform:/plugin/org.eclipse.emf.ecore/model/Ecore.xsd" by schemaLocation="Ecore.xsd")
    * the other to tell xcj that it has to add @XmlRootElement annotations (see http://weblogs.java.net/blog/kohsuke/archive/2006/03/why_does_jaxb_p.html for more details).
    

    
The maven install target will generate and compile POJOs from the XSD files.