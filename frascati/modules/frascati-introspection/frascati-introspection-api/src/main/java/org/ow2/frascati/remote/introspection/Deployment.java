/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Authors: Christophe Demarey
 * 
 * Contributor(s): Gwenael Cattez
 *
 */

package org.ow2.frascati.remote.introspection;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.remote.introspection.stringlist.StringList;

/**
 * This interface is used to deploy SCA applications (contributions) remotely.
 */
@Service
public interface Deployment
{
    /**
     * Get the composite file names from a zip ( useful because reading jar is complicated for some clients)
     * Http method is POST because of the size of the encodedComposite
     * @return a long string compose with the name of the different composite file name separate by %S%
     */
    @POST
    @Path("/util/getCompositesName")
    @Produces({"text/plain"})
    public String getCompositeEntriesFromJar(@FormParam("jar") String encodedComposite);
    
    /**
     * Deploy a contribution on the FraSCAti runtime.
     * 
     * @param contribution A zip file compliant with the SCA contribution
     *        specification encoded as a Base64 String.
     * @return 0 if the deployment succeed, else -1.
     */
    @POST
    @Path("/deploy/contribution")
    @Produces({"application/xml", "application/json"})
    @Deprecated
    public StringList deployContribution(@FormParam("contribution") String encodedContribution);

    /**
     * Deploy a contribution on the FraSCAti runtime.
     * 
     * @param params A map of parameter that contains at least "contribution" parameter and eventually contextual properties
     * @return A List of deployed composites names
     */
    @POST
    @Path("/deploy/contribution/context")
    @Produces({"application/xml", "application/json"})
    public StringList deployContribution(MultivaluedMap<String, String> params);
    
    /**
     * Deploy a composite on the FraSCAti runtime
     * 
     * @param compositeName the name of the composite file to deploy
     * @param encodedComposite contribution A jar file compliant with the SCA contribution
     *        specification encoded as a Base64 String.
     * @return 0 if the deployment succeed, else -1.
     */
    @POST
    @Path("/deploy/composite")
    @Produces({"text/plain"})
    @Deprecated
    public String deployComposite(@FormParam("compositeName") String compositeName, @FormParam("jar") String encodedComposite);
    
    /**
     * Deploy a composite on the FraSCAti runtime
     * 
     * @param params A map of parameter that contains at least "compositeName" and "jar" parameters and eventually contextual properties
     * @return The name of the deployed composite
     */
    @POST
    @Path("/deploy/composite/context")
    @Produces({"text/plain"})
    public String deployComposite(MultivaluedMap<String, String> params);
    
    /**
     * Undeploy a composite on the Frascati Runtime
     * 
     * @param id of the composite to undeploy
     * @return 0 if remove has been done
     */
    @DELETE
    @Path("/undeploy/composite/{id:.*}")
    @Produces({"text/plain"})
    public int undeployComposite(@PathParam("id") String fullCompositeId);

    
    /**
     * Load a library in the main FraSCAtiCLassloader 
     * The library must be a jar file
     * 
     * @param encodedLib the library file encoded in base64
     */
    @POST
    @Path("/loadLibrary")
    public void loadLibrary(@FormParam("lib") String encodedLib);
}
