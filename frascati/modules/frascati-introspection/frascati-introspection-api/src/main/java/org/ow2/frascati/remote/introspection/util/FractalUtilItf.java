/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;

import java.util.List;
import java.util.logging.Level;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.AttributeController;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
/**
 *
 */
public interface FractalUtilItf
{
    /**
     * Display Fractal component by using FINE logger level
     * 
     * @param component to display
     */
    public void displayComponent(Component component);
    
    /**
     * Display Fractal component
     * 
     * @param level of the logger
     * @param component to display
     */
    public void displayComponent(Level level, Component component);
    
    /**
     * Get the AttributeController of a Component
     * (just a wrapper to avoid NoSuchInterfaceException)
     * 
     * @param component : a fractal component
     * @return the AttributeController of the component or null if NoSuchInterfaceException is caught
     */
    public AttributeController getAttributeController(Component component);
    
    /**
     * Get the BindingController of a Component
     * (just a wrapper to avoid NoSuchInterfaceException)
     * 
     * @param component a fractal component
     * @return the BindingController of the component or null if NoSuchInterfaceException is caught
     */
    public BindingController getBindingController(Component component);

    /**
     * Get the SCAPropertyController related to component
     * 
     * @param component
     * @return the SCAPropertyController
     * @throws NoSuchInterfaceException if the SCAPropertyController is not found
     */
    public SCAPropertyController getSCAPropertyController(Component component) ;
    
    /**
     * Get the name of a fractal Component 
     * (just a wrapper to avoid NoSuchInterfaceException)
     * 
     * @param component a fractal component
     * @return The name of of the component or "undefined" if NoSuchInterfaceException is caught when retrieving NameController of the component
     */
    public String getComponentName(Component component);
    
    
    /**
     * Get the status of a fractal Component 
     * (just a wrapper to avoid NoSuchInterfaceException)
     * 
     * @param component a fractal component
     * @return The name of of the component or "undefined" if NoSuchInterfaceException is caught when retrieving LifeCycleController of the component
     */
    public String getComponentStatus(Component component);

    /**
     * Check if a Component have a ContentController i.e is a Primitive component
     * 
     * @param component
     * @return true if the component is a Primitive component, else false
     */
    public boolean isPrimitiveComponent(Component component);
    
    /**
     * Check if a fractal component is a SCa component i.e check if fractal component have a interface named sca-component-controller
     * 
     * @param component
     * @return true if the component is a SCA component, else false
     */
    public boolean isSCAComponent(Component component);
    
    /**
     * Check if a fractal interface is a controller interface
     * ie if it's a server interface named *-controller
     * 
     * @param itf fractal interface
     * @return true if fractal interface is a controller interface, false otherwise
     */
    public boolean isFractalControllerInterface(Interface itf);
    
    
    /**
     * Check if a fractal interface is a multiple reference instance
     * ie check interface name match "baseMultipleReferenceName-Digit"
     * and interface owner have an multiple interface baseMultipleReferenceName
     * 
     * @param itf fractal interface
     * @return true if fractal interface is a controller interface, false otherwise
     */
    public boolean isCollectionInterfaceInstance(Interface itf);
    
    /**
     * Get a reference to the fractal component designated by id.
     * 
     * @param clearedComponentId The full component id (ex: /a/b/d)
     * @return the reference to the fractal component designated by id
     * @throws NoSuchInterfaceException if the component is not found
     */
    public Component getComponent(String componentId) throws NoSuchInterfaceException;

    
    /**
     * Get a reference to the fractal Component owner of the fractal Interface designed by itfId
     * 
     * @param compositeManager where to search for the top level composite
     * @param itfId Id of a Fractal Interface
     * @return the Component owner of the Interface 
     * @throws NoSuchInterfaceException if the component is not found
     */
    public Component findComponentByItfId(String itfId) throws NoSuchInterfaceException;

    
    /**
     * Get a reference to the fractal interface designed by id
     * 
     * @param compositeManager where to search for the top level composite
     * @param itfId Id of a fractal interface to look for
     * @return The fractal interface designed by itfIf 
     * @throws NoSuchInterfaceException if the Interface does not exist
     */
    public Interface getInterface(String itfId) throws NoSuchInterfaceException;
    
    /**
     * Check if a fractal interface is a SCa reference
     * 
     * @param itf
     * @return true if the interface is a SCA reference, else false
     */
    public boolean isSCAReference(Interface itf);
    
    /**
     * Get subComponents of a Fractal Component
     * 
     * @param owner the component to introspect
     * @return a list of all subComponents
     */
    public List<Component> getSubComponents(Component parent);
    
    /**
     * Get a reference to the fractal Component named subComponentName owner by fractal Component parent
     * 
     * @param parent the Component to introspect
     * @param subComponentName the name of the searched subComponent 
     * @return the subcomponent named subComponentName or null if don't exist
     * @throws NoSuchInterfaceException if the parent component is a primitive Component, i.e it have no ContentController
     */
    public Component getSubComponent(Component parent, String subComponentName) throws NoSuchInterfaceException;
    
    /**
     * TODO something look strange, it seems that its not the subComponent but all children at the same level
     * Get subComponents of a Fractal Component
     * 
     * @param owner the component to introspect
     * @return a list of all subComponents
     * @throws NoSuchInterfaceException
     */
    public List<Component> getChildren(Component owner) throws NoSuchInterfaceException;
    
    /**
     * Util method to start component
     * 
     * @param owner The fractal component to start
     * @throws NoSuchInterfaceException
     * @throws IllegalLifeCycleException
     */
    public void startComponent(Component owner) throws NoSuchInterfaceException, IllegalLifeCycleException;
    
    /**
     * Util method to stop component
     * 
     * @param owner The fractal component to stop
     * @throws NoSuchInterfaceException
     * @throws IllegalLifeCycleException
     */
    public void stopComponent(Component owner) throws NoSuchInterfaceException, IllegalLifeCycleException;
    
    /**
     * Util to find an Fractal interface on a Fractal component and encapsulates
     * the Exception
     * 
     * @param owner The Fractal component holding the Fractal interface
     * @param itfId The name of the Fractal interface
     * @return a Fracatal interface resource
     * @throws NoSuchInterfaceException 
     */
    public Interface findInterfaceById(Component owner, String itfId) throws NoSuchInterfaceException;
    
    
    /**
     * Get the object bound to a client fractal interface
     * 
     * @param clientItf interface of the client
     * @return The bounded object or null if not found
     */
    public Object getServerInterface(Interface clientItf);
    
    
    /**
     * Get a list of all fractal interfaces bound to a server fractal interface
     * 
     * @param serverItf interface of the server 
     * @return 
     */
    public List<Interface> getClientInterfaces(Interface serverItf);
    
    
    /**
     * Get the name of the last interface bound to a collection Reference define by collectionName
     * i.e collectionName-(highest index found)
     * 
     * @param owner Owner of the collection
     * @param collectionName name of the collection
     * @return The last interfaceName bound to Collection
     */
    public String getLastInterfaceCollectionName(Component owner, String collectionName);
    
    /**
     * If the interface is a collection we have to set the name of the bind
     * reference We look for other interface, in the owner component of the
     * interface, that have a name that match pattern interfaceName-number
     * The name of the new interface is interfaceName-(highest number found
     * + 1)
     * 
     * @param owner owner Owner of the collection
     * @param collectionName name of the collection
     * @return The next interfaceName to bound to Collection
     */
    public String getNextInterfaceName(Component owner, String collectionName);
}
