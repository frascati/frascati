/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf;

/**
 *
 */
public class BindingBundle
{
    private HashMap<BindingProcessorItf,List<Object>> processorMap;
    private boolean isReferenceBinding;

    public BindingBundle()
    {
        this.processorMap=new HashMap<BindingProcessorItf, List<Object>>();
        this.isReferenceBinding=false;
    }

    public void setReferenceBinding(boolean isReferenceBinding)
    {
        this.isReferenceBinding = isReferenceBinding;
    }

    public boolean isReferenceBinding()
    {
        return isReferenceBinding;
    }
    
    public void addBinding(BindingProcessorItf processor, Object boundedObject)
    {
        List<Object> boundedObjects=processorMap.get(processor);
        if(boundedObjects==null)
        {
            boundedObjects=new ArrayList<Object>();
            processorMap.put(processor, boundedObjects);
        }
        boundedObjects.add(boundedObject);
    }
    
    public boolean isEmpty()
    {
        return processorMap.isEmpty();
    }
    
    public List<BindingProcessorItf> getProcessors()
    {
        return new ArrayList<BindingProcessorItf>(processorMap.keySet());
    }
    
    public List<Object> getBoundedObjects()
    {
        List<Object> boundedObjects=new ArrayList<Object>();
        for(BindingProcessorItf processor : processorMap.keySet())
        {
            boundedObjects.addAll(processorMap.get(processor));
        }
        return boundedObjects;
    }
    
    public List<Object> getBoundedObjects(BindingProcessorItf processor)
    {
        return processorMap.get(processor);
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder=new StringBuilder("BindingBundle for ");
        stringBuilder.append(isReferenceBinding?"reference":"service");
        stringBuilder.append(" :\n");
        for(BindingProcessorItf key : processorMap.keySet())
        {
            stringBuilder.append("\t"+key.getBindingKind().name()+"\n");
            for(Object o : processorMap.get(key))
            {
                stringBuilder.append("\t\t"+o.getClass().getName()+"\n");
            }
        }
        return stringBuilder.toString();
    }
    
    
}
