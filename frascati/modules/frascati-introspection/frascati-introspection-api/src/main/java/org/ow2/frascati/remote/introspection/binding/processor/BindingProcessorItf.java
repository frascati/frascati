/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding.processor;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.ow2.frascati.remote.introspection.binding.BindingAttribute;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 * Interface for Introspection Binding processors
 */
public interface BindingProcessorItf
{
    
    /**
     * @return the identifier of the kind of binding processed (WS,RMI,REST....)
     */
    public BindingKind getBindingKind();
    
    /**
     * Create a binding for fractal named interface referenceName own by referenceItf's owner based on stringHints
     * 
     * @param referenceItf the fractal interface to bind
     * @param referenceName the name of the interface to bind
     * @param stringHints parameters for the new Binding
     * @throws BindingProcessorException if a problem occurred during binding phase
     */
    public void bindReference(Interface referenceItf, String referenceName, Map<String,String> stringHints) throws BindingProcessorException;
    
    
    /**
     * Export a service for fractal named serviceName interfaceName own by serviceItf's owner based on stringHints
     * 
     * @param serviceItf the fractal interface to export
     * @param serviceName the name of the interface to export
     * @param stringHints parameters for the new Binding
     * @throws BindingProcessorException if a problem occurred during export phase
     */
    public void exportService(Interface serviceItf, String serviceName, Map<String,String> stringHints) throws BindingProcessorException;
    
    
    /**
     * Unbind boundedObject from fractal interface named referenceName own by owner 
     * 
     * @param owner of the reference
     * @param referenceName name of the reference to unbind
     * @param boundedObject the object bound to the reference interface
     * @throws BindingProcessorException if a problem occurred during unbind phase
     */
    public void unbindReference(Component owner, String referenceName, Object boundedObject) throws BindingProcessorException;
    
    /**
     * Unexport boundedObject from fractal interface named serviceName own by owner
     * 
     * @param owner of the service
     * @param serviceName name of the service to unexport
     * @param boundedObject the object bound to the service interface
     * @throws BindingProcessorException if a problem occurred during unexport phase
     */
    public void unexportService(Component owner, String serviceName, Object boundedObject) throws BindingProcessorException;
    
    /**
     * Set attribute of an Object bound to fractal reference
     * 
     * @param boundedObject the object bound to the fractal interface
     * @param attribute the name of the attribute to set
     * @param newValue the new value to set
     * @throws BindingProcessorException if a problem occurred during set attribute phase
     */
    public void setAttribute(Object boundedObject, String attributeName, String newValue) throws BindingProcessorException;
    
    
    /**
     * Get the list of @see org.ow2.frascati.remote.introspection.binding.BindingAttribute related to Object bound to fractal interface
     * 
     * @param boundedObject the object bound to the fractal interface
     * @return List of the BindingAttribute related to boundedObject
     * @throws BindingProcessorException if a problem occurred during get attribute phase
     */
    public List<BindingAttribute> getAttributes(Object boundedObject) throws BindingProcessorException;
    
    /**
     * @param skelton a skelton Object bound to a fractal interface
     * @return true if the skelton is processed by this BindingProcessorItf, false otherwise
     */
    public boolean isProcessedService(Object skelton);
    
    
    /**
     * @param stub a stub Object bound to a fractal interface
     * @return true if the stub is processed by this BindingProcessorItf, false otherwise
     */
    public boolean isProcessedReference(Object stub);
}
