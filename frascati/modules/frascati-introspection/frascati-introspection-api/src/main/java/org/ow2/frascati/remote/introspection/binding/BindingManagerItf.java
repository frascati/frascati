/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding;

import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.remote.introspection.binding.exception.NoBindingKindException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingComponentException;
import org.ow2.frascati.remote.introspection.binding.exception.UnsupportedBindingException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorException;
import org.ow2.frascati.remote.introspection.binding.processor.BindingProcessorItf;
import org.ow2.frascati.remote.introspection.resources.BindingKind;

/**
 * TODO doc
 */
public interface BindingManagerItf
{

    /**
     * Get the BindingProcessor for kind (WS,REST,SCA...)
     * 
     * @param kind of processed binding 
     * @return BindingProcessorItf for king
     * @throws UnsupportedBindingException if no Processor is found
     */
    public BindingProcessorItf getBindingProcessor(String kind) throws UnsupportedBindingException;
    
    /**
     * Get the BindingProcessor for kind (WS,REST,SCA...)
     * 
     * @param BindingKind of processed binding 
     * @return BindingProcessorItf for bindingKind
     * @throws UnsupportedBindingException if no BindingProcessor is found
     */
    public BindingProcessorItf getBindingProcessor(BindingKind bindingKing) throws UnsupportedBindingException;
    
    /**
     * Get the BindingProcessor used to generate the component Object
     * ie, give the BindingProcessor related to a stub or skelton object
     * 
     * @param component the stub or skelton from a Binding 
     * @return BindingProcessorItf used to process component
     * @throws UnsupportedBindingComponentException if no BindingProcessor is found
     */
    public BindingProcessorItf getBindingProcessor(Object component) throws UnsupportedBindingComponentException;
    
    /**
     * Get the BindingBundle related to a fractal Interface
     * This bundle contains description of all Binding found for itf
     *  @see org.ow2.frascati.remote.introspection.binding.BindingBundle
     * 
     * @param itf fractal interface 
     * @return BindingBundle related to itf
     * @throws NoSuchInterfaceException TODO
     */
    public BindingBundle getBindingBundle(Interface itf) throws NoSuchInterfaceException;

    
    /**
     * Add a binding to a fractal interface based on hints
     * 
     * @param itf the fractal interface to bind
     * @param itfPath the full path of the interface
     * @param stringHints the parameters of the binding to add
     * @throws NoBindingKindException if no kind parameter is found in hints 
     * @throws UnsupportedBindingException if no BindingProcessor can be found for kind
     * @throws BindingProcessorException if problems occur during binding instantiation
     */
    public void bind(Interface itf, String itfPath, Map<String,String> stringHints) throws NoBindingKindException, UnsupportedBindingException, BindingProcessorException;

    /**
     * Remove the binding found at position on fractal interface 
     * 
     * @param itf the fractal interface
     * @param itfPath the full path of the interface
     * @param position position of the binding
     * @throws BindingProcessorException if problems occur during unbinding phase
     * @throws UnsupportedBindingComponentException if no BindingProcessor can be found to process unbind
     */
    public void unbind(Interface itf,String itfPath, int position) throws BindingProcessorException,UnsupportedBindingComponentException;
    
    /**
     * Remove the binding with hints stringHints found on a fractal interface 
     * 
     * @param itf the fractal interface
     * @param itfPath the full path of the interface
     * @param stringHints hints of the binding to remove
     * @throws BindingProcessorException if problems occur during unbinding phase
     * @throws UnsupportedBindingComponentException if no BindingProcessor can be found to process unbind
     */
    public void unbind(Interface itf,String itfPath, Map<String,String> stringHints) throws NoBindingKindException, UnsupportedBindingException, BindingProcessorException;
    
    /**
     * Set attribute value to newValue on binding found on fractal interface at position
     * 
     * @param itf the fractal interface
     * @param position position of the binding 
     * @param attribute the name of the attribute to set
     * @param newValue the new value for attribute
     * @throws BindingProcessorException  if problems occur during setting attribute phase
     * @throws UnsupportedBindingComponentException if no BindingProcessor can be found to process set attribute action
     */
    public void setBindingAttribute(Interface itf, int position, String attribute, String newValue) throws BindingProcessorException,UnsupportedBindingComponentException;
    
    /**
     * Set attribute value to newValue on binding with hints stringHints on fractal interface
     * 
     * @param itf the fractal interface
     * @param stringHints hints of the binding to set attribute
     * @param attribute the name of the attribute to set
     * @param newValue the new value for attribute
     * @throws BindingProcessorException  if problems occur during setting attribute phase
     * @throws UnsupportedBindingComponentException if no BindingProcessor can be found to process set attribute action
     */
    public void setBindingAttribute(Interface itf, Map<String,String> stringHints, String attribute, String newValue) throws NoBindingKindException, UnsupportedBindingException, BindingProcessorException;
    
    
    /**
     * Get a list of the interface own by a binding component and bounded to itf
     * 
     * @param itf a fractal interface
     * @return a List of binding interface bounded to itf
     */
    public List<Interface> getBindingInterfaces(Interface itf);
}
