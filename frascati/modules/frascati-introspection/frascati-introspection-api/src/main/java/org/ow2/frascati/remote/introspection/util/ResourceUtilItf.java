/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.remote.introspection.util;

import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;

/**
 * Utility class used to convert objects from Fractal API to REST (SCA)
 * resources.
 * 
 * @author Gwenael Cattez
 */
public interface ResourceUtilItf
{
    /**
     * Get full information on a fractal Component.
     * 
     * @param comp A fractal component to introspect.
     * @return A Component Resource or null if comp cannot be introspect.
     */
    public Component getFullComponentResource(org.objectweb.fractal.api.Component comp);
    
    /**
     * Get full information on a fractal Component.
     * 
     * @param componentId id of this component 
     * @param fracatlComponent A fractal component to introspect.
     * @return A Component Resource or null if comp cannot be introspect.
     */
    public Component getFullComponentResource(String componentId, org.objectweb.fractal.api.Component fracatlComponent);
    
    /**
     * Get a Component resource from a fractal Component.
     * 
     * @param comp The fractal component to map.
     * @return A Component Resource or null if comp cannot be introspect.
     */
    public Component getComponentResource(org.objectweb.fractal.api.Component comp);

    /**
     * Get a Port (Service, Reference) resources from a fractal interface
     * 
     * @param portPath path to the port
     * @param itf the Fractal interface
     * @return Port related to itf
     */
    public Port getPortResource(String portPath, org.objectweb.fractal.api.Interface itf);
    
    /**
     * Get a Port (Service, Reference) resources from a fractal interface
     * 
     * @param itf the Fractal interface
     * @return Port related to itf
     */
    public Port getPortResource(org.objectweb.fractal.api.Interface itf);
    
    /**
     * Get a Property resource related to a component property
     * 
     * @param component The fractal component to map.
     * @param propertyName name of the property
     * @return a Property resource or null if no SCAPropertyController can be found for component
     */
    public Property getPropertyResource(org.objectweb.fractal.api.Component component, String propertyName);
    
}