/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle, Gwenael Cattez
 *
 */
package org.ow2.frascati.remote.introspection;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MultivaluedMap;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;

/**
 * This interface is used to expose / introspect an SCA runtime remotely.
 */
@Service
public interface RemoteScaDomain
{
    /**
     * Get the Fractal component for a given name path.
     * @return the Fractal component for the given name path.
     */
    org.objectweb.fractal.api.Component getFractalComponent(String fullComponentId);

    /**
     * Get the list of top-level composites running in this SCA domain.
     * @return the list of composite.
     */
    @GET
    @Path("/components")
    @Produces({"application/xml", "application/json"})
    public Collection<Component> getDomainComposites();
    
    /**
     * Get full information on the component given as parameter. 
     * @param id The full component id (ex: /a/b/d)
     * @return a Component resource.
     */
    @GET
    @Path("/component/{id:.*}")
    @Produces({"application/xml", "application/json"})
    public Component getComponent(@PathParam("id") String fullComponentId);

    /**
     * Get children of the component given as parameter. 
     * @param id The full component id (ex: /a/b/d)
     * @return a list of Component resources.
     */
    @GET
    @Path("/component/{id:.*}/subcomponents")
    @Produces({"application/xml", "application/json"})
    public Collection<Component> getSubComponents(@PathParam("id") String fullComponentId);
    
    /**
     * Get full information on the port give as parameter
     * @param id The full port id (ex: /a/b/d)
     * @return a Port resource.
     */
    @GET
    @Path("/port/{id:.*}")
    @Produces({"application/xml", "application/json"})
    public Port getInterface(@PathParam("id") String fullInterfaceId);
    
    /**
     * Invoke a method on the port give as parameter
     * 
     * @param id The full port id (ex: /a/b/d)
     * @param params contain the name of the method to invoke (methodName) and the parameters (Parameter+index, eg Parameter0,Parameter1)
     * @return a String representing the response of the invocation
     */
    @POST
    @Path("/port/{id:.*}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public String invokeMethod(@PathParam("id") String fullInterfaceId ,MultivaluedMap<String, String> params);

    
    /**
     * Start composite,component, give as parameter
     * @param id The full composite,component id (ex: /a/b/d)
     * @return true if component is started
     */
    @POST
    @Path("/component/{id:.*}/start")
    public boolean startComponent(@PathParam("id") String fullComponentId);

    
    /**
     * Stop composite,component, give as parameter
     * @param id The full composite,component id (ex: /a/b/d)
     * @return true if component is stopped
     */
    @POST
    @Path("/component/{id:.*}/stop")
    public boolean stopComponent(@PathParam("id") String fullComponentId);

    
    /**
     * Get type and value of a property give as parameters
     * @param componentPath The full component id (ex: /a/b/d) that hold the property
     * @param propertyName the name of the property
     * @return a Property resource
     */
    @GET
    @Path("/property/{componentPath:.*}/{propertyName}")
    @Produces({"application/xml", "application/json"})
    public Property getProperty(@PathParam("componentPath") String fullComponentId, @PathParam("propertyName") String propertyName);

    /**
     * Set value of a property give as parameters
     * @param componentPath The full component id (ex: /a/b/d) that hold the property
     * @param propertyName The name of the property
     * @param value The new value of the property
     */
    @POST
    @Path("/property/{componentPath:.*}/{propertyName}")
    @Produces({"text/plain"})
    public void setProperty(@PathParam("componentPath") String fullComponentId, @PathParam("propertyName") String propertyName, @FormParam("value") String value);
    
    /**
     * Create a binding on a Fractal interface
     * 
     * @param id, id of the fractal interface that will hold the binding
     * @param params, parameters to build the binding containing the kind of binding to create and the specifics parameters
     */
    @POST
    @Path("/binding/{id:.*}/")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public void addBinding(@PathParam("id") String fullInterfaceId ,MultivaluedMap<String, String> params);
    
    /**
     * DEPRECATED
     * 
     * Remove a binding at a position from a Fractal interface
     * 
     * @param id, id of the fractal interface that hold the binding
     * @param position, position of the binding in the binding list of the interface
     */
    @DELETE
    @Path("/binding/{id:.*}/")
    @Produces({"text/plain"})
    public void removeBinding(@PathParam("id") String fullInterfaceId , @QueryParam("position") String position);
    
    /**
     * Remove a binding at a position from a Fractal interface
     * 
     * @param id, id of the fractal interface that hold the binding
     * @param params the parameter of the binding to remove
     */
    @DELETE
    @Path("/binding/tocome/{id:.*}/")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public void removeBinding(@PathParam("id") String fullInterfaceId ,MultivaluedMap<String, String> params);
    
    /**
     * Set an attribute of a binding hold by a Fractal Component
     * 
     * @param id, id of the fractal interface that will be modified
     * @param position , position of the binding in the binding list of the interface
     * @param attribute, the name of the attribute to change
     * @param newValue, the new value to assign
     */
    @PUT
    @Path("/binding/{id:.*}/")
    @Produces({"text/plain"})
    public void setBindingAttribute(@PathParam("id") String fullInterfaceId ,@QueryParam("position") String position,@QueryParam("attribute") String attribute,@QueryParam("newValue") String newValue);
    
    /**
     * Set an attribute of a binding hold by a Fractal Component
     * 
     * @param id, id of the fractal interface that will be modified
     * @param position , position of the binding in the binding list of the interface
     * @param attribute, the name of the attribute to change
     * @param newValue, the new value to assign
     */
    @PUT
    @Path("/binding/tocome/{id:.*}/{attribute}/{newValue}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public void setBindingAttribute(@PathParam("id") String fullInterfaceId ,MultivaluedMap<String, String> params,@PathParam("attribute") String attribute,@PathParam("newValue") String newValue);
}
