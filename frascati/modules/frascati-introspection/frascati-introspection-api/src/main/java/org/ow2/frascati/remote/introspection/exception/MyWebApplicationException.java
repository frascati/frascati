/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.exception;

import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This Exception can be throw to a client via http protocol (send an https
 * status and message)
 * 
 */
public class MyWebApplicationException extends WebApplicationException
{

    private static final long serialVersionUID = -6209569322081835480L;

    /**
     * The default HTTP status to send
     */
    private final static int DEFAULT_RESPONSE = 400;

    /**
     * The logger
     */
    protected final static Logger logger = Logger.getLogger(MyWebApplicationException.class.getCanonicalName());

    /**
     * @param status HTTP status to send
     * @param message The message to send
     */
    public MyWebApplicationException(int status, String message)
    {
        super(Response.status(status).entity(message).type(MediaType.TEXT_PLAIN).build());
        logger.severe("Error " + status + " " + MyWebApplicationException.class.getSimpleName() + " : " + message);
    }

    /**
     * @param exception The original Exception use to compose the message to send
     * @param status HTTP status to send
     * @param message A part of the message to send
     */
    public MyWebApplicationException(Exception exception, int status, String message)
    {
        super(exception, Response.status(status).entity(message).type(MediaType.TEXT_PLAIN).build());
        logger.severe("Error " + status + " " + exception.getClass().getSimpleName() + " : " + message);
        /**print the stack trace at logger.FINE level*/
        for(StackTraceElement stackTraceElement : exception.getStackTrace())
        {
            logger.finest(stackTraceElement.toString());
        }
    }

    /**
     * @param message The message to send
     */
    public MyWebApplicationException(Exception exception)
    {
        this(exception,exception.getMessage());
    }
    
    /**
     * @param message The message to send
     */
    public MyWebApplicationException(String message)
    {
        this(DEFAULT_RESPONSE, message);
    }

    /**
     * @param exception The original Exception use to compose the message to send
     * @param message A part of the message to send
     */
    public MyWebApplicationException(Exception exception, String message)
    {
        this(exception, DEFAULT_RESPONSE, message);
    }

}
