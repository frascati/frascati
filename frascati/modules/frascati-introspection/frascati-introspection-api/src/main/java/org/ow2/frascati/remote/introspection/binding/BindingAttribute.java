/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.binding;


/**
 *
 */
public class BindingAttribute
{
    private String name;
    private String clazz;
    private String value;
    
    public BindingAttribute(String name, String value)
    {
        this.name=name;
        this.value=value;
    }
    
    public BindingAttribute(String name, Class<?> clazz, Object value)
    {
        this.name=name;
        this.clazz=clazz.getName();
        if(value==null)
        {
            this.value="undefined value";
        }
        else
        {
            this.value=value.toString();
        }
    }
    
    public String getName()
    {
        return name;
    }

    public String getClazz()
    {
        return clazz;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj==null || !(obj instanceof BindingAttribute))
        {
            return false;
        }
        BindingAttribute bindingAttribte=(BindingAttribute) obj;
        boolean isNameEquals= this.name.equals(bindingAttribte.getName());
        boolean isValueEquals=this.value.equals(bindingAttribte.getValue());
        return isNameEquals && isValueEquals;
    }
    
    
}
