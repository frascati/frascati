/**
 * OW2 FraSCAti Introspection API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.remote.introspection.util;

import java.util.List;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

/**
 *
 */
public interface SCAModelUtilItf
{
    /**
     * Get the name of top level composite of this path, ie the first name of the path
     * 
     * @param path to a fractal object
     * @return the first name in the path 
     */
    public  String getTopLevelCompositeName(String path);
    
    /**
     * Get the ProcessingContext used to proceed the composite related to the path
     * Get the top level composite name in the path and try to retrieve the associated 
     * Processing Context from the AssemblyFactory
     * 
     * @param path to a fractal object
     * @return the related ProcessingContext or null if not found
     */
    public ProcessingContext getProcessingContext(String path);
    
    /**
     * Get the top level Composite related to the path
     * 
     * @param path to a fractal object
     * @return top level Composite or null if not found
     */
    public Composite getTopLevelComposite(String path);
    
    /**
     * Get the Composite related to path
     * 
     * @param path to the Composite
     * @return Composite or null if not found
     */
    public Composite getComposite(String path);
    
    /**
     * Get the last Composite found in the path
     * 
     * @param path to a fractal object
     * @return a Composite or null if nothing found
     */
    public  Composite getLastComposite(String path);
    
    /**
     * Get the Component related to path
     * 
     * @param path to the Component
     * @return a Component or null if not found
     */
    public  Component getComponent(String path);
    
    /**
     * Get the last Component found in the path
     * 
     * @param path to a fractal object
     * @return a Component or null if nothing found
     */
    public  Component getLastComponent(String path);
    
    /**
     * Util method to extract a Reference from a list based on its name
     * 
     * @param references the list to inspect
     * @param referenceName the name of the searched reference
     * @return the Reference named referenceName or null if not found
     */
    public  Reference findReference(List<Reference> references, String referenceName);

    /**
     * Util method to extract a Reference from a composite based on its name
     * 
     * @param composite the composite to inspect
     * @param referenceName the name of the searched reference
     * @return the Reference named referenceName or null if not found
     */
    public  Reference findReference(Composite composite, String referenceName);
    
    
    /**
     * Util method to extract a Component from a list based on its name
     * 
     * @param components the list to inspect
     * @param componentName the name of the searched component
     * @return the Component named componentName or null if not found
     */
    public  Component findComponent(List<Component> components, String componentName);
    
    /**
     * Util method to extract a Component from a Composite based on its name
     * 
     * @param composite the composite to inspect
     * @param componentName the name of the searched component
     * @return the Component named componentName or null if not found
     */
    public  Component findComponent(Composite composite, String componentName);
    
    
    /**
     * Util method to extract a ComponentReference from a list based on its name
     * 
     * @param componentReferences the list to inspect
     * @param referenceName the name of the searched reference
     * @return the ComponentReference named referenceName or null if not found
     */
    public  ComponentReference findComponentReference(List<ComponentReference> componentReferences, String referenceName);
    
    /**
     * Util method to extract a ComponentReference from a Component based on its name
     * 
     * @param component the component to inspect
     * @param referenceName the name of the searched reference
     * @return the ComponentReference named referenceName or null if not found
     */
    public  ComponentReference findComponentReference(Component component, String referenceName);
   
}
