/**
 * OW2 FraSCAti: SCA Interface Native
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.native_;

import static java.util.logging.Level.FINE;
import static java.util.logging.Level.INFO;
import static java.util.logging.Level.SEVERE;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.util.AbstractLoggeable;

import com.ochafik.lang.jnaerator.JNAerator;
import com.ochafik.lang.jnaerator.Result;
import com.ochafik.lang.jnaerator.SourceFiles;
import com.ochafik.lang.jnaerator.JNAerator.Feedback;
import com.ochafik.lang.jnaerator.JNAeratorConfig;

/**
 * OW2 FraSCAti Assembly Factory JNAerator compiler implementation.
 * 
 * @author <a href="mailto:romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public class JNAeratorCompiler
     extends AbstractLoggeable
  implements NativeCompiler
{
    private final JNAeratorConfig cfg = new JNAeratorConfig();
    private final Feedback feedback = new FraSCAtiCompilerFeedback(log);
    private final JNAerator generator = new JNAerator(this.cfg);

    @Init
    public final void init()
    {
        this.cfg.compile = false;
        this.cfg.skipLibraryInstanceDeclarations = true;
        this.cfg.followIncludes = true;
        this.cfg.runtime = JNAeratorConfig.Runtime.JNA;
        this.cfg.autoConf = true;
    }

    /**
     * @see org.ow2.frascati.native_.NativeCompiler#interfaceName(java.io.File)
     */
    public final String interfaceName(File file)
    {
        String itf = packageName(file);
        return Character.toUpperCase(itf.charAt(0))+itf.substring(1)+"Library";
    }

    /**
     * @see org.ow2.frascati.native_.NativeCompiler#packageName(java.io.File)
     */
    public final String packageName(File file)
    {
        return file.getName().substring(0, file.getName().indexOf('.'));
    }

    /**
     * @see org.ow2.frascati.native_.NativeCompiler#compile(java.io.File, java.io.File)
     */
    public final void compile(File file, File output) throws IOException
    {
        this.cfg.outputDir = output;
        if (!this.cfg.outputDir.exists()) {
            this.cfg.outputDir.mkdirs();
        }
        this.cfg.addSourceFile(file, packageName(file), true);
        this.generator.jnaerate(this.feedback);
    }

    private static final class FraSCAtiCompilerFeedback implements Feedback
    {
        private final Logger logger;

        public FraSCAtiCompilerFeedback(Logger log)
        {
            this.logger = log;
        }

        /**
         * @see com.ochafik.lang.jnaerator.JNAerator.Feedback#setFinished(java.io.File)
         */
        public void setFinished(File file)
        {
            if (this.logger.isLoggable(INFO)) {
                this.logger.info("File '" + file.getName()
                        + "' has been successfully generated.");
            }
        }

        /**
         * @see com.ochafik.lang.jnaerator.JNAerator.Feedback#setFinished(java.lang.Throwable)
         */
        public void setFinished(Throwable error)
        {
            if (this.logger.isLoggable(SEVERE)) {
                this.logger.log(SEVERE, "File failed to generate.", error);
            }
        }

        /**
         * @see com.ochafik.lang.jnaerator.JNAerator.Feedback#setStatus(java.lang.String)
         */
        public void setStatus(String status)
        {
            if (this.logger.isLoggable(FINE)) {
                this.logger.fine("Status is '" + status + "'");
            }
        }

        /**
         * @see com.ochafik.lang.jnaerator.JNAerator.Feedback#sourcesParsed(com.ochafik.lang.jnaerator.SourceFiles)
         */
        public void sourcesParsed(SourceFiles sources)
        {
            if (this.logger.isLoggable(FINE)) {
                this.logger.fine("Parsed sources are: "
                        + sources.getNameSpace());
            }
        }

        /**
         * @see com.ochafik.lang.jnaerator.JNAerator.Feedback#wrappersGenerated(com.ochafik.lang.jnaerator.Result)
         */
        public void wrappersGenerated(Result result)
        {
            if (this.logger.isLoggable(FINE)) {
                this.logger.fine("Generated wrappers are: " + result);
            }
        }
    }
}
