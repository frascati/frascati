/**
 * OW2 FraSCAti: SCA Interface Native
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.native_;

import static java.util.logging.Level.INFO;

import java.io.File;
import java.io.IOException;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractInterfaceProcessor;
import org.ow2.frascati.component.factory.api.MembraneGeneration;

/**
 * OW2 FraSCAti Assembly Factory SCA interface Native processor class.
 * 
 * @author <a href="mailto:romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.3
 */
public class FraSCAtiInterfaceNativeProcessor
     extends AbstractInterfaceProcessor<NativeInterface>
{
    // ---------------------------------------------------------------------------
    // Internal state.
    // ---------------------------------------------------------------------------

    @Reference(name = "native-compiler")
    private NativeCompiler compiler;

    @Property(name = "target-directory")
    private String targetDir = "native";

    // ---------------------------------------------------------------------------
    // Internal methods.
    // ---------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(ElementType, StringBuilder)
     */
    @Override
    protected final void toStringBuilder(NativeInterface nativeItf, StringBuilder sb)
    {
        sb.append("native:interface.native");
        append(sb, "descriptor", nativeItf.getDescriptor());
        super.toStringBuilder(nativeItf, sb);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#doCheck(org.eclipse.emf.ecore.EObject, org.ow2.frascati.assembly.factory.api.ProcessingContext)
     */
    @Override
    protected final void doCheck(NativeInterface nativeItf,
            ProcessingContext processingContext) throws ProcessorException
    {
        // Check the attribute 'descriptor'
        String desc = nativeItf.getDescriptor();
        if (desc == null || desc.equals("")) {
            error(processingContext, nativeItf,
                    "The attribute 'descriptor' must be set");
            return;
        }

        File file = new File(desc);
        if (!file.exists()) {
            log.severe("The descriptor '" + desc
                    + "' does not exist on the system.");
        } else if (!file.canRead()) {
            log.severe("The descriptor '" + desc + "' is not readable.");
        }

        String javaInterface = this.compiler.packageName(file) + "."
                + this.compiler.interfaceName(file);
        Class<?> clazz = null;
        try {
            clazz = processingContext.loadClass(javaInterface);
        } catch (ClassNotFoundException cnfe) {
            // If the Java interface is not found then this requires to compile Native to Java.
        }

        storeJavaInterface(nativeItf, processingContext, javaInterface, clazz);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#doGenerate(org.eclipse.emf.ecore.EObject, org.ow2.frascati.assembly.factory.api.ProcessingContext)
     */
    @Override
    protected final void doGenerate(NativeInterface nativeItf,
            ProcessingContext processingContext) throws ProcessorException
    {
        // If no Java interface computed during doCheck() then compile
        if (getClass(nativeItf, processingContext) != null) {
            return;
        }

        final String filename = nativeItf.getDescriptor();
        if (this.log.isLoggable(INFO)) {
            this.log.info("Compiling the descriptor '" + filename + "'...");
        }
        File output = new File(processingContext.getOutputDirectory() + "/"
                + targetDir);

        try {
            this.compiler.compile(new File(filename), output);
        } catch (IOException exc) {
            severe(new ProcessorException("Error when compiling descriptor",
                    exc));
        }

        // Ask OW2 FraSCAti Juliac to compile generated sources.
        processingContext.addJavaSourceDirectoryToCompile(output.getAbsolutePath());
    }

    // ---------------------------------------------------------------------------
    // Public methods.
    // ---------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
     */
    public final String getProcessorID()
    {
        return getID(NativePackage.Literals.NATIVE_INTERFACE);
    }
}
