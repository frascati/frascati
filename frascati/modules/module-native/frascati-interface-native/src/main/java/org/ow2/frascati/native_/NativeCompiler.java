/**
 * OW2 FraSCAti: SCA Interface Native
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.native_;

import java.io.File;
import java.io.IOException;

/**
 * OW2 FraSCAti Assembly Factory Native compiler interface.
 * 
 * @author <a href="mailto:romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.3
 */
public interface NativeCompiler
{
    /**
     * Provides the name of the generated interface.
     *
     * @param file the native library to compile
     * @return the short name of the generated interface
     */
    String interfaceName(File library);
    
    /**
     * Provides the label of the generated package.
     *
     * @param file the native library to compile
     * @return the label of the package containing the generated sources
     */
    String packageName(File library);

    /**
     * Compile a native library into a Java interface.
     * 
     * @param library the native library to compile
     * @param output the output directory
     * @throws IOException exception raised when adding the source file to
     *             compile
     */
    void compile(File library, File output) throws IOException;
}
