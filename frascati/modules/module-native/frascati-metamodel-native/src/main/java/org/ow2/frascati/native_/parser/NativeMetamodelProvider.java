/**
 * OW2 FraSCAti: SCA Metamodel Native
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.native_.parser;

import org.ow2.frascati.native_.NativePackage;
import org.ow2.frascati.parser.metamodel.AbstractMetamodelProvider;

/**
 * OW2 FraSCAti native metamodel provider component implementation.
 *
 * @author <a href="mailto:romain.rouvoy@inria.fr">Romain Rouvoy</a>
 * @version 1.0
 */
public class NativeMetamodelProvider
     extends AbstractMetamodelProvider<NativePackage>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.MetamodelProvider#getEPackage()
   */
  public final NativePackage getEPackage()
  {
    return NativePackage.eINSTANCE;
  }

}
