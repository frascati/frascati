/**
 * OW2 FraSCAti: SCA Binding JNA
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Romain Rouvoy
 * 
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.native_.binding;

import static org.objectweb.fractal.util.Fractal.getNameController;
import static org.objectweb.fractal.util.Fractal.getSuperController;

import java.lang.annotation.ElementType;

import org.eclipse.stp.sca.BaseReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.TypeFactory;
import org.ow2.frascati.native_.JnaBinding;
import org.ow2.frascati.native_.NativePackage;

import com.sun.jna.Native;

/**
 * Bind components using <em>Java Native Access</em> (JNA) bindings. Read more
 * about <a href="https://jna.dev.java.net">JNA</a>.
 * 
 * @author <a href="romain.rouvoy@inria.fr">Romain Rouvoy</a>
 * @version 1.0
 */
public class FrascatiBindingJnaProcessor extends
        AbstractBindingProcessor<JnaBinding> {
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    @Reference(name = "type-factory")
    protected TypeFactory tf;

    @Reference(name = "component-factory")
    protected ComponentFactory cf;

    private static final String JNA_ITF = "jna-proxy";

    // --------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType,
     *      StringBuilder)
     */
    @Override
    protected final void toStringBuilder(JnaBinding jnaBinding, StringBuilder sb) {
        sb.append("binding.jna");
        append(sb, "library", jnaBinding.getLibrary());
        super.toStringBuilder(jnaBinding, sb);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doCheck(JnaBinding jnaBinding,
            ProcessingContext processingContext) throws ProcessorException {
        checkAttributeMustBeSet(jnaBinding, "library", jnaBinding.getLibrary(),
                processingContext);

        if (!hasBaseReference(jnaBinding)) {
            error(processingContext, jnaBinding,
                    "<binding.jna> can only be child of <sca:reference>");
        }

        checkBinding(jnaBinding, processingContext);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#generate(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doGenerate(JnaBinding jnaBinding,
            ProcessingContext processingContext) throws ProcessorException {
        logFine(processingContext, jnaBinding, "nothing to generate");
    }

    private Component createProxyComponent(JnaBinding binding, ProcessingContext ctx)
            throws ProcessorException {
        Class<?> cls = getBaseReferenceJavaInterface(binding, ctx);
        try {
            Object library = Native.loadLibrary(binding.getLibrary(), cls);
            ComponentType proxyType = tf
                    .createComponentType(new InterfaceType[] { tf
                            .createInterfaceType(JNA_ITF, cls.getName(), false,
                                    false, false) });
            return cf.createComponent(ctx, proxyType, "primitive", library);
        } catch (java.lang.UnsatisfiedLinkError ule) {
            throw new ProcessorException(binding, "Internal JNA Error: ", ule);
        } catch (FactoryException e) {
            throw new ProcessorException(binding, "JNA Proxy component failed: ", e);
        }
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#instantiate(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doInstantiate(JnaBinding binding, ProcessingContext ctx)
            throws ProcessorException {
        Component proxy = createProxyComponent(binding, ctx);
        Component port = getFractalComponent(binding, ctx);

        try {
            getNameController(proxy).setFcName(
                    getNameController(port).getFcName() + "-jna-proxy");

        for (Component composite : getSuperController(port)
                .getFcSuperComponents()) {
            addFractalSubComponent(composite, proxy);
        }

        BaseReference reference = getBaseReference(binding);
        bindFractalComponent(port, reference.getName(),
                proxy.getFcInterface(JNA_ITF));
        } catch (NoSuchInterfaceException e) {
            throw new ProcessorException(binding, "JNA Proxy interface not found: ", e);
        }

        logFine(ctx, binding, "importing done");
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doComplete(JnaBinding jnaBinding,
            ProcessingContext processingContext) throws ProcessorException {
        logFine(processingContext, jnaBinding, "nothing to complete");
    }

    // --------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
     */
    public final String getProcessorID() {
        return getID(NativePackage.Literals.JNA_BINDING);
    }
}
