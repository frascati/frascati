/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.test;

import java.io.FileNotFoundException;
import javax.xml.namespace.QName;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ConstrainingType;
import org.eclipse.stp.sca.JavaInterface;

import org.objectweb.fractal.util.Fractal;

import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.core.ParsingContextImpl;

/**
 * JUnit test case for the OW2 FraSCAti SCA Parser component.
 *
 * @author Philippe Merle.
 */
public class ParserTest
{
	Parser<Composite> compositeParser;
	Parser<ConstrainingType> constrainingTypeParser;

    @Before
    public final void initialize() throws Exception
    {
    	// Create a factory of SCA parsers.
    	org.ow2.frascati.parser.Parser parserFactory = new org.ow2.frascati.parser.Parser();
    	// Create a parser component.
    	org.objectweb.fractal.api.Component parser = parserFactory.newFcInstance();
        // Start the parser component.
        Fractal.getLifeCycleController(parser).startFc();
    	// Get the composite parser interface.
        compositeParser = (Parser<Composite>)parser.getFcInterface("composite-parser");
    	// Get the constraining type parser interface.
        constrainingTypeParser = (Parser<ConstrainingType>)parser.getFcInterface("constraining-type-parser");
    }
    
    @Test
    public final void parseParser() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/Parser.composite");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseCompositeParser() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/CompositeParser.composite");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseScaParser() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/ScaParser.composite");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseMetamodelProviderType() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/MetamodelProviderType");
        ConstrainingType constrainingType = constrainingTypeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseResolverPluginType() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/ResolverType");
        ConstrainingType constrainingType = constrainingTypeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseCompositeWithoutFileExtension() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser/Parser");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }
 
    @Test
    public final void parseCompositeWithFullQName() throws Exception
    {
        // Parse the parser composite.
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("org/ow2/frascati/parser", "Parser", "parser");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }
 
    @Test
    public final void parseNotFoundComposite() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("NotFoundComposite");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertNoErrorsAndWarnings(parsingContext);
            assertNotNull("The exception's qname must be not null.", parserException.getQName());
            assertTrue("The exception's cause must be " + FileNotFoundException.class,
                       parserException.getCause() instanceof FileNotFoundException);
        }
    }
 
    @Test
    public final void parseEmptyCompositeFile() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("EmptyCompositeFile");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertNoErrorsAndWarnings(parsingContext);
            assertNotNull("The exception's qname must be not null.", parserException.getQName());
            assertNotNull("The exception's cause must be not null.", parserException.getCause());
        }
    }
 
    @Test
    public final void parseSyntaxError() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("SyntaxError");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertNoErrorsAndWarnings(parsingContext);
            assertNotNull("The exception's qname must be not null.", parserException.getQName());
            assertNotNull("The exception's cause must be not null.", parserException.getCause());
        }
    }
 
    @Test
    public final void parseNotFoundInclude() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("NotFoundInclude");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 1.", 1, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseIncludeSyntaxError() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("IncludeSyntaxError");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 1.", 1, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseNotFoundJavaClass() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("NotFoundJavaClass");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 5.", 5, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseNotFoundConstrainingType() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("NotFoundConstrainingType");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 2.", 2, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseComponentReferenceTarget() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("ComponentReferenceTarget");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 3.", 3, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parsePromote() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("Promote");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 6.", 6, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseCompositeReferencePromoteSeveralComponentReferences() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("CompositeReferencePromoteSeveralComponentReferences");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
    }

    @Test
    public final void parseAutowire() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("Autowire");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);
        assertEquals("The number of wires is incorrect.", 6, composite.getWire().size());
    }

    @Test
    public final void parseCompositeReferenceAutowire() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("CompositeReferenceAutowire");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertNoErrorsAndWarnings(parsingContext);

//        assertEquals("The number of wires is incorrect.", 6, composite.getWire().size());
    }

    @Test
    public final void parseIncorrectAutowire() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("IncorrectAutowire");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 2.", 2, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 2.", 2, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseClassWithServiceAnnotation() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("ClassWithServiceAnnotation");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertEquals("Number of errors must be equals to 0.", 0, parsingContext.getErrors());
        assertEquals("Number of warnings must be equals to 2.", 2, parsingContext.getWarnings());
    }

    @Test
    public final void parseInterfaceJavaWithoutInterface() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("InterfaceJavaWithoutInterface");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 4.", 4, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseClassWithOasisScaAnnotations() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("ClassWithOasisScaAnnotations");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertEquals("Number of errors must be equals to 0.", 0, parsingContext.getErrors());
        assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
        assertEquals("Wrong number of components", 1, composite.getComponent().size());
        Component component = composite.getComponent().get(0);
        assertEquals("Wrong number of services", 2, component.getService().size());
        ComponentService componentService = component.getService().get(0);
        assertEquals("Wrong name of the 1st service", "s1", componentService.getName());
        assertEquals("Wrong interface of the 1st service", "java.lang.Runnable", ((JavaInterface)componentService.getInterface()).getInterface());
        componentService = component.getService().get(1);
        assertEquals("Wrong name of the 2nd service", "s2", componentService.getName());
        assertEquals("Wrong interface of the 2nd service", "java.lang.Runnable", ((JavaInterface)componentService.getInterface()).getInterface());
        assertEquals("Wrong number of references", 4, component.getReference().size());
        ComponentReference componentReference = component.getReference().get(0);
        assertEquals("Wrong name for the 1st reference", "r1", componentReference.getName());
        assertEquals("Wrong interface of the 1st reference", "java.lang.Runnable", ((JavaInterface)componentReference.getInterface()).getInterface());
        componentReference = component.getReference().get(1);
        assertEquals("Wrong name for the 2nd reference", "r2", componentReference.getName());
        assertEquals("Wrong interface of the 2nd reference", "java.lang.Runnable", ((JavaInterface)componentReference.getInterface()).getInterface());
        componentReference = component.getReference().get(2);
        assertEquals("Wrong name for the 3th reference", "r3", componentReference.getName());
        assertEquals("Wrong interface of the 3th reference", "java.lang.Runnable", ((JavaInterface)componentReference.getInterface()).getInterface());
        componentReference = component.getReference().get(3);
        assertEquals("Wrong name for the 4th reference", "r4", componentReference.getName());
        assertEquals("Wrong interface of the 4th reference", "java.lang.Runnable", ((JavaInterface)componentReference.getInterface()).getInterface());
    }

    @Test
    public final void parseInvalidContextualProperties() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("InvalidContextualProperties");
        try {
            Composite composite = compositeParser.parse(qname, parsingContext);
            fail(qname.toString() + " can't be parsed.");
        } catch(ParserException parserException) {
            assertEquals("Number of errors must be equals to 4.", 4, parsingContext.getErrors());
            assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
            assertEquals("The exception's qname must be equals to " + qname.toString(), qname, parserException.getQName());
            assertNull("The exception's cause must be null.", parserException.getCause());
        }
    }

    @Test
    public final void parseContextualProperties() throws Exception
    {
        ParsingContext parsingContext = new ParsingContextImpl();
        QName qname = new QName("ContextualProperties");
        parsingContext.setContextualProperty("composite-name", "MyComposite");
        Composite composite = compositeParser.parse(qname, parsingContext);
        assertEquals("MyComposite", composite.getName());
    }

    protected final void assertNoErrorsAndWarnings(ParsingContext parsingContext)
    {
        assertEquals("Number of errors must be equals to 0.", 0, parsingContext.getErrors());
        assertEquals("Number of warnings must be equals to 0.", 0, parsingContext.getWarnings());
    }
}
