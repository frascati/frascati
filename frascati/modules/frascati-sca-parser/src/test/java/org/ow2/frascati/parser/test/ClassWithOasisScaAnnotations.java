/**
 * OW2 FraSCAti SCA Parser
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.test;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Service;

/**
 * @author Philippe Merle
 */
@Service(value={Runnable.class, Runnable.class}, names={"s1", "s2"})
public class ClassWithOasisScaAnnotations
  implements Runnable
{
  public final void run()
  {
  }
  
  @Reference
  protected Runnable r1;

  @Reference(name="r2")
  protected Runnable r;

  @Reference
  protected void setR3(Runnable r)
  {
  }

  @Reference(name="r4")
  protected void setR(Runnable r)
  {
  }
}
