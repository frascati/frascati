/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Wire;

import org.ow2.frascati.parser.api.MultiplicityHelper;
import org.ow2.frascati.parser.api.ParsingContext;

public class AutowireResolver
     extends AbstractCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve autowire attributes.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    List<Component> components = composite.getComponent();

    // Map for each Java interface all component services.
	Map<String, List<ComponentService>> services = new HashMap<String, List<ComponentService>>();
	// Iterates over all components.
	for(Component component : components) {
	  // register all component services with a Java interface.
      for(ComponentService cs : component.getService()) {
        Interface csi = cs.getInterface();
        if(csi instanceof JavaInterface) {
          addKeyValue(services, ((JavaInterface)csi).getInterface(), cs);
        }
      }
	}

    // Map of all <component reference, component services> wires.
	Map<ComponentReference, List<ComponentService>> wires = new HashMap<ComponentReference, List<ComponentService>>();
	// Iterates over all wires.
	for(Wire wire : composite.getWire()) {
      addKeyValue(wires, wire.getSource2(), wire.getTarget2());
    }

    // Map for each Java interface all component references requiring to be promoted at composite level.
	Map<String, List<ComponentReference>> componentReferencesToPromoteAtCompositeLevel =
      new HashMap<String, List<ComponentReference>>();

	// Iterates over all component references.
	boolean isCompositeAutowire = composite.isAutowire();
	for(Component component : components) {
	  boolean isComponentAutowire = component.isAutowire();
      for(ComponentReference reference : component.getReference()) {
        Interface referenceInterface = reference.getInterface();
        // Test if the reference is autowire, has no target and has a Java interface.
        boolean isAutowire = reference.isAutowire() || isComponentAutowire || isCompositeAutowire;
        if(isAutowire && reference.getTarget() == null && referenceInterface instanceof JavaInterface) {
          String javaInterface = ((JavaInterface)referenceInterface).getInterface();

          // gets all the services matching the reference's Java interface.
          List<ComponentService> matchingComponentServices = services.get(javaInterface);
          int nbMatchingComponentServices = 0;

          // Compute the component services that can be wired to this reference.
          // i.e. equals to all matching services minus all already wired services.
          Set<ComponentService> targetComponentServices = new HashSet<ComponentService>();
          if(matchingComponentServices != null) {
            targetComponentServices.addAll(matchingComponentServices);
            nbMatchingComponentServices = matchingComponentServices.size();
            // gets all the services wired to this reference.
            List<ComponentService> wiredComponentServices = wires.get(reference);
            if(wiredComponentServices != null) {
              targetComponentServices.removeAll(wiredComponentServices);
            }
          }

          // Check if the reference's multiplicity is respected.

          // For multiplicity '0..n', no error and warning needs to be reported.

          if(MultiplicityHelper.is01or11(reference.getMultiplicity())
            && (nbMatchingComponentServices > 1)) {
              parsingContext.warning(
                warningMessageWhenTooManyMatchingComponentServices(reference, targetComponentServices));
              targetComponentServices = Collections.singleton(targetComponentServices.iterator().next());
          }

          if(MultiplicityHelper.is11or1N(reference.getMultiplicity())
            && (nbMatchingComponentServices == 0)) {
              parsingContext.error("<sca:reference name='" + reference.getName() +
                  "' multiplicity='" + reference.getMultiplicity().getLiteral() +
                  "'> no service matches interface '" + javaInterface + "'");
              continue; // the for loop.
          }
        	  
          // Create a wire from the reference to each target component services.
          for(ComponentService cs : targetComponentServices) {
        	Component componentTarget = ((Component)(cs.eContainer()));
            String source = component.getName() + '/' + reference.getName();
            String target = componentTarget.getName() + '/' + cs.getName();
        	if(componentTarget == component) {
              log.info("Don't autowire <sca:wire source='" + source + "' target='" + target + "'/>");
        	  continue; // the for loop.
        	}
            log.fine("Autowire <sca:wire source='" + source + "' target='" + target + "'/>");
            Wire wire = ScaFactory.eINSTANCE.createWire();
            wire.setSource(source);
            wire.setTarget(target);
            composite.getWire().add(wire);
            addKeyValue(wires, reference, cs);
          }
        }

        // when reference has no target and has a Java interface and
        // no wire as this reference as source.
        if(reference.getTarget() == null
            && referenceInterface instanceof JavaInterface
        	&& wires.get(reference) == null) {
          String javaInterface = ((JavaInterface)referenceInterface).getInterface();
          addKeyValue(componentReferencesToPromoteAtCompositeLevel, javaInterface, reference);
        }
      }
	}

    // TODO: following loop should be factorized with class of PromoterResolver

    // A map of all <component name, component>
    Map<String, Component> mapComponents = mapOfComponents(composite);

    // Set of already promoted component references.
	Set<ComponentReference> referencesAlreadyPromoted = new HashSet<ComponentReference>();
	for(Reference reference : composite.getReference()) {
	  String referencePromote = reference.getPromote();
	  if(referencePromote != null) {
        String[] paths = referencePromote.split(" ");
        for(String path : paths) {
          String[] s = path.split("/");
          if(s.length == 2) {
            Component component = mapComponents.get(s[0]);
            if(component != null) {
              ComponentReference cr = getComponentReferenceByName(component.getReference(), s[1]);
              if(cr != null) {
                referencesAlreadyPromoted.add(cr);
              }
            }
          }
        }
	  }
	}

    // Autowire composite references.
	for(Reference reference : composite.getReference()) {

	 if(MultiplicityHelper.is0Nor1N(reference.getMultiplicity()))
	 {
	     log.info("Avoid autowire for collection reference,  <sca:reference name='"+reference.getName() + "' promote='" + reference.getPromote()+"'/>");
	     break;
	 }
	    
	  // TODO: Eclipse STP SCA metamodel seems to be false
	  // a composite reference (Reference) must support 'autowire' attribute.
	  // BaseReference must not have a 'target' attribute.
	  // Must be moved to the ComponentReference class.
      //
	  // boolean isAutowire = reference.isAutowire() || isCompositeAutowire;
      boolean isAutowire = true;

      Interface referenceInterface = reference.getInterface();
      // when the composite reference is autowire and has a Java interface
      if(isAutowire && referenceInterface instanceof JavaInterface) {

        String previousReferencePromote = reference.getPromote();
        String javaInterface = ((JavaInterface)referenceInterface).getInterface();
        // promotes all component references requiring to be promoted at composite level
    	// and having a compatible Java interface.
        StringBuffer sb = new StringBuffer();
        boolean addSpace = false;
        if(previousReferencePromote != null) {
          sb.append(previousReferencePromote);
          addSpace = true;
        }
        List<ComponentReference> componentReferences = componentReferencesToPromoteAtCompositeLevel.remove(javaInterface);
        if(componentReferences != null) {
          for(ComponentReference cr : componentReferences) {
            if(!referencesAlreadyPromoted.contains(cr)) {
              if (addSpace) {
                sb.append(' ');
              }
              sb.append(((Component)(cr.eContainer())).getName()).append('/').append(cr.getName());
              reference.setPromote2(cr);
              addSpace = true;
            }
          }
        }
        String promote = sb.toString();
        if(promote.length() == 0) {
          parsingContext.warning("<sca:reference name='" + reference.getName() + "'> should promote at least one component reference");
        } else {
          if(!promote.equals(previousReferencePromote)) {
            log.fine("Autowire <sca:reference name='" + reference.getName() + "' promote='" + promote +"'/>");
            reference.setPromote(promote);
          }
        }
      }
	}

	// Check that all component references requiring to be promoted at composite level are promoted.
/* TODO
	for(String javaInterface : componentReferencesToPromoteAtCompositeLevel.keySet()) {
      for(ComponentReference cr : componentReferencesToPromoteAtCompositeLevel.get(javaInterface)) {
        parsingContext.error("reference '" + ((Component)(cr.eContainer())).getName() + '/' + cr.getName() +
            "' must have a 'target' attribute, be the source of a wire, or be promoted by a composite reference");
      }
    }
*/
	return composite;
  }

  private static <KT, VT> void addKeyValue(Map<KT, List<VT>> map, KT key, VT value)
  {
	// get the list of values associated to the key.
    List<VT> values = map.get(key);
    if(values == null) {
      // create the list of values at the first time.
      values = new ArrayList<VT>();
      // add the list to the map.
      map.put(key, values);
    }
    // add the value to the list of values associated to the key.
    values.add(value);
  }

  private String warningMessageWhenTooManyMatchingComponentServices (
    ComponentReference reference, Set<ComponentService> targetComponentServices
  ) {
    StringBuffer sb = new StringBuffer();
    sb.append("<sca:reference name='");
    sb.append(reference.getName());
    sb.append("' multiplicity='");
    sb.append(reference.getMultiplicity().getLiteral());
    sb.append("'> can be autowired to several component services:");
    for(ComponentService cs : targetComponentServices) {
      sb.append(' ');
      sb.append(((Component)(cs.eContainer())).getName());
      sb.append('/');
      sb.append(cs.getName());
    }
    sb.append(". The first matching component service is selected.");
    return sb.toString();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
