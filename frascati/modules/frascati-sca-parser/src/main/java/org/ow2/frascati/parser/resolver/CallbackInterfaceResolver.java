/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.lang.reflect.Method;
import java.util.Map;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.introspection.metadata.MetaData;

import org.osoa.sca.annotations.Callback;

import org.ow2.frascati.parser.api.ParsingContext;

public class CallbackInterfaceResolver
     extends AbstractCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    for (Component component : composite.getComponent()) {
      // Get component implementation.
      Implementation impl = component.getImplementation();
      // Check if implementation is Java
      if (impl instanceof JavaImplementation) {
        JavaImplementation javaImpl = (JavaImplementation) impl;
        try {
          // Retrieve Metadata for this component implementation.
          Class<Object> cl = parsingContext.loadClass(javaImpl.getClass_());
          // Resolve CallBack references/services
          MetaData<?> ccmd = new MetaData<Object>(cl);
          resolveItf(component, ccmd, parsingContext);
        } catch (ClassNotFoundException e) {
          // Already reported by the JavaResolver
        }
      }
    }
    // resulting composite model
    return composite;
  }

  private void resolveItf(Component component, MetaData<?> ccmd, ParsingContext parsingContext)
  {
    for (ComponentService service : component.getService()) {
      if (service.getCallback() != null) {
        Map<String, Method> setters = ccmd.getAllUnAnnotatedSetterMethods();
        Method m = setters.get(service.getName());
        if (m != null) {
          log.fine("add @" + Callback.class + " on method " + m.getName());
          ccmd.addMetaData(m, Callback.class);
        }
        resolveJavaItf(service.getInterface(), parsingContext);
      }
    }

    for (ComponentReference reference : component.getReference()) {
      if (reference.getCallback() != null) {
        Map<String, Method> setters = ccmd.getAllUnAnnotatedSetterMethods();
        Method m = setters.get(reference.getName());
        if (m != null) {
          log.fine("add @" + Callback.class + " on method " + m.getName());
          ccmd.addMetaData(m, Callback.class);
        }
        resolveJavaItf(reference.getInterface(), parsingContext);
      }
    }
  }

  private void resolveJavaItf(Interface itf, ParsingContext parsingContext)
  {
    if (itf instanceof JavaInterface) {
      JavaInterface javaInterface = (JavaInterface) itf;
      if (javaInterface.getCallbackInterface() != null) {
        try {
          // Retrieve Metadata for this interface.
          Class<Object> cl = parsingContext.loadClass(javaInterface.getInterface());
          MetaData<?> metadata = new MetaData<Object>(cl);
          Callback callback = metadata.getAnnotation(Callback.class);
          if (callback == null) {
            //callback = metadata.addMetaData(c, md);
          }
        } catch (ClassNotFoundException e) {
          // Already reported by the JavaResolver
          return;
        }
      }
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
