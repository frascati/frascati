package org.ow2.frascati.parser.resolver;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.introspection.metadata.MetaData;
import org.oasisopen.sca.annotation.Authentication;
import org.oasisopen.sca.annotation.Property;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * This resolver is in charge of finding @Authentication annotations
 * and extend the model by adding intent to the requires field of 
 * component or services.
 * Tests are located into the helloworld-auth example.
 * 
 * @author Remi Melisson
 *
 */
public class AuthenticationResolver extends AbstractCompositeResolver {

	/**
	 * The name of the composite file which describe the 
	 * authentication intent.
	 */
	@Property(name="authentication-intent-name")
	String authenticationCompositeName;
	
	@Override
	@SuppressWarnings("unchecked")
	protected Composite doResolve(Composite composite,
			ParsingContext parsingContext) {

		try {
			
			for (Component component : composite.getComponent()) {

				// we look for the @Authentication 
				// annotation in the implementations
				Implementation impl = component.getImplementation();

				if (impl instanceof JavaImplementation) {
					String classS = ((JavaImplementation) impl).getClass_();

					Class<?> clazz = parsingContext.loadClass(classS);
					MetaData<?> md = new MetaData(clazz);
					
					if (clazz == null)
						return composite;
					
					// if the class itself is annotated, we add requires to the
					// component
					if  (provideAuthenticationAnnotation(md))  {

						List<QName> requires = component.getRequires();

						component.setRequires(
								addAuthenticationIntent(requires) );
						
					}
					// else we add it for the required services
					else {

						for (ComponentService service : 
								component.getService()) {

							if (service.getInterface() 
									instanceof JavaInterface) {
								JavaInterface javaInterface =
									(JavaInterface) service.getInterface();

								clazz = parsingContext.loadClass(javaInterface
										.getInterface());
								md = new MetaData(clazz);

								if  (provideAuthenticationAnnotation(md))  {

									List<QName> requires = service
											.getRequires();
									
									service.setRequires(
											addAuthenticationIntent(requires) );
								}
							}
						}
					}
				}
			}
	    } catch (ClassNotFoundException e) {
	        // Already reported by the JavaResolver
	        return composite;
	    }

		// after that, the model includes authentication intent references
		return composite;
	}

	
	/**
	 * Add Authentication intent to a requires list
	 * @param requires list of required intents
	 * @return the incremented list
	 */
	private List<QName> addAuthenticationIntent(List<QName> requires){
		
		if (requires == null)
			requires = new ArrayList<QName>();

		requires.add( new QName(
				this.authenticationCompositeName));
		
		return requires;
	}
	
	
	/**
	 * Check if metadata contains @Authentication
	 * @param md
	 * @return
	 */
	private boolean provideAuthenticationAnnotation(MetaData<?> md){
		return ((md.getAnnotation(Authentication.class) != null) 
				|| 
				(md.getAnnotation(org.osoa.sca.annotations
						.Authentication.class) != null));
	}
}
