/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.api;

import org.eclipse.stp.sca.Multiplicity;

/**
 * Helper functions related to SCA Multiplicity.
 *
 * @author Philippe Merle - Inria
 * @version 1.5
 */
public class MultiplicityHelper {

  public static boolean is01or11(Multiplicity multiplicity)
  {
    return Multiplicity._01.equals(multiplicity) || Multiplicity._11.equals(multiplicity);
  }

  public static boolean is0Nor1N(Multiplicity multiplicity)
  {
    return Multiplicity._0N.equals(multiplicity) || Multiplicity._1N.equals(multiplicity);
  }

  public static boolean is11or1N(Multiplicity multiplicity)
  {
    return Multiplicity._11.equals(multiplicity) || Multiplicity._1N.equals(multiplicity);
  }
}
