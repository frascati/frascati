/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2009-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.parser.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.XMLResource;

import org.eclipse.stp.sca.Composite;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * OW2 FraSCAti Assembly Factory SCA parser component implementation.
 * 
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.1
 */
public class ScaCompositeParser
     extends AbstractCompositeParser
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Java property name where SCA composites are stored.
   */
  public static final String OUTPUT_DIRECTORY_PROPERTY_NAME = "org.ow2.frascati.parser.output.directory";

  /**
   * Default values where SCA composites are stored.
   */
  public static final String OUTPUT_DIRECTORY_PROPERTY_DEFAULT_VALUE = "sca-composites";

  /**
   * debug option
   */
  @Property(name = "debug")
  private boolean debug = false;

  /**
   * The list of resolvers.
   */
  @Reference(name = "resolvers")
  private List<Resolver<Composite>> resolvers;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Print Error when validating model
   * 
   * @throws IOException
   */
  protected final void printDiagnostic(Diagnostic diagnostic, String indent,
      StringBuffer stringBuffer)
  {
    stringBuffer.append(indent);
    stringBuffer.append(diagnostic.getMessage());
    stringBuffer.append("\n");
    for (Diagnostic child : diagnostic.getChildren()) {
      printDiagnostic(child, indent + "  ", stringBuffer);
    }
  }

  /**
   * Write resulting composite file for debug
   * 
   * @throws FileNotFoundException
   */
  protected final void writeComposite(Composite composite)
    throws IOException
  {
    String outputDirectoryPropertyValue = System.getProperty(OUTPUT_DIRECTORY_PROPERTY_NAME, OUTPUT_DIRECTORY_PROPERTY_DEFAULT_VALUE);
    File compositeOuputDirectory = new File(outputDirectoryPropertyValue + '/').getAbsoluteFile();
    compositeOuputDirectory.mkdirs();
    File compositeOuput = new File(compositeOuputDirectory, composite.getName()
        + ".composite");
    FileOutputStream fsout = new FileOutputStream(compositeOuput);

    // Remove <include> elements as there were already included by component IncludeResolver.
    composite.getInclude().clear();

    Map<Object, Object> options = new HashMap<Object, Object>();
    options.put(XMLResource.OPTION_ROOT_OBJECTS,
        Collections.singletonList(composite));
    ((XMLResource) composite.eResource()).save(fsout, options);
    log.warning("Write debug composite " + compositeOuput.getCanonicalPath());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Parser#parse(QName, ParsingContext)
   */
  public final Composite parse(QName qname, ParsingContext parsingContext)
    throws ParserException
  {
	Composite composite = super.parse(qname, parsingContext);

	// resolving on SCA model
	log.fine("Executing resolvers on " + qname);
    for(Resolver<Composite> resolver : resolvers) {
    	composite = resolver.resolve(composite, parsingContext);
    }

	int nbErrors = parsingContext.getErrors();
    if(nbErrors == 0) {
      // Try to validate
      try {
        Diagnostic diagnostic = Diagnostician.INSTANCE.validate(composite);
        if (diagnostic.getSeverity() != Diagnostic.OK) {
          StringBuffer stringBuffer = new StringBuffer();
          printDiagnostic(diagnostic, "", stringBuffer);
          parsingContext.warning(stringBuffer.toString());
        }
        // Write resulting composite file if debug is true
        if (debug || System.getProperty(OUTPUT_DIRECTORY_PROPERTY_NAME) != null) {
          try {
            writeComposite(composite);
          } catch (IOException ioe) {
            severe(new ParserException(qname, "Error when writting debug composite file", ioe));
          }
        }
      } catch (Exception e) {
        // Report the exception.
    	log.warning(qname + " can not be validated: " + e.getMessage());
        parsingContext.error("SCA composite '" + qname + "' can not be validated: " + e.getMessage());
      }
    } else {
      warning(new ParserException(qname, nbErrors + " error" + ((nbErrors==1)?"":"s") + " found in " + qname.toString()));
    }

    return composite;
  }

}
