/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.Service;

import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Check that Java classes and interfaces exist.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class JavaResolver
     extends AbstractCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Check Java classes and interfaces.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    for(Service s : composite.getService()) {
      checkInterface(s.getInterface(), parsingContext);
    }
    for(Reference r : composite.getReference()) {
      checkInterface(r.getInterface(), parsingContext);    	
    }

    for(Component component : composite.getComponent()) {
      Implementation impl = component.getImplementation();
      if (impl instanceof JavaImplementation) {
        JavaImplementation javaImpl = (JavaImplementation) impl;
        try {
          parsingContext.loadClass(javaImpl.getClass_());
        } catch (ClassNotFoundException e) {
          parsingContext.error("<sca:implementation.java class='" +
              javaImpl.getClass_() + "'/> class '" + javaImpl.getClass_() + "' not found");
        }
      }
      for(ComponentService s : component.getService()) {
        checkInterface(s.getInterface(), parsingContext);
      }
      for(ComponentReference r : component.getReference()) {
        checkInterface(r.getInterface(), parsingContext);    	
      }
    }
    return composite;
  }

  /**
   * Check Java interfaces.
   */
  private void checkInterface(Interface itf, ParsingContext parsingContext)
  {
    if (itf instanceof JavaInterface) {
      String interfaceJavaInterface = ((JavaInterface) itf).getInterface();
      if(interfaceJavaInterface == null) {
        parsingContext.error("<sca:interface.java/> attribute 'interface' must be set");
      } else {
        try {
          parsingContext.loadClass(interfaceJavaInterface);
        } catch (ClassNotFoundException e) {
          parsingContext.error("<sca:interface.java interface='" + interfaceJavaInterface +
          "'/> interface '" + interfaceJavaInterface + "' not found");
        }
      }
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
