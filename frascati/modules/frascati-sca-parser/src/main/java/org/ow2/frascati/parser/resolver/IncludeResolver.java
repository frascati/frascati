/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Include;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.core.AbstractCompositeParser;

/**
 * Add to current Composite the components, services, references, properties and
 * wires defined into included composites.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class IncludeResolver
     extends AbstractCompositeParser
  implements Resolver<Composite>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  @Reference(name = "next-resolver")
  private Resolver<Composite> nextResolver;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve includes.
   */
  private final void resolveIncludes(Composite composite, ParsingContext parsingContext)
  {
    for (Include include : composite.getInclude()) {
      QName includeName = NormalizeQName.normalize(include.getName(), include, parsingContext);
      Composite includedComposite;
      try {        
        includedComposite = parse(includeName, parsingContext);
      } catch (ParserException e) {
    	if(e.getCause() instanceof IOException) {
          parsingContext.error("<sca:include name='" + includeName +
              "'/> composite '" + includeName + "' not found");
    	} else {
          parsingContext.error("<sca:include name='" + includeName +
                  "'/> invalid content");
    	}
        continue; // the for loop.
      }
      resolveIncludes(includedComposite, parsingContext);

      // Merge included policySets.
      List<QName> includedCompositePolicySets = includedComposite.getPolicySets();
      if(includedCompositePolicySets != null) {
        List<QName> compositePolicySets = composite.getPolicySets();
        if(compositePolicySets == null) {
          compositePolicySets = new ArrayList<QName>();
          composite.setPolicySets(compositePolicySets);
        }
        compositePolicySets.addAll(includedCompositePolicySets);
      }

      // Merge included requires.
      List<QName> includedCompositeRequires = includedComposite.getRequires();
      if(includedCompositeRequires != null) {
        List<QName> compositeRequires = composite.getRequires();
        if(compositeRequires == null) {
          compositeRequires = new ArrayList<QName>();
          composite.setRequires(compositeRequires);
        }
        compositeRequires.addAll(includedCompositeRequires);
      }

      // Merge children elements.
      composite.getComponent().addAll(includedComposite.getComponent());
      composite.getProperty().addAll(includedComposite.getProperty());
      composite.getService().addAll(includedComposite.getService());
      composite.getReference().addAll(includedComposite.getReference());
      composite.getWire().addAll(includedComposite.getWire());
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  public final Composite resolve(Composite composite, ParsingContext parsingContext)
  {
    resolveIncludes(composite, parsingContext);

    // Delegate to the next resolver.
    return nextResolver.resolve(composite, parsingContext);
  }

}
