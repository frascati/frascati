/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Resolve contextual properties.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class ContextualPropertiesResolver
     extends AbstractRecursiveResolver
{
  @Override
  protected void doResolve(EObject eobject, ParsingContext parsingContext)
  {
    // Iterate over all attributes of the eobject instance. 
    for(EAttribute eAttribute : eobject.eClass().getEAllAttributes()) {
      if(eobject.eIsSet(eAttribute)) {
        Object tmp = eobject.eGet(eAttribute);
        if(tmp != null && tmp instanceof String) {
          String value = (String)tmp;
          int beginProperty = value.indexOf("${");
          if(beginProperty != -1) {
            int endProperty = value.indexOf('}', beginProperty + 2);
            if(endProperty == -1) {
              parsingContext.error(value + " must contain '}'");
            } else {
              String propertyName = value.substring(beginProperty + 2, endProperty);
              Object propertyValue = parsingContext.getContextualProperty(propertyName);
              if(propertyValue == null) {
                parsingContext.error("Contextual property '" + propertyName + "' not found");
              } else {
            	if(!(propertyValue instanceof String)) {
                  parsingContext.error("Contextual property '" + propertyName + "' not a string contextual property");            		
            	} else {
                  value = value.substring(0, beginProperty) + propertyValue + value.substring(endProperty + 1);
                  eobject.eSet(eAttribute, value);
//                  log.info("UPDATE " + eAttribute.getName() + " with " + value);
            	}
              }
            }
          }
        }
      }
    }
  }
}
