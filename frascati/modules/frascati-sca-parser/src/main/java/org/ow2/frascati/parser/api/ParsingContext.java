/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.api;

import org.eclipse.emf.ecore.EObject;

import org.ow2.frascati.util.context.ClassLoaderContext;

/**
 * OW2 FraSCAti SCA Parser parsing context interface.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public interface ParsingContext extends ClassLoaderContext
{
    /**
     * Put a data into the parsing context.
     *
     * @param key the key of the data to put.
     * @param type the type of the data to put.
     * @param data the data to put.
     */
    <T> void putData(Object key, Class<T> type, T data);

    /**
     * Get a data from the parsing context.
     *
     * @param key the key of the data to get.
     * @param type the type of the data to get.
     * @return the data, or null if not found.
     */
    <T> T getData(Object key, Class<T> type);

    /**
     * Signal a warning.
     *
     * @param message the warning message.
     */
    void warning(String message);

    /**
     * Get the number of warnings.
     *
     * @return the number of warnings.
     */
    int getWarnings();

    /**
     * Signal an error.
     *
     * @param message the error message.
     */
    void error(String message);

    /**
     * Get the number of errors.
     *
     * @return the number of errors.
     */
    int getErrors();

    /**
     * Get the document location URI where an given {@link EObject} instance is declared.
     *
     * @return the document location URI where the given {@link EObject} instance is declared.
     */
    String getLocationURI(EObject eObject);
}
