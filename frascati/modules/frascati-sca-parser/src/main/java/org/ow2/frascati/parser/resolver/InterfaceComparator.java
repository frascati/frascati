/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.Comparator;

import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.WSDLPortType;

/**
 * Utility class to compare Interfaces.
 * 
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class InterfaceComparator
  implements Comparator<Interface>, java.io.Serializable
{
  private static final long serialVersionUID = 1L;

  /**
   * @see Comparator#compare(java.lang.Object, java.lang.Object)
   */
  public final int compare(Interface i1, Interface i2)
  {
    if (i1.eClass().getClassifierID() == i1.eClass().getClassifierID()) {

      switch (i1.eClass().getClassifierID()) {
      case ScaPackage.JAVA_INTERFACE:
        JavaInterface itf1 = (JavaInterface) i1;
        JavaInterface itf2 = (JavaInterface) i2;
        if (itf1.getInterface().equals(itf2.getInterface())) {
          return 0;
        }
        break;

      case ScaPackage.WSDL_PORT_TYPE:
        WSDLPortType w1 = (WSDLPortType) i1;
        WSDLPortType w2 = (WSDLPortType) i2;
        if (w1.getInterface().equals(w2.getInterface())) {
          return 0;
        }
        break;
      }
    }
    return -1;
  }

}
