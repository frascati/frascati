/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.introspection.metadata.MetaData;

import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Process Java classes to add missing services and references to the model.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class JavaInterfaceResolver
     extends AbstractCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve Java component implementations.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @SuppressWarnings("unchecked")
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    for(Component c : composite.getComponent()) {
      Implementation impl = c.getImplementation();
      if (impl instanceof JavaImplementation) {
        JavaImplementation javaImpl = (JavaImplementation) impl;
        try {
          Class<?> cl = parsingContext.loadClass(javaImpl.getClass_());
          if(cl != null) {
            MetaData<?> ccmd = new MetaData(cl);
            resolveServices(c, ccmd, parsingContext);
            resolveReferences(c, ccmd);
          }
        } catch (ClassNotFoundException e) {
          // Already reported by the JavaResolver
        }
      }
    }
    return composite;
  }

  /**
   * Resolve service interfaces defined into Java implementations.
   * 
   * @param component
   *          - current component
   * @param cMetaData
   *          - ContentClassMetaData API instance
   */
  private void resolveServices(Component component, MetaData<?> cMetaData, ParsingContext parsingContext)
  {
	// The list of @Service names implemented by the content class.
    List<String> annotatedServiceNames = new ArrayList<String>();
	// The map of @Service interfaces implemented by the content class.
    Map<String, String> annotatedServiceInterfaces = new HashMap<String, String>();

    {
      // Retrieve the OSOA service annotations.
      for(org.osoa.sca.annotations.Service s :
    	  getAllAnnotations(cMetaData.getSupportClass(), org.osoa.sca.annotations.Service.class)) {
        if(s.interfaces().length > 0) {
          if(s.value() != Void.class) {
            parsingContext.warning("@" + org.osoa.sca.annotations.Service.class.getCanonicalName() +
                " applied to class " + cMetaData.getSupportClass().getCanonicalName() +
                " must define the field 'value' or 'interfaces'");
          } else {
            // Add all interfaces of the @Service annotation.
            for(Class<?> itf : s.interfaces()) {
              String serviceName = itf.getSimpleName();
              if(!annotatedServiceNames.contains(serviceName)) {
                annotatedServiceNames.add(serviceName);
                annotatedServiceInterfaces.put(serviceName, itf.getCanonicalName());
              }
            }
          }
        } else {
          if(s.value() == Void.class) {
            parsingContext.warning("@" + org.osoa.sca.annotations.Service.class.getCanonicalName() +
                " applied to class " + cMetaData.getSupportClass().getCanonicalName() +
                " must define the field 'value' or 'interfaces'");
          } else {
            // Add the interface of the @Service annotation.
            String serviceName = s.value().getSimpleName();
            if(!annotatedServiceNames.contains(serviceName)) {
              annotatedServiceNames.add(serviceName);
              annotatedServiceInterfaces.put(serviceName, s.value().getCanonicalName());
            }
          }
        }
      }
    }

    {
      // Retrieve the OASIS service annotations.
      for(org.oasisopen.sca.annotation.Service s :
          getAllAnnotations(cMetaData.getSupportClass(), org.oasisopen.sca.annotation.Service.class)) {
        // Add all interfaces of the @Service annotation.
        int i = 0;
        for(Class<?> itf : s.value()) {
          // Compute the service name.
          String serviceName = null;
          if((s.names().length > i) && (s.names()[i] != null)) {
            serviceName = s.names()[i];
          } else {
            serviceName = itf.getSimpleName();
          }
          // Add the interface of the @Service annotation.
          if(!annotatedServiceNames.contains(serviceName)) {
            annotatedServiceNames.add(serviceName);
            annotatedServiceInterfaces.put(serviceName, itf.getCanonicalName());
          }
          i++;
        }
      }
    }

    try {
      JavaImplementation javaImpl = (JavaImplementation) component.getImplementation();
      Class<?> implementation = parsingContext.loadClass(javaImpl.getClass_());

      // Add all implemented interfaces annotated with @Service.
      for(Class<?> itf : implementation.getInterfaces()) {
    	// If OSOA annotation
        if(itf.getAnnotation(org.osoa.sca.annotations.Service.class) != null) {
          String serviceName = itf.getSimpleName();
          if(!annotatedServiceNames.contains(serviceName)) {
            annotatedServiceNames.add(serviceName);
            annotatedServiceInterfaces.put(serviceName, itf.getCanonicalName());
          }
        }
    	// If OASIS annotation
        if(itf.getAnnotation(org.oasisopen.sca.annotation.Service.class) != null) {
          String serviceName = itf.getSimpleName();
          if(!annotatedServiceNames.contains(serviceName)) {
            annotatedServiceNames.add(serviceName);
            annotatedServiceInterfaces.put(serviceName, itf.getCanonicalName());
          }
        }
      }

    } catch (ClassNotFoundException e) {
      // Already reported by the JavaResolver
      return;
    }

    // Register services already defined in the model for this component.
    Map<String, String> serviceMap = new HashMap<String, String>();
    for (ComponentService componentService : component.getService()) {
      Interface itf = componentService.getInterface();
      // if service interface is defined
      if (itf != null) {
        // if service interface is a Java interface
        if (itf instanceof JavaInterface) {
          // register it into the map.
          serviceMap.put(componentService.getName(),
              ((JavaInterface) itf).getInterface());
        }
      }
      // Service is defined without interfaces, need to add if possible
      else {
        String interfaceClassName = annotatedServiceInterfaces.get(componentService.getName());
        if(interfaceClassName == null && annotatedServiceNames.size() > 0) {
          interfaceClassName = annotatedServiceInterfaces.get(annotatedServiceNames.get(0));
        }
        if(interfaceClassName != null) {
          JavaInterface ji = ScaFactory.eINSTANCE.createJavaInterface();
          ji.setInterface(interfaceClassName);
          componentService.setInterface(ji);
          // register since just added
          serviceMap.put(componentService.getName(), interfaceClassName);
        }
      }
    }

    // Register missing services annotated into the content class but not declared in the composite file.
    for (String serviceName : annotatedServiceNames) {
      String serviceInterface = annotatedServiceInterfaces.get(serviceName);
      // If interface type as already been registered or the service name already declared
      if (!(serviceMap.containsValue(serviceInterface) || serviceMap.containsKey(serviceName))) {
        // create a new component service to add
        ComponentService cs = ScaFactory.eINSTANCE.createComponentService();
        cs.setName(serviceName);
        JavaInterface ji = ScaFactory.eINSTANCE.createJavaInterface();
        ji.setInterface(serviceInterface);
        cs.setInterface(ji);
        // add component service to the model
        component.getService().add(cs);
      }
    }
  }

  /**
   * Resolve reference interfaces defined in Java.
   *
   * @param component
   *          - current component
   * @param cMetaData
   *          - ContentClassMetaData API instance
   */
  private void resolveReferences(Component component, MetaData<?> cMetaData)
  {
    // Map of references declared into the content class.
    Map<String, String> referenceDecMap = new HashMap<String, String>();

    {
      // Retrieve OSOA Reference annotations.
      AccessibleObject aos[] = cMetaData
        .getAllAnnotatedMethodsAndFields(org.osoa.sca.annotations.Reference.class);

      // If fields or methods are annotated with org.osoa.sca.annotations.reference
      if (aos != null) {
        // register annotated reference fields and methods into the map.
        for (AccessibleObject ao : aos) {
          org.osoa.sca.annotations.Reference r = ao.getAnnotation(org.osoa.sca.annotations.Reference.class);
          String referenceName = r.name();
          if(referenceName.equals("")) {
            referenceName = getReferenceName(ao);
          }
          referenceDecMap.put(referenceName, getReferenceClassName(ao));
        }
      }
    }

    {
      // Retrieve OASIS Reference annotations.
      AccessibleObject aos[] = cMetaData
          .getAllAnnotatedMethodsAndFields(org.oasisopen.sca.annotation.Reference.class);

      // If fields or methods are annotated with org.osoa.sca.annotations.reference
      if (aos != null) {
        // register annotated reference fields and methods into the map.
        for (AccessibleObject ao : aos) {
          org.oasisopen.sca.annotation.Reference r = ao.getAnnotation(org.oasisopen.sca.annotation.Reference.class);
          String referenceName = r.name();
          if(referenceName.equals("")) {
            referenceName = getReferenceName(ao);
          }
          referenceDecMap.put(referenceName, getReferenceClassName(ao));
        }
      }
    }
 
    // register references already defined in the model for this component.
    Map<String, String> referenceMap = new HashMap<String, String>();
    for (ComponentReference componentReference : component.getReference()) {
      Interface itf = componentReference.getInterface();
      // if reference has a defined interface
      if (itf != null) {
        if (itf instanceof JavaInterface) {
          // register it into the map if this is a Java interface.
          referenceMap.put(componentReference.getName(),
              ((JavaInterface) itf).getInterface());
        } else {
          // If the interface is defined but not a Java one, then record it also.
          referenceMap.put(componentReference.getName(), "FOO");
          // TODO: variable referenceMap does not require to be a Map,
          // as values associated to keys are never used.
        }
      }
      // reference is defined without an interface, need to add if possible
      else {
        // get interface from the reference defined in the content class.
        String referenceName = componentReference.getName();
        String interfaceClass = referenceDecMap.get(referenceName);
        if (interfaceClass != null) {
          JavaInterface newJavaItf = ScaFactory.eINSTANCE.createJavaInterface();
          newJavaItf.setInterface(interfaceClass);
          componentReference.setInterface(newJavaItf);
          // put to list of registered references
          referenceMap.put(referenceName, interfaceClass);
        }
      }

      // resolveCallback(componentReference, cMetaData);
    }

    // Register missing services annotated into the content class but not declared in the composite file.
    for (Entry<String, String> entry : referenceDecMap.entrySet()) {
      // if the reference (by name) defined in Java class has not been defined in Model
      if (!referenceMap.containsKey(entry.getKey())) {
        // This reference must be added to current model
        // create a new component reference to add
        ComponentReference cr = ScaFactory.eINSTANCE.createComponentReference();
        JavaInterface ji = ScaFactory.eINSTANCE.createJavaInterface();
        ji.setInterface(entry.getValue());
        cr.setName(entry.getKey());
        cr.setInterface(ji);
        // add component service to the model
        component.getReference().add(cr);
      }
    }
  }

  /**
   * Get Class of a reference from annotated field / method
   * 
   * @param object
   *          - Reference annotated Field or Method
   * @return reference Class name
   */
  private String getReferenceClassName(AccessibleObject object)
  {
    if (object instanceof Field) {
      Field field = (Field) object;
      return field.getType().getCanonicalName();
    }

    if (object instanceof Method) {
      Method method = (Method) object;
      Class<?>[] classe = method.getParameterTypes();
      if (classe.length > 0) {
        return classe[0].getCanonicalName();
      }
    }

    return null;
  }

  /**
   * Get Name of a reference from annotated field / method
   * 
   * @param object
   *          - Reference annotated Field or Method
   * @return reference name
   */
  private String getReferenceName(AccessibleObject object)
  {
    if (object instanceof Field) {
      return ((Field) object).getName();
    } else if (object instanceof Method) {
      String name = ((Method) object).getName().substring("set".length());
      return Character.toLowerCase(name.charAt(0)) + name.substring(1);
    }
    return null;
  }

  /**
   * Get all the annotations of a given annotation type for a given class.
   * Recursively traverse super classes.
   *
   * @param clazz the given class.
   * @param annotationType the given annotation type.
   * @return all the annotations of the given annotation type for the given class.
   */
  private static <ANNOTATION_TYPE extends Annotation>
  List<ANNOTATION_TYPE> getAllAnnotations(
      Class<?> clazz,
      Class<ANNOTATION_TYPE> annotationType)
  {
    List<ANNOTATION_TYPE> annotations = new ArrayList<ANNOTATION_TYPE>();
	while(clazz != Object.class) {
      ANNOTATION_TYPE annotation = clazz.getAnnotation(annotationType);
      if(annotation != null) {
        annotations.add(annotation);
      }
      clazz = clazz.getSuperclass();
	}
	return annotations;
  }
  
  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
