/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.core;

import java.net.URL;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.ConstrainingType;

import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * OW2 FraSCAti SCA constraining type parser implementation class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ScaConstrainingTypeParser
     extends AbstractDelegateScaParser<ConstrainingType>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Parser#parse(QName, ParsingContext)
   */
  public final ConstrainingType parse(QName qname, ParsingContext parsingContext)
    throws ParserException
  {
// TODO following code must be factorized with similar code in AbstractCompositeParser
// perhaps could be put into AbstractDelegateScaParser#parseDocument.

	String constrainingTypeUri = asStringURI(qname);

    // Search the URL in the class loader matching the constraining type uri.
    URL url = parsingContext.getResource(constrainingTypeUri);
    log.fine("URL for " + constrainingTypeUri + " is: " + url);

    //
    // Compute the constraining type URI
    //
    QName constrainingTypeQN = null;
    if (url != null) {
      constrainingTypeQN = new QName(url.toString());
	} else {
      // Composite file not loaded as resource of the current class loader
      // Try to load from the local file system or an URL.
      constrainingTypeQN = new QName(constrainingTypeUri);
    }

    return parseDocument(constrainingTypeQN, parsingContext).getConstrainingType();
  }

}
