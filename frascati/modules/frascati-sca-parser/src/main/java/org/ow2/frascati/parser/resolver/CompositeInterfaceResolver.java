/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.Service;

import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Resolve interfaces for composite references/services and component references.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class CompositeInterfaceResolver
     extends AbstractCopierResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    // resolve component references
    for (Component component : composite.getComponent()) {
      for (ComponentReference reference : component.getReference()) {
        Interface itf = reference.getInterface();
        // check if the reference targets a component service but has no interface
        if ((itf == null) && (reference.getTarget() != null)) {
          BaseService target = reference.getTarget2();
          if(target != null) {
            // Make copy of interface element
            reference.setInterface(this.copy(target.getInterface()));
          }
        }
      }
    }

    // resolve composite references
    for (Reference reference : composite.getReference()) {
      Interface itf = reference.getInterface();
      // check if reference promotes a component but does not have interface
      if ((itf == null) && (reference.getPromote() != null)) {
        ComponentReference cmpReference = reference.getPromote2();
        if((cmpReference != null) && (cmpReference.getInterface() != null)) {
          // Make copy of interface element
          reference.setInterface(this.copy(cmpReference.getInterface()));
        }
      }
    }

    // resolve composite services
    for (Service service : composite.getService()) {
      Interface itf = service.getInterface();
      // check if service promotes a component but does not have interface
      if ((itf == null) && (service.getPromote() != null)) {
    	ComponentService cmpService = service.getPromote2();
    	if((cmpService != null) && (cmpService.getInterface() != null)) {
          // Make copy of interface element
          service.setInterface(this.copy(cmpService.getInterface()));
    	}
      }
    }

    return composite;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
