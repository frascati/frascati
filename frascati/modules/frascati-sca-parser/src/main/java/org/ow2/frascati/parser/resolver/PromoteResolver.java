/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.Map;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.Service;

import org.ow2.frascati.parser.api.ParsingContext;

public class PromoteResolver
     extends AbstractInterfaceComparatorCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve promote attributes.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    // A map of all <component name, component>
    Map<String, Component> components = mapOfComponents(composite);

    // Complete composite services requiring to resolve the promote attribute.
    for (Service service : composite.getService()) {
      String servicePromote = service.getPromote();
      if (isIncompletePath(servicePromote)) {
        // Search the component to promote.
        Component promotedComponent = components.get(servicePromote);
        if(promotedComponent == null) {
          parsingContext.error(messageError(service, "undefined"));
          continue; // the for loop.
        }
        ComponentService promotedComponentService;
        // if the composite service does not have interface then
        // promote the first service of the promoted component.
        if (service.getInterface() == null) {
          try {
            promotedComponentService = promotedComponent.getService().get(0);
          } catch(IndexOutOfBoundsException e) {
            parsingContext.error(messageError(service, "has no service to promote"));
            continue; // the for loop.
          }
        } else {
          // else promote the first service which provides a compatible interface type.

          // Search the component service conform to the service.
          promotedComponentService = this.getComponentServiceConformToInterface(promotedComponent, service.getInterface());
          if(promotedComponentService == null) {
            parsingContext.error(messageError(service, "doesn't provide a service with a compatible interface"));
            continue; // the for loop.
          }
        }
        // complete the composite service promote attribute.
        service.setPromote(servicePromote + "/" + promotedComponentService.getName());
        service.setPromote2(promotedComponentService);
      }
    }

    // Complete composite references requiring to resolve the promote attribute.
    for (Reference reference : composite.getReference()) {
      String referencePromote = reference.getPromote();
      // is an incomplete promote reference?
      if (isIncompletePath(referencePromote)) {
        // Search the component to promote.
        Component promotedComponent = components.get(referencePromote);
        if(promotedComponent == null) {
          parsingContext.error(messageError(reference, "undefined"));
          continue; // the for loop.
        }
        ComponentReference promotedComponentReference;
        // if the composite reference has no interface then
        // promote the first reference of the promoted component.
        if (reference.getInterface() == null) {
          try {
            promotedComponentReference = promotedComponent.getReference().get(0);
          } catch(IndexOutOfBoundsException e) {
            parsingContext.error(messageError(reference, "has no reference to promote"));
            continue; // the for loop.
          }
        } else {
          // else promote the first component reference which provides a compatible interface.

          // Search the component reference conform to the reference.
          promotedComponentReference = this.getComponentReferenceConformToInterface(promotedComponent, reference.getInterface());
          if(promotedComponentReference == null) {
            parsingContext.error(messageError(reference, "doesn't provide a reference with a compatible interface"));
            continue; // the for loop.
          }
        }
        // complete the composite reference promote attribute.
        reference.setPromote(referencePromote + "/" + promotedComponentReference.getName());
        reference.setPromote2(promotedComponentReference);
      }

      if(referencePromote != null && reference.getPromote2() == null) {
        // attribute 'promote' is set but computed promoted2 is not set
        // then perhaps 'promote' contains several component/reference values.
        String[] promotedReferences = referencePromote.split(" ");
        if(promotedReferences.length > 1) {
          for(String promotedReference : promotedReferences) {
            String[] paths = promotedReference.split("/");
            if(paths.length == 2) {
              Component promotedComponent = components.get(paths[0]);
              if(promotedComponent != null) {
                ComponentReference promotedComponentReference =
            	  getComponentReferenceByName(promotedComponent.getReference(), paths[1]);
                if(promotedComponentReference != null) {
                  reference.setPromote2(promotedComponentReference);
                  // Following line is required because setPromote2() seems
                  // to update the value returned by getPromote().
                  reference.setPromote(referencePromote);
                  break; // the for loop.
                }
              }
            }
          }
        }
      }
    }

    return composite;
  }

  private String messageError(Service service, String message)
  {
    return messageError("service", service.getName(), service.getPromote(), message);
  }

  private String messageError(Reference reference, String message)
  {
    return messageError("reference", reference.getName(), reference.getPromote(), message);
  }

  private String messageError(String kind, String name, String promote, String message)
  {
    StringBuffer sb = new StringBuffer();
    sb.append("<sca:");
    sb.append(kind);
    sb.append(" name='");
    sb.append(name);
    sb.append("' promote='");
    sb.append(promote);
    sb.append("'> component '");
    sb.append(promote);
    sb.append("' ");
    sb.append(message);
    return sb.toString();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
