/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaFactory;
import org.eclipse.stp.sca.Service;

import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParserException;

/**
 * Resolver for <implementation.composite>
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ImplementationCompositeResolver
     extends AbstractCopierResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Reference to the composite parser.
   */
  @org.osoa.sca.annotations.Reference(name = "composite-parser")
  private Parser<Composite> compositeParser;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    for (Component component : composite.getComponent()) {
      Implementation implementation = component.getImplementation();
      if(implementation instanceof SCAImplementation) {
        SCAImplementation scaImplementation = (SCAImplementation)implementation;

        if(scaImplementation.getName() == null) {
          continue; // pass to the next component;
        }

        if(parsingContext.getData(scaImplementation, Composite.class) != null) {
          log.fine("Already parsed '" + scaImplementation.getName() +"'");
          continue; // pass to the next component;
        }

        // Parse the composite of this <implementation.composite>.
        QName scaImplementationName = NormalizeQName.normalize(scaImplementation.getName(), scaImplementation, parsingContext);
        Composite scaImplementationComposite;
        try {
          scaImplementationComposite = compositeParser.parse(scaImplementationName, parsingContext);
        } catch(ParserException pe) {
          parsingContext.error("<sca:implementation.composite name='" + scaImplementationName + "'> not loaded");
          continue; // pass to the next component;
        }

        // Store the composite in the parsing context.
        parsingContext.putData(scaImplementation, Composite.class, scaImplementationComposite);
        
        // Merge composite services with component services.
        for(Service service : scaImplementationComposite.getService()) {
          ComponentService componentService =
              getComponentServiceByName(component.getService(), service.getName());          
          if(componentService == null) {
            // This component service does not exist then create it.
            componentService = ScaFactory.eINSTANCE.createComponentService();
            component.getService().add(componentService);
            componentService.setName(service.getName());
          }
          // if composite service has an interface and component service has no interface
          // then copy the composite service interface.
          if(service.getInterface() != null && componentService.getInterface() == null) {
            componentService.setInterface(copy(service.getInterface()));
          }
        }

        // Merge composite references with component references.
        for(Reference reference : scaImplementationComposite.getReference()) {
          ComponentReference componentReference =
              getComponentReferenceByName(component.getReference(), reference.getName());          
          if(componentReference == null) {
            // This component reference does not exist then create it.
            componentReference = ScaFactory.eINSTANCE.createComponentReference();
            component.getReference().add(componentReference);
            componentReference.setName(reference.getName());
          }
          // if composite reference has an interface and component reference has no interface
          // then copy the composite reference interface.
          if(reference.getInterface() != null && componentReference.getInterface() == null) {
            componentReference.setInterface(copy(reference.getInterface()));
          }
        }
      }
    }
    return composite;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
