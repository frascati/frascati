/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.Map;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Interface;

import org.ow2.frascati.parser.api.MultiplicityHelper;
import org.ow2.frascati.parser.api.ParsingContext;

public class ComponentReferenceTargetResolver
     extends AbstractInterfaceComparatorCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve Reference target attributes.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
    // A map of all <component name, component>
    Map<String, Component> components = mapOfComponents(composite);

    // Iterates on all component references.
    for (Component component : composite.getComponent()) {
      for (ComponentReference componentReference : component.getReference()) {
        // Do nothing for references with multiplicity equals to 0..N or 1..N.
        if(MultiplicityHelper.is0Nor1N(componentReference.getMultiplicity())) {
          continue;
        }
        // Complete target of references with multiplicity equals to 0..1 or 1..1.
        String componentReferenceTarget = componentReference.getTarget();
        // If component reference target is incomplete path.
        if (isIncompletePath(componentReferenceTarget)) {
          // Search the component targeted by the reference.
          Component targetComponent = components.get(componentReferenceTarget);
          if(targetComponent == null) {
            parsingContext.error("<sca:reference name='" + componentReference.getName() +
                                "' target='" + componentReferenceTarget + "'> component '" + componentReferenceTarget + "' undefined");
            continue; // the for loop.
          }
          // Check if the reference has an interface.
          Interface componentReferenceInterface = componentReference.getInterface();
          if(componentReferenceInterface == null) {
            parsingContext.error("<sca:reference name='" + componentReference.getName() +
                                "' target='" + componentReferenceTarget + "'> interface undefined");
            continue; // the for loop.
          }
          // Search the component service conform to the reference.
          ComponentService targetComponentService = this.getComponentServiceConformToInterface(targetComponent, componentReferenceInterface);
          if(targetComponentService == null) {
            parsingContext.error("<sca:reference name='" + componentReference.getName() +
                                "' target='" + componentReferenceTarget + "'> component '" + componentReferenceTarget +
           	                    "' doesn't provide a service with a compatible interface");
            continue; // the for loop.
          }
          // complete the component reference target attribute.
          componentReference.setTarget(componentReferenceTarget + "/" + targetComponentService.getName());
          componentReference.setTarget2(targetComponentService);
        }
      }
    }

    return composite;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
