/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.Interface;

public abstract class AbstractInterfaceComparatorCompositeResolver
              extends AbstractCompositeResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** Interface comparator. */
  private InterfaceComparator interfaceComparator = new InterfaceComparator();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Is an incomplete target or promote path?
   */
  protected static boolean isIncompletePath(String path)
  {
    return (path != null) && (path.split("/").length < 2);
  }

  protected final ComponentService getComponentServiceConformToInterface(Component component, Interface itf)
  {
    for (ComponentService componentService : component.getService()) {
      // Compare Interfaces using a comparator class
      Interface componentServiceInterface = componentService.getInterface();
      if (componentServiceInterface != null && this.interfaceComparator.compare(componentServiceInterface, itf) == 0) {
        // Same interfaces then return the first component service conform to the required interface.
    	return componentService;
      }
    }
    return null;
  }

  protected final ComponentReference getComponentReferenceConformToInterface(Component component, Interface itf)
  {
    for (ComponentReference componentReference : component.getReference()) {
      // Compare Interfaces using a comparator class
      Interface componentReferenceInterface = componentReference.getInterface();
      if (componentReferenceInterface != null && this.interfaceComparator.compare(componentReferenceInterface, itf) == 0) {
        // Same interfaces then return the first component reference conform to the required interface.
    	return componentReference;
      }
    }
    return null;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
