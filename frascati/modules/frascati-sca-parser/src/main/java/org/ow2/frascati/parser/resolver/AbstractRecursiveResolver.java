/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import org.eclipse.emf.ecore.EObject;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;

import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Abstract recursive resolver.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
@Scope("COMPOSITE")
@Service(Resolver.class)
public abstract class AbstractRecursiveResolver
           implements Resolver<EObject>
{
  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  public final EObject resolve(EObject eobject, ParsingContext parsingContext)
  {
    // Traverse the EMF tree recursively.
    for (EObject containedEObject : eobject.eContents()) {
      doResolve(containedEObject, parsingContext);
      resolve(containedEObject, parsingContext);
    }
    return eobject;
  }

  protected abstract void doResolve(EObject eobject, ParsingContext parsingContext);
}
