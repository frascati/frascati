/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import javax.xml.namespace.QName;
import org.eclipse.emf.ecore.EObject;
import org.ow2.frascati.parser.api.ParsingContext;

/**
 * Normalize QName.
 *
 * @author Philippe Merle at Inria
 * @version 1.5
 */
public class NormalizeQName
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * OSOA SCA namespace.
   */
  static final String OSOA_SCA_NS = "http://www.osoa.org/xmlns/sca/1.0";

  /**
   * Empty quname prefix.
   */
  static final String EMPTY_PREFIX = "";

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  static QName normalize(QName qname, EObject eobject, ParsingContext parsingContext)
  {
    // If the given qname has no prefix and has the OSOA_SCA_NS namespace then
    // change the namespace of this qname to the location where the given eobject was loaded.
    if(qname != null && EMPTY_PREFIX.equals(qname.getPrefix()) && OSOA_SCA_NS.equals(qname.getNamespaceURI())) {
      // Get the URI where the composite was loaded.
      String uri = parsingContext.getLocationURI(eobject);
      // Compute the new namespace of the given qname.
      int indexStart = 0;
      if(uri.startsWith("jar:file:")) {
        indexStart = uri.indexOf('!') + 2;
      } else if(uri.startsWith("file:")) {
        indexStart = 0;
      } else {
        return qname;
      }
      int indexEnd = uri.lastIndexOf('/');
      if(indexStart <= indexEnd) {
        String namespace = uri.substring(indexStart, indexEnd);
        // change the qname.
        qname = new QName(namespace, qname.getLocalPart(), "a_prefix_is_required_by_the_AbstractParser_toStringURI_method");
      }
	}
    return qname;
  }

}
