/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * OW2 FraSCAti SCA resolver abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public abstract class AbstractResolver<Type>
              extends AbstractLoggeable
           implements Resolver<Type>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  @Reference(name = "next-resolver", required=false)
  private Resolver<Type> nextResolver;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  protected abstract Type doResolve(Type element, ParsingContext parsingContext);

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  public final Type resolve(Type element, ParsingContext parsingContext)
  {
      Type result = doResolve(element, parsingContext);
      if(nextResolver != null) {
        result = nextResolver.resolve(result, parsingContext);
      }
      return result;
  }

}
