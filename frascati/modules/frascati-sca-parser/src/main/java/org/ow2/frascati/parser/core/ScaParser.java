/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2009-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.parser.core;

import java.util.List;
import java.io.FileNotFoundException;
import javax.xml.namespace.QName;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;

import org.eclipse.stp.sca.util.ScaResourceFactoryImpl;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.parser.api.MetamodelProvider;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.api.Resolver;

/**
 * OW2 FraSCAti SCA Parser component implementation.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.1
 */
public class ScaParser
     extends AbstractParser<EObject>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** List of metamodel providers. */
  @Reference(name = "metamodels")
  private List<MetamodelProvider<?>> metamodelProviders;

  /** List of resolvers. */
  @Reference(name = "resolvers")
  private List<Resolver<EObject>> resolvers;

  // Create a resource set to hold the resources.
  private ResourceSet resourceSet;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Store into the parsing context the document location URI where a given EObject
   * and its content children are defined.
   * This document location URI could be obtained with {@link ParsingContext#getLocationURI(EObject).
   */
  private void storeLocationURI(EObject eObject, URI uri, ParsingContext parsingContext)
  {
    parsingContext.putData(eObject, URI.class, uri);
    for(EObject child : eObject.eContents()) {
      storeLocationURI(child, uri, parsingContext);
    }
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  @Init
  public final void initialize()
  {
    // Initialize EMF resource
    this.resourceSet = new ResourceSetImpl();

    ExtendedMetaData extendedMetaData = new BasicExtendedMetaData(resourceSet.getPackageRegistry());
    this.resourceSet.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);

    // Register the appropriate resource factory to handle all file
    // extensions.
    this.resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
        "*", new ScaResourceFactoryImpl());

	// Registry the EPackage associated to each MetamodelProvider.
    for(MetamodelProvider<?> metamodelProvider : metamodelProviders) {
      EPackage ePackage = metamodelProvider.getEPackage();
      this.resourceSet.getPackageRegistry().put(ePackage.getNsURI(), ePackage);
    }
  }

  /**
   * Parse any SCA document.
   *
   * @see org.ow2.frascati.parser.api.Parser#parser(ReturnType, ParsingContext)
   */
  public final EObject parse(QName qname, ParsingContext parsingContext)
      throws ParserException
  {	  
    String documentUri = toStringURI(qname);

    log.fine("Getting EMF resource '" + documentUri + "'...");

    // Parse the SCA document indicated by its uri.
    URI resourceUri = URI.createURI(documentUri);
    Resource resource;
    try {
      resource = resourceSet.getResource(resourceUri, true);
    } catch(Exception e) {
      Throwable cause = e.getCause();
      if(cause instanceof FileNotFoundException) {
        warning(new ParserException(qname, "'" + documentUri + "' not found", cause));
      } else {
        warning(new ParserException(qname, "'" + documentUri + "' invalid content", cause));
      }
      return null;
    }

    // ResourceSet acts as a Resource cache, i.e., get several times a same URI always returns the same resource.
    // But when several <sca.implementation.composite name="C"> are declared, FraSCAti Assembly Factory requires
    // that different EMF object graphes are instantiated. This issue was pointed out by Jonathan.
    // So it is needed to clear the Resource cache after each getResource().
    resourceSet.getResources().clear();

    // Get the first element of the loaded resource.
    EObject result = resource.getContents().get(0);

	// Apply all resolvers.
    for(Resolver<EObject> resolver : resolvers) {
      result = resolver.resolve(result, parsingContext);
    }

    // Store the resourceUri as the document location URI of the parsed document root and all its contents.
    storeLocationURI(result, resourceUri, parsingContext);

    return result;
  }
}
