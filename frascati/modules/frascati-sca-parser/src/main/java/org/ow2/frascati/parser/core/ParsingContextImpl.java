/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.core;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.util.context.ClassLoaderContextImpl;

/**
 * OW2 FraSCAti SCA Parser parser context implementation class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public class ParsingContextImpl
     extends ClassLoaderContextImpl
  implements ParsingContext
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** structure to store data of this parsing context. */
  private Map<Object, Map<Class<?>, Object>> data = new HashMap<Object, Map<Class<?>, Object>>();

  /** the number of warnings. */
  private int nbWarnings;

  /** the number of errors. */
  private int nbErrors;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs with a new default class loader.
   */
  public ParsingContextImpl()
  {
    super();
  }

  /**
   * Constructs with a class loader.
   *
   * @param classLoader the class loader of the parsing context.
   */
  public ParsingContextImpl(ClassLoader classLoader)
  {
    super(classLoader);
  }

  /**
   * @see ParsingContext#putData(Object, Class, T)
   */
  public final <T> void putData(Object key, Class<T> type, T data)
  {
    Map<Class<?>, Object> data4key = this.data.get(key);
    if(data4key == null) {
      data4key = new HashMap<Class<?>, Object>();
      this.data.put(key, data4key);
    }
    data4key.put(type, data);
  }

  /**
   * @see ParsingContext#getData(Object, Class)
   */
  @SuppressWarnings("unchecked")
  public final <T> T getData(Object key, Class<T> type)
  {
    Map<Class<?>, Object> data4key = this.data.get(key);
    if(data4key == null) {
      return null;
    }
    return (T)data4key.get(type);
  }

  /**
   * @see ParsingContext#warning(String)
   */
  public final void warning(String message)
  {
    log.warning(message);
    this.nbWarnings++;
  }

  /**
   * @see ParsingContext#getWarnings()
   */
  public final int getWarnings()
  {
    return this.nbWarnings;
  }

  /**
   * @see ParsingContext#error(String)
   */
  public final void error(String message)
  {
    log.severe(message);
    this.nbErrors++;
  }

  /**
   * @see ParsingContext#getErrors()
   */
  public final int getErrors()
  {
    return this.nbErrors;
  }

  /**
   * @see ParsingContext#getLocationURI(EObject)
   */
  public String getLocationURI(EObject eObject)
  {
    URI uri = getData(eObject, URI.class);
    return uri == null ? null : uri.toString();
  }
}
