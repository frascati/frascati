/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.core;

import javax.xml.namespace.QName;

import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * OW2 FraSCAti SCA parser abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
@Scope("COMPOSITE")
public abstract class AbstractParser<ReturnType>
              extends AbstractLoggeable
           implements Parser<ReturnType>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  protected static String toStringURI(QName qname)
  {
    //
    // Compute the URI string for the qname.
    //
    String documentUri = qname.getLocalPart();

    String prefix = qname.getPrefix();
    if (prefix != null && !prefix.equals("")) {
      String namespaceURI = qname.getNamespaceURI();

      // Force the namespace uri to end with "/"
      if (!namespaceURI.endsWith("/")) {
    	  namespaceURI = namespaceURI + "/";
      }

      //
      // This was added to deal with <implementation.composite name="ns:Composite"/>
      // where xmlns:ns starts with 'http://'.
      // OASIS SCA Assembly Model Suite Tests contain this kind of namespaces.
      //
      // Then composites are searched into the parsing context class loader instead of
      // loaded via HTTP.
      //
      // TODO: Perhaps another solution is required when FraSCAti will manage 
      // export/import elements from SCA contribution descriptors.
      //
      if(namespaceURI.startsWith("http://")) {
        // Ignore the namespace.
        namespaceURI = "";
      }

      // Concatenate namespaceURI and uri
      documentUri = namespaceURI + documentUri;
    }

    return documentUri;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
