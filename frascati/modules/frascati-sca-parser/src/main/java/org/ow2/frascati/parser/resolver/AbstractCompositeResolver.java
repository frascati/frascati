/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.ComponentReference;

/**
 * OW2 FraSCAti SCA composite resolver abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractCompositeResolver
              extends AbstractResolver<Composite>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Returns a map of all <component name, component>.
   */
  protected static Map<String, Component> mapOfComponents(Composite composite)
  {
    HashMap<String, Component> components = new HashMap<String, Component>();
    for (Component component : composite.getComponent()) {
      // register each component by its name.
      components.put(component.getName(), component);
    }
    return components;
  }

  protected static ComponentService getComponentServiceByName(List<ComponentService> services, String name)
  {
    if(name != null) {
      for(ComponentService cs : services) {
        if(name.equals(cs.getName())) {
          return cs;
        }
      }
    }
    return null;
  }

  protected static ComponentReference getComponentReferenceByName(List<ComponentReference> references, String name)
  {
    if(name != null) {
      for(ComponentReference cr : references) {
        if(name.equals(cr.getName())) {
          return cr;
        }
      }
    }
    return null;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
