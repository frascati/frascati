/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.resolver;

import java.io.IOException;
import javax.xml.namespace.QName;
import java.util.List;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.ConstrainingType;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParserException;
import org.ow2.frascati.parser.api.ParsingContext;

public class ConstrainingTypeResolver
     extends AbstractCopierResolver
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** Reference to the constraining type parser */
  @Reference(name = "constraining-type-parser")
  private Parser<ConstrainingType> constrainingTypeParser;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Resolve promote attributes.
   *
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  @Override
  protected final Composite doResolve(Composite composite, ParsingContext parsingContext)
  {
	QName constrainingTypeQN = NormalizeQName.normalize(composite.getConstrainingType(), composite, parsingContext);
	if(constrainingTypeQN != null) {
      ConstrainingType constrainingType = parseConstrainingType(constrainingTypeQN, parsingContext);
      if(constrainingType != null) {
          parsingContext.warning("<composite constrainingType=...> not supported by the OW2 FraSCAti SCA parser");
          // TODO IMPLEMENT
      }
	}

	for(Component component : composite.getComponent()) {
      constrainingTypeQN = NormalizeQName.normalize(component.getConstrainingType(), component, parsingContext);
      if(constrainingTypeQN != null) {
        ConstrainingType constrainingType = parseConstrainingType(constrainingTypeQN, parsingContext);
        if(constrainingType != null) {

          // TODO : why next line is required?
          component.setConstrainingType(null);
        	
          List<ComponentService> componentServices = component.getService();
          // add/merge component services
          for(ComponentService s : constrainingType.getService()) {
            ComponentService cs = getComponentServiceByName(componentServices, s.getName());
            if(cs == null) {
              log.fine("Add component service '" + s.getName() + "'");
              cs = copy(s);
              if(s.getInterface() != null) {
                cs.setInterface(copy(s.getInterface()));
              }
              componentServices.add(cs);
            } else {
              log.warning("Merge component service '" + s.getName() + "' NOT IMPLEMENTED");
              // TODO merge s to cs
            }
          }

          List<ComponentReference> references = component.getReference();
      	  // add/merge component references
          for(ComponentReference r : constrainingType.getReference()) {
        	ComponentReference cr = getComponentReferenceByName(references, r.getName());
            if(cr == null) {
          	  log.fine("Add component reference '" + r.getName() + "'");
              cr = copy(r);
              if(r.getInterface() != null) {
                cr.setInterface(copy(r.getInterface()));
              }
              references.add(cr);
            } else {
              log.warning("Merge component reference '" + r.getName() + "' NOT IMPLEMENTED");
              // TODO merge s to cs
            }
          }

          // TODO add when this is a new element not defined by the component
          // TODO merge when this is an element already defined by the component
          // add/merge requires
//        component.getRequires().addAll(0, constrainingType.getRequires());
          // add/merge properties
//        component.getProperty().addAll(0, constrainingType.getProperty());
        }
      }
	}

	return composite;
  }

  private ConstrainingType parseConstrainingType(QName qname, ParsingContext parsingContext)
  {
    try {
      return constrainingTypeParser.parse(qname, parsingContext);
	} catch(ParserException parserException) {
      if(parserException.getCause() instanceof IOException) {
        parsingContext.error("constraining type '" + qname.toString() + "' not found");
      } else {
        parsingContext.error("constraining type '" + qname.toString() + "' invalid content");
      }
      return null;
	}
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
