/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Nicolas Dolet, Philippe Merle
 *
 */

package org.ow2.frascati.parser.core;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import javax.xml.namespace.QName;

import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.Include;
import org.eclipse.stp.sca.ScaFactory;

import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.api.ParserException;

/**
 * OW2 FraSCAti SCA Parser composite parser abstract implementation.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.1
 */
public class AbstractCompositeParser
     extends AbstractDelegateScaParser<Composite>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Parser#parse(QName, ParsingContext)
   */
  public Composite parse(QName qname, ParsingContext parsingContext)
    throws ParserException
  {
	// TODO following code must be factorized with similar code in ScaConstrainingType

    String compositeUri = asStringURI(qname);

    // Search all URLs in the class loader matching the composite uri.
    List<URL> compositeUrls;
    try {
        compositeUrls = Collections.list(parsingContext.getClassLoader().getResources(compositeUri));
    } catch(IOException ioe) {
        severe(new ParserException(qname, qname.toString(), ioe));
        return null;
    }

    log.fine("URLs for " + compositeUri + " are:");
    for(URL url : compositeUrls) {
      log.fine("* " + url);
    }

    //
    // Compute the composite URI
    //
    QName compositeQN = null;
    if (compositeUrls.size() > 0) {
      compositeQN = new QName(compositeUrls.remove(0).toString());
    } else {
      // Composite file not loaded as resource of the current class loader
      // Try to load from the local file system or an URL.
      compositeQN = new QName(compositeUri);
    }

    Composite composite = parseDocument(compositeQN, parsingContext).getComposite();

    // Include other composite files found in the class loader.
    List<Include> includes = composite.getInclude();
    for(URL url : compositeUrls) {
      Include include = ScaFactory.eINSTANCE.createInclude();
      include.setName(new QName(url.toString()));
      includes.add(include);
    }

    return composite;
  }

}
