/**
 * OW2 FraSCAti: SCA Parser
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.parser.core;

import javax.xml.namespace.QName;

import org.eclipse.stp.sca.DocumentRoot;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

import org.ow2.frascati.parser.api.Parser;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.parser.api.ParserException;

/**
 * OW2 FraSCAti SCA parser delegate abstract class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.3
 */
public abstract class AbstractDelegateScaParser<ReturnType>
              extends AbstractParser<ReturnType>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  @Reference(name = "sca-parser")
  private Parser<DocumentRoot> scaParser;

  @Property(name = "file-extension")
  private String fileExtension;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  protected final String asStringURI(QName qname)
  {
    //
    // Compute the URI string for the qname.
    //
    String documentUri = toStringURI(qname);

    // add fileExtension at the end of the uri if necessary
    if (!documentUri.endsWith(fileExtension)) {
    	documentUri = documentUri + fileExtension;
    }

    return documentUri;
  }

  protected final DocumentRoot parseDocument(QName qn, ParsingContext parsingContext)
      throws ParserException
  {
	QName qname = qn;
    String localPart = qname.getLocalPart();
    if(!localPart.endsWith(fileExtension)) {
    	qname = new QName(qname.getNamespaceURI(), localPart + fileExtension, qname.getPrefix());
    }
    return scaParser.parse(qname, parsingContext);
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
