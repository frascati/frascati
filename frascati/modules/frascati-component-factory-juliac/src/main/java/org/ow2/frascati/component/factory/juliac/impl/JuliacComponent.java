/**
 * OW2 FraSCAti Component Factory Juliac
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle, Lionel Seinturier
 *
 */

package org.ow2.frascati.component.factory.juliac.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.fraclet.types.Constants;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.conf.JDKLevel;
import org.objectweb.fractal.juliac.conf.JuliacConfig;
import org.objectweb.fractal.juliac.conf.JulietLoader;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext;
import org.ow2.frascati.component.factory.api.ComponentFactoryContext.MembraneDescription;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.MembraneGeneration;
import org.ow2.frascati.component.factory.api.MembraneProvider;
import org.ow2.frascati.component.factory.juliac.api.JuliacCompilerProvider;
import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;
import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * OW2 FraSCAti Component Factory Juliac component class.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class JuliacComponent
     extends AbstractScopeCompositeLoggeable
  implements MembraneGeneration
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  public static final String FRASCATI_OUTPUT_DIRECTORY_PROPERTY = "org.ow2.frascati.output.directory";

  /**
   * Environment variable associated with default output directory.
   */
  @Property(name = "FRASCATI-GENERATED")
  private String frascatiGeneratedDirectory = "FRASCATI_GENERATED";

  /**
   * Maven target directory.
   */
  @Property(name = "MAVEN-TARGET-DIRECTORY")
  private String mavenTargetDirectory = "target";

  /**
   * Output directory for generated sources.
   */
  @Property(name = "GEN-DIRECTORY")
  private String genDirectory = "/generated-frascati-sources";

  // Output directory for compiled classes
  // Must not be loaded by a ClassLoader which is not managed by the Assembly Factory
  @Property(name = "CLASS-DIRECTORY")
  private String classDirectory = "/generated-frascati-classes";

  /**
   * Reference to Juliac generator plugins.
   */
  @Reference(name = "generators")
  private List<MembraneProvider> juliacGeneratorClassProviders;

  /**
   * Reference to the Juliac compiler provider.
   */
  @Reference(name = "compiler-provider", required=false)
  private JuliacCompilerProvider juliacCompilerProvider;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Deletes all files and subdirectories under dir.
   * Returns true if all deletions were successful.
   * If a deletion fails, the method stops attempting to delete and returns false.
   */
  private static boolean deleteDir(File dir)
  {
    if(dir.isDirectory()) {
      String[] children = dir.list();
      for (int i=0; i<children.length; i++) {
        boolean success = deleteDir(new File(dir, children[i]));
        if (!success) {
          return false;
        }
      }
    }
    // The directory is now empty so delete it
    return dir.delete();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see MembraneGeneration#open(ComponentFactoryContext)
   */
  public final void open(ComponentFactoryContext context) throws FactoryException
  {
    File f;
    try {
      // The Juliac output directory is equals to:
      // - the value of the FRASCATI_OUTPUT_DIRECTORY_PROPERTY Java property if set, or
      // - the value of the FRASCATI_GENERATED system variable if set, or
      // - the Maven target directory if exist, or
      // - a new temp directory.
      String defaultOutputDir =  System.getenv().get(frascatiGeneratedDirectory);
      if(defaultOutputDir != null) {
        f = new File(defaultOutputDir);
      } else {
        defaultOutputDir = System.getProperty(FRASCATI_OUTPUT_DIRECTORY_PROPERTY);
        if(defaultOutputDir != null) {
          f = new File(defaultOutputDir);
        } else {
          f = new File(new File(".").getAbsolutePath() + File.separator + mavenTargetDirectory).getAbsoluteFile();
          if(!f.exists()) {
            f = File.createTempFile("frascati",".tmp");
            f.delete();  // delete the file
            f.mkdir();   // recreate it as a directory
            f.deleteOnExit(); // delete it when the JVM will exit.
          }
        }
      }
      // Set the default output dir.
      if (!f.exists()) {
        // Create output dir if not exists
        f.mkdirs();
      }
      if (!f.isDirectory()) {
        throw new FactoryException(f.toString() + " must be a directory");
      }
      log.info("Default Juliac output directory: " + f.toString());
    } catch(IOException ioe) {
      severe(new FactoryException("Problem when creating a FraSCAti temp directory", ioe));
      return;
    }
    context.setOutputDirectory(f.toString());
  }

  /**
   * @see MembraneGeneration#close(ComponentFactoryContext)
   */
  public final void close(ComponentFactoryContext context) throws FactoryException
  {
    File outputDirectory = new File(context.getOutputDirectory());

    //
    // Create a Juliac instance.
    //
    Juliac jc = null;
    try {
      jc = new Juliac();
  	} catch (Exception e) {
  	  severe(new FactoryException("Problem when instantiating Juliac", e));
      return;
    } 

    //
    // Create a JuliacConfig instance.
    //
    JuliacConfig jcfg = new JuliacConfig(jc);
    jc.setJuliacConfig(jcfg);

    // Work with Java 1.5.
    jcfg.setSourceLevel(JDKLevel.JDK1_5);
    jcfg.setTargetLevel(JDKLevel.JDK1_5);

    // Set the output directory.
    jcfg.setBaseDir(outputDirectory);

    // Use the current context class loader.
    jcfg.setClassLoader(context.getClassLoader());

    //
    // Load the Juliac module associated to the used Java compiler if any.
    //
    if(this.juliacCompilerProvider != null) {
      String compiler = juliacCompilerProvider.getJuliacCompiler();
      try {
        jcfg.loadModule(compiler);
      } catch(IOException ioe) {
        severe(new FactoryException(ioe));
        return;
      }
    }

    //
    // Load the Juliac module of each membrane provider.
    //
    for(MembraneProvider jgc : juliacGeneratorClassProviders) {
      String name = jgc.getMembraneClass().getCanonicalName();
      try {
        jcfg.loadModule(name);
      } catch(IOException ioe) {
        severe(new FactoryException(ioe));
        return;
      }
  	}

    //
    // Compile Java source directories.
    //
    try {
      File classDir = null;

      String defaultClassDir = System.getProperty("org.ow2.frascati.classes.directory");
      if(defaultClassDir != null) {
        classDir = new File(defaultClassDir);
        if(!classDir.exists()) {
          classDir.mkdirs();
        }
      } else {
        classDir = File.createTempFile(classDirectory, "", outputDirectory);
        classDir.delete();  // delete the file
        classDir.mkdir();
      }
      jcfg.setClassDirName(classDir.getCanonicalPath());
      ((FrascatiClassLoader)context.getClassLoader()).addUrl(classDir.toURI().toURL());
    } catch(Exception e) {
      severe(new FactoryException(e));
      return;
    }

    // Add Java source directories to be compiled by Juliac.
    String[] javaSourceDirectoryToCompile = context.getJavaSourceDirectoryToCompile();
    for (String src : javaSourceDirectoryToCompile) {
  	  log.info("* compile Java source: " + src);
      try {
        jcfg.addSrc(src);
  	  } catch(IOException ioe) {
        severe(new FactoryException("Cannot add Java sources", ioe));
        return;
      }
  	}

  	if(javaSourceDirectoryToCompile.length > 0) {
      try {
        // Compile all Java sources with Juliac.
        jc.compile();
      } catch(Exception e) {
        warning(new FactoryException("Errors when compiling Java source", e));
        return;
      }
    }

  	MembraneDescription[] membranes = context.getMembraneToGenerate();

    if(membranes.length > 0) {
      File generatedJavaDir = null;
      try {
        generatedJavaDir = File.createTempFile(genDirectory, "", outputDirectory);
        generatedJavaDir.delete();  // delete the file
        generatedJavaDir.mkdir();
        jcfg.setGenDirName(generatedJavaDir.getCanonicalPath());
      } catch(Exception e) {
        severe(new FactoryException(e));
        return;
      }
    }

    // Generate all the membranes to generate.
	for(MembraneDescription md : membranes) {
      try {
    	if("scaPrimitive".equals(md.membraneDesc) && md.contentDesc instanceof String) {
          String className = (String)md.contentDesc;
          Class<?> clazz = null;
          try {
            clazz = context.loadClass(className);
          } catch(Exception e) {
            severe(new FactoryException("Cannot load class " + className, e));		
          }
          // Is the content class annotated with @org.objectweb.fractal.fraclet.extensions.Membrane?
          Membrane membrane = clazz.getAnnotation(Membrane.class);
          if(membrane != null) {
        	// The content class is annotated with @Membrane.
            // Is its 'controllerDesc' attribute defined?
            clazz = membrane.controllerDesc();
    		if( ! clazz.equals(Constants.class) ) {
              // Yes, then this controlledDesc is annotated with @Membrane.
              membrane = clazz.getAnnotation(Membrane.class);
              // Add to Juliac a binding between the membrane name and the membrane class
              // else Juliac could not know the membrane class.
              JulietLoader mloader =
          	    jcfg.loadModuleIfNew(
        		  JulietLoader.class.getName(), JulietLoader.class );
              mloader.put(membrane.desc(), clazz);
            }
            // Change the membraneDesc to use to instantiate the component.
            md.membraneDesc = membrane.desc();
          }
        }
        jc.getFCSourceCodeGenerator(md.membraneDesc)
    	  .generate(md.componentType, md.membraneDesc, md.contentDesc);
      } catch (Exception e) {
        severe(new FactoryException("Cannot generate component code with Juliac", e));
        return;
      }
	}

	// Compile all generated membranes with Juliac.
	try {
      jc.compile();
    } catch(Exception e) {
	  warning(new FactoryException("Errors when compiling generated Java membrane source", e));
	  return;
    }

    // Close Juliac.
    try {
      jc.close();
    } catch(Exception e) {
  	  severe(new FactoryException("Cannot close Juliac", e));
      return;
    }
  }
}
