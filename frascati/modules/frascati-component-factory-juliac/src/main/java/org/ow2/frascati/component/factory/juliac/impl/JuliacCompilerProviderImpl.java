/**
 * OW2 FraSCAti Component Factory Juliac
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.component.factory.juliac.impl;

import org.osoa.sca.annotations.Property;
import org.ow2.frascati.component.factory.juliac.api.JuliacCompilerProvider;
import org.ow2.frascati.util.AbstractScopeCompositeLoggeable;

/**
 * OW2 FraSCAti Component Factory Juliac compiler provider implementation class.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.4
 */
public class JuliacCompilerProviderImpl
     extends AbstractScopeCompositeLoggeable
  implements JuliacCompilerProvider
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Property for the Juliac compiler name.
   */
  @Property(name = "juliac-compiler")
  private String juliacCompiler;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see JuliacCompilerProvider#getJuliacCompiler()
   */
  public String getJuliacCompiler()
  {
    return this.juliacCompiler;
  }

}
