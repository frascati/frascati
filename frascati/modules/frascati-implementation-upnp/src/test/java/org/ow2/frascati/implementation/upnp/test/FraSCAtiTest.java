/**
 * OW2 FraSCAti: SCA Implementation Widget
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.upnp.test;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;

import javax.xml.namespace.QName;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.implementation.upnp.UPnPException;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Gwenael Cattez.
 */
public class FraSCAtiTest {

    FraSCAti frascati;
    CompositeManager compositeManager;

    @Before
    public void initFraSCAti() throws Exception {
      frascati = FraSCAti.newFraSCAti();
      compositeManager = frascati.getCompositeManager();
    }

    /**
     * Check errors produced during the checking phase.
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processCheckingErrorsWarningsComposite() throws Exception {
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        compositeManager.processComposite(new QName("CheckingErrorsWarnings"), processingContext);
      } catch(ManagerException me) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrorsWarnings.composite'
    	assertEquals("The number of checking errors", 7, processingContext.getErrors());
      }
    }

    @Test
    public void processExample1Composite()
    {
        try
        {
            // Try to get the example1 composite, fail because the device upnpDeviceName not found
            Component example1Composite=frascati.getComposite("example1");
            UPnPDeviceTestItf uPnPDeviceTest=frascati.getService(example1Composite, "example1Service", UPnPDeviceTestItf.class);
            uPnPDeviceTest.action("UPnPVariable");
        }
        catch (Exception e)
        {
            assertEquals(TinfiRuntimeException.class, e.getClass());
            Throwable cause=e.getCause();
            assertEquals(ContentInstantiationException.class, cause.getClass());
            cause=cause.getCause();
            assertEquals(InvocationTargetException.class, cause.getClass());
            cause=cause.getCause();
            assertEquals(UPnPException.class, cause.getClass());
            assertEquals("Device upnpDeviceName not found", cause.getMessage());
            System.out.println(cause.getMessage());
        }

    }
    
}
