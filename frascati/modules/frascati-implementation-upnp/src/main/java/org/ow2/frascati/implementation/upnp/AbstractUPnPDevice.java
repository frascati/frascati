/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.implementation.upnp;

import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.header.STAllHeader;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.ActionArgument;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.RemoteService;
import org.teleal.cling.model.types.Datatype;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.registry.RegistryListener;

public abstract class AbstractUPnPDevice implements RegistryListener
{
    public final static Logger logger = Logger.getLogger(AbstractUPnPDevice.class.getName());
    private final static Long SCAN_TIMEOUT = 3000L;

    @Property(name = "upnpDeviceName")
    private String upnpDeviceName;

    private UpnpService upnpService;
    
    private RemoteDevice upnpDevice;

    public AbstractUPnPDevice()
    {
        this.upnpDevice = null;
        this.upnpService = new UpnpServiceImpl(this);
    }

    @Init
    public void initAbstractUPnPDevice() throws InterruptedException, UPnPException
    {
        // Send a search message to all devices and services, they should
        // respond soon
        this.upnpService.getControlPoint().search(new STAllHeader());

        logger.info("Scanning network...");
        Thread.sleep(SCAN_TIMEOUT);
        if (upnpDevice == null)
        {
            throw new UPnPException(upnpDeviceName);
        }
    }

    protected void doAction(String deviceAction, Map<String, Object> args)
    {
        Action<?> action = this.getAction(deviceAction);
        ActionInvocation<?> actionInvocation = new ActionInvocation(action);
        Object argValue;
        for (Entry<String, Object> arg : args.entrySet())
        {
            argValue = this.getArgValue(action, arg.getKey(), arg.getValue());
            actionInvocation.setInput(arg.getKey(), argValue);
        }

        ActionCallback setTargetCallback = new ActionCallback.Default(actionInvocation, upnpService.getControlPoint());
        setTargetCallback.run();
        // return setTargetCallback.getActionInvocation().getOutput(); //
        // Tableau de retour
    }

    private Action<?> getAction(String actionName)
    {
        for (RemoteService rs : upnpDevice.findServices())
        {
            for (Action<?> act : rs.getActions())
            {
                if (act.getName().equalsIgnoreCase(actionName))
                {
                    return act;
                }
            }
        }
        return null;
    }

    private Object getArgValue(Action<?> action, String argName, Object value)
    {
        ActionArgument<?> actionArgument = action.getInputArgument(argName);
        Datatype<?> datatype = action.getService().getDatatype(actionArgument);
        if (datatype == null)
        {
            return null;
        }
        return datatype.valueOf(value.toString());
    }

    public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice device)
    {
        logger.info("Discovery started: " + device.getDisplayString());
    }

    public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice device, Exception ex)
    {
        logger.info("Discovery failed: " + device.getDisplayString() + " => " + ex);
    }

    public void remoteDeviceAdded(Registry registry, RemoteDevice device)
    {
        // new device found add to devices list
        logger.info("Remote device available: " + device.getDisplayString());
        if (device.getDisplayString().equals(this.upnpDeviceName))
        {
            this.upnpDevice = device;
        }
    }

    public void remoteDeviceUpdated(Registry registry, RemoteDevice device)
    {
        logger.info("Remote device updated: " + device.getDisplayString());
    }

    public void remoteDeviceRemoved(Registry registry, RemoteDevice device)
    {
        logger.info("Remote device removed: " + device.getDisplayString());
    }

    public void localDeviceAdded(Registry registry, LocalDevice device)
    {
        logger.info("Local device added: " + device.getDisplayString());
    }

    public void localDeviceRemoved(Registry registry, LocalDevice device)
    {
        logger.info("Local device removed: " + device.getDisplayString());
    }

    public void beforeShutdown(Registry registry)
    {
        logger.info("Before shutdown, the registry has devices: " + registry.getDevices().size());
    }

    public void afterShutdown()
    {
        logger.info("Shutdown of registry complete!");
    }
}
