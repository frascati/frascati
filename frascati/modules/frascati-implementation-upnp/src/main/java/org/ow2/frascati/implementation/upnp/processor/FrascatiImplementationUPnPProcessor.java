/**
 * OW2 FraSCAti: SCA Implementation UPnP
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor:
 *
 */

package org.ow2.frascati.implementation.upnp.processor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.JavaInterface;
import org.oasisopen.sca.annotation.Property;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.implementation.upnp.AbstractUPnPDevice;
import org.ow2.frascati.implementation.upnp.UPnPAction;
import org.ow2.frascati.implementation.upnp.UPnPService;
import org.ow2.frascati.implementation.upnp.UPnPVariable;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.upnp.UPnPImplementation;
import org.ow2.frascati.upnp.UpnpPackage;

/**
 * OW2 FraSCAti Assembly Factory implementation widget processor class.
 * 
 * @author Gwenael Cattez - INRIA
 * @version 1.5
 */
public class FrascatiImplementationUPnPProcessor extends AbstractComponentFactoryBasedImplementationProcessor<UPnPImplementation>
{
    public static final String UPNP_DEVICE_NAME_PROPERTY = "upnpDeviceName";

    // ---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    /**
     * Package where content classes are generated.
     */
    @Property(name = "package")
    private String packageGeneration;

    // ---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType,
     *      StringBuilder)
     */
    @Override
    protected final void toStringBuilder(UPnPImplementation upnpImplementation, StringBuilder sb)
    {
        sb.append("upnp:implementation.upnp");
        super.toStringBuilder(upnpImplementation, sb);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doCheck(UPnPImplementation upnpImplementation, ProcessingContext processingContext) throws ProcessorException
    {
        String upnpDeviceName = upnpImplementation.getUpnpDeviceName();
        checkAttributeMustBeSet(upnpImplementation, "upnpDeviceName", upnpDeviceName, processingContext);

        // Get the enclosing SCA component.
        Component parentComponent = getParent(upnpImplementation, Component.class);
        List<ComponentReference> componentReferences = parentComponent.getReference();
        if (!componentReferences.isEmpty())
        {
            error(processingContext, upnpImplementation, "<implementation.upnp> can not have reference");
        }

        List<ComponentService> componentServices = parentComponent.getService();
        if (componentServices.isEmpty())
        {
            error(processingContext, upnpImplementation, "<implementation.upnp> must have at least one service");
        }

        List<Class<?>> componentServicesJavaItf = new ArrayList<Class<?>>();
        JavaInterface componentServiceJavaItf;
        Class<?> componentServiceClass;
        for (ComponentService componentService : componentServices)
        {
            if (componentService.getInterface() == null || !(componentService.getInterface() instanceof JavaInterface))
            {
                error(processingContext, upnpImplementation, "<implementation.upnp> service " + componentService.getName() + " must define a java interface");
                continue;
            }
            componentServiceJavaItf = (JavaInterface) componentService.getInterface();

            try
            {
                componentServiceClass = Class.forName(componentServiceJavaItf.getInterface());
            } catch (ClassNotFoundException classNotFoundException)
            {
                throw new ProcessorException(upnpImplementation, classNotFoundException);
            }

            if (!isValidUPnPItf(componentServiceClass))
            {
                error(processingContext, upnpImplementation, "<implementation.upnp> interface " + componentServiceClass.getName()
                        + " is not valid UPnP interface : class, methods and fields must be UPnP annotated");
            } else
            {
                componentServicesJavaItf.add(componentServiceClass);
            }
        }

        processingContext.putData(upnpImplementation, List.class, componentServicesJavaItf);
    }

    /**
     * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType,
     *      ProcessingContext)
     */
    @Override
    protected final void doGenerate(UPnPImplementation upnpImplementation, ProcessingContext processingContext) throws ProcessorException
    {
        List<Class<?>> componentServicesJavaItf = processingContext.getData(upnpImplementation, List.class);

        // Generate hash code from name of the implemented interface
        String hashCodeGeneration = "";
        for (Class<?> c : componentServicesJavaItf)
            hashCodeGeneration += c.getName();

        int hashCode = hashCodeGeneration.hashCode();
        if (hashCode < 0)
        {
            hashCode = -hashCode;
        }

        // Generate className with package-generation (see)
        String contentFullClassName = null;
        String contentClassName = UPnPImplementation.class.getSimpleName() + hashCode;
        contentFullClassName = this.packageGeneration + '.' + contentClassName;

        // If class not exist generate ->
        try
        {
            processingContext.loadClass(contentFullClassName);
        } catch (ClassNotFoundException cnfe)
        {
            // Create directory to received generated class (see)
            // If the class can not be found then generate it.
            log.info(packageGeneration + '.' + contentClassName);

            // Create the output directory for generation.
            String outputDirectory = processingContext.getOutputDirectory() + "/generated-frascati-sources";

            // Add the output directory to the Java compilation process.
            processingContext.addJavaSourceDirectoryToCompile(outputDirectory);

            try
            { 
                // Generate implementation
                File packageDirectory = new File(outputDirectory + '/' + packageGeneration.replace('.', '/'));
                packageDirectory.mkdirs();
                PrintStream file = new PrintStream(new FileOutputStream(new File(packageDirectory, contentClassName + ".java")));

                // Package and class declaration
                file.println("package " + packageGeneration + ";\n");
                file.println("public class " + contentClassName);
                file.print(" extends " + AbstractUPnPDevice.class.getName());

                file.print(" implements ");
                for (int i = 0; i < componentServicesJavaItf.size(); i++)
                {
                    if (i != 0)
                    {
                        file.print(", ");
                    }
                    file.print(componentServicesJavaItf.get(i).getName());
                }
                file.println();
                file.println("{");

                for (Class<?> deviceItf : componentServicesJavaItf)
                {
                    for (Method method : deviceItf.getMethods())
                    {
                        String methodDeclaration = "\tpublic " + method.getReturnType().getSimpleName() + " " + method.getName() + "(";
                        // logger.info("Methode annotee : "+method.getName()+", "+method.getAnnotation(UPnPAction.class));
                        Class<?>[] parametersType = method.getParameterTypes();
                        int nbArgTotal = parametersType.length;

                        for (int i = 0; i < nbArgTotal; i++)
                        {
                            if (i != 0)
                            {
                                methodDeclaration += ", ";
                            }
                            // logger.info("Argument annote : "+(annotation[0]).toString());
                            methodDeclaration += parametersType[i].getSimpleName() + " arg" + i;
                        }
                        methodDeclaration += ")";
                        file.println(methodDeclaration);

                        file.println("\t{");
                        file.println("\t\t" + Map.class.getName() + "<" + String.class.getName() + ", " + Object.class.getName() + "> args = new "
                                + HashMap.class.getName() + "<" + String.class.getName() + ", " + Object.class.getName() + ">();");

                        Annotation[][] annotations = method.getParameterAnnotations();
                        for (int i = 0; i < nbArgTotal; i++)
                        {
                            file.println("\t\targs.put(\"" + ((UPnPVariable) annotations[i][0]).value() + "\", arg" + i + ");");
                        }

                        file.println("\t\tdoAction(\"" + method.getAnnotation(UPnPAction.class).value() + "\", args);");
                        file.println("\t}");
                    }
                }
                file.println("}");
                file.flush();
                file.close();
            } catch (FileNotFoundException fnfe)
            {
                severe(new ProcessorException(upnpImplementation, fnfe));
            }
        }
        // Generate a FraSCAti SCA primitive component.
        generateScaPrimitiveComponent(upnpImplementation, processingContext, contentFullClassName);

        // Store the content class name to retrieve it from next doInstantiate
        // method.
        processingContext.putData(upnpImplementation, String.class, contentFullClassName);

    }

    @Override
    protected final void doInstantiate(UPnPImplementation upnpImplementation, ProcessingContext processingContext) throws ProcessorException
    {
        // Instantiate a FraSCAti SCA primitive component.
        org.objectweb.fractal.api.Component component = instantiateScaPrimitiveComponent(upnpImplementation, processingContext,
                processingContext.getData(upnpImplementation, String.class));

        // Retrieve the SCA property controller of this Fractal component.
        SCAPropertyController propertyController = (SCAPropertyController) getFractalInterface(component, SCAPropertyController.NAME);
        
        // Set the upnpDeviceName property.
        propertyController.setValue("upnpDeviceName", upnpImplementation.getUpnpDeviceName());

    }

    private boolean isValidUPnPItf(Class<?> deviceItf)
    {
        if (!deviceItf.isAnnotationPresent(UPnPService.class))
        {
            return false;
        }
        for (Method method : deviceItf.getMethods())
        {
            if (!method.isAnnotationPresent(UPnPAction.class))
            {
                return false;
            }
            Annotation[][] annotations = method.getParameterAnnotations();
            for (Annotation[] annotation : annotations)
            {
                if (annotation.length != 1)
                {
                    return false;
                }
                if ((annotation[0]).annotationType() != UPnPVariable.class)
                {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * @see org.ow2.frascati.assembly.factory.api.Processor#getProcessorID()
     */
    public String getProcessorID()
    {
        return getID(UpnpPackage.Literals.UPN_PIMPLEMENTATION);
    }

}
