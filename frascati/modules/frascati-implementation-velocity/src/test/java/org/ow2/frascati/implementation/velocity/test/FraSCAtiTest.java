/**
 * OW2 FraSCAti: SCA Implementation Velocity
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.velocity.test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest
{
    FraSCAti frascati;
    CompositeManager compositeManager;

    @Before
    public void initFraSCAti() throws Exception
    {
      this.frascati = FraSCAti.newFraSCAti();
      this.compositeManager = frascati.getCompositeManager();
    }

    /**
     * Check errors produced during the checking phase.
     * Check warnings produced during the checking phase about SCA features not supported by FraSCAti.
     */
    @Test
    public void processCheckingErrorsWarningsComposite() throws Exception
    {
      ProcessingContext processingContext = this.frascati.newProcessingContext();
      try {
        this.compositeManager.processComposite(new QName("CheckingErrorsWarnings"), processingContext);
      } catch(ManagerException me) {
        // Let's note that the following number of errors is conform to comments in file 'CheckingErrorsWarnings.composite'.
        assertEquals("Wrong number of checking errors", 5, processingContext.getErrors());
        // Let's note that the following number of warnings is conform to comments in file 'CheckingErrorsWarnings.composite'.
        assertEquals("Wrong number of checking warnings", 2, processingContext.getWarnings());
      }
    }

    /**
     * Test an example.
     */
    @Test
    public void processExample1Composite() throws Exception
    {
      // Get the example1 composite.
      this.frascati.getComposite("example1");

      Thread.sleep(1000);

      // Try to access an implementation velocity.
      new URL("http://localhost:8090/pages/").openConnection();

      new URL("http://localhost:8090/pages/index.html").openConnection();
    }
    
    /**
     * Default proxy method test
     * @throws Exception 
     */
    @Test
    public void processDefaultProxyMethodComposite() throws Exception
    {
      // Get the special proxy method example
      this.frascati.getComposite("defaultProxyMethodExample");
        
      Thread.sleep(1000);  
      URL testUrl = new URL("http://localhost:18000/defaultTest/");

      InputStream in = testUrl.openStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      
      assertEquals("Hello world, Default template called !", reader.readLine());
      
    }
    
    /**
     * Special proxy method test
     * @throws Exception 
     */
    @Test
    public void processSpecialProxyMethodComposite() throws Exception
    {
      // Get the special proxy method example
      this.frascati.getComposite("specialProxyMethodExample");
        
      Thread.sleep(1000);  
      URL testUrl = new URL("http://localhost:18000/test/");

      InputStream in = testUrl.openStream();
      BufferedReader reader = new BufferedReader(new InputStreamReader(in));
      
      assertEquals("Special template called !", reader.readLine());
      
    }
    
    /**
     * Request encoding test
     * @throws Exception 
     */
    @Test
    public void processEncodingTestComposite() throws Exception
    {
      // Get the special proxy method example
      this.frascati.getComposite("encodingTest");
        
      Thread.sleep(1000);  
      URL url = new URL("http://localhost:18000/encodingTest/");
      
      String name = "test";
      String value = "aéàè";
      Map<String, String> parameters = new HashMap<String,String>();
      parameters.put(name, value);
      HttpURLConnection conn = TestUtils.sendPost(url, parameters, true);

      String res = TestUtils.readInputStream(conn.getInputStream());
      //System.out.println(res);
      
      assertTrue("POST result should contain well encoded accents", res.contains(name + ": " + value));
    }
    
}
