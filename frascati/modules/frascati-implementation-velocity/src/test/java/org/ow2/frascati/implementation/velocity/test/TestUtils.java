/**
 * OW2 FraSCAti: SCA Implementation Velocity
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.velocity.test;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Useful test utils for :
 * * IO (could be replaced by Apache Commons')
 * * HTTP (could be replaced by more complex Apache HTTP Client,
 * or FraSCAti JAXRS but this would break unitarian nature of tests)
 *
 * @author Marc Dutoo
 */
public class TestUtils
{
    
    
    /**
     * 
     * @param url
     * @param rawData ex. name=value
     * @param checkOkStatus if true, does a Junit fail() if not HTTP 200 OK status
     * @return
     * @throws IOException
     */
    public static final HttpURLConnection sendPost(URL url, Map<String, String> parameters,
            boolean checkOkStatus) throws IOException {
        
        // inspired by :
        // http://stackoverflow.com/questions/3324717/sending-http-post-request-in-java
        // http://stackoverflow.com/questions/10116961/can-you-explain-the-httpurlconnection-connection-process
        
        String type = "application/x-www-form-urlencoded";
        
        StringBuffer paramBuf = new StringBuffer();
        for (String name : parameters.keySet()) {
            if (paramBuf.length() != 0) {
                paramBuf.append('&');
            }
            String value = parameters.get(name);
            paramBuf.append(URLEncoder.encode(name, "UTF-8"));
            paramBuf.append('=');
            paramBuf.append(URLEncoder.encode(value, "UTF-8"));
        }
        String encodedData = paramBuf.toString();
        
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", type);
        conn.setRequestProperty("Content-Length", String.valueOf(encodedData.length()));
        OutputStream os = conn.getOutputStream();
        os.write( encodedData.getBytes());
        os.flush();

        if (checkOkStatus && conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
            fail("POST request should return OK status");
        }
        
        return conn;
    }

    public static final String readInputStream(InputStream in) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int nbRead;
        while ((nbRead = in.read(buf, 0, buf.length)) != -1) {
            bos.write(buf, 0, nbRead);
        }
        in.close();
        bos.close();
        return bos.toString();
    }
    
}
