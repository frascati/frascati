/**
 * OW2 FraSCAti: SCA Implementation Velocity
 * Copyright (C) 2011-2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor: Romain Rouvoy
 *
 */
package org.ow2.frascati.implementation.velocity;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.annotation.ElementType;
import java.util.List;
import javax.servlet.Servlet;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.ScaFactory;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractComponentFactoryBasedImplementationProcessor;
import org.ow2.frascati.metamodel.web.VelocityImplementation;
import org.ow2.frascati.metamodel.web.WebPackage;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * OW2 FraSCAti Assembly Factory implementation Velocity processor class.
 *
 * @author Philippe Merle - Inria
 * @version 1.5
 */
public class ImplementationVelocityProcessor
     extends AbstractComponentFactoryBasedImplementationProcessor<VelocityImplementation>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Package where content classes are generated.
   */
  @Property(name = "package")
  private String packageGeneration;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.processor.AbstractProcessor#toStringBuilder(EObjectType, StringBuilder)
   */
  @Override
  protected final void toStringBuilder(VelocityImplementation velocityImplementation, StringBuilder sb)
  {
    sb.append("web:implementation.velocity");
    append(sb, "location", velocityImplementation.getLocation());
    append(sb, "default", velocityImplementation.getDefault());
    super.toStringBuilder(velocityImplementation, sb);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(VelocityImplementation velocityImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    String location = velocityImplementation.getLocation();
    checkAttributeMustBeSet(velocityImplementation, "location", location, processingContext);

    boolean checkDefaultInClassLoader = true;

    // Check that location is present in the class path.
    if(!isNullOrEmpty(location) && processingContext.getClassLoader().getResourceAsStream(location + '/') == null && !locationExists(location)) {
      // Location not found.
      error(processingContext, velocityImplementation, "Location '" + location + "' not found");
      checkDefaultInClassLoader = false;
    }

    String default_ = velocityImplementation.getDefault();
    // default template is not mandatory
    if(!isNullOrEmpty(default_)){
        checkAttributeMustBeSet(velocityImplementation, "default", default_, processingContext);
    }

    // Check that default is present in the class path.
    if(checkDefaultInClassLoader && !isNullOrEmpty(default_) && processingContext.getClassLoader().getResourceAsStream(location + '/' + default_) == null && !locationExists(location + '/' + default_)) {
      // Default not found.
      error(processingContext, velocityImplementation, "Default '" + default_ + "' not found");
    }

    // Get the enclosing SCA component.
    Component component = getParent(velocityImplementation, Component.class);
    List<ComponentService> services = component.getService();
    if(services.size() > 1) {
      error(processingContext, velocityImplementation, "<implementation.velocity> cannot have more than one SCA service");
    } else {
      ComponentService service = null;
      if(services.size() == 0) {
        // The component has zero service then add a service to the component.
        service = ScaFactory.eINSTANCE.createComponentService();
        service.setName("Velocity");
        services.add(service);
      } else { // The component has one service.
        service = services.get(0);
      }
      // Get the service interface.
      Interface itf = service.getInterface();
      if(itf == null) {
        // The component service has no interface then add a Java Servlet interface.
        JavaInterface javaInterface = ScaFactory.eINSTANCE.createJavaInterface();
        javaInterface.setInterface(Servlet.class.getName());
        service.setInterface(javaInterface);
        itf = javaInterface;
      }
      
      // Stores if the component provides the Servlet interface or not
      processingContext.putData(velocityImplementation, String.class, ((JavaInterface)itf).getInterface());
      processingContext.putData(velocityImplementation, Boolean.class, (itf instanceof JavaInterface)
              ? Servlet.class.getName().equals(((JavaInterface)itf).getInterface())
              : false);
    }

    // check attributes 'policySets' and 'requires'.
    checkImplementation(velocityImplementation, processingContext);
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#generate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doGenerate(VelocityImplementation velocityImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
	// Get the parent component.
	Component component = getParent(velocityImplementation, Component.class);

	boolean isServletItf = processingContext.getData(velocityImplementation, Boolean.class);

    // Name of the content class.
    String contentFullClassName = null;

    if(isServletItf && component.getProperty().size() == 0 && component.getReference().size() == 0) {
      // if no property and no reference then use default ImplementationVelocity class.
      contentFullClassName = ServletImplementationVelocity.class.getName();
    } else {
      String basename = isServletItf ? velocityImplementation.getLocation() : velocityImplementation.getLocation()+velocityImplementation.getDefault();
      int hashCode =basename.hashCode();
      if(hashCode < 0) hashCode = -hashCode;
      
      // if some property or some reference then generate an ImplementationVelocity class.
      String contentClassName = "ImplementationVelocity" + hashCode;
      // Add the package to the content class name.
      contentFullClassName = this.packageGeneration + '.' + contentClassName;

      try {
        processingContext.loadClass(contentFullClassName);
      } catch(ClassNotFoundException cnfe) {
        // If the class can not be found then generate it.
        log.info(contentClassName);

        // Create the output directory for generation.
        String outputDirectory = processingContext.getOutputDirectory() + "/generated-frascati-sources";

        // Add the output directory to the Java compilation process.
        processingContext.addJavaSourceDirectoryToCompile(outputDirectory);

        try { // Generate a sub class of ImplementationVelocity declaring SCA references and properties.
            if (isServletItf) {
                ServletImplementationVelocity.generateContent(component, processingContext, outputDirectory, packageGeneration, contentClassName);
            } else {
                Class<?> itf = processingContext.loadClass(processingContext.getData(velocityImplementation, String.class)); 
                ProxyImplementationVelocity.generateContent(component, itf, processingContext, outputDirectory, packageGeneration, contentClassName);
            }
        } catch(FileNotFoundException fnfe) {
            severe(new ProcessorException(velocityImplementation, fnfe));
        } catch (ClassNotFoundException ex) {
            severe(new ProcessorException(velocityImplementation, ex));
        }
      }
    }

    // Generate a FraSCAti SCA primitive component.
    generateScaPrimitiveComponent(velocityImplementation, processingContext, contentFullClassName);

    // Store the content class name to retrieve it from next doInstantiate method.
    processingContext.putData(velocityImplementation, String.class, contentFullClassName);  
  }

  /**
   * @see org.ow2.frascati.assembly.factory.processor.api.Processor#instantiate(ElementType, ProcessingContext)
   */
  @Override
  protected final void doInstantiate(VelocityImplementation velocityImplementation, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Instantiate a FraSCAti SCA primitive component.
    org.objectweb.fractal.api.Component component =
        instantiateScaPrimitiveComponent(velocityImplementation, processingContext,
        		                         processingContext.getData(velocityImplementation, String.class));

    // Retrieve the SCA property controller of this Fractal component.
    SCAPropertyController propertyController =  (SCAPropertyController)getFractalInterface(component, SCAPropertyController.NAME);

    // Set the classloader property.
    propertyController.setValue("classloader", processingContext.getClassLoader());

    // Set the location property.
    propertyController.setValue("location", velocityImplementation.getLocation());

    // Set the default property.
    propertyController.setValue("default", velocityImplementation.getDefault());
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.processor.Processor#getProcessorID()
   */
  public final String getProcessorID()
  {
    return getID(WebPackage.Literals.VELOCITY_IMPLEMENTATION);
  }

  /**
   * Check if the location is accessible in the file system
   */
  private boolean locationExists(String location)
  {
    try{
      File locationFile = new File(location);
      if(locationFile.exists()){
        return true;
      }
    }
    catch(Exception ex){}
    return false;
  }

}
