/**
 * OW2 FraSCAti: SCA Implementation Velocity
 * Copyright (C) 2011 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor: Romain Rouvoy
 *
 */
package org.ow2.frascati.implementation.velocity;

import javax.servlet.http.HttpServlet;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.annotation.Context;
import org.oasisopen.sca.annotation.Init;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.types.Step;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * OW2 FraSCAti abstract implementation template component class.
 * 
 * @author Philippe Merle - Inria
 * @version 1.5
 */
@Scope("COMPOSITE")
public abstract class ImplementationVelocity extends HttpServlet {
    /**
     * ClassLoader where resources are searched.
     */
    @Property(name = "classloader")
    protected ClassLoader classLoader;
    /**
     * Location where templates are searched.
     */
    @Property(name = "location")
    protected String location;
    /**
     * Default resource.
     */
    @Property(name = "default")
    protected String defaultResource;
    /**
     * Context of the SCA component.
     */
    @Context
    private ComponentContext componentContext;
    /**
     * Reference to the owning Fractal component.
     */
    @Controller(name = "component")
    private Component fractalComponent;
    /**
     * Velocity engine.
     */
    protected VelocityEngine velocityEngine;
    /**
     * Velocity context.
     */
    protected VelocityContext velocityContext;

    /**
     * Callback called by OW2 FraSCAti Tinfi each time the component content is instantiated.
     */
    @Init
    public void scaInit() throws Exception {
        // Create a new Velocity engine.
        velocityEngine = new VelocityEngine();

        // Configure the engine:
        // If location is accessible with a classLoader,
        if(this.getClass().getClassLoader().getResourceAsStream(this.location + "/") != null){
            // Use ClassLoaderResourceLoader.
            velocityEngine.setProperty("resource.loader", "class");
            velocityEngine.setProperty("class.resource.loader.description", "OW2 FraSCAti SCA Implementation Velocity ClassLoader Resource Loader");
            velocityEngine.setProperty("class.resource.loader.class", ClassLoaderResourceLoader.class.getName());
            velocityEngine.setProperty("class.resource.loader.classloader", this.classLoader);
            velocityEngine.setProperty("class.resource.loader.location", this.location);
        }
        else {
            // Use Velocity FileResourceLoader.
            velocityEngine.setProperty("resource.loader", "file");
            velocityEngine.setProperty("file.resource.loader.description", "OW2 FraSCAti SCA Implementation Velocity File Resource Loader");
            velocityEngine.setProperty("file.resource.loader.path", this.location);
            velocityEngine.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");
            velocityEngine.setProperty("file.resource.loader.cache", "false");            
        }
        
        // Initialize the Velocity engine.
        velocityEngine.init();
        velocityContext = new VelocityContext();
    
    	// Get the Fractal component type.
        ComponentType componentType = (ComponentType)fractalComponent.getFcType();
        // Iterate over all interface types.
        for(InterfaceType interfaceType : componentType.getFcInterfaceTypes()) {
          // Is a reference ?
          if(interfaceType.isFcClientItf()) {
            // Get the reference name.
        	String referenceName = interfaceType.getFcItfName();
        	// Get the reference instance.
        	Object referenceInstance = null;
        	if(interfaceType.isFcCollectionItf()) {
              referenceInstance = componentContext.getServices(Object.class, referenceName);
            } else {
              referenceInstance = componentContext.getService(Object.class, referenceName);
            }
        	// Put the reference as a variable into the Velocity context.
            velocityContext.put(referenceName, referenceInstance);    	 
          }
        }
      }

    /**
     * Callback called by OW2 FraSCAti Tinfi each time the component is started.
     */
    @Lifecycle(step = Step.START)
    public void registerScaProperties() {
        try {
    	  SCAPropertyController propertyController = (SCAPropertyController)fractalComponent.getFcInterface(SCAPropertyController.NAME);
          for(String propertyName : propertyController.getPropertyNames()) {
            velocityContext.put(propertyName, propertyController.getValue(propertyName));
    	  }
    	} catch(Exception e) {
          e.printStackTrace(System.err);
        }
      }
}