/**
 * OW2 FraSCAti: SCA Implementation Velocity
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.implementation.velocity;

import java.io.InputStream;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.Resource;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;

/**
 * Velocity ResourceLoader for OW2 FraSCAti.
 *
 * @author Philippe Merle - INRIA
 * @version 1.5
 */
public class ClassLoaderResourceLoader extends ResourceLoader
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Class loader where resources are get.
   */
  private ClassLoader classLoader;

  /**
   * Location into the class loader where resources are get.
   */
  private String location;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see ResourceLoader#init(ExtendedProperties)
   */
  public void init(ExtendedProperties configuration)
  {
    rsvc.info("ClasspathResourceLoader: initialization starting.");
    this.classLoader = (ClassLoader)configuration.get("classloader");
    this.location = (String)configuration.get("location");
	rsvc.info("ClasspathResourceLoader: initialization complete.");
  }

  /**
   * @see ResourceLoader#getResourceStream(String)
   */
  public InputStream getResourceStream(String name)
      throws ResourceNotFoundException
  {
    if (name == null || name.length() == 0) {
      throw new ResourceNotFoundException ("No template name provided");
    }

    try {
      return classLoader.getResourceAsStream(this.location + '/' + name);
    } catch(Exception fnfe)  {
      // convert to a general Velocity ResourceNotFoundException.
      throw new ResourceNotFoundException( fnfe.getMessage() );
    }
  }

  /**
   * Defaults to return false.
   */
  public boolean isSourceModified(Resource resource)
  {
    return false;
  }

  /**
   * Defaults to return 0
   */
  public long getLastModified(Resource resource)
  {
    return 0;
  }

}
