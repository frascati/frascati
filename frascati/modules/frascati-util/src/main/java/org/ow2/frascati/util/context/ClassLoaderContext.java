/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util.context;

import java.net.URL;

/**
 * OW2 FraSCAti class loader context interface.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public interface ClassLoaderContext extends ContextualProperties
{
    /**
     * Get the class loader.
     *
     * @return the class loader.
     */
    ClassLoader getClassLoader();

    /**
     * Load a Java class.
     *
     * @param className the Java class name to load.
     * @return the loaded Java class.
     * @throw ClassNotFoundException when the Java class is not found.
     */
    <T> Class<T> loadClass(String className) throws ClassNotFoundException;

    /**
     * Get a resource.
     *
     * @param name the name of the resource to get.
     * @return the URL of the resource, or null if not found.
     */
    URL getResource(String name);
}
