/**
 * OW2 FraSCAti Util
 * Copyright (C) 2011-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * OW2 FraSCAti stream util class.
 *
 * @author Philippe Merle at Inria
 * @version 1.5
 */
public class Stream
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Private default constructor to avoid to instantiate this class.
   */
  private Stream()
  {
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Size of the copy stream buffer.
   */
  public static int BUFFER_SIZE = 1024*64;

  /**
   * Copy an input stream to an output stream.
   */
  public static void copy(InputStream is, OutputStream os) throws IOException
  {
    byte[] buffer = new byte[BUFFER_SIZE];
    int len;
    while ((len = is.read(buffer)) >= 0) {
      os.write(buffer, 0, len);
    }
  }
}
