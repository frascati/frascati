/**
 * OW2 FraSCAti Util
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.util.Fractal;

/**
 * OW2 FraSCAti Util abstract loggeable class providing helper methods related to OW2 Fractal API.
 *
 * @author Philippe Merle at Inria
 * @version 1.3
 */
public abstract class AbstractFractalLoggeable<ExceptionType extends FrascatiException>
              extends AbstractLoggeable
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Create a new exception.
   */
  protected abstract ExceptionType newException(String message);

  //---------------------------------------------------------------------------
  // Internal BindingController methods.
  // --------------------------------------------------------------------------

  protected final BindingController getFractalBindingController(Component component)
    throws ExceptionType
  {
    try {
      return Fractal.getBindingController(component);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal binding controller");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  protected final void bindFractalComponent(BindingController bindingController, String interfaceName, Object value)
    throws ExceptionType
  {
    try {
      bindingController.bindFc(interfaceName, value);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not bind the Fractal component");
      exc.initCause(nsie);
      severe(exc);
    } catch(IllegalBindingException ibe) {
      ExceptionType exc = newException("Can not bind the Fractal component");
      exc.initCause(ibe);
      severe(exc);
    } catch(IllegalLifeCycleException ilce) {
      ExceptionType exc = newException("Can not bind the Fractal component");
      exc.initCause(ilce);
      severe(exc);
    }
  }

  protected final void bindFractalComponent(Component component, String interfaceName, Object value)
    throws ExceptionType
  {
    bindFractalComponent(getFractalBindingController(component), interfaceName, value);
  }

  //---------------------------------------------------------------------------
  // Internal Component methods.
  // --------------------------------------------------------------------------

  protected final Object getFractalInterface(Component component, String interfaceName)
    throws ExceptionType
  {
    try {
      return component.getFcInterface(interfaceName);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal interface '" + interfaceName + "'");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  //---------------------------------------------------------------------------
  // Internal ContentController methods.
  // --------------------------------------------------------------------------

  protected final ContentController getFractalContentController(Component component)
    throws ExceptionType
  {
    try {
      return Fractal.getContentController(component);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal content controller");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  protected final Object getFractalInternalInterface(ContentController contentController, String interfaceName)
    throws ExceptionType
  {
    try {
      return contentController.getFcInternalInterface(interfaceName);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal internal interface '" + interfaceName + "'");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  protected final Object getFractalInternalInterface(Component component, String interfaceName)
    throws ExceptionType
  {
    return getFractalInternalInterface(getFractalContentController(component), interfaceName);
  }

  protected final Component[] getFractalSubComponents(ContentController contentController)
    throws ExceptionType
  {
    return contentController.getFcSubComponents();
  }

  protected final Component[] getFractalSubComponents(Component composite)
    throws ExceptionType
  {
    return getFractalSubComponents(getFractalContentController(composite));
  }

  protected final void addFractalSubComponent(ContentController contentController, Component component)
    throws ExceptionType
  {
	try {
      contentController.addFcSubComponent(component);
	} catch(IllegalContentException ice) {
      ExceptionType exc = newException("Can not add a Fractal sub component");
      exc.initCause(ice);
      severe(exc);
	} catch(IllegalLifeCycleException ilce) {
      ExceptionType exc = newException("Can not add a Fractal sub component");
      exc.initCause(ilce);
      severe(exc);
	}
  }

  protected final void addFractalSubComponent(Component composite, Component component)
    throws ExceptionType
  {
    addFractalSubComponent(getFractalContentController(composite), component);
  }

  protected final void removeFractalSubComponent(ContentController contentController, Component component)
    throws ExceptionType
  {
	try {
      contentController.removeFcSubComponent(component);
	} catch(IllegalContentException ice) {
      ExceptionType exc = newException("Can not remove a Fractal sub component");
      exc.initCause(ice);
      severe(exc);
	} catch(IllegalLifeCycleException ilce) {
      ExceptionType exc = newException("Can not remove a Fractal sub component");
      exc.initCause(ilce);
      severe(exc);
	}
  }

  protected final void removeFractalSubComponent(Component composite, Component component)
    throws ExceptionType
  {
    removeFractalSubComponent(getFractalContentController(composite), component);
  }

  //---------------------------------------------------------------------------
  // Internal LifeCycleController methods.
  // --------------------------------------------------------------------------

  protected final LifeCycleController getFractalLifeCycleController(Component component)
    throws ExceptionType
  {
    try {
      return Fractal.getLifeCycleController(component);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal lifecycle controller");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  protected final void startFractalComponent(LifeCycleController lifeCycleController)
    throws ExceptionType
  {
    try {
      lifeCycleController.startFc();
    } catch(IllegalLifeCycleException nsie) {
      ExceptionType exc = newException("Can not start a Fractal component");
      exc.initCause(nsie);
      severe(exc);
    }
  }

  protected final void startFractalComponent(Component component)
    throws ExceptionType
  {
    startFractalComponent(getFractalLifeCycleController(component));
  }

  protected final void stopFractalComponent(LifeCycleController lifeCycleController)
    throws ExceptionType
  {
    try {
      lifeCycleController.stopFc();
    } catch(IllegalLifeCycleException nsie) {
      ExceptionType exc = newException("Can not stop a Fractal component");
      exc.initCause(nsie);
      severe(exc);
    }
  }

  protected final void stopFractalComponent(Component component)
    throws ExceptionType
  {
    stopFractalComponent(getFractalLifeCycleController(component));
  }

  protected final String getFractalComponentState(LifeCycleController lifeCycleController)
    throws ExceptionType
  {
    return lifeCycleController.getFcState();
  }

  protected final String getFractalComponentState(Component component)
    throws ExceptionType
  {
    return getFractalComponentState(getFractalLifeCycleController(component));
  }

  //---------------------------------------------------------------------------
  // Internal NameController methods.
  // --------------------------------------------------------------------------

  protected final NameController getFractalNameController(Component component)
    throws ExceptionType
  {
    try {
      return Fractal.getNameController(component);
    } catch(NoSuchInterfaceException nsie) {
      ExceptionType exc = newException("Can not get the Fractal name controller");
      exc.initCause(nsie);
      severe(exc);
      return null;
    }
  }

  protected final void setFractalComponentName(NameController nameController, String name)
    throws ExceptionType
  {
    nameController.setFcName(name);    
  }

  protected final void setFractalComponentName(Component component, String name)
    throws ExceptionType
  {
    setFractalComponentName(getFractalNameController(component), name);    
  }

  protected final String getFractalComponentName(NameController nameController)
    throws ExceptionType
  {
    return nameController.getFcName();
  }

  protected final String getFractalComponentName(Component component)
    throws ExceptionType
  {
    return getFractalComponentName(getFractalNameController(component));
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
