/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.util.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Reflection functions.
 * 
 * @author Gwenael Cattez
 * @since 1.6
 */
public class ReflectionUtil
{
    /**
     * Get all fields declared in a given class and all its super classes.
     * 
     * @param clazz The given class.
     * @return All fields declared in the given class and all its super classes.
     */
    public static List<Field> getAllFields(Class<?> clazz)
    {
        List<Field> fields = new ArrayList<Field>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        return fields;
    }

    /**
     * Get all annotationClass annotated fields declared in a given class and all its super classes.
     * 
     * @param clazz The given class.
     * @return All fields declared in the given class and all its super classes.
     */
    public static List<Field> getAllFields(Class<?> clazz, Class<? extends Annotation> annotationClass)
    {
        List<Field> annotatedfields = new ArrayList<Field>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            for(Field field : c.getDeclaredFields())
            {
                if(field.getAnnotation(annotationClass)!=null)
                {
                    annotatedfields.add(field);
                }
            }
        }
        return annotatedfields;
    }
    
    /**
     * Get a field declared in a given class or in one of its super classes.
     * 
     * @param clazz The given class.
     * @param fieldName The name of the field.
     * @return The field declared in a given class or in one of its super classes.
     */
    public static Field getField(Class<?> clazz, String fieldName)
    {
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            for (Field f : c.getDeclaredFields())
            {
                if (f.getName().equals(fieldName))
                {
                    return f;
                }
            }
        }
        return null;
    }

    /**
     * Get a field of a given object.
     * 
     * @param object The given object.
     * @param fieldName The name of the field.
     * @return The field declared in the given object class or in one of its super classes.
     */
    public static Field getField(Object object, String fieldName)
    {
        return ReflectionUtil.getField(object.getClass(), fieldName);
    }

    /**
     * Get the value of a field declared in a given class or in one of its super classes.
     * 
     * @param clazz The given class.
     * @param object The given object.
     * @param fieldName The name of the field.
     * @return The field declared in the given class or in one of its super classes.
     */
    public static Object getFieldValue(Class<?> clazz, Object object, String fieldName)
    {
        Field field = ReflectionUtil.getField(clazz, fieldName);
        if (field == null)
        {
            return null;
        }

        field.setAccessible(true);
        try
        {
            return field.get(object);
        } catch (Exception e)
        {
            return null;
        }
    }

    /**
     * Get the value of a field of a given object.
     * 
     * @param object The given object.
     * @param fieldName The name of the field.
     * @return The field of the given object.
     */
    public static Object getFieldValue(Object object, String fieldName)
    {
        return ReflectionUtil.getFieldValue(object.getClass(), object, fieldName);
    }

    /**
     * Set the value of a field own by owner
     * 
     * @param field the given field
     * @param owner the owner of the field
     * @param value the new value to set
     * @return true if setting is done, false otherwise
     */
    public static boolean setFieldValue(Field field, Object owner, Object value)
    {
        try
        {
            field.setAccessible(true);
            field.set(owner, value);
            return true;
        }
        catch (Exception exception)
        {
            return false;
        }
    }

    /**
     * Set the value of a field named fieldName own by owner
     *
     * @param fieldName the name of the field to set
      * @param owner the owner of the field
     * @param value the new value to set
     * @return true if setting is done, false otherwise
     */
    public static boolean setFieldValue(String fieldName, Object owner, Object value)
    {
        Field field = ReflectionUtil.getField(owner, fieldName);
        if (field == null)
        {
            return false;
        }
        return setFieldValue(field, owner, value);
    }
    
    /**
     * Get all fields declared in a given class and all its super classes.
     * 
     * @param clazz The given class.
     * @return All fields declared in the given class and all its super classes.
     */
    public static List<Method> getAllMethods(Class<?> clazz)
    {
        List<Method> methods = new ArrayList<Method>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            methods.addAll(Arrays.asList(c.getDeclaredMethods()));
        }
        return methods;
    }

    /**
     * Get all annotationClass annotated fields declared in a given class and all its super classes.
     * 
     * @param clazz The given class.
     * @return All fields declared in the given class and all its super classes.
     */
    public static List<Method> getAllMethods(Class<?> clazz, Class<? extends Annotation> annotationClass)
    {
        List<Method> annotatedMethods = new ArrayList<Method>();
        for (Class<?> c = clazz; c != null; c = c.getSuperclass())
        {
            for(Method method : c.getDeclaredMethods())
            {
                if(method.getAnnotation(annotationClass)!=null)
                {
                    annotatedMethods.add(method);
                }
            }
        }
        return annotatedMethods;
    }
    
    /**
     * Invoke static method valueOf on clazz to convert value
     * 
     * @param clazz the class 
     * @param value the value to convert
     * @return value is value.class= clazz, clazz.valueOf(value) if method found, null otherwise
     */
    public static Object valueOf(Class<?> clazz, Object value)
    {
        if (clazz == null || value == null)
        {
            return null;
        }

        if (clazz.isInstance(value))
        {
            return value;
        }

        try
        {
            Class<?> consistentClazz=ReflectionUtil.getConsistentClass(clazz);
            Method valueOfMethod = consistentClazz.getDeclaredMethod("valueOf", value.getClass());
            return valueOfMethod.invoke(null, value);
        }
        catch (Exception exception)
        {
            return null;
        }
    }
    
    public static String generateMethodDeclaration(Method method, int modifier)
    {
        StringBuilder methodDeclarationBuilder=new StringBuilder();
        methodDeclarationBuilder.append(Modifier.toString(modifier));
        methodDeclarationBuilder.append(" ");
        methodDeclarationBuilder.append(method.getReturnType().getName());
        methodDeclarationBuilder.append(" ");
        methodDeclarationBuilder.append(method.getName());
        methodDeclarationBuilder.append("(");
        
        Class<?>[] parameterTypes=method.getParameterTypes();
        for(int i=0; i<parameterTypes.length;i++)
        {
            if(i!=0)
            {
                methodDeclarationBuilder.append(", ");
            }
            methodDeclarationBuilder.append(parameterTypes[i].getName()+" arg"+i);
        }
        methodDeclarationBuilder.append(")");
        
        Class<?>[] methodExceptions = method.getExceptionTypes();
        if (methodExceptions.length != 0)
        {
            methodDeclarationBuilder.append(" throws " + methodExceptions[0].getName());
            for (int i = 1; i < methodExceptions.length; i++)
            {
                methodDeclarationBuilder.append(methodExceptions[i].getName());
            }
        }
        
        return methodDeclarationBuilder.toString();
    }

    public static String generateMethodDeclaration(Method method)
    {
        return ReflectionUtil.generateMethodDeclaration(method, Modifier.PUBLIC);
    }

    private static final Map<String, Class<?>> primitiveTypesMap = new HashMap<String, Class<?>>();
    static
    {
        primitiveTypesMap.put("byte", Byte.class );
        primitiveTypesMap.put("short", Short.class );
        primitiveTypesMap.put("int", Integer.class );
        primitiveTypesMap.put("long", Long.class );
        primitiveTypesMap.put("float", Float.class );
        primitiveTypesMap.put("double", Double.class );
        primitiveTypesMap.put("boolean", Boolean.class );
        primitiveTypesMap.put("char", Character.class );
    }

    /**
     * Get the consistent class for a given class
     * ie if the class is a primitive class return its Object class
     * 
     */
    public static Class<?> getConsistentClass(Class<?> clazz)
    {
        if(clazz.isPrimitive())
        {
            return primitiveTypesMap.get(clazz.getName());
        }
        return clazz;
    }
    
    /**
     * Return a List of the parameterTypes of a method
     * If a primitive class is found replace it by its Object class
     * 
     * @param method
     * @return
     */
    public static List<Class<?>> getConsistentParameterTypes(Method method)
    {
        List<Class<?>> parameterTypes=new ArrayList<Class<?>>();
        for(Class<?> parameterType : method.getParameterTypes())
        {
            parameterTypes.add(ReflectionUtil.getConsistentClass(parameterType));
        }
        return parameterTypes;
    }
    
    
}
