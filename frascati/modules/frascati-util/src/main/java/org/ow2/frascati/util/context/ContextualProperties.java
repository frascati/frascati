/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util.context;

/**
 * OW2 FraSCAti contextual properties interface.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public interface ContextualProperties
{
    /**
     * The path separator for contextual properties.
     */
    String PATH_SEPARATOR = "/";

    /**
     * Get the value of a contextual property.
     *
     * @param path The path of the contextual property.
     * @return The value of the contextual property, or null if not found.
     */
    Object getContextualProperty(String path);

    /**
     * Set a contextual property.
     *
     * @param path The path of the contextual property.
     * @param value The value of the contextual property.
     */
    void setContextualProperty(String path, Object value);
}
