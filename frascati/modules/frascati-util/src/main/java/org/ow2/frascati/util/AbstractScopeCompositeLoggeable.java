/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util;

import org.osoa.sca.annotations.Scope;
// or: import org.oasisopen.sca.annotation.Scope;

/**
 * Abstract SCA scope composite loggeable class.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
@Scope("COMPOSITE")
public abstract class AbstractScopeCompositeLoggeable
              extends AbstractLoggeable
{
    // ----------------------------------------------------------------------
    // Internal methods.
    // ----------------------------------------------------------------------

    /**
     * Log a message.
     *
     * @param message a message to log.
     */
    protected void info(String message)
    {
      log.info(getLoggingHeader() + ": " + message);
    }

    /**
     * Get a textual description for logging.
     *
     * @return a textual description for logging.
     */
    protected String getLoggingHeader()
    {
      return this.getClass().getName();
    }
}
