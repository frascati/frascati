/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util.context;

import java.util.HashMap;
import java.util.Map;

import org.ow2.frascati.util.AbstractLoggeable;

/**
 * OW2 FraSCAti contextual properties implementation class.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class ContextualPropertiesImpl
     extends AbstractLoggeable
  implements ContextualProperties
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** The map to store contextual properties. */
  private Map<String, Object> properties;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Find a map for a given path.
   */
  private Map<String, Object> findMap(String fullPath, String[] splitPath)
  {
    Map<String, Object> result = this.properties;
    for(int i=0; i<splitPath.length-1; i++) {
      Object value = result.get(splitPath[i]);
      if(!(value instanceof Map)) {
        throw new IllegalArgumentException(fullPath + " not found");
      }
      result = (Map<String, Object>)value;
    }
    return result;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs contextual properties.
   */
  public ContextualPropertiesImpl()
  {
    this.properties = new HashMap<String, Object>();
    // Inject Java system properties.
    this.properties.put("java", System.getProperties());
    // Inject environment variables.
    this.properties.put("env", System.getenv());
  }

  /**
   * @see ContextualProperties#getContextualProperty(String)
   */
  public Object getContextualProperty(String path)
  {
    String[] splitPath = path.split(PATH_SEPARATOR);
    Map<String, Object> map = findMap(path, splitPath);
    return map.get(splitPath[splitPath.length-1]);
  }

  /**
   * @see ContextualProperties#setContextualProperty(String,Object)
   */
  public void setContextualProperty(String path, Object value)
  {
    String[] splitPath = path.split(PATH_SEPARATOR);
    Map<String, Object> map = findMap(path, splitPath);
    map.put(splitPath[splitPath.length-1], value);
  }
}
