/**
 * OW2 FraSCAti Util
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * OW2 FraSCAti Util abstract loggeable class.
 *
 * @author Philippe Merle at Inria
 * @version 1.3
 */
public abstract class AbstractLoggeable
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Logger for this object.
   */
  protected Logger log = Logger.getLogger(this.getClass().getCanonicalName());

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  protected final void logDo(String message) {
    log.fine(message + "...");
  }

  protected final void logDone(String message) {
    log.fine(message + " done.");
  }

  protected final <T extends FrascatiException> void warning(T exception) throws T {
    log.warning(exception.getMessage());
    throw exception;
  }

  protected final <T extends FrascatiException> void severe(T exception) throws T {
    log.log(Level.SEVERE, exception.getMessage(), exception);
    throw exception;
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
