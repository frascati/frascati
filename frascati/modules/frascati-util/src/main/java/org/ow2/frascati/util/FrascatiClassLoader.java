/**
 * OW2 FraSCAti Util
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.util;

import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLStreamHandlerFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * OW2 FraSCAti class loader is a specialized @link{URLClassLoader}:
 * - having a name to identify the purpose of a FraSCAti class loader. 
 * - providing the method @link{#addUrl(URL)} to add a new URL dynamically after instantiation.
 * - having several parent class loaders, @see{#addParent(ClassLoader)}.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.5
 * @since 0.5
 */
public class FrascatiClassLoader extends URLClassLoader
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Name of this FraSCAti class loader.
   * @since 1.5
   */
  private String name = "<no-name>";

  /**
   * List of other parent class loaders.
   * @since 1.5
   */
  private List<ClassLoader> parentClassLoaders = new ArrayList<ClassLoader>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see ClassLoader#findClass(String)
   * @since 1.5
   */
  @Override
  protected Class<?> findClass(String name)
    throws ClassNotFoundException
  {
	// Delegate to the URLClassLoader class.
	try {
      return super.findClass(name);
	} catch(ClassNotFoundException cnfe) {
      // ignore and pass to other parent class loaders.		
	}
    // Search into parent class loaders.
    for(ClassLoader parentClassLoader : this.parentClassLoaders) {
      try {
        return parentClassLoader.loadClass(name);
      } catch(ClassNotFoundException cnfe) {
        // ignore and pass to the next parent class loader.
      }
    }
    // class not found.
    throw new ClassNotFoundException(name);
  }

  /**
   * @see ClassLoader#findResource(String)
   * @since 1.5
   */
  @Override
  public URL findResource(String name)
  {
    // Delegate to the URLClassLoader class.
    URL url = super.findResource(name);
    if(url != null) {
      return url;
    }
    // If not found then search into parent class loaders.
    for(ClassLoader parentClassLoader : this.parentClassLoaders) {
      url = parentClassLoader.getResource(name);
      if(url != null) {
        return url;
      }
    }
    // Resource not found.
    return null;
  }

  /**
   * @see ClassLoader#findResources(String)
   * @since 1.5
   */
/*
 * TODO: The following code could be useful later.
 *
  @Override
  public Enumeration findResources(String name)
      throws IOException
  {
    Vector urls = new Vector();
    
    Enumeration e = super.findResources(name);
    while(e.hasMoreElements()) {
      URL url = (URL)e.nextElement();
      urls.addElement(url);
    }
    for(ClassLoader parentClassLoader : this.parentClassLoaders) {
      e = parentClassLoader.getResources(name);
      while(e.hasMoreElements()) {
        URL url = (URL)e.nextElement();
        urls.addElement(url);
      }
    }
    return urls.elements();
  }
*/
  /**
   * @see URLClassLoader#getURLs()
   * @since 1.5
   */
/*
 * TODO: The following code could be useful later.
 *
  @Override
  public URL[] getURLs()
  {
    ArrayList<URL> urls = new ArrayList<URL>();
    for(URL url : super.getURLs()) {
      urls.add(url);
    }
    for(ClassLoader parentClassLoader : this.parentClassLoaders) {
      if(parentClassLoader instanceof URLClassLoader) {
        for(URL url : ((URLClassLoader)parentClassLoader).getURLs()) {
          urls.add(url);
        }
      }
      while(parentClassLoader.getParent() != null) {
        parentClassLoader = parentClassLoader.getParent();
        if(parentClassLoader instanceof URLClassLoader) {
          for(URL url : ((URLClassLoader)parentClassLoader).getURLs()) {
            urls.add(url);
          }
        }
      }
    }
    return urls.toArray(new URL[urls.size()]);
  }
*/  

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Get the current thread's context class loader.
   *
   * @return the current thread's context class loader.
   * @since 1.5
   */
  public static ClassLoader getCurrentThreadContextClassLoader()
  {
    return Thread.currentThread().getContextClassLoader();
  }

  /**
   * Set the current thread's context class loader.
   *
   * @param classLoader the class loader to set as the current thread's context class loader.
   * @since 1.5
   */
  public static void setCurrentThreadContextClassLoader(ClassLoader classLoader)
  {
    Thread.currentThread().setContextClassLoader(classLoader);
  }

  /**
   * Get and set the current thread's context class loader.
   *
   * @param classLoader the class loader to set as the current thread's context class loader.
   * @return the previous current thread's context class loader.
   * @since 1.5
   */
  public static ClassLoader getAndSetCurrentThreadContextClassLoader(ClassLoader classLoader)
  {
    ClassLoader previousClassLoader = getCurrentThreadContextClassLoader();
    setCurrentThreadContextClassLoader(classLoader);
    return previousClassLoader;
  }

  /**
   * Print a @link{ClassLoader} instance.
   *
   * @param classLoader the class loader to print.
   * @since 1.5
   */
  public static void print(ClassLoader classLoader)
  {
    print(classLoader, System.out, "");
  }

  /**
   * Print a @link{ClassLoader} instance.
   *
   * @param classLoader the class loader to print.
   * @param printStream the @link{PrintStream} used to print the class loader.
   * @param indentation
   * @since 1.5
   */
  public static void print(ClassLoader classLoader, PrintStream printStream, String indentation)
  {
    printStream.println(indentation + "ClassLoader: " + classLoader);
    if(classLoader instanceof FrascatiClassLoader) {
      FrascatiClassLoader fcl = (FrascatiClassLoader)classLoader;
      printStream.println(indentation + "  - FrascatiClassLoader name: " + fcl.getName());
    }
    if(classLoader instanceof URLClassLoader) {
      printStream.println(indentation + "  - URLClassLoader.getURLs():");
      for(URL url : ((URLClassLoader)classLoader).getURLs()) {
        printStream.println(indentation + "    * " + url);
      }
    }
    ClassLoader parent = classLoader.getParent();
    if(parent == null) {
      printStream.println(indentation + "  - No parent class loader.");
    } else {
      printStream.println(indentation + "  - Parent class loader:");
      print(parent, printStream, indentation + "    ");
    }
    if(classLoader instanceof FrascatiClassLoader) {
      FrascatiClassLoader fcl = (FrascatiClassLoader)classLoader;
      if(fcl.parentClassLoaders.size() == 0) {
        printStream.println(indentation + "  - No other FraSCAti parent class loaders.");
      } else {
        printStream.println(indentation + "  - Other FraSCAti parent class loaders:");
        for(ClassLoader parentClassLoader : fcl.parentClassLoaders) {
          print(parentClassLoader, printStream, indentation + "    ");
        }
      }
    }
  }

  /**
   * Constructs a FraSCAti class loader child of the current thread's context class loader.
   */
  public FrascatiClassLoader()
  {
    this(getCurrentThreadContextClassLoader());
  }

  /**
   * Constructs a FraSCAti class loader child of the current thread's context class loader.
   *
   * @param name the name of this FraSCAti class loader.
   * @since 1.5
   */
  public FrascatiClassLoader(String name)
  {
    this();
    this.name = name;
  }

  /**
   * Constructs a FraSCAti class loader child of a given class loader.
   *
   * @param parent The parent of the FraSCAti class loader to construct.
   */
  public FrascatiClassLoader(ClassLoader parent)
  {
    super(new URL[0], parent);

    // Following is required for the integration with Kevoree.
    addParent(parent);
  }

  /**
   * Constructs a FraSCAti class loader child of the current thread's context class loader.
   *
   * @param urls An array of @link{URL}s of the FraSCAti class loader to construct.
   */
  public FrascatiClassLoader(URL[] urls)
  {
    super(urls, getCurrentThreadContextClassLoader());
  }

  /**
   * Constructs a FraSCAti class loader child of a given class loader.
   *
   * @param urls An array of @link{URL}s of the FraSCAti class loader to construct.
   * @param parent The parent of the FraSCAti class loader to construct.
   */
  public FrascatiClassLoader(URL[] urls, ClassLoader parent)
  {
    super(urls, parent);

    // Following is required for the integration with Kevoree.
    addParent(parent);
  }

  /**
   * Constructs a FraSCAti class loader child of a given class loader.
   *
   * @param urls An array of @link{URL}s of the FraSCAti class loader to construct.
   * @param parent The parent of the FraSCAti class loader to construct.
   * @param factory The @link{URLStreamHandlerFactory} instance associated to the FraSCAti class loader to construct.
   */
  public FrascatiClassLoader(URL[] urls, ClassLoader parent, URLStreamHandlerFactory factory)
  {
    super(urls, parent, factory);

    // Following is required for the integration with Kevoree.
    addParent(parent);
  }

  /**
   * Adds a @link{URL} instance to this FraSCAti class loader.
   *
   * @param url the @link{URL} instance to add to this FraSCAti class loader.
   */
  public final void addUrl(URL url)
  {
    super.addURL(url);
  }

  /**
   * Gets the name of this FraSCAti class loader.
   *
   * @return the name of this FraSCAti class loader.
   * @since 1.5
   */
  public final String getName()
  {
    return this.name;
  }

  /**
   * Sets the name of this FraSCAti class loader.
   *
   * @param name the name of this FraSCAti class loader.
   * @since 1.5
   */
  public final void setName(String name)
  {
    this.name = name;
  }

  /**
   * Adds a parent class loader.
   *
   * @param classLoader the parent class loader to add.
   * @since 1.5
   */
  public final void addParent(ClassLoader classLoader)
  {
    this.parentClassLoaders.add(classLoader);
  }
}
