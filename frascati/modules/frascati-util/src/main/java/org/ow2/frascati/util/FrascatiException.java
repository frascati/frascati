/**
 * OW2 FraSCAti Util
 * Copyright (C) 2010-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.util;

/**
 * OW2 FraSCAti Util exception class.
 *
 * This class is inherited by other OW2 FraSCAti exception classes.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 */
public class FrascatiException extends Exception
{
    //---------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    private static final long serialVersionUID = 9035619800194492330L;

    //---------------------------------------------------------------------------
    // Internal methods.
    // --------------------------------------------------------------------------

    //---------------------------------------------------------------------------
    // Public methods.
    // --------------------------------------------------------------------------

    /**
     * @see Exception#Exception()
     */
    public FrascatiException() {
        super();
    }

    /**
     * @see Exception#Exception(String)
     */
    public FrascatiException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public FrascatiException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public FrascatiException(Throwable cause) {
        super(cause);
    }

}
