/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.util.regex;

import java.util.ArrayList;
import java.util.List;

/**
 * Regex functions.
 *
 * @author Gwenael Cattez
 * @since 1.6
 */
public class RegexUtil
{
    public static List<String> getTaggedValues(String string, String tagValue)
    {
        StringBuilder tagRegexStringBuilder=new StringBuilder();
        tagRegexStringBuilder.append(".*");
        tagRegexStringBuilder.append(RegexUtil.getOpeningTag(tagValue));
        tagRegexStringBuilder.append("(.*)");
        tagRegexStringBuilder.append(RegexUtil.getClosingTag(tagValue));
        tagRegexStringBuilder.append(".*");
        String tagRegex=tagRegexStringBuilder.toString();
        
        List<String> result = new ArrayList<String>();
        String taggedValue;
        while (string.matches(tagRegex))
        {
            taggedValue = string.replaceAll(tagRegex, "$1");
            result.add(taggedValue);
            string = string.substring(0, string.lastIndexOf(RegexUtil.tagValue(tagValue, taggedValue)));
        }
        return result;
    }
    
    public static String setTaggedValues(String string, String tagValue, String taggedValue, String newValue)
    {
        String tag=RegexUtil.tagValue(tagValue, taggedValue);
        return string.replaceAll(tag, newValue);
    }
    
    public static String tagValue(String tagValue, String value)
    {
        StringBuilder taggedValueStringBuilder=new StringBuilder();
        taggedValueStringBuilder.append(RegexUtil.getOpeningTag(tagValue));
        taggedValueStringBuilder.append(value);
        taggedValueStringBuilder.append(RegexUtil.getClosingTag(tagValue));
        return taggedValueStringBuilder.toString();
    }
    
    public static String getOpeningTag(String tagValue)
    {
        return "<"+tagValue+">";
    }
    
    public static String getClosingTag(String tagValue)
    {
        return "</"+tagValue+">";
    }
}
