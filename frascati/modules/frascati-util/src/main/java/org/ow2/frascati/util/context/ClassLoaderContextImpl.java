/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util.context;

import java.net.URL;

import org.ow2.frascati.util.FrascatiClassLoader;

/**
 * OW2 FraSCAti class loader context implementation class.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class ClassLoaderContextImpl
     extends ContextualPropertiesImpl
  implements ClassLoaderContext
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /** The class loader of this context. */
  private ClassLoader classLoader;

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * Constructs with a new default FrascatiClassLoader.
   */
  public ClassLoaderContextImpl()
  {
    this(new FrascatiClassLoader());
  }

  /**
   * Constructs with a class loader.
   *
   * @param classLoader the class loader for this context.
   */
  public ClassLoaderContextImpl(ClassLoader classLoader)
  {
    this.classLoader = classLoader;
  }

  /**
   * @see ClassLoaderContext#getClassLoader()
   */
  public final ClassLoader getClassLoader()
  {
    return this.classLoader;
  }

  /**
   * @see ClassLoaderContext#loadClass(String)
   */
  @SuppressWarnings("unchecked")
  public <T> Class<T> loadClass(String className) throws ClassNotFoundException
  {
    return (Class<T>)this.classLoader.loadClass(className);
  }

  /**
   * @see ClassLoaderContext#getResource(String)
   */
  public final URL getResource(String name)
  {
    return this.classLoader.getResource(name);
  }
}
