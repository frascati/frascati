/**
 * OW2 FraSCAti Util
 *
 * Copyright (c) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.util;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * ClassLoader use to proceed SCA import export, acting like a filter on the
 * resources and packages
 * 
 * @author Gwenael Cattez at Inria
 * @since 1.5
 */
public class ImportExportFrascatiClassLoader extends FrascatiClassLoader
{
    private Logger logger = Logger.getLogger(ImportExportFrascatiClassLoader.class.getName());

    private List<String> validPackages;
    private List<String> validResources;

    public ImportExportFrascatiClassLoader(FrascatiClassLoader parentClassLoader)
    {
        super(parentClassLoader);
        this.validPackages = new ArrayList<String>();
        this.validResources = new ArrayList<String>();
    }

    /**
     * Add package to the valid packages list
     * 
     * Validity of the package has been check during check phase of the
     * corresponding ImportExportProcessor
     * 
     * @param packageName
     * @return
     */
    public boolean addPackage(String packageName)
    {
        this.validPackages.add(packageName);
        logger.info("[ImportExportFrascatiClassLoader] Add package : " + packageName);
        return true;
    }

    /**
     * Add resource to the valid resources list.
     * 
     * Validity of the resource has been check during check phase of the
     * corresponding ImportExportProcessor
     * 
     * @param resourceUri uri of a valid resource
     * @return
     */
    public boolean addResource(String resourceUri)
    {
        this.validResources.add(resourceUri);
        logger.info("[ImportExportFrascatiClassLoader] Add resource : " + resourceUri);
        return true;
    }

    /**
     * Check that class is valid
     * 
     * @param className name of the class to check
     * @return
     */
    public boolean isValidClass(String className)
    {
        if (className == null || "".equals(className) || validPackages.isEmpty())
        {
            return false;
        }

        for (String validPackage : validPackages)
        {
            if (className.startsWith(validPackage))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Check that resource is valid
     * 
     * @param resourcePath the path of the resource to check
     * @return
     */
    public boolean isValidResource(String resourcePath)
    {
        logger.fine("Entering isValidResource "+resourcePath);
        for(String validResource : validResources)
        {
            logger.fine("\t"+validResource);
        }
        
        /**If the resource is a class file we check if the class is valid*/
        if(resourcePath.endsWith(".class"))
        {
            /**convert resourcePAth to className*/
            String className=resourcePath.replace("/", ".");
            return isValidClass(className);
        }
        
        if (!validResources.isEmpty() && validResources.contains(resourcePath))
        {
            /**
             * If the resource is a java file we must check if its package is in
             * the validPackage list
             */
            if (resourcePath.endsWith(".java"))
            {
                String packageStyleResourcePath = resourcePath.replace("/", ".");
                for (String validPackage : validPackages)
                {
                    if (packageStyleResourcePath.contains(validPackage))
                    {
                        logger.severe("[" + this.getClass().getSimpleName() + "] Java resources " + resourcePath+ " is duplicated as Java and resource export");
                        return true;
                    }
                }
                logger.severe("[" + this.getClass().getSimpleName() + "] Java resources " + resourcePath + " must be declared as a Java export");
                return false;
            }

            return true;
        }
        return false;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException
    {
        if (isValidClass(name))
        {
            return super.loadClass(name);
        }
        logger.warning("Class '" + name + "' is not imported/exported!");
        throw new ClassNotFoundException(name);
    }

    @Override
    public URL getResource(String name)
    {
        if (isValidResource(name))
        {
            return super.getResource(name);
        }
        logger.warning("Resource '" + name + "' is not imported/exported!");
        return null;
    }
}
