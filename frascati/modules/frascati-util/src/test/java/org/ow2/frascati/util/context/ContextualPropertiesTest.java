/**
 * OW2 FraSCAti Util
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.util.context;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test of contextual properties.
 *
 * @author Philippe Merle at Inria
 * @version 1.6
 */
public class ContextualPropertiesTest
{
    @Test
    public void displaySystemProperties()
    {
      System.out.println("Java system properties:");
      for(String name : System.getProperties().stringPropertyNames()) {
        System.out.println("- " + name + "=" + System.getProperties().getProperty(name));
      }
    }

    @Test
    public void displaySystemEnv()
    {
      System.out.println("Environment properties:");
      for(String name : System.getenv().keySet()) {
        System.out.println("- " + name + "=" + System.getenv().get(name));
      }
    }

    @Test
    public void test()
    {
      // Create a contextual properties implementation.
      ContextualProperties context = new ContextualPropertiesImpl();

      // Check that the contextual property 'foo' is not present.
      Object foo = context.getContextualProperty("foo");
      Assert.assertNull(foo);

      // Check that IllegalArgumentException is thrown.
      try {
        context.getContextualProperty("foo/bar");
        Assert.fail();
      } catch(IllegalArgumentException iae) {
    	Assert.assertEquals("foo/bar not found", iae.getMessage());
      }

      // Check that Java system properties are present.
      Object java = context.getContextualProperty("java");
      Assert.assertTrue(java instanceof Map);
      
      // Check that the Java system property 'java.runtime.name' is present.
      Object javaRuntimeName = context.getContextualProperty("java/java.runtime.name");
      Assert.assertNotNull(javaRuntimeName);

      // Check that environment variables are present.
      Object env = context.getContextualProperty("env");
      Assert.assertTrue(env instanceof Map);

      // Check that the environment variable 'HOME' is present.
      Object home = context.getContextualProperty("env/HOME");
      Assert.assertNotNull(home);

      // Check that environment variables are unmodifiable.
      try {
        context.setContextualProperty("env/foo", "bar");
        Assert.fail();
      } catch(UnsupportedOperationException uoe) {
      }
      
      // Check that setProperty(name,value) then getProperty(name) equals to value.
      String name = "foo";
      Object value = "bar";
      context.setContextualProperty(name, value);
      Assert.assertEquals(value, context.getContextualProperty(name));

      // Check composite paths.
      Assert.assertNull(context.getContextualProperty("path"));
      context.setContextualProperty("path", new HashMap<String, Object>());
      Assert.assertNotNull(context.getContextualProperty("path"));
      Assert.assertNull(context.getContextualProperty("path/path"));
      context.setContextualProperty("path/path", new HashMap<String, Object>());
      Assert.assertNotNull(context.getContextualProperty("path/path"));
      Assert.assertNull(context.getContextualProperty("path/path/foo"));
      context.setContextualProperty("path/path/foo", value);
      Assert.assertEquals(value, context.getContextualProperty("path/path/foo"));
    }
}
