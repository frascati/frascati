/**
 * OW2 FraSCAti
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Petitprez
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.jmx;

import java.lang.management.ManagementFactory;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.ow2.frascati.FraSCAti;

/**
 * @author Nicolas Petitprez
 * 
 */
public class JMXTest {

	@Before
	public void setup() throws Exception {
		FraSCAti.newFraSCAti();

		mbs = ManagementFactory.getPlatformMBeanServer();
	}

	MBeanServer mbs;

	@Test
	public void testLoadedComponents() throws Exception {
		// initial load of components
		mbs.invoke(new ObjectName(FrascatiJmx.PACKAGE + ":name=FrascatiJmx"),
				"load", null, null);

		// check there a some exposed components
		Set<ObjectName> components = mbs.queryNames(new ObjectName(
				FrascatiJmx.DOMAIN + ":*"), null);
		Assert.assertFalse(components.isEmpty());

		// check state of a component
		ObjectName tested = new ObjectName(FrascatiJmx.DOMAIN
				+ ":name0=org.ow2.frascati.FraSCAti,name1=jmx");
		Assert.assertEquals(LifeCycleController.STARTED,
				mbs.getAttribute(tested, "state"));
		mbs.invoke(tested, "stop", null, null);
		Assert.assertEquals(LifeCycleController.STOPPED,
				mbs.getAttribute(tested, "state"));
		mbs.invoke(tested, "start", null, null);
		Assert.assertEquals(LifeCycleController.STARTED,
				mbs.getAttribute(tested, "state"));
	}
}
