/**
 * OW2 FraSCAti Jmx
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Petitprez
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.jmx;

import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.osoa.sca.annotations.Destroy;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;

/**
 * @author Nicolas Petitprez
 */
@Scope("COMPOSITE")
@EagerInit
public class FrascatiJmx implements FrascatiJmxMBean {

	private static final Logger logger = Logger.getLogger(FrascatiJmx.class
			.getName());

	public static final String PACKAGE = "org.ow2.frascati.jmx";
	public static final String DOMAIN = "SCA domain";

	@Reference(name = "composite-manager")
	protected CompositeManager compositeManager;

	@Property(required = false)
	private Collection<String> skipComponents = Arrays.asList(
	// "org.ow2.frascati.FraSCAti"
			);

	private MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

	public FrascatiJmx() {
	}

	@Init
	public void init() throws MalformedObjectNameException,
			NullPointerException, InstanceAlreadyExistsException,
			MBeanRegistrationException, NotCompliantMBeanException,
			NoSuchInterfaceException, IllegalLifeCycleException,
			ManagerException {
		if (logger.isLoggable(Level.FINE)) {
			logger.log(Level.FINE, "init FrascatiJMX components");
		}
		ObjectName name = new ObjectName(PACKAGE + ":name=FrascatiJmx");
		mbs.registerMBean(this, name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ow2.frascati.jmx.FrascatiJmxMBean#reload()
	 */
	public void load() throws MBeanRegistrationException,
			InstanceNotFoundException, MalformedObjectNameException,
			InstanceAlreadyExistsException, NotCompliantMBeanException {
		clean();
		for (Component comp : compositeManager.getComposites()) {
			try {
				JmxComponent mbean = new JmxComponent(comp,
						new LinkedHashMap<String, String>());
				if (!skipComponents.contains(mbean.getName()))
					mbean.register(mbs);
			} catch (NoSuchInterfaceException e) {
				if (logger.isLoggable(Level.WARNING)) {
					logger.log(Level.WARNING, "error while creating MBean for "
							+ comp, e);
				}
			} catch (IllegalLifeCycleException e) {
				if (logger.isLoggable(Level.WARNING)) {
					logger.log(Level.WARNING, "error while creating MBean for "
							+ comp, e);
				}
			}
		}
	}

	/**
	 * Unregister all Jmx component.
	 * */
	@Destroy
	public void destroy() throws MBeanRegistrationException,
			InstanceNotFoundException, MalformedObjectNameException,
			NullPointerException {
		clean();
		mbs.unregisterMBean(new ObjectName(PACKAGE + ":name=FrascatiJmx"));
	}

	public void clean() throws MBeanRegistrationException,
			InstanceNotFoundException, MalformedObjectNameException {
 		ObjectName name = new ObjectName(DOMAIN + ":name0=*,*");
		Set<ObjectName> names = mbs.queryNames(name, name);
		for (ObjectName objectName : names) {
			mbs.unregisterMBean(objectName);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return compositeManager.toString();
	}

}
