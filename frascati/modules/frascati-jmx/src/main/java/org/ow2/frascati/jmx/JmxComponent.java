/**
 * OW2 FraSCAti Jmx
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Petitprez
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.jmx;

import static org.objectweb.fractal.util.Fractal.getContentController;
import static org.objectweb.fractal.util.Fractal.getNameController;

import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.DynamicMBean;
import javax.management.InstanceAlreadyExistsException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanException;
import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * {@link DynamicMBean} that allow to manipulate fraSCAti Component.
 * 
 * TODO_Nicolas: add fractal binding properties (see explorer binding panel in
 * example twitter-rest)
 * 
 * @author Nicolas Petitprez
 */
public class JmxComponent implements DynamicMBean {
	private static final Logger logger = Logger.getLogger(JmxComponent.class
			.getName());

	private static final String STATE = "state";

	private static final String START = "start";
	private static final String STOP = "stop";

	private static String map2String(Map<String, String> prefix) {
		StringWriter writer = new StringWriter();

		for (Iterator<Entry<String, String>> iterator = prefix.entrySet()
				.iterator(); iterator.hasNext();) {
			Entry<String, String> entry = iterator.next();
			writer.append(entry.getKey()).append("=").append(entry.getValue());
			if (iterator.hasNext())
				writer.append(",");
		}
		return writer.toString();
	}

	private final Component component;
	private final AttributesHelper attributes;
	private final String name;
	private final ObjectName moduleName;
	private final Map<String, String> prefix;

	/**
	 * @param comp
	 * @throws NoSuchInterfaceException
	 * @throws NullPointerException
	 * @throws MalformedObjectNameException
	 */
	public JmxComponent(Component component, Map<String, String> prefix)
			throws NoSuchInterfaceException, MalformedObjectNameException {
		this.component = component;
		this.name = getNameController(component).getFcName();
		if (logger.isLoggable(Level.FINER)) {
			logger.log(Level.FINER, "create component " + name);
		}
		this.prefix = new LinkedHashMap<String, String>(prefix);
		this.prefix.put("name" + prefix.size(), name);
		moduleName = new ObjectName(FrascatiJmx.DOMAIN + ":"
				+ map2String(this.prefix));
		attributes = new AttributesHelper(component);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.management.DynamicMBean#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String attribute)
			throws AttributeNotFoundException, MBeanException,
			ReflectionException {
		if (STATE.equals(attribute)) {
			try {
				return Fractal.getLifeCycleController(component).getFcState();
			} catch (NoSuchInterfaceException e) {
				throw new MBeanException(e);
			}
		}
		try {
			SCAPropertyController propertyController = (SCAPropertyController) component
					.getFcInterface(SCAPropertyController.NAME);
			return propertyController.getValue(attribute);
		} catch (NoSuchInterfaceException e) {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, e.getMessage(), e);
			}
		}
		try {
			return attributes.getAttributeValue(attribute);
		} catch (Exception e) {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, e.getMessage(), e);
			}
		}
		throw new AttributeNotFoundException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.management.DynamicMBean#getAttributes(java.lang.String[])
	 */
	public AttributeList getAttributes(String[] attributes) {
		AttributeList lst = new AttributeList();
		try {
			for (String attribute : attributes) {
				lst.add(new Attribute(attribute, getAttribute(attribute)));
			}
		} catch (Exception e) {
			logger.log(Level.FINE, e.getMessage(), e);
		}

		return lst;
	}

	@SuppressWarnings("unchecked")
	private List<MBeanAttributeInfo> getAttributesInfos() {
		List<MBeanAttributeInfo> attributes = new ArrayList<MBeanAttributeInfo>();

		try {
			SCAPropertyController propertyController = (SCAPropertyController) component
					.getFcInterface(SCAPropertyController.NAME);

			attributes.add(new MBeanAttributeInfo(STATE, "String",
					"state of component", true, false, false));
			for (String propertyName : propertyController.getPropertyNames()) {
				attributes.add(new MBeanAttributeInfo(propertyName,
						propertyController.getType(propertyName).getName(),
						getName() + "#" + propertyName, true, true, false));
			}
		} catch (NoSuchInterfaceException e) {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, e.getMessage(), e);
			}
		}

		try {
			for (String name : this.attributes.getAttributesNames()) {
				org.ow2.frascati.jmx.AttributesHelper.Attribute attribute = this.attributes
						.getAttribute(name);

				attributes.add(new MBeanAttributeInfo(name, "", attribute
						.getReader(), attribute.getWriter()));
			}

		} catch (Exception e) {
			if (logger.isLoggable(Level.FINE)) {
				logger.log(Level.FINE, e.getMessage(), e);
			}
		}

		return attributes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.management.DynamicMBean#getMBeanInfo()
	 */
	public MBeanInfo getMBeanInfo() {
		return new MBeanInfo(getClass().getName(), "Frascati MBean",
				getAttributesInfos().toArray(new MBeanAttributeInfo[0]), null,
				getMethodsInfos().toArray(new MBeanOperationInfo[0]), null);
	}

	private List<MBeanOperationInfo> getMethodsInfos() {
		List<MBeanOperationInfo> operations = new ArrayList<MBeanOperationInfo>();

		operations.add(new MBeanOperationInfo(STOP, "stop component", null,
				"void", MBeanOperationInfo.ACTION));
		operations.add(new MBeanOperationInfo(START, "start component", null,
				"void", MBeanOperationInfo.ACTION));
		return operations;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.management.DynamicMBean#invoke(java.lang.String,
	 * java.lang.Object[], java.lang.String[])
	 */
	@SuppressWarnings("rawtypes")
	public Object invoke(String actionName, Object[] params, String[] signatures)
			throws MBeanException, ReflectionException {
		if (STOP.equals(actionName)) {
			try {
				Fractal.getLifeCycleController(component).stopFc();
				return null;
			} catch (IllegalLifeCycleException e) {
				throw new ReflectionException(e);
			} catch (NoSuchInterfaceException e) {
				throw new ReflectionException(e);
			}
		} else if (START.equals(actionName)) {
			try {
				Fractal.getLifeCycleController(component).startFc();
				return null;
			} catch (IllegalLifeCycleException e) {
				throw new ReflectionException(e);
			} catch (NoSuchInterfaceException e) {
				throw new ReflectionException(e);
			}
		}

		try {
			ContentController contentController = Fractal
					.getContentController(component);
			String[] names = actionName.split("#");

			Interface itf = (Interface) contentController
					.getFcInternalInterface(names[0]);
			Collection<Class> paramsTypes = new ArrayList<Class>();
			for (String signature : signatures) {
				paramsTypes.add(Class.forName(signature));
			}
			Method m = itf.getClass().getMethod(names[1],
					paramsTypes.toArray(new Class[0]));
			Object ret = m.invoke(itf, params);
			return ret;
		} catch (NoSuchInterfaceException e) {
			throw new ReflectionException(e);
		} catch (ClassNotFoundException e) {
			throw new ReflectionException(e);
		} catch (NoSuchMethodException e) {
			throw new ReflectionException(e);
		} catch (IllegalAccessException e) {
			throw new ReflectionException(e);
		} catch (InvocationTargetException e) {
			throw new ReflectionException(e);
		} catch (RuntimeException e) {
			throw new MBeanException(e);
		}
	}

	public ObjectName register(MBeanServer mbs)
			throws NoSuchInterfaceException, IllegalLifeCycleException,
			MalformedObjectNameException, NullPointerException,
			InstanceAlreadyExistsException, MBeanRegistrationException,
			NotCompliantMBeanException {
		if (logger.isLoggable(Level.FINER)) {
			logger.log(Level.FINER, "registering component " + moduleName
					+ " in MBeanServer");
		}
		try {
			for (Component child : getContentController(component)
					.getFcSubComponents()) {
				new JmxComponent(child, prefix).register(mbs);
			}
		} catch (NoSuchInterfaceException e) {
			// current component is not container
		}

		mbs.registerMBean(this, moduleName);
		return moduleName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.management.DynamicMBean#setAttribute(javax.management.Attribute)
	 */
	public void setAttribute(Attribute attribute)
			throws AttributeNotFoundException, InvalidAttributeValueException,
			MBeanException, ReflectionException {
		try {
			SCAPropertyController propertyController = (SCAPropertyController) component
					.getFcInterface(SCAPropertyController.NAME);
			propertyController.setValue(attribute.getName(),
					attribute.getValue());
		} catch (NoSuchInterfaceException e) {
			logger.log(Level.FINE, e.getMessage(), e);
		}

		try {
			attributes.setAttribute(attribute.getName(), attribute.getValue());
		} catch (Exception e) {
			logger.log(Level.FINE, e.getMessage(), e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.management.DynamicMBean#setAttributes(javax.management.AttributeList
	 * )
	 */
	public AttributeList setAttributes(AttributeList attributes) {
		try {
			for (Attribute attribute : attributes.asList()) {
				setAttribute(attribute);
			}
		} catch (Exception e) {
			if (logger.isLoggable(Level.WARNING)) {
				logger.log(Level.WARNING, e.getMessage(), e);
			}
		}
		return attributes;
	}

	@Override
	public String toString() {
		return getName();
	}

}
