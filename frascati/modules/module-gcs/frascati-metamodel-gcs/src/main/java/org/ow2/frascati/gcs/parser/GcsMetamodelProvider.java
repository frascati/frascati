/**
 * OW2 FraSCAti: Group Communication Metamodel Extension
 * Copyright (C) 2011 University Lille 1, LIFL & INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.gcs.parser;

import org.ow2.frascati.gcs.GcsPackage;
import org.ow2.frascati.parser.metamodel.AbstractMetamodelProvider;

/**
 * OW2 FraSCAti group communication metamodel provider component implementation.
 *
 * @author <a href="mailto:romain.rouvoy@inria.fr">Romain Rouvoy</a>
 * @version 1.0
 */
public class GcsMetamodelProvider
     extends AbstractMetamodelProvider<GcsPackage>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.MetamodelProvider#getEPackage()
   */
  public final GcsPackage getEPackage()
  {
    return GcsPackage.eINSTANCE;
  }

}
