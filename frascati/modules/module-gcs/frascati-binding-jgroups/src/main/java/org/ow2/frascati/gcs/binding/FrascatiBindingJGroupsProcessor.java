/**
 * OW2 FraSCAti: SCA Binding JGroups
 * Copyright (C) 2011-2013 University Lille 1, LIFL & Inria
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.gcs.binding;

import static org.objectweb.fractal.util.Fractal.getAttributeController;
import static org.objectweb.fractal.util.Fractal.getNameController;
import static org.objectweb.fractal.util.Fractal.getSuperController;

import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.BaseService;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.type.ComponentType;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.component.factory.api.ComponentFactory;
import org.ow2.frascati.component.factory.api.TypeFactory;
import org.ow2.frascati.gcs.GcsPackage;
import org.ow2.frascati.gcs.JGroupsBinding;

/**
 * Binding processor for the JGroups communication library.
 * 
 * @author <a href="romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.5
 */
public class FrascatiBindingJGroupsProcessor extends
        AbstractBindingProcessor<JGroupsBinding> {
    @Override
    protected final void toStringBuilder(JGroupsBinding binding,
            StringBuilder sb) {
        sb.append("gcs:binding.jgroups");
        super.toStringBuilder(binding, sb);
    }

    @Override
    protected final void doCheck(JGroupsBinding binding, ProcessingContext ctx)
            throws ProcessorException {
        checkBinding(binding, ctx);
    }

    @Override
    protected final void doGenerate(JGroupsBinding binding,
            ProcessingContext ctx) throws ProcessorException {
        logFine(ctx, binding, "nothing to generate");
    }
    
    @Reference(name="type-factory")
    protected TypeFactory tf;
    
    @Reference(name="component-factory")
    protected ComponentFactory cf;

    @Override
    protected final void doInstantiate(JGroupsBinding binding,
            ProcessingContext ctx) throws ProcessorException {
        try {
            String cluster = binding.getCluster();

            Class<?> itf = null;
            if (hasBaseReference(binding)) { // Client side
                itf = getBaseReferenceJavaInterface(binding, ctx);
            }
            if (hasBaseService(binding)) { // Server side
                itf = getBaseServiceJavaInterface(binding, ctx);
            }

            if (cluster == null || cluster.equals(""))
                cluster = itf.getName();

            // TODO: Share connector instances for a given cluster
            @SuppressWarnings({ "rawtypes", "unchecked" })
            JGroupsRpcConnector content = new JGroupsRpcConnector(itf);
            ComponentType connectorType = content.getComponentType(tf);
            Component connector = cf.createComponent(ctx, connectorType, "primitive", content);
            Component port = getFractalComponent(binding, ctx);

            getNameController(connector).setFcName(
                    getNameController(port).getFcName() + "-jgroups-connector");

            JGroupsRpcAttributes ac = (JGroupsRpcAttributes) getAttributeController(connector);
            ac.setCluster(cluster);
            ac.setProperties(binding.getConfiguration());

            for (Component composite : getSuperController(port)
                    .getFcSuperComponents()) {
                addFractalSubComponent(composite, connector);
            }

            if (hasBaseService(binding)) { // Server side
                BaseService service = getBaseService(binding);
                bindFractalComponent(connector, "servant",
                        port.getFcInterface(service.getName()));
            }

            if (hasBaseReference(binding)) { // Client side
                BaseReference reference = getBaseReference(binding);
                bindFractalComponent(port, reference.getName(),content.getProxy(ctx.getClassLoader()));
            }
        } catch (Exception e) {
            throw new ProcessorException(binding, e);
        }
        logFine(ctx, binding, "importing done");
    }

    @Override
    protected final void doComplete(JGroupsBinding binding,
            ProcessingContext ctx) throws ProcessorException {
        logFine(ctx, binding, "nothing to complete");
    }

    public final String getProcessorID() {
        return getID(GcsPackage.Literals.JGROUPS_BINDING);
    }
}
