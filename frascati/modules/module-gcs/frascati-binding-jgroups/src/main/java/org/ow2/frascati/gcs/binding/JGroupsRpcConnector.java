/**
 * OW2 FraSCAti: SCA Binding JGroups
 * Copyright (C) 2011 University Lille 1, LIFL & INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.gcs.binding;

import static java.lang.reflect.Proxy.newProxyInstance;
import static org.jgroups.blocks.Request.GET_NONE;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import org.jgroups.ChannelException;
import org.jgroups.JChannel;
import org.jgroups.MembershipListener;
import org.jgroups.MessageListener;
import org.jgroups.blocks.RequestOptions;
import org.jgroups.blocks.RpcDispatcher;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.ow2.frascati.component.factory.api.FactoryException;
import org.ow2.frascati.component.factory.api.TypeFactory;

/**
 * JGroups RPC connector for the JGroups communication library.
 * 
 * @author <a href="romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.5
 */
public class JGroupsRpcConnector<T> implements InvocationHandler,
        JGroupsRpcAttributes, BindingController, LifeCycleController {
    protected final Class<T> cls;

    public JGroupsRpcConnector(Class<T> c) {
        this.cls = c;
    }

    protected String cluster;

    public String getCluster() {
        return this.cluster;
    }

    public void setCluster(String name) {
        this.cluster = name;
    }

    protected String properties = null;

    public String getProperties() {
        return this.properties;
    }

    public void setProperties(String uri) {
        this.properties = uri;
    }

    protected String identifier = null;

    public String getName() {
        return this.identifier;
    }

    public void setName(String name) {
        this.identifier = name;
    }

    protected RequestOptions requestOption = new RequestOptions(GET_NONE, 5000);

    private static final String[] modes = new String[] { "GET_FIRST",
            "GET_ALL", "GET_MAJORITY", "GET_ABS_MAJORITY", "GET_N", "GET_NONE" };

    public String getMode() {
        return modes[this.requestOption.getMode() - 1];
    }

    public void setMode(String mode) {
        for (int i = 0; i < modes.length; i++)
            if (modes[i].equalsIgnoreCase(mode))
                this.requestOption.setMode(i + 1);
    }

    public long getTimeout() {
        return this.requestOption.getTimeout();
    }

    public void setTimeout(long value) {
        this.requestOption.setTimeout(value);
    }

    public boolean getAnycasting() {
        return this.requestOption.getAnycasting();
    }

    public void setAnycasting(boolean anycast) {
        this.requestOption.setAnycasting(anycast);
    }

    public short getScope() {
        return this.requestOption.getScope();
    }

    public void setScope(short scope) {
        this.requestOption.setScope(scope);
    }

    protected Object servant;

    protected MessageListener messageListener = null;

    protected MembershipListener membershipListener = null;

    protected static final String[] BINDINGS = new String[] { "servant",
            "message-listener", "membership-listener" };

    public String[] listFc() {
        return BINDINGS;
    }

    public Object lookupFc(String clientItfName)
            throws NoSuchInterfaceException {
        if (BINDINGS[0].equals(clientItfName))
            return this.servant;
        if (BINDINGS[1].equals(clientItfName))
            return this.messageListener;
        if (BINDINGS[2].equals(clientItfName))
            return this.membershipListener;
        return null;
    }

    protected Component comp;

    public void bindFc(String clientItfName, Object serverItf)
            throws NoSuchInterfaceException, IllegalBindingException,
            IllegalLifeCycleException {
        if (BINDINGS[0].equals(clientItfName))
            this.servant = serverItf;
        if (BINDINGS[1].equals(clientItfName))
            this.messageListener = (MessageListener) serverItf;
        if (BINDINGS[2].equals(clientItfName))
            this.membershipListener = (MembershipListener) serverItf;
        if ("component".equals(clientItfName))
            this.comp = (Component) serverItf;
    }

    public void unbindFc(String clientItfName) throws NoSuchInterfaceException,
            IllegalBindingException, IllegalLifeCycleException{
        if (BINDINGS[0].equals(clientItfName))
            this.servant = null;
        if (BINDINGS[1].equals(clientItfName))
            this.messageListener = null;
        if (BINDINGS[2].equals(clientItfName))
            this.membershipListener = null;
        if ("component".equals(clientItfName))
            this.comp = null;
    }

    protected JChannel channel;

    protected RpcDispatcher dispatcher;

    public void startFc() throws IllegalLifeCycleException {
        try {
            this.channel = new JChannel(this.properties);
            this.dispatcher = new RpcDispatcher(this.channel,
            this.messageListener, this.membershipListener, this.servant);
            if (this.identifier != null && !this.identifier.isEmpty())
                this.channel.setName(this.identifier);
            this.channel.connect(this.cluster);
            this.identifier = this.channel.getName();
        } catch (ChannelException e) {
            throw new IllegalLifeCycleException(e.getMessage());
        }
    }

    public void stopFc() throws IllegalLifeCycleException {
        this.channel.close();
    }

    public String getFcState() {
        return null;
    }

    public Object invoke(Object proxy, Method method, Object[] args)	
            throws Throwable {
        return this.dispatcher.callRemoteMethods(null, method.getName(), args,
                method.getParameterTypes(), this.requestOption);
    }

    public ComponentType getComponentType(TypeFactory tf)
            throws FactoryException {
        return tf
                .createComponentType(new InterfaceType[] {
                        tf.createInterfaceType("invocation-handler",
                                InvocationHandler.class.getName(), false,
                                false, false),
                        tf.createInterfaceType("attribute-controller",
                                JGroupsRpcAttributes.class.getName(), false,
                                false, false),
                        tf.createInterfaceType(BINDINGS[0], this.cls.getName(),
                                true, true, false),
                        tf.createInterfaceType(BINDINGS[1],
                                MessageListener.class.getName(), true, true,
                                false),
                        tf.createInterfaceType(BINDINGS[2],
                                MembershipListener.class.getName(), true, true,
                                false) });
    }

    @SuppressWarnings("unchecked")
    public T getProxy(ClassLoader cl) throws IllegalArgumentException,
            NoSuchInterfaceException {
        return (T) newProxyInstance(cl, new Class[] { this.cls },
                (InvocationHandler) comp.getFcInterface("invocation-handler"));
    }
}