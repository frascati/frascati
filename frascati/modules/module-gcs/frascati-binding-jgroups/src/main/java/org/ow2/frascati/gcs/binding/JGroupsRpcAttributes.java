/**
 * OW2 FraSCAti: SCA Binding JGroups
 * Copyright (C) 2011 University Lille 1, LIFL & INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.gcs.binding;

import org.objectweb.fractal.api.control.AttributeController;

/**
 * JGroups attributes for the JGroups RPC connector.
 * 
 * @author <a href="romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 * @version 1.5
 */
public interface JGroupsRpcAttributes extends AttributeController {
    /**
     * The name of the cluster to join (or to create if nobody has joined yet).
     * 
     * @return identifier of the cluster.
     */
    String getCluster();

    void setCluster(String name);

    /**
     * The configuration URI for the underlying JGroups communication stack.
     * 
     * @return URI of the JGroups configuration file.
     */
    String getProperties();

    void setProperties(String uri);

    /**
     * The identifier of the node in the cluster
     * 
     * @return name of the node.
     */
    String getName();

    void setName(String name);

    /**
     * The mode of a request.
     * 
     * @return "GET_FIRST", "GET_ALL", "GET_MAJORITY", "GET_ABS_MAJORITY",
     *         "GET_N" (deprecated), or "GET_NONE"
     */
    String getMode();

    void setMode(String mode);

    /**
     * The max time (in ms) for a blocking call. 0 blocks until all responses
     * have been received (if mode = GET_ALL).
     * 
     * @return timeout value (in ms).
     */
    long getTimeout();

    void setTimeout(long value);

    /**
     * Turns on anycasting; this results in multiple unicasts rather than a
     * multicast for group calls.
     * 
     * @return true if using anycasting.
     */
    boolean getAnycasting();

    void setAnycasting(boolean unicast);

    /**
     * The scope of a message, allows for concurrent delivery of messages from
     * the same sender.
     * 
     * @return scope value (default is 0).
     */
    short getScope();

    void setScope(short scope);
}
