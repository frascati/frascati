/**
 * OW2 FraSCAti: SCA Property with JAXB
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.property.jaxb;

import java.lang.annotation.ElementType;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.SCAPropertyBase;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.Processor;
import org.ow2.frascati.assembly.factory.api.ProcessorException;
import org.ow2.frascati.assembly.factory.processor.AbstractPropertyTypeProcessor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.sun.xml.bind.api.impl.NameConverter;

/**
 * OW2 FraSCAti Assembly Factory JAXB-based property type processor.
 *
 * @author <a href="mailto:damien.fournier@inria.fr">Damien Fournier</a>
 * @version 1.3
 * @since 1.1
 */
public class ScaPropertyTypeJaxbProcessor
     extends AbstractPropertyTypeProcessor
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  /**
   * Created JAXB Unmarshallers are stored into a cache implemented by a Map.
   * The first time a unmarshaller is required for a given Java package then it
   * is created and stored into the map. Next time it is required, it is already
   * present into the cache. This should improve performance.
   *
   * Moreover, the Sun Microsystems's JABX implementation seems to be buggy.
   * Creating three times a JAXB context for a given Java package
   * throws an exception.
   *
   * JAXBContext c1 = JAXBContext.newInstance("com.foo");
   * JAXBContext c2 = JAXBContext.newInstance("com.foo");
   * JAXBContext c3 = JAXBContext.newInstance("com.foo"); // here an exception is thrown.
   *
   * This is a bug or a feature. However, the cache resolves this issue ;-)
   */
  private Map<Class<?>, Unmarshaller> cacheOfUnmarshallers =
              new HashMap<Class<?>, Unmarshaller>();

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#check(ElementType, ProcessingContext)
   */
  @Override
  protected final void doCheck(SCAPropertyBase property, ProcessingContext processingContext)
      throws ProcessorException
  {
    // Obtain the property type for the given property.
	QName propertyType = processingContext.getData(property, QName.class);

    // When no propertyType set then return immediatly as this processor has nothing to do.
    if(propertyType == null) {
      return;
    }

    // Retrieve the package name associated to this property type.
    String packageName = NameConverter.standard.toPackageName(propertyType.getNamespaceURI());

    try {
      // try to retrieve the JAXB package info.
      log.fine("Try to load " + packageName + ".package-info.class");
      processingContext.loadClass(packageName + ".package-info");
      log.fine(packageName + ".package-info.class found.");

      // This property type processor is attached to the given property.
      processingContext.putData(property, Processor.class, this);

      // Compute the property value.
	  doComplete(property, processingContext);

     } catch(ClassNotFoundException cnfe) {
       log.fine("No " + packageName + ".package-info.class found.");
       return;
     }
  }

  /**
   * @see org.ow2.frascati.assembly.factory.api.Processor#complete(ElementType, ProcessingContext)
   */
  @Override
    protected final void doComplete(SCAPropertyBase property, ProcessingContext processingContext) throws ProcessorException
    {
        // Obtain the property type for the given property.
        QName propertyType = processingContext.getData(property, QName.class);

        Map<Object, Object> options = new HashMap<Object, Object>();
        Object rootObject;
        // Check if a Resource for this property can be found in the ProcessingContext
        // If found it means that the property has been defined in an other composite file
        Resource propertyResource = processingContext.getData(property, Resource.class);
        if (propertyResource != null)
        {
            // the rootObject is the property defined in the other composite file
            rootObject = processingContext.getData(property, PropertyValue.class);
        } else
        {
            propertyResource = property.eResource();
            rootObject = property;
        }

        options.put(XMLResource.OPTION_ROOT_OBJECTS, Collections.singletonList(rootObject));
        // Save the property resource in a new XML document.
        Document document = ((XMLResource) propertyResource).save(null, options, null);

        // The XML node defining this SCA property value.
        Node node = document.getChildNodes().item(0).getChildNodes().item(0);

        // Search the first XML element.
        while (node != null && node.getNodeType() != Node.ELEMENT_NODE)
        {
            node = node.getNextSibling();
        }

        // No XML element found.
        if (node == null)
        {
            error(processingContext, property, "No XML element");
            return;
        }

        // Retrieve the package name.
        String packageName = NameConverter.standard.toPackageName(propertyType.getNamespaceURI());

        // Retrieve the JAXB package info class.
        Class<?> packageInfoClass;
        try
        {
            packageInfoClass = processingContext.loadClass(packageName + ".package-info");
        } catch (ClassNotFoundException cnfe)
        {
            // Should never happen but we never know.
            severe(new ProcessorException(property, "JAXB package info for " + toString(property) + " not found", cnfe));
            return;
        }

        // Retrieve the JAXB Unmarshaller for the cache.
        Unmarshaller unmarshaller = cacheOfUnmarshallers.get(packageInfoClass);

        // If not found, then create the JAXB unmarshaller.
        if (unmarshaller == null)
        {
            // Create the JAXB context for the Java package of the property type.
            JAXBContext jaxbContext;
            try
            {
                jaxbContext = JAXBContext.newInstance(packageName, processingContext.getClassLoader());
            } catch (JAXBException je)
            {
                severe(new ProcessorException(property, "create JAXB context failed for " + toString(property), je));
                return;
            }

            // Create the JAXB unmarshaller.
            try
            {
                unmarshaller = jaxbContext.createUnmarshaller();

                // Turn on XML validation.
                // Not supported by sun's JAXB implementation.
                // unmarshaller.setValidating(true);

            } catch (JAXBException je)
            {
                severe(new ProcessorException(property, "create JAXB unmarshaller failed for " + toString(property), je));
                return;
            }

            // Store the unmarshaller into the cache.
            cacheOfUnmarshallers.put(packageInfoClass, unmarshaller);

        }

        // Unmarshal the property node with JAXB.
        Object computedPropertyValue;
        try
        {
            computedPropertyValue = unmarshaller.unmarshal((Element) node);
        } catch (JAXBException je)
        {
            error(processingContext, property, "XML unmarshalling error: " + je.getMessage());
            return;
        }

        // Attach the computed property value to the given property.
        processingContext.putData(property, Object.class, computedPropertyValue);
    }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

}
