/**
 * OW2 FraSCAti: SCA Property with JAXB
 * Copyright (C) 2010 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.property.jaxb;

import static org.junit.Assert.*;
import org.junit.Test;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.util.FrascatiException;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest {

    @Test
    public void processMessageProperties() throws Exception {
      // Create a FraSCAti instance.
      FraSCAti frascati = FraSCAti.newFraSCAti();
      ProcessingContext processingContext = frascati.newProcessingContext();
      try {
        frascati.processComposite("MessageProperties", processingContext);
      } catch(FrascatiException fe) {
    	assertEquals("The number of checking errors", 3 /* but should be 5 */, processingContext.getErrors());
        assertEquals("The number of checking warnings", 0, processingContext.getWarnings());
      }
    }

}
