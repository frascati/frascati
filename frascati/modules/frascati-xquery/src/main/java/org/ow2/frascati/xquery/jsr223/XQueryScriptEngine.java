/**
 * OW2 FraSCAti: Implementation XQuery
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.xquery.jsr223;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Proxy;

import javax.script.AbstractScriptEngine;
import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.Invocable;
import javax.script.ScriptContext;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import org.ow2.frascati.xquery.saxon.JaxBObjectModel;


import net.sf.saxon.Configuration;
import net.sf.saxon.query.StaticQueryContext;
import net.sf.saxon.query.XQueryExpression;
import net.sf.saxon.trans.XPathException;





public class XQueryScriptEngine extends AbstractScriptEngine implements Invocable,Compilable{

	
	

	private ScriptEngineFactory factory;

	private XQueryExpression compiledScript;

	private Configuration saxonConfiguration;

	public static Boolean debug=true;
	
	public XQueryScriptEngine(){
		log("create xquery script engine");
	}

	
	
	public Bindings createBindings() {
		log("create simple binding");
		return new SimpleBindings();
	}

	
	/*
	 * @see javax.script.ScriptEngine#eval(java.lang.String, javax.script.ScriptContext)
	 */
	public Object eval(String script, ScriptContext sctx) throws ScriptException {
		try {
			return eval(new FileReader(script),sctx);
		} catch (FileNotFoundException e) {
			throw new ScriptException(e);
		}
	}

	/*
	 * @see javax.script.ScriptEngine#eval(java.io.Reader, javax.script.ScriptContext)
	 * The returned object in an xquery expression compiled
	 */
	public Object eval(Reader reader, ScriptContext ctx) throws ScriptException {
		
		this.log("compile xquery script ");
		
		//Create default configuration and add JaxbObjectModel implementation
		this.saxonConfiguration = new Configuration();
		this.saxonConfiguration.getExternalObjectModels().add(new JaxBObjectModel());
		
		
		//Create default static query context
		final StaticQueryContext context = new StaticQueryContext(saxonConfiguration);
		
		
		
		String script = null;
		try {
			
			//convert the reader in String
			script = this.convertStreamToString(reader);
			
			//compilation script
			this.compiledScript =  context.compileQuery(script);
		
		} catch (XPathException e) {
			
			//in xquery, we can't just define function without eof token
			//we check just this error for compilation
			if (e.getErrorCodeLocalPart().equals("XPST0003"))
			{
				//we add eof token to the script
				script+= " 0";
				try {
					//and run again the compilation
					this.compiledScript = context.compileQuery(script);
				} 
				catch (XPathException e1) {throw new ScriptException(e1);}
			}
			else throw new ScriptException(e);
			
		} 
		catch (IOException e) {throw new ScriptException(e);}
		
		log("script compiled");
		
		return this.compiledScript;
	}

	
	/*
	 * @see javax.script.ScriptEngine#getFactory()
	 */
	public ScriptEngineFactory getFactory() {
		synchronized (this) {
			if (factory == null) {
				factory = new XQueryScriptEngineFactory();
			}
		}
		return factory;
	}

	

	public void setFactory(ScriptEngineFactory factory) {
		this.factory = factory;
	}

	
	/*
	 * @see javax.script.Invocable#getInterface(java.lang.Class)
	 */
	public Object getInterface(Class aclass) {
		
		log("create proxy for interface interface "+aclass.getCanonicalName());
		
		final Proxy proxy = (Proxy) Proxy.newProxyInstance(
				aclass.getClassLoader(),
				new Class[]{aclass},
				new XQueryInvocationHandler(this,aclass.getCanonicalName()));

		return proxy;
	}

	/*
	 * @see javax.script.Invocable#getInterface(java.lang.Object, java.lang.Class)
	 */
	public Object getInterface(Object obj, Class aclass) {
		
		log("create proxy for interface interface "+aclass.getCanonicalName());
		
		final Proxy proxy = (Proxy) Proxy.newProxyInstance(
				aclass.getClassLoader(),
				new Class[]{aclass},
				new XQueryInvocationHandler(this,aclass.getCanonicalName()));

		return proxy;
	}
	

	/*
	 * @see javax.script.Invocable#invokeFunction(java.lang.String, java.lang.Object[])
	 */
	public Object invokeFunction(String func, Object[] arg1) throws ScriptException, NoSuchMethodException {
		return null;
	}

	/*
	 * @see javax.script.Invocable#invokeMethod(java.lang.Object, java.lang.String, java.lang.Object[])
	 */
	public Object invokeMethod(Object object, String method, Object[] args)
	throws ScriptException, NoSuchMethodException {
		return null;
	}

	/*
	 * @see javax.script.Compilable#compile(java.lang.String)
	 */
	public CompiledScript compile(String script) throws ScriptException {
		return null;
	}

	/*
	 * @see javax.script.Compilable#compile(java.io.Reader)
	 */
	public CompiledScript compile(Reader arg0) throws ScriptException {
		return null;
	}

	public XQueryExpression getCompiledScript(){
		return this.compiledScript;
	}

	public Configuration getSaxonConfiguration(){
		return this.saxonConfiguration;
	}

	
	public void log(final String str){
		if (XQueryScriptEngine.debug)
			System.out.println("[xquery-engine info ] "+str);
	}
	
	/*
	 * Convert Reader to String
	 */
	public String convertStreamToString(Reader reader) throws IOException {
		if (reader != null) {
			Writer writer = new StringWriter();
			char[] buffer = new char[1024];
			try {
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				reader.close();
			}
			return writer.toString();
		} else {        
			return "";
		}
	}
}