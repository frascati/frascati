/**
 * OW2 FraSCAti: Implementation XQuery
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.xquery.jsr223;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;

public class XQueryScriptEngineFactory implements ScriptEngineFactory{

	private static List<String> names;
    private static List<String> extensions;
    private static List<String> mimeTypes;
    static {
        names = new ArrayList<String>(2);
        names.add("xquery");
        names.add("xq");
        names = Collections.unmodifiableList(names);
        extensions = new ArrayList<String>(2);
        extensions.add("xquery");
        extensions.add("xq");
        extensions = Collections.unmodifiableList(extensions);
        mimeTypes = new ArrayList<String>(0);
        mimeTypes = Collections.unmodifiableList(mimeTypes);
    }
	
	
	
	public String getEngineName() {
		return "xquery-engine";
	}

	public String getEngineVersion() {
		return "1.0";
	}

	public List<String> getExtensions() {
		return extensions;
	}

	public String getLanguageName() {
		return "xquery";
	}

	public String getLanguageVersion() {
		return "1.0";
	}

	public List<String> getMimeTypes() {
		return XQueryScriptEngineFactory.mimeTypes;
	}

	public List<String> getNames() {
		return XQueryScriptEngineFactory.names;
	}

	public String getOutputStatement(String str) {
		return "\""+str+"\"";
	}

	public Object getParameter(String arg0) {
		return null;
	}

	

	public ScriptEngine getScriptEngine() {
		final XQueryScriptEngine engine = new XQueryScriptEngine();
		engine.setFactory(this);
		return engine;
	}

	
	public String getMethodCallSyntax(String obj, String m, String... args) {
		final StringBuffer buf = new StringBuffer();
		buf.append(obj);
		buf.append(".");
		buf.append(m);
		buf.append("(");
		for (int i = 0; i < args.length; i++) {
	         buf.append(args[i]);
	          if (i == args.length - 1) {
	        	  buf.append(")");
	          } else {
	        	  buf.append(".");
	          }
	      }
	      return buf.toString();
	}

	
	public String getProgram(String... arg0) {
		return null;
	}

}
