/**
 * OW2 FraSCAti: Implementation XQuery
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.xquery.convert;



import net.sf.saxon.Controller;
import net.sf.saxon.expr.PJConverter;
import net.sf.saxon.om.ValueRepresentation;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.type.ItemType;
import net.sf.saxon.value.Value;






public class XQConvertPJ {

	
	  public static Object convertXPathToJava(ValueRepresentation value, Class<?> clazz,Controller controller) 
		throws XPathException{
	
		System.out.println("convert XPathttoJava");
		  
		// if the function return void
		if (clazz.isAssignableFrom(void.class))
			return null;
		  
		final Value v = Value.asValue(value);
		final ItemType itemType = v.getItemType(controller.getConfiguration().getTypeHierarchy());
		
		final PJConverter converter = PJConverter.allocate(controller.getConfiguration(), itemType,v.getCardinality(), clazz);
		
		
		return converter.convert(value, clazz, controller.newXPathContext());
	}
	
	
	
	
}
