/**
 * OW2 FraSCAti: Implementation XQuery
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.xquery.jsr223;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

import net.sf.saxon.Controller;
import net.sf.saxon.instruct.GlobalVariable;
import net.sf.saxon.instruct.UserFunction;
import net.sf.saxon.om.ValueRepresentation;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.value.ObjectValue;

import org.ow2.frascati.xquery.convert.XQConvertJP;
import org.ow2.frascati.xquery.convert.XQConvertPJ;



public class XQueryInvocationHandler implements InvocationHandler{

	private XQueryScriptEngine engine;

	private String namespace ;

	public XQueryInvocationHandler(XQueryScriptEngine engine,String namespace){
		this.engine = engine;
		this.namespace = namespace;
	}

	
	/*
	 * @see java.lang.reflect.InvocationHandler#invoke(java.lang.Object, java.lang.reflect.Method, java.lang.Object[])
	 */
	public Object invoke(Object object, Method method, Object[] args)
	throws Throwable {
		
		//numbers of function arguments
		int arity = 0;
		if (args != null)
			arity= args.length;

		
		//Look invoke function in compiled  script
		final UserFunction caller = engine.getCompiledScript().getStaticContext().getUserDefinedFunction(
				this.namespace,
				method.getName(),arity);
		
		
		if (caller == null){
			final StringBuffer buffer = new StringBuffer();
			buffer.append("[xquery-engine invoke error] :");
			buffer.append(" cannot find function ");
			buffer.append(this.namespace);
			buffer.append(":");
			buffer.append(method.getName());
			buffer.append(" with "+arity+" arguments");
			throw new Throwable(buffer.toString());

		}
		
		
		//Convert Java args to XPath args
		
		final Controller controller = this.engine.getCompiledScript().newController();
		
		final ValueRepresentation[] xpathArgs = new ValueRepresentation[arity];
		
		for (int i=0;i<xpathArgs.length;i++){
			xpathArgs[i] = XQConvertJP.convertObjectToXPath(args[i],controller);
		}
		
		
		//Bind global variable external with references 
		//and properties given by frascati
		try{
			this.bindGlobaleVariable(caller,controller);
		}
		catch(XPathException e){
			final StringBuffer buffer = new StringBuffer();
			buffer.append("[xquery-engine invoke error] :");
			buffer.append("error globale variable binding");
			System.err.println(buffer.toString());
			e.printStackTrace();
			throw new Throwable(e);
		}

		//Call Function
		ValueRepresentation vr = null;
		try{
				vr = caller.call(xpathArgs, controller);
		}
		catch(XPathException ex){
			ex.printStackTrace();
			throw new Throwable(ex);
		}
		
		//Convert final result
		final Object result = XQConvertPJ.convertXPathToJava(vr, method.getReturnType(),
				controller);
		
		
		return result;
	}

	/*
	 * Bind reference and properties with external variable in xquery script
	 */
	private void bindGlobaleVariable(UserFunction caller, Controller controller) throws XPathException{
		
		final Map map = caller.getExecutable().getCompiledGlobalVariables();
		
		if (map != null){
			
			for (Object o : map.keySet()){
				
				final GlobalVariable gv = (GlobalVariable) map.get(o);
				final Object inject = this.engine.get(gv.getVariableQName().getDisplayName());
				
				System.out.println(gv.getVariableQName().getDisplayName());
				
				if (inject != null){
					controller.getBindery().assignGlobalVariable(gv,new ObjectValue(inject));
				}
			
			}
		}
		controller.preEvaluateGlobals(controller.newXPathContext());
	}

}
