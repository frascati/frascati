/**
 * OW2 FraSCAti: Implementation XQuery
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.xquery.saxon;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlType;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;

import net.sf.saxon.Configuration;
import net.sf.saxon.event.PipelineConfiguration;
import net.sf.saxon.event.Receiver;
import net.sf.saxon.expr.JPConverter;
import net.sf.saxon.expr.PJConverter;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.jdom.JDOMObjectModel;
import net.sf.saxon.om.ExternalObjectModel;
import net.sf.saxon.om.NodeInfo;
import net.sf.saxon.om.SequenceIterator;
import net.sf.saxon.om.ValueRepresentation;
import net.sf.saxon.pattern.AnyNodeTest;
import net.sf.saxon.query.QueryResult;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.type.ItemType;
import net.sf.saxon.value.Value;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

public class JaxBObjectModel implements ExternalObjectModel, Serializable{

	private static final long serialVersionUID = -8533548317416451814L;

	
	/**
     * Test whether this object model recognizes a given class as representing a
     * node in that object model. This method will generally be called at compile time.
     *
     * @param nodeClass A class that possibly represents nodes
     * @return true if the class is used to represent nodes in this object model
     */
	public Boolean isRecognizedNode(Class targetClass){
		return targetClass.isAnnotationPresent(XmlType.class);
	}
	

   /**
    * Test whether this object model recognizes a given node as one of its own
    */
	public Boolean isRecognizedNode(Object targetObject){
		return isRecognizedNode(targetObject.getClass());
	}
	
	
	

	
	public PJConverter getPJConverter(Class targetClass) {
		
		if (isRecognizedNode(targetClass)){
			return new PJConverter() {
				
				public Object convert(ValueRepresentation value, Class targetClass,
						XPathContext context) throws XPathException {
					
					return convertXPathValueToObject(Value.asValue(value), targetClass, context);
				
				}
			};
		}
		else 
			return null;
	}

	
	public JPConverter getJPConverter(Class targetClass) {
		
		if (isRecognizedNode(targetClass)){
			
			return new JPConverter(){

				public ValueRepresentation convert(Object object,XPathContext context)
				throws XPathException {
					return convertJaxbToXPath(object, context);
				}
				
				public ItemType getItemType() {
					return AnyNodeTest.getInstance();
				}
			};
		}
		else
			return null;
	}

	
	public  Object convertXPathValueToObject(Value value, Class targetClass, XPathContext context) throws XPathException {
		
		//iterate the result of the value representation
		final SequenceIterator iterator = value.iterate();
		
		//Properties for output xml output format
		final Properties props = new Properties();
		props.setProperty(OutputKeys.METHOD, "xml");
	    props.setProperty(OutputKeys.OMIT_XML_DECLARATION,"no");
	    props.setProperty(OutputKeys.INDENT, "yes");
	    
	    // creation of the xml document in outstream
		final ByteArrayOutputStream outstream = new ByteArrayOutputStream();
		QueryResult.serialize((NodeInfo)value.asItem(), new StreamResult(outstream), props);
		
		final ByteArrayInputStream instream = new ByteArrayInputStream(outstream.toByteArray());
		try {
			
			//rebuilding of the jaxb object
			final JAXBContext jaxbcontext = JAXBContext.newInstance(targetClass);
			final Unmarshaller unmarshall = jaxbcontext.createUnmarshaller();
			return unmarshall.unmarshal(instream);
		
		} catch (JAXBException e) {
			
			throw new XPathException(e);
		
		}
		
		
	}
	
	
	
	public ValueRepresentation convertJaxbToXPath(Object object, XPathContext context) throws XPathException{
		// with the jaxb api, we convert object to xml document
		try{
			
			final JAXBContext jcontext = JAXBContext.newInstance(object.getClass());
			final Marshaller marshall = jcontext.createMarshaller();
			marshall.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);

			//xml document in outstream
			final ByteArrayOutputStream outstream = new ByteArrayOutputStream();
			marshall.marshal(object,outstream);

			//also we create a Jdom object
			final ByteArrayInputStream instream = new ByteArrayInputStream(outstream.toByteArray());
			final SAXBuilder builder = new SAXBuilder();
			Document document = builder.build(instream);

			JDOMObjectModel jdomobject = new JDOMObjectModel();
			return jdomobject.convertObjectToXPathValue(document, context.getConfiguration());


		} 
		catch (JDOMException e) {
			throw new XPathException(e);
		}
		catch (IOException e) {
			throw new XPathException(e);
		} 
		catch (JAXBException e) {
			throw new XPathException(e);
		}
	}

	
	
	

	
	public PJConverter getNodeListCreator(Object node) {
		return null;
	}

	
	public Receiver getDocumentBuilder(Result result) throws XPathException {
		return null;
	}

	
	public boolean sendSource(Source source, Receiver receiver,
			PipelineConfiguration pipe) throws XPathException {
		return false;
	}

	
	public NodeInfo unravel(Source source, Configuration config) {
		return null;
	}
	
	public String getIdentifyingURI() {
		return null;
	}

}
