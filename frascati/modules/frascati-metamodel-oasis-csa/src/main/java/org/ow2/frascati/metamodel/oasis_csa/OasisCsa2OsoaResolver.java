/**
 * OW2 FraSCAti: OASIS CSA Metamodel
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.metamodel.oasis_csa;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.FeatureMap;

import org.eclipse.stp.sca.ScaFactory;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Reference;

import org.ow2.frascati.parser.api.Resolver;
import org.ow2.frascati.parser.api.ParsingContext;
import org.ow2.frascati.util.AbstractLoggeable;

/**
 * Transform an OASIS CSA model to an OSOA model.
 *
 * @author <a href="mailto:philippe.merle@inria.fr">Philippe Merle</a>
 * @version 1.5
 */
@Scope("COMPOSITE")
public class OasisCsa2OsoaResolver
     extends AbstractLoggeable
  implements Resolver<EObject>
{
  //---------------------------------------------------------------------------
  // Internal state.
  // --------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // Internal methods.
  // --------------------------------------------------------------------------

  /**
   * Transform an OASIS Composite to an OSOA Composite.
   */
  private org.eclipse.stp.sca.Composite transform(org.eclipse.stp.sca.csa.Composite oasisComposite)
  {
    // Create an OSOA Composite.
    org.eclipse.stp.sca.Composite osoaComposite = ScaFactory.eINSTANCE.createComposite();
    // Copy the composite name.
    osoaComposite.setName(oasisComposite.getName());

    // TODO: Copy or transform other properties.

    // Transform all composite services.
    for(org.eclipse.stp.sca.csa.Service oasisService : oasisComposite.getService()) {
      // Create an OSOA Service.
      org.eclipse.stp.sca.Service osoaService = ScaFactory.eINSTANCE.createService();
      // Copy the service name.
      osoaService.setName(oasisService.getName());
      // Copy the promoted component service.
      org.eclipse.stp.sca.csa.ComponentService oasisComponentService = oasisService.getPromote();
      if(oasisComponentService != null) {
        osoaService.setPromote(pathTo(oasisComponentService));
      }

      // TODO: Copy or transform other properties.

      // Transform the service interface.
      osoaService.setInterface(transform(oasisService.getInterface()));

      // Transform bindings.
      for(org.eclipse.stp.sca.csa.Binding oasisBinding : oasisService.getBinding()) {
        osoaService.getBinding().add(transform(oasisBinding));
      }

      // Add the new service to the OSOA composite.
      osoaComposite.getService().add(osoaService);
    }

    // Transform all composite references.
    for(org.eclipse.stp.sca.csa.Reference oasisReference : oasisComposite.getReference()) {
      // Create an OSOA Reference.
      org.eclipse.stp.sca.Reference osoaReference = ScaFactory.eINSTANCE.createReference();
      // Copy the reference name.
      osoaReference.setName(oasisReference.getName());

      // TODO: Copy or transform other properties.

      // Transform the reference interface.
      osoaReference.setInterface(transform(oasisReference.getInterface()));

      // Transform bindings.
      for(org.eclipse.stp.sca.csa.Binding oasisBinding : oasisReference.getBinding()) {
        osoaReference.getBinding().add(transform(oasisBinding));
      }

      // Add the new reference to the OSOA composite.
      osoaComposite.getReference().add(osoaReference);
    }

    // Transform all composite properties.
    for(org.eclipse.stp.sca.csa.Property oasisProperty : oasisComposite.getProperty()) {
      // Create an OSOA Property.
      org.eclipse.stp.sca.Property osoaProperty = ScaFactory.eINSTANCE.createProperty();
      // Copy the property name.
      osoaProperty.setName(oasisProperty.getName());

      // Copy the property type.
      osoaProperty.setType(oasisProperty.getType());

      // Copy the property element.
      osoaProperty.setElement(oasisProperty.getElement());

      // TODO: Copy or transform other properties.

      // Add the new property to the OSOA composite.
      osoaComposite.getProperty().add(osoaProperty);
    }

    // Transform all components.
    for(org.eclipse.stp.sca.csa.Component oasisComponent : oasisComposite.getComponent()) {
      // Create an OSOA Component.
      org.eclipse.stp.sca.Component osoaComponent = ScaFactory.eINSTANCE.createComponent();
      // Copy the property name.
      osoaComponent.setName(oasisComponent.getName());

      // TODO: Copy or transform other properties.

      // Transform the component implementation.
      //
      // The OSOA metamodel does not provide the following method:
      // osoaComponent.setImplementation(transform(oasisComponent.getImplementation()));
      //
      // Then following is a working hack!
      org.eclipse.stp.sca.Implementation osoaImplementation = transform(oasisComponent.getImplementation());
      EReference eReference = null;
      if(osoaImplementation instanceof org.eclipse.stp.sca.SCAImplementation) {
        eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE;
      }
      if(osoaImplementation instanceof org.eclipse.stp.sca.JavaImplementation) {
        eReference = org.eclipse.stp.sca.ScaPackage.Literals.DOCUMENT_ROOT__IMPLEMENTATION_JAVA;
      }
      // TODO: manage other implementations, as BPEL, Spring, etc.

      ((FeatureMap.Internal) osoaComponent.getImplementationGroup()).clear();
      ((FeatureMap.Internal) osoaComponent.getImplementationGroup()).add(
          org.eclipse.stp.sca.ScaPackage.Literals.COMPONENT__IMPLEMENTATION_GROUP,
          org.eclipse.emf.ecore.util.FeatureMapUtil.createEntry(
              eReference,
              osoaImplementation));

      // Transform all component services.
      for(org.eclipse.stp.sca.csa.ComponentService oasisComponentService : oasisComponent.getService()) {
        // Create an OSOA ComponentService.
        org.eclipse.stp.sca.ComponentService osoaComponentService = ScaFactory.eINSTANCE.createComponentService();
        // Copy the component service name.
        osoaComponentService.setName(oasisComponentService.getName());

        // TODO: Copy or transform other properties.

        // Transform the component service interface.
        osoaComponentService.setInterface(transform(oasisComponentService.getInterface()));

        // Transform bindings.
        for(org.eclipse.stp.sca.csa.Binding oasisBinding : oasisComponentService.getBinding()) {
          osoaComponentService.getBinding().add(transform(oasisBinding));
        }

        // Add the new service to the OSOA component.
        osoaComponent.getService().add(osoaComponentService);
      }

      // Transform all component references.
      for(org.eclipse.stp.sca.csa.ComponentReference oasisComponentReference : oasisComponent.getReference()) {
        // Create an OSOA ComponentReference.
        org.eclipse.stp.sca.ComponentReference osoaComponentReference = ScaFactory.eINSTANCE.createComponentReference();
        // Copy the component reference name.
        osoaComponentReference.setName(oasisComponentReference.getName());

        // Copy all target component services.
        StringBuilder sb = new StringBuilder();
        for(org.eclipse.stp.sca.csa.ComponentService oasisComponentService : oasisComponentReference.getTarget()) {
          sb.append(' ');
          sb.append(pathTo(oasisComponentService));
        }
        if(sb.length() > 0) {
          osoaComponentReference.setTarget(sb.toString().substring(1));
        }

        // TODO: Copy or transform other properties.

        // Transform the component reference interface.
        osoaComponentReference.setInterface(transform(oasisComponentReference.getInterface()));

        // Transform bindings.
        for(org.eclipse.stp.sca.csa.Binding oasisBinding : oasisComponentReference.getBinding()) {
          osoaComponentReference.getBinding().add(transform(oasisBinding));
        }

        // Add the new service to the OSOA component.
        osoaComponent.getReference().add(osoaComponentReference);
      }

      // Transform all component properties.
      for(org.eclipse.stp.sca.csa.PropertyValue oasisPropertyValue : oasisComponent.getProperty()) {
        // Create an OSOA PropertyValue.
        org.eclipse.stp.sca.PropertyValue osoaPropertyValue = ScaFactory.eINSTANCE.createPropertyValue();
        // Copy the property name.
        osoaPropertyValue.setName(oasisPropertyValue.getName());
        // Copy the property mixed.
        osoaPropertyValue.getMixed().addAll(oasisPropertyValue.getMixed());
        // Copy the property value.
        osoaPropertyValue.setValue(oasisPropertyValue.getValue());
        // Copy the property mixed to the value.
        FeatureMap mixed = oasisPropertyValue.getMixed();
        if(mixed.size() == 1) {
          osoaPropertyValue.setValue((String)mixed.get(0).getValue());
        }
        // Copy the property type.
        osoaPropertyValue.setType(oasisPropertyValue.getType());
        // Copy the property source.
        osoaPropertyValue.setSource(oasisPropertyValue.getSource());

        // TODO: Copy or transform other properties.

        // Add the new property to the component.
        osoaComponent.getProperty().add(osoaPropertyValue);
      }

      // Add the component to the OSOA composite.
      osoaComposite.getComponent().add(osoaComponent);
    }

    // TODO: Transform wires.

    return osoaComposite;
  }

  /**
   * Transform an OASIS Implementation to an OSOA Implementation.
   */
  private org.eclipse.stp.sca.Implementation transform(org.eclipse.stp.sca.csa.Implementation oasisImplementation)
  {
    // If <implementation.composite>.
    if(oasisImplementation instanceof org.eclipse.stp.sca.csa.SCAImplementation) {
      org.eclipse.stp.sca.csa.SCAImplementation oasisSCAImplementation = (org.eclipse.stp.sca.csa.SCAImplementation)oasisImplementation;
      // Create an OSOA SCAImplementation.
      org.eclipse.stp.sca.SCAImplementation osoaSCAImplementation = ScaFactory.eINSTANCE.createSCAImplementation();
      // Copy the composite name.
      osoaSCAImplementation.setName(oasisSCAImplementation.getName());

      // TODO: Copy or transform other properties.

      return osoaSCAImplementation;
    }

    // If <implementation.java>.
    if(oasisImplementation instanceof org.eclipse.stp.sca.csa.JavaImplementation) {
      org.eclipse.stp.sca.csa.JavaImplementation oasisJavaImplementation = (org.eclipse.stp.sca.csa.JavaImplementation)oasisImplementation;
      // Create an OSOA JavaImplementation.
      org.eclipse.stp.sca.JavaImplementation osoaJavaImplementation = ScaFactory.eINSTANCE.createJavaImplementation();
      // Copy the Java class name.
      osoaJavaImplementation.setClass(oasisJavaImplementation.getClass_());

      // TODO: Copy or transform other properties.

      return osoaJavaImplementation;
    }

    // TODO: BPEL implementation.

    // TODO: Spring implementation.

    // TODO: other implementations.

    // Else do nothing.
    return null;
  }

  /**
   * Transform an OASIS Interface to an OSOA Interface.
   */
  private org.eclipse.stp.sca.Interface transform(org.eclipse.stp.sca.csa.Interface oasisInterface)
  {
    // If <interface.java>.
    if(oasisInterface instanceof org.eclipse.stp.sca.csa.JavaInterface) {
      org.eclipse.stp.sca.csa.JavaInterface oasisJavaInterface =
        (org.eclipse.stp.sca.csa.JavaInterface)oasisInterface;
      // Create an OSOA JavaInterface.
      org.eclipse.stp.sca.JavaInterface osoaJavaInterface = ScaFactory.eINSTANCE.createJavaInterface();
      // Copy the Java interface.
      osoaJavaInterface.setInterface(oasisJavaInterface.getInterface());

      // TODO: Copy or transform other properties.

      return osoaJavaInterface;
    }

    // If <interface.wsdl>.
    if(oasisInterface instanceof org.eclipse.stp.sca.csa.WSDLPortType) {
      org.eclipse.stp.sca.csa.WSDLPortType oasisWSDLPortType =
        (org.eclipse.stp.sca.csa.WSDLPortType)oasisInterface;
      // Create an OSOA WSDLPortType.
      org.eclipse.stp.sca.WSDLPortType osoaWSDLPortType = ScaFactory.eINSTANCE.createWSDLPortType();
      // Copy the interface.
      osoaWSDLPortType.setInterface(oasisWSDLPortType.getInterface());
      // Copy the callback interface.
      osoaWSDLPortType.setCallbackInterface(oasisWSDLPortType.getCallbackInterface());

      // TODO: Copy or transform other properties.

      return osoaWSDLPortType;
    }

    // TODO: other interfaces.

    // Else do nothing.
    return null;
  }

  /**
   * Transform an OASIS Binding to an OSOA Binding.
   */
  private org.eclipse.stp.sca.Binding transform(org.eclipse.stp.sca.csa.Binding oasisBinding)
  {
    // If <binding.ws>.
    if(oasisBinding instanceof org.eclipse.stp.sca.csa.WebServiceBinding) {
      org.eclipse.stp.sca.csa.WebServiceBinding oasisWebServiceBinding =
        (org.eclipse.stp.sca.csa.WebServiceBinding)oasisBinding;
      // Create an OSOA WebServiceBinding.
      org.eclipse.stp.sca.WebServiceBinding osoaWebServiceBinding = ScaFactory.eINSTANCE.createWebServiceBinding();
      // Copy the binding name.
      osoaWebServiceBinding.setName(oasisWebServiceBinding.getName());
      // Copy the binding uri.
      osoaWebServiceBinding.setUri(oasisWebServiceBinding.getUri());
      // Copy the binding wsdl element.
      osoaWebServiceBinding.setWsdlElement(oasisWebServiceBinding.getWsdlElement());
      // Copy the binding wsdl location.
      osoaWebServiceBinding.setWsdlLocation(oasisWebServiceBinding.getWsdlLocation());

      // TODO: Copy or transform other properties.

      return osoaWebServiceBinding;
    }

    // TODO: other bindings.

    // Else do nothing.
    return null;
  }

  /**
   * Path to a component service.
   */
  private String pathTo(org.eclipse.stp.sca.csa.ComponentService oasisComponentService)
  {
    return componentName(oasisComponentService)
           + '/'
           + oasisComponentService.getName();
  }

  private String componentName(EObject eObject)
  {
    return ((org.eclipse.stp.sca.csa.Component)eObject.eContainer()).getName();
  }

  //---------------------------------------------------------------------------
  // Public methods.
  // --------------------------------------------------------------------------

  /**
   * @see org.ow2.frascati.parser.api.Resolver#resolve(Type, ParsingContext)
   */
  public final EObject resolve(EObject element, ParsingContext parsingContext)
  {
	// If OASIS CSA document
    if(element instanceof org.eclipse.stp.sca.csa.DocumentRoot) {
      org.eclipse.stp.sca.csa.DocumentRoot oasisCsaDocumentRoot = (org.eclipse.stp.sca.csa.DocumentRoot)element;

      // Create an OSOA document.
      org.eclipse.stp.sca.DocumentRoot osoaDocumentRoot = ScaFactory.eINSTANCE.createDocumentRoot();

      // TODO: Copy all properties.

      // TODO: Transform the OASIS contribution.

      // Transform the OASIS composite.
      osoaDocumentRoot.setComposite(transform(oasisCsaDocumentRoot.getComposite()));

      return osoaDocumentRoot;
    }

    // Else do nothing.
    return element;
  }
}
