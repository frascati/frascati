/**
 * OW2 FraSCAti: OASIS CSA Metamodel
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.metamodel.oasis_csa.test;

import org.junit.Test;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.assembly.factory.api.ProcessingMode;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class FraSCAtiTest
{
    @Test
    public void newFraSCAti() throws Exception 
    {
      FraSCAti.newFraSCAti();
    }

    @Test
    public void parseThreadComposite() throws Exception
    {
      parseComposite("ThreadComposite");
    }

    @Test
    public void parseCompositeWithThreadComposite() throws Exception
    {
      parseComposite("CompositeWithThreadComposite");
    }

    @Test
    public void parseTEST_ASM_5002() throws Exception
    {
      parseComposite("TEST_ASM_5002");
    }

    private void parseComposite(String composite) throws Exception
    {
      FraSCAti frascati = FraSCAti.newFraSCAti();
      ProcessingContext processingContext = frascati.newProcessingContext();
      processingContext.setProcessingMode(ProcessingMode.check);
      frascati.processComposite(composite, processingContext);
      System.out.println("Composite " + composite + " parsed and checked successfully.\n");
    }
}
