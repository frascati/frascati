/**
 * OW2 FraSCAti HTTP Proxy
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.http.proxy;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.IOUtils;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.http.proxy.exception.BadParameterException;

/**
 *
 */
@Scope("COMPOSITE")
public class HttpProxyImpl implements HttpProxyItf
{
    private static Logger logger = Logger.getLogger(HttpProxyImpl.class.getName());
    
    /**
     * @see org.ow2.frascati.http.proxy.HttpProxyItf#sendRequest(java.lang.String, java.lang.String, java.lang.String)
     */
    public Response sendRequest(String url,String method, String contentType, String data)
    {
        logger.fine("[HttpProxyImpl sendRequest] method : "+method+", url : "+url+", data : "+data);
        
        HttpURLConnection connection = null; 
        
        //create http connection
        try
        {
            URL requestURL=new URL(url);
            connection = (HttpURLConnection) requestURL.openConnection();
        }
        catch (Exception exception)
        {
            logger.severe(url+" value is not valid for parameter url");
            if(connection!=null)
            {
                connection.disconnect();
            }
            throw new BadParameterException(exception, "url", url);
        }
        //set http method
        try
        {
            connection.setRequestMethod(method);
            connection.setUseCaches (false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
        }
        catch (Exception exception)
        {
            logger.severe(method+" value is not valid for parameter method");
            connection.disconnect();
            throw new BadParameterException(exception, "method", method);
        }
        
        //set content type property
        if(contentType != null && !contentType.equals(""))
        {
            connection.setRequestProperty( "Content-Type", contentType);
        }
        
        //write data
        if(data!=null && !data.equals(""))
        {
            try
            {
                connection.setRequestProperty("Content-Length", String.valueOf(data.length()));
                OutputStream conectionOutputStream = connection.getOutputStream();
                IOUtils.write(data, conectionOutputStream);
                IOUtils.closeQuietly(conectionOutputStream);
            }
            catch (IOException ioException)
            {
              logger.severe(data+" value is not valid for parameter data");
              connection.disconnect();
              throw new BadParameterException(ioException, "data", data);
            }
        }
        
        //get Request response
        try
        {
            InputStream connectionInputStream=connection.getInputStream();
            String responseContent=IOUtils.toString(connectionInputStream);
            int responseCode=connection.getResponseCode();
            ResponseBuilder responseBuilder=Response.status(responseCode);
            responseBuilder.entity(responseContent);
            return responseBuilder.build();
        }
        catch (IOException ioException)
        {
            throw new WebApplicationException(ioException);
        }
        finally
        {
            connection.disconnect();
        }
       
    }

}
