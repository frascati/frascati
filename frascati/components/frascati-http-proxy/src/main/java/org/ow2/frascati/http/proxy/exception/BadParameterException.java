/**
 * OW2 FraSCAti HTTP Proxy
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.http.proxy.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 *
 */
public class BadParameterException extends WebApplicationException
{
    /**
     * 
     */
    private static final long serialVersionUID = 3962619238520127538L;

    /**
     * 
     */
    public BadParameterException(String parameterName, String paramValue)
    {
        super(Response.status(Status.BAD_REQUEST).entity("[BadParameterException] "+paramValue+ " value is not valid for parameter "+parameterName).type(MediaType.TEXT_PLAIN).build());
    }
    
    public BadParameterException(Throwable cause, String parameterName, String paramValue)
    {
        super(cause,Response.status(Status.BAD_REQUEST).entity("[BadParameterException] "+paramValue+ " value is not valid for parameter "+parameterName).type(MediaType.TEXT_PLAIN).build());
    }
}
