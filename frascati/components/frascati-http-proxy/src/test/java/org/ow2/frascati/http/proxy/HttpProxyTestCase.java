/**
 * OW2 FraSCAti HTTP Proxy
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.http.proxy;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
public class HttpProxyTestCase
{
    private static String TEST_RESOURCES_DIR;
    
    @BeforeClass
    public static void init()
    {
        TEST_RESOURCES_DIR=System.getProperty("test.resources.directory","src/test/resources/");
    }
    
    @Test
    public void test() throws FrascatiException, IOException
    {
        FraSCAti frascati=FraSCAti.newFraSCAti();
        //deploy counter rest exemple
        File counterRestFile=new File(TEST_RESOURCES_DIR+"counter-server.jar");
        frascati.getComposite("counter-server",new URL[]{counterRestFile.toURI().toURL()});
        String counterServerURI="http://localhost:9090/CounterService";
        FraSCAtiTestUtils.assertWADLExist(true, counterServerURI);
        
        //deploy proxy exemple
        frascati.getComposite("frascati-http-proxy-exemple");
        String httpProxyExempleUri=FraSCAtiTestUtils.completeBindingURI("/httpProxyExemple");
        FraSCAtiTestUtils.assertWADLExist(true, httpProxyExempleUri);
        
        //test proxy
        HttpProxyItf proxyClient=JAXRSClientFactory.create(httpProxyExempleUri, HttpProxyItf.class);
        Response response=proxyClient.sendRequest(counterServerURI, "GET", null, null);
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        String responseString=IOUtils.toString((InputStream) response.getEntity());
        assertEquals("0", responseString);
        
        response=proxyClient.sendRequest(counterServerURI+"/incr", "POST", null, "v=5");
        assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus());
        responseString=IOUtils.toString((InputStream) response.getEntity());
        assertEquals("", responseString);
    }
}
