/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.widget.registry;

import java.util.List;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.Widgets;
import org.ow2.frascati.widget.provider.WidgetProviderImpl;
import org.ow2.frascati.widget.provider.WidgetProviderItf;

/**
 *
 */
@Scope("COMPOSITE")
public class WidgetRegistryImpl implements WidgetRegistryItf
{
    private static Logger logger=Logger.getLogger(WidgetRegistryImpl.class.getName());
    
    private Widgets widgets;
    private List<WidgetProviderItf> widgetProviders;
    
    public WidgetRegistryImpl()
    {
        this.widgets=new Widgets();
    }
    
    public List<WidgetProviderItf> getWidgetProviders()
    {
        logger.fine("getWidgetProviders");
        return widgetProviders;
    }
    
    @Reference(name="widgetProviders")
    public void setWidgetProviders(List<WidgetProviderItf> widgetProviders)
    {
        this.widgetProviders = widgetProviders;
        this.widgets=new Widgets();
        for(WidgetProviderItf widgetProvider : widgetProviders)
        {
            this.widgets.getWidgets().add(widgetProvider.getWidget());
        }
    }
    

    /**
     * @see org.ow2.frascati.widget.registry.WidgetRegistryItf#getWidgets()
     */
    public Widgets getWidgets()
    {
        return this.widgets;
    }
    

    /**
     * @see org.ow2.frascati.widget.registry.WidgetRegistryItf#addWidget(org.ow2.frascati.widget.Widget)
     */
    public boolean addWidget(Widget widget)
    {
        WidgetProviderImpl widgetProviderImpl=new WidgetProviderImpl(widget);
        this.widgetProviders.add(widgetProviderImpl);
        this.widgets.getWidgets().add(widget);
        return true;
    }

    public boolean addWidget(String name, String iconUrl, String smallWidgetUrl, String bigWidgetUrl)
    {
        Widget widget=new Widget();
        widget.setName(name);
        widget.setIconURL(iconUrl);
        widget.setSmallWidgetURL(smallWidgetUrl);
        widget.setLargeWidgetURL(bigWidgetUrl);
        WidgetProviderImpl widgetProviderImpl=new WidgetProviderImpl(widget);
        this.widgetProviders.add(widgetProviderImpl);
        this.widgets.getWidgets().add(widget);
        return true;
    }


    /**
     * @see org.ow2.frascati.widget.registry.WidgetRegistryItf#removeWidget(java.lang.String)
     */
    public boolean removeWidget(String name)
    {
        Widget targetWidget = null;
        for(Widget widget : this.widgets.getWidgets())
        {
            if(widget.getName().equals(name))
            {
                targetWidget=widget;
            }
        }
        
        for(WidgetProviderItf widgetProvider: widgetProviders)
        {
            if(widgetProvider.getWidget().getName().equals(name))
            {
                this.widgetProviders.remove(widgetProvider);
            }
        }
        
        if(targetWidget==null)
        {              
            return false;
        }
        
        this.widgets.getWidgets().remove(targetWidget);
        return true;
    }

}
