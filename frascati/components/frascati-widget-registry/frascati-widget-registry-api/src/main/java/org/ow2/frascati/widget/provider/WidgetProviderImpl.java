/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.widget.provider;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.widget.Widget;

/**
 *
 */
@Scope("COMPOSITE")
public class WidgetProviderImpl implements WidgetProviderItf
{
    public String name;
    public String iconURL;
    public String smallWidgetURL;
    public String largeWidgetURL;
    
    
    public WidgetProviderImpl()
    {
        this.name="undefined";
        this.iconURL="undefined";
        this.smallWidgetURL="undefined";
        this.largeWidgetURL="undefined";
    }
    
    public WidgetProviderImpl(Widget widget)
    {
        this.name=widget.getName();
        this.iconURL=widget.getIconURL();
        this.smallWidgetURL=widget.getSmallWidgetURL();
        this.largeWidgetURL=widget.getLargeWidgetURL();
    }
    
    public Widget getWidget()
    {
        Widget widget=new Widget();
        widget.setName(name);
        widget.setIconURL(iconURL);
        widget.setSmallWidgetURL(smallWidgetURL);
        widget.setLargeWidgetURL(largeWidgetURL);
        return widget;
    }

    public String getName()
    {
        return name;
    }

    @Property(name="name")
    public void setName(String name)
    {
        this.name = name;
    }

    public String getIconURL()
    {
        return iconURL;
    }

    @Property(name="iconURL")
    public void setIconURL(String iconURL)
    {
        this.iconURL = this.completeURL(iconURL);
    }

    public String getSmallWidgetURL()
    {
        return smallWidgetURL;
    }

    @Property(name="smallWidgetURL")
    public void setSmallWidgetURL(String smallWidgetURL)
    {
        this.smallWidgetURL = this.completeURL(smallWidgetURL);
    }

    public String getBigWidgetURL()
    {
        return largeWidgetURL;
    }

    @Property(name="largeWidgetURL")
    public void setLargeWidgetURL(String bigWidgetURL)
    {
        this.largeWidgetURL = this.completeURL(bigWidgetURL);
    }
    
    private String completeURL(String url)
    {
        String completedUrl=url;
        if(completedUrl != null && completedUrl.startsWith("/"))
        {
            completedUrl = System.getProperty(AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingProcessor.BINDING_URI_BASE_DEFAULT_VALUE) + url;
        }
        return completedUrl;
    }
}
