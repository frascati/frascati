/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.exemple.test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.ow2.frascati.cep.eventListener.EventListenerItf;
import org.ow2.frascati.cep.eventType.EventTypeItf;
import org.ow2.frascati.cep.statement.StatementItf;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
public class CEPEngineExempleTest extends  FraSCAtiTestCase
{
    @Test
    public void test() throws FrascatiException
    {
        EventTypeItf lightEventTypeItf=this.getService(EventTypeItf.class,"lightEventType");
        assertNotNull(lightEventTypeItf);
        StatementItf allLightMessageStatement = this.getService(StatementItf.class,"allLightMessageStatement");
        assertNotNull(allLightMessageStatement);
        EventListenerItf lightListener=this.getService(EventListenerItf.class, "lightListener");
        assertNotNull(lightListener);
    }
}
