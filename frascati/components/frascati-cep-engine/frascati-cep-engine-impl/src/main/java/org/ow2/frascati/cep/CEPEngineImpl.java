/**
 * OW2 FraSCAti : FraSCAti CEP Engine
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.io.IOUtils;
import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.cep.coreManager.CEPCoreManagerItf;
import org.ow2.frascati.cep.eventType.EventTypeImpl;
import org.ow2.frascati.cep.eventType.EventTypeItf;
import org.ow2.frascati.cep.eventType.EventTypeManagerItf;
import org.ow2.frascati.cep.exception.IllegalCEPLifeCycleException;
import org.ow2.frascati.cep.util.CEPLogger;
import org.ow2.frascati.cep.util.MultipleReferenceHelper;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.ConfigurationEventTypeXMLDOM;
import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;


/**
 * @see org.ow2.frascati.cep.CEPEngineItf
 */
@Scope("COMPOSITE")
public class CEPEngineImpl extends MultipleReferenceHelper<EventTypeItf> implements CEPEngineItf,EventTypeManagerItf
{
    /**
     * ClassName of the factory to obtain DOMImplementation
     */
    private static final String DOM_IMPLEMENTATION_REGISTRY = "com.sun.org.apache.xerces.internal.dom.DOMXSImplementationSourceImpl";

    /**
     * Reference to the Statement manager
     */
    @Reference(name="statementCoreManager")
    private CEPCoreManagerItf statementCoreManager;
    
    /**
     * Reference to the EventListener manager
     */
    @Reference(name="eventListenerCoreManager")
    private CEPCoreManagerItf eventListenerCoreManager;
    
    /**
     * Name of the SCA component
     */
    @ComponentName
    protected String componentName;

    /**
     * Configuration of the Esper Engine
     */
    private Configuration esperEngineConfig;

    /**
     * The Esper Engine
     */
    private EPServiceProvider esperEngine;
    
    /**
     * State of the CEP engine
     */
    private boolean isStarted;
    
    /**
     * Default constructor
     */
    public CEPEngineImpl()
    {
        this.isStarted=false;
    }
    
    @Init
    public void init()
    {
        CEPLogger.logEntering(this,"init");
        System.setProperty(DOMImplementationRegistry.PROPERTY, CEPEngineImpl.DOM_IMPLEMENTATION_REGISTRY);
        this.esperEngineConfig = new Configuration();
        
        /** Init Esper configuration*/
        /** Add the event types bound to eventTypes reference*/
        for(EventTypeItf eventType : this.managedObjects)
        {
            this.addEventTypeItf(eventType);
        }
        
        start();
    }

    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#start()
     */
    public void start()
    {
        CEPLogger.logEntering(this,"start");
        this.esperEngine = EPServiceProviderManager.getProvider(this.componentName, this.esperEngineConfig);
        EPAdministrator epAdministrator=this.esperEngine.getEPAdministrator();
        statementCoreManager.start(epAdministrator);
        eventListenerCoreManager.start(epAdministrator);
        this.isStarted=true;
    }
    
    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#stop()
     */
    public void stop()
    {
        CEPLogger.logEntering(this,"stop");
        this.isStarted=false;
        if(this.esperEngine!=null)
        {
            this.esperEngine.destroy();
            CEPLogger.log(this, "esperEngine is destroy ? "+this.esperEngine.isDestroyed());
        }
        CEPLogger.log(this, "trying to stop statementCoreManager..."+statementCoreManager);
        statementCoreManager.stop();
        CEPLogger.log(this, "statementCoreManager stopped...trying to stop eventListenerCoreManager");
        eventListenerCoreManager.stop();
        CEPLogger.logExiting(this, "stop ");
    }

    /**
     * Util to restart the Engine
     * Useful when Esper Configuration change
     */
    public void restart()
    {
        this.stop();
        this.start();
    }
    
    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#isStarted()
     */
    public boolean isStarted()
    {
       return isStarted;
    }
    
    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#sendEvent(java.lang.Object)
     */
    public void sendEvent(Object event)
    {
        /** Engine must be started to send  */
        if(!this.isStarted())
        {
           throw new IllegalCEPLifeCycleException("sendEvent");
        }
        this.esperEngine.getEPRuntime().sendEvent(event);
    }
    
    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#sendAsynchronousEvent(java.lang.Object)
     */
    public void sendAsynchronousEvent(final Object event)
    {
        new Thread(new Runnable()
        {
            public void run()
            {
                sendEvent(event);
            }
        }).start();
    }
    
    /**
     * @return a list of all EventTypeItf bounded to eventTypes reference
     */
    public List<EventTypeItf> getEventTypesReference()
    {
        CEPLogger.logEntering(this,"getEventTypesReference ", getReferencedObjects().size());
        return getReferencedObjects();
    }
    
    /**
     * @param eventTypes the new updated reference list
     */
    @Reference(name="eventTypesReference")
    public void setEventTypesReference(List<EventTypeItf> eventTypes)
    {
        CEPLogger.logEntering(this,"setEventTypesReference ", eventTypes.size());
        setReferencedObjects(eventTypes);
    }
    
    /**
     * Setter for eventTypesProperty SCA property
     * 
     * @param eventTypesProperty the EventTypes property
     */
    @Property(name="eventTypesProperty")
    public void setEventTypesProperty(EventTypes eventTypesProperty)
    {
        CEPLogger.logEntering(this, "setEventTypesProperty", eventTypesProperty.getEventTypes().size());
        /** Add the event types defined in eventTypesProperty property to the managedObject list*/
        EventTypeItf eventType;
        for(EventType eventTypeProperty : eventTypesProperty.getEventTypes())
        {
            eventType=new EventTypeImpl(eventTypeProperty);
            this.managedObjects.add(eventType);
        }
    }
    
    /**
     * This method is call when a new EventTypeItf is bound to the engine 
     * 
     * @see org.ow2.frascati.cep.util.MultipleReferenceHelper#newReferencedObject(java.lang.Object)
     */
    @Override
    public void newReferencedObject(EventTypeItf eventType)
    {
        CEPLogger.logEntering(this, "newReferencedObject", "Engine started ? ", this.isStarted);
        
        if(this.isStarted)
        {
            boolean isEventTypeItfAdded=this.addEventTypeItf(eventType);
            CEPLogger.log(this, "isEventTypeItfAdded ? "+isEventTypeItfAdded);
            if(isEventTypeItfAdded)
            {
                /**If the Engine is running we must restart it after modifying the engine configuration*/
                this.restart();
            }
        }
    }
    
    /**
     * This method is used internally when a new EventTypeItf is bounded
     * 
     * @param eventType the new bounded EventTypeItf
     * @throws ClassNotFoundException 
     */
    private boolean addEventTypeItf(EventTypeItf eventType)
    {
        CEPLogger.logEntering(this, "addEventTypeItf", eventType.getEventAttType(), eventType.getValue());
       
        try
        {
            this.addEventType(eventType.getEventAttType(), eventType.getValue());
            return true;
        }
        catch(ClassNotFoundException classNotFoundException)
        {
            CEPLogger.log(this, Level.SEVERE, "ClassNotFoundException when trying to add EventType "+eventType.getEventAttType().name()+" "+eventType.getValue());
            return false;
        }
    }

    /**
     * This method is used at runtime to add new eventType
     * @throws ClassNotFoundException 
     * 
     * @see org.ow2.frascati.cep.eventType.EventTypeManagerItf#addEventType(org.ow2.frascati.cep.EventType)
     */
    public void addEventType(EventType eventType) throws ClassNotFoundException
    {
        CEPLogger.logEntering(this, "addEventType", eventType.getEventType(), eventType.getValue());
        if(!isStarted)
        {
            throw new IllegalCEPLifeCycleException("addEventType");
        }
        this.addEventType(eventType.getEventType(), eventType.getValue());
        this.restart();
    }
    
    /**
     * Add a new event type to engine configuration
     * 
     * @param eventAttType the type of the EventType(CLASS,NODE...)
     * @param value value of the new EventType
     * @throws ClassNotFoundException 
     */
    private void addEventType(EventAttType eventAttType, String value) throws ClassNotFoundException
    {
        CEPLogger.logEntering(this, "addEventType", eventAttType.name(),value);
        if(eventAttType.equals(EventAttType.CLASS))
        {
            this.addPOJOEventType(value);
        }
        else if(eventAttType.equals(EventAttType.NODE))
        {
            
        }
        else
        {
            CEPLogger.log(this, Level.SEVERE, eventAttType.name()+" event type is not supported");
        }
    }
    
    /**
     * Add a new POJO event type to engine configuration
     * 
     * @param classname The class name of the new EventType
     * @throws ClassNotFoundException if the class can't be found in the current ClassLoader
     */
    private void addPOJOEventType(String classname) throws ClassNotFoundException
    {
        CEPLogger.logEntering(this, "addPOJOEventType", classname);
        // Load class using its package and name
        Class<?> clazz =null;
        try
        {
            clazz = Class.forName(classname);
        }
        catch(ClassNotFoundException classNotFoundException)
        {
            CEPLogger.log(this, Level.SEVERE, "Can't find "+classname+" in the current ClassLoader");
            throw classNotFoundException;
        }

        
//        if(this.esperEngine==null)
//        {
            this.esperEngineConfig.addImport(classname);
            this.esperEngineConfig.addEventType(clazz.getSimpleName(), clazz);
//        }
//        else
//        {
//            this.esperEngine.getEPAdministrator().getConfiguration().addImport(classname);
//            this.esperEngine.getEPAdministrator().getConfiguration().addEventType(clazz.getSimpleName(), clazz);
//        }
    }
    
    
    
    public void addXSDEventType(String xsdFileName, String rootElementName) throws MalformedURLException
    {
        CEPLogger.logEntering(this, "addXSDEventType", xsdFileName, rootElementName);
        URL xsdURL = this.getClass().getClassLoader().getResource(xsdFileName);
        addXSDEventType(xsdURL, rootElementName);
    }
    
    private void addXSDEvent(InputStream fileInputStream, String rootElementName) throws IOException
    {
        File xsdFile=File.createTempFile("frascati", "cepEngine");
        FileOutputStream xsdOutputSream=new FileOutputStream(xsdFile);
        IOUtils.copy(fileInputStream, xsdOutputSream);
        URL xsdURL=new URL(xsdFile.getPath());
        addXSDEventType(xsdURL, rootElementName);
    }
    
    private void addXSDEventType(URL xsdURL, String rootElementName) throws MalformedURLException
    {
        CEPLogger.logEntering(this, "addXSDEventType", xsdURL.getPath(), rootElementName);
        ConfigurationEventTypeXMLDOM xsdConfig = new ConfigurationEventTypeXMLDOM();
        xsdConfig.setRootElementName(rootElementName);
        xsdConfig.setSchemaResource(xsdURL.toString());
        esperEngine.getEPAdministrator().getConfiguration().addEventType(rootElementName,xsdConfig);
        this.restart();
    }
    
}
