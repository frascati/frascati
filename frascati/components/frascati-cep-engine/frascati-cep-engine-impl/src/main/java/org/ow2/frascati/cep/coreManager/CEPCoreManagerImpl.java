/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.coreManager;

import org.ow2.frascati.cep.util.CEPLogger;
import org.ow2.frascati.cep.util.MultipleReferenceHelper;

import com.espertech.esper.client.EPAdministrator;

/**
 *
 */
public abstract class CEPCoreManagerImpl<ReferencedObject> extends MultipleReferenceHelper<ReferencedObject> implements CEPCoreManagerItf
{
    protected EPAdministrator epAdministrator;

    private boolean isStarted;
    
    public CEPCoreManagerImpl()
    {
        super();
        CEPLogger.logEntering(this, "constructor");
        this.isStarted=false;
    }
    
    /**
     * @see org.ow2.frascati.cep.coreManager.CEPCoreManagerItf#start(com.espertech.esper.client.EPAdministrator)
     */
    public void start(EPAdministrator epAdministrator)
    {
        CEPLogger.logEntering(this, "start", epAdministrator);
        this.epAdministrator=epAdministrator;
        this.isStarted=true;
        for(ReferencedObject managedObject : managedObjects)
        {
            this.registerNewManagedObject(managedObject);
        }
    }

    /**
     * @see org.ow2.frascati.cep.coreManager.CEPCoreManagerItf#stop()
     */
    public void stop()
    {
        CEPLogger.logEntering(this, "stop");
        isStarted=false;
    }
    
    /**
     * @see org.ow2.frascati.cep.coreManager.CEPCoreManagerItf#isStarted()
     */
    public boolean isStarted()
    {
        CEPLogger.logEntering(this, "stop");
        return this.isStarted;
    }
    
    @Override
    public void newReferencedObject(ReferencedObject referencedObject)
    {
        if(this.isStarted)
        {
            this.registerNewManagedObject(referencedObject);
        }
    }
    
    protected abstract void registerNewManagedObject(ReferencedObject managedObject);
}
