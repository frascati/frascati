/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.statement;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.cep.Statement;
import org.ow2.frascati.cep.Statements;
import org.ow2.frascati.cep.coreManager.CEPCoreManagerImpl;
import org.ow2.frascati.cep.statement.exception.AlreadyRegisteredStatementException;
import org.ow2.frascati.cep.statement.exception.InvalidStatementException;
import org.ow2.frascati.cep.statement.exception.NotRegisteredStatementException;
import org.ow2.frascati.cep.util.CEPLogger;

import com.espertech.esper.client.EPException;
import com.espertech.esper.client.EPStatement;

/**
 *
 */
@Scope("COMPOSITE")
public class StatementManagerImpl extends CEPCoreManagerImpl<StatementItf> implements StatementManagerItf
{
    public List<StatementItf> getStatementsReference()
    {
        CEPLogger.logEntering(this,"getStatementsReference", getReferencedObjects().size());
        return getReferencedObjects();
    }
    
    @Reference(name="statementsReference")
    public void setStatementsReference(List<StatementItf> statements)
    {
        CEPLogger.logEntering(this,"setStatementsReference", statements.size());
        setReferencedObjects(statements);
    }
    
    @Property(name = "statementsProperty")
    public void setStatementsProperty(Statements statementsProperty)
    {
        CEPLogger.logEntering(this, "setStatementsProperty", statementsProperty.getStatements().size());
        /** Add all the statement properties in the statements reference list*/
        StatementItf statement;
        for(Statement statementProperty : statementsProperty.getStatements())
        {
            statement=new StatementImpl(statementProperty);
            this.managedObjects.add(statement);
            CEPLogger.log(this, "add statement property "+statement.getId()+" "+statement.getText());
        }
    }
    
    /**
     * @see org.ow2.frascati.cep.coreManager.CEPCoreManagerImpl#registerNewManagedObject(java.lang.Object)
     */
    @Override
    protected void registerNewManagedObject(StatementItf statement)
    {
        registerStatement(statement);
    }
    
    private boolean registerStatement(StatementItf statement)
    {
        String statementId = statement.getId();
        String statementText = statement.getText();
        CEPLogger.logEntering(this, "registerStatement", statementId, statementText);
        try
        {
            this.epAdministrator.createEPL(statementText, statementId);
            CEPLogger.log(this,"Register statement"+statement.getId()+" "+statement.getText());
            return true;
        }
        catch (EPException epException)
        {
            CEPLogger.log(this, Level.WARNING, "EPException while creating statement " + statementText+" "+epException.getMessage());
            return false;
        }

    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#getStatement(java.lang.String)
     */
    public Statement getStatement(String statementId) throws NotRegisteredStatementException
    {
        CEPLogger.logEntering(this, "getStatement",statementId);
        EPStatement epStatement = epAdministrator.getStatement(statementId);
        
        if(epStatement == null)
        {
            CEPLogger.log(this,Level.WARNING, "No statement found for id "+statementId);
            throw new NotRegisteredStatementException(statementId);
        }
        
        Statement statement=new Statement();
        statement.setId(statementId);
        statement.setValue(epStatement.getText());
        return statement;
    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#getStatements()
     */
    public List<Statement> getStatements()
    {
        CEPLogger.logEntering(this, "getStatements");
        List<Statement> statements=new ArrayList<Statement>();
        Statement statement;
        EPStatement epStatement;
        String[] statementIds=epAdministrator.getStatementNames();
        for(String statementId : statementIds)
        {
            epStatement=epAdministrator.getStatement(statementId);
            statement=new Statement();
            statement.setId(statementId);
            statement.setValue(epStatement.getText());
            statements.add(statement);
        }
        
        return statements;
    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#addStatement(java.lang.String)
     */
    public Statement addStatement(String statementText) throws InvalidStatementException
    {
        CEPLogger.logEntering(this, "addStatement",statementText);
        EPStatement epStatement=null;
        try
        {
            epStatement=epAdministrator.createEPL(statementText);
        }
        catch (EPException epException)
        {
            CEPLogger.log(this,Level.WARNING, "EPException while creating statement "+statementText);
            throw new InvalidStatementException(statementText, epException);
        }
        
        Statement statement=new Statement();
        String statementId=epStatement.getName();
        statement.setId(statementId);
        statement.setValue(statementText);
        CEPLogger.logExiting(this,"addStatement",statementId, statement);
        return statement;
    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#addStatement(java.lang.String, java.lang.String)
     */
    public void addStatement(String statementId, String statementText) throws AlreadyRegisteredStatementException, InvalidStatementException
    {
        CEPLogger.logEntering(this, "addStatement",statementId,statementText);
        EPStatement epStatement = epAdministrator.getStatement(statementId);

        if(epStatement != null)
        {
            CEPLogger.log(this,Level.WARNING, "Already registered statement for statementId "+statementId+" (statement = "+epStatement.getText()+")");
            throw new AlreadyRegisteredStatementException(statementId, epStatement);
        }
        
        try
        {
            epStatement=epAdministrator.createEPL(statementText, statementId);
        }
        catch (EPException epException)
        {
            CEPLogger.log(this,Level.WARNING, "EPException while creating statement "+statementText);
            throw new InvalidStatementException(statementId, statementText, epException);
        }

        CEPLogger.logExiting(this,"addStatement",epStatement);
        
    }

    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#addStatement(org.ow2.frascati.cep.Statement)
     */
    public void addStatement(Statement statement) throws AlreadyRegisteredStatementException, InvalidStatementException
    {
       this.addStatement(statement.getId(), statement.getValue());
    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#updateStatement(java.lang.String, java.lang.String)
     */
    public void updateStatement(String statementId, String statementText) throws NotRegisteredStatementException, InvalidStatementException
    {
        CEPLogger.logEntering(this, "updateStatement",statementId, statementText);
        removeStatement(statementId);
        try
        {
            addStatement(statementId, statementText);
        } catch (AlreadyRegisteredStatementException e)
        {
            CEPLogger.log(this,Level.SEVERE, "This must not happen");
        }
        CEPLogger.logExiting(this,"updateStatement");
        
    }

    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#updateStatement(org.ow2.frascati.cep.Statement)
     */
    public void updateStatement(Statement statement) throws NotRegisteredStatementException, InvalidStatementException
    {
        updateStatement(statement.getId(), statement.getValue());
    }

    /**
     * @see org.ow2.frascati.cep.statement.StatementManagerItf#removeStatement(java.lang.String)
     */
    public void removeStatement(String statementId) throws NotRegisteredStatementException
    {
        CEPLogger.logEntering(this, "removeStatement",statementId);
        EPStatement epStatement = epAdministrator.getStatement(statementId);
        
        if(epStatement==null)
        {
            CEPLogger.log(this,Level.WARNING, "No statement found for id "+statementId);
            throw new NotRegisteredStatementException(statementId);
        }
        
        epStatement.destroy();
        CEPLogger.logExiting(this,"removeStatement");
    }

    /**
     * @see org.ow2.frascati.cep.CEPEngineItf#removeStatement(org.ow2.frascati.cep.Statement)
     */
    public void removeStatement(Statement statement) throws NotRegisteredStatementException
    {
        this.removeStatement(statement.getId());
    }
}
