/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.eventListener;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import javax.ws.rs.core.MultivaluedMap;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.cep.coreManager.CEPCoreManagerImpl;
import org.ow2.frascati.cep.eventListener.exception.NotRegisteredEventListener;
import org.ow2.frascati.cep.exception.CEPException;
import org.ow2.frascati.cep.statement.exception.NotRegisteredStatementException;
import org.ow2.frascati.cep.util.CEPLogger;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;

import com.espertech.esper.client.EPStatement;

/**
 *
 */
@Scope("COMPOSITE")
public class EventListenerManagerImpl extends CEPCoreManagerImpl<EventListenerItf> implements EventListenerManagerItf
{
    @Reference(name="introspection")
    private RemoteScaDomain introspection;
    
    @ComponentName
    protected String componentName;
    
    private Map<String, EventListenerWrapper> eventListenerRegistry;
    
    public EventListenerManagerImpl()
    {
        super();
        this.eventListenerRegistry=new HashMap<String, EventListenerWrapper>();
    }

    
    public List<EventListenerItf> getEventListeners()
    {
        CEPLogger.logEntering(this,"getEventListeners", getReferencedObjects().size());
        return getReferencedObjects();
    }
    
    @Reference(name="eventListenersReference")
    public void setEventListeners(List<EventListenerItf> eventListeners)
    {
        CEPLogger.logEntering(this,"eventListenersReference", eventListeners.size());
        setReferencedObjects(eventListeners);
    }

    /**
     * @see org.ow2.frascati.cep.coreManager.CEPCoreManagerImpl#registerNewManagedObject(java.lang.Object)
     */
    @Override
    protected void registerNewManagedObject(EventListenerItf eventListener)
    {
        registerEventListener(eventListener);
    }
    
    public void registerEventListener(EventListenerItf eventListener)
    {
      registerListener(eventListener);
      CEPLogger.logEntering(this, "update", eventListener.getId());
      String eventListenerId=eventListener.getId();
      CEPLogger.log(this, "register EventListener id : "+eventListenerId);
      for(String eventId : eventListener.getEventIds())
      {
          try
          {
              addListener(eventListenerId, eventId);
              CEPLogger.log(this, "add EventListener id : "+eventListenerId+" to statement id : "+eventId);
          }
          catch(CEPException cepException)
          {
              CEPLogger.log(this,Level.SEVERE, "Fail to add EventListener id : "+eventListenerId+" to statement id : "+eventId+" ("+cepException.getMessage()+")");
          }
      }
    }
    
    public void registerListener(MultivaluedMap<String, String> params)
    {
        CEPLogger.logEntering(this, "registerListener");
        for(String entryParam : params.keySet())
        {
            CEPLogger.log(this,entryParam+" "+params.getFirst(entryParam));
        }
        introspection.addBinding("", params);
    }
    
    public void registerListener(EventListenerItf eventListener)
    {
        CEPLogger.logEntering(this, "registerListener", eventListener);
        UUID uuid=UUID.randomUUID();
        String eventListenerId=uuid.toString();
        eventListener.setId(eventListenerId);
        EventListenerWrapper eventListenerWrapper=new EventListenerWrapper(eventListener);
        eventListenerRegistry.put(eventListenerId, eventListenerWrapper);
    }
    
    public void addListener(String eventListenerId, String statementId) throws CEPException
    {
      CEPLogger.logEntering(this, "addListener", eventListenerId, statementId);
      EPStatement epStatement = epAdministrator.getStatement(statementId);
      if(epStatement==null)
      {
          CEPLogger.log(this,Level.WARNING, "No statement found for id "+statementId);
          throw new NotRegisteredStatementException(statementId);
      }
      
      EventListenerWrapper eventListenerWrapper=eventListenerRegistry.get(eventListenerId);
      if(eventListenerWrapper==null)
      {
          CEPLogger.log(Level.WARNING, "No eventListener found for id "+eventListenerId);
          throw new NotRegisteredEventListener(eventListenerId);
      }
      
      epStatement.addListener(eventListenerWrapper);
      CEPLogger.logExiting(this, "addListener");
    }
}
