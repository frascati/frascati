/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for composite owning a multiple reference
 */
public abstract class MultipleReferenceHelper<ReferencedObject>
{
    /**
     * All the ReferencedObject owned by a composite
     */
    protected List<ReferencedObject> managedObjects;
    
    
    /**
     * The referenced object list
     */
    private List<ReferencedObject> referencedObjects;
    
    
    public MultipleReferenceHelper()
    {
        this.managedObjects=new ArrayList<ReferencedObject>();
        this.referencedObjects=new ArrayList<ReferencedObject>();
    }
    
    /**
     * This method have to be call in the reference getter method of the composite
     * 
     * @return
     */
    public List<ReferencedObject> getReferencedObjects()
    {
        return referencedObjects;
    }
    
    /**
     * This method have to be call in the reference setter method of the composite
     * For all ReferencedObject not already in the managedObjects list call newReferencedObject method and add it to the managedObject list
     * 
     * @param referencedObjects the updated list of referenced objects
     */
    public void setReferencedObjects(List<ReferencedObject> referencedObjects)
    {
        this.referencedObjects = referencedObjects;
        for (ReferencedObject referencedObject : this.referencedObjects)
        {
            if(!managedObjects.contains(referencedObject))
            {
                managedObjects.add(referencedObject);
                newReferencedObject(referencedObject);
            }
        }
    }
    
    /**
     * This method is call when a new ReferencedObject is found
     * 
     * @param referencedObject the new ReferencedObject
     */
    public abstract void newReferencedObject(ReferencedObject referencedObject);
}
