/**
 * OW2 FraSCAti : FraSCAti CEP Engine
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.cep.eventListener.EventListenerManagerItf;
import org.ow2.frascati.cep.eventListener.EventListenerTestItf;
import org.ow2.frascati.cep.eventListener.SimpleMessageListener;
import org.ow2.frascati.cep.eventType.EventTypeManagerItf;
import org.ow2.frascati.cep.eventType.RuntimeMessage;
import org.ow2.frascati.cep.eventType.SimpleMessage;
import org.ow2.frascati.cep.exception.CEPException;
import org.ow2.frascati.cep.exception.IllegalCEPLifeCycleException;
import org.ow2.frascati.cep.statement.StatementManagerItf;
import org.ow2.frascati.cep.statement.exception.AlreadyRegisteredStatementException;
import org.ow2.frascati.cep.statement.exception.InvalidStatementException;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.cep.exemple.message.LightMessage;
/**
 * 
 */
public class CEPEngineTestSuite
{
    private static final String DEFAULT_TEST_RESOURCES_DIR="src/test/resources/";
    
    private final static Logger logger=Logger.getLogger(CEPEngineTestSuite.class.getName());
    
    private static String TEST_RESOURCES_DIR;
    
    private static FraSCAti frascati;
    
    private static Component frascatiComponent;
    
    private static Component cepEngineComponent;
    
    private static RemoteScaDomain introspection;
    
    private static Deployment deployment;
    
    private static CEPEngineItf cepEngine;
    
    private static CEPEngineItf remoteCepEngine;
    
    private static StatementManagerItf statementManager;
    
    private static EventListenerManagerItf eventListenerManager;
    
    private static EventTypeManagerItf eventTypeManager;
    
    private static EventTypeManagerItf remoteEventTypeManager;
    
    @BeforeClass
    public static void init() throws FrascatiException, NoSuchInterfaceException
    {
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTRest");
        TEST_RESOURCES_DIR=System.getProperty("test.resources.directory");
        
        frascati=FraSCAti.newFraSCAti();
        assertNotNull(frascati);
        
        frascatiComponent=frascati.getFrascatiComposite();
        assertNotNull(frascatiComponent);
        introspection=frascati.getService(frascatiComponent, "introspection", RemoteScaDomain.class);
        assertNotNull(introspection);
        deployment=frascati.getService(frascatiComponent, "deployment", Deployment.class);
        assertNotNull(deployment);
        
        cepEngineComponent=frascati.getComposite("cepEngine-test");
        assertNotNull(cepEngineComponent);
        cepEngine=frascati.getService(cepEngineComponent, "cepEngine", CEPEngineItf.class);
        assertNotNull(cepEngine);
        
        statementManager=frascati.getService(cepEngineComponent, "cepStatementManager", StatementManagerItf.class);
        assertNotNull(statementManager);
        Interface cepStatementsItf=(Interface)cepEngineComponent.getFcInterface("cepStatements");
        assertNotNull(cepStatementsItf);
        assertEquals(cepStatementsItf.getFcItfName(), "cepStatements");
        InterfaceType cepStatementsItfType=(InterfaceType) cepStatementsItf.getFcItfType();
        assertTrue(cepStatementsItfType.isFcCollectionItf());
        assertTrue(cepStatementsItfType.isFcClientItf());
        
        eventListenerManager=frascati.getService(cepEngineComponent, "cepEventListenerManager", EventListenerManagerItf.class);
        assertNotNull(eventListenerManager);
        Interface cepEventListenersItf=(Interface) cepEngineComponent.getFcInterface("cepEventListeners");
        assertNotNull(cepEventListenersItf);
        assertEquals(cepEventListenersItf.getFcItfName(), "cepEventListeners");
        InterfaceType cepEventListenersItfType=(InterfaceType) cepEventListenersItf.getFcItfType();
        assertTrue(cepEventListenersItfType.isFcCollectionItf());
        assertTrue(cepEventListenersItfType.isFcClientItf());
        
        Port cepEventListeners= introspection.getInterface("cepEngine-test/cepEventListeners");
        assertNotNull(cepEventListeners);
        assertEquals(Multiplicity._0N, cepEventListeners.getMutiplicity());
        
        Interface cepEngineItf;
        cepEventListenersItf=null;
        for(Object interfaceObject : cepEngineComponent.getFcInterfaces())
        {
            cepEngineItf=(Interface) interfaceObject;
            logger.info(cepEngineItf.getFcItfName());
            if(cepEngineItf.getFcItfName().equals("cepEventListeners"))
            {
                cepEventListenersItf=cepEngineItf;
            }
        }
        assertNotNull(cepEventListenersItf);
        
        eventTypeManager=frascati.getService(cepEngineComponent, "cepEventTypeManager", EventTypeManagerItf.class);
        assertNotNull(eventTypeManager);
        Interface cepEventTypesItf=(Interface) cepEngineComponent.getFcInterface("cepEventTypes");
        assertNotNull(cepEventTypesItf);
        assertEquals(cepEventTypesItf.getFcItfName(), "cepEventTypes");
        InterfaceType cepEventTypesItfType=(InterfaceType) cepEventTypesItf.getFcItfType();
        assertTrue(cepEventTypesItfType.isFcCollectionItf());
        assertTrue(cepEventTypesItfType.isFcClientItf());
        
        String bindingURI=System.getProperty("org.ow2.frascati.binding.uri.base");
        if(bindingURI==null)
        {
            bindingURI= AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;
        }
        logger.info("bindingURI "+bindingURI);
        remoteCepEngine = JAXRSClientFactory.create(bindingURI +"/CEPEngine", CEPEngineItf.class);
        remoteEventTypeManager= JAXRSClientFactory.create(bindingURI +"/EventTypeManager", EventTypeManagerItf.class);
    }
    
    @AfterClass
    public static void finish() throws FrascatiException
    {
        frascati.close();
    }
    
//    @Test
    public void engineTest() throws FrascatiException
    {
        SimpleMessage helloMessage=new SimpleMessage("hello", "the world");
        cepEngine.sendEvent(helloMessage);
        remoteCepEngine.stop();
        
        try
        {
            cepEngine.sendEvent(helloMessage);
            fail("An exception was occured");
        }
        catch(IllegalCEPLifeCycleException illegalCEPLifeCycleException)
        {
            assertEquals("sendEvent action can't be performed due to illegal lifecyle state", illegalCEPLifeCycleException.getMessage());
        }
        
        remoteCepEngine.start();
        
        /**send a new Event and check if received*/
        cepEngine.sendEvent(helloMessage);
        EventListenerTestItf simpleMessageListener=frascati.getService(cepEngineComponent, "eventListenerTest", EventListenerTestItf.class);
        Object lastUpdateObject= simpleMessageListener.getLastUpdate();
        assertEquals(SimpleMessage.class, lastUpdateObject.getClass());
        SimpleMessage lastUpdateMessage=(SimpleMessage) lastUpdateObject;
        assertEquals("hello", lastUpdateMessage.getId());
        assertEquals("the world", lastUpdateMessage.getContent());
        
        SimpleMessage byebyeMessage=new SimpleMessage("bye", "the world");
        cepEngine.sendEvent(byebyeMessage);
        lastUpdateObject= simpleMessageListener.getLastUpdate();
        assertEquals(SimpleMessage.class, lastUpdateObject.getClass());
        lastUpdateMessage=(SimpleMessage) lastUpdateObject;
        assertEquals("bye", lastUpdateMessage.getId());
        assertEquals("the world", lastUpdateMessage.getContent());
    }
    
    
//    @Test
    public void runtimeTest() throws ClassNotFoundException, CEPException
    {
        EventType eventType=new EventType();
        eventType.setEventType(EventAttType.CLASS);
        eventType.setValue("org.ow2.frascati.cep.eventType.RuntimeMessage");
        eventTypeManager.addEventType(eventType);
        
        try
        {
            eventType.setEventType(EventAttType.CLASS);
            eventType.setValue("org.ow2.frascati.cep.eventType.NotExistingEventType");
            eventTypeManager.addEventType(eventType);
            fail("An exception was occured");
        }
        catch(Exception classNotFoundException)
        {
        }
        
        RuntimeMessage runtimeMessage=new RuntimeMessage();
        cepEngine.sendEvent(runtimeMessage);
        
        Statement statement = new Statement();
        statement.setId("allRuntimeMessage");
        statement.setValue("select * from RuntimeMessage");
        statementManager.addStatement(statement);
        
        SimpleMessageListener runtimeEventListener=new SimpleMessageListener();
        eventListenerManager.registerListener(runtimeEventListener);
        eventListenerManager.addListener(runtimeEventListener.getId(), "allRuntimeMessage");
        
        runtimeMessage=new RuntimeMessage();
        cepEngine.sendEvent(runtimeMessage);
        
        Object runtimeMessageObject=runtimeEventListener.getLastUpdate();
        assertEquals(RuntimeMessage.class, runtimeMessageObject.getClass());
        assertEquals(runtimeMessage.getId(), ((RuntimeMessage)runtimeMessageObject).getId());
        
        RuntimeMessage statementRuntimeMessage=new RuntimeMessage();
        statement = new Statement();
        statement.setId("RuntimeMessage"+statementRuntimeMessage.getId());
        statement.setValue("select * from RuntimeMessage where id="+statementRuntimeMessage.getId());
        statementManager.addStatement(statement);
    }

//    @Test
    public void addTwoEventType() throws ClassNotFoundException, IOException
    {
        EventType eventType=new EventType();
        eventType.setEventType(EventAttType.CLASS);
        eventType.setValue("org.ow2.frascati.cep.eventType.RuntimeMessage");
        eventTypeManager.addEventType(eventType);
        
        File exempleAPIFile=getResource("frascati-cep-exemple-api.jar");
        String exempleAPIEncoded = FileUtil.getStringFromFile(exempleAPIFile);
        /**we first load frascati-cep-engine-exemple-api that contains interface definition*/
        deployment.loadLibrary(exempleAPIEncoded);
        
        /**we deploy frascati-cep-exemple contribution*/
        File exempleImplementationFile=getResource("frascati-cep-exemple.zip");
        String exempleImplementationEncoded=FileUtil.getStringFromFile(exempleImplementationFile);
        deployment.deployContribution(exempleImplementationEncoded);
        
        /**bind the new eventType service from the exemple*/
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("kind", "SCA");
        params.add("uri", "light-listener/lightEventType");
        introspection.addBinding("cepEngine-test/cepEventTypes", params);
        
        eventType=new EventType();
        eventType.setEventType(EventAttType.CLASS);
        eventType.setValue("org.ow2.frascati.cep.exemple.message.LightMessage");
        eventTypeManager.addEventType(eventType);
    }
    
    @Test
    public void runtimeDeploymentTest() throws IOException, CEPException, ClassNotFoundException
    {
        File exempleAPIFile=getResource("frascati-cep-exemple-api.jar");
        String exempleAPIEncoded = FileUtil.getStringFromFile(exempleAPIFile);
        /**we first load frascati-cep-engine-exemple-api that contains interface definition*/
        deployment.loadLibrary(exempleAPIEncoded);
        
        /**we deploy frascati-cep-exemple contribution*/
        File exempleImplementationFile=getResource("frascati-cep-exemple.zip");
        String exempleImplementationEncoded=FileUtil.getStringFromFile(exempleImplementationFile);
        deployment.deployContribution(exempleImplementationEncoded);
        
        /**bind the new eventType service from the exemple*/
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("kind", "SCA");
        params.add("uri", "light-listener/lightEventType");
        introspection.addBinding("cepEngine-test/cepEventTypes", params);
        
        /**bind the new statement service from the exemple*/
        params.clear();
        params.add("kind", "SCA");
        params.add("uri", "light-listener/allLightMessageStatement");
        introspection.addBinding("cepEngine-test/cepStatements", params);
        
        /**bind the new eventListener service from the exemple*/
        params.clear();
        params.add("kind", "SCA");
        params.add("uri", "light-listener/lightListener");
        introspection.addBinding("cepEngine-test/cepEventListeners", params);
        
        /** Send a LightMessage*/
        LightMessage zwaveLightMessage = new LightMessage("ZwaveLight","OFF",0);
        cepEngine.sendEvent(zwaveLightMessage);
        
        /**Create new statement about LightMessage*/
        Statement statement = new Statement();
        statement.setId("roomLightMessage");
        statement.setValue("select * from LightMessage where name = 'RoomLightMessage'");
        statementManager.addStatement(statement);
        
        SimpleMessageListener roomLightMessageListener=new SimpleMessageListener();
        eventListenerManager.registerListener(roomLightMessageListener);
        eventListenerManager.addListener(roomLightMessageListener.getId(), "roomLightMessage");
        
        LightMessage roomLightMessage = new LightMessage("RoomLightMessage","OFF",0);
        cepEngine.sendEvent(roomLightMessage);
        
        /**send zwaveLight message to be sure that it will not be received by the roomLightMessageListener*/
        cepEngine.sendEvent(zwaveLightMessage);
        
        Object lightMessageObject=roomLightMessageListener.getLastUpdate();
        assertEquals(LightMessage.class,lightMessageObject.getClass());
        assertEquals("RoomLightMessage", ((LightMessage)lightMessageObject).getName());
        assertEquals("OFF", ((LightMessage)lightMessageObject).getStatus());
        assertEquals(0, ((LightMessage)lightMessageObject).getConso());
        
        roomLightMessage.setStatus("ON");
        roomLightMessage.setConso(20);
        cepEngine.sendEvent(roomLightMessage);
        cepEngine.sendEvent(zwaveLightMessage);
        
        lightMessageObject=roomLightMessageListener.getLastUpdate();
        assertEquals(LightMessage.class,lightMessageObject.getClass());
        assertEquals("RoomLightMessage", ((LightMessage)lightMessageObject).getName());
        assertEquals("ON", ((LightMessage)lightMessageObject).getStatus());
        assertEquals(20, ((LightMessage)lightMessageObject).getConso());
    }
    
    /**********************Utils**********************/
    
    public static File getResource(String resourceName)
    {
        if(TEST_RESOURCES_DIR!=null)
        {
            File resource=new File(TEST_RESOURCES_DIR+resourceName);
            if(resource.exists())
            {
                return resource;
            }
        }
        return new File(DEFAULT_TEST_RESOURCES_DIR+resourceName);
    }
    
}
