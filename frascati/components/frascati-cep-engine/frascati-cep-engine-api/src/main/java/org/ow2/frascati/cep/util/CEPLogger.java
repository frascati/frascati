/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class CEPLogger
{
    private final static Logger logger = Logger.getLogger(CEPLogger.class.getName());
    
    private static final Level defaultLevel = Level.INFO;

    /***************************** Logger util methods ****************************/
    /**
     * Wrapper for java.util.logging.Logger.entering
     * 
     * @param method
     * @param params
     */
    public static void logEntering(Object o, String method, Object... params)
    {
        logMethod(o, "entering "+method, params);
    }

    public static void logExiting(Object o, String method, Object... params)
    {
        logMethod(o, "exiting "+method, params);
    }
    
    public static void logMethod(Object o, String method, Object... params)
    {
        StringBuilder messageBuilder=new StringBuilder();
        messageBuilder.append(method);
        for(Object param : params)
        {
            messageBuilder.append(" "+param);
        }
        log(o,messageBuilder.toString());
    }
    
    public static void log(Object o, Level level, String message)
    {
        String fullMessage = "[" + o.getClass().getSimpleName() + "] ";
        fullMessage += message;
        logger.log(level, fullMessage);
    }

    public static void log(Object o, String message)
    {
        log(o, defaultLevel, message);
    }
    
}
