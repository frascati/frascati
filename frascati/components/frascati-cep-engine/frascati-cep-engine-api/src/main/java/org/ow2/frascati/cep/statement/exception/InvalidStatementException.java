/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.statement.exception;

import org.ow2.frascati.cep.exception.CEPException;

import com.espertech.esper.client.EPException;

/**
 *
 */
public class InvalidStatementException extends CEPException
{

    /**
     * 
     */
    private static final long serialVersionUID = -5663361946952477143L;

    public InvalidStatementException(String statementId, String statement, EPException epException)
    {
        super("EPException while creating statement "+statement+" with id "+statementId, epException);
    }
    
    public InvalidStatementException(String statement, EPException epException)
    {
        super("EPException while creating statement "+statement, epException);
    }
    
}
