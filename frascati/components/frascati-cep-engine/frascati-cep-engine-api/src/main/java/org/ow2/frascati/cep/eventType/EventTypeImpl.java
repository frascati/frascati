/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.eventType;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.cep.EventAttType;
import org.ow2.frascati.cep.EventType;
import org.ow2.frascati.cep.eventType.EventTypeItf;

/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class EventTypeImpl implements EventTypeItf
{
    public EventAttType eventAttType;
    
    public String value;

    public EventTypeImpl(){}
    
    public EventTypeImpl(EventType eventType)
    {
        this.eventAttType=eventType.getEventType();
        this.value=eventType.getValue();
    }
    
    public EventAttType getEventAttType()
    {
        return eventAttType;
    }

    @Property(name="type")
    public void setEventAttType(String eventAttType)
    {
        this.eventAttType = EventAttType.valueOf(eventAttType);
    }

    public String getValue()
    {
        return value;
    }

    @Property(name="value")
    public void setValue(String value)
    {
        this.value = value;
    }
}
