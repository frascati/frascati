/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.statement;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.cep.Statement;
import org.ow2.frascati.cep.statement.StatementItf;
/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class StatementImpl implements StatementItf
{
public String id;
    
    public String text;
    
    /**Default constructor*/
    public StatementImpl(){}
    
    public StatementImpl(Statement statement)
    {
        this.id=statement.getId();
        this.text=statement.getValue();
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.cep.statement.StatementItf#getId()
     */
    public String getId()
    {
        return id;
    }

    @Property(name="id")
    public void setId(String id)
    {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.cep.statement.StatementItf#getText()
     */
    public String getText()
    {
        return text;
    }
    
    @Property(name="text")
    public void setText(String text)
    {
        this.text = text;
    }
    
    
}
