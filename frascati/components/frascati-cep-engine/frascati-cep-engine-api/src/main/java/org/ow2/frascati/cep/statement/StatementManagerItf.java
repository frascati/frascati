/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.statement;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.ow2.frascati.cep.Statement;
import org.ow2.frascati.cep.statement.exception.AlreadyRegisteredStatementException;
import org.ow2.frascati.cep.statement.exception.InvalidStatementException;
import org.ow2.frascati.cep.statement.exception.NotRegisteredStatementException;

/**
 * Interface to manage statement of the CEP Engine
 */
public interface StatementManagerItf
{
    /**
     * @param statementId id of the searched statement
     * @return The statement with id statementId
     * @throws NotRegisteredStatementException if the statement can not be found
     */
    @GET
    @Path("/statement/{id}")
    @Produces({"application/xml", "application/json"})
    public Statement getStatement(@PathParam("id")String statementId) throws NotRegisteredStatementException;

    /**
     * @return A list of all registered statements
     */
    @GET
    @Path("/statements")
    @Produces({"application/xml", "application/json"})
    public List<Statement> getStatements();
    
    /**
     * Register an anonymous statement in the Engine
     * 
     * @param statement the statement to add
     * @return the id of the the created statement 
     * @throws InvalidStatementException if the content of the statement definition si invalid
     */
    @POST
    @Path("/addAnonymousStatement")
    @Produces({"application/xml", "application/json"})
    public Statement addStatement(@PathParam("statement")String statement) throws InvalidStatementException;

    /**
     * Register a new statement in the Engine
     * 
     * @param statementId id of the new statement
     * @param statement the text of the statement
     * @throws AlreadyRegisteredStatementException if an existing statement with id statementId is found
     * @throws InvalidStatementException if the statement text is invalid
     */
    @POST
    @Path("/addStatement/{id}")
    public void addStatement(@PathParam("id")String statementId, @QueryParam("statement")String statement) throws AlreadyRegisteredStatementException, InvalidStatementException;

    /**
     * Register a new statement in the Engine
     * 
     * @param statement the Statement to register
     * @throws AlreadyRegisteredStatementException if an existing statement with id statementId is found
     * @throws InvalidStatementException if the statement text is invalid
     */
    @POST
    @Path("/addStatement")
    @Consumes({"application/xml", "application/json"})
    public void addStatement(Statement statement) throws AlreadyRegisteredStatementException, InvalidStatementException;

    /**
     * Update a statement text in the Engine
     * 
     * @param statementId id of the statement to update
     * @param statement the new text for the statement
     * @throws NotRegisteredStatementException  if the statement can not be found
     * @throws InvalidStatementException if the statement text is invalid
     */
    @PUT
    @Path("/updateStatement/{id}")
    public void updateStatement(@PathParam("id")String statementId, @QueryParam("statement") String statement) throws NotRegisteredStatementException, InvalidStatementException;

    /**
     * Update a statement text in the Engine
     * 
     * @param statement the statement to update
     * @throws NotRegisteredStatementException if the statement can not be found
     * @throws InvalidStatementException if the statement text is invalid
     */
    @PUT
    @Path("/updateStatement")
    @Consumes({"application/xml", "application/json"})
    public void updateStatement(Statement statement) throws NotRegisteredStatementException, InvalidStatementException;

    /**
     * Remove a statement from the CEP Engine
     * 
     * @param statementId the id of the statement to remove
     * @throws NotRegisteredStatementException if the statement can not be found
     */
    @DELETE
    @Path("/removeStatement/{id}")
    public void removeStatement(String statementId) throws NotRegisteredStatementException;

    /**
     * Remove a statement from the CEP Engine
     * 
     * @param statement the statement to remove
     * @throws NotRegisteredStatementException if the statement can not be found
     */
    @DELETE
    @Path("/removeStatement")
    @Consumes({"application/xml", "application/json"})
    public void removeStatement(Statement statement) throws NotRegisteredStatementException;
}
