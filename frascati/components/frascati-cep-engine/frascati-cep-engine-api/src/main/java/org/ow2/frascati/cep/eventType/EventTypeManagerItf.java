/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep.eventType;

import java.net.MalformedURLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.cep.EventType;

/**
 * Interface for EventTypeManager
 */
@Service
public interface EventTypeManagerItf
{
    /**
     * Add a new EventType to the CEPEngine configuration
     * This operation need the engine to restarts
     * 
     * @param eventType the new EventType definition
     * @throws ClassNotFoundException if the EventType is a class that can not be found in the Engine ClassLoader
     */
    @POST
    @Path("/addEventType")
    @Consumes({"application/xml", "application/json"})
    @Produces({"text/plain"})
    public void addEventType(EventType eventType) throws ClassNotFoundException;
    
    @POST
    @Path("/addXSDEventType")
    @Consumes({"application/xml", "application/json"})
    @Produces({"text/plain"})
    public void addXSDEventType(@QueryParam("xsdFileName")String xsdFileName,@QueryParam("rootElementName") String rootElementName) throws MalformedURLException;
    
}
