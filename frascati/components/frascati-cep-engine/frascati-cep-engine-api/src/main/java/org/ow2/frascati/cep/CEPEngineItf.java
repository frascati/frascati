/**
 * OW2 FraSCAti : FraSCAti CEP Engine API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.cep;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;


/**
 * Interface for FraSCAti CEPEngine
 */
@Service
public interface CEPEngineItf
{
    /**
     * Start the engine and the associated core managers
     */
    @POST
    @Path("/start")
    public void start();
    
    
    /**
     * Stop the engine and the associated core managers
     */
    @POST
    @Path("/stop")
    public void stop();
    
    /**
     * Get the state of the enfine
     * 
     * @return true if the engine is started, false otherwise
     */
    @GET
    @Path("/isStarted")
    @Produces({"text/plain"})
    public boolean isStarted();
    
    
    /**
     * Send a CEP event 
     * 
     * @param event the event Object to send
     */
    public void sendEvent(Object event);
    
    /**
     * Send a CEP event in a separate Thread
     * 
     * @param event the event Object to send
     */
    public void sendAsynchronousEvent(Object event);
}
