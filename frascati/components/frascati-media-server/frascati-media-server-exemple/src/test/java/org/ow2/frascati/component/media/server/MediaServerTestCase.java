/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.fractal.upnp.client.CommonClientUPnPService;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.UDN;
import org.teleal.cling.support.contentdirectory.DIDLParser;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.container.Container;
import org.teleal.cling.support.model.item.AudioItem;
import org.teleal.cling.support.model.item.ImageItem;
import org.teleal.cling.support.model.item.Item;
import org.teleal.cling.support.model.item.VideoItem;
import org.teleal.common.util.MimeType;
/**
 *
 */
@SuppressWarnings("rawtypes")
public class MediaServerTestCase extends FraSCAtiTestCase
{
   private static Service contentDirectoryService;
   
   private static List<MimeType> imageMimeTypes;
   private static List<MimeType> audioMimeTypes;
   private static List<MimeType> videoMimeTypes;
   
   
   @BeforeClass
   public static void initTest()
   {
       imageMimeTypes = new ArrayList<MimeType>();
       imageMimeTypes.add(new MimeType("image", "jpeg"));
       imageMimeTypes.add(new MimeType("image", "png"));
       audioMimeTypes = new ArrayList<MimeType>();
       audioMimeTypes.add(new MimeType("audio", "mpeg"));
       audioMimeTypes.add(new MimeType("audio", "x-wav"));
       videoMimeTypes = new ArrayList<MimeType>();
       videoMimeTypes.add(new MimeType("video", "mp4"));
       videoMimeTypes.add(new MimeType("video", "x-msvideo"));
       videoMimeTypes.add(new MimeType("video", "x-flv"));
   }
   
    @Test
    public void testSuite() throws Exception
    {
        contentDirectoryService = CommonClientUPnPService.getInstance().getUPnPService("FraSCAtiMediaServer", "MediaServer", "", "ContentDirectory");
        assertEquals(UDN.valueOf("FraSCAtiMediaServer"), contentDirectoryService.getDevice().getIdentity().getUdn());
        this.metadataRequestTest();
        this.browseRootRequestTest();
        this.browseMediaLocationRequestTest();
        this.browseMediaRequestTest();
    }

    public void metadataRequestTest() throws Exception
    {
        ContentDirectoryActionCallBack contentDirectoryActionCallBack = this.getBrowseActionCallBack("0", "BrowseMetadata");
        contentDirectoryActionCallBack.run();
        
        assertTrue(contentDirectoryActionCallBack.isActionCallBackSucessed());
        ActionInvocation actionInvocation = contentDirectoryActionCallBack.getActionInvocation();
        assertEquals("1", actionInvocation.getOutput("BrowseCount").getValue().toString());
        assertEquals("1", actionInvocation.getOutput("BrowseTotalMatches").getValue().toString());
        DIDLParser didlParser = new DIDLParser();
        DIDLContent didlContent = didlParser.parse(actionInvocation.getOutput("BrowseResult").getValue().toString());
        assertEquals(1, didlContent.getContainers().size());
        Container rootContainer = didlContent.getFirstContainer();
        assertEquals("0", rootContainer.getId());
        assertEquals("-1", rootContainer.getParentID());
        assertEquals("root", rootContainer.getTitle());
    }
    
    public void browseRootRequestTest() throws Exception
    {
        ContentDirectoryActionCallBack contentDirectoryActionCallBack = this.getBrowseActionCallBack("0", "BrowseDirectChildren");
        contentDirectoryActionCallBack.run();
        
        assertTrue(contentDirectoryActionCallBack.isActionCallBackSucessed());
        ActionInvocation actionInvocation = contentDirectoryActionCallBack.getActionInvocation();
        assertEquals("3", actionInvocation.getOutput("BrowseCount").getValue().toString());
        assertEquals("3", actionInvocation.getOutput("BrowseTotalMatches").getValue().toString());
        DIDLParser didlParser = new DIDLParser();
        DIDLContent didlContent = didlParser.parse(actionInvocation.getOutput("BrowseResult").getValue().toString());
        assertEquals(3, didlContent.getContainers().size());
    }
    
    public void browseMediaLocationRequestTest() throws Exception
    {
        ContentDirectoryActionCallBack contentDirectoryActionCallBack = this.getBrowseActionCallBack("music", "BrowseDirectChildren");
        contentDirectoryActionCallBack.run();

        assertTrue(contentDirectoryActionCallBack.isActionCallBackSucessed());
        ActionInvocation actionInvocation = contentDirectoryActionCallBack.getActionInvocation();
        assertEquals("11", actionInvocation.getOutput("BrowseCount").getValue().toString());
        assertEquals("11", actionInvocation.getOutput("BrowseTotalMatches").getValue().toString());
        DIDLParser didlParser = new DIDLParser();
        String browseResult = actionInvocation.getOutput("BrowseResult").getValue().toString();
        DIDLContent didlContent = didlParser.parse(browseResult);
        assertEquals(11, didlContent.getContainers().size());
        assertEquals(0, didlContent.getItems().size());
    }
    
    public void browseMediaRequestTest() throws Exception
    {
        ContentDirectoryActionCallBack contentDirectoryActionCallBack = this.getBrowseActionCallBack("music|%5CupnpTest", "BrowseDirectChildren");
        contentDirectoryActionCallBack.run();

        assertTrue(contentDirectoryActionCallBack.isActionCallBackSucessed());
        ActionInvocation actionInvocation = contentDirectoryActionCallBack.getActionInvocation();
        assertEquals("7", actionInvocation.getOutput("BrowseCount").getValue().toString());
        assertEquals("7", actionInvocation.getOutput("BrowseTotalMatches").getValue().toString());
        DIDLParser didlParser = new DIDLParser();
        String browseResult = actionInvocation.getOutput("BrowseResult").getValue().toString();
        DIDLContent didlContent = didlParser.parse(browseResult);
        assertEquals(0, didlContent.getContainers().size());
        assertEquals(7, didlContent.getItems().size());
        
        Res itemResource;
        MimeType itemMimeType;
        for(Item item : didlContent.getItems())
        {
            assertEquals(1, item.getResources().size());
            itemResource = item.getFirstResource();
            FraSCAtiTestUtils.assertURLExist(true, itemResource.getValue());
            itemMimeType = itemResource.getProtocolInfo().getContentFormatMimeType();
            if(item instanceof AudioItem)
            {
                assertTrue(audioMimeTypes.contains(itemMimeType));
            }
            else if(item instanceof ImageItem)
            {
                assertTrue(imageMimeTypes.contains(itemMimeType));
            }
            else if(item instanceof VideoItem)
            {
                assertTrue(videoMimeTypes.contains(itemMimeType));
            }
        }
    }
    
    private ContentDirectoryActionCallBack getBrowseActionCallBack(String objectID, String browseFlag)
    {
        return getBrowseActionCallBack(contentDirectoryService, objectID, browseFlag);
    }
    
    @SuppressWarnings("unchecked")
    private ContentDirectoryActionCallBack getBrowseActionCallBack(Service upnpService, String objectID, String browseFlag)
    {
        Action browseAction = upnpService.getAction("Browse");
        ActionInvocation mActionInvocation = new ActionInvocation(browseAction);
        mActionInvocation.setInput("ObjectID", objectID);
        mActionInvocation.setInput("BrowseFlag", browseFlag);
        mActionInvocation.setInput("StartingIndex", "0");
        mActionInvocation.setInput("RequestedCount", "0");
        return new ContentDirectoryActionCallBack(mActionInvocation);
    }
    
    private class ContentDirectoryActionCallBack extends ActionCallback
    {
        private boolean isActionCallBackSucessed;
        private ActionInvocation actionInvocation;
        
        protected ContentDirectoryActionCallBack(ActionInvocation actionInvocation)
        {
            super(actionInvocation,CommonClientUPnPService.getInstance().getControlPoint());
        }

        @Override
        public void success(ActionInvocation invocation)
        {
            this.isActionCallBackSucessed=true;
            this.actionInvocation=invocation;
        }

        @Override
        public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg)
        {
            System.out.println(operation.getResponseDetails());
            this.isActionCallBackSucessed=false;
        }

        public boolean isActionCallBackSucessed()
        {
            return this.isActionCallBackSucessed;
        }

        public ActionInvocation getActionInvocation()
        {
            return this.actionInvocation;
        }
    }
}


