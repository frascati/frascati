/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.provider.resource;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.component.media.server.MediaLocation;
import org.ow2.frascati.component.media.server.MediaLocations;
import org.ow2.frascati.component.media.server.exception.ContentNotFoundException;
import org.ow2.frascati.component.media.server.exception.InvalidObjectIdException;

/**
 *
 */
@Scope("COMPOSITE")
public class ResourceProviderImpl implements ResourceProviderItf
{
    private final static Logger logger = Logger.getLogger(ResourceProviderImpl.class.getName());
    
    public final static String OBJECT_ID_SEPARATOR = "|";
    private final static String OBJECT_ID_SPLITTER = "\\"+OBJECT_ID_SEPARATOR;
    
    @Property(name="enableCache")
    private boolean enableCache;
    
    private Map<String, ResourceBundle> resourceCache;
    
    private Map<String, File> mediaLocations;
    
    public ResourceProviderImpl()
    {
        this.enableCache = true;
        this.resourceCache = new HashMap<String, ResourceBundle>();
    }
    
    @Property(name = "mediaLocations")
    public void setMediaLocations(MediaLocations mediaLocations)
    {
        this.mediaLocations = new HashMap<String, File>();
        File mediaLocationDirectory;
        for(MediaLocation mediaLocation : mediaLocations.getMediaLocations())
        {
            if(this.mediaLocations.containsKey(mediaLocation.getName()))
            {
                logger.severe("Duplicate media location name : "+mediaLocation.getName());
                continue;
            }
            
            mediaLocationDirectory = new File(mediaLocation.getValue());
            if(!mediaLocationDirectory.exists() || !mediaLocationDirectory.isDirectory())
            {
                logger.severe("Directory "+mediaLocation.getValue()+" does not exist or is not a directory");
                continue;
            }
            
            logger.info("UPNP Media Server add media location : "+mediaLocation.getName()+" : "+ mediaLocationDirectory);
            this.mediaLocations.put(mediaLocation.getName(), mediaLocationDirectory);
        }
    }
    

    public ResourceBundle getResourceBundle(String resourceId) throws ContentNotFoundException, UnsupportedEncodingException, InvalidObjectIdException
    {
        logger.fine("[getResourceBundle] resourceId : "+resourceId);
        
        ResourceBundle resourceBundle = null;
        if(this.enableCache)
        {
            resourceBundle = this.resourceCache.get(resourceId);
        }
        
        if(resourceBundle != null)
        {
            return resourceBundle;
        }
        
        resourceBundle = new ResourceBundle();
        resourceBundle.setResourceId(resourceId);
        
        String[] objectIdParts = resourceId.split(OBJECT_ID_SPLITTER);
        if(objectIdParts.length > 2)
        {
            throw new InvalidObjectIdException(resourceId);
        }
        
        String resourceMedia = objectIdParts[0];
        logger.fine("[getResourceBundle] resourceMedia : "+resourceMedia);
        resourceBundle.setResourceMedia(resourceMedia);
        
        File mediaLocation = mediaLocations.get(resourceMedia);
        if(mediaLocation==null)
        {
          throw new ContentNotFoundException("No media Location found for "+resourceMedia);
        }
        
        File resourceFile;
        if(objectIdParts.length == 1)
        {
            resourceFile=mediaLocation;
        }
        else
        {
            String resourceFilePath = URLDecoder.decode(objectIdParts[1], "UTF-8");
            resourceFile = new File(mediaLocation, resourceFilePath);
            if(!resourceFile.exists())
            {
                throw new ContentNotFoundException("No resource "+resourceFilePath+" found in media location "+resourceMedia);
            }
        }
        resourceBundle.setResourceFile(resourceFile);
        logger.fine("[getResourceBundle] resource File : "+resourceFile.getPath());
        return resourceBundle;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.component.media.resourceProvider.ResourceProviderItf#getMediaResources()
     */
    public List<String> getMediaResources()
    {
        return new ArrayList<String>(this.mediaLocations.keySet());
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.component.media.resourceProvider.ResourceProviderItf#getContentResourceId(org.ow2.frascati.component.media.resourceProvider.ResourceBundle, java.io.File)
     */
    public String getContentResourceId(ResourceBundle parentBundle, File contentFile) throws UnsupportedEncodingException
    {
        StringBuilder resourceObjectIdBuilder = new StringBuilder();
        resourceObjectIdBuilder.append(parentBundle.getResourceMedia());
        resourceObjectIdBuilder.append(OBJECT_ID_SEPARATOR);
        
        String medialLocationPath=this.mediaLocations.get(parentBundle.getResourceMedia()).getPath();
        String fileResourcePath = contentFile.getPath();
        String fileResourceRelativePath = fileResourcePath.substring(medialLocationPath.length());
        String resourcePath = fileResourceRelativePath.replace(File.separatorChar, '/');
        String encodedResourcePath = URLEncoder.encode(resourcePath, "UTF-8");
        resourceObjectIdBuilder.append(encodedResourcePath);
        return resourceObjectIdBuilder.toString();
    }
}
