/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.server;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.processor.AbstractBindingProcessor;
import org.ow2.frascati.component.media.provider.item.MediaItemProviderItf;
import org.ow2.frascati.component.media.provider.resource.ResourceBundle;
import org.ow2.frascati.component.media.provider.resource.ResourceProviderItf;
import org.ow2.frascati.component.media.server.exception.ContentNotFoundException;
import org.ow2.frascati.component.media.server.exception.InvalidObjectIdException;
import org.ow2.frascati.upnp.common.Browse;
import org.ow2.frascati.upnp.common.UPnPDeviceContentDirectoryItf;
import org.teleal.cling.support.contentdirectory.ContentDirectoryErrorCode;
import org.teleal.cling.support.contentdirectory.ContentDirectoryException;
import org.teleal.cling.support.contentdirectory.DIDLParser;
import org.teleal.cling.support.model.BrowseFlag;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.DIDLObject.Class;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.WriteStatus;
import org.teleal.cling.support.model.container.Container;
import org.teleal.cling.support.model.item.Item;

@Scope("COMPOSITE")
public class MediaServerImpl implements UPnPDeviceContentDirectoryItf, MediaServerItf
{
    private final static Logger logger = Logger.getLogger(MediaServerImpl.class.getName());

    private final static String ROOT_CONTAINER_ID="0";
    
    private String baseResourceURI;
    
    @Reference(name="mediaItemProviders")
    private List<MediaItemProviderItf<?>> mediaItemProviders;
    
    @Reference(name="resourceProvider")
    private ResourceProviderItf resourceProvider;
    
    @Property(name="mediaProviderURI")
    public void setMediaProviderURI(String mediaProviderURI)
    {
        this.baseResourceURI=mediaProviderURI;
        if(mediaProviderURI != null && mediaProviderURI.startsWith("/"))
        {
            this.baseResourceURI = System.getProperty(AbstractBindingProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingProcessor.BINDING_URI_BASE_DEFAULT_VALUE) + mediaProviderURI;
        }
    }
    
    public Browse browse(String objectID, String BrowseFlagString, String Filter, int StartingIndex, int RequestedCount, String SortCriteria) throws ContentDirectoryException
    {
        try
        {
            int count=0;
            int totalMatch = 0;
            DIDLContent didl = new DIDLContent();
            BrowseFlag browseFlag = BrowseFlag.valueOrNullOf(BrowseFlagString);
            
            if(browseFlag.equals(BrowseFlag.METADATA))
            {
                /**Root Metadata request send root container definition*/
//                if(objectID == null || objectID.equals("") || objectID.equals(ROOT_CONTAINER_ID))
//                {
                    logger.info("[Browse Action] Metadata request, objectId "+objectID);
                    Container rootContainer = new Container();
                    rootContainer.setId(ROOT_CONTAINER_ID);
                    rootContainer.setParentID("-1");
                    rootContainer.setTitle("root");
                    rootContainer.setSearchable(false);
                    rootContainer.setRestricted(true);
                    rootContainer.setClazz(new Class("object.container.storageFolder"));
                    rootContainer.setWriteStatus(WriteStatus.NOT_WRITABLE);
                    rootContainer.setChildCount(0);
                    didl.addContainer(rootContainer);
                    count=1;
                    totalMatch = 1;
//                }
//                else
//                {
//                    childCount = browseMetadata(objectID, didl);
//                }
            }
            else
            {
                if(objectID.equals(ROOT_CONTAINER_ID))
                {
                    logger.info("[Browse Action] Root container request");
                    Container mediaContainer;
                    for(String mediaLocationName : this.resourceProvider.getMediaResources())
                    {
                        mediaContainer = new Container();
                        mediaContainer.setParentID(ROOT_CONTAINER_ID);
                        mediaContainer.setId(mediaLocationName);
                        mediaContainer.setTitle(mediaLocationName);
                        mediaContainer.setSearchable(false);
                        mediaContainer.setRestricted(false);
                        mediaContainer.setWriteStatus(WriteStatus.NOT_WRITABLE);
                        mediaContainer.setClazz(new Class("object.container.storageFolder"));
                        didl.addContainer(mediaContainer);
                        totalMatch++;
                    }
                }
                else
                {
                    totalMatch = browseDirectChildren(objectID, didl);
                }
            }
            
            Browse browseResult = new Browse();
            browseResult.setCount(count);
            browseResult.setTotalMatches(totalMatch);
            browseResult.setResult(new DIDLParser().generate(didl));
            return browseResult;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new ContentDirectoryException(ContentDirectoryErrorCode.CANNOT_PROCESS, ex.toString());
        }
    }
    
    private int browseMetadata(String objectId, DIDLContent didl) throws UnsupportedEncodingException, ContentNotFoundException, InvalidObjectIdException
    {
        logger.info("[BrowseMetadata Action] Resource : "+objectId);
        ResourceBundle resourceBundle = this.resourceProvider.getResourceBundle(objectId);
        logger.info("[BrowseMetadata Action] File resource : "+resourceBundle.getResourceFile().getPath());
        
        File resourceFile = resourceBundle.getResourceFile();
        if(resourceFile.isDirectory())
        {
            Container directoryContainer = new Container();
            directoryContainer.setId(objectId);
            directoryContainer.setParentID("-1");
            directoryContainer.setRestricted(true);
            directoryContainer.setWriteStatus(WriteStatus.NOT_WRITABLE);
            directoryContainer.setClazz(new Class("object.container.storageFolder"));
            directoryContainer.setTitle(resourceFile.getName());
            didl.addContainer(directoryContainer);
        }
        else
        {
            
        }
        
        return 1;
    }
    
    private int browseDirectChildren(String objectId, DIDLContent didl) throws ContentNotFoundException, InvalidObjectIdException, UnsupportedEncodingException
    {
        logger.info("[BrowseDirectChildren Action] Resource : "+objectId);
        ResourceBundle resourceBundle = this.resourceProvider.getResourceBundle(objectId);
        logger.info("[BrowseDirectChildren Action] File resource : "+resourceBundle.getResourceFile().getPath());
        
        int childCount=0;
        Item item;
        for(File resourceContentFile : resourceBundle.getResourceFile().listFiles())
        {
            if(resourceContentFile.isDirectory())
            {
                didl.addContainer(this.getDirectoryContainer(resourceBundle, resourceContentFile));
                childCount++;
            }
            else
            {
                item=this.getFileItem(resourceBundle, resourceContentFile);
                if(item!=null)
                {
                    didl.addItem(item);
                    childCount++;
                }
            }
        }
        return childCount;
    }
    
    
    private Container getDirectoryContainer(ResourceBundle resourceBundle, File directoryResource) throws UnsupportedEncodingException
    {
        Container directoryContainer = new Container();
        directoryContainer.setParentID(resourceBundle.getResourceId());
        directoryContainer.setId(this.resourceProvider.getContentResourceId(resourceBundle, directoryResource));
        directoryContainer.setTitle(directoryResource.getName());
        directoryContainer.setSearchable(false);
        directoryContainer.setRestricted(false);
        directoryContainer.setWriteStatus(WriteStatus.NOT_WRITABLE);
        directoryContainer.setClazz(new Class("object.container.storageFolder"));
        return directoryContainer;
    }
    
    private Item getFileItem(ResourceBundle resourceBundle, File fileResource) throws UnsupportedEncodingException
    {
        for(MediaItemProviderItf<?> mediaProvider : this.mediaItemProviders)
        {
            if(mediaProvider.isItemResourceFile(fileResource))
            {
                Item item = mediaProvider.getItem(fileResource);
                String resourceId = this.resourceProvider.getContentResourceId(resourceBundle, fileResource);
                item.setId(resourceId);
                item.setParentID(resourceBundle.getResourceId());
                
                Res itemResource = item.getFirstResource();
                String resourceURL=baseResourceURI+"/"+resourceId;
                itemResource.setValue(resourceURL);

                return item;
            }
        }
        return null;
    }
    
    public MediaBrowse browseMedia(String browseMediaId) throws Exception
    {
        Browse browse = this.browse(browseMediaId, BrowseFlag.DIRECT_CHILDREN.toString(), "", 0, 0, "");
        MediaBrowse mediaBrowse = new MediaBrowse();
        mediaBrowse.setId(browseMediaId);
        mediaBrowse.setParentId(browseMediaId);
        
        DIDLParser didlParser = new DIDLParser();
        DIDLContent didlContent = didlParser.parse(browse.getResult());
        
        MediaDirectoryBrowses mediaDirectoryBrowses= new MediaDirectoryBrowses();
        MediaDirectoryBrowse mediaDirectoryBrowse;
        for(Container container : didlContent.getContainers())
        {
            mediaDirectoryBrowse = new MediaDirectoryBrowse();
            mediaDirectoryBrowse.setId(container.getId());
            mediaDirectoryBrowse.setName(container.getTitle());
            mediaDirectoryBrowses.getMediaDirectoryBrowses().add(mediaDirectoryBrowse);
        }
        mediaBrowse.setMediaDirectoryBrowses(mediaDirectoryBrowses);
        
        MediaItemBrowses mediaItemBrowses = new MediaItemBrowses();
        MediaItemBrowse mediaItemBrowse;
        for(Item item : didlContent.getItems())
        {
            mediaItemBrowse = new MediaItemBrowse();
            mediaItemBrowse.setId(item.getId());
            mediaItemBrowse.setName(item.getTitle());
            Res itemResource = item.getFirstResource();
            mediaItemBrowse.setUri(itemResource.getValue());
            mediaItemBrowses.getMediaItemBrowses().add(mediaItemBrowse);
        }
        mediaBrowse.setMediaItemBrowses(mediaItemBrowses);
        
        return mediaBrowse;
    }
    
    public String getSearchCapabilities()
    {
        return null;
    }

    public String getSortCapabilities()
    {
        return null;
    }

    public String getSystemUpdateID()
    {
        return "0";
    }
}


//<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"> <container id="2|C%3A%5CUsers%5CPublic%5CMusic%5CSample%20Music" searchable="0" restricted="0" parentID="0">
//<dc:title>Sample Music</dc:title>
//<dc:date>2009-07-14T07:32:38</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic" searchable="0" restricted="0" parentID="0">
//<dc:title>music</dc:title>
//<dc:date>2011-12-29T14:55:04</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="4|C%3A%5CUsers%5CPublic%5CPictures%5CSample%20Pictures" searchable="0" restricted="0" parentID="0">
//<dc:title>Sample Pictures</dc:title>
//<dc:date>2011-10-17T11:15:18</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="4|E%3A%5Cmusic" searchable="0" restricted="0" parentID="0">
//<dc:title>music</dc:title>
//<dc:date>2011-12-29T14:55:04</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="23|http%3A%2F%2Ffreemiplayer.free.fr%2Fdownload%2Fpodcasts.xml" searchable="0" restricted="0" parentID="0">
//<dc:title>Podcasts</dc:title>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> </DIDL-Lite>

//<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"> <container id="2|E%3A%5Cmusic%5C16%20GONG%20Albums" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>16 GONG Albums</dc:title>
//<dc:date>2013-03-13T15:49:58</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CAmo" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>Amo</dc:title>
//<dc:date>2012-04-25T11:03:34</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5Cdisney" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>disney</dc:title>
//<dc:date>2011-12-30T14:49:33</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5Cdivers%20xav" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>divers xav</dc:title>
//<dc:date>2012-01-16T09:22:15</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5Cdub" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>dub</dc:title>
//<dc:date>2011-12-30T14:53:22</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CGhost.Dog-The.Way.Of.The.Samurai.1CD.2000.Soundtrack.%5BWmC%5D" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>Ghost.Dog-The.Way.Of.The.Samurai.1CD.2000.Soundtrack.[WmC]</dc:title>
//<dc:date>2013-03-13T15:51:51</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CMeTaPuchKa_Si%20tout%20%C3%A9tait%20%C3%A0%20refaire_2012_lamouetteproduction" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>MeTaPuchKa_Si tout �tait � refaire_2012_lamouetteproduction</dc:title>
//<dc:date>2012-11-19T10:56:04</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CMos%20Def%20-%20Complete%20Discography" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>Mos Def - Complete Discography</dc:title>
//<dc:date>2013-03-14T09:44:55</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5COl'%20Dirty%20Bastard%20Discography%20%40%20320%20(6%20Albums%2B1)(RAP)(by%20dragan09)" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>Ol' Dirty Bastard Discography @ 320 (6 Albums+1)(RAP)(by dragan09)</dc:title>
//<dc:date>2013-03-14T09:18:57</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CSkip_and_Die_-_Riots_in_the_Jungle-CD-2012-TALiON" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>Skip_and_Die_-_Riots_in_the_Jungle-CD-2012-TALiON</dc:title>
//<dc:date>2013-02-14T12:14:35</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic%5CupnpTest" searchable="0" restricted="0" parentID="2|E%3A%5Cmusic">
//<dc:title>upnpTest</dc:title>
//<dc:date>2013-09-24T20:31:56</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> </DIDL-Lite>

//<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"> <item id="0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Collie%20Herb%20Man.mp3%2F0" restricted="0" parentID="2|E%3A%5Cmusic%5CupnpTest">
//<dc:title>Katchafire - Collie Herb Man.mp3</dc:title>
//<dc:date>2013-09-25T16:09:43</dc:date>
//<upnp:class>object.item.audioItem.musicTrack</upnp:class>
//<res size="3753830" protocolInfo="http-get:*:audio/mpeg:*">http://192.168.0.25:62182/FreeMi/0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Collie%20Herb%20Man.mp3%2F0</res>
//</item> <item id="0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Collie%20Herb.mp3%2F0" restricted="0" parentID="2|E%3A%5Cmusic%5CupnpTest">
//<dc:title>Katchafire - Collie Herb.mp3</dc:title>
//<dc:date>2013-09-25T16:09:43</dc:date>
//<upnp:class>object.item.audioItem.musicTrack</upnp:class>
//<res size="3034819" protocolInfo="http-get:*:audio/mpeg:*">http://192.168.0.25:62182/FreeMi/0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Collie%20Herb.mp3%2F0</res>
//</item> <item id="0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Colour%20Me%20Life.mp3%2F0" restricted="0" parentID="2|E%3A%5Cmusic%5CupnpTest">
//<dc:title>Katchafire - Colour Me Life.mp3</dc:title>
//<dc:date>2013-09-24T20:32:14</dc:date>
//<upnp:class>object.item.audioItem.musicTrack</upnp:class>
//<res size="3212735" protocolInfo="http-get:*:audio/mpeg:*">http://192.168.0.25:62182/FreeMi/0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Colour%20Me%20Life.mp3%2F0</res>
//</item> </DIDL-Lite>


