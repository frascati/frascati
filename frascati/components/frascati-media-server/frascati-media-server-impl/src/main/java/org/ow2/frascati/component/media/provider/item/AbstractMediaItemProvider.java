/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.provider.item;

import java.io.File;

import org.osoa.sca.annotations.Property;
import org.ow2.frascati.component.media.provider.resource.ResourceBundle;
import org.ow2.frascati.component.media.server.MimeType;
import org.ow2.frascati.component.media.server.MimeTypes;
import org.teleal.cling.support.model.ProtocolInfo;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.item.Item;

/**
 *
 */
public abstract class AbstractMediaItemProvider<T extends Item> implements MediaItemProviderItf<T>
{
    @Property(name="mimeTypes")
    private MimeTypes mimeTypes;
    
    /**
     * @see org.ow2.frascati.component.media.provider.item.MediaItemProviderItf#isItemResourceFile(java.io.File)
     */
    public boolean isItemResourceFile(File fileResource)
    {
        String mimeTypeFileResource = ResourceBundle.getFileMIMEType(fileResource);
        String mimeTypeString;
        for(MimeType mimeType : this.mimeTypes.getMimeTypes())
        {
            mimeTypeString = mimeType.getSubType()+"/"+mimeType.getType();
            if(mimeTypeFileResource.equals(mimeTypeString))
            {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @see org.ow2.frascati.component.media.provider.MediaProviderItf#getItem(java.io.File)
     */
    public T getItem(File fileResource)
    {
        T item = this.instanciateItem();
        item.setTitle(fileResource.getName());

        Res resource = new Res();
        resource.setSize(fileResource.length());
        
        String resourceMimeType = ResourceBundle.getFileMIMEType(fileResource);
        String[] mimeTypeSplitted = resourceMimeType.split("/");
        org.teleal.common.util.MimeType mimeType = new org.teleal.common.util.MimeType(mimeTypeSplitted[0], mimeTypeSplitted[1]);
        ProtocolInfo protocolInfo = new ProtocolInfo(mimeType);
        resource.setProtocolInfo(protocolInfo);
        
        item.addResource(resource);
        return item;
    }
    
    protected abstract T instanciateItem();
}
