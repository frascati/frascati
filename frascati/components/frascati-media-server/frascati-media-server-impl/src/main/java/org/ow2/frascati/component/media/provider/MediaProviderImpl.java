/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.provider;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.component.media.provider.resource.ResourceBundle;
import org.ow2.frascati.component.media.provider.resource.ResourceProviderItf;

/**
 *
 */
@Scope("COMPOSITE")
public class MediaProviderImpl extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    @Reference(name="resourceProvider")
    private ResourceProviderItf resourceProvider;
    
    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // The requested resource.
        String pathInfo = request.getPathInfo();
        if (pathInfo == null)
        {
            super.doGet(request, response);
            return;
        }

        if (pathInfo.startsWith("/"))
        {
            pathInfo = pathInfo.substring(1);
        }
        
        try
        {
            ResourceBundle resourceBundle = resourceProvider.getResourceBundle(pathInfo);
            response.setContentType(resourceBundle.getResourceMimeType());
            InputStream resourceStream = FileUtils.openInputStream(resourceBundle.getResourceFile());
            IOUtils.copy(resourceStream, response.getOutputStream());
            IOUtils.closeQuietly(resourceStream);
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

}
