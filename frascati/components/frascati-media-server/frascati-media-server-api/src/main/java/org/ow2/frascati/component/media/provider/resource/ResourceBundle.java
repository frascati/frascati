/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.media.provider.resource;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 *
 */
public class ResourceBundle
{
    /**
     * Mapping between file extensions and MIME types.
     */
    private static Properties extensions2mimeTypes = new Properties();
    static
    {
        // Load mapping between file extensions and MIME types.
        try
        {
            extensions2mimeTypes.load(ResourceBundle.class.getClassLoader().getResourceAsStream(
                    ResourceBundle.class.getPackage().getName().replace('.', '/') + "/extensions2mimeTypes.properties"));
        } catch (IOException ioe)
        {
            throw new Error(ioe);
        }
    }
    
    private String resourceId;
    
    private String resourceMedia;
    
    private File resourceFile;
    
    private String resourceMimeType;

    public String getResourceId()
    {
        return resourceId;
    }

    public void setResourceId(String resourceId)
    {
        this.resourceId = resourceId;
    }

    public String getResourceMedia()
    {
        return resourceMedia;
    }

    public void setResourceMedia(String resourceMedia)
    {
        this.resourceMedia = resourceMedia;
    }

    public File getResourceFile()
    {
        return resourceFile;
    }

    public void setResourceFile(File resourceFile)
    {
        this.resourceFile = resourceFile;
        this.resourceMimeType = ResourceBundle.getFileMIMEType(resourceFile);
    }

    public String getResourceMimeType()
    {
        return resourceMimeType;
    }

    public static String getFileMIMEType(File file)
    {
        int idx = file.getName().lastIndexOf('.');
        String fileExtension = (idx != -1) ? file.getName().substring(idx) : "";
        String resourceMimeType = extensions2mimeTypes.getProperty(fileExtension.toLowerCase());
        if (resourceMimeType == null)
        {
            resourceMimeType = "text/plain";
        }
        return resourceMimeType;
    }
}
