/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.device.state;

import java.util.List;
import java.util.logging.Logger;

import org.ow2.frascati.rf.message.RFMessage;
import org.ow2.frascati.rf.protocol.RFProtocolItf;
import org.ow2.frascati.util.reflection.ReflectionUtil;
import org.ow2.frascati.util.regex.RegexUtil;

/**
 *
 */
public class RfDeviceStateObject
{
    private final static Logger logger = Logger.getLogger(RfDeviceStateObject.class.getName());
    
    private String name;
    
    /**
     * The RFDeviceState annotation itself
     */
    private RFDeviceState rfDeviceState;
    
    /**
     * The target of rfDeviceState annotation, can be class method or field
     */
    private Object target;
    
    /**
     * Constructor
     * 
     * @param rfDeviceState
     * @param target
     */
    public RfDeviceStateObject(RFDeviceState rfDeviceState, Object target)
    {
        this.rfDeviceState=rfDeviceState;
        this.target=target;
        
        this.name=rfDeviceState.name();
        if(name==null || name.equals(""))
        {
            name=rfDeviceState.value();
        }
    }
    
    /**
     * Get the name of this RfDeviceStateObject
     * This name is rfDeviceState name if define, rfDeviceState value if not
     * 
     * @return The name of RfDeviceStateObject 
     */
    public String getName()
    {
       return this.name;
    }

    /**
     * Set the name of this RfDeviceStateObject
     * Used to set name with target field name
     * 
     * @return The name of RfDeviceStateObject 
     */
    public void setName(String name)
    {
        this.name=name;
    }
    
    /**
     * @return The target of rfDeviceState annotation, can be class method or field
     */
    public Object getTarget()
    {
        return target;
    }
    
    /**
     *  @return The target class of rfDeviceState annotation, can be Class Method or Field
     */
    public Class<?> getTargetType()
    {
        return target.getClass();
    }
    
    /**
     * Get the regular expression related to the RFDeviceState
     * RFDevicestate owner is needed to replace dynamic value (as rfAddress)
     * found in RFDevicestate value.
     * 
     * @param owner the Object that own the RFDevicestate annotation
     * @return regular expression related to the current RFDeviceState
     */
    private String getRfDeviceStateRegex(Object owner)
    {
        String value=rfDeviceState.value();
        List<String> classFieldNames = RegexUtil.getTaggedValues(value, RFDeviceState.CLASSFIELD_TAG);
        if (!classFieldNames.isEmpty())
        {
            Object classFieldValue;
            for (String classFieldName : classFieldNames)
            {
                classFieldValue = ReflectionUtil.getFieldValue(owner, classFieldName);
                if (classFieldValue == null)
                {
                    logger.severe("Can't  find field named " + classFieldName + " for class " + owner.getClass());
                    continue;
                }
                value = RegexUtil.setTaggedValues(value, RFDeviceState.CLASSFIELD_TAG, classFieldName, classFieldValue.toString());
            }
        }
        logger.fine("RfDeviceStateObject value : " + value);
        return ".*"+value+".*";
    }

    public String getRfDeviceStateValue(RFMessage<? extends RFProtocolItf> rfMessage, Object owner)
    {
        String rfDeviceStateRegex=this.getRfDeviceStateRegex(owner);
        String rfRawMessage = rfMessage.getRfRawMessage();
        if(!rfRawMessage.matches(rfDeviceStateRegex))
        {
            return null;
        }
        
        try
        {
            return rfRawMessage.replaceAll(rfDeviceStateRegex, "$1");
        }
        catch (Exception e)
        {
            return rfDeviceStateRegex;
        }
    }
    
    public String toString()
    {
        return this.getClass().getSimpleName()+" name : "+this.name+", value : "+this.rfDeviceState.value()+", targetType "+this.getTargetType().getSimpleName();
    }
}
