/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.device.impl;

import org.osoa.sca.annotations.Property;
import org.ow2.frascati.rf.RFRemoteButton;
import org.ow2.frascati.rf.RFRemoteButtons;
import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.controller.RFControllerItf;
import org.ow2.frascati.rf.device.RFRemoteDeviceItf;
import org.ow2.frascati.rf.message.RFMessage;
import org.ow2.frascati.rf.protocol.RFProtocolItf;

/**
 *
 */
public class RFRemoteDeviceImpl<RFProtocol extends RFProtocolItf, RFControllerType extends RFControllerItf<RFProtocol>> extends AbstractRFDevice<RFProtocol, RFControllerType> implements RFRemoteDeviceItf<RFProtocol>
{
    private final static Long DEFAULT_RFREMOTEBUTTON_TIMEOUT=5000L;
    
    @Property(name="rfRemoteTimeOut")
    private Long rfRemoteTimeOut;
    
    private RFRemoteButtons rfRemoteButtons;

    @Property(name = "rfRemoteButtons")
    public void setRfRemoteButtons(RFRemoteButtons rfRemoteButtons)
    {
        this.rfRemoteButtons = rfRemoteButtons;
        logger.fine(this.rfRemoteButtons.getRFRemoteButtons().size()+" buttons");
    }

    /**
     * @see org.ow2.frascati.rf.device.RFRemoteDeviceItf#getState(java.lang.String)
     */
    public boolean getState(String buttonName)
    {
        Long timeOut=this.rfRemoteTimeOut;
        if(timeOut==null || timeOut.equals(0L))
        {
            timeOut=DEFAULT_RFREMOTEBUTTON_TIMEOUT;
        }
        return System.currentTimeMillis()-this.getRfDeviceStateUpdateTime(buttonName)<timeOut;
    }

    /**
     * @see org.ow2.frascati.rf.device.impl.AbstractRFDevice#acceptRFMessage(org.ow2.frascati.rf.message.RFMessage)
     */
    @Override
    protected boolean acceptRFMessage(RFMessage<RFProtocol> rfMessage)
    {
        return getRFRemoteButton(rfMessage.getRfAddress())!=null;
    }
    
    /**
     * @see org.ow2.frascati.rf.device.impl.AbstractRFDevice#acceptRFMessage(org.ow2.frascati.rf.message.RFMessage)
     */
    @Override
    protected void proceedRFMessage(RFMessage<RFProtocol> rfMessage)
    {
        logger.fine(rfMessage.toString());
        RFAddress<RFProtocol> rfMessageAddress=rfMessage.getRfAddress();
        if(rfMessageAddress==null)
        {
            return;
        }
        
        for(RFRemoteButton rfRemoteButton : this.rfRemoteButtons.getRFRemoteButtons())
        {
            if(rfMessageAddress.equals(rfRemoteButton.getAddress()))
            {
                this.updateDeviceStateUpdateTime(rfRemoteButton.getValue());
            }
        }
    }
    
    /**
     * @param rfMessageAddress
     * @return
     */
    private RFRemoteButton getRFRemoteButton(RFAddress<RFProtocol> rfMessageAddress)
    {
        for(RFRemoteButton rfRemoteButton : this.rfRemoteButtons.getRFRemoteButtons())
        {
            if(rfMessageAddress.equals(rfRemoteButton.getAddress()))
            {
               return rfRemoteButton;
            }
        }
        return null;
    }
}
