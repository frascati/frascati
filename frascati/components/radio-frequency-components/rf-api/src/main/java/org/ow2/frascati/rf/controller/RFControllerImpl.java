/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.controller;

import java.util.List;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.address.RFInvalidAdressException;
import org.ow2.frascati.rf.listener.RFListenerItf;
import org.ow2.frascati.rf.message.RFMessage;
import org.ow2.frascati.rf.protocol.RFProtocolItf;

/**
 *
 */
@Scope("COMPOSITE")
public class RFControllerImpl<RFProtocol extends RFProtocolItf> implements RFControllerItf<RFProtocol>
{
    protected final Logger logger = Logger.getLogger(this.getClass().getName());
    
    /**
     * An instance of the RFProtocol processed
     */
    protected RFProtocol rfProtocol;
    
    /**
     * Multiple reference of RFListeners
     */
    protected List<RFListenerItf<RFProtocol>> rfListeners;
    
    /**
     * Last time catch command has been call
     */
    private long catchingTime;
    
    /**
     * The last caught address
     */
    private RFAddress<RFProtocol> caughtRFAdress;
    
    
    public RFControllerImpl(RFProtocol rfProtocol)
    {
        this.rfProtocol=rfProtocol;
    }
    
    public List<RFListenerItf<RFProtocol>> getRFListeners()
    {
        return this.rfListeners;
    }
    
    @Reference(name="rfListeners")
    public void setRFListeners(List<RFListenerItf<RFProtocol>> rfListeners)
    {
        this.rfListeners = rfListeners;
    }
    
    /**
     * @throws RFInvalidAdressException 
     * @see org.ow2.frascati.rf.controller.RFControllerItf#processRFRawAddress(java.lang.String)
     */
    public RFAddress<RFProtocol> processRFRawAddress(String rfRawAddress) throws RFInvalidAdressException
    {
        return new RFAddress<RFProtocol>(rfProtocol, rfRawAddress);
    }
    
    public void processRFRawMessage(String rfRawMessage)
    {
        String rfProtocolId=rfProtocol.getProtocolId().toUpperCase();
        if(!rfRawMessage.toUpperCase().contains(rfProtocolId))
        {
            logger.fine(this.rfProtocol.getProtocolName()+" filtered : "+rfRawMessage);
            return;
        }
        
        logger.fine("RFController "+this.rfProtocol.getProtocolName()+" receveived : "+rfRawMessage);
        
        RFAddress<RFProtocol> rfAddress;
        try
        {
            rfAddress=new RFAddress<RFProtocol>(rfProtocol, rfRawMessage);
        }
        catch (RFInvalidAdressException rfInvalidAdressException)
        {
            logger.fine(rfInvalidAdressException.getMessage());
            return;
        }
        
        if(isCatching())
        {
            this.caughtRFAdress=rfAddress;
        }
        
        RFMessage<RFProtocol> rfMessage=new RFMessage<RFProtocol>(rfAddress, rfRawMessage);
        for(RFListenerItf<RFProtocol> rfListener : this.rfListeners)
        {
            rfListener.onRFMessageReceived(rfMessage);
        }
    }

    /**
     * @see org.ow2.frascati.visionic.VisionicController#isCatching()
     */
    public boolean isCatching()
    {
        return (System.currentTimeMillis()-catchingTime<RFControllerItf.CATCHING_TIMEOUT);
    }

    /**
     * @throws RFControllerAlreadyCatchingException 
     * @see org.ow2.frascati.rf.controller.RFControllerItf#catchRFAdress()
     */
    public void catchRFAdress() throws RFControllerAlreadyCatchingException
    {
        if(this.isCatching())
        {
            throw new RFControllerAlreadyCatchingException();
        }
        this.catchingTime=System.currentTimeMillis();
        this.caughtRFAdress=null;
    }

    /**
     * @see org.ow2.frascati.rf.controller.RFControllerItf#getCaughtRFAdress()
     */
    public RFAddress<RFProtocol> getCaughtRFAdress()
    {
        if(isCatching())
        {
            return this.caughtRFAdress;
        }
        return null;
    }
}
