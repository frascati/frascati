/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.device.impl;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.rf.controller.RFControllerItf;
import org.ow2.frascati.rf.device.RFDeviceItf;
import org.ow2.frascati.rf.device.RFDeviceStateBundle;
import org.ow2.frascati.rf.device.RFDeviceStatesBundle;
import org.ow2.frascati.rf.device.listener.RFDeviceListenerItf;
import org.ow2.frascati.rf.device.state.RFDeviceState;
import org.ow2.frascati.rf.device.state.RfDeviceStateObject;
import org.ow2.frascati.rf.message.RFMessage;
import org.ow2.frascati.rf.protocol.RFProtocolItf;
import org.ow2.frascati.util.reflection.ReflectionUtil;

/**
 *
 */
@Scope("COMPOSITE")
public abstract class AbstractRFDevice<RFProtocol extends RFProtocolItf, RFControllerType extends RFControllerItf<RFProtocol>> implements RFDeviceItf<RFProtocol>
{
    protected final Logger logger = Logger.getLogger(this.getClass().getName());
    /**
     * RfController reference
     */
    @Reference(name = "rfController")
    protected RFControllerType rfController;

    /**
     * List of device listeners for this device
     */
    protected List<RFDeviceListenerItf> rfDeviceListeners;
    
    /**
     * Map of the last update time for a given RFDeviceState annotation
     */
    protected Map<String, Long> rfDeviceStateUpdateTimeMap;
    
    /**
     * List of the RfDeviceStateObject found for this class 
     */
    protected List<RfDeviceStateObject> rfDeviceStateObjects;
    
    
    /**
     * Use java reflection to get all RFDeviceState annotation found this class
     * 
     */
    public AbstractRFDevice()
    {
        this.rfDeviceStateUpdateTimeMap=new HashMap<String, Long>();
        this.rfDeviceStateObjects = new ArrayList<RfDeviceStateObject>();
        RFDeviceState rfDeviceState ;
        RfDeviceStateObject rfDeviceStateObject;
        
        rfDeviceState = this.getClass().getAnnotation(RFDeviceState.class);
        if (rfDeviceState != null)
        {
            //RFDeviceState annotation found on the class
            rfDeviceStateObject = new RfDeviceStateObject(rfDeviceState, this.getClass());
            this.addRFDeviceStateObject(rfDeviceStateObject);
        }
        for (Field rfDeviceStateAnnotatedField : ReflectionUtil.getAllFields(this.getClass(), RFDeviceState.class))
        {
            if(isValidRFDeviceStateAnnotatedField(rfDeviceStateAnnotatedField))
            {
                rfDeviceStateObject = new RfDeviceStateObject(rfDeviceStateAnnotatedField.getAnnotation(RFDeviceState.class), rfDeviceStateAnnotatedField);
                rfDeviceStateObject.setName(rfDeviceStateAnnotatedField.getName());
                this.addRFDeviceStateObject(rfDeviceStateObject);
            }
        }
        for(Method rfDeviceStateAnnotatedMethod : ReflectionUtil.getAllMethods(this.getClass(), RFDeviceState.class))
        {
            if(isValidRFDeviceStateAnnotatedMethod(rfDeviceStateAnnotatedMethod))
            {
                rfDeviceStateObject = new RfDeviceStateObject(rfDeviceStateAnnotatedMethod.getAnnotation(RFDeviceState.class), rfDeviceStateAnnotatedMethod);
                this.addRFDeviceStateObject(rfDeviceStateObject);
            }
        }
    }
    
    /**
     * Define if a field is valid for RFDeviceState annotation
     * ie if the type of the field have a static valueOf(String) method
     * 
     * @param rfDeviceStateAnnotatedField
     * @return
     */
    private boolean isValidRFDeviceStateAnnotatedField(Field rfDeviceStateAnnotatedField)
    {
        Class<?> rfDeviceStateAnnotatedFieldType=ReflectionUtil.getConsistentClass(rfDeviceStateAnnotatedField.getType());
        if(rfDeviceStateAnnotatedFieldType.equals(String.class))
        {
            return true;
        }
        
        try
        {
            rfDeviceStateAnnotatedFieldType.getDeclaredMethod("valueOf", String.class);
            return true;
        }
        catch (Exception e)
        {
            logger.warning(rfDeviceStateAnnotatedField.getName()+" field is not valid for RFDeviceState annotation, "+rfDeviceStateAnnotatedFieldType.getName()+" class must have a static valueOf(String) method");
            return false;
        }
    }
    
    /**
     * Define if a method is valid for RFDeviceState annotation
     * ie if the the method have void return type and one String parameter
     * 
     * @param rfDeviceStateAnnotatedMethod
     * @return
     */
    private boolean isValidRFDeviceStateAnnotatedMethod(Method rfDeviceStateAnnotatedMethod)
    {
        if(!rfDeviceStateAnnotatedMethod.getReturnType().equals(Void.TYPE))
        {
            logger.warning(rfDeviceStateAnnotatedMethod.getName()+"method is not valid for RFDeviceState annotation : return type must be void");
            return false;
        }
        if(!(rfDeviceStateAnnotatedMethod.getParameterTypes().length==1 && rfDeviceStateAnnotatedMethod.getParameterTypes()[0].equals(String.class)))
        {
            logger.warning(rfDeviceStateAnnotatedMethod.getName()+"method is not valid for RFDeviceState annotation : parameters must be a single String");
            return false;
        }
        return true;
    }
    
    /**
     * Add a RfDeviceStateObject to rfDeviceStateObjects list and create a key with its name 
     * in the rfDeviceStateUpdateTimeMap
     * 
     * @param rfDeviceStateObject
     */
    private void addRFDeviceStateObject(RfDeviceStateObject rfDeviceStateObject)
    {
        this.rfDeviceStateObjects.add(rfDeviceStateObject);
        this.rfDeviceStateUpdateTimeMap.put(rfDeviceStateObject.getName(),0L);
        logger.fine("Add "+rfDeviceStateObject.toString());
    }
    
    /**
     * @see org.ow2.frascati.rf.listener.RFListenerItf#onRFMessageReceived(org.ow2.frascati.rf.message.RFMessage)
     */
    public void onRFMessageReceived(RFMessage<RFProtocol> rfMessage)
    {
        if (this.acceptRFMessage(rfMessage))
        {
            logger.fine(rfMessage.toString());
            this.proceedRFMessage(rfMessage);
            this.notifyRFDeviceListeners();
        }
    }

    /**
     * Define if a message can be proceed
     * 
     * @param rfMessage the RF message received by the RFController
     * @return
     */
    protected abstract boolean acceptRFMessage(RFMessage<RFProtocol> rfMessage);
    
    
    /**
     * Proceed rfMessage
     * 
     * @param rfMessage 
     */
    protected void proceedRFMessage(RFMessage<RFProtocol> rfMessage)
    {
        String rfDeviceStateStringValue;
        
        for(RfDeviceStateObject rfDeviceStateObject : rfDeviceStateObjects)
        {
            rfDeviceStateStringValue=rfDeviceStateObject.getRfDeviceStateValue(rfMessage, this);
            if(rfDeviceStateStringValue==null)
            {
                logger.fine("Can't extract "+rfDeviceStateObject.getName()+" from RFMessage : "+rfMessage);
                continue;
            }
            if(rfDeviceStateObject.getTargetType().equals(Class.class))
            {
                this.updateDeviceStateUpdateTime(rfDeviceStateObject.getName());
            }
            else if (rfDeviceStateObject.getTargetType().equals(Field.class))
            {
                this.proceedRfDeviceStateObject(rfDeviceStateObject, (Field)rfDeviceStateObject.getTarget(), rfDeviceStateStringValue);
            }
            else if (rfDeviceStateObject.getTargetType().equals(Method.class))
            {
                this.proceedRfDeviceStateObject(rfDeviceStateObject, (Method)rfDeviceStateObject.getTarget(), rfDeviceStateStringValue);
            }
        }
    }
    
    /**
     * Notify RFDeviceListeners
     */
    protected void notifyRFDeviceListeners()
    {
      RFDeviceStatesBundle rfDeviceStatesBundle=new RFDeviceStatesBundle();
      rfDeviceStatesBundle.setName(this.getName());
      
      RFDeviceStateBundle rfDeviceStateBundle;
      Object rfDeviceStateObjectValue;
      for(String rfDeviceStateName : this.rfDeviceStateUpdateTimeMap.keySet())
      {
          rfDeviceStateBundle=new RFDeviceStateBundle();
          rfDeviceStateBundle.setName(rfDeviceStateName);
          rfDeviceStateObjectValue=ReflectionUtil.getFieldValue(this, rfDeviceStateName);
          if(rfDeviceStateObjectValue != null)
          {
              rfDeviceStateBundle.setValue(rfDeviceStateObjectValue.toString());
              rfDeviceStateBundle.setType(rfDeviceStateObjectValue.getClass().getSimpleName());
          }
          rfDeviceStateBundle.setUpdateTime(this.rfDeviceStateUpdateTimeMap.get(rfDeviceStateName));
          rfDeviceStatesBundle.getRFDeviceStatesBundles().add(rfDeviceStateBundle);
      }
      
      for(RFDeviceListenerItf rfDeviceListener : this.rfDeviceListeners)
      {
          rfDeviceListener.notifyRFDeviceStateChange(rfDeviceStatesBundle);
      }
    }
    
    /**
     * Proceed a matched annotation found on a field
     * i.e set value of the field with value extract from RFDeviceState annotation
     * 
     * @param rfDeviceStateObject
     * @param targetField
     * @param rfDeviceStateStringValue
     */
    private void proceedRfDeviceStateObject(RfDeviceStateObject rfDeviceStateObject, Field targetField, String rfDeviceStateStringValue)
    {
        Object rfDeviceStateObjectValue = ReflectionUtil.valueOf(targetField.getType(), rfDeviceStateStringValue);
        if (ReflectionUtil.setFieldValue(targetField, this, rfDeviceStateObjectValue))
        {
            this.updateDeviceStateUpdateTime(rfDeviceStateObject.getName(), rfDeviceStateObjectValue);
        }
        else
        {
            logger.severe(targetField.getName() +" update failed");
        }
    }
    
    /**
     * Proceed a matched annotation found on a method
     * i.e set call this method with value extract from RFDeviceState annotation
     * 
     * @param rfDeviceStateObject
     * @param targetMethod
     * @param rfDeviceStateStringValue
     */
    private void proceedRfDeviceStateObject(RfDeviceStateObject rfDeviceStateObject, Method targetMethod, String rfDeviceStateStringValue)
    {
        try
        {
            targetMethod.setAccessible(true);
            targetMethod.invoke(this, rfDeviceStateStringValue);
            Object rfDeviceStatefieldValue = ReflectionUtil.getFieldValue(this, rfDeviceStateObject.getName());
            if(rfDeviceStatefieldValue!=null)
            {
                this.updateDeviceStateUpdateTime(rfDeviceStateObject.getName(), rfDeviceStatefieldValue);
            }
            else
            {
                this.updateDeviceStateUpdateTime(rfDeviceStateObject.getName());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            logger.severe(rfDeviceStateObject.getName() +" update failed");
        }
    }
    
    /**
     * Update "update time" for state deviceStateName
     * 
     * @param deviceStateName
     */
    protected void updateDeviceStateUpdateTime(String deviceStateName)
    {
        Long currentTime=System.currentTimeMillis();
        this.rfDeviceStateUpdateTimeMap.put(deviceStateName, currentTime);
        logger.info("["+new Date(currentTime)+"] "+this.getClass().getSimpleName() + " update " + deviceStateName);
    }
    
    /**
     * Update "update time" for state deviceStateName, deviceStateValue is used just for logging
     * 
     * @param deviceStateName
     * @param deviceStateValue
     */
    protected void updateDeviceStateUpdateTime(String deviceStateName, Object deviceStateValue)
    {
        Long currentTime=System.currentTimeMillis();
        this.rfDeviceStateUpdateTimeMap.put(deviceStateName, currentTime);
        logger.info("["+new Date(currentTime)+"] "+this.getClass().getSimpleName() + " update " + deviceStateName + " : " + deviceStateValue.toString());
    }
    
    /**
     * Get the time of the last RFDeviceState update
     * 
     * @param rfDeviceStateName
     * @return
     */
    protected Long getRfDeviceStateUpdateTime(String rfDeviceStateName)
    {
        Long rfDeviceStateUpdateTime=this.rfDeviceStateUpdateTimeMap.get(rfDeviceStateName);
        if(rfDeviceStateUpdateTime==null)
        {
            return 0L;
        }
        return rfDeviceStateUpdateTime;
    }
    
    /**
     * @return list of device listeners for this device
     */
    public List<RFDeviceListenerItf> getRFDeviceListeners()
    {
        return this.rfDeviceListeners;
    }
    
    /**
     * @param rfDeviceListeners list of device listeners for this device
     */
    @Reference(name="rfDeviceListeners")
    public void setRFDeviceListeners(List<RFDeviceListenerItf> rfDeviceListeners)
    {
        this.rfDeviceListeners = rfDeviceListeners;
    }
    
    public String getName()
    {
        return this.getClass().getSimpleName();
    }
    
}
