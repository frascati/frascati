/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.device.impl;

import org.osoa.sca.annotations.Property;
import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.address.RFInvalidAdressException;
import org.ow2.frascati.rf.controller.RFControllerItf;
import org.ow2.frascati.rf.message.RFMessage;
import org.ow2.frascati.rf.protocol.RFProtocolItf;

/**
 *
 */
public class SimpleAddressRFDeviceImpl<RFProtocol extends RFProtocolItf, RFControllerType extends RFControllerItf<RFProtocol>> extends AbstractRFDevice<RFProtocol, RFControllerType>
{
	protected String rfAddressString;
	
    /**
     * Device address
     */
    protected RFAddress<RFProtocol> rfAddress;

    @Property(name="address")
    public void setAddress(String address) throws RFInvalidAdressException
    {
    	if(rfController != null)
    	{
            this.rfAddress=rfController.processRFRawAddress(address);
    	}
    	else
    	{
    		this.rfAddressString = address;
    	}
    }

    public RFAddress<RFProtocol> getRfAddress()
    {
    	try
    	{
        	if(this.rfAddress == null)
        	{
                this.rfAddress=rfController.processRFRawAddress(this.rfAddressString);
        	}
            return this.rfAddress;
    	}
    	catch(Exception e)
    	{
    		return null;
    	}
    }
    
    /**
     * @see org.ow2.frascati.rf.device.impl.AbstractRFDevice#acceptRFMessage(org.ow2.frascati.rf.message.RFMessage)
     */
    protected boolean acceptRFMessage(RFMessage<RFProtocol> rfMessage)
    {
        RFAddress<RFProtocol> rfMessageAddress = rfMessage.getRfAddress();
        return rfMessageAddress != null && rfMessageAddress.equals(this.rfAddress);
    }
}
