/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.rf.address;

import org.ow2.frascati.rf.protocol.RFProtocolItf;


/**
 *
 */
public class RFAddress<RFProtocol extends RFProtocolItf>
{
    protected String address;
    
    public RFAddress(RFProtocol rfProtocol, String rfRawMessage) throws RFInvalidAdressException
    {
        String protocolAddressRegex=".*("+rfProtocol.getProtocolAddressPattern()+").*";
        if(rfRawMessage.matches(protocolAddressRegex))
        {
            this.address=rfRawMessage.replaceAll(protocolAddressRegex, "$1");
        }
        else
        {
            throw new RFInvalidAdressException(rfProtocol,rfRawMessage);
        }
    }
    
    public String toString()
    {
        return address;
    }
    
    public boolean equals(Object object)
    {
        if(object!=null && object.toString().equals(this.toString()))
        {
            return true;
        }
        return super.equals(object);
    }
    
}
