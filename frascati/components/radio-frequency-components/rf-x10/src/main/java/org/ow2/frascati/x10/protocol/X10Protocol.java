/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.x10.protocol;

import org.ow2.frascati.rf.protocol.RFProtocolItf;


/**
 *
 */
public class X10Protocol implements RFProtocolItf
{
    /**
     * @see org.ow2.frascati.rf.protocol.RFProtocolItf#getProtocolId()
     */
    public String getProtocolName()
    {
        return "X10";
    }

    /**
     * @see org.ow2.frascati.rf.protocol.RFProtocolItf#getProtocolId()
     */
    public String getProtocolId()
    {
        return "433Mhz";
    }

    /**
     * @see org.ow2.frascati.rf.protocol.RFProtocolItf#getProtocolAddressPattern()
     */
    public String getProtocolAddressPattern()
    {
        return "XS(\\d*)|[A-P][1-9][0-6]?";
    }

}
