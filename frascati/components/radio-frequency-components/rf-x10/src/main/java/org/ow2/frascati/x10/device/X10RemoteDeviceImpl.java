/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.x10.device;

import org.ow2.frascati.rf.controller.RFControllerItf;
import org.ow2.frascati.rf.device.impl.RFRemoteDeviceImpl;
import org.ow2.frascati.rf.device.state.RFDeviceState;
import org.ow2.frascati.x10.protocol.X10Protocol;

/**
 *
 */
public class X10RemoteDeviceImpl extends RFRemoteDeviceImpl<X10Protocol, RFControllerItf<X10Protocol>> implements X10RemoteDeviceItf
{

    @RFDeviceState("Noise=<noise>(\\d*)</noise>")
    private Integer noise;
    
    @RFDeviceState("Level=<lev>(.*)</lev>/5")
    private Double level;

    public X10RemoteDeviceImpl()
    {
        super();
        this.noise=new Integer(-1);
        this.level=new Double(-1);
    }
    
    /**
     * @see org.ow2.frascati.visonic.device.VisionicDeviceItf#getNoise()
     */
    public Integer getNoise()
    {
        return noise;
    }

    /**
     * @see org.ow2.frascati.visonic.device.VisionicDeviceItf#getLevel()
     */
    public Double getLevel()
    {
        return level;
    }
}
