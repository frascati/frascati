/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.zwave.device;

import java.util.logging.Logger;

import org.ow2.frascati.rf.device.state.RFDeviceState;

/**
 *
 */
public class ZwaveBinaryDeviceImpl extends ZwaveDeviceImpl implements ZwaveBinaryDeviceItf
{
    protected boolean state;
    
	@RFDeviceState(name = "state", value = "<dev>CMD</dev>.*(<classField>rfAddress</classField>_OFF|<classField>rfAddress</classField>)")
	protected void setState(String stateString)
	{
		Logger.getAnonymousLogger().info(ZwaveBinaryDeviceImpl.class.getName()+" updtae "+stateString);
		if (stateString.equals(this.getRfAddress().toString()))
		{
			this.state = true;
		} else
		{
			this.state = false;
		}
	}
    
    /**
     * @see org.ow2.frascati.zwave.device.ZwaveBinaryDetectorItf#getState()
     */
    public boolean getState()
    {
        return state;
    }
}
