/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.zwave.controller;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.controller.RFControllerItf;
import org.ow2.frascati.rf.exception.RFException;
import org.ow2.frascati.zwave.protocol.ZwaveProtocol;


/**
 *
 */
public interface ZWaveControllerItf extends RFControllerItf<ZwaveProtocol>
{
    @POST
    @Path("/command/{address}/{command}")
    public void sendCommand(@PathParam("address")String address,@PathParam("command") String command) throws RFException;
    
    @GET
    @Path("/busy")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean isBusy();
    
    @POST
    @Path("/inclusion")
    public void inclusion() throws RFException;
    
    @GET
    @Path("/inclusion")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean isIncluding();
    
    @GET
    @Path("/includedDeviceAddress")
    @Produces(MediaType.TEXT_PLAIN)
    public RFAddress<ZwaveProtocol> getLastIncludedDeviceAddress();
    
    @POST
    @Path("/exclusion")
    public void exclusion() throws RFException;
    
    @GET
    @Path("/exclusion")
    @Produces(MediaType.TEXT_PLAIN)
    public boolean isExcluding();
    
    @GET
    @Path("/excludedDeviceAddress")
    @Produces(MediaType.TEXT_PLAIN)
    public RFAddress<ZwaveProtocol> getLastExcludedDeviceAddress();

}
