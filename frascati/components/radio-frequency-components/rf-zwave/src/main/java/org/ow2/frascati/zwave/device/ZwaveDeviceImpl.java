/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.zwave.device;

import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.device.impl.SimpleAddressRFDeviceImpl;
import org.ow2.frascati.rf.device.state.RFDeviceState;
import org.ow2.frascati.rf.exception.RFException;
import org.ow2.frascati.zwave.controller.ZWaveControllerItf;
import org.ow2.frascati.zwave.protocol.ZwaveProtocol;

/**
 *
 */
public class ZwaveDeviceImpl extends SimpleAddressRFDeviceImpl<ZwaveProtocol, ZWaveControllerItf> implements ZwaveDeviceItf
{
    public final static Long ASSOCIATION_TIMEOUT=60000L;
    public final static Long ASSOCIATION_SLEEPINGTIME=1000L;
    
    @RFDeviceState("Batt=<bat>(.*)</bat>")
    private String battery;
    
    private boolean presence = false;
    
    @RFDeviceState(name="presence", value="device.*(<classField>rfAddress</classField>).*signals its presence")
    public void setPresence(String presence)
    {
        this.presence = true;
    }

    public boolean getPresence()
    {
        return presence;
    }
    
    /**
     * @see org.ow2.frascati.zwave.device.ZwaveDeviceItf#getBattery()
     */
    public String getBattery()
    {
       return battery;
    }
    
    /**
     * @see org.ow2.frascati.zwave.device.ZwaveAssociableDeviceItf#associate()
     */
    public boolean associate() throws RFException
    {
        logger.info(this.getClass().getSimpleName()+" start install");
        
        long startTime=System.currentTimeMillis();
        long time= System.currentTimeMillis();
        
        rfController.inclusion();
        while(!rfController.isIncluding() && (time-startTime)<ASSOCIATION_TIMEOUT)
        {
            try
            {
                Thread.sleep(ASSOCIATION_SLEEPINGTIME);
            }
            catch (InterruptedException interruptedException)
            {
                throw new RFException(interruptedException);
            }
            time=System.currentTimeMillis();
        }
        
        if((time-startTime)> ASSOCIATION_TIMEOUT)
        {
            logger.severe(this.getClass().getSimpleName()+ " installation failed, cannot set Zwave controller to inclusion mode");
            return false;
        }
        
        logger.info("Inclusion started, associate now the new device....");
        startTime=System.currentTimeMillis();
        time= System.currentTimeMillis();
        while(rfController.isIncluding() && (time-startTime)<ASSOCIATION_TIMEOUT)
        {
            try
            {
                Thread.sleep(ASSOCIATION_SLEEPINGTIME);   
            }
            catch (InterruptedException interruptedException)
            {
                throw new RFException(interruptedException);
            }
            logger.info(time+" waiting for inclusion... "+rfController.isIncluding());
            time=System.currentTimeMillis();
        }
        
        if((time-startTime)> ASSOCIATION_TIMEOUT)
        {
            logger.severe(this.getClass().getSimpleName()+ " installation failed, operation takes too long");
            return false;
        }
        logger.info("Inclusion finished");
        
        RFAddress<ZwaveProtocol> lastIncludedDeviceAddress=rfController.getLastIncludedDeviceAddress();
        if(lastIncludedDeviceAddress==null)
        {
            logger.severe(this.getClass().getSimpleName()+ " installation failed, can't get zwave id for the new device");
            return false;
        }
        
        this.rfAddress=lastIncludedDeviceAddress;
        logger.info("Installation done, new device Id : "+this.getRfAddress());
        return true;
    }

    /**
     * @see org.ow2.frascati.zwave.device.ZwaveAssociableDeviceItf#dissociate()
     */
    public boolean dissociate() throws RFException
    {
        logger.info(this.getClass().getSimpleName()+" start uninstall");
        
        long startTime=System.currentTimeMillis();
        long time= System.currentTimeMillis();
        
        rfController.exclusion();
        while(!rfController.isExcluding() && (time-startTime)<ASSOCIATION_TIMEOUT)
        {
            try
            {
                Thread.sleep(ASSOCIATION_SLEEPINGTIME);   
            }
            catch (InterruptedException interruptedException)
            {
                throw new RFException(interruptedException);
            }
            time=System.currentTimeMillis();
            logger.info(time+" isExclunding "+rfController.isExcluding());
        }
        
        if(!rfController.isExcluding())
        {
            logger.severe(this.getClass().getSimpleName()+ " uninstallation failed, cannot set Zwave controller to exclusion mode");
            return false;
        }
        logger.info("Exclusion started, unassociate the device now....");
        
        startTime=System.currentTimeMillis();
        time= System.currentTimeMillis();
        while(rfController.isExcluding() && (time-startTime)<ASSOCIATION_TIMEOUT)
        {
            try
            {
                Thread.sleep(ASSOCIATION_SLEEPINGTIME);   
            }
            catch (InterruptedException interruptedException)
            {
                throw new RFException(interruptedException);
            }
            time=System.currentTimeMillis();
        }
        
        if(rfController.isExcluding())
        {
            logger.severe(this.getClass().getSimpleName()+ " uninstallation failed, operation takes too long");
            return false;
        }
        logger.info("Exclusion finished");
        
        RFAddress<ZwaveProtocol> lastExclusionDeviceAddres=rfController.getLastExcludedDeviceAddress();
        if(lastExclusionDeviceAddres==null)
        {
            logger.severe(this.getClass().getSimpleName()+ " uninstallation failed, can't get zwave id for the new device");
            return false;
        }
        
        if(!lastExclusionDeviceAddres.equals(this.rfAddress))
        {
            logger.severe(this.getClass().getSimpleName()+ " uninstallation failed, the remove device is not the current device, remove device id : "+lastExclusionDeviceAddres+", current id "+this.rfAddress);
            return false;
        }
        
        this.rfAddress=null;
        logger.info("Uninstallation done");
        return true;
    }

    /**
     * @see org.ow2.frascati.zwave.device.ZwaveAssociableDeviceItf#isAssociated()
     */
    public boolean isAssociated()
    {
        return this.rfAddress!=null;
    }
}
