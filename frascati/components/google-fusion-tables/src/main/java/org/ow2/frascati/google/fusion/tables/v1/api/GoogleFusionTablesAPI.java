/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.api;

import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;

/**
 * Google Fusion Tables API.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
@Path("/v1")
public interface GoogleFusionTablesAPI
{
    /**
     * Get the description of a table.
     *
     * @param tableId the table identifier.
     * @return the description of the table.
     */
    @GET
    @Path("/tables/{tableId}")
    @Produces(MediaType.APPLICATION_JSON)
    Table getTable(@PathParam("tableId") String tableId);

    /**
     * Get the description of all the columns of a table.
     *
     * @param tableId the table identifier.
     * @return the description of all the columns of the table.
     */
    @GET
    @Path("/tables/{tableId}/columns")
    @Produces(MediaType.APPLICATION_JSON)
    Columns getColumns(@PathParam("tableId") String tableId);

    /**
     * Get the description of a column of a table.
     *
     * @param tableId the table identifier.
     * @param columnId the column identifier.
     * @return the description of the table.
     */
    @GET
    @Path("/tables/{tableId}/columns/{columnId}")
    @Produces(MediaType.APPLICATION_JSON)
    Column getColumn(@PathParam("tableId") String tableId, @PathParam("columnId") int columnId);

    /**
     * Evaluate an SQL query.
     *
     * @param sql the SQL query to evaluate.
     * @return the query result.
     */
    @GET
    @Path("/query")
    @Produces(MediaType.APPLICATION_JSON)
    QueryResult query(@QueryParam("sql") String sql);

    /**
     * Evaluate an SQL query.
     *
     * @param sql the SQL query to evaluate.
     * @return the query result.
     */
    @POST
    @Path("/query")
    @Produces(MediaType.APPLICATION_JSON)
    QueryResult queryPost(@QueryParam("sql") String sql);

    //
    // TODO: Other operations of the Google Fusion Tables API
    // should be added according to usage.
    //
}
