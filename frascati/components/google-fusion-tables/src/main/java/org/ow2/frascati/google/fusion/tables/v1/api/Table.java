/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.api;

import java.util.List;

/**
 * Result class returned by the GoogleFusionTablesAPI#table method.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class Table extends Kind
{
    private String tableId;
    private String name;
    private List<Column> columns;
    private String description;
    private boolean isExportable;
    private String attribution;

    /**
     * Get the table id.
     *
     * @return the table id.
     */
    public final String getTableId()
    {
      return this.tableId;
    }

    /**
     * Set the table id.
     *
     * @param tableId the table id.
     */
    public final void setTableId(String tableId)
    {
      this.tableId = tableId;
    }

    /**
     * Get the table name.
     *
     * @return the table name.
     */
    public final String getName()
    {
      return this.name;
    }

    /**
     * Set the table name.
     *
     * @param name the table name.
     */
    public final void setName(String name)
    {
      this.name = name;
    }

    /**
     * Get the table columns.
     *
     * @return the columns.
     */
    public final List<Column> getColumns()
    {
      return this.columns;
    }

    /**
     * Set the table columns.
     *
     * @param columns the table columns.
     */
    public final void setColumns(List<Column> columns)
    {
      this.columns = columns;
    }

    /**
     * Get the table description.
     *
     * @return the table description.
     */
    public final String getDescription()
    {
      return this.description;
    }

    /**
     * Set the table description.
     *
     * @param description the table description.
     */
    public final void setDescription(String description)
    {
      this.description = description;
    }

    /**
     * Get the exportable table attribute.
     *
     * @return the exportable table attribute.
     */
    public final boolean isExportable()
    {
      return this.isExportable;
    }

    /**
     * Set the exportable table attribute.
     *
     * @param isExportable the exportable table attribute.
     */
    public final void setIsExportable(boolean isExportable)
    {
      this.isExportable = isExportable;
    }

    /**
     * Get the table attribution.
     *
     * @return the table attribution.
     */
    public final String getAttribution()
    {
      return this.attribution;
    }

    /**
     * Set the table attribution.
     *
     * @param attribution the table attribution.
     */
    public final void setAttribution(String attribution)
    {
      this.attribution = attribution;
    }
}
