/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.api;

/**
 * Class returned by the GoogleFusionTablesAPI#column method.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class Column extends Kind
{
    private int columnId;
    private String name;
    private String type;

    /**
     * Get the column id.
     *
     * @return the column id.
     */
    public final int getColumnId()
    {
      return this.columnId;
    }

    /**
     * Set the column id.
     *
     * @param columnId the column id.
     */
    public final void setColumnId(int columnId)
    {
      this.columnId = columnId;
    }

    /**
     * Get the column name.
     *
     * @return the column name.
     */
    public final String getName()
    {
      return this.name;
    }

    /**
     * Set the column name.
     *
     * @param name the column name.
     */
    public final void setName(String name)
    {
      this.name = name;
    }

    /**
     * Get the column type.
     *
     * @return the column type.
     */
    public final String getType()
    {
      return this.type;
    }

    /**
     * Set the column type.
     *
     * @param type the column type.
     */
    public final void setType(String type)
    {
      this.type = type;
    }
}
