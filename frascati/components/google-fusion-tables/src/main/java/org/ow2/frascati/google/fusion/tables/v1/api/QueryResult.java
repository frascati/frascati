/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.api;

import java.util.List;

/**
 * Result class returned by the GoogleFusionTablesAPI#query method.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class QueryResult extends Kind
{
    private List<String> columns;
    private List<List<Object>> rows;

    /**
     * Get the columns.
     *
     * @return the columns.
     */
    public final List<String> getColumns()
    {
      return this.columns;
    }

    /**
     * Set the columns.
     *
     * @param columns the columns.
     */
    public final void setColumns(List<String> columns)
    {
      this.columns = columns;
    }

    /**
     * Get the rows.
     *
     * @return the rows.
     */
    public final List<List<Object>> getRows()
    {
      return this.rows;
    }

    /**
     * Set the rows.
     *
     * @param rows the rows.
     */
    public final void setRows(List<List<Object>> rows)
    {
      this.rows = rows;
    }
}
