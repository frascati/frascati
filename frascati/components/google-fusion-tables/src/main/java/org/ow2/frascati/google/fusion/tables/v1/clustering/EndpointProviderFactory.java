/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.clustering;

import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Endpoint;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.intent.cxf.clustering.endpoint.EndpointProvider;
import org.ow2.frascati.intent.cxf.clustering.endpoint.AbstractEndpointProviderFactory;
import org.ow2.frascati.intent.cxf.endpoint.EndpointHelper;

import org.ow2.frascati.google.fusion.tables.v1.api.GoogleFusionTablesAPI;
import org.ow2.frascati.google.fusion.tables.v1.api.QueryResult;

/**
 * Factory of clustering endpoint providers using a Google Fusion Table.
 *
 * @author Philippe Merle at Inria
 * @since 1.6
 */
public class EndpointProviderFactory
     extends AbstractEndpointProviderFactory
{
    // ----------------------------------------------------------------------
    // SCA configuration.
    // ----------------------------------------------------------------------

    /**
     * Google Fusion Table to query for obtaining endpoint addresses.
     */
    @Property(name="tableId")
    protected String tableId;

    /**
     * Google Fusion Table column containing endpoint addresses.
     */
    @Property(name="addressColumnName")
    private String addressColumnName;

    /**
     * Google Fusion Table column containing endpoint types.
     */
    @Property(name="typeColumnName")
    private String typeColumnName;

    /**
     * API for Google Fusion Tables.
     */
    @Reference(name="google-fusion-tables")
    protected GoogleFusionTablesAPI googleFusionTables;
	
    // ----------------------------------------------------------------------
    // For interface EndpointProviderFactory.
    // ----------------------------------------------------------------------

    /**
     * @see EndpointProviderFactory#newEndpointProvider(Endpoint)
     */
    public EndpointProvider newEndpointProvider(Endpoint endpoint)
    {
      // the type of the endpoint used to query endpoint addresses.
      final QName type = EndpointHelper.getInterfaceQName(endpoint);

      return new EndpointProvider() {
        /**
         * @see EndpointProvider#getEndpointAddresses()
         */
        public List<String> getEndpointAddresses()
        {
          // Prepare the SQL request.
          String sqlRequest = "select " + addressColumnName 
        		            + " from " + tableId
        		            + " where " + typeColumnName + " = '" + type.toString() + "'";
          info("Google Fusion Tables: " + sqlRequest);
          QueryResult queryResult = googleFusionTables.query(sqlRequest);

          // Compute the list of endpoint addresses.
          ArrayList<String> result = new ArrayList<String>();
          for(List<Object> row : queryResult.getRows()) {
            for(Object item : row) {
              result.add(item.toString());
            }
          }

          return result;
        }
      };
    }
}
