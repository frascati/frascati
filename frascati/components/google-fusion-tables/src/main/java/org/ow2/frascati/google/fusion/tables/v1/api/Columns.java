/**
 * OW2 FraSCAti: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.google.fusion.tables.v1.api;

import java.util.List;

/**
 * Result class returned by the GoogleFusionTablesAPI#columns method.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class Columns extends Kind
{
    private int totalItems;
    private List<Column> items;

    /**
     * Get the total of items.
     *
     * @return the total of items.
     */
    public final int getTotalItems()
    {
      return this.totalItems;
    }

    /**
     * Set the total of items.
     *
     * @param totalItems the total of items.
     */
    public final void setTotalItems(int totalItems)
    {
      this.totalItems = totalItems;
    }

    /**
     * Get the items.
     *
     * @return the items.
     */
    public final List<Column> getItems()
    {
      return this.items;
    }

    /**
     * Set the items.
     *
     * @param items the items.
     */
    public final void setItems(List<Column> items)
    {
      this.items = items;
    }
}
