/**
 * OW2 FraSCAti Logger Test
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.logger;
import static org.junit.Assert.*;
import java.util.List;
import java.util.logging.Level;

import org.junit.Test;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
@Scope("COMPONENT")
public class LoggerTestCase extends FraSCAtiTestCase
{

    @Test
    public void test() throws FrascatiException
    {
        String loggerServiceUri=FraSCAtiTestUtils.completeBindingURI("/logger");
        FraSCAtiTestUtils.assertWADLExist(true, loggerServiceUri);
        String loggerWidgetUri=FraSCAtiTestUtils.completeBindingURI("/loggerWidget");
        FraSCAtiTestUtils.assertURLExist(true, loggerWidgetUri+"/Widget.js");
        FraSCAtiTestUtils.assertURLExist(true, loggerWidgetUri+"/logger.html");
        
        LoggerItf logger=getService(LoggerItf.class, "logger");
        List<LogMessage> logMessageList=logger.getLogMessages();
        assertEquals(logMessageList.size(), 0);
        
        Long time=System.currentTimeMillis();
        logger.log(this, "default log message test");
        logMessageList=logger.getLogMessages();
        assertEquals(1,logMessageList.size());
        logMessageList=logger.getLogMessages(time);
        assertEquals(1,logMessageList.size());

        logger.log(this, Level.WARNING, "warning log message test");
        logMessageList=logger.getLogMessages(time);
        assertEquals(2,logMessageList.size());
        logMessageList=logger.getLogMessages(time,this.getClass().getSimpleName(),Level.WARNING);
        assertEquals(1,logMessageList.size());
        LogMessages logMessages=logger.getLogMessages(String.valueOf(time),"WARNING");
        assertEquals(1,logMessages.getLogMessages().size());
    }
    
}
