/**
 * OW2 FraSCAti Logger implementation
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.logger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;

/**
 *
 */
@Scope("COMPOSITE")
public class LoggerImpl implements LoggerItf
{
    private final static Logger logger = Logger.getLogger(LoggerImpl.class.getName());
    
    private Level defaultLogLevel=Level.FINE;
    
    private List<LogMessage> logMessages;
    
    @Init
    public void init()
    {
        this.logMessages=new LinkedList<LogMessage>();
    }
    
    public String getDefaultLogLevel()
    {
        return defaultLogLevel.getName();
    }

    @Property(name="defaultLogLevel")
    public void setDefaultLogLevel(String defaultLogLevel)
    {
        try
        {
            Level level=Level.parse(defaultLogLevel);
            this.defaultLogLevel = level;
        }
        catch(NullPointerException nullPointerException)
        {
            logger.severe("LoggerImpl.setDefaultLogLevel : the provided level is null");
        }
        catch (IllegalArgumentException illegalArgumentException)
        {
            logger.severe("LoggerImpl.setDefaultLogLevel : the provided level "+defaultLogLevel+" is not a valid Level");
        }
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#log(java.lang.Object, java.util.logging.Level, java.lang.String)
     */
    public void log(Object from, Level level, String message)
    {
        this.log(from.getClass().getName(), from.getClass().getSimpleName(), level, message);
        
    }

    public void log(Object from, String message)
    {
        this.log(from.getClass().getName(), from.getClass().getSimpleName(), defaultLogLevel, message);
    }
    
    /**
     * @see org.ow2.frascati.logger.LoggerItf#log(java.lang.String, java.lang.String, java.lang.String)
     */
    public void log(String from, String stringLevel, String message)
    {
        Level level=getLevel(stringLevel);
        this.log(from,from,level, message);
    }

    public void log(String from, String message)
    {
        this.log(from,from,defaultLogLevel, message);
    }
    
    /**
     * @see org.ow2.frascati.logger.LoggerItf#logMessage(org.ow2.frascati.logger.LogMessage)
     */
    public void logMessage(LogMessage logMessage)
    {
        this.log(logMessage.getFrom(), logMessage.getLevel(), logMessage.getMessage());
    }
    
    private void log(String loggerId, String from, Level level, String message)
    {
        Logger logger;
        try
        {
            logger=Logger.getLogger(loggerId);
        }
        catch(NullPointerException nullPointerException)
        {
            logger=Logger.getAnonymousLogger();
        }
        
        String fullMessage = "[" + from + "] ";
        fullMessage += message;
        logger.log(level, fullMessage);
        
        LogMessage logMessage=new LogMessage();
        String time=String.valueOf(System.currentTimeMillis());
        logMessage.setTime(time);
        logMessage.setFrom(from);
        logMessage.setLevel(level.getName());
        logMessage.setMessage(fullMessage);
        logMessages.add(logMessage);
    }
    
    private Level getLevel(String stringLevel)
    {
        Level level;
        try
        {
            level=Level.parse(stringLevel);
        }
        catch(Exception exception)
        {
            level=defaultLogLevel;
        }
        return level;
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages()
     */
    public List<LogMessage> getLogMessages()
    {
        return logMessages;
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.Long)
     */
    public List<LogMessage> getLogMessages(Long time)
    {
        return getLogMessages(time, null, null);
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.Long, java.util.logging.Level)
     */
    public List<LogMessage> getLogMessages(Long time, Level level)
    {
        return getLogMessages(time, null, level);
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.Long, java.lang.String, java.lang.String)
     */
    public List<LogMessage> getLogMessages(Long time, String from, Level level)
    {
        List<LogMessage> logMessages=new ArrayList<LogMessage>();
        Level logMessageLevel;
        for(LogMessage logMessage : this.logMessages)
        {
            logMessageLevel=getLevel(logMessage.getLevel());
            if((time==null ||Long.valueOf(logMessage.getTime())>=time) &&
               (from ==null || logMessage.from.equals(from)) &&
               (level == null || logMessageLevel.intValue()>=level.intValue()))
            {
                logMessages.add(logMessage);
            }
        }
        return logMessages;
    }

    /**
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.String)
     */
    public LogMessages getLogMessages(String stringTime)
    {
        Long time=Long.valueOf(stringTime);
        List<LogMessage> logMessageList=getLogMessages(time);
        LogMessages logMessages=new LogMessages();
        logMessages.getLogMessages().addAll(logMessageList);
        return logMessages;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.String, java.lang.String)
     */
    public LogMessages getLogMessages(String stringTime, String stringLevel)
    {
        Long time=Long.valueOf(stringTime);
        Level level=getLevel(stringLevel);
        List<LogMessage> logMessageList=getLogMessages(time, level);
        LogMessages logMessages=new LogMessages();
        logMessages.getLogMessages().addAll(logMessageList);
        return logMessages;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.logger.LoggerItf#getLogMessages(java.lang.String, java.lang.String, java.lang.String)
     */
    public LogMessages getLogMessages(String stringTime, String from, String stringLevel)
    {
        Long time=Long.valueOf(stringTime);
        Level level=getLevel(stringLevel);
        List<LogMessage> logMessageList=getLogMessages(time,from,level);
        LogMessages logMessages=new LogMessages();
        logMessages.getLogMessages().addAll(logMessageList);
        return logMessages;
    }
    
    
}
