/**
 * OW2 FraSCAti Logger API
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.logger;

import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;

/**
 *
 */
@Service
public interface LoggerItf
{
    public void log(Object from, String message);
    
    @POST
    @Path("/log")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public void log(@FormParam("from") String from,@FormParam("message") String message);

    public void log(Object from, Level level, String message);
    
    @POST
    @Path("/log/{level}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces({"text/plain"})
    public void log(@FormParam("from") String from,@PathParam("level") String level, @FormParam("message") String message);
    
    @POST
    @Path("/logMessage")
    @Consumes({"application/xml", "application/json"})
    @Produces({"text/plain"})
    public void logMessage(@FormParam("logMessage") LogMessage logMessage);
    
    public List<LogMessage> getLogMessages();
    public List<LogMessage> getLogMessages(Long time);
    public List<LogMessage> getLogMessages(Long time, Level level);
    public List<LogMessage> getLogMessages(Long time, String from, Level level);
    
    @GET
    @Path("/logMessage/{time}")
    @Consumes({"text/plain"})
    @Produces({"application/xml", "application/json"})
    public LogMessages getLogMessages(@PathParam("time")String time);
    
    @GET
    @Path("/logMessage/{time}/{level}")
    @Consumes({"text/plain"})
    @Produces({"application/xml", "application/json"})
    public LogMessages getLogMessages(@PathParam("time")String time,@PathParam("level") String level);
}
