package org.ow2.frascati.device.philips.np3500;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.upnp.common.UPnPDeviceAVTransportItf;
import org.ow2.frascati.upnp.common.UPnPDeviceConnectionManagerItf;
import org.ow2.frascati.upnp.common.UPnPDeviceRenderingControlItf;

@Service
public interface PhilipsNP3500Itf extends UPnPDeviceRenderingControlItf, UPnPDeviceAVTransportItf, UPnPDeviceConnectionManagerItf
{
}
