package org.ow2.frascati.device.philips.np3500;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.upnp.common.GetCurrentConnectionInfo;
import org.ow2.frascati.upnp.common.GetDeviceCapabilities;
import org.ow2.frascati.upnp.common.GetMediaInfo;
import org.ow2.frascati.upnp.common.GetPositionInfo;
import org.ow2.frascati.upnp.common.GetProtocolInfo;
import org.ow2.frascati.upnp.common.GetTransportInfo;
import org.ow2.frascati.upnp.common.GetTransportSettings;
import org.ow2.frascati.upnp.common.ListPresets;
import org.ow2.frascati.upnp.common.UPnPDeviceAVTransportItf;
import org.ow2.frascati.upnp.common.UPnPDeviceConnectionManagerItf;
import org.ow2.frascati.upnp.common.UPnPDeviceRenderingControlItf;

@Scope("COMPOSITE")
public class PhilipsNP3500Impl implements PhilipsNP3500Itf
{
    @Reference(name="upnpRenderingControl")
    private UPnPDeviceRenderingControlItf upnpRenderingControl;

    @Reference(name="upnpAVTransport")
    protected UPnPDeviceAVTransportItf upnpAVTransport;

    @Reference(name="upnpConnectionManager")
    protected UPnPDeviceConnectionManagerItf upnpConnectionManager;
    
    public GetCurrentConnectionInfo getCurrentConnectionInfo(String ConnectionID)
    {
            return upnpConnectionManager.getCurrentConnectionInfo(ConnectionID);
    }

    public GetProtocolInfo getProtocolInfo()
    {
            return upnpConnectionManager.getProtocolInfo();
    }

    public String getCurrentConnectionIDs()
    {
            return upnpConnectionManager.getCurrentConnectionIDs();
    }

    public void seek(String InstanceID, String Unit, String Target)
    {
            upnpAVTransport.seek(InstanceID, Unit, Target);
    }

    public void next(String InstanceID)
    {
            upnpAVTransport.next(InstanceID);
    }

    public void play(String InstanceID, String Speed)
    {
            upnpAVTransport.play(InstanceID, Speed);
    }

    public GetTransportInfo getTransportInfo(String InstanceID)
    {
            return upnpAVTransport.getTransportInfo(InstanceID);
    }

    public void previous(String InstanceID)
    {
            upnpAVTransport.previous(InstanceID);
    }
    
    public GetMediaInfo getMediaInfo(String InstanceID)
    {
            return upnpAVTransport.getMediaInfo(InstanceID);
    }

    public GetDeviceCapabilities getDeviceCapabilities(String InstanceID)
    {
            return upnpAVTransport.getDeviceCapabilities(InstanceID);
    }

    public void pause(String InstanceID)
    {
            upnpAVTransport.pause(InstanceID);
    }

    public String getCurrentTransportActions(String InstanceID)
    {
            return upnpAVTransport.getCurrentTransportActions(InstanceID);
    }

    public void setAVTransportURI(String InstanceID, String CurrentURI, String CurrentURIMetaData)
    {
            upnpAVTransport.setAVTransportURI(InstanceID, CurrentURI, CurrentURIMetaData);
    }

    public GetPositionInfo getPositionInfo(String InstanceID)
    {
            return upnpAVTransport.getPositionInfo(InstanceID);
    }

    public GetTransportSettings getTransportSettings(String InstanceID)
    {
            return upnpAVTransport.getTransportSettings(InstanceID);
    }

    public int stop(String InstanceID)
    {
            return upnpAVTransport.stop(InstanceID);
    }

    public ListPresets listPresets(String InstanceID)
    {
            return upnpRenderingControl.listPresets(InstanceID);
    }

    public void setMute(String InstanceID, String Channel, String DesiredMute)
    {
            upnpRenderingControl.setMute(InstanceID, Channel, DesiredMute);
    }

    public void setVolume(String InstanceID, String Channel, String DesiredVolume)
    {
            upnpRenderingControl.setVolume(InstanceID, Channel, DesiredVolume);
    }

    public int getVolume(String InstanceID, String Channel)
    {
            return upnpRenderingControl.getVolume(InstanceID, Channel);
    }

    public boolean getMute(String InstanceID, String Channel)
    {
            return upnpRenderingControl.getMute(InstanceID, Channel);
    }

    public void selectPresets(String InstanceID, String PresetName)
    {
            upnpRenderingControl.selectPresets(InstanceID, PresetName);
    }
    
}
