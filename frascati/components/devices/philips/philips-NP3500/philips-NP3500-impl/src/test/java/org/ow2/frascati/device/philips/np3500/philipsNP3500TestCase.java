package org.ow2.frascati.device.philips.np3500;

import org.ow2.frascati.device.philips.np3500.PhilipsNP3500Itf;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.junit.Test;

public class philipsNP3500TestCase extends FraSCAtiTestCase
{
    @Test
    public void test() throws InterruptedException
    {
        PhilipsNP3500Itf philipsNP3500 = this.getService(PhilipsNP3500Itf.class, "philipsNP3500Service");
        philipsNP3500.setMute("0", "MASTER", "true");
        Thread.sleep(1000);
        philipsNP3500.setMute("0", "MASTER", "false");
        
        philipsNP3500.setAVTransportURI("0", "http://192.168.0.25:62182/FreeMi/0%2FE%3A%5Cmusic%5CupnpTest%5CKatchafire%20-%20Collie%20Herb%20Man.mp3%2F0", "");
        philipsNP3500.play("0", "1");
    }
}
