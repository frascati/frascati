/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.aeon;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.provider.WidgetProviderItf;
import org.ow2.frascati.rf.device.RFBinaryDeviceItf;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.zwave.protocol.ZwaveProtocol;

/**
 *
 */
public class AeonDoorSensorTestCase extends FraSCAtiTestCase
{
    @Test
    public final void test() throws InterruptedException, IllegalStateException, IOException
    {
        String aeonDoorSensorUrl=FraSCAtiTestUtils.completeBindingURI("/aeonDoorSensor");
        FraSCAtiTestUtils.assertWADLExist(true, aeonDoorSensorUrl);
        
        WidgetProviderItf widgetProvider=this.getService(WidgetProviderItf.class, "widgetProvider");
        Widget widget=widgetProvider.getWidget();
        assertEquals("Aeon door sensor", widget.getName());
        FraSCAtiTestUtils.assertURLExist(true,widget.getIconURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getSmallWidgetURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getLargeWidgetURL());
    }
}