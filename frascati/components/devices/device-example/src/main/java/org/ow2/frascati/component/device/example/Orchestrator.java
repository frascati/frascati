/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.device.example;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.device.everspringan158.EverspringAN158Itf;
import org.ow2.frascati.device.visonicmct201.VisonicMCT201;
import org.ow2.frascati.rf.device.RFDeviceStateBundle;
import org.ow2.frascati.rf.device.RFDeviceStatesBundle;
import org.ow2.frascati.rf.device.listener.RFDeviceListenerItf;
import org.ow2.frascati.rf.exception.RFException;


/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class Orchestrator implements RFDeviceListenerItf
{
    @Reference(name="everspringAN158")
    public EverspringAN158Itf everspringAN158;

    private Long lastVisonicMCT201AlarmReceived=0L;
    
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.rf.device.listener.RFDeviceListenerItf#notifyRFDeviceStateChange(org.ow2.frascati.rf.device.RFDeviceStatesBundle)
     */
    public void notifyRFDeviceStateChange(RFDeviceStatesBundle rfDeviceStatesBundle)
    {
        if(rfDeviceStatesBundle.getName().equals(VisonicMCT201.class.getSimpleName()))
        {
            for(RFDeviceStateBundle rfDeviceStateBundle : rfDeviceStatesBundle.getRFDeviceStatesBundles())
            {
                if(rfDeviceStateBundle.getName().equals("alarm") && System.currentTimeMillis()-lastVisonicMCT201AlarmReceived>3000L)
                {
                    try
                    {
                        everspringAN158.turn(!everspringAN158.getState());
                        this.lastVisonicMCT201AlarmReceived=System.currentTimeMillis();
                    } catch (RFException e)
                    {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
