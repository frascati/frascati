/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.component.device.example;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.Test;
import org.ow2.frascati.device.everspringan158.EverspringAN158Itf;
import org.ow2.frascati.device.everspringst814.EverspringST184Itf;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.Widgets;
import org.ow2.frascati.widget.registry.WidgetRegistryItf;
import org.ow2.frascati.device.philips.np3500.PhilipsNP3500Itf;
/**
 *
 */
public class DeviceExampleTestCase extends FraSCAtiTestCase
{
    @Test
    public void testInstall() throws Exception
    {
        EverspringAN158Itf everspringAN158 = this.getService(EverspringAN158Itf.class, "everspringAN158Service");
//        everspringAN158.associate();
        everspringAN158.on();

        //      everspringAN158.dissociate();
        System.in.read();
    }
    
//    @Test
    public void testWADLs() throws Exception
    {
        String everSpringAN158URL = FraSCAtiTestUtils.completeBindingURI("/AN158");
        FraSCAtiTestUtils.assertWADLExist(true, everSpringAN158URL);
        EverspringAN158Itf everspringAN158Service = JAXRSClientFactory.create(everSpringAN158URL, EverspringAN158Itf.class);
        everspringAN158Service.on();
        Thread.sleep(1000);
        everspringAN158Service.off();

        String everSpringST184URL = FraSCAtiTestUtils.completeBindingURI("/ST814");
        FraSCAtiTestUtils.assertWADLExist(true, everSpringST184URL);
        EverspringST184Itf everspringST184Service = JAXRSClientFactory.create(everSpringST184URL, EverspringST184Itf.class);
        everspringST184Service.getCelciusTemperature();
        everspringST184Service.getFahrenheitTemperature();
        everspringST184Service.getHumidity();

        String visonicMCT201URL = FraSCAtiTestUtils.completeBindingURI("/visonicMCT201");
        FraSCAtiTestUtils.assertWADLExist(true, visonicMCT201URL);

        String aeonDoorSensorURL = FraSCAtiTestUtils.completeBindingURI("/aeonDoorSensor");
        FraSCAtiTestUtils.assertWADLExist(true, aeonDoorSensorURL);

        String bmbWD18URL = FraSCAtiTestUtils.completeBindingURI("/bmbWD18");
        FraSCAtiTestUtils.assertWADLExist(true, bmbWD18URL);

        String bmbCOD18URL = FraSCAtiTestUtils.completeBindingURI("/bmbCOD18");
        FraSCAtiTestUtils.assertWADLExist(true, bmbCOD18URL);

        String bmbSD18URL = FraSCAtiTestUtils.completeBindingURI("/bmbSD18");
        FraSCAtiTestUtils.assertWADLExist(true, bmbSD18URL);

        String bmbKR18EURL = FraSCAtiTestUtils.completeBindingURI("/bmbKR18E");
        FraSCAtiTestUtils.assertWADLExist(true, bmbKR18EURL);

        String bmbMS18EURL = FraSCAtiTestUtils.completeBindingURI("/bmbMS18E");
        FraSCAtiTestUtils.assertWADLExist(true, bmbMS18EURL);

        String bmbMS13E2URL = FraSCAtiTestUtils.completeBindingURI("/bmbMS13E2");
        FraSCAtiTestUtils.assertWADLExist(true, bmbMS13E2URL);

        PhilipsNP3500Itf philipsNP3500 = this.getService(PhilipsNP3500Itf.class, "philipsNP3500Service");
        philipsNP3500.setMute("0", "MASTER", "true");
        
        String widgetRegistryURL = FraSCAtiTestUtils.completeBindingURI("/widgetRegistry");
        FraSCAtiTestUtils.assertWADLExist(true, widgetRegistryURL);
        WidgetRegistryItf widgetRegistry = JAXRSClientFactory.create(widgetRegistryURL, WidgetRegistryItf.class);
        Widgets widgets = widgetRegistry.getWidgets();
        List<Widget> widgetList = widgets.getWidgets();
        assertEquals(11, widgetList.size());
        for(Widget widget : widgetList)
        {
            FraSCAtiTestUtils.assertURLExist(true,widget.getIconURL());
            FraSCAtiTestUtils.assertURLExist(true,widget.getSmallWidgetURL());
            FraSCAtiTestUtils.assertURLExist(true,widget.getLargeWidgetURL());
        }
    }

//    @Test
    public void launchServer() throws Exception
    {
    	System.in.read();
    }
    
}
