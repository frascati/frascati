package org.ow2.frascati.device.sony.braviaKDL40EX720;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.provider.WidgetProviderItf;

public class BraviaKDL40EX720TestCase extends FraSCAtiTestCase
{
    @Test
    public void test() throws InterruptedException
    {
        String widgetJSClientUrl = FraSCAtiTestUtils.completeBindingURI("/Sony-braviaKDL40EX720-Widget.js");
        WidgetProviderItf widgetProvider=this.getService(WidgetProviderItf.class, "widgetProvider");
        Widget widget=widgetProvider.getWidget();
        assertEquals("Sony Bravia KDL40EX720", widget.getName());
        FraSCAtiTestUtils.assertURLExist(true,widget.getIconURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getSmallWidgetURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getLargeWidgetURL());
        
        String sonyBraviaKDL40EX720Url=FraSCAtiTestUtils.completeBindingURI("/BraviaKDL40EX720");
        FraSCAtiTestUtils.assertWADLExist(true, sonyBraviaKDL40EX720Url);
        BraviaKDL40EX720Itf braviaKDL40EX720Service = this.getService(BraviaKDL40EX720Itf.class, "sonyBraviaKDL40EX720Service");
        braviaKDL40EX720Service.setVolume("0", "Master", "50");
        assertEquals(50,braviaKDL40EX720Service.getVolume("0", "Master"));
        braviaKDL40EX720Service.setVolume("0", "Master", "10");
        assertEquals(10,braviaKDL40EX720Service.getVolume("0", "Master"));
        
        String sonyBraviaKDL40EX720RemoteUrl=FraSCAtiTestUtils.completeBindingURI("/RemoteBraviaKDL40EX720");
        FraSCAtiTestUtils.assertWADLExist(true, sonyBraviaKDL40EX720RemoteUrl);
        BraviaKDL40EX720RemoteItf braviaKDL40EX720RemoteService = this.getService(BraviaKDL40EX720RemoteItf.class, "sonyBraviaKDL40EX720RemoteService");
        braviaKDL40EX720RemoteService.home();
    }
}
