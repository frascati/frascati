package org.ow2.frascati.device.sony.braviaKDL40EX720;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.device.sony.braviaKDL40EX720.exception.InvalidChannelException;


public class BraviaKDL40EX720RemoteImpl implements BraviaKDL40EX720RemoteItf
{
    @Reference(name="braviaKDL40EX720")
    private BraviaKDL40EX720Itf braviaKDL40EX720;
    
    public void setChannel(int channelNumber) throws InvalidChannelException
    {
        if(channelNumber<0 || channelNumber>999)
        {
            throw new InvalidChannelException(channelNumber);
        }
        
        String channelStringNumber = String.valueOf(channelNumber);
        for(int i=0;i<channelStringNumber.length();i++)
        {
                this.sendChannelNumber(channelStringNumber.charAt(i));
                try
                {
                        Thread.sleep(200);
                }
                catch (InterruptedException e)
                {
                        e.printStackTrace();
                }
        }
    }
    
    private void sendChannelNumber(char channelCharDigit)
    {
            switch(Character.getNumericValue(channelCharDigit))
            {
                    case 0 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num0);
                    break;
            
                    case 1 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num1);
                    break;
                    
                    case 2 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num2);
                    break;
                    
                    case 3 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num3);
                    break;
                    
                    case 4 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num4);
                    break;
                    
                    case 5 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num5);
                    break;
                    
                    case 6 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num6);
                    break;
                    
                    case 7 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num7);
                    break;
                    
                    case 8 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num8);
                    break;
                    
                    case 9 :
                            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Num9);
                    break;
            }
    }
    
    public void channelUp()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.ChannelUp);
    }
    
    public void channelDown()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.ChannelDown);
    }
    
    public void volumeUp()
    {
        braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.VolumeUp);
    }

    public void volumeDown()
    {
        braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.VolumeDown);
        
    }
    
    public void power()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Power);
    }
    
    public void arrowUp()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Up);
    }
    
    public void arrowDown()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Down);
    }
    
    public void arrowLeft()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Left);
    }
    
    public void arrowRight()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Right);
    }
    
    public void confirm()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Confirm);
    }
    
    public void stop()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Stop);
    }
    
    public void previous()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Prev);
    }
    
    public void rewind()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Rewind);
    }
    
    public void pause()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Pause);
    }
    
    public void play()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Play);
    }
    
    public void advance()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Advance);
    }
    
    public void next()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Next);
    }
    
    public void record()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Rec);
    }
    
    public void teletext()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Teletext);
    }
    
    public void teletextRed()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Red);
    }
    
    public void teletextGreen()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Green);
    }
    
    public void teletextYellow()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Yellow);
    }
    
    public void teletextBlue()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Blue);
    }
    
    public void returnButton()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Return);
    }
    
    public void home()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Home);
    }
    
    public void options()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Options);
    }
    
    public void guide()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.GGuide);
    }
    
    public void button3D()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Mode3D);
    }
    
    public void trackID()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.TrackID);
    }
    
    public void digitalAnalog()
    {
            //TODO
    }
    
    public void exit()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Exit);
    }
    
    public void scene()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.SceneSelect);
    }
    
    public void internetVideo()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.InternetVideo);
    }
    
    public void descriptionProgram()
    {
            braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.ProgramDescription);
    }

    public void manual()
    {
        braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.iManual);
    }

    public void syncMenu()
    {
        braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.SyncMenu);
    }

    public void audio()
    {
        braviaKDL40EX720.sendIRCC(BraviaKDL40EX720RemoteItf.Audio);   
    }
}
