package org.ow2.frascati.device.sony.braviaKDL40EX720;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.device.sony.braviaKDL40EX720.exception.InvalidChannelException;


@Service
public interface BraviaKDL40EX720RemoteItf
{
    public final static String Confirm = "AAAAAQAAAAEAAABlAw==";
    public final static String Up = "AAAAAQAAAAEAAAB0Aw==";
    public final static String Down = "AAAAAQAAAAEAAAB1Aw==";
    public final static String Right = "AAAAAQAAAAEAAAAzAw==";
    public final static String Left = "AAAAAQAAAAEAAAA0Aw==";
    public final static String Home = "AAAAAQAAAAEAAABgAw==";
    public final static String Options = "AAAAAgAAAJcAAAA2Aw==";
    public final static String Return = "AAAAAgAAAJcAAAAjAw==";
    public final static String Num1 = "AAAAAQAAAAEAAAAAAw==";
    public final static String Num2 = "AAAAAQAAAAEAAAABAw==";
    public final static String Num3 = "AAAAAQAAAAEAAAACAw==";
    public final static String Num4 = "AAAAAQAAAAEAAAADAw==";
    public final static String Num5 = "AAAAAQAAAAEAAAAEAw==";
    public final static String Num6 = "AAAAAQAAAAEAAAAFAw==";
    public final static String Num7 = "AAAAAQAAAAEAAAAGAw==";
    public final static String Num8 = "AAAAAQAAAAEAAAAHAw==";
    public final static String Num9 = "AAAAAQAAAAEAAAAIAw==";
    public final static String Num0 = "AAAAAQAAAAEAAAAJAw==";
    public final static String Num11 = "AAAAAQAAAAEAAAAKAw==";
    public final static String Num12 = "AAAAAQAAAAEAAAALAw==";
    public final static String Power = "AAAAAQAAAAEAAAAVAw==";
    public final static String Display = "AAAAAQAAAAEAAAA6Aw==";
    public final static String VolumeUp = "AAAAAQAAAAEAAAASAw==";
    public final static String VolumeDown = "AAAAAQAAAAEAAAATAw==";
    public final static String Mute = "AAAAAQAAAAEAAAAUAw==";
    public final static String Audio = "AAAAAQAAAAEAAAAXAw==";
    public final static String SubTitle = "AAAAAgAAAJcAAAAoAw==";
    public final static String Yellow = "AAAAAgAAAJcAAAAnAw==";
    public final static String Blue = "AAAAAgAAAJcAAAAkAw==";
    public final static String Red = "AAAAAgAAAJcAAAAlAw==";
    public final static String Green = "AAAAAgAAAJcAAAAmAw==";
    public final static String Play = "AAAAAgAAAJcAAAAaAw==";
    public final static String Stop = "AAAAAgAAAJcAAAAYAw==";
    public final static String Pause = "AAAAAgAAAJcAAAAZAw==";
    public final static String Rewind = "AAAAAgAAAJcAAAAbAw==";
    public final static String Forward = "AAAAAgAAAJcAAAAcAw==";
    public final static String Prev = "AAAAAgAAAJcAAAA8Aw==";
    public final static String Next = "AAAAAgAAAJcAAAA9Aw==";
    public final static String Replay = "AAAAAgAAAJcAAAB5Aw==";
    public final static String Advance = "AAAAAgAAAJcAAAB4Aw==";
    public final static String TopMenu = "AAAAAgAAABoAAABgAw==";
    public final static String PopUpMenu = "AAAAAgAAABoAAABhAw==";
    public final static String Eject = "AAAAAgAAAJcAAABIAw==";
    public final static String Rec = "AAAAAgAAAJcAAAAgAw==";
    public final static String SyncMenu = "AAAAAgAAABoAAABYAw==";
    public final static String ClosedCaption = "AAAAAgAAAKQAAAAQAw==";
    public final static String Teletext = "AAAAAQAAAAEAAAA/Aw==";
    public final static String ChannelUp = "AAAAAQAAAAEAAAAQAw==";
    public final static String ChannelDown = "AAAAAQAAAAEAAAARAw==";
    public final static String Input = "AAAAAQAAAAEAAAAlAw==";
    public final static String GGuide = "AAAAAQAAAAEAAAAOAw==";
    public final static String EPG = "AAAAAgAAAKQAAABbAw==";
    public final static String DOT = "AAAAAgAAAJcAAAAdAw==";
    public final static String Analog = "AAAAAgAAAHcAAAANAw==";
    public final static String Exit = "AAAAAQAAAAEAAABjAw==";
    public final static String Digital = "AAAAAgAAAJcAAAAyAw==";
    public final static String BS = "AAAAAgAAAJcAAAAsAw==";
    public final static String CS = "AAAAAgAAAJcAAAArAw==";
    public final static String BSCS = "AAAAAgAAAJcAAAAQAw==";
    public final static String Ddata = "AAAAAgAAAJcAAAAVAw==";
    public final static String InternetWidgets = "AAAAAgAAABoAAAB6Aw==";
    public final static String InternetVideo = "AAAAAgAAABoAAAB5Aw==";
    public final static String SceneSelect = "AAAAAgAAABoAAAB4Aw==";
    public final static String Mode3D = "AAAAAgAAAHcAAABNAw==";
    public final static String iManual = "AAAAAgAAABoAAAB7Aw==";
    public final static String Wide = "AAAAAgAAAKQAAAA9Aw==";
    public final static String Jump = "AAAAAQAAAAEAAAA7Aw==";
    public final static String PAP = "AAAAAgAAAKQAAAB3Aw==";
    public final static String MyEPG = "AAAAAgAAAHcAAABrAw==";
    public final static String ProgramDescription = "AAAAAgAAAJcAAAAWAw==";
    public final static String WriteChapter = "AAAAAgAAAHcAAABsAw==";
    public final static String TrackID = "AAAAAgAAABoAAAB+Aw==";
    public final static String TenKey = "AAAAAgAAAJcAAAAMAw==";
    public final static String AppliCast = "AAAAAgAAABoAAABvAw==";
    public final static String acTVila = "AAAAAgAAABoAAAByAw==";
    public final static String DeleteVideo = "AAAAAgAAAHcAAAAfAw==";
    public final static String EasyStartUp = "AAAAAgAAAHcAAABqAw==";
    public final static String OneTouchTimeRec = "AAAAAgAAABoAAABkAw==";
    public final static String OneTouchView = "AAAAAgAAABoAAABlAw==";
    public final static String OneTouchRec = "AAAAAgAAABoAAABiAw==";
    public final static String OneTouchRecStop = "AAAAAgAAABoAAABjAw==";
    
    @POST
    @Path("/channel/{channelNumber}")
    public void setChannel(@PathParam("channelNumber")int channel) throws InvalidChannelException;
    
    @POST
    @Path("/channelUp")
    public void channelUp();
    
    @POST
    @Path("/channelDown")
    public void channelDown();
    
    @POST
    @Path("/volumeUp")
    public void volumeUp();
    
    @POST
    @Path("/volumeDown")
    public void volumeDown();
    
    @POST
    @Path("/power")
    public void power();
    
    @POST
    @Path("/arrowUp")
    public void arrowUp();
    
    @POST
    @Path("/arrowDown")
    public void arrowDown();
    
    @POST
    @Path("/arrowLeft")
    public void arrowLeft();
    
    @POST
    @Path("/arrowRight")
    public void arrowRight();
    
    @POST
    @Path("/confirm")
    public void confirm();
    
    @POST
    @Path("/stop")
    public void stop();
    
    @POST
    @Path("/previous")
    public void previous();
    
    @POST
    @Path("/rewind")
    public void rewind();
    
    @POST
    @Path("/pause")
    public void pause();
    
    @POST
    @Path("/play")
    public void play();
    
    @POST
    @Path("/advance")
    public void advance();
    
    @POST
    @Path("/next")
    public void next();
    
    @POST
    @Path("/record")
    public void record();
    
    @POST
    @Path("/teletext")
    public void teletext();
    
    @POST
    @Path("/teletextRed")
    public void teletextRed();
    
    @POST
    @Path("/teletextGreen")
    public void teletextGreen();
    
    @POST
    @Path("/teletextYellow")
    public void teletextYellow();
    
    @POST
    @Path("/teletextBlue")
    public void teletextBlue();
    
    @POST
    @Path("/returnButton")
    public void returnButton();
    
    @POST
    @Path("/home")
    public void home();
    
    @POST
    @Path("/options")
    public void options();
    
    @POST
    @Path("/guide")
    public void guide();
    
    @POST
    @Path("/button3D")
    public void button3D();
    
    @POST
    @Path("/trackID")
    public void trackID();
    
    @POST
    @Path("/digitalAnalog")
    public void digitalAnalog();
    
    @POST
    @Path("/exit")
    public void exit();
    
    @POST
    @Path("/scene")
    public void scene();
    
    @POST
    @Path("/internetVideo")
    public void internetVideo();
    
    @POST
    @Path("/descriptionProgram")
    public void descriptionProgram();
    
    @POST
    @Path("/manual")
    public void manual();
    
    @POST
    @Path("/syncMenu")
    public void syncMenu();
    
    @POST
    @Path("/audio")
    public void audio();
}
