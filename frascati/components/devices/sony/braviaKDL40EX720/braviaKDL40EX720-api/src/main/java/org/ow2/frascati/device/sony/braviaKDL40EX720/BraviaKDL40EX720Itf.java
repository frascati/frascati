package org.ow2.frascati.device.sony.braviaKDL40EX720;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.upnp.common.UPnPDeviceRenderingControlItf;
import org.ow2.frascati.upnp.common.UPnPDeviceAVTransportItf;
import org.ow2.frascati.upnp.common.UPnPDeviceConnectionManagerItf;
import org.ow2.frascati.upnp.common.UPnPDeviceIRCCItf;

@Service
public interface BraviaKDL40EX720Itf extends UPnPDeviceRenderingControlItf, UPnPDeviceAVTransportItf, UPnPDeviceConnectionManagerItf, UPnPDeviceIRCCItf
{
}
