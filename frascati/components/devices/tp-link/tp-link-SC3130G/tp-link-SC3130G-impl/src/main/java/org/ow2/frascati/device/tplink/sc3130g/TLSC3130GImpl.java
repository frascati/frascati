/**
 * OW2 FraSCAti TP-LINK SC3130G
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.tplink.sc3130g;

import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;

/**
 *
 */
public class TLSC3130GImpl implements TLSC3130GItf
{
    private static final Logger logger= Logger.getLogger(TLSC3130GImpl.class.getName());
    
    private static final String IPADDRESS_PATTERN = 
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    private String login;
    private String password;
    private String ipAdress;
    
    private HttpClient httpClient;
    
    @Init
    public void init()
    {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        AuthScope scope=new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT);
        Credentials credentials = new UsernamePasswordCredentials(login, password);
        CredentialsProvider credentialsProvider=httpclient.getCredentialsProvider();
        credentialsProvider.setCredentials(scope, credentials);
        this.httpClient=httpclient;
    }
    
    public Response getData(Long time)
    {
        StringBuilder requestURIBuilder=new StringBuilder();
        requestURIBuilder.append("http://");
        requestURIBuilder.append(ipAdress);
        requestURIBuilder.append("/jpg/image?");
        requestURIBuilder.append(time);
        String requestURI=requestURIBuilder.toString();
        HttpGet request=new HttpGet(requestURI);
        try
        {
            HttpResponse httpResponse= httpClient.execute(request);
            
            //set response status
            StatusLine statusLine=httpResponse.getStatusLine();
            int status = statusLine.getStatusCode();
            ResponseBuilder responseBuilder=Response.status(status);
            
            String headerName,headerValue;
            for(Header responseHeader :  httpResponse.getAllHeaders())
            {
                headerName=responseHeader.getName();
                headerValue=responseHeader.getValue();
                logger.fine("set header "+headerName+" : "+headerValue);
                responseBuilder=responseBuilder.header(headerName,headerValue);
            }
            
            HttpEntity httpEntity = httpResponse.getEntity();
            byte[] byteImage=IOUtils.toByteArray(httpEntity.getContent());
            String base64Image= Base64.encodeBase64String(byteImage);
            responseBuilder = responseBuilder.entity(base64Image);
            return responseBuilder.build();
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            throw new WebApplicationException(exception, Status.INTERNAL_SERVER_ERROR);
        }
    }
    
    public String getLogin()
    {
        return login;
    }

    @Property(name = "login") 
    public void setLogin(String login)
    {
        this.login = login;
    }
    
    public String getPassword()
    {
        return password;
    }

    @Property(name = "password")
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getIpAdress()
    {
        return ipAdress;
    }

    @Property(name = "ipAdress") 
    public void setIpAdress(String ipAdress)
    {
        if(!ipAdress.matches(IPADDRESS_PATTERN))
        {
            logger.severe("The provided IP adress : "+ipAdress+", is not valid");
            return;
        }
        logger.fine("Set camera IP adress");
        this.ipAdress = ipAdress;
    }
    
    
}
