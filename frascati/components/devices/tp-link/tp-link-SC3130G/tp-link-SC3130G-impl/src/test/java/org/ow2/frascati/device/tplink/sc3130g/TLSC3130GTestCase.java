/**
 * OW2 FraSCAti Zibase
 *
 * Copyright (c) 2011-2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.tplink.sc3130g;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.ws.rs.core.Response;
import javax.xml.namespace.QName;

import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ProcessingContext;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.provider.WidgetProviderItf;

/**
 *
 */
public class TLSC3130GTestCase extends FraSCAtiTestCase
{
	@Test
	public void digihomeTest() throws Exception
	{
		FraSCAti fraSCAti = FraSCAti.newFraSCAti();
		CompositeManager compositeManager = fraSCAti.getCompositeManager();
		QName compositeQname = new QName("digihome-tpSC3130G");
		ProcessingContext processingContext = compositeManager.newProcessingContext();
		processingContext.setContextualProperty("digihome-name", "SimpleOrchestratorTestCase");
		processingContext.setContextualProperty("login", "admin");
		processingContext.setContextualProperty("password", "admin");
		processingContext.setContextualProperty("ip", "192.168.1.17");
		fraSCAti.getCompositeManager().processComposite(compositeQname, processingContext);
		fraSCAti.close();
	}
	
//    @Test
    public final void test() throws InterruptedException, IllegalStateException, IOException
    {
        String tpSC3130GServiceURL=FraSCAtiTestUtils.completeBindingURI("/tpSC3130G");
        FraSCAtiTestUtils.assertWADLExist(true, tpSC3130GServiceURL);
        
        WidgetProviderItf widgetProvider=this.getService(WidgetProviderItf.class, "widgetProvider");
        Widget widget=widgetProvider.getWidget();
        assertEquals("TPLink SC3130G", widget.getName());
        FraSCAtiTestUtils.assertURLExist(true,widget.getIconURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getSmallWidgetURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getLargeWidgetURL());
        
        TLSC3130GItf tlSC3130G = this.getService(TLSC3130GItf.class, "cameraService");
        Response response=tlSC3130G.getData(System.currentTimeMillis());
        assertNotNull(response);
        assertEquals(200, response.getStatus());
    }
}
