/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.bmb;

import org.ow2.frascati.rf.device.state.RFDeviceState;
import org.ow2.frascati.x10.device.X10BinaryDeviceImpl;

/**
 *
 */
@RFDeviceState(name="smokeAlert", value="<dev>CMD</dev>.*<classField>rfAddress</classField>")
public class BMBSD18 extends X10BinaryDeviceImpl
{
    public final static long ALARM_TIMEOUT=3000L;
    
    /**
     * @see org.ow2.frascati.rf.device.RFBinaryDeviceItf#getState()
     */
    public boolean getState()
    {
        Long lastAlertUpdateTime=this.getRfDeviceStateUpdateTime("smokeAlert");
        return System.currentTimeMillis()-lastAlertUpdateTime < ALARM_TIMEOUT;
    }

}
