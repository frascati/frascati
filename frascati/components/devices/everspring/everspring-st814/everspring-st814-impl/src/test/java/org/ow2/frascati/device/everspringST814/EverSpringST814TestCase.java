/**
 * OW2 FraSCAti AN158
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.everspringST814;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.provider.WidgetProviderItf;
/**
 *
 */
public class EverSpringST814TestCase extends FraSCAtiTestCase
{
    @Test
    public final void test() throws InterruptedException, IllegalStateException, IOException
    {
        String everspringST814Url=FraSCAtiTestUtils.completeBindingURI("/ST814");
        FraSCAtiTestUtils.assertWADLExist(true, everspringST814Url);
        
        WidgetProviderItf widgetProvider=this.getService(WidgetProviderItf.class, "widgetProvider");
        Widget widget=widgetProvider.getWidget();
        assertEquals("EverSpring ST814", widget.getName());
        FraSCAtiTestUtils.assertURLExist(true,widget.getIconURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getSmallWidgetURL());
        FraSCAtiTestUtils.assertURLExist(true,widget.getLargeWidgetURL());
    }
}
