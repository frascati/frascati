/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.everspringst814;

import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.rf.device.state.RFDeviceState;
import org.ow2.frascati.zwave.device.ZwaveDeviceImpl;

/**
 *
 */
@Scope("COMPOSITE")
public class EverspringST814Impl extends ZwaveDeviceImpl implements EverspringST184Itf
{
    @RFDeviceState("T=<tem>(.*)</tem>.C")
    private double celciusTemperature;
    
    @RFDeviceState("T=<tem>.*</tem>.C \\((.*).F\\)")
    private double fahrenheitTemperature;
    
    @RFDeviceState("Humidity=<hum>(.*)</hum>%")
    private int humidity;
    
    public EverspringST814Impl()
    {
        this.celciusTemperature=new Double(0.0);
        this.fahrenheitTemperature=new Double(0.0);
        this.humidity=-1;
    }
    
    public double getCelciusTemperature()
    {
        return this.celciusTemperature;
    }

    public double getFahrenheitTemperature()
    {
        return this.fahrenheitTemperature;
    }
    
    public int getHumidity()
    {
        return this.humidity;
    }
}
