/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.everspringan158;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.zwave.device.ZwaveBinaryActuatorItf;

/**
 *
 */
@Service
public interface EverspringAN158Itf extends ZwaveBinaryActuatorItf
{
    @GET
    @Path("/power")
    @Produces(MediaType.TEXT_PLAIN)
    public Integer getPower();
    
    @GET
    @Path("/energy")
    @Produces(MediaType.TEXT_PLAIN)
    public Double getEnergy();
    
}
