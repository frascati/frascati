/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 *
 */
public interface ZibaseItf
{
    /**
     * Send command to Zibase device
     * 
     * @param address    : address of the device to command (A1, A2, ..., A16, B1, ..., P16)
     * @param zbAction   : id of the command to send (0 : ON, 1 : OFF)
     * @param zbProtocol : id of the protocol (X10 : 5, ZWave : 6)
     */
    @POST
    @Path("/command")
    public void sendCommand(@QueryParam("adress") String address,@QueryParam("action") String zbAction,@QueryParam("protocol") String zbProtocol);
}
