/*
        j-zapi - Une implementation Java de l'API de la Zibase
    Copyright (C) 2012 Luc Doré luc.dore@free.fr

        This library is free software; you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2.1 of the License, or
        (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this library; if not, write to the
        Free Software Foundation, Inc., 59 Temple Place, Suite 330,
        Boston, MA 02111-1307 USA
        
        Contributor(s):  Gwenael Cattez
*/

package fr.zapi;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import fr.zapi.utils.NetUtils;

public class Broadcast
{
    private final static Logger logger= Logger.getLogger(Broadcast.class.getName());
    
    public static ZbNopResponse nop() throws SocketException
    {
        InetAddress broadcastAdress, clientAddress;
        ZbNopResponse zbNopResponse;
        for(InterfaceAddress interfaceAddress : NetUtils.getBroadcastableInterfaceAdresses())
        {
            broadcastAdress=interfaceAddress.getBroadcast();
            try
            {
                zbNopResponse= Broadcast.nop(broadcastAdress);
                if(zbNopResponse!=null)
                {
                    clientAddress=interfaceAddress.getAddress();
                    zbNopResponse.setClientInetAddress(clientAddress);
                    return zbNopResponse;
                }
            } catch (Exception e)
            {
                logger.info(" An exception occured when trying Zibase NOP request on adress "+broadcastAdress);
            } 
        }
        return null;
    }
    
    /**
     * nop sur le r�seau local
     * 
     * @return
     * @throws UnknownHostException
     * @throws SocketException 
     */
    public static ZbNopResponse nop(InterfaceAddress ethernetInterfaceAddress) throws SocketTimeoutException,UnknownHostException,IOException
    {
        InetAddress broadcastAdress=ethernetInterfaceAddress.getBroadcast();
        return Broadcast.nop(broadcastAdress);
    }
    
    public static ZbNopResponse nop(InetAddress broadcastAdress) throws SocketTimeoutException,UnknownHostException,IOException
    {
        ZbRequest zbRequest = new ZbRequest((short) 8, null, null, null, null);
        ZbNopResponse zbNopResponse = null;
        byte[] buffer = zbRequest.toBytes();
        DatagramSocket clientSocket = null;

        try
        {
            clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(20 * 1000);

            DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, broadcastAdress, Zibase.port);
            clientSocket.send(sendPacket);

            byte[] ack = new byte[70]; // 70 l'entete
            DatagramPacket receivePacket = new DatagramPacket(ack, ack.length);
            clientSocket.receive(receivePacket);

            zbNopResponse = new ZbNopResponse(new ZbHeader(receivePacket.getData()), receivePacket.getAddress());
            
        } catch (SocketTimeoutException socketTimeoutException)
        {
            throw socketTimeoutException;

        } catch (UnknownHostException unknownHostException)
        {
            throw unknownHostException;

        } catch (IOException ioException)
        {
            throw ioException;

        } finally
        {
            if (clientSocket != null)
            {
                clientSocket.close();
            }
        }
        return zbNopResponse;
    }
}
