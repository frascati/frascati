/*
        j-zapi - Une implementation Java de l'API de la Zibase
    Copyright (C) 2012 Luc Doré luc.dore@free.fr

        This library is free software; you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2.1 of the License, or
        (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this library; if not, write to the
        Free Software Foundation, Inc., 59 Temple Place, Suite 330,
        Boston, MA 02111-1307 USA
        
        Contributor(s):  Gwenael Cattez
 */

package fr.zapi.enums;

/**
 * Enum des actions possibles par la Zibase
 */
public enum ZbAction
{
    OFF(0), ON(1), DIM_BRIGHT(2), TEST1(3), ALL_LIGHTS_ON(4), ALL_LIGHTS_OFF(5), ALL_OFF(6), ASSOC(7), UNASSOC(8);

    private int code;

    private ZbAction(int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return code;
    }

    public static ZbAction fromStringCode(int zbActionCode)
    {
        for (ZbAction zbAction : ZbAction.values())
        {
            if (zbActionCode == zbAction.getCode())
            {
                return zbAction;
            }
        }
        return null;
    }

    public static ZbAction fromStringName(String stringZbAction)
    {
        for (ZbAction zbAction : ZbAction.values())
        {
            if (stringZbAction.equals(zbAction.name()))
            {
                return zbAction;
            }
        }
        return null;
    }

    public static ZbAction fromString(String stringZbAction)
    {
        ZbAction zbAction = null;
        if (stringZbAction != null && !stringZbAction.equals(""))
        {
            try
            {
                int zbActionCode = Integer.valueOf(stringZbAction);
                zbAction = ZbAction.fromStringCode(zbActionCode);
            } catch (NumberFormatException numberFormatException)
            {
                zbAction = ZbAction.fromStringName(stringZbAction);
            }
        }
        return zbAction;

    }

    public String toString()
    {
        return String.valueOf(this.code);
    }

    public static String usage()
    {
        String usage = "Actions allow on Zibase :\n";
        for (ZbAction tmpZbAction : ZbAction.values())
        {
            usage += "\t- " + tmpZbAction.name() + " : " + tmpZbAction.getCode() + "\n";
        }
        usage += "\n";
        return usage;
    }

}
