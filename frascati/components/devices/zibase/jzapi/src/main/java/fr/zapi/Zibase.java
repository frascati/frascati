/*
        j-zapi - Une implementation Java de l'API de la Zibase
    Copyright (C) 2012 Luc Doré luc.dore@free.fr

        This library is free software; you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2.1 of the License, or
        (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this library; if not, write to the
        Free Software Foundation, Inc., 59 Temple Place, Suite 330,
        Boston, MA 02111-1307 USA
        
        Contributor(s):  Gwenael Cattez
*/

package fr.zapi;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.StringTokenizer;

import fr.zapi.enums.ZbAction;
import fr.zapi.enums.ZbProtocol;
import fr.zapi.utils.NetUtils;

public class Zibase
{

    private String ip;
    private InetAddress ipAddress = null;
    public static final int port = 49999;
    
    /**
     * @param ipAddr Adresse IP de la zibase
     */
    public Zibase(String ipAddr)
    {
        ip = ipAddr;
    }

    /**
     * @param ipAdress Adresse IP de la zibase
     */

    public Zibase(InetAddress ipAdress)
    {
        this.ipAddress = ipAdress;
    }

    public InetAddress getIpAddress() throws UnknownHostException
    {
        if (ipAddress == null)
            ipAddress = InetAddress.getByName(ip);
        return ipAddress;
    }

    /**
     * Envoie la requ�te � la Zibase
     * 
     * @param ZbRequest requ�te au format Zibase
     * @return ZbResponse r�ponse de la zibase
     */
    public ZbResponse sendRequest(final ZbRequest zbRequest, boolean readResponse) throws SocketTimeoutException,UnknownHostException,IOException
    {
        byte[] buffer = zbRequest.toBytes();

        return sendRequest(buffer, readResponse);
    }

    /**
     * sendRequest Envoi d'une requ�te � la Zibase
     * 
     * @param buffer
     * @param readResponse
     * @return
     */
    private ZbResponse sendRequest(byte[] buffer, boolean readResponse) throws SocketTimeoutException,UnknownHostException,IOException
    {
        ZbResponse zbResponse = null;
        DatagramSocket clientSocket = null;
        try
        {

            clientSocket = new DatagramSocket();
            clientSocket.setSoTimeout(5000); // 5sec. la Zibase est censée
                                             // répondre dans les 3s.

            DatagramPacket sendPacket = new DatagramPacket(buffer, buffer.length, getIpAddress(), port);
            clientSocket.send(sendPacket);

            if (readResponse == true)
            {
                byte[] ack = new byte[512]; // 70 l'entete + 400 au max de
                                            // message
                DatagramPacket receivePacket = new DatagramPacket(ack, ack.length);
                clientSocket.receive(receivePacket);
                zbResponse = new ZbResponse(receivePacket.getData());
            }

        }
        catch (SocketTimeoutException socketTimeoutException)
        {
            throw socketTimeoutException;

        } catch (UnknownHostException unknownHostException)
        {
            throw unknownHostException;

        } catch (IOException ioException)
        {
            throw ioException;

        } finally
        {
            if (clientSocket != null)
            {
                clientSocket.close();
            }
        }

        return zbResponse;
    }

    /**
     * hostRegistering Permet l'enregistrement du syst�me HOST aupr�s de la
     * ZIBASE
     * 
     * exemples de messages re�us: Zapi linked to host
     * IP=<zip>192.168.0.49</zip> UDP Port=<zudp>9876</zudp>
     * 
     * @param host
     * @param port
     * @throws UnknownHostException
     */
    public void hostRegistering(InetAddress ipAddressHost, int port) throws SocketTimeoutException,UnknownHostException,IOException
    {
        String ipHost = ipAddressHost.getHostAddress();
        if (ipHost.equals("127.0.0.1") || (ipHost.equals("localhost")))
            ipHost = NetUtils.getLocaleIpAddress();

        StringTokenizer st = new StringTokenizer(ipHost, ".");
        int i = 0;
        byte[] b = new byte[4];
        while (st.hasMoreTokens())
            b[i++] = (byte) Integer.parseInt(st.nextToken());

        ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.BIG_ENDIAN);

        ZbRequest zbRequest = new ZbRequest((short) 13, bb.getInt(), port, 0, 0);
        zbRequest.setReserved1("ZapiInit");

        sendRequest(zbRequest, false);
    }

    public void hostRegistering(String host, int port) throws SocketTimeoutException,UnknownHostException,IOException
    {
        InetAddress ipAddressHost = InetAddress.getByName(host);
        hostRegistering(ipAddressHost, port);
    }

    /**
     * hostUnregistering Permet le d�s-enregistrement du syst�me HOST aupr�s de
     * la ZIBASE
     * 
     * @param host
     * @param port
     * @throws UnknownHostException
     */
    public void hostUnregistering(String host, int port) throws SocketTimeoutException,UnknownHostException,IOException
    {
        int ipHost = getIpAsInt(host);
        ZbRequest zbRequest = new ZbRequest((short) 22, ipHost, port, null, null);
        sendRequest(zbRequest, false);
    }

    public void hostUnregistering(InetAddress host, int port) throws SocketTimeoutException,UnknownHostException,IOException
    {
        String ipString = host.getHostAddress();
        hostUnregistering(ipString, port);
    }

    /**
     * getIpAsInt
     * 
     * @param host
     * @return l'ip du host dans un int
     * @throws UnknownHostException
     */
    private int getIpAsInt(String host) throws UnknownHostException
    {
        InetAddress ipAddressHost = InetAddress.getByName(host);

        String ipHost = ipAddressHost.getHostAddress();
        if (ipHost.equals("127.0.0.1"))
            ipHost = InetAddress.getLocalHost().getHostAddress();

        StringTokenizer st = new StringTokenizer(ipHost, ".");
        int i = 0;
        byte[] b = new byte[4];
        while (st.hasMoreTokens())
            b[i++] = (byte) Integer.parseInt(st.nextToken());

        ByteBuffer bb = ByteBuffer.wrap(b);
        bb.order(ByteOrder.BIG_ENDIAN);

        return bb.getInt();
    }

    /**
     * R�cup�rer l'�tat d'un actionneur. La zibase ne recoit que les ordres RF
     * et non les ordres CPL X10, donc l'�tat d'un actionneur X10 connu par la
     * zibase peut �tre erron�
     * 
     * @param string adresse au format X10 de l'actionneur
     * @return true:ON, false:OFF, null
     */
    public Boolean getState(String address) throws SocketTimeoutException,UnknownHostException,IOException
    {
        if (address.length() > 1)
        {
            address = address.toUpperCase();
            ZbRequest request = new ZbRequest();
            request.setCommand((short) 11);
            request.setParam1(5);
            request.setParam3(4);

            int houseCode = address.charAt(0) - 0x41;
            int device = Integer.parseInt(address.substring(1)) - 1;

            device |= (houseCode & 0x0F) << 0x04;
            request.setParam4(device);

            ZbResponse response = sendRequest(request, true);
            if (response != null)
                return (response.getZbHeader().getParam1() == 1) ? Boolean.TRUE : Boolean.FALSE;
        }
        return null;
    }

    /**
     * setEvent
     * 
     * command = decimal 11 param1 = 4 param2 = action param3 = ID param4 =
     * ev_type action : - 0 : inactiver une alerte - 1 : activer une alerte - 2
     * : simuler l'arriv�e d'un ID de detecteur (peut entra�ner l'execution de
     * scenarii)
     */
    private void setEvent(ZbAction action, int id, int evType) throws SocketTimeoutException,UnknownHostException,IOException
    {
        ZbRequest request = new ZbRequest();
        request.setCommand((short) 11);

        request.setParam1(4);
        request.setParam2(action.getCode());
        request.setParam3(id); // ID

        // AXX_PXX_ON = 4
        // AXX_PXX_OFF = 9
        // AXX_PXX_ZW_ON = 19
        // AXX_PXX_ZW_OFF = 20
        // => ID 0..255 pour adresses X10 ou ZWave

        // autres valeurs 0-(2**31-1) : detector id ????
        request.setParam4(evType);

        sendRequest(request, true);
    }

    /**
     * setEventX10
     * 
     * @param id
     * @param on true: ON / false: OFF
     */
    public void setEventX10(int id, boolean on) throws SocketTimeoutException,UnknownHostException,IOException
    {
        setEvent(on ? ZbAction.ON : ZbAction.OFF, id, on ? 4 : 9); // AXX_PXX_ON
                                                                   // /
                                                                   // AXX_PXX_OFF
    }

    /**
     * setEventZWave
     * 
     * @param id
     * @param on true: ON / false: OFF
     */
    public void setEventZWave(int id, boolean on) throws SocketTimeoutException,UnknownHostException,IOException
    {
        setEvent(on ? ZbAction.ON : ZbAction.OFF, id, on ? 19 : 20); // AXX_PXX_ZW_ON
                                                                     // /
                                                                     // AXX_PXX_ZW_OFF
    }

    /**
     * Lance la commande de l'actionneur sp�cifi� par son adresse et son
     * protocol
     * 
     * @param address Adresse au format X10 de l'actionneur (ex: B5)
     * @param action Action � r�aliser (Utiliser l'enum ZbAction)
     * @param protocol Protocole RF (Utiliser l'enum ZbProtocol)
     * @param dimLevel Non support� par la zibase pour l'instant
     * @param nbBurst Nombre d'�missions RF
     */
    public void sendCommand(String address, ZbAction action, ZbProtocol protocol, int dimLevel, int nbBurst) throws SocketTimeoutException,UnknownHostException,IOException
    {
        address = address.toUpperCase();

        ZbRequest request = new ZbRequest();
        request.setCommand((short) 11);

        if (action == ZbAction.DIM_BRIGHT && dimLevel == 0)
            action = ZbAction.OFF;

        int p2 = action.ordinal();
        p2 |= (protocol.ordinal() & 0xFF) << 0x08;

        if (action == ZbAction.DIM_BRIGHT)
            p2 |= (dimLevel & 0xFF) << 0x10;

        if (nbBurst > 1)
            p2 |= (nbBurst & 0xFF) << 0x18;

        request.setParam2(p2);

        if(address!=null && !"".equals(address))
        {
            int p3 = Integer.parseInt(address.substring(1)) - 1;
            request.setParam3(p3);
            char c4 = (char) (address.charAt(0) - 65);
            request.setParam4(c4);   
        }

        sendRequest(request, true);
    }

    /**
     * Lance la commande de l'actionneur sp�cifi� par son adresse et son
     * protocol
     * 
     * @param address
     * @param action
     * @param protocol
     */
    public void sendCommand(String address, ZbAction action, ZbProtocol protocol) throws SocketTimeoutException,UnknownHostException,IOException
    {
        sendCommand(address, action, protocol, 0, 1);
    }

    /**
     * Lance le scenario rep�r� par son num�ro
     * 
     * @param numScenario Le num�ro du scenario (indiqu� entre parenth�se dans
     *        le suivi d'activit� de la console)
     */
    public void launchScenario(int numScenario) throws SocketTimeoutException,UnknownHostException,IOException
    {
        ZbRequest request = new ZbRequest();
        request.setCommand((short) 11);
        request.setParam1(1);
        request.setParam2(numScenario);
        sendRequest(request, true);
    }

    /**
     * Lance un script de commande
     * 
     * @param script le script � lancer exemple: cmd: lm [toto]
     */
    public ZbResponse launchScript(String script) throws SocketTimeoutException,UnknownHostException,IOException
    {
        ZbRequest request = new ZbRequest();
        request.setCommand((short) 16);

        String cmdScript = "cmd: " + script;
        ByteBuffer bb = ByteBuffer.allocate(70 + 96);
        bb.put(request.toBytes());
        bb.put(cmdScript.getBytes());

        return sendRequest(bb.array(), false);
    }

    /**
     * R�cup�re la valeur d'une variable Vx de la Zibase
     * 
     * @param numVar le num�ro de la variable V0 � V14 sont volatiles et remises
     *        � z�ro � chaque relancement du moteur (modif d'un sc�nario par
     *        exemple) V15 � V31 sont persitantes
     * @return int la valeur de la variable demand�e
     */
    public synchronized Integer getVariable(int numVar) throws SocketTimeoutException,UnknownHostException,IOException
    {

        ZbRequest request = new ZbRequest((short) 11, 5, null, 0, numVar);

        ZbResponse response = sendRequest(request, true);
        if (response != null)
            return response.getZbHeader().getParam1();
        return null;
    }

    /**
     * Met �jour une variable Zibase avec la valeur sp�cifi�e
     * 
     * @param num�ro de la variable (0 �31)
     * @param valeur � �crire
     */
    public synchronized ZbResponse setVariable(int numVar, int value) throws SocketTimeoutException,UnknownHostException,IOException
    {

        ZbRequest request = new ZbRequest();
        request.setCommand((short) 11);

        request.setParam1(5);
        request.setParam2(value & 0xFFFF);
        request.setParam3(1); // WRITE
        request.setParam4(numVar);

        return sendRequest(request, true);
    }

    public ZbResponse setVirtualProbeValue(int sensorId, ZbVirtualProbe zbVirtualProbe) throws SocketTimeoutException,UnknownHostException,IOException
    {

        ZbRequest request = new ZbRequest();
        request.setCommand((short) 11);

        request.setParam1(6);
        request.setParam2(sensorId);

        request.setParam3(0); // TODO

        request.setParam4(zbVirtualProbe.getCode());

        return sendRequest(request, true);
    }

}
