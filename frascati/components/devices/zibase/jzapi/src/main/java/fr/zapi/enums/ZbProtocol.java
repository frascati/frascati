/*
        j-zapi - Une implementation Java de l'API de la Zibase
    Copyright (C) 2012 Luc Doré luc.dore@free.fr

        This library is free software; you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2.1 of the License, or
        (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this library; if not, write to the
        Free Software Foundation, Inc., 59 Temple Place, Suite 330,
        Boston, MA 02111-1307 USA
 */
package fr.zapi.enums;

/**
 * 
 * enum des protocoles
 * 
 */
public enum ZbProtocol
{
    PRESET(0), VISONIC433(1), VISONIC868(2), CHACON(3), DOMIA(4), X10(5), // RFX10
    ZWAVE(6), RFS10(7), // TS10
    X2D433(8), X2D433ALRM(8), X2D868(9), X2D868ALRM(9), X2D868INSH(10), // XDD868INTER
    X2D868PIWI(11), // XDD868PILOTWIRE
    X2D868BOAC(12); // XDD868BOILER

    private int code;

    private ZbProtocol(int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return code;
    }

    public static ZbProtocol fromStringCode(int zbProtocolCode)
    {
        for (ZbProtocol zbProtocol : ZbProtocol.values())
        {
            if (zbProtocolCode == zbProtocol.getCode())
            {
                return zbProtocol;
            }
        }
        return null;
    }

    public static ZbProtocol fromStringName(String stringZbProtocol)
    {
        for (ZbProtocol zbProtocol : ZbProtocol.values())
        {
            if (stringZbProtocol.equals(zbProtocol.name()))
            {
                return zbProtocol;
            }
        }
        return null;
    }

    public static ZbProtocol fromString(String stringZbProtocol)
    {
        ZbProtocol zbProtocol = null;
        if (stringZbProtocol != null && !stringZbProtocol.equals(""))
        {
            try
            {
                int zbProtocolCode = Integer.valueOf(stringZbProtocol);
                zbProtocol = ZbProtocol.fromStringCode(zbProtocolCode);
            } catch (NumberFormatException numberFormatException)
            {
                zbProtocol = ZbProtocol.fromStringName(stringZbProtocol);
            }
        }
        return zbProtocol;

    }

    public String toString()
    {
        return String.valueOf(this.code);
    }

    public static String usage()
    {
        String usage = "Protocols allow on Zibase :\n";
        for (ZbProtocol tmpZbProtocol : ZbProtocol.values())
        {
            usage += "\t- " + tmpZbProtocol.name() + " : " + tmpZbProtocol.getCode() + "\n";
        }
        usage += "\n";
        return usage;
    }
}
