/*
        j-zapi - Une implementation Java de l'API de la Zibase
    Copyright (C) 2012 Luc Doré luc.dore@free.fr

        This library is free software; you can redistribute it and/or modify
        it under the terms of the GNU Lesser General Public License as published by
        the Free Software Foundation; either version 2.1 of the License, or
        (at your option) any later version.

        This library is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
        See the GNU Lesser General Public License for more details.

        You should have received a copy of the GNU Lesser General Public License
        along with this library; if not, write to the
        Free Software Foundation, Inc., 59 Temple Place, Suite 330,
        Boston, MA 02111-1307 USA
        
        Contributor(s):  Gwenael Cattez
*/

package fr.zapi.utils;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

public class NetUtils
{
    private static final String IPV4_PATTERN="\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b";
    
    private final static Logger logger= Logger.getLogger(NetUtils.class.getName());
    
    /**
     * getLocaleIpAddress
     * 
     * recherche de l'adresse ip locale � communiquer � la Zibase pas toujours
     * facile de retrouver la bonne ip: on ignore les ip v6 il faut trouver l'ip
     * de la machine correspondant au r�seau local donc ignore aussi les boucles
     * locales, les VPN, et autres interfaces... en conclusion prend la premiere
     * adresse ip v4 locale qui ne soit pas une boucle
     * 
     * @return l'ip locale ou null
     */
    public static String getLocaleIpAddress() throws SocketException
    {

        String ip = null;
        Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
        while (e.hasMoreElements())
        {

            Enumeration<InetAddress> i = e.nextElement().getInetAddresses();
            while (i.hasMoreElements())
            {
                InetAddress a = i.nextElement();
                if ((a.isLoopbackAddress() == false) && (a.isSiteLocalAddress() == true) && ((a instanceof Inet6Address) == false)
                        && (a.getHostAddress().indexOf("192") == 0))
                    ip = a.getHostAddress();
            }
        }

        return ip;
    }

    public static InetAddress getIPAddress()
    {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.indexOf("win") >= 0)
        {
            try
            {
                return Inet4Address.getLocalHost();
            } catch (UnknownHostException e)
            {
                logger.warning("Can't find Inet4Address.getLocalHost() in getIPAddress()");
            }
        } else
        {
            try
            {
                Enumeration<NetworkInterface> nInterfaces = NetworkInterface.getNetworkInterfaces();
                while (nInterfaces.hasMoreElements())
                {
                    Enumeration<InetAddress> inetAddresses = nInterfaces.nextElement().getInetAddresses();
                    while (inetAddresses.hasMoreElements())
                    {
                        InetAddress ret = inetAddresses.nextElement();
                        String address = ret.getHostAddress();
                        logger.info("Address : " + address + ", taille split : " + address.split(".").length);
                        if (!address.equals("127.0.0.1") && address.matches(IPV4_PATTERN))
                        {
                            return ret;
                        }
                    }
                }
            } catch (SocketException e)
            {
                logger.severe("SocketException in getIPAddress()");
            }
        }
        return null;
    }
    
    /**
     * getEthernetInterfaceAddress
     * 
     * @return InterfaceAddress of Ethernet connexion or null 
     * @throws SocketException 
     */
    /**
     * getEthernetInterfaceAddress
     * 
     * @return InterfaceAddress of Ethernet connexion or null 
     */
    public static InterfaceAddress getEthernetInterfaceAddress()
    {
        try
        {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            NetworkInterface networkInterfaceTmp;
            String networkInterfaceName;
            boolean isNetworkInterfaceUp;
            Enumeration<InetAddress> inetAdresses;
            while (networkInterfaces.hasMoreElements())
            {
                networkInterfaceTmp = networkInterfaces.nextElement();
                networkInterfaceName = networkInterfaceTmp.getName();
                isNetworkInterfaceUp = networkInterfaceTmp.isUp();
                inetAdresses = networkInterfaceTmp.getInetAddresses();
                if (networkInterfaceName.startsWith("eth") && isNetworkInterfaceUp && inetAdresses.hasMoreElements())
                {
                    return networkInterfaceTmp.getInterfaceAddresses().get(0);
                }
            }
            return null;
        } catch (Exception e)
        {
            return null;
        }

    }
    
    public static List<InterfaceAddress> getBroadcastableInterfaceAdresses() throws SocketException
    {
        List<InterfaceAddress> inetAddresses=new ArrayList<InterfaceAddress>();
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        InterfaceAddress newtworkInterfaceAdress;
        InetAddress broadcastInetAdress;
        for(NetworkInterface networkInterface : Collections.list(networkInterfaces))
        {
            try
            {
                logger.fine("NetworkInterface "+networkInterface.getDisplayName()+" , name "+networkInterface.getName());
                if(networkInterface.isUp() && !networkInterface.isLoopback() && networkInterface.getInetAddresses().hasMoreElements())
                {
                    logger.fine("is available");
                    newtworkInterfaceAdress=networkInterface.getInterfaceAddresses().get(0);
                    broadcastInetAdress=newtworkInterfaceAdress.getBroadcast();
                    logger.fine("broadcastInetAdress "+broadcastInetAdress);
                    if(broadcastInetAdress!=null)
                    {
                        inetAddresses.add(newtworkInterfaceAdress);
                    }
                }
            } catch (SocketException e)
            {
                logger.fine("SocketException when proceeding NetworkInterface : "+networkInterface.getDisplayName());
            }
        }
        return inetAddresses;
        
    }
}