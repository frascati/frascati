/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase;

import org.junit.Test;
import org.ow2.frascati.rf.exception.RFException;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.zwave.controller.ZWaveControllerItf;

/**
 *
 */
public class ZibaseTestCase extends FraSCAtiTestCase
{
    @Test
    public final void test() throws InterruptedException, RFException
    {
//        ZibaseItf zibaseService=this.getService(ZibaseItf.class, "zibaseMockService");
        ZibaseItf zibaseService=this.getService(ZibaseItf.class, "zibaseService");
        zibaseService.sendCommand("P15","1","6");
        zibaseService.sendCommand("P15","OFF","ZWAVE");
        ZWaveControllerItf zWaveController=this.getService(ZWaveControllerItf.class, "zwaveController");
        zWaveController.sendCommand("P15", "ON");
        zWaveController.sendCommand("A3", "ON");
        zWaveController.sendCommand("A3", "OFF");
//        Thread.sleep(10000);
        
//        ZibaseMockItf zibaseMockService=this.getService(ZibaseMockItf.class, "zibaseMockService");
//        zibaseMockService.sendMessage("test message");
//        zibaseMockService.sendMessage("Received radio ID (<rf>ZWAVE</rf>  <dev>CMD</dev>  Batt=<bat>Ok</bat>): <id>ZA4</id>");
//        zibaseMockService.sendMessage("ZWave message -  ADD process aborted (always REMOVE an already associated device before ADD).");
    }
}