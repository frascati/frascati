/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.device.zibase.exception.CommandFailedException;
import org.ow2.frascati.device.zibase.exception.InvalidActionException;
import org.ow2.frascati.device.zibase.exception.InvalidProtocolException;
import org.ow2.frascati.device.zibase.exception.ZibaseUnreachableException;

import fr.zapi.Broadcast;
import fr.zapi.ZbNopResponse;
import fr.zapi.Zibase;
import fr.zapi.enums.ZbAction;
import fr.zapi.enums.ZbProtocol;
import fr.zapi.utils.NetUtils;


/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class ZibaseImpl implements ZibaseItf
{
    /* The logger */
    private static final Logger logger = Logger.getLogger(ZibaseImpl.class.getName());

    /* IP of the zibase */
    public String zibaseIp;
    
    /* The port where Zibase forward messages */
    private int zibasePort = 49999;

    /* current IP of ZibaseClient */
    private InetAddress myInetAddress;

    /* The current Zibase object*/
    private Zibase zibase;
    
    /* The listener that receive all messages coming from Zibase */
    private ZibaseClient zibaseClient;
    
    private List<ZibaseListenerItf> zibaseListeners;
    
    @Init
    public void init() throws UnknownHostException
    {
        logger.info("ZibaseImpl init");
        boolean isZibaseInit=false;
        
        /**Try to reach Zibase on the provided IP*/
        if(this.zibaseIp!=null && !this.zibaseIp.equals(""))
        {
            try
            {
                this.zibase=new Zibase(zibaseIp);
                this.myInetAddress=NetUtils.getIPAddress();
                logger.info("Zibase found, IP : "+this.zibase.getIpAddress()+", client IP : "+this.myInetAddress);
                isZibaseInit=true;
            }
            catch (UnknownHostException e)
            {
                logger.warning("Can't find Zibase on provided IP "+this.zibaseIp);
            }
        }
        
        /**If the Zibase has not been found, broadcast the network with nop request*/
        if(!isZibaseInit)
        {
            ZbNopResponse nopResponse;
            try
            {
                nopResponse = Broadcast.nop();
            } catch (SocketException socketException)
            {
                logger.severe("Zibase unreachable : no available networks");
                throw new ZibaseUnreachableException(socketException);
            }
            
            if(nopResponse==null)
            {
                logger.severe("Zibase unreachable");
                throw new ZibaseUnreachableException();
            }
            
            this.zibase = new Zibase(nopResponse.getInetAddress());
            this.myInetAddress=nopResponse.getClientInetAddress();
            logger.info("Zibase found, IP : "+this.zibase.getIpAddress()+", client IP : "+this.myInetAddress);
        }

        /** Try to register to Zibase to be notified by all new incoming messages */
        /** Client registering is not compulsory*/
        try
        {
            registerZibaseClient();
            logger.info("Client is now registered on Zibase, IP : "+this.myInetAddress.getHostAddress());
        }
        catch(UnknownHostException unknownHostException)
        {
            logger.severe("Can't get IP for this client : "+unknownHostException.getMessage());
        }
        catch (SocketException socketException)
        {
            logger.severe("Can't create socket on Client to receive Zibase messages : "+socketException.getMessage());
        }
        catch (IOException ioException)
        {
            String message="an error occured when trying to send command to Zibase";
            logger.severe("Zibase unreachable : "+message);
        }
        
    }
    
    public void sendCommand(String address, String zbActionString, String zbProtocolString) throws InvalidActionException,InvalidProtocolException,CommandFailedException
    {
        /*Get ZbAction Object related to the action id */
        ZbAction zbAction=ZbAction.fromString(zbActionString);
        if(zbAction==null)
        {
            throw new InvalidActionException(zbActionString);
        }
        
        /*Get ZbProtocol Object related to the protocol id */
        ZbProtocol zbProtocol=ZbProtocol.fromString(zbProtocolString);
        if(zbProtocol==null)
        {
            throw new InvalidProtocolException(zbProtocolString);
        }
        
        /*Send command to Zibase*/
        try
        {
            this.zibase.sendCommand(address, zbAction, zbProtocol);
        }
        catch (SocketTimeoutException socketTimeoutException)
        {
            String message="connection takes too long";
            logger.severe("Zibase unreachable : "+message);
            throw new CommandFailedException(message);
        }
        catch (UnknownHostException unknownHostException)
        {
            String message="can't find zibase on the local network";
            logger.severe("Zibase unreachable : "+message);
            throw new CommandFailedException(message);

        } catch (IOException ioException)
        {
            String message="an error occured when trying to send command to Zibase";
            logger.severe("Zibase unreachable : "+message);
            throw new CommandFailedException(message);
        }
    }
    
    /**
     * Register Client to Zibase in order to be forwarded by Message going through Zibase device
     * 
     * @throws SocketTimeoutException
     * @throws UnknownHostException
     * @throws IOException
     */
    private void registerZibaseClient() throws SocketTimeoutException,UnknownHostException,IOException
    {
            this.zibase.hostRegistering(this.myInetAddress, this.zibasePort);
            this.zibaseClient = new ZibaseClient(myInetAddress, this.zibasePort, this.zibaseListeners);
            Thread listenerThread = new Thread(zibaseClient);
            listenerThread.start();
    }
    /**
     * Unregister Client from Zibase
     * 
     * @throws SocketTimeoutException
     * @throws UnknownHostException
     * @throws IOException
     */
    private void unregisterZibaseClient() throws SocketTimeoutException,UnknownHostException,IOException
    {
            this.zibase.hostUnregistering(this.myInetAddress, this.zibasePort);
            this.zibaseClient.stop();
    }
    
    /**
     * @return Port use to receive message from Zibase
     */
    public int getZibasePort()
    {
        return zibasePort;
    }
    
    /**
     * Set the port used to receive message from Zibase.
     * First unregistering and destroy existing listener, then create a new one and register it 
     * 
     * @param zibasePort The new port 
     */
    public void setZibasePort(int zibasePort)
    {
        if(this.zibasePort!=zibasePort)
        {
            try
            {
                unregisterZibaseClient();
                this.zibasePort = zibasePort;
                registerZibaseClient();
            }
            catch(UnknownHostException unknownHostException)
            {
                logger.severe("Zibase seems unreachable : "+unknownHostException.getMessage());
            }
            catch (SocketException socketException)
            {
                logger.severe("Can't create socket on port "+zibasePort+" to receive Zibase messages : "+socketException.getMessage());
            }
            catch (IOException ioException)
            {
                logger.severe("Can't send registring command to zibase : "+ioException.getMessage());
            }
        }
    }
    
    public String getZibaseIp()
    {
        return zibaseIp;
    }

    @Property(name="zibaseIp")
    public void setZibaseIp(String zibaseIp)
    {
        this.zibaseIp = zibaseIp;
    }
    
    public List<ZibaseListenerItf> getZibaseListeners()
    {
        return zibaseListeners;
    }

    @Reference(name="zibaseListeners")
    public void setZibaseListeners(List<ZibaseListenerItf> zibaseListeners)
    {
        this.zibaseListeners = zibaseListeners;
        if(this.zibaseClient!=null)
        {
            this.zibaseClient.setZibaseListeners(this.zibaseListeners);
        }
    }
}
