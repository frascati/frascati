/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase;

import java.util.List;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Reference;

/**
 *
 */
public class ZibaseMock implements ZibaseMockItf
{
    /* The logger */
    private static final Logger logger = Logger.getLogger(ZibaseMock.class.getName());

    private List<ZibaseListenerItf> zibaseListeners;

    /* (non-Javadoc)
     * @see org.ow2.frascati.device.zibase.ZibaseItf#sendCommand(java.lang.String, java.lang.String, java.lang.String)
     */
    public void sendCommand(String address, String zbAction, String zbProtocol)
    {
        logger.info("sendCommand adress : "+address+", zbAction : "+zbAction+", zbProtocol : "+zbProtocol);
    }
    
    public List<ZibaseListenerItf> getZibaseListeners()
    {
        return zibaseListeners;
    }

    @Reference(name="zibaseListeners")
    public void setZibaseListeners(List<ZibaseListenerItf> zibaseListeners)
    {
        this.zibaseListeners = zibaseListeners;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.device.zibase.ZibaseMockItf#sendMessage(java.lang.String)
     */
    public void sendMessage(String message)
    {
//       Object zibaseMEssageObject=ZibaseClient.proceedRawMessage(message);
//       for(ZibaseListenerItf zibaseListener : this.zibaseListeners)
//       {
//           zibaseListener.onReceived(zibaseMEssageObject);
//       }
    }
}
