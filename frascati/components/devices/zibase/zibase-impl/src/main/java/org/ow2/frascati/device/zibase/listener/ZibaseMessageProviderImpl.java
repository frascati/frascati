/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase.listener;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.device.zibase.ZibaseListenerItf;
import org.ow2.frascati.device.zibase.ZibaseMessage;
import org.ow2.frascati.device.zibase.ZibaseMessageProviderItf;
import org.ow2.frascati.device.zibase.ZibaseMessages;

/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class ZibaseMessageProviderImpl implements ZibaseListenerItf, ZibaseMessageProviderItf
{
    private ZibaseMessages zibaseMessages;

    @Init
    public void init()
    {
        this.zibaseMessages=new ZibaseMessages();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.ow2.frascati.device.zibase.ZibaseListenerItf#onReceived(java.lang
     * .String)
     */
    public void onReceived(ZibaseMessage zibaseMessage)
    {
        zibaseMessages.getZibaseMessages().add(zibaseMessage);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.ow2.frascati.device.zibase.ZibaseMessageBrokerItf#getZibaseMessages
     * (java.lang.String)
     */
    public ZibaseMessages getZibaseMessages(String time)
    {
        if (time == null || "".equals(time))
        {
            return this.zibaseMessages;
        }

        Long from = null;
        try
        {
            from = Long.parseLong(time);
        } catch (NumberFormatException numberFormatException)
        {
            return this.zibaseMessages;
        }

        ZibaseMessages zibaseMessages=new ZibaseMessages();
        for (ZibaseMessage zibaseMessage : zibaseMessages.getZibaseMessages())
        {
            if (zibaseMessage.getTime() > from)
            {
                zibaseMessages.getZibaseMessages().add(zibaseMessage);
            }
        }
        return zibaseMessages;
    }

}
