/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.List;
import java.util.logging.Logger;

import fr.zapi.ZbResponse;

/**
 *
 */
public class ZibaseClient implements Runnable
{
    /* The logger */
    private static final Logger logger = Logger.getLogger(ZibaseClient.class.getName());
    
    private InetAddress listenedInetAddress;
    private int listenedPort;
    
    private List<ZibaseListenerItf> zibaseListeners;
    
    private boolean isStopped;

    public ZibaseClient(InetAddress listenedInetAddress, int listenedPort, List<ZibaseListenerItf> zibaseListeners)
    {
        this.listenedInetAddress = listenedInetAddress;
        this.listenedPort = listenedPort;
        this.zibaseListeners=zibaseListeners;
        this.isStopped = false;
    }
    
    public void run()
    {
        DatagramSocket clientSocket = null;
        try
        {
            clientSocket = new DatagramSocket(this.listenedPort, this.listenedInetAddress);
            byte[] ack = new byte[512]; // 70 l'entete + 400 au max de
                                        // message
            DatagramPacket receivePacket = new DatagramPacket(ack, ack.length);
            ZbResponse zbResponse;
            ZibaseMessage zibaseMessage = null;
            while (!isStopped())
            {
                clientSocket.receive(receivePacket);
                zbResponse = new ZbResponse(receivePacket.getData());
                
                zibaseMessage=new ZibaseMessage();
                zibaseMessage.setTime(System.currentTimeMillis());
                zibaseMessage.setMessage(zbResponse.getMessage().trim());
                logger.info("ZibaseClient received : "+zibaseMessage.getMessage());
                for(ZibaseListenerItf zibaseListener : this.zibaseListeners)
                {
                    zibaseListener.onReceived(zibaseMessage);
                }
                ack= new byte[512];
                receivePacket.setData(ack);
            }
        } catch (SocketException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally
        {
            if (clientSocket != null)
            {
                clientSocket.close();
            }
        }
    }
    
    public void setZibaseListeners(List<ZibaseListenerItf> zibaseListeners)
    {
        this.zibaseListeners = zibaseListeners;
    }
    
    public void stop()
    {
        isStopped = true;
        logger.info("Current Zibase listener has been stopped");
    }
    
    public boolean isStopped()
    {
        return isStopped;
    }
}