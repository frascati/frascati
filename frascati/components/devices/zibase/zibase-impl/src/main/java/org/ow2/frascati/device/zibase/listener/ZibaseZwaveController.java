/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.device.zibase.listener;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.device.zibase.ZibaseItf;
import org.ow2.frascati.device.zibase.ZibaseMessage;
import org.ow2.frascati.rf.address.RFAddress;
import org.ow2.frascati.rf.address.RFInvalidAdressException;
import org.ow2.frascati.rf.exception.RFException;
import org.ow2.frascati.zwave.controller.ZWaveControllerItf;
import org.ow2.frascati.zwave.protocol.ZwaveProtocol;

import fr.zapi.enums.ZbAction;
import fr.zapi.enums.ZbProtocol;

/**
 *
 */
@Scope("COMPOSITE")
public class ZibaseZwaveController extends ZibaseRfController<ZwaveProtocol> implements ZWaveControllerItf
{
    public final static String PROCESS_START="ZWave message - Push the association device button several times before 60s \\(LEDs on\\) to complete (ADD|REMOVE) device operation";
    public final static String PROCESS_ABORTED="ZWave message -  (ADD|REMOVE) process aborted.*";
    public final static String PROCESS_FINISHED="ZWave message -  (ADD|REMOVE) process completed.";
    public final static String ADD_PROCESS="ADD";
    public final static String REMOVE_PROCESS="REMOVE";
    
    public final static String WARNING_ZWAVE="ZWave warning -  (.*)";
    public final static String WELCOME_ZWAVE_REGEX="Welcome to Zwave device(.*)address: Z([A-P][1-9][0-6]?)";
    public final static String UNLINK_ZWAVE_REGEX="Unlink Zwave Device(.*)Address: Z([A-P][1-9][0-6]?)";
    
    @Reference(name="zibase")
    private ZibaseItf zibase;

    private String currentProcess;
    private RFAddress<ZwaveProtocol> lastIncludedDeviceAddress;
    private RFAddress<ZwaveProtocol> lastExcludedDeviceAddress;
    
    /**
     * @param rfProtocol
     */
    public ZibaseZwaveController()
    {
        super(new ZwaveProtocol());
    }
    
    /**
     * @see org.ow2.frascati.device.zibase.ZibaseListenerItf#onReceived(java.lang.String)
     */
    public void onReceived(ZibaseMessage zibaseMessage)
    {
        super.onReceived(zibaseMessage);
        
        logger.fine("onReceived "+zibaseMessage.getMessage());
        String zibaseRawMessage=zibaseMessage.getMessage();
        
        if(zibaseRawMessage.matches(PROCESS_START))
        {
            this.currentProcess=zibaseRawMessage.replaceAll(PROCESS_START, "$1");
            logger.fine("PROCESS_START");
        }
        else if(zibaseRawMessage.matches(PROCESS_ABORTED))
        {
            logger.fine("PROCESS_ABORTED");
            this.currentProcess=null;
            this.lastIncludedDeviceAddress=null;
            this.lastExcludedDeviceAddress=null;
        }
        else if(zibaseRawMessage.matches(PROCESS_FINISHED))
        {
            logger.fine("PROCESS_FINISHED");
            this.currentProcess=null;
        }
        else if(zibaseRawMessage.matches(WELCOME_ZWAVE_REGEX))
        {
            logger.fine("WELCOME_ZWAVE_REGEX");
            String includedDeviceStringAddress=zibaseRawMessage.replaceAll(WELCOME_ZWAVE_REGEX, "$2");
            try
            {
                this.lastIncludedDeviceAddress=this.processRFRawAddress(includedDeviceStringAddress);
            }
            catch (RFInvalidAdressException e)
            {
                logger.severe(includedDeviceStringAddress+" is not a valid address");
            }
            
        }
        else if(zibaseRawMessage.matches(UNLINK_ZWAVE_REGEX))
        {
            logger.fine("UNLINK_ZWAVE_REGEX");
            String excludedDeviceStringAddress=zibaseRawMessage.replaceAll(UNLINK_ZWAVE_REGEX, "$2");
            try
            {
                this.lastExcludedDeviceAddress=this.processRFRawAddress(excludedDeviceStringAddress);
            } catch (RFInvalidAdressException e)
            {
                logger.severe(excludedDeviceStringAddress+" is not a valid address");
            }
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.zwave.ZWaveController#sendCommand(java.lang.String, java.lang.String)
     */
    public void sendCommand(String address, String command) throws RFException
    {
        try
        {
            this.zibase.sendCommand(address, command, ZbProtocol.ZWAVE.name());
            logger.info("ZibaseZwaveController send command "+command+" to address "+address);
        } catch (Exception e)
        {
            throw new RFException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.zwave.ZWaveController#inclusion()
     */
    public void inclusion() throws RFException
    {
        try
        {
            this.zibase.sendCommand("", ZbAction.ASSOC.name(), ZbProtocol.ZWAVE.name());
        }
        catch (Exception e)
        {
            throw new RFException(e);
        }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.zwave.ZWaveController#exclusion()
     */
    public void exclusion() throws RFException
    {
        try
        {
            this.zibase.sendCommand("", ZbAction.UNASSOC.name(), ZbProtocol.ZWAVE.name());
        }
        catch (Exception e)
        {
            throw new RFException(e);
        }
    }

    public boolean isIncluding()
    {
        logger.fine("isIncluding "+(ADD_PROCESS.equals(this.currentProcess)));
        return ADD_PROCESS.equals(this.currentProcess);
    }

    public boolean isExcluding()
    {
        logger.fine("isExcluding "+(REMOVE_PROCESS.equals(this.currentProcess)));
        return REMOVE_PROCESS.equals(this.currentProcess);
    }

    public RFAddress<ZwaveProtocol> getLastIncludedDeviceAddress()
    {
        if(this.lastIncludedDeviceAddress==null)
        {
            return null;
        }
        return lastIncludedDeviceAddress;
    }
    
    public RFAddress<ZwaveProtocol> getLastExcludedDeviceAddress()
    {
        if(this.lastExcludedDeviceAddress==null)
        {
            return null;
        }
        return lastExcludedDeviceAddress;
    }

    public boolean isBusy()
    {
        return currentProcess!=null;
    }
}
