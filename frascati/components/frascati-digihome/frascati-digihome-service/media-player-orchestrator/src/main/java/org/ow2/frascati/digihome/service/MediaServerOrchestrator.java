/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.service;

import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.rf.device.RFDeviceStateBundle;
import org.ow2.frascati.rf.device.RFDeviceStatesBundle;
import org.ow2.frascati.rf.device.listener.RFDeviceListenerItf;
import org.ow2.frascati.rf.exception.RFException;
import org.ow2.frascati.upnp.common.UPnPDeviceAVTransportItf;

/**
 *
 */
@Scope("COMPOSITE")
public class MediaServerOrchestrator implements RFDeviceListenerItf
{
	private final static Logger logger = Logger.getLogger(MediaServerOrchestrator.class.getName());
	
    @Reference(name = "media-server")
    private UPnPDeviceAVTransportItf mediaRenderer;

    private int lastActionSleepTime=100000;

	private Long lastActionTime = 0L;
    
	@Property(name="mediaName")
	private String mediaName;
	
	public void notifyRFDeviceStateChange(RFDeviceStatesBundle rfDeviceStatesBundle)
	{
		if (System.currentTimeMillis() - lastActionTime < lastActionSleepTime)
		{
			logger.info(this.getClass().getSimpleName() + " is sleeping, no orchestration done");
			return;
		}

		for (RFDeviceStateBundle rfDeviceStateBundle : rfDeviceStatesBundle.getRFDeviceStatesBundles())
		{
			if (rfDeviceStateBundle.getName().equals("alarm") || rfDeviceStateBundle.getName().equals("state"))
			{
				logger.info(rfDeviceStateBundle.getName() + " receveived alarm (" + rfDeviceStateBundle.getType() + ") : " + rfDeviceStateBundle.getValue());
				
				String mediaURI;
				if(mediaName==null || !mediaName.equals("alarm"))
				{
					mediaURI="http://193.51.236.251:58931/FreeMi/0%2FC%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos%5CLEDAteaser.mp4%2F0";
				}
				else
				{
					mediaURI="http://193.51.236.251:58931/FreeMi/0%2FC%3A%5CUsers%5CPublic%5CMusic%5CSample%20Music%5Calarme.mp3%2F0";
				}
				mediaRenderer.setAVTransportURI("0",mediaURI, "");
				mediaRenderer.play("0", "1");
				lastActionTime = System.currentTimeMillis();
				break;
			}

		}
	}
}
