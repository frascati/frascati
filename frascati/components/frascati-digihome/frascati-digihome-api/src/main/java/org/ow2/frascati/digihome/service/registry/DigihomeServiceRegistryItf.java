/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.service.registry;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.digihome.DigihomeServiceDescriptor;
import org.ow2.frascati.digihome.DigihomeServiceDescriptors;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.exception.InvalidDigihomeIdException;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.stringlist.StringList;

/**
 *
 */
@Service
public interface DigihomeServiceRegistryItf
{
    public boolean addDigihomeService(DigihomeService digihomeService);
    public DigihomeService getDigihomeService(String digihomeId);
    
    @GET
    @Path("/digihomeServiceDescriptors")
    @Produces({"application/xml"})
    public DigihomeServiceDescriptors getDigihomeServiceDescriptors();
    
    @GET
    @Path("/digihomeServiceDescriptor/{uuidString}")
    @Produces({"application/xml"})
    public DigihomeServiceDescriptor getDigihomeServiceDescriptor(@PathParam("uuidString")String uuidString) throws InvalidDigihomeIdException;
    
    public List<DigihomeService> getDigihomeServices(DigihomeServiceStatus digihomeServiceStatus);
    
    @GET
    @Path("/availableServices")
    @Produces({"application/xml"})
    public StringList getAvailableServices() throws DigihomeException;
    
    @GET
    @Path("/availableServices/{serviceClassString}")
    @Produces({"application/xml"})
    public StringList getAvailableServices(@PathParam("serviceClassString")String serviceClassString) throws DigihomeException;

    public List<Port> getAvailableServices(Class<?> serviceClass); 

    
    
    @GET
    @Path("/availableReferences")
    @Produces({"application/xml"})
    public StringList getAvailableReferences() throws DigihomeException;
    
    @GET
    @Path("/availableReferences/{referenceClassString}")
    @Produces({"application/xml"})
    public StringList getAvailableReferences(@PathParam("referenceClassString")String referenceClassString) throws DigihomeException;

    public List<Port> getAvailableReferences(Class<?> referenceClass);
    
    public Class<?> getPortClass(Port port) throws DigihomeException;
}
