/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.digihome.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceDescriptor;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.mojo.digihomecontribution.DigihomeContribution;
import org.ow2.frascati.remote.introspection.resources.Port;

/**
 *
 */
public class DigihomeService
{
    private DigihomeServiceStatus digihomeServiceStatus;
    
    private String digihomeId;

    private DigihomeContribution digihomeContribution;

    private DigihomeParameters digihomeParameters;
    
    private List<String> deployedComposites;
    
    private DigihomePortMap digihomeServicePorts;
    
    private DigihomePortMap digihomeReferencePorts;
    
    public DigihomeService(String digihomeId)
    {
        this.digihomeId = digihomeId;
        this.digihomeServiceStatus = DigihomeServiceStatus.INSTANTIATED;
        this.deployedComposites = new ArrayList<String>();
        this.digihomeReferencePorts = new DigihomePortMap();
        this.digihomeServicePorts = new DigihomePortMap();
    }
    
    public void addService(Class<?> serviceClass, Port servicePort)
    {
        this.digihomeServicePorts.put(serviceClass, servicePort);
        DigihomeLogger.log(this, "Add Service "+serviceClass.getSimpleName()+" : "+servicePort.getPath());
    }
    
    public List<Port> getServices()
    {
        return this.digihomeServicePorts.portValues();
    }
    
    public List<Port> getServices(Class<?> portClass)
    {
    	if(portClass == null)
    	{
    		return this.getServices();
    	}
        return this.digihomeServicePorts.get(portClass);
    }
    
    public void addReference(Class<?> referenceClass, Port referencePort)
    {
        this.digihomeReferencePorts.put(referenceClass, referencePort);
        DigihomeLogger.log(this, "Add reference "+referenceClass.getSimpleName()+" : "+referencePort.getPath());
    }
    
    public List<Port> getReferences()
    {
        return this.digihomeReferencePorts.portValues();
    }
    
    public List<Port> getReferences(Class<?> portClass)
    {
    	if(portClass == null)
    	{
    		return this.getReferences();
    	}
        return this.digihomeReferencePorts.get(portClass);
    }

    public DigihomeServiceStatus getDigihomeServiceStatus()
    {
        return digihomeServiceStatus;
    }

    public void setDigihomeServiceStatus(DigihomeServiceStatus digihomeServiceStatus)
    {
        this.digihomeServiceStatus = digihomeServiceStatus;
    }

    public String getDigihomeId()
    {
        return digihomeId;
    }

    public void setDigihomeId(String digihomeId)
    {
        this.digihomeId = digihomeId;
    }

    public DigihomeContribution getDigihomeContribution()
    {
        return digihomeContribution;
    }

    public void setDigihomeContribution(DigihomeContribution digihomeContribution)
    {
        this.digihomeContribution = digihomeContribution;
    }

    public DigihomeParameters getDigihomeParameters()
    {
        return digihomeParameters;
    }

    public void setDigihomeParameters(DigihomeParameters digihomeParameters)
    {
        this.digihomeParameters = digihomeParameters;
    }

    public List<String> getDeployedComposites()
    {
        return deployedComposites;
    }

    public void setDeployedComposites(List<String> deployedComposites)
    {
        this.deployedComposites = deployedComposites;
    }
    
    public DigihomeServiceDescriptor getDescriptor()
    {
    	DigihomeServiceDescriptor descriptor= new DigihomeServiceDescriptor();
    	descriptor.setDigihomeId(digihomeId);
    	descriptor.setStatus(digihomeServiceStatus);
    	descriptor.setDigihomeParameters(digihomeParameters);
    	return descriptor;
    }
}
