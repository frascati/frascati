/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.ow2.frascati.remote.introspection.resources.Port;
/**
 *
 */
public class DigihomePortMap extends HashMap<Class<?>, List<Port>>
{

    private static final long serialVersionUID = 1L;

    @Override
    public boolean containsKey(Object key)
    {
        if(key instanceof Class<?>)
        {
            Class<?> keyClass = (Class<?>) key;
            for(Class<?> myKeyClass : this.keySet())
            {
                if((keyClass.isAssignableFrom(myKeyClass)))
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    public void put(Class<?> keyClass, Port port)
    {
        List<Port> keyValue = super.get(keyClass);
        if(keyValue==null)
        {
            keyValue = new ArrayList<Port>();
        }
        
        if(!keyValue.contains(port))
        {
            keyValue.add(port);
            super.put(keyClass, keyValue);
        }
    }
    
    @Override
    public List<Port> get(Object key)
    {
        List<Port> result = new ArrayList<Port>();
        if(key instanceof Class<?>)
        {
            Class<?> keyClass = (Class<?>) key;
            for(Entry<Class<?>,List<Port>> entry : this.entrySet())
            {
                if(entry.getKey().isAssignableFrom(keyClass))
                {
                    result.addAll(entry.getValue());
                }
            }
        }
        return result;
    }

    public List<Port> portValues()
    {
        List<Port> portValues = new ArrayList<Port>();
        for(List<Port> singlePortValues : this.values())
        {
            portValues.addAll(singlePortValues);
        }
        return portValues;
    }

    @Override
    public List<Port> remove(Object key)
    {
        List<Port> removedElements = new ArrayList<Port>();
        if(key instanceof Class<?>)
        {
            List<Class<?>> assignableKeys = new ArrayList<Class<?>>();
            Class<?> keyClass = (Class<?>) key;
            for(Class<?> myKeyClass : this.keySet())
            {
                if((keyClass.isAssignableFrom(myKeyClass)))
                {
                	assignableKeys.add(myKeyClass);
                }
            }
            for(Class<?> assignableKeyClass : assignableKeys)
            {
            	removedElements.addAll(super.remove(assignableKeyClass));
            }
        }
        return removedElements;
    }
    
    public boolean removeValue(Port port)
    {
        List<Port> matchingPortList = null;
        Port matchingPort = null;
        for(List<Port> singlePortValues : this.values())
        {
            for(Port singlePort : singlePortValues)
            {
                if(singlePort.getPath().equals(port.getPath()))
                {
                    matchingPort = singlePort;
                    break;
                }
            }
            
            if(matchingPort!=null)
            {
                matchingPortList = singlePortValues;
                break;
            }
        }
        
        if(matchingPort == null)
        {
            return false;
        }
        
        return matchingPortList.remove(matchingPort);
    }
}
