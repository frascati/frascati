/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp;
import org.ow2.frascati.digihomeapp.task.DigihomeServiceRegistryDescriptorTask;
import org.ow2.frascati.digihomeapp.task.DigihomeTaskListener;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceDescriptors;
import org.ow2.frascati.digihomeapp.ui.descriptor.DigihomeServiceDescriptorListAdapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class DigihomeServiceFragment extends Fragment implements DigihomeTaskListener<Void, DigihomeServiceDescriptors>
{
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        Log.i("DigihomeServiceFragment","onCreateView ...");

        DigihomeServiceRegistryDescriptorTask digihomeServiceRegistryDescriptorTask = new DigihomeServiceRegistryDescriptorTask();
        digihomeServiceRegistryDescriptorTask.addDigihomeTaskListener(this);
        digihomeServiceRegistryDescriptorTask.execute();
        
        View mainContainer = inflater.inflate(R.layout.fragment_service_layout, container, false);
		return mainContainer;
	}

	@Override
	public void onDigihomeTaskProgress(Void... progress)
	{
	}

	@Override
	public void onDigihomeTaskPost(DigihomeServiceDescriptors result)
	{
		ListView listView = (ListView) this.getView().findViewById(R.id.service_list);
		DigihomeServiceDescriptorListAdapter digihomeServiceDescriptorListAdapter = new DigihomeServiceDescriptorListAdapter(getActivity(), result);
	    listView.setAdapter(digihomeServiceDescriptorListAdapter);
	}
}
