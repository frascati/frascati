package org.ow2.frascati.digihomeapp.types.parameter;

import java.io.IOException;

import org.ow2.frascati.digihomeapp.util.xml.XmlSerializer;

public class DigihomeParameter
{
	public final static String DIGIHOMEPARAMETER_TAG = "DigihomeParameter";
	public final static String DIGIHOMEPARAMETER_NAME_TAG = "name";
	public final static String DIGIHOMEPARAMETER_TYPE_TAG = "type";
	public final static String DIGIHOMEPARAMETER_VALUE_TAG = "value";
	
	
	protected String name;
	protected DigihomeParameterType type;
	protected String value;

	public DigihomeParameter() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DigihomeParameterType getType() {
		return type;
	}

	public void setType(DigihomeParameterType type) {
		this.type = type;
	}

	public void setType(String typeString)
	{
		DigihomeParameterType digihomeParameterType = DigihomeParameterType.valueOf(typeString);
		if(digihomeParameterType != null)
		{
			this.type = digihomeParameterType;	
		}
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("[ "+DigihomeParameter.class.getSimpleName()+" ]");
		stringBuilder.append("name : ");
		stringBuilder.append(this.name+", ");
		stringBuilder.append("type : ");
		stringBuilder.append(this.type.toString()+", ");
		stringBuilder.append("value : ");
		stringBuilder.append(this.value);
		return stringBuilder.toString();
	}
	
	public void toXML(XmlSerializer xmlSerializer) throws IllegalArgumentException, IllegalStateException, IOException
	{
	    xmlSerializer.startTag(DIGIHOMEPARAMETER_TAG);
	    
	    xmlSerializer.startTag(DIGIHOMEPARAMETER_NAME_TAG);
	    xmlSerializer.text(this.name);
	    xmlSerializer.endTag(DIGIHOMEPARAMETER_NAME_TAG);
	    
	    xmlSerializer.startTag(DIGIHOMEPARAMETER_TYPE_TAG);
	    xmlSerializer.text(this.type.toString());
	    xmlSerializer.endTag(DIGIHOMEPARAMETER_TYPE_TAG);
	    
	    xmlSerializer.startTag(DIGIHOMEPARAMETER_VALUE_TAG);
	    xmlSerializer.text(this.value);
	    xmlSerializer.endTag(DIGIHOMEPARAMETER_VALUE_TAG);
	    
	    xmlSerializer.endTag( DIGIHOMEPARAMETER_TAG);
	}
}
