//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.2-hudson-jaxb-ri-2.2-63- 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.10.28 at 12:53:46 AM CET 
//


package org.ow2.frascati.digihomeapp.types.descriptor;

import java.util.ArrayList;
import java.util.List;

public class DigihomeServiceDescriptors
{
    protected List<DigihomeServiceDescriptor> digihomeServiceDescriptor;

    /**
     * Gets the value of the digihomeServiceDescriptors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the digihomeServiceDescriptors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDigihomeServiceDescriptors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DigihomeServiceDescriptor }
     * 
     * 
     */
    public List<DigihomeServiceDescriptor> getDigihomeServiceDescriptors() {
        if (digihomeServiceDescriptor == null) {
            digihomeServiceDescriptor = new ArrayList<DigihomeServiceDescriptor>();
        }
        return this.digihomeServiceDescriptor;
    }

}
