/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.ow2.frascati.digihomeapp.task.DigihomeCoreInstallTask;
import org.ow2.frascati.digihomeapp.task.DigihomeCoreParameterTask;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceDescriptor;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameter;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameters;
import org.ow2.frascati.digihomeapp.ui.processor.DigihomeParameterProcessor;
import org.ow2.frascati.digihomeapp.ui.processor.DigihomeParameterProcessorFactory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DigihomeInstallFragment extends Fragment
{
	private int digihomeInstallRequestCode;
	
	/**
	 * The current digihomeParametersManager
	 */
	private DigihomeCoreParameterTask digihomeCoreParameterTask;

	/**
	 * The current digihomeInstallationManager
	 */
	private DigihomeCoreInstallTask digihomeCoreInstallTask;

	/**
	 * DigihomeParameterProcessors used to process the currents installation form fields
	 */
	private List<DigihomeParameterProcessor> currentDigihomeParameterProcessors;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        Log.i("DigihomeInstallFragment","onCreateView ...");

        View mainContainer = inflater.inflate(R.layout.fragment_digihome_layout, container, false);
        ViewGroup installFormContainer = (ViewGroup) mainContainer.findViewById(R.id.digihome_install_form_container);
		installFormContainer.setVisibility(View.GONE);
		
		ImageView scanCode = (ImageView) mainContainer.findViewById(R.id.scanCode);
        scanCode.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
				Random random = new Random();
				/**lower 16bits request code*/
				digihomeInstallRequestCode = random.nextInt(1 << 15);
				Log.i("DigihomeInstallFragment","startActivityForResult requestCode : "+digihomeInstallRequestCode);
				startActivityForResult(intent, digihomeInstallRequestCode);
			}
		});
        
        Button installFormInstallButton = (Button) mainContainer.findViewById(R.id.installFormInstallButton);
        installFormInstallButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				installFormInstallButtonClick();
			}
		});
        
		Button installFormCancelInstallButton = (Button) mainContainer.findViewById(R.id.installFormCancelInstallButton);
		installFormCancelInstallButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onInstallFormCancelInstallButtonClick();
			}
		});
        
		return mainContainer;
	}

	public void onDigihomeCoreParameterTaskExecuted(DigihomeParameters digihomeParameters)
	{
		Log.i("DigihomeInstallFragment","onDigihomeParameterFound "+digihomeParameters.size()+" DigihomeParameter(s) found");
		
		DigihomeParameter digihomeIdParameter = digihomeParameters.getDigihomeParameter("digihomeId");
		TextView installFormTitle = (TextView) this.getView().findViewById(R.id.digihome_install_form_title);
		installFormTitle.setText("Install\n"+digihomeIdParameter.getValue());
		
		View installQRCodeContainer = this.getView().findViewById(R.id.digihome_install_qrcode_container);
		installQRCodeContainer.setVisibility(View.GONE);
		
		ViewGroup  installFormContainer = (ViewGroup) this.getView().findViewById(R.id.digihome_install_form_container);
		installFormContainer.setVisibility(View.VISIBLE);
		
		ViewGroup installFormFieldContainer = (ViewGroup) installFormContainer.findViewById(R.id.digihome_install_form_field_container);
		this.currentDigihomeParameterProcessors = new ArrayList<DigihomeParameterProcessor>();
		DigihomeParameterProcessor digihomeParameterProcessor;
		View formFieldView;
		for(DigihomeParameter digihomeParameter : digihomeParameters.getDigihomeParameters())
		{
			digihomeParameterProcessor = DigihomeParameterProcessorFactory.getDigihomeParameterProcessor(digihomeParameter);
			if(digihomeParameterProcessor!=null)
			{
				this.currentDigihomeParameterProcessors.add(digihomeParameterProcessor);
				formFieldView = digihomeParameterProcessor.getView(this);
				installFormFieldContainer.addView(formFieldView);
			}
		}
	}

	private void onInstallFormCancelInstallButtonClick()
	{
		Log.i("DigihomeInstallFragment","onInstallFormCancelInstallButtonClick");
		onFinishInstall();
	}
	
	private void onFinishInstall()
	{
		View installQRCodeContainer = this.getView().findViewById(R.id.digihome_install_qrcode_container);
		installQRCodeContainer.setVisibility(View.VISIBLE);
		ViewGroup  installFormContainer = (ViewGroup) this.getView().findViewById(R.id.digihome_install_form_container);
		installFormContainer.setVisibility(View.GONE);
		ViewGroup installFormFieldContainer = (ViewGroup) installFormContainer.findViewById(R.id.digihome_install_form_field_container);
		installFormFieldContainer.removeAllViews();
		this.currentDigihomeParameterProcessors.clear();
	}
	
	private void installFormInstallButtonClick()
	{
		Log.i("DigihomeInstallFragment","installFormInstallButtonClick ");
		DigihomeParameters digihomeParameters = this.digihomeCoreParameterTask.getDigihomeParameters();
		this.digihomeCoreInstallTask  = new DigihomeCoreInstallTask(this);
		this.digihomeCoreInstallTask.execute(digihomeParameters);
	}
	
	public void digihomeCoreInstallTaskExecuted(DigihomeServiceDescriptor digihomeServiceDescriptor)
	{
		Log.i("DigihomeInstallFragment","onInstallationDone " + digihomeServiceDescriptor.getDigihomeId());
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getActivity());
		alertDialogBuilder.setTitle("Installation done");

		// set dialog message
		alertDialogBuilder
				.setMessage("Installation "+digihomeServiceDescriptor.getDigihomeId()+" ended on status "+digihomeServiceDescriptor.getStatus()+"\nMore options in the Service tab")
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id)
							{
								dialog.dismiss();
							}
						});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
		this.onInstallFormCancelInstallButtonClick();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		Log.i("DigihomeInstallFragment","onActivityResult requestCode : "+requestCode+", resultCode : "+resultCode);
		if(requestCode == digihomeInstallRequestCode)
		{
			if(resultCode == Activity.RESULT_OK)
			{
				String digihomeId = intent.getStringExtra("SCAN_RESULT");
				this.digihomeCoreParameterTask = new DigihomeCoreParameterTask(this);
				this.digihomeCoreParameterTask.execute(digihomeId);
			}
			return;
		}
//		if(this.currentDigihomeParameterProcessors!=null)
//		{
			for(DigihomeParameterProcessor digihomeParameterProcessor : this.currentDigihomeParameterProcessors)
			{
				digihomeParameterProcessor.onActivityResult(requestCode, resultCode, intent);
			}
//		}
	}

	
	@Override
    public void onStop()
    {
        super.onStop();
        /**stop digihomeParametersManager if running*/
        if(this.digihomeCoreParameterTask!=null && !this.digihomeCoreParameterTask.getStatus().equals(Status.FINISHED))
        {
	        this.digihomeCoreParameterTask.cancel(true);
        }
        /**stop digihomeInstallationManager if running*/
        if(this.digihomeCoreInstallTask!=null && !this.digihomeCoreInstallTask.getStatus().equals(Status.FINISHED))
        {
	        this.digihomeCoreInstallTask.cancel(true);
        }
        
        Log.i("DigihomeInstallFragment","stopped ...");
    }
	
    @Override
    public void onDestroy()
    {
		super.onDestroy();
    	Log.i("DigihomeInstallFragment","destroyed ...");
    }
}
