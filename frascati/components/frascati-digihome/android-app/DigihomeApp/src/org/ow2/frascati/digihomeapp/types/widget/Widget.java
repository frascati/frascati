/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.types.widget;

import java.io.Serializable;

import android.graphics.Bitmap;

/* This class is representing a Widget */
public class Widget implements Serializable
{
	public final static String WIDGETS_TAG = "Widgets";
	public final static String WIDGET_TAG = "Widget";
	public final static String WIDGET_NAME_TAG = "name";
	public final static String WIDGET_ICON_TAG = "iconURL";
	public final static String WIDGET_SMALL_TAG = "smallWidgetURL";
	public final static String WIDGET_LARGE_TAG = "largeWidgetURL";
	
	private static final long serialVersionUID = 7743733092576989223L;

	private String name;
	
	private String iconURL;
	
	private String smallWidgetURL;
	
	private String largeWidgetURL;

	private int delay;
	
	private int delayGraph;
	
	private transient Bitmap icon;
	
	public Widget()
	{
		this.delay = 2000;
		this.delayGraph = 2000;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconURL() {
		return iconURL;
	}
	
	public void setIconURL(String iconURL) {
		this.iconURL = iconURL;
	}

	public Bitmap getIcon()
	{
		return this.icon;
	}

	public void setIcon(Bitmap icon)
	{
		this.icon = icon;
	}
	
	public String getSmallWidgetURL() {
		return smallWidgetURL;
	}

	public void setSmallWidgetURL(String smallURL) {
		this.smallWidgetURL = smallURL;
	}

	public String getLargeWidgetURL() {
		return largeWidgetURL;
	}

	public void setLargeWidgetURL(String largeURL) {
		this.largeWidgetURL = largeURL;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public boolean isDelayEnable()
	{
		return this.delay!=-1;
	}
	
	public int getDelayGraph() {
		return delayGraph;
	}

	public void setDelayGraph(int delayGraph) {
		this.delayGraph = delayGraph;
	}
	
	public boolean isDelayGraphEnable()
	{
		return this.delayGraph!=-1;
	}
	
	@Override
	public boolean equals(Object anotherObject)
	{
		if(anotherObject instanceof String && ((String)anotherObject).equals(this.smallWidgetURL))
		{
			return true;
		}
		return(anotherObject instanceof Widget) && ((Widget)anotherObject).getSmallWidgetURL().equals(this.smallWidgetURL);
		
	}
}
