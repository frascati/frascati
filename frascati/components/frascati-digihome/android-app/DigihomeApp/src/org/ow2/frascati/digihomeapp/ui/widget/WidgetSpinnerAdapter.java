/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.widget;

import java.util.List;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.types.widget.Widget;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/* Class for having a custom spinner */
public class WidgetSpinnerAdapter extends ArrayAdapter<Widget>
{
   private LayoutInflater inflater;
   
   private List<Widget> widgets;
   
   public WidgetSpinnerAdapter(Context context, LayoutInflater inflater, List<Widget> widgets)
   {
       super(context,  R.drawable.spinner_custom, widgets.toArray(new Widget[widgets.size()]));
       this.inflater=inflater;
       this.widgets = widgets;
   }
   
   /* Return the DropDownView */
   public View getDropDownView(int position, View convertView,ViewGroup parent)
   {
       return getCustomView(position, convertView, parent);
   }

   /* Return the view at position in our spinner */
   public View getView(int position, View convertView, ViewGroup parent)
   {
       return getCustomView(position, convertView, parent);
   }

   /* Return the CustomView for the position th widget in our spinner */
   public View getCustomView(int position, View convertView, ViewGroup parent)
	{
	   Log.i("WidgetSpinnerAdapter", "getCustomView");
	   //TODO optimise loading
	   View row=inflater.inflate(R.layout.row, parent, false);
	   TextView label=(TextView)row.findViewById(R.id.company);
	   Widget widget = this.widgets.get(position);
	   
	   label.setText(widget.getName());
	   TextView sub=(TextView)row.findViewById(R.id.sub);
	   sub.setVisibility(View.INVISIBLE);
	   ImageView icon=(ImageView)row.findViewById(R.id.image);

	   /**Listen for icon to be loaded*/
	   if(widget.getIcon()!=null)
	   {
		   icon.setImageBitmap(widget.getIcon());
	   }
	   else
	   {
		   Log.w("WidgetSpinnerAdapter", "icon null");
	   }
	   return row;
	}
}
