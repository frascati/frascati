/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ow2.frascati.digihomeapp.types.stringList.StringList;
import org.ow2.frascati.digihomeapp.util.xml.XMLParser;

import android.util.Log;

public abstract class DigihomeServiceRegistryAvailablePortTask extends DigihomeServiceRegistryTask<String, Void, List<String>>
{
	@Override
	protected List<String> doInBackground(String... params)
	{
		Log.i(this.getClass().getSimpleName(),"doInBackground");
		String portClassName = null;
		if(params!=null && params.length != 0)
		{
			portClassName = params[0];
		}

		try
		{
			URL requestURL = this.getRequestURL(portClassName);
			Log.i(this.getClass().getSimpleName(),"get ports from : "+requestURL.toString());
			
			HttpGet httpget = new HttpGet(requestURL.toURI());
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpget);
			InputStream responseInputStream = response.getEntity().getContent();
			
			StringList availablePortPathes = XMLParser.parseStream(responseInputStream, StringList.class);
			Log.i(this.getClass().getSimpleName(),availablePortPathes.getStringList().size()+ "parameters found for id "+portClassName);
			return availablePortPathes.getStringList();
		}
		catch(Exception e)
		{
			Log.w(this.getClass().getSimpleName(),"error "+ e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

}
