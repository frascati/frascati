/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.io.InputStream;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.util.Log;

public class DigihomeSimpleGetTask extends DigihomeTask<String, Void, InputStream>
{
	@Override
	protected InputStream doInBackground(String... params)
	{
		Log.i("DigihomeSimpleGetTask","doInBackground");
		if(params==null || params.length == 0)
		{
			Log.w("DigihomeSimpleGetTask","No URL provided");
		}

		String requestURLString=params[0];
		Log.i("DigihomeSimpleGetTask","requestURL : "+requestURLString);
		try
    	{
			URL requestURL = new URL(requestURLString);
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(requestURL.toURI());
			HttpResponse response = httpClient.execute(httpGet);
			return  response.getEntity().getContent();
		}
    	catch (Exception e)
		{
    		Log.w("DigihomeSimpleGetTask",e);
    		return null;
		}
	}

}
