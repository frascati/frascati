package org.ow2.frascati.digihomeapp.ui.widget;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.types.widget.Widget;
import org.ow2.frascati.digihomeapp.webclient.DigihomeWebChromeClient;
import org.ow2.frascati.digihomeapp.webclient.DigihomeWebViewClient;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WidgetOptionsManager
{
	private Activity activity;
	
	private Widget widget;
	
	private View widgetView;
	
	public WidgetOptionsManager(Activity activity,View widgetView, Widget widget)
	{
		this.activity = activity;
		this.widget = widget;
		this.widgetView = widgetView;
	}
	
	public void onOptionClicked()
	{
    	final Dialog dialogzoom = new Dialog(activity);
    	dialogzoom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogzoom.setContentView(R.layout.dialog_click_gear);
		TextView title = (TextView) dialogzoom.findViewById(R.id.title_gear);
		title.setText(widget.getName());
		title.setTypeface(title.getTypeface(),Typeface.BOLD);
		
		ImageView cross = (ImageView) dialogzoom.findViewById(R.id.cross_gear);
		/* If the cross of the dialog is clicked */
		cross.setOnClickListener(new OnClickListener()
		{
			public void onClick(View view)
			{
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
				dialogzoom.dismiss();
			}
		});
		dialogzoom.show();
		
		TextView zoom_gear = (TextView) dialogzoom.findViewById(R.id.text_zoom_gear);
		zoom_gear.setOnClickListener(new OnClickListener()
		{
			public void onClick(View view)
			{
				onTextZoomGearClick(view);
			}
		});
		
		TextView delay_gear = (TextView) dialogzoom.findViewById(R.id.text_delay_gear);

		if(!widget.isDelayEnable())
		{
			delay_gear.setVisibility(View.GONE);
		}
		else
		{
			delay_gear.setOnClickListener(new OnClickListener()
			{
				public void onClick(View view)
				{
					onTextDelayGearClick(view);
				}
			});
		}
		
		TextView delay_graph_gear = (TextView) dialogzoom.findViewById(R.id.text_delay_graph_gear);
		if(!widget.isDelayGraphEnable())
		{
			delay_graph_gear.setVisibility(View.GONE);
		}
		else
		{
			delay_graph_gear.setOnClickListener(new OnClickListener()
			{
				public void onClick(View view)
				{
					onTextDelayGraphGearClick(view);
				}
			});
		}
	}
	
	 /* If the zoom for text in gear menu is asked */
    @SuppressLint("SetJavaScriptEnabled")
	public void onTextZoomGearClick(View view)
    {    	
		this.activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
    	final Dialog dialogzoom = new Dialog(this.activity);
    	dialogzoom.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogzoom.setContentView(R.layout.dialog_zoom_webview);
		TextView text = (TextView) dialogzoom.findViewById(R.id.miniWindowsTitle_zoom);
		text.setText(widget.getName());
		text.setTypeface(text.getTypeface(),Typeface.BOLD);
		
		WebView webzoom = (WebView) dialogzoom.findViewById(R.id.webview_zoom);
		webzoom.getSettings().setJavaScriptEnabled(true);
				
		//loads the WebView completely zoomed out
        webzoom.getSettings().setLoadWithOverviewMode(true);
         
        //true makes the Webview have a normal viewport such as a normal desktop browser
        //when false the webview will have a viewport constrained to it's own dimensions
        webzoom.getSettings().setUseWideViewPort(true);
         
        //override the web client to open all links in the same webview
        webzoom.setWebViewClient(new DigihomeWebViewClient());
        webzoom.setWebChromeClient(new DigihomeWebChromeClient());
         
        //Injects the supplied Java object into this WebView. The object is injected into the
        //JavaScript fragment.getContext() of the main frame, using the supplied name. This allows the
        //Java object's public methods to be accessed from JavaScript.
        webzoom.addJavascriptInterface(widget, "Android");
		webzoom.loadUrl(widget.getLargeWidgetURL());
		
		ImageView cross = (ImageView) dialogzoom.findViewById(R.id.miniWindowsTitle2_zoom);
		
		/* If the cross of the zoom dialog is clicked */
		cross.setOnClickListener(new OnClickListener()
		{

			public void onClick(View view)
			{
				dialogzoom.dismiss();
				activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
			}
		});
		dialogzoom.show();
		
		/* Changing size of dialog window */
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels*90/100;
		int height = metrics.heightPixels*90/100;
		dialogzoom.getWindow().setLayout(width,height);
    }
    
    /* Called when the user click on "Change delay" */
    public void onTextDelayGearClick(View view)
    {
    	final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_delay);
 		// set the custom dialog components - text, image and button
		
		Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialog_delay_button_ok);
		Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialog_delay_button_cancel);
		
		final EditText edittext = (EditText) dialog.findViewById(R.id.edittext_dialog_delay);
		
		// if button is clicked, close the custom dialog
		dialogButtonOK.setOnClickListener(new OnClickListener()
		{
			public void onClick(View view)
			{
				String delayString = edittext.getText().toString();
				if(delayString==null)
				{
					dialog.dismiss();
					return;
				}
				
				try
				{
					/* Ici faire le traitement du txt entre */
					int delay = Integer.parseInt(delayString);
					widget.setDelay(delay);
					RelativeLayout widgetOptionBar = (RelativeLayout) widgetView.getParent();
			    	LinearLayout widgetContainer = (LinearLayout) widgetOptionBar.getParent();
			    	WebView widgetWebView = (WebView) widgetContainer.getChildAt(1);
			    	widgetWebView.reload();
			    	dialog.dismiss();
				}
				catch(Exception e)
				{
					Log.w("WidgetOptionsManager", "Invalid delay value : "+delayString);
				}
			}
		});
		
		// if button is clicked, close the custom dialog
		dialogButtonCancel.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		dialog.show();
    }
    
    /* Called when the user click on "Change delay graph" */
    public void onTextDelayGraphGearClick(View view)
    {
    	final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_delay_graph);
 		// set the custom dialog components - text, image and button
		
		Button dialogButtonOK = (Button) dialog.findViewById(R.id.dialog_delay_graph_button_ok);
		Button dialogButtonCancel = (Button) dialog.findViewById(R.id.dialog_delay_graph_button_cancel);
		
		final EditText edittext = (EditText) dialog.findViewById(R.id.edittext_dialog_delay_graph);
		
		// if button is clicked, close the custom dialog
		dialogButtonOK.setOnClickListener(new OnClickListener()
		{
			public void onClick(View view)
			{
				String delayString = edittext.getText().toString();
				if(delayString==null)
				{
					dialog.dismiss();
					return;
				}
				
				try
				{
					/* Ici faire le traitement du txt entre */
					int delay = Integer.parseInt(delayString);
					widget.setDelay(delay);
					RelativeLayout widgetOptionBar = (RelativeLayout) widgetView.getParent();
			    	LinearLayout widgetContainer = (LinearLayout) widgetOptionBar.getParent();
			    	WebView widgetWebView = (WebView) widgetContainer.getChildAt(1);
			    	widgetWebView.reload();
			    	dialog.dismiss();
				}
				catch(Exception e)
				{
					Log.w("WidgetOptionsManager", "Invalid delay value : "+delayString);
				}
			}
		});
		
		// if button is clicked, close the custom dialog
		dialogButtonCancel.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				dialog.dismiss();
			}
		});
		
		dialog.show();
    }
}
