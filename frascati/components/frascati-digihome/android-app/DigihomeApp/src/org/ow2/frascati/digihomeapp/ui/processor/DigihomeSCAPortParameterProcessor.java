/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.processor;

import java.util.List;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.task.DigihomeServiceRegistryAvailablePortTask;
import org.ow2.frascati.digihomeapp.task.DigihomeTaskListener;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameter;
import org.ow2.frascati.digihomeapp.ui.processor.port.PortListDialog;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public abstract class DigihomeSCAPortParameterProcessor extends DigihomeParameterProcessor implements DigihomeTaskListener<Void, List<String>>, OnClickListener
{
	protected String[] ports;

	protected boolean[] checkedPorts;
	
	public DigihomeSCAPortParameterProcessor(DigihomeParameter digihomeParameter)
	{
		super(digihomeParameter);
	}

	@Override
	protected int getFieldResourceId()
	{
		return R.layout.form_field_port;
	}
	
	@Override
	protected String getFieldName()
	{
		String digihomeParameterName = this.digihomeParameter.getName();
		String referenceName = digihomeParameterName.substring(digihomeParameterName.lastIndexOf("/")+1);
		return referenceName;
	}

	protected abstract int getSelectionButtonTextResourceId();
	
	protected abstract DigihomeServiceRegistryAvailablePortTask getDigihomeServiceRegistryAvailablePortTask();
	
	@Override
	public View getView(Fragment fragment)
	{
		View superView = super.getView(fragment);
		
		Button selectServicesButton = (Button) container.findViewById(R.id.selectButton);
		selectServicesButton.setText(this.getSelectionButtonTextResourceId());
		selectServicesButton.setOnClickListener(this);
		
		DigihomeServiceRegistryAvailablePortTask digihomeServiceRegistryAvailablePortTask = this.getDigihomeServiceRegistryAvailablePortTask();
		digihomeServiceRegistryAvailablePortTask.addDigihomeTaskListener(this);
		digihomeServiceRegistryAvailablePortTask.execute(digihomeParameter.getValue());
		return superView;
	}

	@Override
	public void onDigihomeTaskProgress(Void... progress)
	{
		//do nothing
	}

	@Override
	public void onDigihomeTaskPost(List<String> result)
	{
		this.ports = result.toArray(new String[result.size()]);
		this.checkedPorts = new boolean[result.size()];
	}

	private PortListDialog portListDialog;
	
	@Override
	public void onClick(View v)
	{
		if(v.getId() == R.id.selectButton)
		{
			portListDialog = new PortListDialog();
			portListDialog.setTitleId(this.getSelectionButtonTextResourceId());
			portListDialog.setPorts(this.ports);
			portListDialog.setCheckedPorts(this.checkedPorts);
			portListDialog.setOnClickListener(this);
			portListDialog.show(this.fragment.getFragmentManager(), "Port List Fragment");
		}
//		else if(v.getId() == R.id.port_list_ok_button)
		else
		{
			portListDialog.dismiss();
			String portListValue = portListDialog.getSelectedValue();
			this.checkedPorts = portListDialog.getCheckedPorts();
			this.digihomeParameter.setValue(portListValue);
			Log.i("DigihomeSCAReferenceParameterProcessor", "set parameter  : "+this.getFieldName()+" : "+portListValue);
		}
	}
}