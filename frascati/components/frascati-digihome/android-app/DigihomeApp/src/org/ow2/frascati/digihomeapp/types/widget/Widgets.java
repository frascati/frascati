/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.types.widget;

import java.util.ArrayList;
import java.util.List;

public class Widgets
{
	protected List<Widget> widget;

	public List<Widget> getWidgets()
	{
		if (widget == null)
		{
			widget = new ArrayList<Widget>();
		}
		return this.widget;
	}
	
	
	@Override
	public String toString()
	{
		StringBuilder toStringBuilder = new StringBuilder();
		toStringBuilder.append(this.getClass().getSimpleName());
		toStringBuilder.append(" [ ");
		for (Widget widget : this.getWidgets())
		{
			toStringBuilder.append(widget.toString());
			toStringBuilder.append(", ");
		}
		toStringBuilder.append(" ]");
		return toStringBuilder.toString();
	}
}
