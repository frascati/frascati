/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.processor;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public abstract class DigihomeParameterProcessor
{
	protected DigihomeParameter digihomeParameter;
	
	protected ViewGroup container;
	
	protected Fragment fragment;
	
	public DigihomeParameterProcessor(DigihomeParameter digihomeParameter)
	{
		this.digihomeParameter = digihomeParameter;
	}
	
	public View getView(Fragment fragment)
	{
		this.fragment = fragment;
		
		LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.container = (ViewGroup) inflater.inflate(R.layout.form_field, null);
		
		TextView formFieldName = (TextView) this.container.findViewById(R.id.formFieldName);
		formFieldName.setText(this.getFieldName());
		
		ViewGroup formFieldContainer = (ViewGroup) this.container.findViewById(R.id.formFieldContainer);
		inflater.inflate(this.getFieldResourceId(), formFieldContainer, true);
		
		return this.container;
	}

	protected String getFieldName()
	{
		return this.digihomeParameter.getName();
	}
	
	protected abstract int getFieldResourceId();
	
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		//Nothing to do
	}
}
