/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ow2.frascati.digihomeapp.WebviewsFragment;
import org.ow2.frascati.digihomeapp.types.widget.Widget;
import org.ow2.frascati.digihomeapp.types.widget.Widgets;
import org.ow2.frascati.digihomeapp.util.xml.XMLParser;
import org.xmlpull.v1.XmlPullParserException;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class DigihomeWidgetRegistryTask extends DigihomeTask<Void, Widget, Void>
{
	private final static Long RELOAD_TASK_DELAY = 5000L;

	private final static String WIDGET_REGISTRY_NAME ="demo";
	
	private boolean status;
	
	private HttpClient httpclient;
	
	private List<Widget> currentWidgets;
	
	private WebviewsFragment webviewsFragment;
	
	public DigihomeWidgetRegistryTask(WebviewsFragment webviewsFragment)
	{
		super();
		this.webviewsFragment = webviewsFragment;
		this.status = true;
		this.httpclient = new DefaultHttpClient();
		this.currentWidgets = new ArrayList<Widget>();
	}
	
	@Override
	protected StringBuilder getRequestBuilder()
	{
		return super.getRequestBuilder().append("/widgetRegistry-"+WIDGET_REGISTRY_NAME);
	}
	
	public List<Widget> getWidgets()
	{
		return this.currentWidgets;
	}
	
	public void stopTask()
	{
		this.status = false;
		Log.i("DigihomeWidgetRegistryTask", "stop");
	}
	
	public boolean getTaskStatus()
	{
		return status;
	}

	@Override
	protected Void doInBackground(Void... arg0)
    {
    	Log.i("DigihomeWidgetRegistryTask", "doInBackground");
    	while(status == true)
		{
			try
			{
				this.checkWidgetRegistry();
				Thread.sleep(RELOAD_TASK_DELAY);
			}
			catch (Exception e)
			{
				Log.i("DigihomeWidgetRegistryTask","error "+ e.getMessage());
				e.printStackTrace();
			}
		}
    	return null;
    }
    
	private void checkWidgetRegistry() throws ClientProtocolException, IOException, XmlPullParserException, URISyntaxException, InstantiationException, IllegalAccessException
	{
		URL requestURL = this.getRequestURL();
		Log.i("DigihomeWidgetRegistryTask","Check widget registry at : "+requestURL.toString());
		HttpGet httpget = new HttpGet(requestURL.toURI());
		HttpResponse response = httpclient.execute(httpget);
		InputStream responseInputStream = response.getEntity().getContent();
		Widgets widgets = XMLParser.parseStream(responseInputStream, Widgets.class);
		List<Widget> widgetList = widgets.getWidgets();
		Log.i("DigihomeWidgetRegistryTask",widgetList.size()+ "widget(s) found in the registry");

		boolean widgetRegistryChange = (this.currentWidgets==null) || !(this.currentWidgets.containsAll(widgetList) && this.currentWidgets.size()==widgetList.size());
		if(widgetRegistryChange)
		{
			Bitmap widgetIcon;
			/**if the widgetRegistry had changed reload the widget icons*/
			for(Widget widget : widgetList)
			{
				widgetIcon = this.getWidgetIcon(widget);
				widget.setIcon(widgetIcon);
			}
			publishProgress(widgetList.toArray(new Widget[widgetList.size()]));
		}
	}
	
	private Bitmap getWidgetIcon(Widget widget) throws URISyntaxException, ClientProtocolException, IOException
	{
		URL requestURL = new URL(widget.getIconURL());
		HttpGet httpGet = new HttpGet(requestURL.toURI());
		HttpResponse response = this.httpclient.execute(httpGet);
		InputStream widgetIconStream = response.getEntity().getContent();
		return BitmapFactory.decodeStream(widgetIconStream);
	}
	
	/* Publications of the widgets found in the file analysed */
    protected void onProgressUpdate(Widget... result)
    {
    	super.onProgressUpdate(result);
		Log.i("DigihomeWidgetRegistryTask", "widgetRegistry changed");
		this.currentWidgets = Arrays.asList(result);
		this.webviewsFragment.onDigihomeWidgetRegistryChange(this.currentWidgets);
    }
}
