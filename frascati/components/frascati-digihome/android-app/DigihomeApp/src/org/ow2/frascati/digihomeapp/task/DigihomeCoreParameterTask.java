/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.io.InputStream;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.ow2.frascati.digihomeapp.DigihomeInstallFragment;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameters;
import org.ow2.frascati.digihomeapp.util.xml.XMLParser;

import android.util.Log;

public class DigihomeCoreParameterTask extends DigihomeCoreTask<String, Void, DigihomeParameters>
{
	private DigihomeInstallFragment digihomeFragment;
	
	private DigihomeParameters digihomeParameters;
	
	public DigihomeCoreParameterTask(DigihomeInstallFragment digihomeFragment)
	{
		this.digihomeFragment = digihomeFragment;
	}
	
	@Override
	protected StringBuilder getRequestBuilder()
	{
		return super.getRequestBuilder().append("/parameters");
	}
	
	public DigihomeParameters getDigihomeParameters()
	{
		return digihomeParameters;
	}

	@Override
	protected DigihomeParameters doInBackground(String... params)
	{
		Log.i("DigihomeCoreParameterTask","doInBackground");
		if(params==null || params.length == 0)
		{
			Log.w("DigihomeCoreParameterTask","No digihomeId provided");
			return null;
		}

		String digihomeId=params[0];
		try
		{
			URL requestURL = this.getRequestURL(digihomeId);
			Log.i("DigihomeCoreParameterTask","get DigihomeParameters from : "+requestURL.toString());
			
			HttpGet httpget = new HttpGet(requestURL.toURI());
			HttpClient httpclient = new DefaultHttpClient();
			HttpResponse response = httpclient.execute(httpget);
			InputStream responseInputStream = response.getEntity().getContent();
			
			DigihomeParameters digihomeParameters = XMLParser.parseStream(responseInputStream, DigihomeParameters.class);
			Log.i("DigihomeCoreParameterTask",digihomeParameters.size()+ "parameters found for id "+digihomeId);
			return digihomeParameters;
		}
		catch(Exception e)
		{
			Log.w("DigihomeCoreParameterTask","error "+ e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(DigihomeParameters digihomeParameters)
	{
		super.onPostExecute(digihomeParameters);
		this.digihomeParameters = digihomeParameters;
		this.digihomeFragment.onDigihomeCoreParameterTaskExecuted(this.digihomeParameters);
	}

}
