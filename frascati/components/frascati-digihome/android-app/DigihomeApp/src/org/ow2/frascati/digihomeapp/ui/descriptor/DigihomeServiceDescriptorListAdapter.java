/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.descriptor;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceDescriptor;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceDescriptors;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceStatus;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class DigihomeServiceDescriptorListAdapter extends ArrayAdapter<DigihomeServiceDescriptor>
{

	public DigihomeServiceDescriptorListAdapter(Context context, DigihomeServiceDescriptors digihomeServiceDescriptors)
	{
		super(context, R.layout.digihome_service_descriptor_row,R.id.digihome_service_id, digihomeServiceDescriptors.getDigihomeServiceDescriptors());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		ViewGroup digihomeServiceDescriptorContainer = (ViewGroup) super.getView(position, convertView, parent);
		DigihomeServiceDescriptor digihomeServiceDescriptor = this.getItem(position);
		TextView digihomeId = (TextView) digihomeServiceDescriptorContainer.findViewById(R.id.digihome_service_id);
		digihomeId.setText(digihomeServiceDescriptor.getDigihomeId());
		TextView digihomeStatus = (TextView) digihomeServiceDescriptorContainer.findViewById(R.id.digihome_service_status);
		DigihomeServiceStatus digihomeServiceStatus = digihomeServiceDescriptor.getStatus();
		digihomeStatus.setText(digihomeServiceStatus.toString());
		
//		Spinner digihomeServiceMethodSprinner = (Spinner) digihomeServiceDescriptorContainer.findViewById(R.id.digihome_service_method);
//		if(!digihomeServiceStatus.equals(DigihomeServiceStatus.BOUNDED))
//		{
//			digihomeServiceMethodSprinner.setVisibility(View.GONE);
//			return digihomeServiceDescriptorContainer;
//		}
//		
//		//TODO
//		
		return digihomeServiceDescriptorContainer;
		
	}



}