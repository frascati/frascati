/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.io.OutputStream;
import java.net.URLConnection;

import org.ow2.frascati.digihomeapp.DigihomeInstallFragment;
import org.ow2.frascati.digihomeapp.types.descriptor.DigihomeServiceDescriptor;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameters;
import org.ow2.frascati.digihomeapp.util.xml.XMLParser;

import android.util.Log;

public class DigihomeCoreInstallTask extends DigihomeCoreTask<DigihomeParameters, Void, DigihomeServiceDescriptor>
{
	private DigihomeInstallFragment digihomeFragment;
	
	public DigihomeCoreInstallTask(DigihomeInstallFragment digihomeFragment)
	{
		this.digihomeFragment = digihomeFragment;
	}
	
	@Override
	protected StringBuilder getRequestBuilder()
	{
		return super.getRequestBuilder().append("/install");
	}
	
	@Override
	protected DigihomeServiceDescriptor doInBackground(DigihomeParameters... params)
	{
		Log.i("DigihomeCoreInstallTask","doInBackground");
		if(params==null || params.length == 0)
		{
			Log.w("DigihomeCoreInstallTask","No DigihomeParameters provided");
		}

		try
		{
			DigihomeParameters digihomeParameters=params[0];
			Log.i("DigihomeCoreInstallTask",digihomeParameters.getDigihomeParameters().size()+" DigihomeParameter(s)");
			String digihomeParametersXML = digihomeParameters.toXML();
			Log.i("DigihomeCoreInstallTask","digihomeParametersXML "+digihomeParametersXML);

			URLConnection connection = this.getRequestURL().openConnection();
//			connection.setConnectTimeout(DigihomeTask.REQUEST_TIMEOUT);
			// Http Method becomes POST
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Type", "application/xml");
			connection.setRequestProperty("Content-Length", String.valueOf(digihomeParametersXML.getBytes().length)); 
			OutputStream output = connection.getOutputStream(); 
			output.write(digihomeParametersXML.getBytes());
			output.close();
			
			return XMLParser.parseStream(connection.getInputStream(), DigihomeServiceDescriptor.class);
		}
		catch(Exception e)
		{
			Log.w("DigihomeCoreInstallTask","error "+ e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(DigihomeServiceDescriptor digihomeServiceDescriptor) {
		super.onPostExecute(digihomeServiceDescriptor);
		this.digihomeFragment.digihomeCoreInstallTaskExecuted(digihomeServiceDescriptor);
	}

}
