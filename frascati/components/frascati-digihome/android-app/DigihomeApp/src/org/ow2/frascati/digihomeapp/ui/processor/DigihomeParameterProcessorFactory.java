/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.processor;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameter;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameterType;

import android.util.Log;

public class DigihomeParameterProcessorFactory
{
	private static Map<DigihomeParameterType, Class<? extends DigihomeParameterProcessor>> digihomeParameterProcessorRegistry;
	static
	{
		digihomeParameterProcessorRegistry = new HashMap<DigihomeParameterType, Class<? extends DigihomeParameterProcessor>>();
		digihomeParameterProcessorRegistry.put(DigihomeParameterType.SCA_CONTEXTUAL_PROPERTY, DigihomeDefaultParameterProcessor.class);
		digihomeParameterProcessorRegistry.put(DigihomeParameterType.SCA_PROPERTY, DigihomeSCAPropertyParameterProcessor.class);
		digihomeParameterProcessorRegistry.put(DigihomeParameterType.SCA_REFERENCE, DigihomeSCAReferenceParameterProcessor.class);
		digihomeParameterProcessorRegistry.put(DigihomeParameterType.SCA_SERVICE, DigihomeSCAServiceParameterProcessor.class);
	}
	
	public static DigihomeParameterProcessor getDigihomeParameterProcessor(DigihomeParameter digihomeParameter)
	{
		DigihomeParameterType digihomeParameterType = digihomeParameter.getType();
		Class<? extends DigihomeParameterProcessor> digihomeParameterProcessorClass = DigihomeParameterProcessorFactory.digihomeParameterProcessorRegistry.get(digihomeParameterType);
		if(digihomeParameterProcessorClass==null)
		{
			Log.i("DigihomeParameterProcessorRegistry", "No DigihomeParameterProcessor found for type "+digihomeParameterType.value());
			return null;
		}

		try
		{
			Constructor<? extends DigihomeParameterProcessor>  digihomeParameterProcessorConstructor = digihomeParameterProcessorClass.getConstructor(DigihomeParameter.class);
			return digihomeParameterProcessorConstructor.newInstance(digihomeParameter);
			
//			digihomeParameterProcessor = digihomeParameterProcessorClass.getc
		} catch (Exception e)
		{
			// should not happen
			return null;
		}
	}
}
