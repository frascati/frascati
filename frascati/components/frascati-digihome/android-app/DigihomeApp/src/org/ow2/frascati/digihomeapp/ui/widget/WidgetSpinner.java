package org.ow2.frascati.digihomeapp.ui.widget;

import java.util.List;

import org.ow2.frascati.digihomeapp.types.widget.Widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.Spinner;

public class WidgetSpinner
{
	private Context context;
	
	private LayoutInflater layoutInflater;

	private Spinner spinner;
	
	public WidgetSpinner(Context context, LayoutInflater inflater, Spinner spinner)
	{
		this.context = context;
		this.layoutInflater = inflater;
		this.spinner = spinner;
	}

	public void setWidget(List<Widget> widgets)
	{
		WidgetSpinnerAdapter adapter = new WidgetSpinnerAdapter(context,this.layoutInflater,widgets);
		this.spinner.setAdapter(adapter);
	}

	public void setSpinner(Spinner spinner)
	{
		this.spinner = spinner;
	}
	public Widget getSelectedItem()
	{
		return (Widget) this.spinner.getSelectedItem();
	}
}
