/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.digihomeapp.task.DigihomeWidgetRegistryTask;
import org.ow2.frascati.digihomeapp.types.widget.Widget;
import org.ow2.frascati.digihomeapp.ui.widget.WidgetOptionsManager;
import org.ow2.frascati.digihomeapp.ui.widget.WidgetSpinner;
import org.ow2.frascati.digihomeapp.webclient.DigihomeWebChromeClient;
import org.ow2.frascati.digihomeapp.webclient.DigihomeWebViewClient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class WebviewsFragment extends Fragment
{
    private WidgetSpinner widgetSpinner;
    
    private DigihomeWidgetRegistryTask digihomeWidgetRegistryTask;
    
    private GridLayout widgetsContainer;
	
    private List<Widget> displayedWidgets;
    
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
    	Log.i("WebviewsFragment","onCreateView ...");
        
		View view = inflater.inflate(R.layout.fragment_webviews_layout, container, false);
		this.widgetsContainer = (GridLayout) view.findViewById(R.id.mainGridLayout);
		
		Spinner spinner = (Spinner) view.findViewById(R.id.widget_spinner);
        this.widgetSpinner = new WidgetSpinner(this.getActivity(), inflater, spinner);
		
		Button widgetSpinnerButton = (Button) view.findViewById(R.id.buttonSpinner);
        widgetSpinnerButton.setOnClickListener(new Button.OnClickListener()
        {
        	public void onClick(View v)
        	{
        		onAddButtonClicked(v);
        	}
        });
       
      this.digihomeWidgetRegistryTask = new DigihomeWidgetRegistryTask(this);
      digihomeWidgetRegistryTask.execute();
      
      this.displayedWidgets = this.getDisplayWidgets(savedInstanceState);
	  /**Restore the widgets*/
      for(Widget widget : this.displayedWidgets)
      {
    	  this.addWidget(widget);
      }
      
      return view;
	}

	public void onDigihomeWidgetRegistryChange(List<Widget> widgets)
	{
		Log.i("WebviewsFragment","onDigihomeWidgetRegistryChange");
		updateWidgetSprinner(widgets);
	}
	
	private void updateWidgetSprinner(List<Widget> widgets)
	{
		List<Widget> availableWidgets = new ArrayList<Widget>(widgets);
		availableWidgets.removeAll(displayedWidgets);
		Log.i("WebviewsFragment",availableWidgets.size()+" availableWidgets");
		this.widgetSpinner.setWidget(availableWidgets);
	}
	
    public void onAddButtonClicked(View view)
    {
    	Widget selectedWidget = widgetSpinner.getSelectedItem();
    	if(selectedWidget!=null)
    	{
        	Log.i("WebviewsFragment", "selectedWidget "+selectedWidget.getName());
        	this.addWidget(selectedWidget);
        	this.displayedWidgets.add(selectedWidget);
        	this.updateWidgetSprinner(this.digihomeWidgetRegistryTask.getWidgets());
    	}
    }
    
	@SuppressLint({ "JavascriptInterface", "SetJavaScriptEnabled" })
	public void addWidget(Widget widget)
    {
		Log.i("WebviewsFragment", "addWidget "+widget.getName());
		
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout widgetContainer = (LinearLayout) inflater.inflate(R.layout.exemple_webview, this.widgetsContainer, false);
		this.widgetsContainer.addView(widgetContainer);
		RelativeLayout widgetContainerOptionBar = (RelativeLayout) widgetContainer.getChildAt(0);
		WebView widgetContainerWebview = (WebView) widgetContainer.getChildAt(1);
		ImageView gearOption = (ImageView) widgetContainerOptionBar.getChildAt(0);
		gearOption.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{
				Widget widget = getWidget(v);
				WidgetOptionsManager widgetOptionsManager = new WidgetOptionsManager(getActivity(), v, widget);
				widgetOptionsManager.onOptionClicked();
			}
		});
		
		ImageView cross = (ImageView) widgetContainerOptionBar.getChildAt(1);
		cross.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View view)
			{
				removeWidget(view);
			}
		});
		
		/* Settings for size of WebViews and Layouts */
		int size;
		DisplayMetrics metrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int width = metrics.widthPixels;
		
		 /* Landscape */
		if(getResources().getConfiguration().orientation==Configuration.ORIENTATION_LANDSCAPE)
		{
			size = width/(this.widgetsContainer.getColumnCount())-20;
			Log.i("WebviewsFragment", "size "+size+" width "+width);
			widgetContainer.getLayoutParams().width = size;
			widgetContainer.getLayoutParams().height = size*4/5;
		}
		/* Portrait */
		else 
		{
			size=width/(this.widgetsContainer.getColumnCount())-20;
			Log.i("WebviewsFragment", "size "+size+" width "+width);
			widgetContainer.getLayoutParams().width = size;
			widgetContainer.getLayoutParams().height = size*17/20;
		}
		
		/**Set Title of the container*/
		TextView textview = (TextView) widgetContainerOptionBar.getChildAt(2);
		textview.setText(widget.getName());
		textview.setTypeface(textview.getTypeface(),Typeface.BOLD);
		
		/**Set widgetContainer Webview*/
		widgetContainerWebview.getSettings().setJavaScriptEnabled(true);
		//loads the WebView completely zoomed out
		widgetContainerWebview.getSettings().setLoadWithOverviewMode(true);
        //true makes the Webview have a normal viewport such as a normal desktop browser
        //when false the webview will have a viewport constrained to it's own dimensions
		widgetContainerWebview.getSettings().setUseWideViewPort(true);
        //override the web client to open all links in the same webview
		widgetContainerWebview.setWebViewClient(new DigihomeWebViewClient());
		widgetContainerWebview.setWebChromeClient(new DigihomeWebChromeClient());
        //Injects the supplied Java object into this WebView. The object is injected into the
        //JavaScript context of the main frame, using the supplied name. This allows the
        //Java object's public methods to be accessed from JavaScript.
		widgetContainerWebview.setInitialScale(1);
		widgetContainerWebview.addJavascriptInterface(widget, "Android");
		widgetContainerWebview.loadUrl(widget.getSmallWidgetURL());
    }
    
	private void removeWidget(View widgetView)
	{
		RelativeLayout widgetOptionBar = (RelativeLayout) widgetView.getParent();
    	LinearLayout widgetContainer = (LinearLayout) widgetOptionBar.getParent();
    	widgetsContainer.removeView(widgetContainer);
    	Widget widget = this.getWidget(widgetView);
    	if(widget != null)
    	{
    		displayedWidgets.remove(widget);
        	this.updateWidgetSprinner(this.digihomeWidgetRegistryTask.getWidgets());
    	}
    	else
    	{
        	Log.w("WebviewsFragment", "No display widget found to remove");	
    	}
	}
	
	public Widget getWidget(View widgetView)
	{
		View widgetOptionBar = (View) widgetView.getParent();
		ViewGroup widgetContainer = (ViewGroup) widgetOptionBar.getParent();
		WebView widgetWebView = (WebView) widgetContainer.getChildAt(1);
		String widgetSmallURL = widgetWebView.getUrl();
		Log.i("WebviewsFragment", "getWidget "+widgetSmallURL);
		List<Widget> widgets = this.digihomeWidgetRegistryTask.getWidgets();
		for(Widget widget : widgets)
		{
			if(widget.equals(widgetSmallURL))
			{
				return widget;
			}
		}
		return null;
	}
	
    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
    	if(digihomeWidgetRegistryTask!=null)
    	{
      	  List<Widget> widgets = digihomeWidgetRegistryTask.getWidgets();
      	  bundle.putSerializable("widgets", (Serializable) widgets);
    	}
   	  	bundle.putSerializable("displayedWidgets", (Serializable) this.displayedWidgets);
        Log.i("WebviewsFragment","onSaveInstanceState");
        super.onSaveInstanceState(bundle);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        this.digihomeWidgetRegistryTask.stopTask();
        this.digihomeWidgetRegistryTask.cancel(true);
        Log.i("WebviewsFragment","stopped ...");
    }
	
    @Override
    public void onDestroy()
    {
		super.onDestroy();
    	Log.i("WebviewsFragment","destroyed ...");
    }
    
    /***************************** Helper methods ******************************/
	
	@SuppressWarnings("unchecked")
	private List<Widget> getDisplayWidgets(Bundle savedInstanceState)
	{	
		List<Widget> displayedWidgets;
		if(this.displayedWidgets!=null)
		{
			displayedWidgets = this.displayedWidgets;
			Log.i("WebviewsFragment","get "+displayedWidgets.size()+" widgets from current displayedWidgets");
		}
		else if(savedInstanceState != null && savedInstanceState.containsKey("displayedWidgets"))
		{
			displayedWidgets = (List<Widget>) savedInstanceState.getSerializable("displayedWidgets");
	    	Log.i("WebviewsFragment","get "+displayedWidgets.size()+" displayed widgets from value saved instance : ");
		}
		else
		{
	    	  displayedWidgets = new ArrayList<Widget>();
	    	  Log.i("WebviewsFragment","No displayedWidgets found");
		}
		
		return displayedWidgets;
	}
}
