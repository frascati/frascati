/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.processor.port;

import org.ow2.frascati.digihomeapp.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class PortListDialog extends DialogFragment
{
	private int titleId;

	private String[] ports;

	private boolean[] checkedPorts;
	
	private OnClickListener onOkbuttonClickedListener;
	
	public String getSelectedValue()
	{
		StringBuilder selectedValueBuilder = new StringBuilder();
		int nbSelectedService = 0;
		for(int i=0;i<this.ports.length;i++)
		{
			if(this.checkedPorts[i])
			{
				if(nbSelectedService!=0)
				{
					selectedValueBuilder.append("|");
				}
				selectedValueBuilder.append(this.ports[i]);
				nbSelectedService++;
			}
		}
		return selectedValueBuilder.toString();
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		ViewGroup portListDialogContainer = (ViewGroup) inflater.inflate(R.layout.port_list_dialog, null);
		builder.setView(portListDialogContainer);
		TextView title = new TextView(getActivity());
		title.setText(this.titleId);
		title.setBackgroundColor(Color.WHITE);
		title.setPadding(10, 10, 10, 10);
		title.setGravity(Gravity.CENTER);
		title.setTextColor(Color.BLACK);
		title.setTextSize(20);
		builder.setCustomTitle(title);
		builder.setCancelable(false);

		final ListView portListContainer = (ListView) portListDialogContainer.findViewById(R.id.port_list_container);
		portListContainer.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.port_list_dialog_item, ports));
//		portListContainer.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_multiple_choice, ports));
		portListContainer.setItemsCanFocus(false);
		portListContainer.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		portListContainer.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3)
			{
				checkedPorts[pos] = !checkedPorts[pos];
			}
		});
		
		for(int i=0; i<this.checkedPorts.length; i++)
		{
			portListContainer.setItemChecked(i, checkedPorts[i]);
		}
		
		Button portListOkButton = (Button) portListDialogContainer.findViewById(R.id.port_list_ok_button);
		portListOkButton.setOnClickListener(this.onOkbuttonClickedListener);
		
		return builder.create();
	}
	
	public int getTitleId()
	{
		return titleId;
	}

	public void setTitleId(int titleId)
	{
		this.titleId = titleId;
	}

	public String[] getPorts()
	{
		return ports;
	}

	public void setPorts(String[] ports)
	{
		this.ports = ports;
	}

	public boolean[] getCheckedPorts()
	{
		return checkedPorts;
	}

	public void setCheckedPorts(boolean[] checkedPorts)
	{
		this.checkedPorts = checkedPorts;
	}

	public OnClickListener getOnOkbuttonClickedListener()
	{
		return onOkbuttonClickedListener;
	}

	public void setOnOkbuttonClickedListener(OnClickListener onOkbuttonClickedListener)
	{
		this.onOkbuttonClickedListener = onOkbuttonClickedListener;
	}

	public void setOnClickListener(OnClickListener onOkbuttonClickedListener)
	{
		this.onOkbuttonClickedListener = onOkbuttonClickedListener;
	}
}
