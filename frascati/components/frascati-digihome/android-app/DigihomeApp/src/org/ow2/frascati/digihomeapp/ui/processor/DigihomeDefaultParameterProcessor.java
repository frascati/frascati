/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.ui.processor;

import java.util.Random;

import org.ow2.frascati.digihomeapp.R;
import org.ow2.frascati.digihomeapp.types.parameter.DigihomeParameter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;

public class DigihomeDefaultParameterProcessor extends DigihomeParameterProcessor
{
	protected int qrCodeScanRequestCode;
	
	public DigihomeDefaultParameterProcessor(DigihomeParameter digihomeParameter)
	{
		super(digihomeParameter);
	}
	
	@Override
	protected int getFieldResourceId()
	{
		return R.layout.form_field_text;
	}

	@Override
	public View getView(final Fragment fragment)
	{
		View superView = super.getView(fragment);
		
		EditText formFieldValue = (EditText) this.container.findViewById(R.id.formFieldValue);
		formFieldValue.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3){}
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3)
			{
				onEditTextChanged(arg0.toString());
			}
			
			@Override
			public void afterTextChanged(Editable arg0){}
		});
		
		String digihomeParameterValue = this.digihomeParameter.getValue();
		if(digihomeParameterValue!=null && !digihomeParameterValue.equals(""))
		{
			formFieldValue.setText(digihomeParameterValue);
		}
		
		ImageView formFieldQRCode= (ImageView) container.findViewById(R.id.formFieldQRCode);
		formFieldQRCode.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
				Random random = new Random();
				/**lower 16bits request code*/
				qrCodeScanRequestCode = random.nextInt(1 << 15);
				fragment.startActivityForResult(intent, qrCodeScanRequestCode);
			}
		});
		return superView;
	}
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		Log.i("DigihomeTextParameterProcessor","onActivityResult requestCode : "+requestCode+", resultCode : "+resultCode);
		if (requestCode == qrCodeScanRequestCode)
		{
			if (resultCode == Activity.RESULT_OK)
			{
				String scanContent = intent.getStringExtra("SCAN_RESULT");
				EditText formFieldValue = (EditText) this.container.findViewById(R.id.formFieldValue);
				formFieldValue.setText(scanContent);
				this.digihomeParameter.setValue(scanContent);
				Log.i("DigihomeFragment", "set  " + this.digihomeParameter.getName() + " field : "+ scanContent);
			}
		}
	}

	public void onEditTextChanged(String editTextValue)
	{
		Log.i("DigihomeTextParameterProcessor","onEditTextChanged");
		this.digihomeParameter.setValue(editTextValue);
	}
}
