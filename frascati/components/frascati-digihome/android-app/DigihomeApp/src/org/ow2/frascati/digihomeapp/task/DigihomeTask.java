/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.task;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;

public abstract class DigihomeTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result>
{
//	private final static String BINDING_URI_BASE="http://192.168.0.25:8765";
//	private final static String BINDING_URI_BASE="http://192.168.0.25:8765";
	private final static String BINDING_URI_BASE="http://193.51.236.251:8765";
	
	protected final static int REQUEST_TIMEOUT = 2000;
	
	protected List<DigihomeTaskListener<Progress, Result>> digihomeTaskListeners;
	
	public DigihomeTask()
	{
		this.digihomeTaskListeners = new ArrayList<DigihomeTaskListener<Progress,Result>>();
	}
	
	protected URL getRequestURL(String... pathParams) throws MalformedURLException
	{
		StringBuilder requestBuilder = this.getRequestBuilder();
		
		if(pathParams != null)
		{
			for(String pathParam : pathParams)
			{
				if(pathParam != null)
				{
					requestBuilder.append("/"+pathParam);
				}
			}
		}
		String requestURLString = requestBuilder.toString();
		return new URL(requestURLString);
	}
	
	protected StringBuilder getRequestBuilder()
	{
		StringBuilder requestBuilder= new StringBuilder();
		requestBuilder.append(BINDING_URI_BASE);
		return requestBuilder;
	}
	
	public void addDigihomeTaskListener(DigihomeTaskListener<Progress, Result> digihomeTaskListener)
	{
		this.digihomeTaskListeners.add(digihomeTaskListener);
	}
	
	@Override
	protected void onProgressUpdate(Progress... progress)
	{
		super.onProgressUpdate(progress);
		for(DigihomeTaskListener<Progress, Result> digihomeTaskListener : this.digihomeTaskListeners)
		{
			digihomeTaskListener.onDigihomeTaskProgress(progress);
		}
	}
	
	@Override
	protected void onPostExecute(Result result)
	{
		super.onPostExecute(result);
		for(DigihomeTaskListener<Progress, Result> digihomeTaskListener : this.digihomeTaskListeners)
		{
			digihomeTaskListener.onDigihomeTaskPost(result);
		}
	}
}


//<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"> <container id="1|C%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos" searchable="0" restricted="0" parentID="0">
//<dc:title>Sample Videos</dc:title>
//<dc:date>2009-07-14T07:32:38</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|C%3A%5CUsers%5CPublic%5CMusic%5CSample%20Music" searchable="0" restricted="0" parentID="0">
//<dc:title>Sample Music</dc:title>
//<dc:date>2009-07-14T07:32:38</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="2|E%3A%5Cmusic" searchable="0" restricted="0" parentID="0">
//<dc:title>music</dc:title>
//<dc:date>2011-12-29T14:55:04</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="4|C%3A%5CUsers%5CPublic%5CPictures%5CSample%20Pictures" searchable="0" restricted="0" parentID="0">
//<dc:title>Sample Pictures</dc:title>
//<dc:date>2011-10-17T11:15:18</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="4|E%3A%5Cmusic" searchable="0" restricted="0" parentID="0">
//<dc:title>music</dc:title>
//<dc:date>2011-12-29T14:55:04</dc:date>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> <container id="23|http%3A%2F%2Ffreemiplayer.free.fr%2Fdownload%2Fpodcasts.xml" searchable="0" restricted="0" parentID="0">
//<dc:title>Podcasts</dc:title>
//<upnp:class>object.container.storageFolder</upnp:class>
//<upnp:writeStatus>NOT_WRITABLE</upnp:writeStatus>
//</container> </DIDL-Lite>


//<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"> <item id="0%2FC%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos%5CLEDAteaser.mp4%2F0" restricted="0" parentID="1|C%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos">
//<dc:title>LEDAteaser.mp4</dc:title>
//<dc:date>2013-10-31T10:22:51</dc:date>
//<upnp:class>object.item.videoItem.movie</upnp:class>
//<res size="9588642" protocolInfo="http-get:*:video/mp4:*">http://193.51.236.251:58931/FreeMi/0%2FC%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos%5CLEDAteaser.mp4%2F0</res>
//</item> <item id="0%2FC%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos%5CWildlife.wmv%2F0" restricted="0" parentID="1|C%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos">
//<dc:title>Wildlife.wmv</dc:title>
//<dc:date>2009-07-14T07:32:38</dc:date>
//<upnp:class>object.item.videoItem.movie</upnp:class>
//<res size="26246026" protocolInfo="http-get:*:video/x-ms-wmv:*">http://193.51.236.251:58931/FreeMi/0%2FC%3A%5CUsers%5CPublic%5CVideos%5CSample%20Videos%5CWildlife.wmv%2F0</res>
//</item> </DIDL-Lite>
