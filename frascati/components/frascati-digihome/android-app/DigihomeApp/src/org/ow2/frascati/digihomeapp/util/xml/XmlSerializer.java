package org.ow2.frascati.digihomeapp.util.xml;

public class XmlSerializer
{
	private StringBuilder xmlBuilder;
	
	private boolean isXmlnsSet;
	
	private String xmlns;
	
	public XmlSerializer()
	{
		this.xmlBuilder = new StringBuilder();
		this.isXmlnsSet = false;
	}
	
	public void startDocument(String encoding, boolean standAlone, String xmlns)
	{
		this.xmlns = xmlns;
		xmlBuilder.append("<?");
		xmlBuilder.append("xml version=\"1.0\" ");
		xmlBuilder.append("encoding=\""+encoding+"\" ");
		xmlBuilder.append("standalone=\""+(standAlone ? "yes" : "no")+"\"");
		xmlBuilder.append("?>");
	}
	
	public void startTag(String tag)
	{
		xmlBuilder.append("<"+tag);
		if(!isXmlnsSet)
		{
			xmlBuilder.append(" xmlns=\""+this.xmlns+"\"");
			isXmlnsSet = true;
		}
		xmlBuilder.append(">");
	}
	
	public void text(String text)
	{
		xmlBuilder.append(text);
	}
	
	public void endTag(String tag)
	{
		xmlBuilder.append("</"+tag+">");
	}
	
	@Override
	public String toString()
	{
		return this.xmlBuilder.toString();
	}
}
