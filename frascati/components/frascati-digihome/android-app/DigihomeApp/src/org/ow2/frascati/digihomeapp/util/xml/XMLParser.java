/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp.util.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.digihomeapp.util.ReflectionUtil;
import org.ow2.frascati.digihomeapp.util.StringUtil;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Log;
import android.util.Xml;

public class XMLParser
{
	public static <T> T parseStream(InputStream xmlStream, Class<T> objectClass) throws XmlPullParserException, IOException, InstantiationException, IllegalAccessException
	{
		Log.d("XMLParser", "parseStream class :" + objectClass.getName());
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			
			String content = StringUtil.getStringFromInputStream(xmlStream);
			Log.i("XMLParser", content);
			Reader reader = new StringReader(content);
			parser.setInput(reader);
			
//			parser.setInput(xmlStream, null);
			parser.nextTag();
			T object = objectClass.newInstance();
			parseObject(parser, object);
			Log.d("XMLParser", "parseStream result :" + object.toString());
			return object;
		} finally
		{
			xmlStream.close();
		}
	}

	private static <T> void parseObject(XmlPullParser parser, T object) throws XmlPullParserException, IOException, InstantiationException, IllegalAccessException
	{
		String objectClassTag = object.getClass().getSimpleName();
		Log.d("XMLParser", "parseObject tag :" + objectClassTag);
		parser.require(XmlPullParser.START_TAG, "", objectClassTag);

		String tagName;
		Field objectField;
		while (parser.next() != XmlPullParser.END_TAG)
		{
			if (parser.getEventType() != XmlPullParser.START_TAG)
			{
				continue;
			}
			tagName = parser.getName();
			Log.d("XMLParser", "tag : " + tagName);

			objectField = getField(object.getClass(), tagName);
			if (objectField == null)
			{
				Log.d("XMLParser", "No field " + tagName + " found for class " + object.getClass().getName());
				continue;
			}
			parseField(parser, object, objectField);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static <T> void parseField(XmlPullParser parser, T object, Field objectField) throws IOException, XmlPullParserException, IllegalArgumentException, IllegalAccessException,
			InstantiationException
	{
		Class<?> objectFieldType = objectField.getType();
		Log.d("XMLParser", "parseField :" + objectField.getName()+" "+objectFieldType.getName());
		if(objectFieldType.isPrimitive() || objectFieldType.equals(String.class) || objectFieldType.isEnum())
		{
			parseSimpleField(parser, object, objectField);
		}
		else if (objectFieldType.isAssignableFrom(List.class))
		{
			parseListField(parser, object, objectField);
		}
		else
		{
			parseObjectField(parser, object, objectField);
		}
	}

	private static <T> void parseSimpleField(XmlPullParser parser, T object, Field objectField) throws IOException, XmlPullParserException, IllegalArgumentException, IllegalAccessException
	{
		Class<?> objectFieldType = objectField.getType();
		Class<?> objectFieldConsistentType = ReflectionUtil.getConsistentClass(objectFieldType);
		Log.d("XMLParser", "parseField class :" + objectFieldConsistentType.getName());
		String objectFieldStringValue = readTagValue(parser, objectField.getName());
		Log.d("XMLParser", "parseField string value :" + objectFieldStringValue);
		Object objectFieldValue = ReflectionUtil.valueOf(objectFieldConsistentType, objectFieldStringValue);
		Log.d("XMLParser", "parseField value :" + objectFieldValue.toString() + " " + objectFieldValue.getClass().getSimpleName());
		objectField.set(object, objectFieldValue);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <T> void parseListField(XmlPullParser parser, T object, Field objectField) throws IllegalArgumentException, IllegalAccessException, IOException, XmlPullParserException, InstantiationException
	{
		ParameterizedType objectFieldGenericType = (ParameterizedType) objectField.getGenericType();
		Class<?> objectFieldGenericTypeClass = (Class<?>) objectFieldGenericType.getActualTypeArguments()[0];
		Log.d("XMLParser", "parseField class : List<" + objectFieldGenericTypeClass.getSimpleName() + ">");
		List objectListFieldValue = (List) objectField.get(object);
		if (objectListFieldValue == null)
		{
			objectListFieldValue = new ArrayList();
			objectField.set(object, objectListFieldValue);
		}

		Object objectFieldValue;
		if (objectFieldGenericTypeClass.isPrimitive() || objectFieldGenericTypeClass.equals(String.class))
		{
			Class<?> objectFieldGenericTypeConsistentType = ReflectionUtil.getConsistentClass(objectFieldGenericTypeClass);
			Log.d("XMLParser", "parseField class :" + objectFieldGenericTypeConsistentType.getName());
			String objectFieldStringValue = readTagValue(parser, objectField.getName());
			Log.d("XMLParser", "parseField string value :" + objectFieldStringValue);
			objectFieldValue = ReflectionUtil.valueOf(objectFieldGenericTypeConsistentType, objectFieldStringValue);
			Log.d("XMLParser", "parseField value :" + objectFieldValue.toString() + " " + objectFieldValue.getClass().getSimpleName());
		}
		else
		{
			objectFieldValue = objectFieldGenericTypeClass.newInstance();
			parseObject(parser, objectFieldValue);
		}
		objectListFieldValue.add(objectFieldValue);
	}
	
	private static <T> void parseObjectField(XmlPullParser parser, T object, Field objectField) throws InstantiationException, IllegalAccessException, XmlPullParserException, IOException
	{
		Class<?> objectFieldType = objectField.getType();
		Object objectFieldTypeInstance = objectFieldType.newInstance();
		objectField.set(object, objectFieldTypeInstance);
		parseObject(parser, objectFieldTypeInstance);
	}
	
	private static String readTagValue(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException
	{
		parser.require(XmlPullParser.START_TAG, "", tagName);
		parser.next();
		String tagValue = parser.getText();
		parser.nextTag();
		parser.require(XmlPullParser.END_TAG, "", tagName);
		Log.d("XMLParser", "readTagValue :"+tagName+" : "+tagValue);
		return tagValue;
	}

	private static Field getField(Class<?> clazz, String fiedlName)
	{
		for (Field field : ReflectionUtil.getAllFields(clazz))
		{
			field.setAccessible(true);
			if (field.getName().equalsIgnoreCase(fiedlName))
			{
				return field;
			}
		}
		return null;
	}
}
