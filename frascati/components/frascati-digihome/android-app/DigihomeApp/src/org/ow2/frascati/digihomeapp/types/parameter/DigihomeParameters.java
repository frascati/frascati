package org.ow2.frascati.digihomeapp.types.parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.digihomeapp.util.xml.XmlSerializer;

import android.util.Log;

public class DigihomeParameters
{
	public final static String DIGIHOMEPARAMETERS_NS ="http://frascati.ow2.org/digihome";
	
	public final static String DIGIHOMEPARAMETERS_TAG = "DigihomeParameters";
	
    protected List<DigihomeParameter> digihomeParameter;

    public List<DigihomeParameter> getDigihomeParameters()
    {
        if (digihomeParameter == null) {
            digihomeParameter = new ArrayList<DigihomeParameter>();
        }
        return this.digihomeParameter;
    }
    
    public DigihomeParameter getDigihomeParameter(String digihomeParameterName)
    {
        for(DigihomeParameter digihomeParameter : this.getDigihomeParameters())
        {
            if(digihomeParameter.getName().equals(digihomeParameterName))
            {
                return digihomeParameter;
            }
        }
        return null;
    }
    
    public int size()
    {
    	return this.getDigihomeParameters().size();
    }
    
    public String toXML() throws IllegalArgumentException, IllegalStateException, IOException
	{
		XmlSerializer xmlSerializer = new XmlSerializer();
	    xmlSerializer.startDocument("UTF-8", true, DIGIHOMEPARAMETERS_NS);
	    
	    xmlSerializer.startTag(DIGIHOMEPARAMETERS_TAG);
	    Log.i("toXML", this.getDigihomeParameters().size()+"");
	    for(DigihomeParameter digihomeParameter : this.getDigihomeParameters())
	    {
	    	digihomeParameter.toXML(xmlSerializer);
	    }
	    xmlSerializer.endTag(DIGIHOMEPARAMETERS_TAG);
	    return xmlSerializer.toString();
	}
    
    @Override
    public String toString()
    {
    	StringBuilder toStringBuilder = new StringBuilder();
    	toStringBuilder.append(this.getClass().getSimpleName());
    	toStringBuilder.append(" [ ");
    	for(DigihomeParameter digihomeParameter : this.getDigihomeParameters())
    	{
    		toStringBuilder.append(digihomeParameter.toString());
    		toStringBuilder.append(", ");
    	}
    	toStringBuilder.append(" ]");
    	return toStringBuilder.toString();
    }
}
