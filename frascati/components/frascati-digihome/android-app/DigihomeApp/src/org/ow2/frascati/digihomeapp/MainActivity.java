/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mathieu SCHEPENS
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihomeapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends FragmentActivity implements DialogInterface.OnClickListener
{
	/*
	 * Called when there is a screen rotation, or when the application is
	 * started
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.activity_main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.window_title);

		FragmentTabHost tabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		tabHost.setup(this, getSupportFragmentManager(), R.id.tab_container);

		Bundle args1 = new Bundle();
		args1.putString("Webviews", "Webviews");
		tabHost.addTab(tabHost.newTabSpec("Webviews").setIndicator("Webviews", getResources().getDrawable(R.drawable.digihome_icon)), WebviewsFragment.class, args1);

		Bundle args2 = new Bundle();
		args2.putString("Install", "Install");
		tabHost.addTab(tabHost.newTabSpec("Install").setIndicator("Install", getResources().getDrawable(R.drawable.option_gear)), DigihomeInstallFragment.class, args2);

		Bundle args3 = new Bundle();
		args3.putString("Services", "Services");
		tabHost.addTab(tabHost.newTabSpec("Services").setIndicator("Services", getResources().getDrawable(R.drawable.icon_livre)), DigihomeServiceFragment.class, args3);
			
		if(!this.isOnline())
		{
			AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
			alertBuilder.setTitle("OFFLine mode");
			alertBuilder.setMessage("The network must to be up to use DigihomeApp");
			alertBuilder.setPositiveButton("Close", this);
			alertBuilder.create().show();
		}
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		Log.i("MainActivity", "densityDpi : "+metrics.toString());
	}

	public boolean isOnline()
	{
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting())
		{
			return true;
		}
		return false;
	}

	@Override
	public void onClick(DialogInterface arg0, int arg1)
	{
		this.finish();
	}
}
