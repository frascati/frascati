<?xml version="1.0"?>
<!--
  * OW2 FraSCAti
  *
  * Copyright (c) 2013 Inria, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  * USA
  *
  * Contact: frascati@ow2.org
  *
  * Author: Gwenael Cattez
  *
  * Contributor(s):
  *
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<artifactId>frascati-digihome</artifactId>
		<groupId>org.ow2.frascati.components</groupId>
		<version>1.6-SNAPSHOT</version>
		<relativePath>..</relativePath>
	</parent>
	<artifactId>frascati-digihome-core</artifactId>
	<name>OW2 FraSCAti Digihome demonstration core implementation</name>

	<properties>
		<frascati.logging.properties>./logging.properties</frascati.logging.properties>
<!-- 		<digihome.resources.directory>target/classes/</digihome.resources.directory> -->
		<digihome.resources.directory>target/dependencies/</digihome.resources.directory>
		<org.ow2.frascati.bootstrap>org.ow2.frascati.bootstrap.FraSCAtiJDTRest</org.ow2.frascati.bootstrap>
		<java.net.preferIPv4Stack>true</java.net.preferIPv4Stack>
<!-- 		<org.ow2.frascati.binding.uri.base>http://193.51.236.251:8765</org.ow2.frascati.binding.uri.base> -->
<!-- 		<org.ow2.frascati.binding.uri.base>http://192.168.1.14:8765</org.ow2.frascati.binding.uri.base> -->
<!-- 		<org.ow2.frascati.binding.uri.base>http://192.168.0.13:8765</org.ow2.frascati.binding.uri.base> -->
<!-- 		<org.ow2.frascati.binding.uri.base>http://192.168.0.25:8765</org.ow2.frascati.binding.uri.base> -->
		<composite.file>digihome-core</composite.file>
	</properties>

	<dependencies>
		<!-- OW2 FraSCAti Remote Management. -->
		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-introspection-impl</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>org.ow2.frascati.components</groupId>
			<artifactId>frascati-digihome-api</artifactId>
			<version>${project.version}</version>
		</dependency>

		<!-- could be used by the deployed contribution -->
		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-binding-rest</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-binding-http</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-implementation-widget</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-binding-ws</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati.upnp</groupId>
			<artifactId>frascati-binding-upnp</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati</groupId>
			<artifactId>frascati-property-jaxb</artifactId>
			<version>${project.version}</version>
		</dependency>

		<dependency>
			<groupId>org.ow2.frascati.components</groupId>
			<artifactId>frascati-widget-registry-api</artifactId>
			<version>${project.version}</version>
			<scope>test</scope>
		</dependency>

	<dependency>
		<groupId>org.ow2.frascati</groupId>
		<artifactId>upnp-common-interfaces</artifactId>
		<version>${project.version}</version>
	</dependency>
		
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<skipTests>true</skipTests>
					<test>org.ow2.frascati.digihome.DigihomeTestSuite</test>
					<systemPropertyVariables>
						<java.util.logging.config.file>${frascati.logging.properties}</java.util.logging.config.file>
						<test.resources.directory>${digihome.resources.directory}</test.resources.directory>
					</systemPropertyVariables>
				</configuration>
			</plugin>
<!-- 			<plugin> -->
<!-- 				<groupId>org.apache.maven.plugins</groupId> -->
<!-- 				<artifactId>maven-dependency-plugin</artifactId> -->
<!-- 				<version>2.6</version> -->
<!-- 				<executions> -->
<!-- 					<execution> -->
<!-- 						<id>copy</id> -->
<!-- 						<phase>process-resources</phase> -->
<!-- 						<goals> -->
<!-- 							<goal>copy</goal> -->
<!-- 						</goals> -->
<!-- 						<configuration> -->
<!-- 							<outputDirectory>${digihome.resources.directory}</outputDirectory> -->
<!-- 							<stripVersion>true</stripVersion> -->
<!-- 							<overWriteIfNewer>true</overWriteIfNewer> -->
<!-- 							<artifactItems> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>zibase-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-zibase.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>everspring-an158-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-everspring-an158.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>visonic-MCT201</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>visonic-MCT201.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>everspring-st814-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-everspring-st814.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>aeon-doorsensor</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-aeon-doorsensor.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-WD18</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-WD18.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-SD18</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-SD18.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-COD18</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-COD18.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-KR18E</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-KR18E.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-MS18E</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-MS18E.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>bmb-MS13E2</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-bmb-MS13E2.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>tp-link-SC3130G-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-tp-link-SC3130G.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>frascati-widget-registry-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-widget-registry.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>braviaKDL40EX720-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-braviaKDL40EX720.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>philips-NP3500-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>digihome-philips-NP3500.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>frascati-digihome-simple-orchestrator</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>frascati-digihome-simple-orchestrator.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>media-player-orchestrator</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>frascati-digihome-mediaserver-orchestrator.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 								<artifactItem> -->
<!-- 									<groupId>org.ow2.frascati.components</groupId> -->
<!-- 									<artifactId>braviaKDL40EX720-impl</artifactId> -->
<!-- 									<version>${project.version}</version> -->
<!-- 									<type>zip</type> -->
<!-- 									<classifier>frascati-digihome-contribution</classifier> -->
<!-- 									<destFileName>frascati-digihome-braviaKDL40EX720.zip</destFileName> -->
<!-- 								</artifactItem> -->
<!-- 							</artifactItems> -->
<!-- 						</configuration> -->
<!-- 					</execution> -->
<!-- 				</executions> -->
<!-- 			</plugin> -->
		</plugins>
	</build>
<!-- 03 5935 8700 -->
<!-- 03 5935 8701 -->
	<profiles>
		<!-- To execute an SCA composite type 'mvn -Prun'. -->
		<profile>
			<id>run</id>
			<build>
				<defaultGoal>org.ow2.frascati.mojo:frascati-launcher-plugin:exec</defaultGoal>
				<plugins>
					<plugin>
						<groupId>org.ow2.frascati.mojo</groupId>
						<artifactId>frascati-launcher-plugin</artifactId>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>