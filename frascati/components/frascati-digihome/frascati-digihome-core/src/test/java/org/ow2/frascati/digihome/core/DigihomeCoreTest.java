/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceDescriptors;
import org.ow2.frascati.digihome.DigihomeTestSuite;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.registry.DigihomeServiceRegistryItf;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.util.FrascatiException;
import org.ow2.frascati.widget.Widget;
import org.ow2.frascati.widget.Widgets;
import org.ow2.frascati.widget.registry.WidgetRegistryItf;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
//import org.ow2.frascati.test.util.FraSCAtiTestUtils;
/**
 *
 */
public class DigihomeCoreTest
{
	private static DigihomeCoreItf digihomeCoreService;
	private static DigihomeServiceRegistryItf digihomeServiceRegistry;
	private static WidgetRegistryItf widgetRegistryService;
	
	private static List<DigihomeParameters> digihomeServiceList;
	private static List<String> digihomeServiceURLList;
	
	private static List<DigihomeParameters> digihomeOrchestratorList;
	
	private static int nbbWidget;
	
//	@BeforeClass
	public static void init_raw_demonstration()  throws Exception
	{
		FraSCAti frascati = DigihomeTestSuite.getFrascati();
		Component digihomeCoreComponent = frascati.getComposite("digihome-core");
		assertNotNull(digihomeCoreComponent);

		String digihomeCoreServiceURI = FraSCAtiTestUtils.completeBindingURI("/digihomeCoreService");
		FraSCAtiTestUtils.assertWADLExist(true, digihomeCoreServiceURI);
		digihomeCoreService = JAXRSClientFactory.create(digihomeCoreServiceURI, DigihomeCoreItf.class);
		assertNotNull(digihomeCoreService);

		String digihomeServiceRegistryServiceURI = FraSCAtiTestUtils.completeBindingURI("/digihomeServiceRegistryService");
		FraSCAtiTestUtils.assertWADLExist(true, digihomeServiceRegistryServiceURI);
		digihomeServiceRegistry = JAXRSClientFactory.create(digihomeServiceRegistryServiceURI, DigihomeServiceRegistryItf.class);
		assertNotNull(digihomeServiceRegistry);
		
		DigihomeParameters digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-widget-registry");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "demo");
		digihomeCoreService.installDigihomeService(digihomeParameters);
		String widgetRegistryServiceURL = FraSCAtiTestUtils.completeBindingURI("/widgetRegistry-demo");
		FraSCAtiTestUtils.assertWADLExist(true, widgetRegistryServiceURL);
		widgetRegistryService = JAXRSClientFactory.create(widgetRegistryServiceURL, WidgetRegistryItf.class);
		assertNotNull(widgetRegistryService);
		
		digihomeServiceList = new ArrayList<DigihomeParameters>();
		digihomeServiceURLList = new ArrayList<String>();
		nbbWidget = 0;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-zibase");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "salon");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "ip", "193.51.236.74");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/zibaseService-salon");
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-everspring-an158");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "prise 1");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A3");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/AN158-prise_1");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-everspring-an158");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "prise 2");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A8");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/AN158-prise_2");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-everspring-an158");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "prise 3");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A9");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/AN158-prise_3");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("visonic-MCT201");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "collier 1");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "visonic-address", "VS1091402498");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/visonicMCT201-collier_1");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("visonic-MCT201");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "collier 2");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "visonic-address", "VS890075906");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/visonicMCT201-collier_2");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-everspring-st814");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "sonde temperature");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A7");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/everspringST814-sonde_temperature");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "porte salon 1");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A5");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_1");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "porte salon 2");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A10");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_2");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "porte salon 3");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "zwave-address", "A11");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_3");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-tp-link-SC3130G");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "camera salon");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "login", "admin");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "password", "admin");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "ip", "193.51.236.252");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/tpSC3130G-camera_salon");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-WD18");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "detecteur eau salon 1");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "x10-address", "XS817894500");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbWD18-detecteur_eau_salon_1");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-WD18");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "detecteur eau salon 2");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "x10-address", "XS2188565860");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbWD18-detecteur_eau_salon_2");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-KR18E");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "telecommande secu");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "arm-x10-id", "XS2790680160");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "disarm-x10-id", "XS2790680161");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "A_ON-x10-id", "XS2790680130");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "A_OFF-x10-id", "XS2790680131");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "B_ON-x10-id", "XS2790680162");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "B_OFF-x10-id", "XS2790680163");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbKR18E-telecommande_secu");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-COD18");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "detecteur CO");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "x10-address", "XS2340132384");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbCOD18-detecteur_CO");
		nbbWidget++;
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-MS18E");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "detecteur mouvement 1");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "x10-address", "XS1904338992");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbMS18E-detecteur_mouvement_1");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-bmb-MS18E");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "detecteur mouvement 2");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "x10-address", "XS634753840");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/bmbMS18E-detecteur_mouvement_2");
		nbbWidget++;

		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-philips-NP3500");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "Philips lecteur multimedia");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "upnp-device-id", "F00DBABE-SA5E-1000-8000-188ED536B25D");
		digihomeServiceList.add(digihomeParameters);

		digihomeParameters = digihomeCoreService.getDigihomeParameters("frascati-digihome-braviaKDL40EX720");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "Sony TV");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "upnp-device-id", "00000000-0000-1010-8000-5453ED0F2153");
		digihomeServiceList.add(digihomeParameters);
		digihomeServiceURLList.add("/sony-braviaKDL40EX720-Sony_TV");
		digihomeServiceURLList.add("/remote-sony-braviaKDL40EX720-Sony_TV");
		nbbWidget++;
		
		digihomeOrchestratorList = new ArrayList<DigihomeParameters>();
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("frascati-digihome-simple-orchestrator");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "click_light");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/everspringAN158", "digihome-everspringAN158-prise_2/everspringAN158Service");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/rfDeviceListener", "visonicMCT201-collier_1/rfDeviceListeners");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/alarmSleepTimeOut", "3000");
		digihomeOrchestratorList.add(digihomeParameters);
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("frascati-digihome-mediaserver-orchestrator");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "run music");
//		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/media-server", "philips-NP3500-Philips_lecteur_multimedia/philipsNP3500Service");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/media-server", "sony-braviaKDL40EX720-Sony_TV/sonyBraviaKDL40EX720Service");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/rfDeviceListener", "visonicMCT201-collier_1/rfDeviceListeners");
		digihomeOrchestratorList.add(digihomeParameters);
	}

	@BeforeClass
	public static void init_LEDA_Demonstration() throws Exception
	{
		FraSCAti frascati = DigihomeTestSuite.getFrascati();
		Component digihomeCoreComponent = frascati.getComposite("digihome-core");
		assertNotNull(digihomeCoreComponent);

		String digihomeCoreServiceURI = FraSCAtiTestUtils.completeBindingURI("/digihomeCoreService");
		FraSCAtiTestUtils.assertWADLExist(true, digihomeCoreServiceURI);
		digihomeCoreService = JAXRSClientFactory.create(digihomeCoreServiceURI, DigihomeCoreItf.class);
		assertNotNull(digihomeCoreService);

		String digihomeServiceRegistryServiceURI = FraSCAtiTestUtils.completeBindingURI("/digihomeServiceRegistryService");
		FraSCAtiTestUtils.assertWADLExist(true, digihomeServiceRegistryServiceURI);
		digihomeServiceRegistry = JAXRSClientFactory.create(digihomeServiceRegistryServiceURI, DigihomeServiceRegistryItf.class);
		assertNotNull(digihomeServiceRegistry);
		
		DigihomeParameters digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-frascati-widget-registry");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "demo");
		digihomeCoreService.installDigihomeService(digihomeParameters);
		String widgetRegistryServiceURL = FraSCAtiTestUtils.completeBindingURI("/widgetRegistry-demo");
		FraSCAtiTestUtils.assertWADLExist(true, widgetRegistryServiceURL);
		widgetRegistryService = JAXRSClientFactory.create(widgetRegistryServiceURL, WidgetRegistryItf.class);
		assertNotNull(widgetRegistryService);
		
		digihomeServiceList = new ArrayList<DigihomeParameters>();
		digihomeServiceURLList = new ArrayList<String>();
		nbbWidget = 0;
		
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-zibase-1"));
		digihomeServiceURLList.add("/zibaseService-zibase");
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-everspring-an158-1"));
		digihomeServiceURLList.add("/AN158-prise_1");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-everspring-an158-2"));
		digihomeServiceURLList.add("/AN158-prise_2");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-everspring-an158-3"));
		digihomeServiceURLList.add("/AN158-prise_3");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-visonic-MCT201-1"));
		digihomeServiceURLList.add("/visonicMCT201-collier_1");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-visonic-MCT201-2"));
		digihomeServiceURLList.add("/visonicMCT201-collier_2");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-everspring-st814-1"));
		digihomeServiceURLList.add("/everspringST814-sonde_temperature");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor-1"));
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_1");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor-2"));
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_2");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-aeon-doorsensor-3"));
		digihomeServiceURLList.add("/aeonDoorSensor-porte_salon_3");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-tp-link-SC3130G-1"));
		digihomeServiceURLList.add("/tpSC3130G-camera_salon");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-WD18-1"));
		digihomeServiceURLList.add("/bmbWD18-detecteur_eau_salon_1");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-WD18-2"));
		digihomeServiceURLList.add("/bmbWD18-detecteur_eau_salon_2");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-KR18E-1"));
		digihomeServiceURLList.add("/bmbKR18E-telecommande_secu");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-COD18-1"));
		digihomeServiceURLList.add("/bmbCOD18-detecteur_CO");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-MS18E-1"));
		digihomeServiceURLList.add("/bmbMS18E-detecteur_mouvement_1");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-bmb-MS18E-2"));
		digihomeServiceURLList.add("/bmbMS18E-detecteur_mouvement_2");
		nbbWidget++;
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-philips-NP3500-1"));
		digihomeServiceList.add(digihomeCoreService.getDigihomeParameters("digihome-frascati-sony-braviaKDL40EX720-1"));
		digihomeServiceURLList.add("/sony-braviaKDL40EX720-Sony_TV");
		digihomeServiceURLList.add("/remote-sony-braviaKDL40EX720-Sony_TV");
		nbbWidget++;
		
		digihomeOrchestratorList = new ArrayList<DigihomeParameters>();
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-frascati-simple-orchestrator");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "click_light");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/everspringAN158", "digihome-everspringAN158-prise_2/everspringAN158Service");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/rfDeviceListener", "visonicMCT201-collier_1/rfDeviceListeners");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-simple-orchestrator-$[digihome-name]/alarmSleepTimeOut", "3000");
		digihomeOrchestratorList.add(digihomeParameters);
		
		digihomeParameters = digihomeCoreService.getDigihomeParameters("digihome-frascati-mediaserver-orchestrator");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-name", "run music");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/media-server", "sony-braviaKDL40EX720-Sony_TV/sonyBraviaKDL40EX720Service");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/rfDeviceListener", "digihome-aeonDoorSensor-detecteur_porte_2/rfDeviceListeners");
		DigihomeParametersUtil.setDigihomeParameter(digihomeParameters, "digihome-mediaserver-orchestrator-$[digihome-name]/mediaName", "alarm");
		digihomeOrchestratorList.add(digihomeParameters);
	}
	
	/**
	 * Randomly deploy a list of of DigihomeParameters
	 * @param digihomeParametersList
	 */
	private void randomlyDeployDigihomeParameters(List<DigihomeParameters> digihomeParametersList) throws Exception
	{
		List<DigihomeParameters> digihomeParametersCloneList = new ArrayList<DigihomeParameters>(digihomeParametersList);
		Random random = new Random();
		int randomDeviceIndex;
		DigihomeParameters toDeployDigihomeService;
		while(!digihomeParametersCloneList.isEmpty())
		{
			randomDeviceIndex = random.nextInt(digihomeParametersCloneList.size());
			toDeployDigihomeService = digihomeParametersCloneList.remove(randomDeviceIndex);
			digihomeCoreService.installDigihomeService(toDeployDigihomeService);
		}
	}
	
	/**
	 * Deploy all services and check WADL services and URL widget
	 */
	@Test
	public void randomDeployTest() throws Exception
	{
		randomlyDeployDigihomeParameters(digihomeServiceList);

//		for(String digihomeServiceURL : digihomeServiceURLList)
//		{
//			FraSCAtiTestUtils.assertWADLExist(true, digihomeServiceURL);
//		}
		
		randomlyDeployDigihomeParameters(digihomeOrchestratorList);
		
		Widgets widgets = widgetRegistryService.getWidgets();
		List<Widget> widgetList = widgets.getWidgets();
		System.out.println("excpected : "+nbbWidget);
		System.out.println("actual : "+widgetList.size());
		for(Widget widget : widgetList)
		{
			System.out.println(widget.getName());
		}
		
//		assertEquals(nbbWidget, widgetList.size());
//		for (Widget widget : widgetList)
//		{
//			FraSCAtiTestUtils.assertURLExist(true, widget.getIconURL());
//			FraSCAtiTestUtils.assertURLExist(true, widget.getSmallWidgetURL());
//			FraSCAtiTestUtils.assertURLExist(true, widget.getLargeWidgetURL());
//		}
//		
//		DigihomeServiceDescriptors digihomeServiceDescriptors = digihomeServiceRegistry.getDigihomeServiceDescriptors();
//		assertEquals(digihomeServiceDescriptors.getDigihomeServiceDescriptors().size(), digihomeServiceList.size()+digihomeOrchestratorList.size()+1);

		
		System.in.read();
	}
}
