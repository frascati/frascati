/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.digihome.core.DigihomeCoreTest;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
@RunWith(Suite.class)
@SuiteClasses({DigihomeCoreTest.class})
public class DigihomeTestSuite
{
    private static final String DEFAULT_TEST_RESOURCES_DIR="src/test/resources/";
    
    private static String TEST_RESOURCES_DIR;
    
    /** REST binding URI */
    public final static String BINDING_URI = System.getProperty(AbstractBindingFactoryProcessor.BINDING_URI_BASE_PROPERTY_NAME, AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE);
    
    private static FraSCAti frascati;
    
    /** The Frascati Domain */
    private static RemoteScaDomain domain;

    
    @BeforeClass
    public static void init() throws FrascatiException
    {
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTRest");
        TEST_RESOURCES_DIR=System.getProperty("test.resources.directory");
        frascati=FraSCAti.newFraSCAti();
        assertNotNull(frascati);

        domain = JAXRSClientFactory.create(BINDING_URI + "/introspection", RemoteScaDomain.class);
    }
    
    public static FraSCAti getFrascati()
    {
        return frascati;
    }

    public static RemoteScaDomain getDomain()
    {
        return domain;
    }
    
    @AfterClass
    public static void finish() throws FrascatiException
    {
        frascati.close();
    }
    
    public static File getResource(String resourceName)
    {
        File resource;
        if(TEST_RESOURCES_DIR!=null)
        {
            resource=new File(TEST_RESOURCES_DIR+resourceName);
            if(resource.exists())
            {
                return resource;
            }
        }
        resource=new File(DEFAULT_TEST_RESOURCES_DIR+resourceName);
        assertTrue(resource.exists());
        return resource;
    }
}
