/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.core;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceDescriptor;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.exception.InvalidDigihomeIdException;
import org.ow2.frascati.digihome.processor.DigihomeProcessorItf;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.service.registry.DigihomeServiceRegistryItf;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;
import org.ow2.frascati.mojo.digihomecontribution.DigihomeContribution;

/**
 *
 */
@Scope("COMPOSITE")
public class DigihomeCoreImpl implements DigihomeCoreItf
{
    private File resourceDirectory;
 
    @Reference(name="digihomeProcessors")
    private List<DigihomeProcessorItf> digihomeProcessors;
    
    @Reference(name="digihomeServiceRegistry")
    private DigihomeServiceRegistryItf digihomeServiceRegistry;
    
    private Map<String, DigihomeContribution> contributionFiles;
    
    public DigihomeCoreImpl()
    {
        this.digihomeProcessors=new ArrayList<DigihomeProcessorItf>();
        
//        String resourceDirectoryPath = System.getProperty("digihome.resources.directory", "target/dependencies");
        String resourceDirectoryPath = System.getProperty("digihome.resources.directory", "src/main/resources");
        this.resourceDirectory = new File(resourceDirectoryPath);
        this.resourceDirectory.mkdirs();
        DigihomeLogger.log(this,"Resource directory : "+this.resourceDirectory.getAbsolutePath());
        this.contributionFiles = new HashMap<String, DigihomeContribution>();
    }
    
    /**
     * @throws InvalidDigihomeIdException 
     * @throws ClassNotFoundException 
     * @see org.ow2.frascati.digihome.DigihomeCoreItf#installDigihomeService(java.lang.String, org.ow2.frascati.digihome.DigihomeParameters)
     */
    public DigihomeServiceDescriptor installDigihomeService(DigihomeParameters digihomeParameters) throws DigihomeException
    {
        //TODO check if a DigihomeService already exist for UUID
        DigihomeLogger.logEntering(this, "installDigihomeService");
        
        DigihomeParameter digihomeIdParameter = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, "digihomeId");
        String digihomeIdParameterValue = digihomeIdParameter.getValue();
        DigihomeService digihomeService = this.digihomeServiceRegistry.getDigihomeService(digihomeIdParameterValue);
        /**if digihomeService is null create a new DigihomeService otherwise reuse existing one*/
        if(digihomeService==null)
        {
            DigihomeLogger.log(this, "Create a new DigihomeService for digihomeId "+digihomeIdParameterValue);
            digihomeService = new DigihomeService(digihomeIdParameterValue);
            DigihomeContribution digihomeContribution = this.getDigihomeContribution(digihomeIdParameterValue);
            digihomeService.setDigihomeContribution(digihomeContribution);
        }
        else
        {
            DigihomeLogger.log(this, "Reuse DigihomeService for digihomeId "+digihomeIdParameterValue +", current status : "+digihomeService.getDigihomeServiceStatus());
        }
        digihomeService.setDigihomeParameters(digihomeParameters);
        
        DigihomeServiceStatus currentDigihomeServiceStatus;
        DigihomeProcessorItf digihomeProcessor;
        do
        {
            currentDigihomeServiceStatus = digihomeService.getDigihomeServiceStatus();
            digihomeProcessor = this.getDigihomeProcessor(currentDigihomeServiceStatus);
            if(digihomeProcessor==null)
            {
                /**if no DigihomeProcessor is found for the current DigihomeServiceStatus*/
                break;
            }
            digihomeProcessor.processDigihomeService(digihomeService);
        }
        /**while the currentState change after DigihomeService is proceed, avoid using the same processor several time*/
        while(!digihomeService.getDigihomeServiceStatus().equals(currentDigihomeServiceStatus));

        return digihomeService.getDescriptor();
    }

    private DigihomeProcessorItf getDigihomeProcessor(DigihomeServiceStatus digihomeServiceStatus)
    {
        for(DigihomeProcessorItf digihomeProcessor : this.digihomeProcessors)
        {
            if(digihomeProcessor.getProcessDigihomeServiceStatus().equals(digihomeServiceStatus))
            {
                return digihomeProcessor;
            }
        }
        return null;
    }
    
    /***
     * @throws InvalidDigihomeIdException 
     * @see org.ow2.frascati.digihome.DigihomeCoreItf#getDigihomeParameters(java.lang.String)
     */
    public DigihomeParameters getDigihomeParameters(String digihomeId) throws DigihomeException
    {
        DigihomeLogger.logEntering(this, "getDigihomeParameters", digihomeId);
        DigihomeContribution digihomeContribution = this.getDigihomeContribution(digihomeId);
        DigihomeParameters digihomeParameters = this.extractDigihomeParameters(digihomeContribution);
        
        DigihomeParameter digihomeIdParameter = new DigihomeParameter();
        digihomeIdParameter.setName("digihomeId");
        digihomeIdParameter.setType(DigihomeParameterType.HIDDEN);
        digihomeIdParameter.setValue(digihomeId);
        digihomeParameters.getDigihomeParameters().add(digihomeIdParameter);
        return digihomeParameters;
    }
   
    private DigihomeContribution getDigihomeContribution(String digihomeId) throws DigihomeException
    {
        DigihomeContribution digihomeContribution = this.contributionFiles.get(digihomeId);
        if(digihomeContribution==null)
        {
            try
            {
                File digihomeContributionFile = new File(this.resourceDirectory,digihomeId+".zip");
                digihomeContribution = new DigihomeContribution(digihomeContributionFile);
                this.contributionFiles.put(digihomeId, digihomeContribution);
            }
            catch(Exception e)
            {
                throw new InvalidDigihomeIdException(digihomeId, e);
            }
        }
        return digihomeContribution;
    }
    
    private DigihomeParameters extractDigihomeParameters(DigihomeContribution digihomeContribution)
    {
        DigihomeParameters digihomeParameters = new DigihomeParameters();
        DigihomeParameter digihomeParameter;
        
        String digihomeParameterTypeString;
        DigihomeParameterType digihomeParameterType;
        
        for(org.ow2.frascati.mojo.DigihomeParameter digihomeMojoParameter : digihomeContribution.getDigihomeParameters())
        {
            DigihomeLogger.log(this, "org.ow2.frascati.mojo.DigihomeParameter "+digihomeMojoParameter.getName());
            digihomeParameter = new DigihomeParameter();
            digihomeParameter.setName(digihomeMojoParameter.getName());
            digihomeParameter.setValue(digihomeMojoParameter.getValue());
            digihomeParameterTypeString = digihomeMojoParameter.getType();
            if(digihomeParameterTypeString != null && !digihomeParameterTypeString.equals("") && DigihomeParameterType.valueOf(digihomeParameterTypeString)!=null)
            {
                digihomeParameterType = DigihomeParameterType.valueOf(digihomeParameterTypeString);
                digihomeParameter.setType(digihomeParameterType);
                digihomeParameters.getDigihomeParameters().add(digihomeParameter);
            }

        }
        return digihomeParameters;
    }
}
