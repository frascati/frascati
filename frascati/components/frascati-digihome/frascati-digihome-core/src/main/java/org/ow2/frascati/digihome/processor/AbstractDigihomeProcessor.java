/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.processor;

import java.util.ArrayList;
import java.util.List;

import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.DigihomeService;

/**
 *
 */
public abstract class AbstractDigihomeProcessor implements DigihomeProcessorItf
{

    /**
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#processDigihomeService(org.ow2.frascati.digihome.service.DigihomeService)
     */
    public void processDigihomeService(DigihomeService digihomeService) throws DigihomeException
    {
        List<DigihomeParameterType> toCheckParameterTypes = this.getCheckedDigihomeParameterTypes();
        for(DigihomeParameter digihomeParameter : digihomeService.getDigihomeParameters().getDigihomeParameters())
        {
            if(toCheckParameterTypes.contains(digihomeParameter.getType()))
            {
                this.checkDigihomeParameter(digihomeService, digihomeParameter);
            }
        }
    }

    public List<DigihomeParameterType> getCheckedDigihomeParameterTypes()
    {
        return new ArrayList<DigihomeParameterType>();
    }
    
    public void checkDigihomeParameter(DigihomeService digihomeService, DigihomeParameter digihomeParameter) throws DigihomeException
    {
       checkDigihomeParameter(digihomeParameter);
    }
    
    public void checkDigihomeParameter(DigihomeParameter digihomeParameter) throws DigihomeException
    {
        //nothing to do
    }
}
