/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.exception.DigihomeException;

/**
 *
 */
public class DigihomeParametersUtil
{
    private static final String CONTEXTUAL_PROPERTY_REGEX = ".*\\$\\[(.*)\\].*";

    private static final String PORT_VALUES_WILDCARD = "\\|";

    public static DigihomeParameter getDigihomeParameter(DigihomeParameters digihomeParameters, String parameterName)
    {
        for (DigihomeParameter digihomeParameter : digihomeParameters.getDigihomeParameters())
        {
            if (digihomeParameter.getName().equals(parameterName))
            {
                return digihomeParameter;
            }
        }
        return null;
    }

    public static boolean setDigihomeParameter(DigihomeParameters digihomeParameters, String parameterName, String parameterValue)
    {
    	DigihomeParameter digihomeParameter = getDigihomeParameter(digihomeParameters, parameterName);
    	if(digihomeParameter == null)
    	{
    		return false;
    	}
		digihomeParameter.setValue(parameterValue);
		return true;
    }
    
    public static List<DigihomeParameter> getDigihomeParameter(DigihomeParameters digihomeParameters, DigihomeParameterType... digihomeParameterType)
    {
        if (digihomeParameterType == null)
        {
            return digihomeParameters.getDigihomeParameters();
        }

        List<DigihomeParameterType> digihomeParameterTypes = Arrays.asList(digihomeParameterType);
        List<DigihomeParameter> digihomeParametersTyped = new ArrayList<DigihomeParameter>();
        for (DigihomeParameter digihomeParameter : digihomeParameters.getDigihomeParameters())
        {
            if (digihomeParameterTypes.contains(digihomeParameter.getType()))
            {
                digihomeParametersTyped.add(digihomeParameter);
            }
        }
        return digihomeParametersTyped;
    }

    public static List<String> getDigihomePortParameterValues(DigihomeParameter digihomePortParameter)
    {
        DigihomeParameterType digihomeParameterType = digihomePortParameter.getType();
        if (!digihomeParameterType.equals(DigihomeParameterType.SCA_REFERENCE) && !digihomeParameterType.equals(DigihomeParameterType.SCA_SERVICE))
        {
            return null;
        }

        String[] digihomePortParameterSplittedValue = digihomePortParameter.getValue().split(PORT_VALUES_WILDCARD);
        return Arrays.asList(digihomePortParameterSplittedValue);
    }

    public static boolean containsContextualProperty(String s)
    {
        return s.matches(CONTEXTUAL_PROPERTY_REGEX);
    }

    public static String replaceContextualProperty(String s, Map<String, String> contextualProperties)
    {
        String contextualPropertyName = s.replaceAll(CONTEXTUAL_PROPERTY_REGEX, "$1");
        if (!contextualProperties.containsKey(contextualPropertyName))
        {
            return s;
        }
        String contextualPropertyNameRegex = "\\$\\[" + contextualPropertyName + "\\]";
        String contextualPropertyValue = contextualProperties.get(contextualPropertyName);
        return s.replaceAll(contextualPropertyNameRegex, contextualPropertyValue);
    }

    public static String getSCAParentPath(String path)
    {
        return path.substring(0, path.lastIndexOf("/"));
    }

    public static String getSCAPathValue(String path)
    {
        return path.substring(path.lastIndexOf("/")+1);
    }
    
    public static void mustBeSet(DigihomeParameter digihomeParameter) throws DigihomeException
    {
        String digihomeNameParameterValue = digihomeParameter.getValue();
        if (digihomeNameParameterValue == null || digihomeNameParameterValue.equals(""))
        {
            throw new DigihomeException(digihomeParameter.getName()+" parameter must be set");
        }
    }
}
