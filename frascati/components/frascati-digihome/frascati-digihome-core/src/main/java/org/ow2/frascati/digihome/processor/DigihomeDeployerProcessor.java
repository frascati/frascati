/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;
import org.ow2.frascati.mojo.digihomecontribution.DigihomeContribution;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Interface;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;

/**
 *
 */
@Scope("COMPOSITE")
public class DigihomeDeployerProcessor extends AbstractDigihomeProcessor
{
    @Reference(name = "deployment")
    private Deployment deployment;

    @Reference(name = "introspection")
    private RemoteScaDomain introspection;

    /** The library loaded in the FraSCAti ClassLoader */
    private List<String> loadedLibrairies;

    @Init
    public void init()
    {
        this.loadedLibrairies = new ArrayList<String>();
    }

    /**
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#getProcessDigihomeServiceStatus()
     */
    public DigihomeServiceStatus getProcessDigihomeServiceStatus()
    {
        return DigihomeServiceStatus.VALIDATED;
    }

    /**
     * @throws DigihomeException 
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#processDigihomeService(org.ow2.frascati.digihome.service.DigihomeService)
     */
    public void processDigihomeService(DigihomeService digihomeService) throws DigihomeException
    {
        DigihomeLogger.logEntering(this, "processDigihomeService");
        super.processDigihomeService(digihomeService);
        
        DigihomeContribution digihomeServiceContribution = digihomeService.getDigihomeContribution();
        String libName;
        for (File sharedLib : digihomeServiceContribution.getShraredLibs())
        {
            libName = sharedLib.getName();
            if (!loadedLibrairies.contains(libName))
            {
                DigihomeLogger.log(this, "Add librairy " + libName + " to FraSCAti Classloader");
                try
                {
                    deployment.loadLibrary(FileUtil.getStringFromFile(sharedLib));
                    loadedLibrairies.add(sharedLib.getName());
                } catch (Exception exception)
                {
                    DigihomeLogger.log(this, Level.SEVERE, exception.getClass().getName() + " when trying to deploy " + sharedLib.getName() + " in the FraSCAti Classloader");
                }
            } else
            {
                DigihomeLogger.log(this, libName + " librairy is already loaded in the FraSCAti Classloader, skip it");
            }
        }

        try
        {
            File contributionFile = digihomeServiceContribution.getContributionFile();
            MultivaluedMap<String, String> deploymentParameters = new MetadataMap<String, String>();
            String encodedContributionFile = FileUtil.getStringFromFile(contributionFile);
            deploymentParameters.add("contribution", encodedContributionFile);
            DigihomeParameters digihomeServiceParameters = digihomeService.getDigihomeParameters();
            for (DigihomeParameter contextualPropertyParameter : DigihomeParametersUtil.getDigihomeParameter(digihomeServiceParameters, DigihomeParameterType.SCA_CONTEXTUAL_PROPERTY))
            {
                deploymentParameters.add(contextualPropertyParameter.getName(), contextualPropertyParameter.getValue());
                DigihomeLogger.log(this, " Set contextual property " + contextualPropertyParameter.getName() + " : " + contextualPropertyParameter.getValue());
            }

            StringList deployComposites = deployment.deployContribution(deploymentParameters);
            digihomeService.setDeployedComposites(deployComposites.getStringList());
            digihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.DEPLOYED);
        }
        catch (Exception exception)
        {
            DigihomeLogger.log(this, Level.SEVERE, exception.getClass().getName() + " when trying to deploy digihome contribution " + digihomeService.getDigihomeId());
            throw new DigihomeException(exception.getClass().getName() + " when trying to deploy digihome contribution " + digihomeService.getDigihomeId(), exception);
        }
        
    }
}
