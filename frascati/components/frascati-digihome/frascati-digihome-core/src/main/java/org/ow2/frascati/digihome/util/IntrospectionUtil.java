/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.util;

import java.util.List;

import org.ow2.frascati.remote.introspection.resources.Binding;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Multiplicity;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.stringlist.StringList;

/**
 *
 */
public class IntrospectionUtil
{
    /**
     * @param component
     * @return true if all references of the component are bounded
     */
    public static boolean isBoundedComponent(Component component)
    {
        for(Port portReference : component.getReferences())
        {
            if(!isBoundedPort(portReference))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * @param port
     * @return true if the port is bounded or if it's a multiple reference
     */
    public static boolean isBoundedPort(Port port)
    {
        if(isMultiplePort(port))
        {
            return true;
        } else
        {
            List<Binding> portBindings = port.getBindings();
            return portBindings != null && !portBindings.isEmpty();
        }
    }

    /**
     * @param port
     * @return true if multiplicity of the port is 0..n 1..n
     */
    public static boolean isMultiplePort(Port port)
    {
        Multiplicity portMultiplicity = port.getMutiplicity();
        return portMultiplicity.equals(Multiplicity._0N) || portMultiplicity.equals(Multiplicity._1N);
    }
    
    
    /**
     * @param ports
     * @return
     */
    public static StringList convertPortsList(List<Port> ports)
	{
		StringList convertedPortsList  = new StringList();
		for(Port port : ports)
		{
			convertedPortsList.getStringList().add(port.getPath());
		}
		return convertedPortsList;
	}
}
