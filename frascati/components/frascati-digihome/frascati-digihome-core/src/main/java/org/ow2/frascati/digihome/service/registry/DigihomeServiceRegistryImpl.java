/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.service.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeServiceDescriptor;
import org.ow2.frascati.digihome.DigihomeServiceDescriptors;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.exception.InvalidDigihomeIdException;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.util.IntrospectionUtil;
import org.ow2.frascati.remote.introspection.resources.Interface;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.stringlist.StringList;

/**
 *
 */
@Scope("COMPOSITE")
public class DigihomeServiceRegistryImpl implements DigihomeServiceRegistryItf
{
	@Reference(name = "classloader-manager")
	protected ClassLoaderManager classLoaderManager;

	private Map<String, DigihomeService> digihomeServiceRegistryMap;

	public DigihomeServiceRegistryImpl()
	{
		this.digihomeServiceRegistryMap = new HashMap<String, DigihomeService>();
	}

	public boolean addDigihomeService(DigihomeService digihomeService)
	{
	    if(digihomeServiceRegistryMap.containsKey(digihomeService.getDigihomeId()))
	    {
	        DigihomeLogger.log(this, Level.SEVERE, "A DigihomeService with id "+digihomeService.getDigihomeId()+" is already registered");
	        return false;
	    }
	    this.digihomeServiceRegistryMap.put(digihomeService.getDigihomeId(), digihomeService);
	    return true;
	}

	public DigihomeService getDigihomeService(String digihomeId)
	{
		return this.digihomeServiceRegistryMap.get(digihomeId);
	}

	public DigihomeServiceDescriptors getDigihomeServiceDescriptors()
	{
		List<DigihomeServiceDescriptor> digihomeServiceDescriptorList = new ArrayList<DigihomeServiceDescriptor>();
		for (DigihomeService digihomeService : digihomeServiceRegistryMap.values())
		{
			digihomeServiceDescriptorList.add(digihomeService.getDescriptor());
		}
		DigihomeServiceDescriptors digihomeServiceDescriptors = new DigihomeServiceDescriptors();
		digihomeServiceDescriptors.getDigihomeServiceDescriptors().addAll(digihomeServiceDescriptorList);
		return digihomeServiceDescriptors;
	}

	public DigihomeServiceDescriptor getDigihomeServiceDescriptor(String digihomeId) throws InvalidDigihomeIdException
	{
		if (!this.digihomeServiceRegistryMap.containsKey(digihomeId))
		{
			return null;
		}
		DigihomeService digihomeService = this.digihomeServiceRegistryMap.get(digihomeId);
		return digihomeService.getDescriptor();
	}

	public List<DigihomeService> getDigihomeServices(DigihomeServiceStatus digihomeServiceStatus)
	{
		List<DigihomeService> availableDigihomeServices = new ArrayList<DigihomeService>();
		for (DigihomeService digihomeService : this.digihomeServiceRegistryMap.values())
		{
			if (digihomeService.getDigihomeServiceStatus().equals(digihomeServiceStatus))
			{
				availableDigihomeServices.add(digihomeService);
			}
		}
		return availableDigihomeServices;
	}

	public StringList getAvailableServices() throws DigihomeException
	{
		return this.getAvailableServices("");
	}

	public StringList getAvailableServices(String serviceClassString) throws DigihomeException
	{
		Class<?> serviceClass = null;
		if (serviceClassString != null && !serviceClassString.equals(""))
		{
			serviceClass = this.getClazz(serviceClassString);
		}
		List<Port> availablePorts = this.getAvailableServices(serviceClass);
		return IntrospectionUtil.convertPortsList(availablePorts);
	}

	public List<Port> getAvailableServices(Class<?> serviceClass)
	{
		return this.getAvailablePorts(DigihomeParameterType.SCA_SERVICE, serviceClass);
	}

	public StringList getAvailableReferences() throws DigihomeException
	{
		return this.getAvailableReferences("");
	}

	public StringList getAvailableReferences(String referenceClassString) throws DigihomeException
	{
		Class<?> referenceClass = null;
		if (referenceClassString != null && !referenceClassString.equals(""))
		{
			referenceClass = this.getClazz(referenceClassString);
		}
		List<Port> availablePorts = this.getAvailableReferences(referenceClass);
		return IntrospectionUtil.convertPortsList(availablePorts);
	}

	public List<Port> getAvailableReferences(Class<?> referenceClass)
	{
		return this.getAvailablePorts(DigihomeParameterType.SCA_REFERENCE, referenceClass);
	}

	public List<Port> getAvailablePorts(DigihomeParameterType digihomeParameterType, Class<?> portClass)
	{
		if (!digihomeParameterType.equals(DigihomeParameterType.SCA_SERVICE) && !digihomeParameterType.equals(DigihomeParameterType.SCA_REFERENCE))
		{
			return null;
		}

		List<Port> digihomeServicePort;
		List<Port> availablePorts = new ArrayList<Port>();
		for (DigihomeService digihomeService : this.getBoundedDigihomeService())
		{
			if (digihomeParameterType.equals(DigihomeParameterType.SCA_SERVICE))
			{
				digihomeServicePort = digihomeService.getServices(portClass);
			} else
			{
				digihomeServicePort = digihomeService.getReferences(portClass);
			}
			availablePorts.addAll(digihomeServicePort);
		}
		return availablePorts;
	}

	/******************** Helper method ************/

	private List<DigihomeService> getBoundedDigihomeService()
	{
		List<DigihomeService> boundedDigihomeService = new ArrayList<DigihomeService>();
		for (DigihomeService digihomeService : this.digihomeServiceRegistryMap.values())
		{
			if (digihomeService.getDigihomeServiceStatus().compareTo(DigihomeServiceStatus.BOUNDED) >= 0)
			{
				boundedDigihomeService.add(digihomeService);
			}
		}
		return boundedDigihomeService;
	}

	public Class<?> getPortClass(Port port) throws DigihomeException
	{
		Interface referenceInterface = port.getImplementedInterface();
		String className = referenceInterface.getClazz();
		return this.getClazz(className);
	}

	private Class<?> getClazz(String clazzName) throws DigihomeException
	{
		try
		{
			return classLoaderManager.getClassLoader().loadClass(clazzName);
		} catch (ClassNotFoundException e)
		{
			throw new DigihomeException(e);
		}
	}

}
