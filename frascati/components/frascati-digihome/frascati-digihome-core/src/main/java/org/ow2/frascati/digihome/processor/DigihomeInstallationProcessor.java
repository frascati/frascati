/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.processor;

import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Interface;
import org.ow2.frascati.remote.introspection.resources.Method;
import org.ow2.frascati.remote.introspection.resources.Port;

/**
 *
 */
public class DigihomeInstallationProcessor extends AbstractDigihomeProcessor
{
    @Reference(name="introspection")
    private RemoteScaDomain introspection;
    
    /***
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#
     * getProcessDigihomeServiceStatus()
     */
    public DigihomeServiceStatus getProcessDigihomeServiceStatus()
    {
        return DigihomeServiceStatus.BOUNDED;
    }

    @Override
    public List<DigihomeParameterType> getCheckedDigihomeParameterTypes()
    {
        List<DigihomeParameterType> toCheckDigihomeParameterTypes = super.getCheckedDigihomeParameterTypes();
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.DIGIHOME_METHOD);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.DIGIHOME_METHOD_REQUEST);
        return toCheckDigihomeParameterTypes;
    }

    @Override
    public void checkDigihomeParameter(DigihomeService digihomeService, DigihomeParameter digihomeParameter) throws DigihomeException
    {
        DigihomeParameterType digihomeParameterType = digihomeParameter.getType();
        if(digihomeParameterType.equals(DigihomeParameterType.DIGIHOME_METHOD))
        {
            this.checkDigihomeMethodPropertyParameter(digihomeParameter);
        }
        else if(digihomeParameterType.equals(DigihomeParameterType.DIGIHOME_METHOD_REQUEST))
        {
            this.checkDigihomeMethodRequestPropertyParameter(digihomeService, digihomeParameter);
        }
    }
    
    /***
     * 
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#
     * processDigihomeService(org.ow2.frascati.digihome.service.DigihomeService)
     */
    public void processDigihomeService(DigihomeService digihomeService) throws DigihomeException
    {
        super.processDigihomeService(digihomeService);
        DigihomeLogger.logEntering(this, "processDigihomeService");
        
        List<DigihomeParameter> digihomeMehthodParameters = DigihomeParametersUtil.getDigihomeParameter(digihomeService.getDigihomeParameters(), DigihomeParameterType.DIGIHOME_METHOD);
        if(digihomeMehthodParameters.isEmpty())
        {
            //No method parameter define, digihomeService is consider as installed
            digihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.INSTALLED);
            DigihomeLogger.logEntering(this, "No DIGIHOME_METHOD parameter found for DigihomeService : "+digihomeService.getDigihomeId());
            return;
        }
        
        List<DigihomeParameter> digihomeMehthodRequestParameters = DigihomeParametersUtil.getDigihomeParameter(digihomeService.getDigihomeParameters(), DigihomeParameterType.DIGIHOME_METHOD);
        if(digihomeMehthodRequestParameters.size() == 0)
        {
            DigihomeLogger.logEntering(this, "No DIGIHOME_METHOD_REQUEST parameter found for DigihomeService : "+digihomeService.getDigihomeId());
            return;
        }
        
        boolean isDigihomeMethodRequestAlreadySucced;
        boolean digihomeMethodRequestStatus;
        for(DigihomeParameter digihomeMehthodRequestParameter : digihomeMehthodRequestParameters)
        {
            try
            {
                isDigihomeMethodRequestAlreadySucced = Boolean.valueOf(digihomeMehthodRequestParameter.getValue());
            }
            catch (Exception e)
            {
                isDigihomeMethodRequestAlreadySucced = false;
            }
            
            if(isDigihomeMethodRequestAlreadySucced)
            {
                DigihomeLogger.logEntering(this,digihomeService.getDigihomeId()+" DIGIHOME_METHOD_REQUEST "+ digihomeMehthodRequestParameter.getName()+" already successed ");
                continue;
            }
            digihomeMethodRequestStatus = this.executeMethodRequest(digihomeMehthodParameters, digihomeMehthodRequestParameter);
            digihomeMehthodRequestParameter.setValue(Boolean.toString(digihomeMethodRequestStatus));
            if(digihomeMethodRequestStatus)
            {
                digihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.INSTALLED);
            }
        }
        
    }
    
    private boolean executeMethodRequest(List<DigihomeParameter> digihomeMehthodParameters, DigihomeParameter digihomeMehthodRequestParameter)
    {
        
        DigihomeParameter requestedDigihomeMehthodParameter = null;
        for(DigihomeParameter digihomeMehthodParameter : digihomeMehthodParameters)
        {
            if(digihomeMehthodParameter.getName().equals(digihomeMehthodRequestParameter.getName()))
            {
                requestedDigihomeMehthodParameter = digihomeMehthodParameter;
                break;
            }
        }
        
        boolean isInvokationSuccesed;
        try
        {
            String requestedDigihomeMehthodParameterValue = requestedDigihomeMehthodParameter.getValue();
            String requestedPortPath = DigihomeParametersUtil.getSCAParentPath(requestedDigihomeMehthodParameterValue);
            String requestedMethodName = DigihomeParametersUtil.getSCAPathValue(requestedDigihomeMehthodParameterValue);
            MultivaluedMap<String, String> invokationParams = new MetadataMap<String, String>();
            invokationParams.add("methodName", requestedMethodName);
            String invokationResponse = introspection.invokeMethod(requestedPortPath, invokationParams);
            isInvokationSuccesed = Boolean.valueOf(invokationResponse);
        }
        catch (Exception e)
        {
            isInvokationSuccesed = false;
        }
        return isInvokationSuccesed;
    }
    
    private void checkDigihomeMethodPropertyParameter(DigihomeParameter digihomeMethodParameter) throws DigihomeException
    {
        boolean digihomeMethodFound = false;
        try
        {
            String digihomeMethodParameterValue = digihomeMethodParameter.getValue();
            String digihomeDigihomeMethodParameterParentPath = DigihomeParametersUtil.getSCAParentPath(digihomeMethodParameterValue);
            DigihomeLogger.log(this, " digihomeDigihomeMethodParameterParentPath " + digihomeDigihomeMethodParameterParentPath);
            String digihomeDigihomeMethodParameterName = DigihomeParametersUtil.getSCAPathValue(digihomeMethodParameterValue);
            DigihomeLogger.log(this, " digihomeDigihomeMethodParameterName " + digihomeDigihomeMethodParameterName);
            Port digihomeMethodParentPort = introspection.getInterface(digihomeDigihomeMethodParameterParentPath);
            DigihomeLogger.log(this, digihomeMethodParentPort.toString());
            Interface digihomeMethodParentPortInterface = digihomeMethodParentPort.getImplementedInterface();
            for (Method digihomeMethodParentMethod : digihomeMethodParentPortInterface.getMethods())
            {
                if (digihomeMethodParentMethod.getName().equals(digihomeDigihomeMethodParameterName) && digihomeMethodParentMethod.getParameters().size() == 0 && digihomeMethodParentMethod.getResult() == "boolean")
                {
                    digihomeMethodFound = true;
                    break;
                }
            }
        } catch (Exception e)
        {
            digihomeMethodFound = false;
        }

        if (!digihomeMethodFound)
        {
            throw new DigihomeException("Parameter " + digihomeMethodParameter.getName() + ", no suitable method can be found for path  " + digihomeMethodParameter.getValue() + " path. DIGIHOME_METHOD signature must be boolean method()");
        }
    }
    
    private void checkDigihomeMethodRequestPropertyParameter(DigihomeService digihomeService, DigihomeParameter digihomeMethodRequestParameter) throws DigihomeException
    {
        String digihomeMethodRequestParameterName = digihomeMethodRequestParameter.getName();
        List<DigihomeParameter> digihomeMehthodParameters = DigihomeParametersUtil.getDigihomeParameter(digihomeService.getDigihomeParameters(), DigihomeParameterType.DIGIHOME_METHOD);
        
        boolean isMethodRequestFound = false;
        for(DigihomeParameter digihomeMehthodParameter : digihomeMehthodParameters)
        {
            if(digihomeMehthodParameter.getName().equals(digihomeMethodRequestParameterName))
            {
                isMethodRequestFound = true;
                break;
            }
        }
        
        if (!isMethodRequestFound)
        {
            throw new DigihomeException("Parameter " + digihomeMethodRequestParameter.getName() + ", no  " + digihomeMethodRequestParameterName + " DIGIHOME_METHOD parameter can be found");
        }
    }
}
