/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.service.registry.DigihomeServiceRegistryItf;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;

/**
 *
 */
@Scope("COMPOSITE")
public class DigihomeValidationProcessor extends AbstractDigihomeProcessor
{
    @Reference(name="digihomeServiceRegistry")
    private DigihomeServiceRegistryItf digihomeServiceRegistry;
    
    /***
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#
     * getProcessDigihomeServiceStatus()
     */
    public DigihomeServiceStatus getProcessDigihomeServiceStatus()
    {
        return DigihomeServiceStatus.INSTANTIATED;
    }

    @Override
    public List<DigihomeParameterType> getCheckedDigihomeParameterTypes()
    {
        List<DigihomeParameterType> toCheckDigihomeParameterTypes = super.getCheckedDigihomeParameterTypes();
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_CONTEXTUAL_PROPERTY);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_REFERENCE);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_SERVICE);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.DIGIHOME_METHOD);
        return toCheckDigihomeParameterTypes;
    }

    @Override
    public void checkDigihomeParameter(DigihomeParameter digihomeParameter) throws DigihomeException
    {
        DigihomeParametersUtil.mustBeSet(digihomeParameter);
    }
    
    /***
     * 
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#
     * processDigihomeService(org.ow2.frascati.digihome.service.DigihomeService)
     */
    public void processDigihomeService(DigihomeService digihomeService) throws DigihomeException
    {
        super.processDigihomeService(digihomeService);
        DigihomeLogger.logEntering(this, "processDigihomeService");
        
        DigihomeParameters digihomeParameters = digihomeService.getDigihomeParameters();
        DigihomeParameter digihomeNameParameter = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, "digihome-name");
        if (digihomeNameParameter != null)
        {
            DigihomeParametersUtil.mustBeSet(digihomeNameParameter);
            if (digihomeNameParameter.getValue().contains(" "))
            {
                //remove white space from name
                String digihomeNameParameterValue = digihomeNameParameter.getValue().replaceAll(" ", "_");
                digihomeNameParameter.setValue(digihomeNameParameterValue);
            }
            //create a new digihome-id based on the digihome-name and the previous contribution id
            String newDigihomeId = digihomeService.getDigihomeId()+"_"+digihomeNameParameter.getValue();
            digihomeService.setDigihomeId(newDigihomeId);
            DigihomeParameter digihomeIdParameter = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, "digihomeId");
            digihomeIdParameter.setValue(newDigihomeId);
        }

        //Add the digihomeService to the registry after setting its right name and id
        this.digihomeServiceRegistry.addDigihomeService(digihomeService);
        this.setDigihomeParameterContextualProperties(digihomeService.getDigihomeParameters());
        digihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.VALIDATED);
    }

    private void setDigihomeParameterContextualProperties(DigihomeParameters digihomeParameters) throws DigihomeException
    {
        Map<String, String> contextualProperties = new HashMap<String, String>();
        List<DigihomeParameter> toCheckDigihomeParameters = new ArrayList<DigihomeParameter>();
        for (DigihomeParameter digihomeParameter : digihomeParameters.getDigihomeParameters())
        {
            if (digihomeParameter.getType().equals(DigihomeParameterType.SCA_CONTEXTUAL_PROPERTY))
            {
                contextualProperties.put(digihomeParameter.getName(), digihomeParameter.getValue());
            } else
            {
                toCheckDigihomeParameters.add(digihomeParameter);
            }
        }

        for (DigihomeParameter toCheckDigihomeParameter : toCheckDigihomeParameters)
        {
            this.setDigihomeParameterContextualProperties(toCheckDigihomeParameter, contextualProperties);
        }
    }

    private void setDigihomeParameterContextualProperties(DigihomeParameter digihomeParameter, Map<String, String> contextualProperties)
    {
        if (DigihomeParametersUtil.containsContextualProperty(digihomeParameter.getName()))
        {
            String replacedDigihomeName = DigihomeParametersUtil.replaceContextualProperty(digihomeParameter.getName(), contextualProperties);
            DigihomeLogger.log(this, "set DigihomeName " + digihomeParameter.getName() + " : " + replacedDigihomeName);
            digihomeParameter.setName(replacedDigihomeName);
        }

        if (DigihomeParametersUtil.containsContextualProperty(digihomeParameter.getValue()))
        {
            String replacedDigihomeValue = DigihomeParametersUtil.replaceContextualProperty(digihomeParameter.getValue(), contextualProperties);
            DigihomeLogger.log(this, "set DigihomeValue " + digihomeParameter.getValue() + " : " + replacedDigihomeValue);
            digihomeParameter.setValue(replacedDigihomeValue);
        }
    }
}
