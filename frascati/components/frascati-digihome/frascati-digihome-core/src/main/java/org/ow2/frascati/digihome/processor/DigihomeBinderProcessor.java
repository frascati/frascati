/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.digihome.processor;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.digihome.DigihomeParameter;
import org.ow2.frascati.digihome.DigihomeParameterType;
import org.ow2.frascati.digihome.DigihomeParameters;
import org.ow2.frascati.digihome.DigihomeServiceStatus;
import org.ow2.frascati.digihome.core.logger.DigihomeLogger;
import org.ow2.frascati.digihome.exception.DigihomeException;
import org.ow2.frascati.digihome.service.DigihomePortMap;
import org.ow2.frascati.digihome.service.DigihomeService;
import org.ow2.frascati.digihome.service.registry.DigihomeServiceRegistryItf;
import org.ow2.frascati.digihome.util.DigihomeParametersUtil;
import org.ow2.frascati.digihome.util.IntrospectionUtil;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.resources.Port;
import org.ow2.frascati.remote.introspection.resources.Property;

@Scope("COMPOSITE")
public class DigihomeBinderProcessor extends AbstractDigihomeProcessor
{
    @Reference(name="introspection")
    private RemoteScaDomain introspection;
    
    @Reference(name="digihomeServiceRegistry")
    private DigihomeServiceRegistryItf digihomeServiceRegistry;
    
    /**
     * Map<serviceClass, List<servicePath>> multiple references
     */
    private DigihomePortMap multipleReferences;
    
    /**
     * Map<serviceClass, List<servicePath>> missing references, waiting to be bound
     */
    private DigihomePortMap missingReferences;
    
    public DigihomeBinderProcessor()
    {
        multipleReferences = new DigihomePortMap();
        missingReferences = new DigihomePortMap();
    }

    /***
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#getProcessDigihomeServiceStatus()
     */
    public DigihomeServiceStatus getProcessDigihomeServiceStatus()
    {
        return DigihomeServiceStatus.DEPLOYED;
    }
    
    @Override
    public List<DigihomeParameterType> getCheckedDigihomeParameterTypes()
    {
        List<DigihomeParameterType> toCheckDigihomeParameterTypes = super.getCheckedDigihomeParameterTypes();
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_REFERENCE);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_SERVICE);
        toCheckDigihomeParameterTypes.add(DigihomeParameterType.SCA_PROPERTY);
        return toCheckDigihomeParameterTypes;
    }

    @Override
    public void checkDigihomeParameter(DigihomeParameter digihomeParameter) throws DigihomeException
    {
        DigihomeParameterType digihomeParameterType = digihomeParameter.getType();
        if(digihomeParameterType.equals(DigihomeParameterType.SCA_SERVICE) || digihomeParameterType.equals(DigihomeParameterType.SCA_REFERENCE))
        {
            this.checkPortValueParameter(digihomeParameter);
        }
        else if(digihomeParameterType.equals(DigihomeParameterType.SCA_PROPERTY))
        {
            this.checkSCAPropertyParameter(digihomeParameter);
        }
    }
    
    /***
     * @throws DigihomeException 
     * @see org.ow2.frascati.digihome.processor.DigihomeProcessorItf#processDigihomeService(org.ow2.frascati.digihome.service.DigihomeService)
     */
    public void processDigihomeService(DigihomeService digihomeService) throws DigihomeException
    {
        DigihomeLogger.logEntering(this, "processDigihomeService");
        super.processDigihomeService(digihomeService);
        
        DigihomeParameters digihomeParameters = digihomeService.getDigihomeParameters();
        List<DigihomeParameter> digihomeSCAPropertyParameters = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, DigihomeParameterType.SCA_PROPERTY);
        for(DigihomeParameter digihomeSCAPropertyParameter : digihomeSCAPropertyParameters)
        {
            this.setSCAPropertyValue(digihomeSCAPropertyParameter);
        }
        
        List<String> digihomeServiceDeployedComposites = digihomeService.getDeployedComposites();
        boolean isContributionBounded = true;
        for(String deployedCompositeName : digihomeServiceDeployedComposites)
        {
            isContributionBounded &= bindComposite(digihomeService, deployedCompositeName); 
        }
        
        if(isContributionBounded)
        {
            digihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.BOUNDED);
        }
    }

    private void setSCAPropertyValue(DigihomeParameter digihomeSCAPropertyParameter)
    {
        String scaPropertyParameterName = digihomeSCAPropertyParameter.getName();
        if(scaPropertyParameterName == null || scaPropertyParameterName.equals(""))
        {
            return;
        }

        DigihomeLogger.log(this, "setSCAPropertyValue "+scaPropertyParameterName);
        String scaPropertyParentPath = DigihomeParametersUtil.getSCAParentPath(scaPropertyParameterName);
        String scaPropertyValue = DigihomeParametersUtil.getSCAPathValue(scaPropertyParameterName);
        String scaPropertyParameterValue = digihomeSCAPropertyParameter.getValue();
        introspection.setProperty(scaPropertyParentPath, scaPropertyValue, scaPropertyParameterValue);
        DigihomeLogger.log(this, " Set SCA property " + scaPropertyParentPath + "/" + scaPropertyValue + " : " + scaPropertyParameterValue);
    }
    
    /**
     * Bind all composite's references to available services
     * 
     * @param deployedCompositePath path of the deployed composite path
     * @return true if the composite is fully bounded
     * @throws ClassNotFoundException
     */
    private boolean bindComposite(DigihomeService digihomeService, String deployedCompositePath) throws DigihomeException
    {
        DigihomeLogger.logEntering(this, (new StringBuilder()).append("bindComposite ").append(deployedCompositePath).toString());
        Component composite = introspection.getComponent(deployedCompositePath);
        boolean isBoundedComposite = true;
        
        boolean isBoundedReference;
        Class<?> compositeReferenceClass;
        for(Port compositeReference : composite.getReferences())
        {
            compositeReferenceClass = this.digihomeServiceRegistry.getPortClass(compositeReference);
            isBoundedReference = bindCompositeReference(digihomeService, compositeReference);
            this.refreshPortData(compositeReference);
            if(IntrospectionUtil.isMultiplePort(compositeReference))
            {
                multipleReferences.put(compositeReferenceClass, compositeReference);
                digihomeService.addReference(compositeReferenceClass, compositeReference);
                DigihomeLogger.log(this, (new StringBuilder()).append("Add  ").append(compositeReference.getPath()).append(" to multiple references map").toString());
            }
            else if(!isBoundedReference)
            {
                isBoundedComposite = false;
                missingReferences.put(compositeReferenceClass, compositeReference);
                DigihomeLogger.log(this, "Add  "+compositeReference.getPath()+" to missing references map");
            }
            else
            {
                digihomeService.addReference(compositeReferenceClass, compositeReference);
                DigihomeLogger.log(this, "Add  "+compositeReference.getPath()+" to digihomeService references map");
            }
        }
        
        if(isBoundedComposite)
        {
            addComponentServices(digihomeService, composite);
        }
        else
        {
            DigihomeLogger.log(this, ("Composite "+composite.getName()+" is not fully bounded"));
        }
        return isBoundedComposite;
    }

    /**
     * Bind a composite reference to available services
     * 
     * @param reference composite reference
     * @return true if the composite reference is bounded
     * @throws ClassNotFoundException
     */
    private boolean bindCompositeReference(DigihomeService digihomeService, Port reference) throws DigihomeException
    {
        Class<?> referenceClass = this.digihomeServiceRegistry.getPortClass(reference);
        DigihomeLogger.logEntering(this, (new StringBuilder()).append("bindCompositeReference ").append(reference.getPath()).append(" (").append(referenceClass).append(")").toString());
        
        DigihomeParameters digihomeParameters = digihomeService.getDigihomeParameters();
        DigihomeParameter digihomeReferenceParameter = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, reference.getPath());
        
        List<Port> servicePorts = null;
        if(digihomeReferenceParameter!=null)
        {
            DigihomeLogger.log(this, "DigihomeReferenceParameter found for value : "+reference.getPath());
            servicePorts = new ArrayList<Port>();
            
            List<String> servicePortsPath = DigihomeParametersUtil.getDigihomePortParameterValues(digihomeReferenceParameter);
            for(String servicePortPath : servicePortsPath)
            {
                servicePorts.add(introspection.getInterface(servicePortPath));
            }
        }
        else
        {
            servicePorts = this.digihomeServiceRegistry.getAvailableServices(referenceClass);
        }

        if(IntrospectionUtil.isMultiplePort(reference))
        {
            for(Port servicePort : servicePorts)
            {
                bindPorts(servicePort, reference);
            }
            /**we consider a multiple reference is already fully bounded*/
            return true;
        }
        
        if(servicePorts.isEmpty())
        {
            DigihomeLogger.log(this, (new StringBuilder()).append("No service available for reference ").append(reference.getPath()).append("( ").append(referenceClass.getSimpleName()).append(" )").toString());
            return false;
        } else
        {
            return bindPorts(servicePorts.get(0), reference);
        }
    }

    /**
     * Bind service and ranference with SCA binding
     * 
     * @param servicePath path of the service to bind
     * @param referencePath reference of the service to bind
     * @return true if the binding is done successfully
     */
    private boolean bindPorts(Port service, Port reference)
    {
        DigihomeLogger.logEntering(this,"bindPorts servicePath : "+service.getPath()+", referencePath : "+reference.getPath());
        try
        {
            MultivaluedMap<String,String> bindingParams = new MetadataMap<String,String>();
            bindingParams.add("kind", "SCA");
            bindingParams.add("uri", service.getPath());
            introspection.addBinding(reference.getPath(), bindingParams);
            return true;
        }
        catch(Exception e)
        {
            DigihomeLogger.log(this, e.getClass().getSimpleName()+" when trying to bind "+service.getPath()+" and "+reference.getPath());
            e.printStackTrace();
            return false;
        }        
    }
    
    /**
     * Add all services of the DigihomeService

     * @param digihomeService
     * @throws DigihomeException
     */
    private void addComponentServices(DigihomeService digihomeService) throws DigihomeException
    {
        Component deployedComposite;
        for(String deployedCompositePath : digihomeService.getDeployedComposites())
        {
            deployedComposite = introspection.getComponent(deployedCompositePath);
            this.addComponentServices(digihomeService, deployedComposite);
        }
    }
    
    /**
     * Add all services of the composite to available services map
     * 
     * @param composite
     * @throws ClassNotFoundException
     */
    private void addComponentServices(DigihomeService digihomeService, Component composite)
        throws DigihomeException
    {
        for(Port compositeService : composite.getServices())
        {
            this.addComponentService(digihomeService, compositeService);
        }
    }

    /**
     * Add a composite service to available services map
     * 
     * @param compositeService
     * @throws DigihomeException
     */
    private void addComponentService(DigihomeService digihomeService, Port compositeService)
        throws DigihomeException
    {
        Class<?> serviceClass = this.digihomeServiceRegistry.getPortClass(compositeService);
        DigihomeLogger.logEntering(this, (new StringBuilder()).append("addComponentService componentService : ").append(compositeService.getName()).append(" (").append(serviceClass.getSimpleName()).append(")").toString(), new Object[0]);
        
        this.refreshPortData(compositeService);
        digihomeService.addService(serviceClass, compositeService);
        
        DigihomeParameters digihomeParameters = digihomeService.getDigihomeParameters();
        DigihomeParameter digihomeServiceParameter = DigihomeParametersUtil.getDigihomeParameter(digihomeParameters, compositeService.getPath());
        if(digihomeServiceParameter!=null)
        {
            DigihomeLogger.log(this, "DigihomeServiceParameter found for value : "+compositeService.getPath());
            List<String> referencesPath = DigihomeParametersUtil.getDigihomePortParameterValues(digihomeServiceParameter);
            Port reference;
            for(String referencePath : referencesPath)
            {
                reference = introspection.getInterface(referencePath);
                this.bindPorts(compositeService, reference);
                this.missingReferences.removeValue(reference);
            }
            return;
        }
        
        List<Port> missingReferences = this.missingReferences.get(serviceClass);
        if(!missingReferences.isEmpty())
        {
            DigihomeLogger.log(this, (new StringBuilder()).append("New service ").append(serviceClass).append(" is missing").toString());
            for(Port missingReference : missingReferences)
            {
                this.bindPorts(compositeService, missingReference);
            }
            this.missingReferences.remove(serviceClass);

            /**Deployed component that may be fully bounded now*/
            List<DigihomeService> notBoundedDigihomeServices = this.digihomeServiceRegistry.getDigihomeServices(DigihomeServiceStatus.DEPLOYED);
            for(DigihomeService notBoundedDigihomeService : notBoundedDigihomeServices)
            {
                if(this.isBoundedDigihomeService(notBoundedDigihomeService))
                {
                    notBoundedDigihomeService.setDigihomeServiceStatus(DigihomeServiceStatus.BOUNDED);
                    this.addComponentServices(notBoundedDigihomeService);
                    DigihomeLogger.log(this, (new StringBuilder()).append("Composite ").append(notBoundedDigihomeService.getDigihomeId()).append(" is now fully bounded").toString());
                }
            }
        }
        
        List<Port> relatedMultipleReferences = multipleReferences.get(serviceClass);
        for(Port relatedMultipleReference : relatedMultipleReferences)
        {
            bindPorts(compositeService, relatedMultipleReference);
        }
    }
    
    
    /******************** DigihomeParameter checking methods **************/
    private void checkPortValueParameter(DigihomeParameter portValueParameter) throws DigihomeException
    {
        List<String> portValues = DigihomeParametersUtil.getDigihomePortParameterValues(portValueParameter);
        for (String portPath : portValues)
        {
            try
            {
                introspection.getInterface(portPath);
            } catch (Exception e)
            {
                throw new DigihomeException("Parameter " + portValueParameter.getName() + ", no port can be found for  " + portPath + " path");
            }
        }
    }

    private void checkSCAPropertyParameter(DigihomeParameter digihomeSCAPropertyParameter) throws DigihomeException
    {
        boolean scaPropertyFound = false;
        try
        {
            String digihomeSCAPropertyParameterName = digihomeSCAPropertyParameter.getName();
            String scaPropertyParentPath = DigihomeParametersUtil.getSCAParentPath(digihomeSCAPropertyParameterName);
            String scaPropertyName = DigihomeParametersUtil.getSCAPathValue(digihomeSCAPropertyParameterName);
            Component scaPropertyParentComponent = introspection.getComponent(scaPropertyParentPath);
            for (Property scaComponentProperty : scaPropertyParentComponent.getProperties())
            {
                if (scaComponentProperty.getName().equals(scaPropertyName))
                {
                    scaPropertyFound = true;
                    break;
                }
            }
        } catch (Exception e)
        {
            scaPropertyFound = false;
        }

        if (!scaPropertyFound)
        {
            throw new DigihomeException("Parameter " + digihomeSCAPropertyParameter.getName() + ", no SCA property can be found for  " + digihomeSCAPropertyParameter.getValue() + " path");
        }
    }
    
    /************** Helper methods ***********/
    private boolean isBoundedDigihomeService(DigihomeService digihomeService)
    {
        Component deployedComposite;
        for(String deployedCompositePath : digihomeService.getDeployedComposites())
        {
            deployedComposite = introspection.getComponent(deployedCompositePath);
            if(!IntrospectionUtil.isBoundedComponent(deployedComposite))
            {
                return false;
            }
        }
        return true;
    }
    
    private Port refreshPortData(Port port)
    {
        return this.introspection.getInterface(port.getPath());
    }
}
