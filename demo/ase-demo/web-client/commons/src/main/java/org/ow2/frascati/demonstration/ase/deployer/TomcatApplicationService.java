package org.ow2.frascati.demonstration.ase.deployer;

import java.net.URI;
import java.net.URISyntaxException;

import org.osoa.sca.annotations.Service;

@Service
public interface TomcatApplicationService {

	public URI getWarLocation() throws URISyntaxException;
	
}
