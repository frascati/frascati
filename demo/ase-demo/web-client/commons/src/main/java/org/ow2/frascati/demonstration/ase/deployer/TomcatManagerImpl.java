package org.ow2.frascati.demonstration.ase.deployer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.util.HashMap;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.assembly.factory.api.ManagerException;

@Scope("COMPOSITE")
public class TomcatManagerImpl implements InterfaceManagerService {

	//@Reference(name="assembly-factory")
	CompositeManager compositeManager;
	
	@Property(name="tomcat-webapps")
	String webAppsDirectory;
	
	private HashMap<String, URI> wars = new HashMap<String, URI>();
	
	public void setCompositeManager(CompositeManager compositeManager){
		this.compositeManager = compositeManager;
	}
	
	public void deploy(String compositeName) {		
		try {			
			System.out.println("Deploying ... " + compositeName);
			
			// we start the component on the FraSCAti side			
			Component c = compositeManager.getComposite(compositeName);
						
			// we check out informations about the war interface
			URI location = getWarLocationFromTomcatAppComponent(c);
			System.out.println(location.toString());
			
			String fileName = (new File(location.toString())).getName();
			
			// finally, we deploy the war into our Tomcat webapps directory			
			if ( fileName.endsWith(".war") ){
							
				String newLocation = webAppsDirectory + File.separator + fileName;
				
				// copy war to webApps directory
				copy( location.toString(), newLocation );
				
				// remember this interface
				wars.put(compositeName, (new File(newLocation)).toURI() );				
			}
			
			System.out.println(compositeName + " successfully deployed on " + webAppsDirectory);
			
		} catch (ManagerException e) {
			e.printStackTrace();
		} catch (NoSuchInterfaceException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}		
	}

	public void undeploy(String compositeName) {		
		try {			
			// we remove the war from the Tomcat webapps directory (eg. undeploy interface)
			(new File(wars.get(compositeName))).delete();
			
			// forget this interface
			wars.remove(compositeName);
			
			// then we stop the component into our FraSCAti platform			
			compositeManager.removeComposite(compositeName);
			
		} catch (ManagerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	// TODO find a library in order to not include this method
	// which is extract from http://www.developpez.com
	private void copy(String fromPath, String toPath) {

		FileChannel in = null; 
		FileChannel out = null;
		 
		try {
		  in = new FileInputStream(fromPath).getChannel();
		  out = new FileOutputStream(toPath).getChannel();
		 
		  in.transferTo(0, in.size(), out);
		} catch (Exception e) {
		  e.printStackTrace();
		} finally {
		  if(in != null) {
		  	try {
			  in.close();
			} catch (IOException e) {}
		  }
		  if(out != null) {
		  	try {
			  out.close();
			} catch (IOException e) {}
		  }
		}
	}
	
	private URI getWarLocationFromTomcatAppComponent(Component c) throws NoSuchInterfaceException, URISyntaxException{
		// we retrieve the itf controller
		BindingController bc = (BindingController) c.getFcInterface("binding-controller");
		
		// then we extract the tomcat app service
		TomcatApplicationService tomcatApp = (TomcatApplicationService) bc.lookupFc("tomcat-service");
		
		// in order to retrieve the war location 
		return tomcatApp.getWarLocation();
	}

}
