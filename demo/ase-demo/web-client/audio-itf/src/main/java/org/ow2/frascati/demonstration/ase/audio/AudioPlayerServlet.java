package org.ow2.frascati.demonstration.ase.audio;

import java.io.IOException;
import java.net.URISyntaxException;
import java.rmi.Naming;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AudioPlayerServlet implements Servlet {

	/*
	 * Action keywords
	 */
	public static final String action = "action";
	public static final String getList = "getlist";
	public static final String add = "add";
	public static final String play = "play";
	public static final String up = "up";
	public static final String down = "down";
	
	/*
	 * Action parameters
	 */
	public static final String id = "id";
	public static final String location = "location";
	
	/*
	 * Attributes
	 */
	public static final String list = "list";
	
	/*
	 * Pages
	 */
	public static final String main = "AudioPlayerItf.jsp";
	
	private AudioPlayerItfService audioPlayerItfService;
	
	public void destroy() {
		// TODO Auto-generated method stub		
	}

	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String formatServletUrl(String action){
		//FIXME beurk
		return "/audio-itf-1.3-SNAPSHOT" + "/player?" + AudioPlayerServlet.action + "=" + action;
	}

	public void init(ServletConfig arg0) throws ServletException {		
		try {
			System.setSecurityManager(new java.rmi.RMISecurityManager());
			
			audioPlayerItfService = (AudioPlayerItfService)Naming.lookup("//localhost:1099/player-rmi-service");
		} catch (Exception e) {			
			throw new ServletException(e);			
		}				
	}

	public void service(ServletRequest request, ServletResponse response)
			throws ServletException, IOException {
		
		String action = request.getParameter(AudioPlayerServlet.action);
		
		if ( (action == null) || (action.equals("")) )
			action = AudioPlayerServlet.getList;
		
		
		if ( action.equals(AudioPlayerServlet.getList) ){
			request.getRequestDispatcher(AudioPlayerServlet.main).forward(request, response);
			return;
		}		
		
		if (action.equals(AudioPlayerServlet.up))
			audioPlayerItfService.increaseSoundLevel();
						
		if (action.equals(AudioPlayerServlet.down))
			audioPlayerItfService.decreaseSoundLevel(); 

		
		if ( action.equals(AudioPlayerServlet.play) ){
			
			String location = request.getParameter(AudioPlayerServlet.location);			
			try {				
				if ((location != null) && (!location.equals(""))) {
					audioPlayerItfService.playAudioLocation(location);
				}				
			} catch (NumberFormatException e) {
				throw new ServletException(e);
			} catch (URISyntaxException e) {
				throw new ServletException(e);
			}			
			
		}
		
		request.getRequestDispatcher("/player?" + AudioPlayerServlet.action + "=" + AudioPlayerServlet.getList).forward(request, response);
	}

}
