package org.ow2.frascati.demonstration.ase.audio;

import java.net.URI;
import java.net.URISyntaxException;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Property;
import org.ow2.frascati.demonstration.ase.deployer.TomcatApplicationService;

@Scope("COMPOSITE")
public class AudioPlayerItfRemoteImpl implements AudioPlayerItfService, TomcatApplicationService {	
	
	@Reference(name="audio-player-itf-reference", required=false)
	public AudioPlayerItfService audioPlayerItfService;
	
	@Property(name="war-path")
	String warPath;
	
	public void decreaseSoundLevel() {
		this.audioPlayerItfService.decreaseSoundLevel();
	}

	public int getSoundLevel() {
		return audioPlayerItfService.getSoundLevel();
	}

	public void increaseSoundLevel() {
		this.audioPlayerItfService.increaseSoundLevel();		
	}

	public void playAudioLocation(String location) throws URISyntaxException {
		this.audioPlayerItfService.playAudioLocation(location);
	}

	public URI getWarLocation() throws URISyntaxException {
		return new URI(warPath);
	}

}
