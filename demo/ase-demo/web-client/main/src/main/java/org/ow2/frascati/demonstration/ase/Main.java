package org.ow2.frascati.demonstration.ase;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.demonstration.ase.deployer.InterfaceManagerService;

public class Main {

	/**
	 * Hack main method in order to allow FraSCAti reconfiguration from inside
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		// first, we launch FraSCAti
		System.out.println("\n\n\nStarting the OW2 FraSCAti ASE Demonstration -- Tomcat based web client \n\n\n");

	    FraSCAti frascati = FraSCAti.newFraSCAti();

	    // then we create the interface manager
	    Component composite = frascati.getComposite("web-client");

	    CompositeManager cM = frascati.getCompositeManager();
	   
	    // finally we use it in order to create the audio interface
	    InterfaceManagerService itfManager = frascati.getService(composite, "itf-deployer", org.ow2.frascati.demonstration.ase.deployer.InterfaceManagerService.class);	    
	    
	    itfManager.setCompositeManager(cM);
	    itfManager.deploy("audio-itf");

	}

}
