package org.ow2.frascati.demonstration.ase.energy;

import java.io.IOException;
import java.rmi.Naming;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EnergySaverServlet implements Servlet {

	/*
	 * Actions
	 */
	public static final String action = "action";
	
	public static final String incBright = "inc";
	public static final String decBright = "dec";
	
	/*
	 * Pages
	 */
	public static final String jsp ="EnergySaverItf.jsp";
	
	
	private EnergySaverService energySaverService;
	
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String formatServletUrl(String action){
		//FIXME beurk
		return "/energy-itf-1.3-SNAPSHOT" + "/energy?" + EnergySaverServlet.action + "=" + action;
	}

	public void init(ServletConfig arg0) throws ServletException {
		try {
			energySaverService = (EnergySaverService)Naming.lookup("//localhost:1099/energy-rmi-service");
		} catch (Exception e) {			
			throw new ServletException(e);			
		}		
	}

	public void service(ServletRequest request, ServletResponse response)
			throws ServletException, IOException {
		
		String action = request.getParameter(EnergySaverServlet.action);
		
		if ( (action != null) ){
			if (action.equals(EnergySaverServlet.incBright))
				energySaverService.increaseBrightness();
				
			if (action.equals(EnergySaverServlet.decBright))
				energySaverService.reduceBrightness();			
		}
		
		request.getRequestDispatcher(EnergySaverServlet.jsp).forward(request, response);
	}

}
