package org.ow2.frascati.demonstration.ase.energy;

import java.net.URI;
import java.net.URISyntaxException;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;
import org.ow2.frascati.demonstration.ase.deployer.TomcatApplicationService;


public class EnergySaverItfRemoteImpl implements EnergySaverService, TomcatApplicationService {

	@Reference(name="energy-saver-itf-reference")
	EnergySaverService energySaverService;
	
	@Property(name="war-path")
	String warPath;
	
	public void increaseBrightness() {
		energySaverService.increaseBrightness();
	}

	public void increaseCPUSpeed() {
		// TODO not yet implemented
	}

	public void reduceBrightness() {
		energySaverService.reduceBrightness();
	}

	public void reduceCPUSpeed() {
		// TODO not yet implemented
	}

	public URI getWarLocation() throws URISyntaxException {
		return new URI(this.warPath);
	}
	
}
