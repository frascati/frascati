<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="org.ow2.frascati.demonstration.ase.energy.EnergySaverServlet"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.net.URI"%>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Energy saver</title>
	<style type="text/css">
		<!--
		html,body
		{
			margin-top:0;
			margin-right:1;
			margin-left:1;
			margin-bottom:0;
			font-size : 10pt;
			font-family:Arial,Helvetica,sans-serif;
			color:#5B7778;
			height: 100%;
			padding:0;
			background-color:#9CAA9C;
		}
	
		#frame{
			width:60%;
			margin-left: auto;
			margin-right: auto;
		}
	
		#header{
			padding:10px;
	
			border: 2pt dotted;
			margin:15px;
			background-color:#EFEFDE;
		}	
	
		#main{
			border: 2pt dotted;
			margin:15px;
			text-align: center;
			background-color:#EFEFDE;
		}
	
		.btn{
	
			width:25px;
			font-size : 25pt;
			color: #CEDFCE;
			background-color:#BD9A52;
		}
	
		a {
			text-decoration: none;
		}
		-->
	</style>
</head>
<body>

	<!-- Manage brightness level -->
	<%
		String up = EnergySaverServlet.formatServletUrl(EnergySaverServlet.incBright);
		String down = EnergySaverServlet.formatServletUrl(EnergySaverServlet.decBright);
	%>	

	<div id="frame">

		<div id="header">
			<h1>Energy Saving</h1>
		</div>

		<div id="main">
			<h3>Adjust screen brightness : </h3><br />
			<a href="<% out.println(up); %>"><span class="btn">&nbsp;+&nbsp;</span></a>
			&nbsp;&nbsp;
			<a href="<% out.println(down); %>"><span class="btn">&nbsp;-&nbsp;</span></a>

			<br /><br />
		</div>

	</div>
</body>
</html>