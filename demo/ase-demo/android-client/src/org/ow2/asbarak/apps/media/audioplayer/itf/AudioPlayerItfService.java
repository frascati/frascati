package org.ow2.asbarak.apps.media.audioplayer.itf;

import java.net.URISyntaxException;

public interface AudioPlayerItfService {
	
	// audio player features provided by interface	

	public void playAudioLocation(String location) throws URISyntaxException; 
		
	public void increaseSoundLevel();
	
	public void decreaseSoundLevel();
	
	public int getSoundLevel();
	
}
