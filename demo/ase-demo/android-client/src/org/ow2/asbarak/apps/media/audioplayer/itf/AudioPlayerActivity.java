package org.ow2.asbarak.apps.media.audioplayer.itf;

import java.net.URISyntaxException;

import org.ow2.binjiu.runtime.controlpoint.APIControlPoint;
import org.ow2.binjiu.runtime.controlpoint.GenericCyberlinkControlPointWrapper;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AudioPlayerActivity extends Activity implements AudioPlayerItfService {
	
	private AudioPlayerItfService audioPlayer;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        GenericCyberlinkControlPointWrapper wrapper = APIControlPoint.createUPnPControlPoint("AudioPlayerItfServiceType");	    
        audioPlayer = (AudioPlayerItfService) APIControlPoint.createGenericStub(AudioPlayerItfService.class, wrapper);    
	    
	    wrapper.start();
	    wrapper.search();
	    
	    // -- We let time for discovery connection
	    int i = 0;
	    while ( (i++ < 5) && (! wrapper.isConnected())){
		    try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			wrapper.search();
	    }

	    String log = wrapper.isConnected() ? "connection in " + i + " time" : "connection error";    
	    
	    ( (TextView) findViewById(R.id.log)).setText(log);
	    
	    // --
	    
        
        // -- Create listeners					
		OnClickListener upL = new View.OnClickListener() {
            public void onClick(View v) {
            	increaseSoundLevel();            	
            }
		};						
		((Button) findViewById(R.id.up)).setOnClickListener(upL);
		
		OnClickListener downL = new View.OnClickListener() {
            public void onClick(View v) {
            	decreaseSoundLevel();            	
            }
		};						
		((Button) findViewById(R.id.down)).setOnClickListener(downL);
		
		OnClickListener playL = new View.OnClickListener() {
            public void onClick(View v) {
            	String location = ( (EditText) findViewById(R.id.location)).getText().toString();
            	try {
					playAudioLocation(location);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}            	
            }
		};						
		((Button) findViewById(R.id.play_btn)).setOnClickListener(playL);		
		
		// --   
    }
    
	public void playAudioLocation(String location) throws URISyntaxException {
		audioPlayer.playAudioLocation(location);
	}
	
	public void decreaseSoundLevel() {
		audioPlayer.decreaseSoundLevel();
	}

	public int getSoundLevel() {
		return audioPlayer.getSoundLevel();
	}

	public void increaseSoundLevel() {
		audioPlayer.increaseSoundLevel();
	}
    
}