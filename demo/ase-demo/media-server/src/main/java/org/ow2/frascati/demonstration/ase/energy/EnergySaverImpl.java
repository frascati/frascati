package org.ow2.frascati.demonstration.ase.energy;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demonstration.ase.deployer.InterfaceManagerService;
import org.ow2.frascati.demonstration.ase.os.CommandProcessorService;

@Scope("COMPOSITE")
@EagerInit
public class EnergySaverImpl implements EnergySaverService {

	private static final String manageBrightness = "sudo brightness ";
	
	@Reference(name="os-command-service")
	CommandProcessorService commandProcessor;
	
	@Reference(name="interface-manager")
	InterfaceManagerService interfaceManager;
	
	@Init
	public void init() {
		System.out.println("Deploy energy-itf");
		interfaceManager.deploy("energy-itf");
	}
	
	public void increaseBrightness() {
		String cmd = manageBrightness + "plus"; 
		commandProcessor.process(cmd);
		
		return;
	}

	public void increaseCPUSpeed() {
		// TODO Auto-generated method stub
		
	}

	public void reduceBrightness() {
		String cmd = manageBrightness + "minus"; 
		commandProcessor.process(cmd);
		
		return;		
	}

	public void reduceCPUSpeed() {
		// TODO Auto-generated method stub
		
	}

}
