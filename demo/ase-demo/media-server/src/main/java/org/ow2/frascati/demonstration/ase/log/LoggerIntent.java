package org.ow2.frascati.demonstration.ase.log;

import org.ow2.frascati.tinfi.control.intent.IntentHandler;
import org.ow2.frascati.tinfi.control.intent.IntentJoinPoint;

public class LoggerIntent implements IntentHandler {

	public Object invoke(IntentJoinPoint ijp) throws Throwable {
		
		System.out.println( 
				"calling ... " + 
				ijp.getInterface().getFcItfName() + 
				" - " + 
				ijp.getMethod().getName() );
		
		return ijp.proceed();
	}

}
