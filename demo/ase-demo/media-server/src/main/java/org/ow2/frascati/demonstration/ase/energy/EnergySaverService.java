package org.ow2.frascati.demonstration.ase.energy;

import org.osoa.sca.annotations.Service;

@Service
public interface EnergySaverService {
	
	public void reduceBrightness();
	
	public void increaseBrightness();
	
	public void reduceCPUSpeed();
	
	public void increaseCPUSpeed();
}