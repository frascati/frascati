#!/bin/bash
CUR=`cat /proc/acpi/video/VID/LCD/brightness | grep 'current' | cut -f 2 -d ' '`
echo "current brightness level is : $CUR"

case "$1" in
plus)
    if [ "$CUR"  = 100 ]; then
        echo "brigthness is already max "
        exit 0
    fi
    NEW=`expr "$CUR" + 20`
    ;;
minus)
    if [ "$CUR"  = 0 ]; then
        echo "brigthness is already min "
        exit 0
    fi
    NEW=`expr "$CUR" - 20`
    ;;
*)
    echo "usage: $0 {plus|minus}"      
esac
echo "new brightness is: $NEW"
/bin/echo -n $NEW > /proc/acpi/video/VID/LCD/brightness
