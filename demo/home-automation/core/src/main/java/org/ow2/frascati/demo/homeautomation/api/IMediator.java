/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.api;

import java.util.List;

import com.google.gdata.data.calendar.CalendarEventEntry;

/**
 * Interface for the mediator component service.
 * 
 * @author Mahmoud Ben hassine
 * @version 1.0
 *
 */
public interface IMediator {

	/**
	 * add a {@link StaffUIService} to the current user interface using {@link GUIService}
	 * @param sus the {@link StaffUIService} to add
	 */
	public void addStaffUI(StaffUIService sus);
	
	/**
	 * remove a {@link StaffUIService} from the current user interface using {@link GUIService}
	 * @param sus the {@link StaffUIService} to remove
	 */
	public void removeStaffUI(StaffUIService sus);

	/**
	 * return the weather conditions using {@link WeatherService}
	 * @return weather conditions
	 */
	public String getWeatherData();
	
	/**
	 * get a visual map for the local position
	 * @return a visual map for the local position
	 */
	public javax.swing.JPanel getMapData();

	/**
	 * get available meetings from the primary calendar using {@link GcalService}
	 * @return available meetings
	 */
	public List<CalendarEventEntry> getAvailableMeetings();
	
	/**
	 * add a meeting to the primary calendar using {@link GcalService}
	 * @param meeting the meeting to add
	 */
	public void validateMeeting(CalendarEventEntry meeting);

	/**
	 * Switch ON a light using {@link KnxService} 
	 * @param lightId the light identifier 
	 */
	public void switchLightON(String lightId);

	/**
	 * Switch OFF a light using {@link KnxService} 
	 * @param lightId the light identifier 
	 */
	public void switchLightOFF(String lightId);
	
	/**
	 * Send a short message using {@link SkypeService}
	 * @param number the phone number to send the message to
	 * @param sms the short message to send
	 */
	public void sendSMS(String number,String sms);

	/**
	 * initialize services
	 */
	public void init();
	
	/**
	 * start the demo
	 */
	public void start();
	
	/*
	 * Getters for local data
	 */
	
	public String getPatientId();
	
	public String getPatientName();
	
	public String getDoctorPhoneNumber();
	
	public String getNursePhoneNumber();
	
	public String getCity();
	
	public String getCountry();
	
	public double getLatitude();
	
	public double getLongitude();
}
