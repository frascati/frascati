/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.util;

import javax.swing.UIManager;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

import org.ow2.frascati.demo.homeautomation.api.IMediator;
// import fr.inria.monitoring.db.InitDB;

public class Launcher {

	public static void main(String[] args) throws FrascatiException {

		//initialize monitoring framework DB 
		//InitDB.getInstance(ResourceBundle.getBundle("connection")).resetDB();
		
	    // Creates a new instance of OW2 FraSCAti.
		FraSCAti frascati = FraSCAti.newFraSCAti();
	    
	    // Creates a new SCA composite 
	    Component scaComposite = frascati.getComposite("home-automation");
	    
	    //start the demo
	    IMediator m = frascati.getService(scaComposite, "mediatorService", IMediator.class);

		//set the system l&f
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception ex) {
             ex.printStackTrace();
         }
        
         //start the demo
         m.init();
         m.start();

	}
}
