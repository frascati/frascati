/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.api;

/**
 * Interface allowing interaction with KNX devices
 * 
 * @author Mahmoud Ben hassine
 * @version 1.0
 *
 */
public interface KnxService {
	
	/**
	 * Getter for the current temperature
	 * @return the current temperature
	 */
	public double getTemperature();
	
	/**
	 * Switch ON the light with identifier lightId
	 * @param lightId the light identifier
	 */
	public void switchLightON(String lightId);
	
	/**
	 * Switch OFF the light with identifier lightId
	 * @param lightId the light identifier
	 */
	public void switchLightOFF(String lightId);
	
	/**
	 * initialize the service
	 */
	public void init();

}
