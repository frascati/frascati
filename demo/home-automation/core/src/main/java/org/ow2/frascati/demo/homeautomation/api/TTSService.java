/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.api;

import java.io.File;
import java.util.HashMap;

/**
 * Text to speech service
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
public interface TTSService {
	
	/**
	 * play the given mp3 file
	 * @param mp3file the file to play
	 * @throws Exception thrown if the file cannot be played
	 */
	public void playFile(File mp3file) throws Exception;
	
	/**
	 * Getter for available sounds
	 * @return sounds map
	 */
	public HashMap<String,File> getSounds();
	
	/**
	 * Initialize the TTS service
	 */
	public void init();

}
