/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.api;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.util.ServiceException;


/**
 * Google Service
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 */

public interface GcalService {

	//Calendar related services

	/**
	 * get events from the <b>primary</b> calendar
	 * @return the list of calendar entries
	 */
	List<CalendarEventEntry> getEvents() throws IOException,ServiceException;

	/**
	 * post a new entry to the <b>primary</b> calendar
	 * @param entry the entry to post
	 */
	void addEvent(CalendarEventEntry entry) throws IOException,ServiceException;
	
	/**
	 * delete the given calendar entry from the <b>primary</b> calendar
	 * @param entry the entry to delete
	 */
	void deleteEvent(CalendarEventEntry entry) throws IOException,ServiceException;
	
	/**
	 * schedule a task to be done at the start time of the given calendar entry
	 * @param entry the entry for which the task will be scheduled
	 * @param task the task to schedule
	 */
	void scheduleTask(CalendarEventEntry entry,TimerTask task);
	
	/**
	 * get available meetings from <b>primary</b> calendar for a date range
	 * @param date starting date
	 * @param field time unit
	 * @param number number of time units after the starting date
	 * @return a list of available meetings from the <b>primary</b> calendar
	 */
	public List<CalendarEventEntry> getAvailableMeetings(Date date,int field,int number);

	/**
	 * initialize the service
	 */
	public void init();

}
