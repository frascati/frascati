/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.lib;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.ws.WebServiceException;
import org.osoa.sca.ServiceUnavailableException;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

import com.google.gdata.data.PlainTextConstruct;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.util.ServiceException;

import org.ow2.frascati.demo.homeautomation.api.CommunicationService;
import org.ow2.frascati.demo.homeautomation.api.GUIService;
import org.ow2.frascati.demo.homeautomation.api.GcalService;
import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.KnxService;
import org.ow2.frascati.demo.homeautomation.api.MapService;
import org.ow2.frascati.demo.homeautomation.api.RFIDService;
import org.ow2.frascati.demo.homeautomation.api.StaffUIService;
import org.ow2.frascati.demo.homeautomation.api.SunSpotService;
import org.ow2.frascati.demo.homeautomation.api.TTSService;
import org.ow2.frascati.demo.homeautomation.api.WeatherService;

/**
 * 
 * Implementation of {@link IMediator} interface. This component plays the role of a mediator in the 'mediator design pattern' and is used to coordinate other services according to a higher level business logic
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
@Scope("COMPOSITE")
public class Mediator implements IMediator {

	@Reference
	private WeatherService weatherService;

	@Reference
	private GcalService calendarService;

	@Reference
	private GUIService guiService;

	@Reference
	private TTSService ttsService;

	@Reference
	private CommunicationService communicationService;

	@Reference
	private KnxService knxService;
	
	@Reference
	private RFIDService rfidService;
	
	@Reference
	private SunSpotService sunspotService;
	
	@Reference
	private MapService mapService;

	/*
	 * Local data properties
	 */
	@Property
	private String patientId;
	
	@Property
	private String patientName;
	
	@Property
	private String doctorPhoneNumber;
	
	@Property
	private String nursePhoneNumber;
	
	@Property
	private String city;
	
	@Property
	private String country;
	
	@Property
	private double latitude;
	
	@Property
	private double longitude;
	
	/*
	 * public methods to call services
	 */

	//TODO use @org.osoa.sca.annotations.Init
	public void init(){
		rfidService.init();
		sunspotService.init();
		calendarService.init();
		knxService.init();
		ttsService.init();
	}
	
	public void addStaffUI(StaffUIService sus) {
		guiService.registerStaffUiService(sus);
	}
	
	public void removeStaffUI(StaffUIService sus) {
		guiService.unregisterStaffUiService(sus);
	}

	public String getWeatherData() {
		String weatherData = null;
		try{
			// call the weather WS
			weatherData = weatherService.getWeather(city,country).substring(40);
		} catch (WebServiceException e) {
			Logger.getLogger(Mediator.class.getName()).log(Level.WARNING,"[ Mediator ] web service exception for weather service");
			return null;
		} catch(ServiceUnavailableException e){
			Logger.getLogger(Mediator.class.getName()).log(Level.WARNING," [ Mediator ] timeout exception for weather service");
			return null;
		}
		return weatherData;
	}

	public javax.swing.JPanel getMapData(){
		return mapService.getMap(latitude, longitude);
	}
	
	public List<CalendarEventEntry> getAvailableMeetings() {
		//get available meetings for the next week
		return calendarService.getAvailableMeetings(Calendar.getInstance().getTime(),Calendar.DAY_OF_MONTH,7);
	}

	public void start() {
		guiService.showGUI();
	}

	public void switchLightON(String lightId) {
		try {
			ttsService.playFile(ttsService.getSounds().get(lightId + "-on"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		knxService.switchLightON(lightId);
	}

	public void switchLightOFF(String lightId) {
		try {
			ttsService.playFile(ttsService.getSounds().get(lightId + "-off"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		knxService.switchLightOFF(lightId);
	}
	
	public void sendSMS(String number,String sms){
		communicationService.sendSMS(number, sms);
	}

	public void validateMeeting(CalendarEventEntry meeting) {
        
		//update meeting title
		String meetingTitle = "RDV patient : " + patientName;
		meeting.setTitle(new PlainTextConstruct(meetingTitle));
		
		//add the meeting to the calendar
		try {
			calendarService.addEvent(meeting);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		//send confirmation sms to the doctor
		String startTime = meeting.getTimes().get(0).getStartTime().toUiString();
		sendSMS(doctorPhoneNumber, meetingTitle + " at " + startTime + "added to your agenda");
		
	}

	
	
	/*
	 * Getters and setters for local data properties
	 */
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getDoctorPhoneNumber() {
		return doctorPhoneNumber;
	}

	public void setDoctorPhoneNumber(String doctorPhoneNumber) {
		this.doctorPhoneNumber = doctorPhoneNumber;
	}

	public String getNursePhoneNumber() {
		return nursePhoneNumber;
	}

	public void setNursePhoneNumber(String nursePhoneNumber) {
		this.nursePhoneNumber = nursePhoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	
}
