/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.rfid;

//import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.RFIDService;
import org.ow2.frascati.demo.homeautomation.api.StaffUIService;
import org.ow2.frascati.demo.homeautomation.rfid.MirrorLibrary;

/**
 * Implementation of the {@link RFIDService} interface
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
@Scope("COMPOSITE")
public class RFIDServiceImpl implements RFIDService, MirrorCallBack {

	@Reference
	protected IMediator mediator;
	
	/*
	 * Should use a collection to map tags to user interface services
	 * Frascati : collection injection pb
	 */
	//@Property 
	//Map<Integer,StaffUIService> staffs;
	
	@Property
	private int expertTag;
	
	@Property
	private int nurseTag;
	
	@Reference
	protected StaffUIService expertUIService;
	
	@Reference
	protected StaffUIService nurseUIService;

    @Reference
    protected MirrorLibrary mirware;


	public void init(){
		RFIDThread t = new RFIDThread(this, mirware.getDevice());
		t.start();
		Logger.getLogger(RFIDServiceImpl.class.getName()).log(Level.INFO,"[RFID service] RFID service initialized successfully");
	}
	
    public void callback(int position,int tag) {
        switch (position) {
        case 0:
            System.err.println("Mirror UP");
            break;
        case 1:
            System.err.println("Mirror DOWN");
            break;
        case 2:
            // FIXME use the map to remove this ugly test
            //mediator.addStaffUI(staffs.get(tag));
            if( tag == expertTag)
                mediator.addStaffUI(expertUIService);
            else if (tag == nurseTag)
                mediator.addStaffUI(nurseUIService);
            break;
        case 3:
            // FIXME use the map to remove this ugly test
            //mediator.addStaffUI(staffs.get(tag));
            if( tag == expertTag)
                mediator.removeStaffUI(expertUIService);
            else if (tag == nurseTag)
                mediator.removeStaffUI(nurseUIService);
            break;
        default:
            break;
        }
    }

	class RFIDThread extends Thread{
		
		private RFIDService rfid;
		private String device;
		
		public RFIDThread(RFIDService rfid, String device) {
			this.rfid = rfid;
			this.device = device;
		}
		
		@Override
		public void run() {
			 mirware.register_NewEvent(rfid);
			 mirware.init(device);
		}
	}

}
