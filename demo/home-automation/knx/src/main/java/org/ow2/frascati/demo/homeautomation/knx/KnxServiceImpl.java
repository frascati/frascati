/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.knx;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;

import tuwien.auto.calimero.GroupAddress;
import tuwien.auto.calimero.exception.KNXException;
import tuwien.auto.calimero.exception.KNXFormatException;
import tuwien.auto.calimero.exception.KNXTimeoutException;
import tuwien.auto.calimero.knxnetip.KNXnetIPConnection;
import tuwien.auto.calimero.link.KNXLinkClosedException;
import tuwien.auto.calimero.link.KNXNetworkLinkIP;
import tuwien.auto.calimero.link.medium.TPSettings;
import tuwien.auto.calimero.process.ProcessCommunicator;
import tuwien.auto.calimero.process.ProcessCommunicatorImpl;

import org.ow2.frascati.demo.homeautomation.api.KnxService;

/**
 * Implementation of the {@link KnxService} interface
 * 
 * @version 1.0
 * @author Mahmoud Ben Hassine
 *
 */
public class KnxServiceImpl implements KnxService{

	@Property
	private String hostIpAddress;
	
	@Property
	private String knxIpAddress;
	
	@Property
	private String temperatureSensorAddress;
	
	private KNXNetworkLinkIP netLinkIp;
	
	private ProcessCommunicator pc;
	
	private GroupAddress temperatureGA;
	
	@Property
	private Map<String,String> lights;
	
	public void init(){
		try{
		InetSocketAddress hostSocketAddress = new InetSocketAddress(InetAddress.getByName(hostIpAddress), 0);
		InetSocketAddress knxSocketAddress = new InetSocketAddress(InetAddress.getByName(knxIpAddress),KNXnetIPConnection.IP_PORT);
		TPSettings tpSettings = new TPSettings(false);	// twisted pair settings	
		netLinkIp = new KNXNetworkLinkIP(KNXNetworkLinkIP.TUNNEL, hostSocketAddress, knxSocketAddress, false, tpSettings);
		pc = new ProcessCommunicatorImpl(netLinkIp);
		temperatureGA = new GroupAddress(temperatureSensorAddress);
		// FIXME collection injection with frascati
		lights = new HashMap<String,String>();
		lights.put("cuisine", "1/0/0");
		lights.put("salon", "1/0/1");
		lights.put("chambre", "1/0/2");
		lights.put("sdb", "1/0/3");
		Logger.getLogger(KnxServiceImpl.class.getName()).log(Level.INFO," [ KNX Service ] connection to KNX devices established successfully");
		}catch(Exception e){
			Logger.getLogger(KnxServiceImpl.class.getName()).log(Level.INFO," [ KNX Service ] connection to KNX devices failed");
		}
		
	}

	@Override
	public double getTemperature() {
		float temperature = 0;
		try {
			temperature = pc.readFloat(temperatureGA);
		}catch (KNXException e) {
			e.printStackTrace();
		}finally {
			closeKnxConnection();
		}
		return temperature;
	}

	@Override
	public void switchLightOFF(String lightId) {
		try {
			pc.write(new GroupAddress(lights.get(lightId)), false);
		} catch (KNXTimeoutException e) {
			e.printStackTrace();
		} catch (KNXLinkClosedException e) {
			e.printStackTrace();
		} catch (KNXFormatException e) {
			e.printStackTrace();
		}finally {
			closeKnxConnection();
		}
	}

	@Override
	public void switchLightON(String lightId) {
		try {
			pc.write(new GroupAddress(lights.get(lightId)), true);
		} catch (KNXTimeoutException e) {
			e.printStackTrace();
		} catch (KNXLinkClosedException e) {
			e.printStackTrace();
		} catch (KNXFormatException e) {
			e.printStackTrace();
		}finally {
			closeKnxConnection();
		}
	}

	private void closeKnxConnection() {
		if (netLinkIp != null) {
			netLinkIp.close();
		}
	}

	/**
	 * Setter for the temperature sensor address
	 * @param temperatureSensorAddress the temperature sensor address to set
	 */
	public void setTemperatureSensorAddress(String temperatureSensorAddress) {
		this.temperatureSensorAddress = temperatureSensorAddress;
	}

	/**
	 * Setter for the host IP address
	 */
	public void setHostIpAddress(String hostIpAddress) {
		this.hostIpAddress = hostIpAddress;
	}

	/**
	 * setter for the KNX devices IP address
	 */
	public void setKnxIpAddress(String knxIpAddress) {
		this.knxIpAddress = knxIpAddress;
	}

	/**
	 * setter for lights map
	 */
	public void setLights(Map<String, String> lights) {
		this.lights = lights;
	}
	
}
