/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Philippe Merle, Mahmoud Ben Hassine
 *
 * Contributor(s): Christophe Demarey
 */

package org.ow2.frascati.demo.homeautomation.comm;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.demo.homeautomation.api.CommunicationService;

/**
 * Implementation of
 * {@link org.ow2.frascati.demo.homeautomation.api.CommunicationService} interface
 *
 * 
 */
public class OrangeServiceImpl implements CommunicationService {

	/** Reference to the Orange service. */
	@Reference(name = "orange")
	private OrangeAPI orange;

	/** Property to configure the 'api-key' to use. */
	@Property(name = "api-key")
	private String apiKey;

	/** Property to configure the 'from' to use. */
	@Property(name = "from")
	private String from;

	public OrangeServiceImpl() {
	}

	@Override
	public void sendSMS(String number, String sms) {
		orange.sendSMS(this.apiKey, this.from, number, sms);
		Logger.getLogger(OrangeServiceImpl.class.getName()).log(Level.INFO," [Communication service] sms '" + sms + "' sent to " + number + " using Orange API");
	}

}