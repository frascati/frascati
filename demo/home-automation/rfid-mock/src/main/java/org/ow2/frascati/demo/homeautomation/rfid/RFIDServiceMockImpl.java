/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.rfid;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.KeyEvent;
//import java.awt.event.KeyListener;
import java.util.logging.Level;
import java.util.logging.Logger;

//import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.RFIDService;
import org.ow2.frascati.demo.homeautomation.api.StaffUIService;

/**
 * Mock implementation of the {@link RFIDService} interface
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
@Scope("COMPOSITE")
public class RFIDServiceMockImpl implements RFIDService {
	
	@Reference
	private IMediator mediator;
	
	/*
	 * Should use a collection to map RFID tags to user interface services
	 */
	
	@Reference
	private StaffUIService expertUIService;
	
	@Reference
	private StaffUIService nurseUIService;

	public void callback(int position,int tag) {
	}
	
	public RFIDServiceMockImpl() {
	}
	
	public void init(){
		nurseButton = new JToggleButton("Nurse");
		expertButton = new JToggleButton("Expert");
		RFIDActionListener rfidActionListener = new RFIDActionListener();
		nurseButton.addActionListener(rfidActionListener);
		nurseButton.setActionCommand("nurse");
		expertButton.addActionListener(rfidActionListener);
		expertButton.setActionCommand("expert");
		JFrame f = new JFrame("RFID service emulator");
		f.getRootPane().setLayout(new FlowLayout());
		f.getRootPane().add(nurseButton);
		f.getRootPane().add(expertButton);
		f.pack();
		f.setAlwaysOnTop(true);
		f.setVisible(true);
		Logger.getLogger(RFIDServiceMockImpl.class.getName()).log(Level.INFO," [RFID mock service] RFID mock service initialized successfully");
	}
	

	private JToggleButton nurseButton;
	private JToggleButton expertButton;

	private class RFIDActionListener implements ActionListener{

		// should use the map to remove this ugly test
		@Override
		public void actionPerformed(ActionEvent e) {
			if("nurse".equalsIgnoreCase(e.getActionCommand()))
			{ if(nurseButton.isSelected())
				mediator.addStaffUI(nurseUIService);
				else mediator.removeStaffUI(nurseUIService);
			}
				
			else if("expert".equalsIgnoreCase(e.getActionCommand())){
				if(expertButton.isSelected())
					mediator.addStaffUI(expertUIService);
					else mediator.removeStaffUI(expertUIService);
			}
		}
	}
	
	/*
	 * Idea : use a combination of key stroke to simulate external staff RFID events
	 * Pb : KeyEvent is triggered only by gui components that must be visible and focusable
	
	public void init(){
		hiddenButton = new JButton();
		rfidKeyListener = new RFIDKeyListener();
		hiddenButton.addKeyListener(rfidKeyListener);
		Logger.getLogger(RFIDServiceMockImpl.class.getName()).log(Level.INFO," [RFID mock service] RFID mock service initialized successfully");
	}
	
	private JButton hiddenButton;
	private RFIDKeyListener rfidKeyListener;
	
	private class RFIDKeyListener implements KeyListener{
		
		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyChar() == 'n')
				mediator.addStaffUI(nurseUIService);
			else if(e.getKeyChar() == 'e')
				mediator.addStaffUI(expertUIService);
			else {
				mediator.removeStaffUI(expertUIService);
				mediator.removeStaffUI(nurseUIService);
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

		@Override
		public void keyTyped(KeyEvent e) {		
		}
		
	} */

}
