/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.nurse;

import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.google.gdata.data.calendar.CalendarEventEntry;

import org.ow2.frascati.demo.homeautomation.api.IMediator;

public class RDVFrame extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

    public RDVFrame(IMediator mediator,List<CalendarEventEntry> meetings) {
    	this.mediator = mediator;
    	this.meetings = meetings;
        try {
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        DefaultListModel meetingslistModel = new DefaultListModel();
        for (CalendarEventEntry meeting : meetings)
        	meetingslistModel.addElement(meeting.getId());

        initComponents(meetingslistModel);
        setLocationRelativeTo(null);
    }

    private void initComponents(DefaultListModel meetingslistModel) {

        validerButton = new javax.swing.JButton();
        annulerButton = new javax.swing.JButton();
        dispoScrollPane = new javax.swing.JScrollPane();
        disponibilitesList = new javax.swing.JList();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("RDV disponibles");
        setResizable(false);

        validerButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/add.png")));
        validerButton.setFocusable(false);
        validerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validerButtonActionPerformed(evt);
            }
        });

        annulerButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/close.png")));
        annulerButton.setFocusable(false);
        annulerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerButtonActionPerformed(evt);
            }
        });

        disponibilitesList.setBorder(javax.swing.BorderFactory.createTitledBorder("Disponibilites"));
        disponibilitesList.setModel(meetingslistModel);
        disponibilitesList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        dispoScrollPane.setViewportView(disponibilitesList);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(98, Short.MAX_VALUE)
                .addComponent(validerButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(annulerButton)
                .addGap(102, 102, 102))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dispoScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dispoScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(validerButton, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                    .addComponent(annulerButton))
                .addContainerGap())
        );

        pack();
    }

    private void validerButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	int selectedMeetingIndex = disponibilitesList.getSelectedIndex();
    	if(selectedMeetingIndex == -1)
    		JOptionPane.showMessageDialog(this, " Veuillez selectionner un RDV","Erreur",JOptionPane.ERROR_MESSAGE);
    	else{
    	mediator.validateMeeting(meetings.get(selectedMeetingIndex));
    	dispose();
    	}
    }

    private void annulerButtonActionPerformed(java.awt.event.ActionEvent evt) {
        dispose();
    }
    
    private javax.swing.JButton annulerButton;
    private javax.swing.JScrollPane dispoScrollPane;
    private javax.swing.JList disponibilitesList;
    private javax.swing.JButton validerButton;
    private List<CalendarEventEntry> meetings;
    private IMediator mediator;

}
