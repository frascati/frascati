/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.home;


/**
*
* @author Grégory Nain
* @contact gregory.nain@irisa.fr
* 
*/
@SuppressWarnings(value = { "all" })
public class ShutterWidget extends javax.swing.JPanel {

    /** Creates new form ShutterWidget */
    public ShutterWidget() {
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        shutter1 = new Shutter();
        jPanel3 = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jButton1 = new javax.swing.JButton();
        jToggleButton3 = new javax.swing.JToggleButton();

        setMinimumSize(new java.awt.Dimension(300, 464));
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(300, 550));
        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.PAGE_AXIS));

        jPanel2.setMinimumSize(new java.awt.Dimension(78, 40));
        jPanel2.setName("jPanel2"); // NOI18N
        jPanel2.setLayout(new java.awt.GridLayout(1, 1, 20, 20));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("jLabel1");
        jLabel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4));
        jLabel1.setMaximumSize(new java.awt.Dimension(78, 40));
        jLabel1.setMinimumSize(new java.awt.Dimension(78, 40));
        jLabel1.setName("jLabel1"); // NOI18N
        jLabel1.setOpaque(true);
        jLabel1.setPreferredSize(new java.awt.Dimension(78, 40));
        jPanel2.add(jLabel1);

        add(jPanel2);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4));
        jPanel1.setName("jPanel1"); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(300, 100));
        jPanel1.setLayout(new java.awt.BorderLayout());

        shutter1.setName("shutter1"); // NOI18N
        shutter1.setPreferredSize(new java.awt.Dimension(190, 300));
        jPanel1.add(shutter1, java.awt.BorderLayout.WEST);

        jPanel3.setName("jPanel3"); // NOI18N
        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(100, 320));
        jPanel3.setLayout(new java.awt.GridLayout(3, 0, 0, 25));

        buttonGroup1.add(jToggleButton1);
        jToggleButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/upArrow.png"))); // NOI18N
        jToggleButton1.setText("OUVRIR");
        jToggleButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jToggleButton1.setContentAreaFilled(false);
        jToggleButton1.setFocusPainted(false);
        jToggleButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jToggleButton1.setName("jToggleButton1"); // NOI18N
        jToggleButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                upBt_MouseReleased(evt);
            }
        });
        jPanel3.add(jToggleButton1);

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/square.png"))); // NOI18N
        jButton1.setText("ARRETER");
        jButton1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        buttonGroup1.add(jButton1);
        jButton1.setContentAreaFilled(false);
        jButton1.setFocusPainted(false);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setName("jButton1"); // NOI18N
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                stopbt_MouseReleased(evt);
            }
        });
        jPanel3.add(jButton1);

        buttonGroup1.add(jToggleButton3);
        jToggleButton3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jToggleButton3.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/downArrow.png"))); // NOI18N
        jToggleButton3.setText("FERMER");
        jToggleButton3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jToggleButton3.setContentAreaFilled(false);
        jToggleButton3.setFocusPainted(false);
        jToggleButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jToggleButton3.setName("jToggleButton3"); // NOI18N
        jToggleButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToggleButton3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                downbt_MouseReleased(evt);
            }
        });
        jPanel3.add(jToggleButton3);

        jPanel1.add(jPanel3, java.awt.BorderLayout.EAST);

        add(jPanel1);
    }// </editor-fold>//GEN-END:initComponents

    private void upBt_MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_upBt_MouseReleased
        // TODO add your handling code here:
        shutter1.up();
    }//GEN-LAST:event_upBt_MouseReleased

    private void stopbt_MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stopbt_MouseReleased
        // TODO add your handling code here:
        shutter1.stop();
    }//GEN-LAST:event_stopbt_MouseReleased

    private void downbt_MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_downbt_MouseReleased
        // TODO add your handling code here:
        shutter1.down();
    }//GEN-LAST:event_downbt_MouseReleased

    public void setLabel(String text) {
        jLabel1.setText(text.toUpperCase());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton3;
    private Shutter shutter1;
    // End of variables declaration//GEN-END:variables

}
