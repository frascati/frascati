/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.home;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JSlider;
import javax.swing.plaf.basic.BasicSliderUI;

/**
*
* @author Grégory Nain
* @contact gregory.nain@irisa.fr
* 
*/
@SuppressWarnings(value = { "all" })
public class TempSlider extends JSlider {

    public TempSlider() {
        super();
        setBounds(getX(), getY(), getWidth(), getHeight());

        setUI(new ColoredThumbSliderUI(this));
        //setOpaque(false);

        MouseListener[] listeners = getMouseListeners();
        for (MouseListener l : listeners) {
            removeMouseListener(l); // remove UI-installed TrackListener
        }
        final BasicSliderUI ui = (BasicSliderUI) getUI();
        BasicSliderUI.TrackListener tl = ui.new TrackListener() {
            // this is where we jump to absolute value of click

            @Override
            public void mouseClicked(MouseEvent e) {
                Point p = e.getPoint();
                int value = ui.valueForYPosition(p.y);

                setValue(value);
            }
            // disable check that will invoke scrollDueToClickInTrack

            @Override
            public boolean shouldScroll(int dir) {
                return false;
            }
        };
        addMouseListener(tl);

    }

    public class ColoredThumbSliderUI extends BasicSliderUI {

        int realHeight = 200;
        int realY = 0;
        int buttonHeight = 200;
        int nbButton = 7;

        ColoredThumbSliderUI(JSlider s) {
            super(s);
        }

        protected void scrollDueToClickInTrack(int direction) {
            // this is the default behaviour, let's comment that out
            //scrollByBlock(direction);

            int value = slider.getValue();

            if (slider.getOrientation() == JSlider.HORIZONTAL) {
                value = this.valueForXPosition(slider.getMousePosition().x);
            } else if (slider.getOrientation() == JSlider.VERTICAL) {
                value = this.valueForYPosition(slider.getMousePosition().y);
            }
            slider.setValue(value);
        }

        public void paintTrack(Graphics g) {

            Graphics2D g2d = (Graphics2D) g;
            Color backUp = g2d.getColor();

            buttonHeight = trackRect.height / nbButton;
            realHeight = buttonHeight * (nbButton - 1);
            realY = trackRect.y + (buttonHeight / 2);

            /*g2d.drawRect(
            trackRect.x + trackRect.width,
            trackRect.y,
            trackRect.x + trackRect.width,
            trackRect.height);*/
            g2d.setColor(Color.red);
            g2d.fillArc(
                    trackRect.x - 10,
                    realY + realHeight,
                    trackRect.width + 20,
                    trackRect.width + 20,
                    100,
                    340);

            g2d.setColor(Color.black);
            g2d.setStroke(new BasicStroke(2));
            g2d.drawLine(trackRect.x, realY, trackRect.x, realY + realHeight);
            g2d.drawLine(trackRect.x + trackRect.width,
                    realY,
                    trackRect.x + trackRect.width,
                    realY + realHeight);
            g2d.drawArc(
                    trackRect.x,
                    realY - 10,
                    20,
                    20,
                    0,
                    180);
            g2d.drawArc(
                    trackRect.x - 10,
                    realY + realHeight,
                    trackRect.width + 20,
                    trackRect.width + 20,
                    100,
                    340);
            g2d.setColor(backUp);
        }

        public void paintTicks(Graphics g) {
            //super.paintTicks(g);

            Color backUp = g.getColor();
            g.setColor(Color.black);

            for (int i = 0; i < nbButton; i++) {
                g.drawLine(
                        trackRect.x + trackRect.width,
                        realY + (buttonHeight * i),
                        trackRect.x + trackRect.width + 10,
                        realY + (buttonHeight * i));
            }
            
            g.setColor(backUp);
        }

        public void paintLabels(Graphics g) {
            super.paintLabels(g);
        }

        public void paintThumb(Graphics g) {
            //super.paintThumb(g);
            Graphics2D g2d = (Graphics2D) g;
            Color backUp = g2d.getColor();
            g2d.setColor(Color.RED);

            int maxValue = getMaximum();
            int btPlace = maxValue - getValue();

            int Y_location = realY + (btPlace * buttonHeight);


            g2d.fillRect(trackRect.x + 1, Y_location, trackRect.width - 2, labelRect.height - Y_location);
            //g2d.fillOval(trackRect.x, Y_location - 6, trackRect.width - 2, 12);
            g2d.fillArc(
                    trackRect.x,
                    Y_location - 10,
                    20,
                    20,
                    0,
                    180);
            //super.paintTrack(g2d);
            g2d.setColor(backUp);

        /*
        Graphics2D g2d = (Graphics2D) g;
        Color backUp = g2d.getColor();
        g2d.setColor(Color.WHITE);
        g2d.setStroke(new BasicStroke(5));
        g2d.draw(new Rectangle(thumbRect.x - 10, thumbRect.y - 10, thumbRect.width + 50, thumbRect.height + 20));
        g2d.setColor(backUp);
         */
        }
    }

    public void paint(Graphics g) {
        /*
        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(new GradientPaint(0, 0, Color.RED, getX() + 1, getHeight(), Color.BLUE, false));
        Rectangle rec = getBounds();
        g2d.fillRect((int) rec.getX(), (int) rec.getY(), (int) rec.getWidth(), (int) rec.getHeight());
         */
        super.paint(g);

    }
}
