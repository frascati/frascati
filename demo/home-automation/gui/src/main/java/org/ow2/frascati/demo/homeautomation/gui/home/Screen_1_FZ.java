/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.home;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.ow2.frascati.demo.homeautomation.api.IMediator;

/**
*
* @author Grégory Nain
* @contact gregory.nain@irisa.fr
* 
*/
@SuppressWarnings(value = { "all" })
public class Screen_1_FZ extends JPanel implements Updateable {

	//private ToolBar tb;
	private JButton scr1_FZ_bt_heaters;
	private JButton scr1_FZ_bt_lights;
	private JButton scr1_FZ_bt_shutters;
	private JPanel scr1_FZ_functions_panel;
	private JPanel scr1_FZ_panel;
	private JPanel scr1_FZ_panel_center;
	private JPanel scr1_FZ_panel_heat;
	private JPanel scr1_FZ_panel_lights;
	private JPanel scr1_FZ_panel_shutters;
	private JPanel scr1_FZ_panelTop;
	private JScrollPane jScrollPane1;
	private HeaterWidget scr1_FZ_panel_heatWidget_chambre;
	private HeaterWidget scr1_FZ_panel_heatWidget_cuisine;
	private HeaterWidget scr1_FZ_panel_heatWidget_salon;
	private HeaterWidget scr1_FZ_panel_heatWidget_sdb;
	private LightWidget scr1_FZ_panel_lightWidget_chambre;
	private LightWidget scr1_FZ_panel_lightWidget_cuisine;
	private LightWidget scr1_FZ_panel_lightWidget_salon;
	private LightWidget scr1_FZ_panel_lightWidget_sdb;

	private ShutterWidget scr1_FZ_panel_shutterWidget_chambre;
	private ShutterWidget scr1_FZ_panel_shutterWidget_cuisine;
	private ShutterWidget scr1_FZ_panel_shutterWidget_salon;
	private ShutterWidget scr1_FZ_panel_shutterWidget_sdb;

	private Vector<LightWidget> lightWidgets;

	public Screen_1_FZ(IMediator mediator) {
		this.mediator = mediator;
		
		initComponents();

		layoutComponents();

	}

	private void initLightWidgets() {

		lightWidgets = new Vector<LightWidget>();

		fillLightWidgets();

	}

	private void fillLightWidgets() {

		lightWidgets.clear();

		/*ServiceReference refCl = Main.context.getServiceReference(CausalLink.class.getName());
		OSGiCausalLink cl = null;
		if( refCl != null)
			cl = (OSGiCausalLink)Main.context.getService(refCl); 

		try {

			ServiceReference[] refs = Main.context.getServiceReferences(OnOffService.class.getName(), null);
			if( refs != null)
				for(ServiceReference ref : refs) {
					final LightWidget wid = new LightWidget();
					String group = "";
					if( cl != null) {
						String iName = (String)ref.getProperty("InstanceName");
						if(iName == null) {
							group = "<Pas de Groupe>";
						}else{
							List<String> groups = cl.getGroup(iName);
							if( groups.size() > 0)
								group = groups.get(0).substring(groups.get(0).lastIndexOf("/")+1);
							else
								group = "<Pas de Groupe>";
						}
					} else 
						group = "<Groupe Inconnu>";
					wid.setLabelText(group);
					wid.setOnOffService((OnOffService)Main.context.getService(ref));
					
					ServiceReference[] stateInformer = 
						Main.context.getServiceReferences(OnOffStateInformer.class.getName(),
								"(DeviceName="+(String)ref.getProperty("DeviceName") + ")");
					
					if(stateInformer != null)
						if(stateInformer.length > 0)
							wid.setStateInformer((OnOffStateInformer)Main.context.getService(stateInformer[0]));
					
					lightWidgets.add(wid);
				}

		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		
		scr1_FZ_panel_lightWidget_cuisine = new LightWidget("cuisine");
		scr1_FZ_panel_lightWidget_cuisine.setLabelText("CUISINE");
		scr1_FZ_panel_lightWidget_cuisine.setMediator(mediator);

		scr1_FZ_panel_lightWidget_salon = new LightWidget("salon");
		scr1_FZ_panel_lightWidget_salon.setLabelText("Salon");
		scr1_FZ_panel_lightWidget_salon.setMediator(mediator);
		
		scr1_FZ_panel_lightWidget_chambre = new LightWidget("chambre");
		scr1_FZ_panel_lightWidget_chambre.setLabelText("chambre");
		scr1_FZ_panel_lightWidget_chambre.setMediator(mediator);

		scr1_FZ_panel_lightWidget_sdb = new LightWidget("sdb");
		scr1_FZ_panel_lightWidget_sdb.setLabelText("salle de bain");
		scr1_FZ_panel_lightWidget_sdb.setMediator(mediator);
		
        lightWidgets.add(scr1_FZ_panel_lightWidget_cuisine);
        lightWidgets.add(scr1_FZ_panel_lightWidget_salon);
        lightWidgets.add(scr1_FZ_panel_lightWidget_chambre);
        lightWidgets.add(scr1_FZ_panel_lightWidget_sdb);

	}

	private void initComponents() {

		//	tb = new ToolBar();

		scr1_FZ_panelTop = new JPanel();
		scr1_FZ_functions_panel = new JPanel();

		scr1_FZ_bt_lights = new JButton();
		scr1_FZ_bt_lights.setBackground(new java.awt.Color(255, 255, 153));
		scr1_FZ_bt_lights.setFont(new java.awt.Font("Tahoma", 0, 24));
		scr1_FZ_bt_lights.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/lightOff.png"))); // NOI18N
		scr1_FZ_bt_lights.setText("LUMIERE");
		scr1_FZ_bt_lights.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4));
		scr1_FZ_bt_lights.setHorizontalTextPosition(SwingConstants.CENTER);
		scr1_FZ_bt_lights.setVerticalTextPosition(SwingConstants.BOTTOM);
		scr1_FZ_bt_lights.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				//MainFrame.playMP3("mp3/lumiere.mp3");
				((CardLayout) scr1_FZ_panel_center.getLayout()).show(scr1_FZ_panel_center, "scr1_FZ_panel_lights");
			}
		});


		scr1_FZ_bt_heaters = new JButton();
		scr1_FZ_bt_heaters.setBackground(new java.awt.Color(153, 204, 255));
		scr1_FZ_bt_heaters.setFont(new java.awt.Font("Tahoma", 0, 24));
		scr1_FZ_bt_heaters.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/radiateur.png"))); // NOI18N
		scr1_FZ_bt_heaters.setText("CHAUFFAGE");
		scr1_FZ_bt_heaters.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4));
		scr1_FZ_bt_heaters.setHorizontalTextPosition(SwingConstants.CENTER);
		scr1_FZ_bt_heaters.setVerticalTextPosition(SwingConstants.BOTTOM);
		scr1_FZ_bt_heaters.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				//MainFrame.playMP3("mp3/chauffage.mp3");
				((CardLayout) scr1_FZ_panel_center.getLayout()).show(scr1_FZ_panel_center, "scr1_FZ_panel_heat");
			}
		});


		scr1_FZ_bt_shutters = new JButton();
		scr1_FZ_bt_shutters.setBackground(new java.awt.Color(204, 255, 204));
		scr1_FZ_bt_shutters.setFont(new java.awt.Font("Tahoma", 0, 24));
		scr1_FZ_bt_shutters.setIcon(new ImageIcon(getClass().getClassLoader().getResource("images/volet_petit.png"))); // NOI18N
		scr1_FZ_bt_shutters.setText("VOLETS");
		scr1_FZ_bt_shutters.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 4));
		scr1_FZ_bt_shutters.setHorizontalTextPosition(SwingConstants.CENTER);
		scr1_FZ_bt_shutters.setVerticalTextPosition(SwingConstants.BOTTOM);
		scr1_FZ_bt_shutters.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				//MainFrame.playMP3("mp3/volet.mp3");
				((CardLayout) scr1_FZ_panel_center.getLayout()).show(scr1_FZ_panel_center, "scr1_FZ_panel_shutters");

			}
		});


		scr1_FZ_panel = new JPanel();
		scr1_FZ_panel.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 10));

		scr1_FZ_panel_center = new JPanel();
		scr1_FZ_panel_center.setMaximumSize(new java.awt.Dimension(800, 2147483647));
		scr1_FZ_panel_center.setPreferredSize(new java.awt.Dimension(800, 466));

		jScrollPane1 = new JScrollPane();


		scr1_FZ_panel_lights = new JPanel();
		scr1_FZ_panel_lights.setBackground(new java.awt.Color(255, 255, 153));
		scr1_FZ_panel_lights.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
		scr1_FZ_panel_lights.setPreferredSize(new java.awt.Dimension(1000, 222));


		initLightWidgets();


		scr1_FZ_panel_heat = new JPanel();
		scr1_FZ_panel_heat.setBackground(new java.awt.Color(153, 204, 255));
		scr1_FZ_panel_heat.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
		scr1_FZ_panel_heat.setPreferredSize(new java.awt.Dimension(1000, 442));

		scr1_FZ_panel_heatWidget_cuisine = new HeaterWidget();
		scr1_FZ_panel_heatWidget_cuisine.setLabel("cuisine");

		scr1_FZ_panel_heatWidget_salon = new HeaterWidget();
		scr1_FZ_panel_heatWidget_salon.setLabel("salon");

		scr1_FZ_panel_heatWidget_chambre = new HeaterWidget();
		scr1_FZ_panel_heatWidget_chambre.setLabel("chambre");

		scr1_FZ_panel_heatWidget_sdb = new HeaterWidget();
		scr1_FZ_panel_heatWidget_sdb.setLabel("salle de bain");

		scr1_FZ_panel_shutters = new JPanel();
		scr1_FZ_panel_shutters.setBackground(new java.awt.Color(204, 255, 204));
		scr1_FZ_panel_shutters.setBorder(BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
		scr1_FZ_panel_shutters.setPreferredSize(new java.awt.Dimension(1000, 466));

		scr1_FZ_panel_shutterWidget_cuisine = new ShutterWidget();
		scr1_FZ_panel_shutterWidget_cuisine.setLabel("cuisine");

		scr1_FZ_panel_shutterWidget_salon = new ShutterWidget();
		scr1_FZ_panel_shutterWidget_salon.setLabel("salon");

		scr1_FZ_panel_shutterWidget_chambre = new ShutterWidget();
		scr1_FZ_panel_shutterWidget_chambre.setLabel("chambre");

		scr1_FZ_panel_shutterWidget_sdb = new ShutterWidget();
		scr1_FZ_panel_shutterWidget_sdb.setLabel("salle de bain");


		this.setBackground(new Color(255, 255, 102));
	}

	private void layoutLights() {
		scr1_FZ_panel_lights.removeAll();
		scr1_FZ_panel_lights.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 20, 20));

		for(LightWidget w : lightWidgets)
			scr1_FZ_panel_lights.add(w);
	}

	private void layoutComponents() {

		scr1_FZ_functions_panel.setLayout(new java.awt.GridLayout(1, 0));

		scr1_FZ_functions_panel.add(scr1_FZ_bt_lights);
		scr1_FZ_functions_panel.add(scr1_FZ_bt_heaters);
		scr1_FZ_functions_panel.add(scr1_FZ_bt_shutters);

		scr1_FZ_panelTop.setLayout(new java.awt.BorderLayout());
		scr1_FZ_panelTop.add(scr1_FZ_functions_panel, java.awt.BorderLayout.CENTER);
		//	scr1_FZ_panelTop.add(tb, java.awt.BorderLayout.EAST);

		layoutLights();

		scr1_FZ_panel_heat.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 20, 20));
		scr1_FZ_panel_heat.add(scr1_FZ_panel_heatWidget_cuisine);
		scr1_FZ_panel_heat.add(scr1_FZ_panel_heatWidget_salon);
		scr1_FZ_panel_heat.add(scr1_FZ_panel_heatWidget_chambre);
		scr1_FZ_panel_heat.add(scr1_FZ_panel_heatWidget_sdb);

		scr1_FZ_panel_shutters.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 20, 20));
		scr1_FZ_panel_shutters.add(scr1_FZ_panel_shutterWidget_cuisine);
		scr1_FZ_panel_shutters.add(scr1_FZ_panel_shutterWidget_salon);
		scr1_FZ_panel_shutters.add(scr1_FZ_panel_shutterWidget_chambre);
		scr1_FZ_panel_shutters.add(scr1_FZ_panel_shutterWidget_sdb);


		scr1_FZ_panel_center.setLayout(new java.awt.CardLayout());
		scr1_FZ_panel_center.add(scr1_FZ_panel_lights, "scr1_FZ_panel_lights");
		scr1_FZ_panel_center.add(scr1_FZ_panel_heat, "scr1_FZ_panel_heat");
		scr1_FZ_panel_center.add(scr1_FZ_panel_shutters, "scr1_FZ_panel_shutters");

		jScrollPane1.setViewportView(scr1_FZ_panel_center);

		scr1_FZ_panel.setLayout(new java.awt.BorderLayout());
		scr1_FZ_panel.add(jScrollPane1, java.awt.BorderLayout.CENTER);

		this.setLayout(new BorderLayout());
		this.add(scr1_FZ_panelTop, java.awt.BorderLayout.NORTH);
		this.add(scr1_FZ_panel, java.awt.BorderLayout.CENTER);
	}

	public void update() {
		fillLightWidgets();
		layoutLights();
		scr1_FZ_panel_lights.revalidate();
		repaint();
	}

	private IMediator mediator;
	
       public static void main(String[] args) {
            JFrame f = new JFrame();
            f.add(new Screen_1_FZ(null));
            f.pack();
            f.setVisible(true);
        }

}
