/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.nurse;

import java.util.List;

import javax.swing.UIManager;

import com.google.gdata.data.calendar.CalendarEventEntry;

import org.ow2.frascati.demo.homeautomation.api.IMediator;

public class NurseFrame extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;
	
	public NurseFrame() {
		try {
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        initComponents();
        pack();
        setLocationRelativeTo(null);
    }
	
	public void setMediator(IMediator mediator) {
		this.mediator = mediator;
	}
	
    private void initComponents() {

        rightPanel = new javax.swing.JPanel();
        statusPanel = new javax.swing.JPanel();
        sendReportButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        tensionLabel = new javax.swing.JLabel();
        tensionComboBox = new javax.swing.JComboBox();
        temperatureLabel = new javax.swing.JLabel();
        temperatureComboBox2 = new javax.swing.JComboBox();
        respirationLabel = new javax.swing.JLabel();
        respirationComboBox = new javax.swing.JComboBox();
        diureseLabel = new javax.swing.JLabel();
        diureseComboBox = new javax.swing.JComboBox();
        poulsLabel = new javax.swing.JLabel();
        poulsComboBox = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        visiteLabel = new javax.swing.JLabel();
        visiteMedecinCheckBox = new javax.swing.JCheckBox();

        setTitle("Fiche patient");
        setResizable(false);

        rightPanel.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        statusPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Fiche patient"));

        sendReportButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/sendReport.png")));
        sendReportButton.setFocusable(false);
        sendReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendReportButtonActionPerformed(evt);
            }
        });

        cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/close.png")));
        cancelButton.setFocusable(false);
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        tensionLabel.setText("Tension arterielle : ");

        tensionComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "hypotension", "normal", "hypertension" }));

        temperatureLabel.setText("Temperature : ");

        temperatureComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "hypothermique", "normal", "hyperthermique" }));

        respirationLabel.setText("Respiration : ");

        respirationComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "bradypnee", "normal", "polypnee" }));

        diureseLabel.setText("Diurese : ");

        diureseComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "pollakiurie", "normal", "dysurie" }));

        poulsLabel.setText("Pouls : ");

        poulsComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "bradycardie", "normal", "tachycardie" }));

        visiteLabel.setText("Planifier une visite du medecin ? ");

        visiteMedecinCheckBox.setFocusPainted(false);
        visiteMedecinCheckBox.setFocusable(false);

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(statusPanelLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(poulsLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(diureseLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(respirationLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(temperatureLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tensionLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 138, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(temperatureComboBox2, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(tensionComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, 163, Short.MAX_VALUE)
                                        .addComponent(respirationComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(diureseComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(poulsComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                     )
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                            .addGroup(statusPanelLayout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(visiteLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(visiteMedecinCheckBox))))
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(sendReportButton)
                        .addGap(18, 18, 18)
                        .addComponent(cancelButton)))
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, statusPanelLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tensionLabel)
                    .addComponent(tensionComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(temperatureLabel)
                    .addComponent(temperatureComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(respirationLabel)
                    .addComponent(respirationComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(diureseLabel)
                    .addComponent(diureseComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(poulsLabel)
                    .addComponent(poulsComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        )
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        ))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(visiteLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                    .addComponent(visiteMedecinCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addGap(46, 46, 46)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(sendReportButton)
                    .addComponent(cancelButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout rightPanelLayout = new javax.swing.GroupLayout(rightPanel);
        rightPanel.setLayout(rightPanelLayout);
        rightPanelLayout.setHorizontalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        rightPanelLayout.setVerticalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(rightPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(rightPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }

    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        setVisible(false);
    }

    private void sendReportButtonActionPerformed(java.awt.event.ActionEvent evt) {
        
    	//Look for available meetings
    	List<CalendarEventEntry> meetings = null;
        
		if(visiteMedecinCheckBox.isSelected()){
        	meetings = mediator.getAvailableMeetings();
        	new RDVFrame(mediator,meetings).setVisible(true);
		}
		
    	setVisible(false);
        // get report data and send effectively a report to the remote center
    }
    
    public static void main(String[] args){
    	new NurseFrame().setVisible(true);
    }

    private javax.swing.JButton cancelButton;
    private javax.swing.JComboBox diureseComboBox;
    private javax.swing.JLabel diureseLabel;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JComboBox poulsComboBox;
    private javax.swing.JLabel poulsLabel;
    private javax.swing.JComboBox respirationComboBox;
    private javax.swing.JLabel respirationLabel;
    private javax.swing.JPanel rightPanel;
    private javax.swing.JButton sendReportButton;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JComboBox temperatureComboBox2;
    private javax.swing.JLabel temperatureLabel;
    private javax.swing.JComboBox tensionComboBox;
    private javax.swing.JLabel tensionLabel;
    private javax.swing.JLabel visiteLabel;
    private javax.swing.JCheckBox visiteMedecinCheckBox;
    private IMediator mediator;

}
