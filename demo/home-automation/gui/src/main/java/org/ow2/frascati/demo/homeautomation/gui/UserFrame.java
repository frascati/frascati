/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui;

import java.awt.GraphicsDevice;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;

import javax.swing.UIManager;

import org.osoa.sca.annotations.Init;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.demo.homeautomation.api.GUIService;
import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.StaffUIService;
import org.ow2.frascati.demo.homeautomation.gui.home.Screen_1_FZ;


@Scope("COMPOSITE")
public class UserFrame extends javax.swing.JFrame implements GUIService {

	private static final long serialVersionUID = 1L;

	@Init
    public void initComponents() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception ex) {
             ex.printStackTrace();
         }
         
        staffUiServices = new ArrayList<StaffUIService>();
        
        timePanel = new TimePanel();
        contentPanel = new javax.swing.JPanel();
        homeButton = new javax.swing.JButton();
        weatherButton = new javax.swing.JButton();
        mapButton = new javax.swing.JButton();
        weatherFrame = new WeatherFrame(mediator);

        homeFrame = new javax.swing.JFrame();
        homeFrame.add(new Screen_1_FZ(mediator));
        homeFrame.setDefaultCloseOperation(HIDE_ON_CLOSE);
        homeFrame.pack();

        setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        //setUndecorated(true);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.PAGE_AXIS));
        getContentPane().add(timePanel);

        contentPanel.setLayout(new java.awt.GridLayout(1, 3, 50, 50));

        homeButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/home.png"))); 
        homeButton.setFocusable(false);
        homeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeButtonActionPerformed(evt);
            }
        });
        contentPanel.add(homeButton);

        weatherButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/weather.png"))); 
        weatherButton.setFocusable(false);
        weatherButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                weatherButtonActionPerformed(evt);
            }
        });
        contentPanel.add(weatherButton);
        
        mapButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/map.png"))); 
        mapButton.setFocusable(false);
        mapButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mapButtonActionPerformed(evt);
            }
        });
        contentPanel.add(mapButton);
        
        getContentPane().add(contentPanel);
        pack();
    }

	private void mapButtonActionPerformed(java.awt.event.ActionEvent evt) {
		javax.swing.JFrame mapFrame = new javax.swing.JFrame();
		mapFrame.add(mediator.getMapData());
		mapFrame.setSize(800,600);
		mapFrame.setResizable(false);
		mapFrame.setVisible(true);
		mapFrame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
    private void homeButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	homeFrame.setVisible(true);
    }

    private void weatherButtonActionPerformed(java.awt.event.ActionEvent evt) {
        	weatherFrame.setVisible(true);
    }

    public void setFullScreen() {
        GraphicsDevice gd = java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        Window w = this;
        if (gd.isFullScreenSupported()) {
            try {
               gd.setFullScreenWindow(w);
            } catch (Exception e) {
                gd.setFullScreenWindow(null);
            }
        }
    }

    private javax.swing.JPanel contentPanel;
    private javax.swing.JButton homeButton;
    private javax.swing.JPanel timePanel;
    private javax.swing.JButton weatherButton;
    private javax.swing.JButton mapButton;
    private WeatherFrame weatherFrame;
    private javax.swing.JFrame homeFrame;
    private IMediator mediator;
    
    private List<StaffUIService> staffUiServices;
    
	public void showGUI() {
		setVisible(true);
	}

	synchronized public void registerStaffUiService(StaffUIService sus){
		staffUiServices.add(sus);
		update();
	}
	
	synchronized public void unregisterStaffUiService(StaffUIService sus){
		staffUiServices.remove(sus);
		update();
	}
	
	private void update() {
		contentPanel.removeAll();
		//add common buttons
		contentPanel.add(homeButton);
		contentPanel.add(weatherButton);
		contentPanel.add(mapButton);
		//add additional buttons for each staff ui service registered
		for(StaffUIService sus : staffUiServices) {
			contentPanel.add(sus.getButton());
		}
	}
	
	@Reference
	public void setMediator(IMediator mediator) {
		this.mediator = mediator;
	}
	
}
