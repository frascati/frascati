/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui.expert;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;

import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.demo.homeautomation.api.StaffUIService;

@Scope("COMPOSITE")
public class ExpertButton extends JButton implements StaffUIService {

	private static final long serialVersionUID = 1L;
	
	public ExpertButton() {
        setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/monitor.png"))); 
        setFocusable(false);
        addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	// TODO replace with :
            	//java.awt.Desktop.getDesktop().browse(new java.net.URI("path/to/logfile/or/monitoring/console"));
            	Logger.getLogger(ExpertButton.class.getName()).log(Level.INFO,"[Expert UI service] events are logged in the console");
            }
        });
	}

	@Override
	public JButton getButton() {
		return this;
	}

}
