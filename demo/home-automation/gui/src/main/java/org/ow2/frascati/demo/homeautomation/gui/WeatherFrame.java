/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import javax.swing.UIManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import org.ow2.frascati.demo.homeautomation.api.IMediator;

public class WeatherFrame extends javax.swing.JFrame {

	private static final long serialVersionUID = 1L;

    public WeatherFrame(IMediator mediator) {
    	this.mediator = mediator;
    	
        try {
    		dbf = DocumentBuilderFactory.newInstance();
    		db = dbf.newDocumentBuilder();
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
         } catch (Exception ex) {
             ex.printStackTrace();
        }
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dateLabel.setText("Date : " + dateFormat.format(GregorianCalendar.getInstance().getTime()));
        timeLabel.setText("Heure : " + timeFormat.format(GregorianCalendar.getInstance().getTime()));
        pack();
        setLocationRelativeTo(null);
    }

    private void initComponents() {

        actualiserButton = new javax.swing.JButton();
        pressionLabel = new javax.swing.JLabel();
        meteoLabel = new javax.swing.JLabel();
        humiditeLabel = new javax.swing.JLabel();
        temperatureLabel = new javax.swing.JLabel();
        retourButton = new javax.swing.JButton();
        conditionLabel = new javax.swing.JLabel();
        lieuLabel = new javax.swing.JLabel();
        dateLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setResizable(false);

        actualiserButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/refresh.png")));
        actualiserButton.setToolTipText("Actualiser");
        actualiserButton.setFocusable(false);
        actualiserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actualiserButtonActionPerformed(evt);
            }
        });

        pressionLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        pressionLabel.setText("Pression : ");

        meteoLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        meteoLabel.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/weather.png")));

        humiditeLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        humiditeLabel.setText("Humidite : ");

        temperatureLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        temperatureLabel.setText("Temperature : ");

        retourButton.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/close.png")));
        retourButton.setFocusable(false);
        retourButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retourButtonActionPerformed(evt);
            }
        });

        conditionLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        conditionLabel.setText("Conditions meteo : ");

        lieuLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        lieuLabel.setText("Lieu : " + mediator.getCity() + "(" + mediator.getCountry()  + ")");

        dateLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        dateLabel.setText("Date : ");

        timeLabel.setFont(new java.awt.Font("DejaVu Sans", 1, 18));
        timeLabel.setText("Heure : ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(meteoLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(timeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dateLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lieuLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(conditionLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(140, 140, 140)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pressionLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(humiditeLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(temperatureLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE))
                .addContainerGap(69, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(200, Short.MAX_VALUE)
                .addComponent(actualiserButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(retourButton)
                .addGap(169, 169, 169))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(conditionLabel)
                        .addGap(18, 18, 18)
                        .addComponent(lieuLabel)
                        .addGap(18, 18, 18)
                        .addComponent(dateLabel)
                        .addGap(18, 18, 18)
                        .addComponent(timeLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(meteoLabel)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(temperatureLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(humiditeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pressionLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(retourButton)
                    .addComponent(actualiserButton))
                .addContainerGap())
        );
    }

    private void retourButtonActionPerformed(java.awt.event.ActionEvent evt) {
        setVisible(false);
    }

	private void actualiserButtonActionPerformed(java.awt.event.ActionEvent evt) {

		String weatherData = mediator.getWeatherData();

		dateLabel.setText("Date : " + dateFormat.format(GregorianCalendar.getInstance().getTime()));
		timeLabel.setText("Heure : " + timeFormat.format(GregorianCalendar.getInstance().getTime()));

		if (weatherData != null) {
			// format xml feed result
			InputStream stream = new ByteArrayInputStream(weatherData
					.getBytes());
			Document doc;
			try {
				doc = db.parse(stream);
				doc.getDocumentElement().normalize();
				String temperature = doc.getElementsByTagName("Temperature").item(0).getTextContent();
				String pressure = doc.getElementsByTagName("Pressure").item(0).getTextContent();
				String humidity = doc.getElementsByTagName("RelativeHumidity").item(0).getTextContent();
				temperatureLabel.setText("Temperature : " + temperature);
				humiditeLabel.setText("Humidite : " + humidity);
				pressionLabel.setText("Pression : " + pressure);
			} catch (SAXException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			temperatureLabel.setText(" Service meteo indisponible");
			humiditeLabel.setText("          pour le moment");
			pressionLabel.setText("");
		}
	}

    private DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE);
    private DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.FRANCE);
	private DocumentBuilderFactory dbf;
	private DocumentBuilder db;
    private javax.swing.JButton actualiserButton;
    private javax.swing.JLabel conditionLabel;
    private javax.swing.JLabel dateLabel;
    private javax.swing.JLabel humiditeLabel;
    private javax.swing.JLabel lieuLabel;
    private javax.swing.JLabel meteoLabel;
    private javax.swing.JLabel pressionLabel;
    private javax.swing.JButton retourButton;
    private javax.swing.JLabel temperatureLabel;
    private javax.swing.JLabel timeLabel;
    private IMediator mediator;

}
