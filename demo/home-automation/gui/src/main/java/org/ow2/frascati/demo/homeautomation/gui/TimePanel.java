/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.gui;

import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.UIManager;


public class TimePanel extends javax.swing.JPanel {


	private static final long serialVersionUID = 1L;

    public TimePanel() {
        try {
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        initComponents();
        Timer timer = new Timer();
        final DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.FULL, Locale.FRANCE);
        final DateFormat timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM, Locale.FRANCE);
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                dateLabel.setText("" + dateFormat.format(GregorianCalendar.getInstance().getTime()));
                timeLabel.setText(" \t \t \t \t \t \t \t \t \t \t" + timeFormat.format(GregorianCalendar.getInstance().getTime()));
            }
        }, 0, 1000);
    }

    private void initComponents() {

        dateLabel = new javax.swing.JLabel();
        timeLabel = new javax.swing.JLabel();

        dateLabel.setFont(new java.awt.Font("Serif", 3, 48));
        dateLabel.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/calendar_date.png"))); 

        timeLabel.setFont(new java.awt.Font("Serif", 3, 48)); 
        timeLabel.setIcon(new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/clock.png")));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(dateLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(timeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 760, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(dateLabel)
                .addGap(22, 22, 22)
                .addComponent(timeLabel)
                .addContainerGap(27, Short.MAX_VALUE))
        );
    }


    private javax.swing.JLabel dateLabel;
    private javax.swing.JLabel timeLabel;

}
