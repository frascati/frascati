package org.ow2.frascati.demo.homeautomation.api;

/**
 * Communication service
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
public interface CommunicationService {

	/**
	 * Send a short message to a phone number
	 * @param number the phone number to send the short message to
	 * @param sms the short message to send
	 */
	void sendSMS(String number,String sms);
	
}
