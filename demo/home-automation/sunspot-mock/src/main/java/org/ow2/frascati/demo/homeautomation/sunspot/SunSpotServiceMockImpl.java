/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): GrÃ©gory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.sunspot;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.SunSpotService;

/**
 * Mock implementation of {@link SunSpotService} interface
 * 
 * @author Mahmoud Ben Hassine
 *
 */
public class SunSpotServiceMockImpl implements SunSpotService {

	@Reference
	private IMediator mediator;
	
	public void init() {
		JButton button = new JButton("simulate patient fall");
		SunSpotActionListener sunSpotActionListener = new SunSpotActionListener();
		button.addActionListener(sunSpotActionListener);
		JFrame f = new JFrame("SunSpot service emulator");
		f.getRootPane().setLayout(new FlowLayout());
		f.getRootPane().add(button);
		f.pack();
		f.setAlwaysOnTop(true);
		f.setVisible(true);
		Logger.getLogger(SunSpotServiceMockImpl.class.getName()).log(Level.INFO," [SunSpot mock service] SunSpot mock service initialized successfully");
	}

	public void notifyAccelerometerVariation() {
		Logger.getLogger(SunSpotServiceMockImpl.class.getName()).log(Level.INFO," [SunSpot mock service] accelerometer variation detected");
		mediator.sendSMS(mediator.getNursePhoneNumber(), "fall detected for patient '" + mediator.getPatientName() + "' [id=" + mediator.getPatientId() + "]");
	}

	private class SunSpotActionListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			notifyAccelerometerVariation();
		}
		
	}
}
