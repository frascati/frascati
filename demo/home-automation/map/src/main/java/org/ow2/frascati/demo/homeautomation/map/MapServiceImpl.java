/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.map;

import javax.swing.JPanel;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.mapviewer.GeoPosition;

import org.ow2.frascati.demo.homeautomation.api.MapService;

/**
 * Implementation of the {@link MapService} interface.
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 */

public class MapServiceImpl implements MapService {

	@Override
	public JPanel getMap(double latitude,double longitude) {
		
		JXMapKit jXMapKit = new JXMapKit();
        jXMapKit.setDefaultProvider(JXMapKit.DefaultProviders.OpenStreetMaps);
        jXMapKit.setDataProviderCreditShown(true);
        jXMapKit.setAddressLocation(new GeoPosition(latitude,longitude));
        jXMapKit.setZoom(5);
        
        return  jXMapKit;   
	}
}
