/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.tts;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;


import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.homeautomation.api.TTSService;

/**
 * Implementation of the {@link TTSService} interface
 *
 */
@Scope("COMPOSITE")
public class TTSServiceImpl implements TTSService {

	private HashMap<String,File> sounds;
	
	public TTSServiceImpl() {
	}

	public void init(){
		sounds = new HashMap<String, File>();
		sounds.put("cuisine-on", new File("sounds/cuisine_allumee.mp3"));
		sounds.put("cuisine-off", new File("sounds/cuisine_eteinte.mp3"));
		sounds.put("salon-on", new File("sounds/salon_allume.mp3"));
		sounds.put("salon-off", new File("sounds/salon_eteint.mp3"));
		sounds.put("chambre-on", new File("sounds/chambre_allumee.mp3"));
		sounds.put("chambre-off", new File("sounds/chambre_eteinte.mp3"));
		sounds.put("sdb-on", new File("sounds/sdb_allumee.mp3"));
		sounds.put("sdb-off", new File("sounds/sdb_eteinte.mp3"));
		Logger.getLogger(TTSServiceImpl.class.getName()).log(Level.INFO," [TTS service] TTS service initialized successfully");
	}
	
	public void playFile(File mp3file) throws Exception {

            InputStream is = this.getClass().getClassLoader().getResource(mp3file.toString()).openStream();
			AudioInputStream in = AudioSystem.getAudioInputStream(is);
			AudioFormat baseFormat = in.getFormat();
			AudioFormat decodedFormat = new AudioFormat(
					AudioFormat.Encoding.PCM_SIGNED,
					baseFormat.getSampleRate(), 16, baseFormat.getChannels(),
					baseFormat.getChannels() * 2, baseFormat.getSampleRate(),
					false);
			AudioInputStream din = AudioSystem.getAudioInputStream(
					decodedFormat, in);
			DataLine.Info info = new DataLine.Info(SourceDataLine.class,
					decodedFormat);
			SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
			if (line != null) {
				line.open(decodedFormat);
				byte[] data = new byte[4096];
				// Start
				line.start();
				int nBytesRead;
				while ((nBytesRead = din.read(data, 0, data.length)) != -1) {
					line.write(data, 0, nBytesRead);
				}
				// Stop
				line.drain();
				line.stop();
				line.close();
				din.close();
			}
	}

	@Override
	public HashMap<String, File> getSounds() {
		return sounds;
	}

}
