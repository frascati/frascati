/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.calendar;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gdata.data.DateTime;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.extensions.When;
import com.google.gdata.util.ServiceException;

import org.ow2.frascati.demo.homeautomation.api.GcalService;

/**
 * Mock implementation of {@link org.ow2.frascati.demo.homeautomation.api.GcalService} interface
 * 
 * @author Mahmoud Ben Hassine
 *
 */
public class GcalServiceMockImpl implements GcalService {

	@Override
	public void addEvent(CalendarEventEntry entry) throws IOException,
			ServiceException {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar mock service] meeting '" + entry.getTitle().getPlainText() + "' added to the primary calendar");
	}

	@Override
	public void init() {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar mock service] Calendar mock service initialized successfully");
	}

	@Override
	public void scheduleTask(CalendarEventEntry entry, TimerTask task) {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar mock service] task scheduled for meeting '" + entry.getTitle().getPlainText());
	}
	
	@Override
	public void deleteEvent(CalendarEventEntry entry) throws IOException,
			ServiceException {
	}

	@Override
	public List<CalendarEventEntry> getEvents() throws IOException,
			ServiceException {
		return null;
	}

	@Override
	public List<CalendarEventEntry> getAvailableMeetings(Date startDate,int field,int number) {
			
		  //TODO add filter for unavailable entries
			List<CalendarEventEntry> entries = new ArrayList<CalendarEventEntry>();
			//Date lastMeeting = getLastMeeting(startDate,field,number);
			Date nextMeeting = getNextMeeting(startDate);
			//while (nextMeeting.before(lastMeeting)) {
			for (int i = 0; i < 6; i++) {
				// keep only working day meetings  
				if(Time.valueOf(extractTime(nextMeeting)).before(Time.valueOf(LAST_MEETING))
						&& Time.valueOf(extractTime(nextMeeting)).after(Time.valueOf(FIRST_MEETING))){
				CalendarEventEntry entry = new CalendarEventEntry();
				entry.setId(nextMeeting.toString());
		        When entryTimes = new When();
		        entryTimes.setStartTime(new DateTime(nextMeeting,Calendar.getInstance().getTimeZone()));
		        entryTimes.setEndTime(new DateTime(getNextMeeting(nextMeeting),Calendar.getInstance().getTimeZone()));
		        entry.addTime(entryTimes);
				entries.add(entry);
				}
				nextMeeting = getNextMeeting(nextMeeting);
			}
			return entries;
	}

	/*
	 * private utility methods
	 * TODO improve filters
	 */
	
	private Date getNextMeeting(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MINUTE);
		calendar.roll(Calendar.HOUR_OF_DAY, true);
		if(calendar.get(Calendar.HOUR_OF_DAY) == Calendar.getInstance().getMaximum(Calendar.HOUR_OF_DAY))
			calendar.roll(Calendar.DAY_OF_MONTH, true);
		return calendar.getTime();
	}
	
	@SuppressWarnings(value = { "unused" })
	private Date getLastMeeting(Date now,int field,int number) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
	    calendar.add(field,number);
		return calendar.getTime();
	}
	
	private String extractTime(Date date){
		// TODO remove this ugly substring
		return date.toString().substring(11, 19);
	}
	
	private static final String FIRST_MEETING = "07:00:00";

	private static final String LAST_MEETING = "19:00:00";
	
}
