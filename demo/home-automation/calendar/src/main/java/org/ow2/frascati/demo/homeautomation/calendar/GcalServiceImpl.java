/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): GrÃ©gory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

/**
 * 
 */
package org.ow2.frascati.demo.homeautomation.calendar;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Property;

import com.google.gdata.client.calendar.CalendarService;
import com.google.gdata.data.DateTime;
import com.google.gdata.data.calendar.CalendarEventEntry;
import com.google.gdata.data.calendar.CalendarEventFeed;
import com.google.gdata.data.extensions.When;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import org.ow2.frascati.demo.homeautomation.api.GcalService;

/**
 * Implementation of the {@link org.ow2.frascati.demo.homeautomation.api.GcalService} interface
 * 
 * @author Mahmoud Ben Hassine
 * @version 1.0
 *
 */
public class GcalServiceImpl implements GcalService {

	private static final String FIRST_MEETING = "07:00:00";

	private static final String LAST_MEETING = "19:00:00";

	/**
	 * The Calendar service
	 */
	CalendarService service;
	
	/**
	 * The primary calendar feed URL
	 */
	URL primaryCalendarFeedUrl;
	
	/**
	 * Timer used for scheduling task at the beginning of calendar events
	 */
	Timer timer;

	@Property
	private String login;
	
	@Property
	private String password;
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public GcalServiceImpl() {
	}
	
	public void init(){
		
		timer = new Timer();
		
		try {
			primaryCalendarFeedUrl = new URL("http://www.google.com/calendar/feeds/default/private/full");
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		
		service = new CalendarService("Galaxy : Home automation demo");
		try {
			service.setUserCredentials(login,password);
		} catch (AuthenticationException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar service]  Authentication Exception for user '" + login + "' and password '" + password + "'");
		}
		Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar service] Calendar service initialized successfully");
	}
	
	
	@Override
	public void addEvent(CalendarEventEntry entry) throws IOException, ServiceException {
			service.insert(primaryCalendarFeedUrl, entry);
			Logger.getLogger(this.getClass().getName()).log(Level.INFO," [Calendar service] meeting '" + entry.getTitle().getPlainText() + "' added to the primary calendar");
	}

	@Override
	public void deleteEvent(CalendarEventEntry entry) throws IOException, ServiceException {
		URL deleteUrl = null;
		try {
			deleteUrl = new URL(entry.getEditLink().getHref());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		service.delete(deleteUrl);
	}

	@Override
	public List<CalendarEventEntry> getEvents() throws IOException, ServiceException {
		CalendarEventFeed ef = service.getFeed(primaryCalendarFeedUrl, CalendarEventFeed.class);
		return ef.getEntries();
	}

	@Override
	public void scheduleTask(CalendarEventEntry entry, TimerTask task) {
			DateTime dt = entry.getTimes().get(0).getStartTime();
			timer.schedule(task, new java.util.Date(dt.getValue()));		
	}


	@Override
	public List<CalendarEventEntry> getAvailableMeetings(Date startDate,int field,int number) {
			//TODO add filter for unavailable entries
			List<CalendarEventEntry> entries = new ArrayList<CalendarEventEntry>();
			Date lastMeeting = getLastMeeting(startDate,field,number);
			Date nextMeeting = getNextMeeting(startDate);
			
			while (nextMeeting.before(lastMeeting)) {
				// keep only working day meetings  
				if(Time.valueOf(extractTime(nextMeeting)).before(Time.valueOf(LAST_MEETING))
						&& Time.valueOf(extractTime(nextMeeting)).after(Time.valueOf(FIRST_MEETING))){
				CalendarEventEntry entry = new CalendarEventEntry();
				entry.setId(nextMeeting.toString());
		        When entryTimes = new When();
		        entryTimes.setStartTime(new DateTime(nextMeeting,Calendar.getInstance().getTimeZone()));
		        entryTimes.setEndTime(new DateTime(getNextMeeting(nextMeeting),Calendar.getInstance().getTimeZone()));
		        entry.addTime(entryTimes);
				entries.add(entry);
				}
				nextMeeting = getNextMeeting(nextMeeting);
			}
			return entries;
	}

	/*
	 * private utility methods
	 * TODO improve filters
	 */
	
	private Date getNextMeeting(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.clear(Calendar.SECOND);
		calendar.clear(Calendar.MINUTE);
		calendar.roll(Calendar.HOUR_OF_DAY, true);
		if(calendar.get(Calendar.HOUR_OF_DAY) == Calendar.getInstance().getMaximum(Calendar.HOUR_OF_DAY))
			calendar.roll(Calendar.DAY_OF_MONTH, true);
		return calendar.getTime();
	}
	
	private Date getLastMeeting(Date now,int field,int number) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
	    calendar.add(field,number);
		return calendar.getTime();
	}
	
	private String extractTime(Date date){
		// TODO remove this ugly substring
		return date.toString().substring(11, 19);
	}
	
}
