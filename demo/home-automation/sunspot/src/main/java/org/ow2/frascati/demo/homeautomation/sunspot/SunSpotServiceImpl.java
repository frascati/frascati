/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s): 
 */

package org.ow2.frascati.demo.homeautomation.sunspot;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.demo.homeautomation.api.IMediator;
import org.ow2.frascati.demo.homeautomation.api.SunSpotService;

/**
 * Implementation of {@link SunSpotService} interface
 * 
 * @author Mahmoud Ben Hassine
 *
 */
public class SunSpotServiceImpl implements SunSpotService {

	@Reference
	private IMediator mediator;
	
	public void init() {
		//TODO add initialization code
		Logger.getLogger(SunSpotServiceImpl.class.getName()).log(Level.INFO," [SunSpot service] SunSpot service initialized successfully");
	}

	public void notifyAccelerometerVariation() {
		Logger.getLogger(SunSpotServiceImpl.class.getName()).log(Level.INFO," [SunSpot service] accelerometer variation detected");
		mediator.sendSMS(mediator.getNursePhoneNumber(), "fall detected for patient '" + mediator.getPatientName() + "' [id=" + mediator.getPatientId() + "]");
	}

}
