/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.qos;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.wildcat.Context;
import org.ow2.wildcat.ContextException;
import org.ow2.wildcat.ContextFactory;

/**
 * Implementation of the {@link MonitoringService} interface
 * 
 * @author Mahmoud Ben Hassine
 * 
 */
@Scope("COMPOSITE")
public class MonitoringServiceImpl implements MonitoringService{
	
	/**
	 * reference to wildcat service used to create hierarchy, queries and listeners 
	 */
	@Reference
	private Context wildcatContext;
	
	@Init
	public void init(){
		
		/*
		 * create a wildcat context
		 */
		wildcatContext = ContextFactory.getDefaultFactory().createContext();
		
		PersistEventAction pea = new PersistEventAction();
		String updateQuery = "select * from WAttributeEvent(source = 'self://demoHA/services#lastMonitoringData')";
		
		try {
			/*
			 * Create wildcat resources hierarchy
			 */
			for (DemoServices service : DemoServices.values()) 							
				wildcatContext.createAttribute("self://demoHA/services/" + service + "#responseTime",0);

			/*
			 * add a basic attribute to get last monitoring data
			 */
			wildcatContext.createAttribute("self://demoHA/services#lastMonitoringData",new MonitoringData());
			
			/*
			 * register a new action to persist events
			 */
			wildcatContext.registerActions(updateQuery, pea);

		} catch (ContextException e) {
			e.printStackTrace();
		}
	}

	public void notifyMonitoringData(MonitoringData md) throws ContextException {
		wildcatContext.setValue("self://demoHA/services#lastMonitoringData", md);
		wildcatContext.setValue("self://demoHA/services/" + md.getServiceName() + "#responseTime", md.getResponseTime());		
	}
	
}
