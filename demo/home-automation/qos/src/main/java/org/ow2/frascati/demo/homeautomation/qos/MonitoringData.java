/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.qos;

/**
 * A simple bean encapsulating monitoring data to be notified to wildcat context
 * 
 * @author Mahmoud Ben Hassine
 * 
 */
public class MonitoringData {

	private String componentName;
	private String serviceName;
	private long responseTime;
	
	public MonitoringData(long responseTime, String componentName, String serviceName) {
		this.responseTime = responseTime;
		this.componentName = componentName;
		this.serviceName = serviceName;
	}

	public MonitoringData() {}

	public long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	@Override
	public String toString() {
		return "Monitoring data : date = " + java.util.Calendar.getInstance().getTime()
		+ " - component name = " + componentName
		+ " - service called = " + serviceName  
		+ " - response time = " + responseTime + " ms";
	}
	
}
