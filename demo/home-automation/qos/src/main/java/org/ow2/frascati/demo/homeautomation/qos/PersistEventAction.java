/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.qos;

import java.sql.Timestamp;

import org.ow2.wildcat.WAction;

import fr.inria.galaxy.monitoring.BEP4galaxy;
import fr.inria.galaxy.monitoring.event.FraSCAtiServiceEvent;

/**
 * WAction to persist monitoring data to the monitoring framework database
 * 
 * @author Mahmoud Ben Hassine
 *
 */
public class PersistEventAction extends WAction {
	
	@Override
	public void onEvent() {
		
		/*
		 * get encapsulated monitoring data
		 */
		MonitoringData md = (MonitoringData) getListener().getNewEvents()[0].get("value");
		
		BEP4galaxy.getInstance().eventArrived(
		new FraSCAtiServiceEvent(new Timestamp(System.currentTimeMillis()), "-1",md.getResponseTime(),
				md.getComponentName() + "/" + md.getServiceName() ));
	}

}
