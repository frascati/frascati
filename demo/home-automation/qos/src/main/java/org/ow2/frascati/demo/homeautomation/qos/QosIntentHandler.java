/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.qos;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.tinfi.control.intent.IntentHandler;
import org.ow2.frascati.tinfi.control.intent.IntentJoinPoint;

/**
 * Intent used to notify monitoring data to wildcat context for each service invoked
 * 
 * @author Mahmoud Ben Hassine
 * 
 */
public class QosIntentHandler implements IntentHandler {

	private long startTime;
	private long estimatedTime;

	/**
	 * reference to the QoS monitoring service
	 */
	@Reference
	private MonitoringService monitoringDataNotifier;
	
	/**
	 * Monitoring data to be notified to wildcat context
	 */
	private MonitoringData monitoringData;

	@Init
	public void init() {
		monitoringData = new MonitoringData();
	}
	
	public Object invoke(IntentJoinPoint ijp) throws Throwable {

		Component c = ((Interface) ijp.getComponentContext()).getFcItfOwner();
		String componentName = Fractal.getNameController(c).getFcName();
		String serviceName = ijp.getInterface().getFcItfName();

		Object ret = null;

		startTime = System.currentTimeMillis();

		ret = ijp.proceed();

		estimatedTime = System.currentTimeMillis() - startTime;

		/*
		 * Set monitoring data to be notified
		 */
		monitoringData.setComponentName(componentName);
		monitoringData.setServiceName(serviceName);
		monitoringData.setResponseTime(estimatedTime);

		/*
		 * Notify monitoring data to wildcat context
		 */
		monitoringDataNotifier.notifyMonitoringData(monitoringData);
		
		return ret;
	}

}
