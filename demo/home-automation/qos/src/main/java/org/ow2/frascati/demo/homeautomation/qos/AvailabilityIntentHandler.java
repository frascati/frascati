/***
 * OW2 FraSCAti: Home Automation demonstration
 * Copyright (C) 2010 INRIA, IRISA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author(s): Grégory Nain, Mahmoud Ben Hassine
 *
 * Contributor(s):
 */

package org.ow2.frascati.demo.homeautomation.qos;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.ServiceUnavailableException;
import org.ow2.frascati.tinfi.control.intent.IntentHandler;
import org.ow2.frascati.tinfi.control.intent.IntentJoinPoint;

@Scope("COMPOSITE")
@Service(IntentHandler.class)
public class AvailabilityIntentHandler implements IntentHandler {

    // Default timeout if not set.
    private static final long DEFAULT_TIMEOUT = 2000;

    // Timeout for the proceed thread.
    private long timeout = DEFAULT_TIMEOUT;

    /**
     * Setter for the timeout property.
     * @param timeout
     */
    @Property
        public void setTimeout(final long timeout) {
                System.out.println(this + ".setTimeout(" + timeout + ")");
                this.timeout = timeout;
    }

        @Override
    public Object invoke(final IntentJoinPoint ijp) throws ServiceUnavailableException, Throwable {

        // run the proceed in a separate thread.
        ProceedThread pt = new ProceedThread();
        // set the IntentJoinPoint to proceed.
        pt.ijp = ijp;
        // start the proceed thread.
        pt.start();

        try {
            // wait the proceed thread to return for maximum timeout value.
            pt.join(timeout);
            // timeout exceeded and the thread didn't returned yet.
            if (pt.isAlive()) {
                // don't interrupt the proceed thread because we don't know in which state it is.
                throw new ServiceUnavailableException("Timeout of " + timeout + " ms exceeded");
            }
        } catch (InterruptedException e) {
        }

        // throw the exception caught by the proceed thread.
        if(pt.throwable != null)
          throw pt.throwable;

       // the thread returned before timeout => return the value.
        return pt.result;
    }
}

class ProceedThread extends Thread {
    // The intent join point to proceed.
    IntentJoinPoint ijp = null;

    // The result returned by proceed().
    Object result = null;

    // Exception thrown by proceed().
    Throwable throwable = null;

    public void run() {
        try {
            result = ijp.proceed();
        } catch (Throwable e) {
            throwable = e;
        }
    }
}
