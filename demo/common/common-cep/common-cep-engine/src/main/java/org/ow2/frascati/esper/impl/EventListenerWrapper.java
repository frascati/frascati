/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.esper.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ow2.frascati.esper.api.EventListener;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.api.bean.JAXBBean;
import org.ow2.frascati.esper.api.bean.POJOBean;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.StatementAwareUpdateListener;
import com.espertech.esper.event.bean.BeanEventBean;
import com.espertech.esper.event.map.MapEventBean;

/**
 *
 */
public class EventListenerWrapper implements StatementAwareUpdateListener
{
    private EventListener listener;
    
    public EventListenerWrapper(EventListener listener)
    {
        this.listener=listener;
    }
    
    /* (non-Javadoc)
     * @see com.espertech.esper.client.StatementAwareUpdateListener#update(com.espertech.esper.client.EventBean[], com.espertech.esper.client.EventBean[], com.espertech.esper.client.EPStatement, com.espertech.esper.client.EPServiceProvider)
     */
    public void update(EventBean[] newBeans, EventBean[] oldBeans, EPStatement statement, EPServiceProvider provider)
    {
        String eventID=statement.getName();
//        System.out.println("EventID : "+eventID);
        List<CEPBean> cepBeans = new ArrayList<CEPBean>();
        CEPBean cepBean;
        for(EventBean bean : newBeans)
        {
            if(bean instanceof MapEventBean)
            {
                Map<String,Object> map=((MapEventBean) bean).getProperties();
//                System.out.println("MapEventBean");
                for(String s : map.keySet())
                {
                    BeanEventBean wrappedBean=(BeanEventBean) map.get(s);
//                    System.out.println(s+ " "+wrappedBean.getClass().getName()+" "+wrappedBean);
                    cepBean=new POJOBean(wrappedBean.getUnderlying());
                    cepBeans.add(cepBean);
                }
            }
            else
            {
//                System.out.println(bean.getUnderlying().getClass().getName()+"---------------------------");
                cepBean=new POJOBean(bean.getUnderlying());
                cepBeans.add(cepBean);
            }
        }
        
        listener.update(eventID, cepBeans);
    }
    
}
