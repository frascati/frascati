/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.esper.impl;

import java.util.List;

import org.osoa.sca.annotations.Property;
import org.ow2.frascati.esper.StatementIDs;
import org.ow2.frascati.esper.api.EventListener;

/**
 *
 */
public abstract class EventListenerAdaptor implements EventListener
{
    protected List<String> statementIDList;
    
    @Property(name = "statementIDs")
    public void setEvents(StatementIDs statementIDs)
    {
        this.statementIDList=statementIDs.getStatementIDs();
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#getEventIDs()
     */
    public List<String> getEventIDs()
    {
        return statementIDList;
    }
}
