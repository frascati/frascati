/**
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact:
 *
 * Author(s): Damien Fournier
 *
 * Contributor(s) : Fernand PARAÏSO
 *
 */

package org.ow2.frascati.esper.impl;

import org.oasisopen.sca.annotation.Service;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.Statement;

@Service(Statement.class)
@Scope("COMPOSITE")
@EagerInit
public class StatementImpl implements Statement
{

    private String statement;

    private String eventID;

    public StatementImpl(){}
    
    public StatementImpl(String eventID, String statement)
    {
        this.eventID=eventID;
        this.statement=statement;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.esper.api.Statement#getStatement()
     */
    public String getEventID()
    {
        return eventID;
    }

    @Property
    public void setEventID(String eventID)
    {
        this.eventID = eventID;
    }

    public String getStatement()
    {
        return statement;
    }

    @Property
    public void setStatement(String statement)
    {
        this.statement = statement;
    }

    public boolean equals(Object object)
    {
        if (!(object instanceof Statement))
            return false;
        Statement statement = (Statement) object;
        return this.eventID.equals(statement.getEventID());
    }
}