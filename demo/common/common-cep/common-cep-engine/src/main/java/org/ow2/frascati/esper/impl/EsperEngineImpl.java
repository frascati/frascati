/**
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact:
 *
 * Author(s): Damien Fournier
 *
 * Contributor(s) : Fernand PARAÏSO
 *
 */
package org.ow2.frascati.esper.impl;

import static com.espertech.esper.client.EPServiceProviderManager.getProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.oasisopen.sca.annotation.Service;
import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.EventAttType;
import org.ow2.frascati.esper.EventType;
import org.ow2.frascati.esper.Events;
import org.ow2.frascati.esper.api.EsperEngine;
import org.ow2.frascati.esper.api.EventListener;
import org.ow2.frascati.esper.api.Statement;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPAdministrator;
import com.espertech.esper.client.EPException;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPStatement;

/**
 * SCA component implementation for the Esper Engine
 */
@Service(EsperEngine.class)
@Scope("COMPOSITE")
public class EsperEngineImpl implements EsperEngine
{
    private static final String DOM_IMPLEMENTATION_REGISTRY = "com.sun.org.apache.xerces.internal.dom.DOMXSImplementationSourceImpl";

    @Reference(name = "statements", required = false)
    private List<Statement> statements;

    @Reference(name = "listeners", required = false)
    private List<EventListener> listeners;

    @ComponentName
    protected String componentName;

    private String logHeader = "[ Esper Engine ] ";

    private String engineLogHeader;

    private Configuration esperEngineConfig = new Configuration();

    protected EPServiceProvider esperEngine;

    private Map<String, List<EventListener>> listenersMap;

    /**
     * Initialize Esper Engine init() method is invoked when the service is
     * accessed for the first time
     */

    @Init
    public void init()
    {
        // //load esp configuration this defines the XML event types to be
        System.setProperty(DOMImplementationRegistry.PROPERTY, EsperEngineImpl.DOM_IMPLEMENTATION_REGISTRY);
        this.engineLogHeader = "[ Engine : " + componentName + "] ";
        System.out.println(engineLogHeader + "Init EsperEngine");
        listenersMap = new HashMap<String, List<EventListener>>();
        for (EventListener listener : listeners)
        {
            List<EventListener> eventListenerList;
            for (String eventID : listener.getEventIDs())
            {
                if (!listenersMap.containsKey(eventID))
                {
                    List<EventListener> newEventListenerList = new LinkedList<EventListener>();
                    newEventListenerList.add(listener);
                    listenersMap.put(eventID, newEventListenerList);
                } else
                {
                    eventListenerList = listenersMap.get(eventID);
                    eventListenerList.add(listener);
                }
            }
        }
        startEngine();
    }

    private void startEngine()
    {
        this.esperEngine = getProvider(this.componentName, this.esperEngineConfig);
        System.out.println(engineLogHeader + " engine started, start registring  " + statements.size() + " statements");
        registerStatements();
    }

    private void restartEngine()
    {
        if (this.esperEngine != null)
        {
            this.esperEngine.destroy();
            startEngine();
        } else
        {
            System.out.println(logHeader + " Try to reStart an null engine");
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.esper.api.EsperEngine#sendEvent(java.lang.Object)
     */
    public void sendEvent(CEPBean eventBean)
    {
        if (eventBean != null)
        {
            Object bean = eventBean.getUnderlyingBean();
            if (bean != null)
            {
                this.esperEngine.getEPRuntime().sendEvent(bean);
                System.out.println(engineLogHeader + "Event " + bean.getClass() + " sent");
                return;
            }
        }
        System.out.println(engineLogHeader + "Can't send eventBean");
    }

    public void sendAsynchronousEvent(final CEPBean eventBean)
    {
         new Thread(new Runnable()
        {
            
            public void run()
            {
                sendEvent(eventBean);
            }
        }).start();
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * org.ow2.frascati.esper.api.EsperEngine#addStatement(java.lang.String,
     * java.lang.String)
     */
    public boolean addStatement(String eventID, String statement)
    {
        Statement statementImpl = new StatementImpl(eventID, statement);
        boolean isRegistered = registerStatement(statementImpl);
        if (isRegistered && !listenersMap.containsKey(eventID))
        {
            List<EventListener> newListener = new LinkedList<EventListener>();
            listenersMap.put(eventID, newListener);
            return true;
        } else
        {
            System.out.println(engineLogHeader + "Can't add statement : Statement with id " + eventID + " already bound");
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.ow2.frascati.esper.api.EsperEngine#updateStatement(java.lang.String,
     * java.lang.String)
     */
    public boolean updateStatement(String eventID, String statement)
    {
        boolean isUnregistered = unRegisterStatement(eventID);
        if (isUnregistered)
        {
            Statement newStatement = new StatementImpl(eventID, statement);
            System.out.println(engineLogHeader + "Update statement, eventId : " + eventID + " , new statement : " + statement);
            return registerStatement(newStatement);

        } else
        {
            System.out.println(engineLogHeader + "Can't update statement : No statement found for eventId : " + eventID);
            return false;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.ow2.frascati.esper.api.EsperEngine#deleteStatement(java.lang.String)
     */
    public boolean deleteStatement(String eventID)
    {
        boolean isUnregistered = unRegisterStatement(eventID);
        if (isUnregistered)
        {
            System.out.println(engineLogHeader + "Delete statement for eventID : " + eventID);
            if (listenersMap.containsKey(eventID))
            {
                listenersMap.remove(eventID);
            }
            return true;
        } else
        {
            System.out.println(engineLogHeader + "Can't delete statement : No statement found for eventID : " + eventID);
            return false;
        }
    }

    private void registerStatements()
    {
        List<Statement> errorStatements = new ArrayList<Statement>();

        for (Statement stmt : this.statements)
        {
            try
            {
                registerStatement(stmt);
            } catch (EPException e)
            {
                errorStatements.add(stmt);
            }
        }
        // Register statement components not registered by first pass
        for (Statement stmt : errorStatements)
        {
            try
            {
                registerStatement(stmt);
            } catch (EPException e)
            {
                System.out.println(engineLogHeader + "Error while registering query");
            }
            System.out.println(engineLogHeader + "Second pass successfully register missing statements");
        }
    }

    private boolean registerStatement(Statement statement)
    {
        String eventID = statement.getEventID();
        EPAdministrator epAdministator = this.esperEngine.getEPAdministrator();
        EPStatement epStatement = epAdministator.getStatement(eventID);

        if (epStatement == null)
        {
            String statementString = statement.getStatement();
            try
            {
                epStatement = epAdministator.createEPL(statementString, eventID);
            } catch (EPException e)
            {
                System.out.println(this.engineLogHeader + "Cannot create statement for eventID : " + statement.getEventID());
                throw new EPException(this.engineLogHeader + "Cannot create statement for eventID : " + statement.getEventID(), e);
            }
            System.out.println(engineLogHeader + "Create Statement for eventID : " + statement.getEventID() + ", query : " + statement.getStatement());

            List<EventListener> eventListeners = listenersMap.get(eventID);
            if (eventListeners != null)
            {
                EventListenerWrapper eventWrapper;
                for (EventListener eventListener : eventListeners)
                {
                    eventWrapper = new EventListenerWrapper(eventListener);
                    epStatement.addListener(eventWrapper);
                    System.out.println(engineLogHeader + "Add Listener " + eventListener.getClass());
                }
            }
            return true;
        } else
        {
            System.out.println(this.engineLogHeader + "Try to register a statement with an already existing id :" + eventID);
            return false;
        }
    }

    public boolean addListener(String eventID, EventListener eventListener)
    {
        EPAdministrator epAdministator = this.esperEngine.getEPAdministrator();
        EPStatement epStatement = epAdministator.getStatement(eventID);
        if(epStatement!=null)
        {
            EventListenerWrapper eventWrapper = new EventListenerWrapper(eventListener);
            epStatement.addListener(eventWrapper);
            return true;
        }
        System.out.println(engineLogHeader+"Cannot find statement for id "+eventID);
        return false;
    }

    private boolean unRegisterStatement(String eventId)
    {
        EPStatement epStatement = this.esperEngine.getEPAdministrator().getStatement(eventId);
        if (epStatement != null)
        {
            epStatement.destroy();
            System.out.println(this.engineLogHeader + "Unregister statement id: " + eventId + " from Esper Engine");
            return true;
        } else
        {
            System.out.println(this.engineLogHeader + "Try to unregister a non existing statement id :" + eventId);
            return false;
        }
    }

    @Property(name = "events")
    public void setEvents(Events events)
    {
        EventAttType eventAttType;
        for (EventType event : events.getEvents())
        {
            eventAttType = event.getEventType();
            if (eventAttType == null || eventAttType == EventAttType.CLASS)
            {
                setClassEvent(event.getValue());
            } else
            {
                System.out.println(logHeader + "The event attribute type " + eventAttType + " is not supported by this engine");
            }
        }
        // Configuration change restart the esper engine
        restartEngine();
    }

    /**
     * Load events defined
     * 
     * @param classname
     */
    private void setClassEvent(String classname)
    {
        try
        {
            // Load class using its package and name
            Class<?> clazz = Class.forName(classname);
            // Import event class into esper configuration
            this.esperEngineConfig.addImport(classname);
            // Add event type, Event are registered according to the class name
            this.esperEngineConfig.addEventType(clazz.getSimpleName(), clazz);
            System.out.println(logHeader + "Event '" + clazz.getSimpleName() + "' registered");
        } catch (ClassNotFoundException e)
        {
            System.out.println(logHeader + "Could not find Class for event type : " + classname);
            e.printStackTrace();
        }
    }
}
