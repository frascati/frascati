/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.esper.listener;

import java.util.List;

import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.bean.TestBean1;
import org.ow2.frascati.esper.impl.EventListenerAdaptor;

/**
 *
 */
public class TestListener1 extends EventListenerAdaptor implements TestListenerItf
{
    private TestBean1 currentTestBean;

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.listener.TestBeanItf#getCurrentTestBeanID()
     */
    public String getCurrentTestBeanID()
    {
        if(currentTestBean==null)       return "";
        return currentTestBean.getId();
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.listener.TestBeanItf#getCurrentTestBeanValue()
     */
    public int getCurrentTestBeanValue()
    {
        if(currentTestBean==null)       return 0;
        return currentTestBean.getValue();
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#update(java.lang.String, java.util.Collection)
     */
    public void update(String eventID, List<CEPBean> beans)
    {
        if(eventID=="EventID1")
        {
            CEPBean eventBean=beans.get(0);
            currentTestBean=(TestBean1) eventBean.getUnderlyingBean();
        }
    }
}
