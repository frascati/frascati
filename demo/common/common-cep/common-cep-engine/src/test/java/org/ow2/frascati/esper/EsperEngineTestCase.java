/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.esper;

import org.junit.Test;
import org.ow2.frascati.esper.api.EsperEngine;
import org.ow2.frascati.esper.api.bean.JAXBBean;
import org.ow2.frascati.esper.api.bean.POJOBean;
import org.ow2.frascati.esper.bean.TestBean1;
import org.ow2.frascati.esper.bean.TestBean2;
import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 *
 */
public class EsperEngineTestCase extends FraSCAtiTestCase
{    
    @Test
    public void test()
    {
        EsperEngine cepEngine=getService(EsperEngine.class, "cep-engine-service");
        TestBean1 testBean1=new TestBean1("testBean1",10);
        cepEngine.sendEvent(new JAXBBean(testBean1));
        
        TestBean2 testBean2=new TestBean2("testBean2",10);
        cepEngine.sendEvent(new POJOBean(testBean2));
        
        String eventID="runtimeTest";
        String statementValue="select * from TestBean1 where value > 50";
        cepEngine.addStatement(eventID, statementValue);
        
        testBean1=new TestBean1("testBean1",15);
        cepEngine.sendEvent(new JAXBBean(testBean1));
        testBean1=new TestBean1("testBean1",70);
        cepEngine.sendEvent(new JAXBBean(testBean1));
        
        statementValue="select * from TestBean1 where value < 20";
        cepEngine.updateStatement(eventID, statementValue);
        
        testBean1=new TestBean1("testBean1",15);
        cepEngine.sendEvent(new JAXBBean(testBean1));
        testBean1=new TestBean1("testBean1",70);
        cepEngine.sendEvent(new JAXBBean(testBean1));
        
        cepEngine.deleteStatement(eventID);
    }
}
