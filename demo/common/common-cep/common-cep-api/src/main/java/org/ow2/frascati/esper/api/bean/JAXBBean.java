/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.esper.api.bean;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.stream.XMLStreamReader;

import org.codehaus.jettison.json.JSONObject;
import org.codehaus.jettison.mapped.Configuration;
import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamReader;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;

/**
 *
 */
@XmlRootElement(name="jaxbBean")
@XmlAccessorType(XmlAccessType.NONE)
public class JAXBBean extends CEPBean
{

   public JAXBBean()
   {
       super();
   }
    
   public JAXBBean(Object bean)
   {
       super(bean);
   }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.examples.counter.bean.CEPBean#getStringRepresentation(java.lang.Object)
     */
    public String getStringRepresentation(Object bean)
    {
        try
      {
          JAXBContext jaxbContext=JAXBContext.newInstance(bean.getClass());
          Marshaller marshaller=jaxbContext.createMarshaller();
          Configuration config=new Configuration();
          MappedNamespaceConvention con = new MappedNamespaceConvention(config);
          StringWriter stringWriter = new StringWriter();
          MappedXMLStreamWriter xmlStreamWriter = new MappedXMLStreamWriter(con, stringWriter);
          marshaller.marshal(bean, xmlStreamWriter);
          StringBuffer stringBuffer=stringWriter.getBuffer();
          return stringBuffer.toString();
      }
      catch(Exception e)
      {
          e.printStackTrace();
          return "";
      }
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.examples.counter.bean.CEPBean#getUnderlyingBean()
     */
    public Object getUnderlyingBean()
    {
        try
        {
            Class<?> clazz = Class.forName(this.clazz);
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            JSONObject jsonObject = new JSONObject(this.stringRepresentation);
            XMLStreamReader xmlStreamReader = new MappedXMLStreamReader(jsonObject);
            Object toReturn =unmarshaller.unmarshal(xmlStreamReader);
            return toReturn;
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
}
