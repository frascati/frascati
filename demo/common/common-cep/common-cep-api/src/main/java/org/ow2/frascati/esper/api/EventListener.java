/**
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact:
 *
 * Author(s): Damien Fournier
 *
 * Contributor(s) : 
 *
 */

package org.ow2.frascati.esper.api;

import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.esper.api.bean.CEPBean;

/**
 * Abstract class for Listener Components
 */
@Service(EventListener.class)
public interface EventListener
{
    @GET
    @Path("/events/{id:.*}")
    @Produces({"application/xml", "application/json"})
    public Collection<String> getEventIDs();
    
    @POST
    @Path("/update/{id:.*}")
    @Consumes("application/x-www-form-urlencoded")
    public void update(@PathParam("id") String eventID, List<CEPBean> beans);
   
}
