/**
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact:
 *
 * Author(s): Damien Fournier
 *
 * Contributor(s) : Fernand PARAISO
 *
 */

package org.ow2.frascati.esper.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

import org.osoa.sca.annotations.Service;
import org.ow2.frascati.esper.api.bean.CEPBean;

/**
 * SCA service interface for an Esper Engine as SCA component
 */
@Service
public interface EsperEngine
{
    @POST
    @Path("/statements/{statementId}/{statement}")
    public boolean addStatement(@PathParam("statementId") String statementId, @PathParam("statement")String statement);

    @PUT
    @Path("/statements/{statementId}/{statement}")
    public boolean updateStatement(@PathParam("statementId") String statementId, @PathParam("statement") String statement);

    @DELETE
    @Path("/statements/{statementId}")
    public boolean deleteStatement(@PathParam("statementId") String statementId);

    @POST
    @Path("/send")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public void sendEvent(CEPBean eventBean);
    
    @POST
    @Path("/sendAsync")
    @Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public void sendAsynchronousEvent(CEPBean eventBean);
    
    public boolean addListener(String eventID,  EventListener eventListener);
}
