/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.common.valuedevice;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JSpinner.NumberEditor;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;

import org.osoa.sca.annotations.Property;

/**
 * Implementation of the IValueDevice interface, this class display a Swing GUI that represent the device, configuration is done via SCA properties
 */
/**
 *
 */
public abstract class SwValueDevice extends JFrame implements IValueDevice
{
    private static final long serialVersionUID = 8711912671655826452L;

    /**
     * The title of the JFrame
     * 
     * @Property
     */
    protected String frameTitle;

    /**
     * The current value of the device
     * 
     * @Property
     */
    protected int currentValue=0;

    /**
     * The minimum value for the device
     * 
     * @Property
     */
    protected int minValue=0;
    
    /**
     * The maximum value for the device
     * 
     * @Property
     */
    protected int maxValue=100;
    
    /**
     * The step size between two values in the JSpinner
     * 
     * @Property
     */
    protected int stepSize=1;
    
    
    /**
     * The label that holds the current icon of the device
     */
    protected JLabel imageValueLabel;
    
    
    /**
     * The spinner that allow user to change the value of the device
     */
    protected JSpinner setValueSpinner;
    
    
    /**
     * The action button to set the value of the device
     */
    protected JButton setValueButton;
    
    
    /**
     * The icon display if no user icon is define for a value
     */
    protected ImageIcon defaultIcon;
    
    
    /**
     * Constructor, set title of the Frame and initalise the Swing components
     */
    protected SwValueDevice()
    {
        super();
        updateTitle();
        initComponents();
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.valuedevice.IValueDevice#dim(int)
     */
    public int setCurrentValue(int value)
    {
        setValue(value);
        setCurrentValueAction(value);
        return this.currentValue;
    }
    
    /**
     * Method call after graphical changes on the JFrame, allow inherited class to do specific actions
     * 
     * @param value : The current value of the device
     */
    public abstract void setCurrentValueAction(int value);
    
    /**
     * Create the different swing component, add listener to the button
     */
    private void initComponents()
    {               
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Dimension preferredSize=new Dimension(300,300);
        setPreferredSize(preferredSize);
        setResizable(false);
        
        defaultIcon=new ImageIcon(getClass().getClassLoader().getResource("images/default.png"));
        
        imageValueLabel = new JLabel();
        imageValueLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        ImageIcon icon=getImageIcon(currentValue);
        imageValueLabel.setIcon(icon);
        
        JPanel spinnerContainer=new JPanel();
        preferredSize=new Dimension(30,20);
        spinnerContainer.setPreferredSize(preferredSize);
        spinnerContainer.setAlignmentX(Component.CENTER_ALIGNMENT);
        SpinnerNumberModel model=new SpinnerNumberModel(currentValue,minValue,maxValue,stepSize);
        setValueSpinner=new JSpinner(model);
        NumberEditor numberEditor=(NumberEditor)setValueSpinner.getEditor();
        JFormattedTextField formattedTextField=numberEditor.getTextField();
        formattedTextField.setEditable(false);
        spinnerContainer.add(setValueSpinner);
        
        setValueButton = new JButton();
        setValueButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        setValueButton.setText("Set value");
        setValueButton.addActionListener(new ActionListener()
        {
                public void actionPerformed(ActionEvent evt)
                {
                        setValueActionPerformed();
                }
        });

        BoxLayout boxLayout=new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS);
        getContentPane().setLayout(boxLayout);
        getContentPane().add(imageValueLabel);
        getContentPane().add(spinnerContainer);
        getContentPane().add(setValueButton);

        pack();
        setVisible(true);
    }
    
    /**
     * Listener on the "set value" button
     */
    private void setValueActionPerformed()
    {
        Integer value=(Integer) setValueSpinner.getValue();
        setCurrentValue(value);
    }
    
    /**Utils for getter and setter**/
    
    private void updateSpinnerModel()
    {
        SpinnerModel model=new SpinnerNumberModel(currentValue,minValue,maxValue,stepSize);
        setValueSpinner.setModel(model);
    }
    
    private void updateTitle()
    {
        setTitle(this.frameTitle+", value: "+this.currentValue);
    }
    
    /**Properties getter and setter**/
    
    public String getFrameTitle()
    {
        return frameTitle;
    }
    
    @Property
    public void setFrameTitle(String title)
    {
        this.frameTitle = title;
        updateTitle();
    }
    
    public int getValue()
    {
        return currentValue;
    }

    /**
     * Set the value of the device, checks if the value is in the device value range (if not correct it with min and max values) then changes icon and title 
     * 
     * @param value
     */
    @Property
    public void setValue(int value)
    {
        if(value > maxValue)
        {
            this.currentValue=maxValue;
        }
        else if(value < minValue)
        {
            this.currentValue=minValue;
        }
        else
        {
            this.currentValue = value;
        }
        
        setValueSpinner.setValue(value);
        updateTitle();
        ImageIcon icon=this.getImageIcon(value);
        if(icon!=null)
        {
            this.imageValueLabel.setIcon(icon);
        }
        else
        {
            this.imageValueLabel.setIcon(defaultIcon);
        }
    }
    
    public int getMinValue()
    {
        return minValue;
    }

    @Property
    public void setMinValue(int minValue)
    {
        this.minValue = minValue;
        updateSpinnerModel();
    }

    public int getMaxValue()
    {
        return maxValue;
    }

    @Property
    public void setMaxValue(int maxValue)
    {
        this.maxValue = maxValue;
        updateSpinnerModel();
    }

    public int getStepSize()
    {
        return stepSize;
    }

    @Property
    public void setStepSize(int stepSize)
    {
        this.stepSize = stepSize;
        updateSpinnerModel();
    }
}
