/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.demo.common.valuedevice;

import javax.swing.ImageIcon;

/**
 * Interface for device that have a valued state like a video screen that can be open, close and in an intermediate state
 */
public interface IValueDevice
{
    /**
     * Set the Current Value of the device
     * 
     * @return Current value of the device
     */
    public int setCurrentValue(int value);
    
    /**
     * Get the icon to display depending on the value of the device
     * 
     * @param value : the value of the device
     * @return The Swing ImgaIcon
     */
    public ImageIcon getImageIcon(int value);
}
