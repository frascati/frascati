/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez 
 *
 *
 */

package org.ow2.frascati.demo.common.onoffdevice;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import javax.swing.border.Border;

import org.osoa.sca.annotations.Property;

/**
 * Abstract class providing a SwingUI with a panel composed off a image and a
 * button, image and text change depending on the status on the device
 */
public abstract class SwOnOffDevice extends JFrame implements IOnOffDevice,ActionListener
{

    private static final long serialVersionUID = 1L;

    /**
     *  @Property
     *  Title of the frame
     */
    protected String frameTitle = "No Title";
    
    /**
     * @Property Width of the Frame
     */
    protected String frameWidth ="500";

    /**
     * @Property height of the Frame
     */
    protected String frameHeight = "500";

    /**
     * @Property horizontal offset of the Frame starting from top-left corner
     */
    protected String frameXPosition = "500";

    /**
     * @Property vertical offset of the Frame starting from top-left corner
     */
    protected String frameYPosition = "0";

    /**
     * @Property Button's text to turn on the device
     */
    protected String onText = "on";

    /**
     * @Property Button's text to turn off the device
     */
    protected String offText = "off";

    /**
     * @Property Path to image shows when the device status is true
     */
    protected String onIconPath = "images/defaultOn.png";

    /**
     * @Property Path to image shows when the device status is false
     */
    protected String offIconPath = "images/defaultOff.png";

    /**
     * @Property Color of the frame background
     */
    protected String backgroundHexColor="0xFFFFFF";
    
    /**
     * The current status, default status is false
     */
    protected Boolean status = false;

    protected JLabel imageStatusLabel;
    protected JButton setStatusButton;
    protected ImageIcon onIcon, offIcon;

    /**
     * Create graphical components and set the title
     * 
     * @param frameTitle : title of the main JFrame
     */
    protected SwOnOffDevice()
    {
      buildGUI();
      
      this.pack();
      this.setVisible(true);
    }

    /**
     * Create graphical components and set the title and different properties
     * 
     * @param frameTitle title of the main JFrame
     * @param onText
     * @param offText
     * @param onIconPath
     * @param offIconPath
     */
    protected SwOnOffDevice(String frameTitle, String onText, String offText, String onIconPath, String offIconPath, int width, int height,int xPosition, int yPosition)
    {
        this.frameTitle=frameTitle;
        this.onText = onText;
        this.offText = offText;
        this.onIconPath = onIconPath;
        this.offIconPath = offIconPath;
        this.frameWidth=String.valueOf(width);
        this.frameHeight=String.valueOf(height);
        this.frameXPosition=String.valueOf(xPosition);
        this.frameYPosition=String.valueOf(yPosition);
        this.status = false;
        
        buildGUI();
        
        this.pack();
        this.setVisible(true);
    }

    private void updateLocation()
    {
        Integer xPosition=Integer.valueOf(this.frameXPosition);
        Integer yPosition=Integer.valueOf(this.frameYPosition);
        this.setLocation(xPosition, yPosition);
        this.repaint();
    }
    
    private void updateSize()
    {
        Integer width=Integer.valueOf(this.frameWidth);
        Integer height=Integer.valueOf(this.frameHeight);
        this.setSize(width, height);
        this.repaint();
    }
    
    private void buildGUI()
    {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle(frameTitle);
        this.setResizable(false);
        
        this.setBackgroundHexColor(this.backgroundHexColor);
        this.updateSize();
        this.updateLocation();

        Container contentPane=this.getContentPane();
        contentPane.setLayout(new GridBagLayout());

        onIcon=new ImageIcon(getClass().getClassLoader().getResource(onIconPath));
        offIcon=new ImageIcon(getClass().getClassLoader().getResource(offIconPath));
        
        GridBagConstraints imageConstraint=new GridBagConstraints();
        imageConstraint.fill=GridBagConstraints.BOTH;
        imageConstraint.gridwidth=8;
        imageConstraint.gridheight=10;
        imageConstraint.gridx=1;
        imageConstraint.gridy=1;
        imageConstraint.weightx=0.9;
        imageConstraint.weighty=0.9;
        imageStatusLabel = new JLabel(offIcon, JLabel.CENTER);
        contentPane.add(imageStatusLabel,imageConstraint);
        Border imageBorder=BorderFactory.createLineBorder(Color.BLACK);
        imageStatusLabel.setBorder(imageBorder);
        
        GridBagConstraints buttonConstraint=new GridBagConstraints();
        buttonConstraint.fill=GridBagConstraints.HORIZONTAL;
        buttonConstraint.anchor=GridBagConstraints.PAGE_END;
        buttonConstraint.gridwidth=8;
        buttonConstraint.gridheight=2;
        buttonConstraint.gridx=1;
        buttonConstraint.gridy=12;
        buttonConstraint.weightx=0.1;
        buttonConstraint.weighty=0.1;
        setStatusButton = new JButton();
        setStatusButton.setText(onText);
        setStatusButton.addActionListener(this);
        contentPane.add(setStatusButton, buttonConstraint);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.demo.common.onoffdevice.IOnOffDevice#off()
     * 
     * Implement off() method of IOnOffDevice, call after a user click to turn
     * on the device. Set the status true, set the button'text and the image
     * then call onAction() an abstract method containing the business code
     */
    public Boolean on()
    {
        status = true;
        setStatusButton.setText(offText);
        imageStatusLabel.setIcon(onIcon);
        repaint();
        return onAction();
    }

    /**
     * Contains the business code, call after updating the GUI
     * 
     * @return Status of the action performed
     */
    public abstract Boolean onAction();

    /*
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.demo.common.onoffdevice.IOnOffDevice#off()
     * 
     * Implement off() method of IOnOffDevice, call after a user click to turn
     * off the device. Set the status false, set the button'text and the image
     * then call offAction() an abstract method containing the business code
     */
    public Boolean off()
    {
        status = false;
        setStatusButton.setText(onText);
        imageStatusLabel.setIcon(offIcon);
        repaint();
        return offAction();
    }

    /**
     * Contains the business code updating the GUI
     * 
     * @return Status of the action performed
     */
    public abstract Boolean offAction();

    /*
     * (non-Javadoc)
     * 
     * @see org.ow2.frascati.demo.common.onoffdevice.IOnOffDevice#getStatus()
     * 
     * Implement the getStatus() method from IOnOffDevice interface
     * 
     * @Return the current status off the device, true=on, false=off
     */
    public Boolean getStatus()
    {
        return status;
    }

    /**
     * Listener on the status switcher button, call the method from the
     * implemented interface
     * 
     */
    public void actionPerformed(ActionEvent e)
    {
        if (status)
            off();
        else
            on();
    }

    public String getFrameTitle()
    {
        return frameTitle;
    }

    @Property
    public void setFrameTitle(String frameTitle)
    {
        this.frameTitle = frameTitle;
        this.setTitle(this.frameTitle);
        this.repaint();
    }
    
    protected String getOnText()
    {
        return onText;
    }

    /**
     * Setter of the onText property update the GUI after setting the onText
     * field
     * 
     * @param onText text display on the button to turn on the device
     */
    @Property
    protected void setOnText(String onText)
    {
        this.onText = onText;
        if (!status)
            setStatusButton.setText(onText);
    }

    protected String getOffText()
    {
        return offText;
    }

    /**
     * Setter of the offText property update the GUI after setting the offText
     * field
     * 
     * @param offText text display on the button to turn off the device
     */
    @Property
    protected void setOffText(String offText)
    {
        this.offText = offText;
        if (status)
            setStatusButton.setText(offText);
    }

    protected String getOnIconPath()
    {
        return onIconPath;
    }

    /**
     * Setter of the onIconPath property update the GUI after setting the
     * onIconPath field
     * 
     * @param onIconPath Path to the image displayed when the device is on
     */
    @Property
    protected void setOnIconPath(String onIconPath)
    {
        this.onIconPath = onIconPath;
        onIcon = new ImageIcon(getClass().getClassLoader().getResource(onIconPath));
        if (status)
            imageStatusLabel.setIcon(onIcon);

    }

    protected String getOffIconPath()
    {
        return offIconPath;
    }

    /**
     * Setter of the offIconPath property update the GUI after setting the
     * offIconPath field
     * 
     * @param offIconPath Path to the image displayed when the device is off
     */
    @Property
    protected void setOffIconPath(String offIconPath)
    {
        this.offIconPath = offIconPath;
        offIcon = new ImageIcon(getClass().getClassLoader().getResource(offIconPath));
        if (!status)
            imageStatusLabel.setIcon(offIcon);
    }

    public String getFrameWidth()
    {
        return frameWidth;
    }

    @Property
    public void setFrameWidth(String frameWidth)
    {
        this.frameWidth = frameWidth;
        updateSize();
    }

    public String getFrameHeight()
    {
        return frameHeight;
    }

    @Property
    public void setFrameHeight(String frameHeight)
    {
        this.frameHeight = frameHeight;
        updateSize();
    }

    public String getFrameXPosition()
    {
        return frameXPosition;
    }

    @Property
    public void setFrameXPosition(String frameXPosition)
    {
        this.frameXPosition = frameXPosition;
        updateLocation();
    }

    public String getFrameYPosition()
    {
        return frameYPosition;
    }

    @Property
    public void setFrameYPosition(String frameYPosition)
    {
        this.frameYPosition = frameYPosition;
        updateLocation();
    }

    public String getBackgroundHexColor()
    {
        return backgroundHexColor;
    }

    @Property
    public void setBackgroundHexColor(String backgroundHexColor)
    {
        Color backgroundColor;
        try
        {
            backgroundColor=Color.decode(backgroundHexColor);
            this.backgroundHexColor = backgroundHexColor;
            
        } catch (Exception e)
        {
            backgroundColor=Color.WHITE;
            this.backgroundHexColor="0xFFFFFF";
        }
        if(this.getContentPane()!=null)
        {
            this.getContentPane().setBackground(backgroundColor);
        }
    }
}
