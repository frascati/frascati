/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez 
 *
 * Contributor(s):  
 *
 */

package org.ow2.frascati.demo.common.onoffdevice;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;


/**
 * 
 * Common interface for component with two states as doors, sprinklers, fireDectetor
 * 
 */
public interface IOnOffDevice
{
	/**
	 * Operation call when the device turn on 
	 * 
	 * @return Status of the action performed
	 */
        @POST
        @Path("/on")
	public Boolean on();
	
	/**
	 * Operation call when the device turn off
	 * 
	 * @return Status of the action performed
	 */
        @POST
        @Path("/off")
	public Boolean off();
	
	
	/**
	 * Status of the device, on=true , off=false
	 * 
	 * @return The current status of the device
	 */
        @GET
	public Boolean getStatus();
}
