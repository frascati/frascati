
package org.oasis_open.docs.wsn.bw_2;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.4.2
 * 2012-07-25T12:55:13.998+02:00
 * Generated source version: 2.4.2
 */

@WebFault(name = "UnableToGetMessagesFault", targetNamespace = "http://docs.oasis-open.org/wsn/b-2")
public class UnableToGetMessagesFault extends Exception {
    
    private org.oasis_open.docs.wsn.b_2.UnableToGetMessagesFaultType unableToGetMessagesFault;

    public UnableToGetMessagesFault() {
        super();
    }
    
    public UnableToGetMessagesFault(String message) {
        super(message);
    }
    
    public UnableToGetMessagesFault(String message, Throwable cause) {
        super(message, cause);
    }

    public UnableToGetMessagesFault(String message, org.oasis_open.docs.wsn.b_2.UnableToGetMessagesFaultType unableToGetMessagesFault) {
        super(message);
        this.unableToGetMessagesFault = unableToGetMessagesFault;
    }

    public UnableToGetMessagesFault(String message, org.oasis_open.docs.wsn.b_2.UnableToGetMessagesFaultType unableToGetMessagesFault, Throwable cause) {
        super(message, cause);
        this.unableToGetMessagesFault = unableToGetMessagesFault;
    }

    public org.oasis_open.docs.wsn.b_2.UnableToGetMessagesFaultType getFaultInfo() {
        return this.unableToGetMessagesFault;
    }
}
