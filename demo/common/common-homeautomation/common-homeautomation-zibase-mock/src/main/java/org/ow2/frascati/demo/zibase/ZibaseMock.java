/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 *
 */


package org.ow2.frascati.demo.zibase;

import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.zibase.ZibasePortType;

/**
 * Zibase mock class, use to simulate a zibase for testing
 */
@EagerInit
@Scope("COMPOSITE")
public class ZibaseMock implements ZibasePortType
{

    /**
     * @Property
     * ID for Zibase authentication
     */
    private String zibaseID;
    
    /**
     * @Property
     * token for Zibase authentication
     */
    private String zibaseToken;
    
    /**
     * Logger
     */
    public final static Logger LOG = Logger.getLogger(ZibaseMock.class.getCanonicalName());
    
    public String getZibaseID()
    {
        return zibaseID;
    }

    @Property
    public void setZibaseID(String zibaseID)
    {
        LOG.info("[Zibase Mock] set zibaseID, new value : "+zibaseID);
        this.zibaseID = zibaseID;
    }

    public String getZibaseToken()
    {
        return zibaseToken;
    }

    @Property
    public void setZibaseToken(String zibaseToken)
    {
        LOG.info("[Zibase Mock] set zibaseToken, new value : "+zibaseToken);
        this.zibaseToken = zibaseToken;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.Zibase#invokeModule(String, int)
     */
    public String invokeModule(String idModule, String function, String protocol, String dim)
    {
       String res="[Zibase Mock] mock request : invoke function "+function+" on module "+idModule+" by protocol "+protocol+" (dim : "+dim+" )";
       LOG.info(res);
       return res;
    }
}
