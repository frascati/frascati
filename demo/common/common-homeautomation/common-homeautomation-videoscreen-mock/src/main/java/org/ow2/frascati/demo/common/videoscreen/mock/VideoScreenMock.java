/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.common.videoscreen.mock;

import java.util.logging.Logger;

import javax.swing.ImageIcon;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.actuator.OnOffDimActuator;
import org.ow2.frascati.demo.common.valuedevice.SwValueDevice;

/**
 * Mock version of the VideoScreen device, no action done except giving informations about the action perform
 */
@EagerInit
@Scope("COMPOSITE")
public class VideoScreenMock extends SwValueDevice implements OnOffDimActuator
{
    
   private static final long serialVersionUID = -5017903378088292083L;

   private static final Logger logger = Logger.getLogger(VideoScreenMock.class.getName());
   
   private static ImageIcon[] icones;
   
   public VideoScreenMock()
   {
     super();
   }
 
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOn()
     */
    public void acturatorOn()
    {
       logger.info("[VideoScreen Mock] invoke action ON on device"); 
       setCurrentValue(this.maxValue);
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOff()
     */
    public void acturatorOff()
    {
        logger.info("[VideoScreen Mock] invoke action OFF on device");
        setCurrentValue(this.minValue);
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffDimActuator#actuatorDim(int)
     */
    public void actuatorDim(int dimValue)
    {
        logger.info("[VideoScreen Mock] invoke action DIM on device with value "+dimValue);
        setCurrentValue(dimValue);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.valuedevice.SwValueDevice#setCurrentValueAction(int)
     */
    @Override
    public void setCurrentValueAction(int value)
    {
        logger.info("[VideoScreen Mock] Set current value to "+value);
    }
   
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.valuedevice.IValueDevice#getImageIcon(int)
     */
    public ImageIcon getImageIcon(int value)
    {
        if(icones==null)
        {
            icones=new ImageIcon[11];
            for(int i=0;i<11;i++)
            {
                icones[i]=new ImageIcon(getClass().getClassLoader().getResource("images/videoScreen"+i+"0.jpg"));
            }
        }
        
        int iconIndex=Math.round((float)value/10);
        return icones[iconIndex];
    }
   
}
