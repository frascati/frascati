/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.demo.common.homeautomation.light;

import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.actuator.OnOffDimActuator;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;

/**
 * Implementation of an on,off,dim light communicated with a zibase
 */
@EagerInit
@Scope("COMPOSITE")
public class LightMock extends SwOnOffDevice implements OnOffDimActuator
{

    private static final long serialVersionUID = 7739837659903828745L;

    private static final Logger logger = Logger.getLogger(LightMock.class.getName());
  
    /**
     * Constructor for {@link SwOnOffDevice} class, set the title of the Swing Panel
     */
    public LightMock()
    {
        super();
    }
    
    /**
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#onAction()
     */
    @Override
    public Boolean onAction()
    {
        acturatorOn();
        return null;
    }

    /**
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#offAction()
     */
    @Override
    public Boolean offAction()
    {
        acturatorOff();
        return null;
    }
    
    /**
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOn()
     */
    public void acturatorOn()
    {
        logger.info("[LightMock] invoke method on");
    }

    /**
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOff()
     */
    public void acturatorOff()
    {
        logger.info("[LightMock] invoke method off");
    }

    /**
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffDimActuator#actuatorDim(int)
     */
    public void actuatorDim(int dimValue)
    {
        logger.info("[LightMock] invoke method dim with value "+dimValue);
    }
}
