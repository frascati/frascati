/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.common.homeautomation;

import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;

/**
 * Mock version of the Switch device, no action done except giving informations about the action perform
 */
@EagerInit
@Scope("COMPOSITE")
public class SwitchMock extends SwOnOffDevice implements OnOffActuator
{

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(SwitchMock.class.getName());
    
    public SwitchMock()
    {
        super();
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOn()
     */
    public void acturatorOn()
    {
        logger.info("[SwitchMock] invoke method on");
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOff()
     */
    public void acturatorOff()
    {
        logger.info("[SwitchMock] invoke method off");
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#offAction()
     */
    @Override
    public Boolean offAction()
    {
        acturatorOff();
        return null;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#onAction()
     */
    @Override
    public Boolean onAction()
    {
        acturatorOn();
        return null;
    }

}
