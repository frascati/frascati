/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.ligthTest;

import static org.junit.Assert.fail;
import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.assembly.factory.api.ClassLoaderManager;
import org.ow2.frascati.assembly.factory.api.CompositeManager;
import org.ow2.frascati.demo.common.homeautomation.actuator.OnOffDimActuator;
import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.util.FrascatiClassLoader;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
public class LigthTest  extends FraSCAtiTestCase
{   
//    /**FraSCAti instance**/
//    private static FraSCAti fraSCAti;
//
    
//    /**
//     * Launch a new instance of FraSCAti and deploy Zibase component needed by the switch
//     * 
//     * @throws FrascatiException
//     */
//    @Before
//    public void init() throws FrascatiException
//    {
//        fraSCAti=FraSCAti.newFraSCAti();
//        
//        try
//        {
//            ClassLoaderManager classLoaderManager = fraSCAti.getClassLoaderManager();
//            FrascatiClassLoader frascatiClassLoader = new FrascatiClassLoader(classLoaderManager.getClassLoader()); 
//            frascatiClassLoader.addUrl(new File(
//                    "target/dependency/common-homeautomation-zibase-0.0.1-SNAPSHOT.jar").toURI().toURL());
//            CompositeManager compositeManager = fraSCAti.getCompositeManager();
//            compositeManager.getComposite("zibase.composite", frascatiClassLoader);
//            
//        } catch (IOException e)
//        {
//            fail();
//        }
//    }
//    
    @Test
    public final void testInvoke() throws InterruptedException
    {
//      OnOffDimActuator lighter=getService(OnOffDimActuator.class, "onoffdim-actuator");
//      lighter.acturatorOn();
//      Thread.sleep(2000);
//      lighter.acturatorOff();
    }
//    
//    /**
//     * Close FraSCAti instance launch for this test
//     * 
//     * @throws FrascatiException
//     */
//    @AfterClass
//    public static void finish() throws FrascatiException
//    {
//        if(fraSCAti != null)
//        {
//            fraSCAti.close();
//        }
//    }
}
