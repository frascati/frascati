/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.common.homeautomation;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator;
import org.ow2.frascati.demo.common.homeautomation.zibase.ZibasePortType;

/**
 * Implementation of an on,off switch communicated with a zibase
 */
@EagerInit
@Scope("COMPOSITE")
public class Switch implements OnOffActuator
{

    /**
     * @Property
     * Zibase module Id
     */
    private String moduleID;
    
    /**
     * @Property
     * Zibase protocol Id
     */
    private String protocolID;
     
    @Reference(required = false)
    protected ZibasePortType zibase;
    
    /**
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOn()
     */
    public void acturatorOn()
    {
        zibase.invokeModule(moduleID,String.valueOf(ZIBASE_FUNCTION_ON),protocolID,null);        
    }

    /**
     * @see org.ow2.frascati.demo.common.homeautomation.actuator.OnOffActuator#acturatorOff()
     */
    public void acturatorOff()
    {
        zibase.invokeModule(moduleID,String.valueOf(ZIBASE_FUNCTION_OFF),protocolID,null);
    }

    public String getModuleID()
    {
        return moduleID;
    }
    
    @Property
    public void setModuleID(String moduleID)
    {
        this.moduleID = moduleID;
    }
    
    public String getProtocolID()
    {
        return protocolID;
    }

    @Property
    public void setProtocolID(String protocolID)
    {
        this.protocolID = protocolID;
    }
}
