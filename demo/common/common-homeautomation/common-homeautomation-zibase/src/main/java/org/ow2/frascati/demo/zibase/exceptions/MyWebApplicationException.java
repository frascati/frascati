/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.zibase.exceptions;

import java.util.logging.Logger;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 */
public class MyWebApplicationException extends WebApplicationException
{
    private static final long serialVersionUID = 7541979053302408609L;

    /**
     * The default HTTP status to send
     */
    protected final static int DEFAULT_RESPONSE = 400;

    /**
     * The logger
     */
    protected final static Logger LOG = Logger.getLogger(MyWebApplicationException.class.getCanonicalName());
   
    public MyWebApplicationException(String message)
    {
       this(DEFAULT_RESPONSE,message);
    }
    
    public MyWebApplicationException(int responseStatus, String message)
    {
        super(Response.status(responseStatus).entity(message).type(MediaType.TEXT_PLAIN).build());
        LOG.severe("Error " + responseStatus + " : " + message);
    }
}
