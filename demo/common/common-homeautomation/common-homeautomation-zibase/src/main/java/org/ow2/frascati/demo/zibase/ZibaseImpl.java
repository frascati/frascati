/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.zibase;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.core.MultivaluedMap;

import org.eclipse.jetty.util.log.Log;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.homeautomation.zibase.ZibasePortType;
import org.ow2.frascati.demo.zibase.client.ZibaseClient;
import org.ow2.frascati.demo.zibase.elements.ZibaseDeviceElement;
import org.ow2.frascati.demo.zibase.elements.ZibaseDeviceElementImpl;
import org.ow2.frascati.demo.zibase.elements.ZibaseElement;
import org.ow2.frascati.demo.zibase.elements.ZibaseMacroElement;
import org.ow2.frascati.demo.zibase.elements.ZibaseMacroElementImpl;
import org.ow2.frascati.demo.zibase.exceptions.MyWebApplicationException;
import org.ow2.frascati.demo.zibase.exceptions.WrongZibaseInitialisationException;
import org.ow2.frascati.demo.zibase.exceptions.ZibaseElementNotExistException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Implementation of the Zibase interface, this class act as a proxy with the ZAPI see http://zodianet.com/images/zapi/ZAPI1.12.pdf for more details.
 */
@EagerInit
@Scope("COMPOSITE")
public class ZibaseImpl implements ZibasePortType
{
    /**
     * The java logger
     */
    private static final Logger logger = Logger.getLogger(ZibaseImpl.class.getName());
    
    /**
     * The URL of the Zibase REST resource for action requests
     */
    private final static String ZIBASE_API_ACTION_URL="https://zibase.net/m/set_iphone.php";
    
    /**
     * The URL of the Zibase REST resource for informations about devices requests
     */
    private final static String ZIBASE_API_DEVICE_URL="https://zibase.net/m/get_xml.php";
    
    /**
     * The Default function value to send to Zibase
     */
    private final static int ZIBASE_FUNCTION_DEFAULT_VALUE=0;
    
    /**
     * The Default protocol value to send to Zibase
     */
    private final static int ZIBASE_DEFAULT_BROADCAST=0;
    
    /**
     * The Default dim value to send to Zibase
     */
    private final static int ZIBASE_DIM_DEFAULT_VALUE=0;
    
    /**
     * @Property
     * ID for Zibase authentication
     */
    private String zibaseID;
    
    /**
     * @Property
     * token for Zibase authentication
     */
    private String zibaseToken;

    /**
     * Jersey Client for http secure invocation to ZAPI
     */
    private Client zibaseClient;
    
    /**
     * Jersey WebResource for REST invocation to get informations about devices
     */
    private WebResource zibaseInfoResource;
    
    /**
     * Jersey WebResource for REST invocation to invoke method on Zibase Api
     */
    private WebResource zibaseActionResource;
    
    /**
     * Map of the devices connected to the Zibase, the entry keys of the map are the idModule of the devices
     */
    private HashMap<String,ZibaseDeviceElement> devices;
    
    /**
     * Map of the macros available on the Zibase, the entry keys of the map are the idMacro of the macros
     */
    private HashMap<String,ZibaseMacroElement> macros;
    
    /**
     * Constructor, instantiate Web client and resources
     */
    public ZibaseImpl()
    {
        this.zibaseClient=ZibaseClient.createClient();
        this.zibaseInfoResource=zibaseClient.resource(ZIBASE_API_DEVICE_URL);
        this.zibaseActionResource=zibaseClient.resource(ZIBASE_API_ACTION_URL);
    }
    
    /**
     * Update the zibaseResource object with the zibaseId and zibaseToken parameters, refresh the devices and macros maps.
     * Used in the setters of these parameters. 
     * 
     * @throws WrongZibaseInitialisationException
     * @throws IOException 
     * @throws JDOMException 
     */
    @SuppressWarnings("unchecked")
    @Init
    public void updateZibase() throws WrongZibaseInitialisationException, JDOMException, IOException
    {
        /*Avoid null parameters when the first initialization of the zibaseID and zibaseToken properties */
        if(this.zibaseID==null || this.zibaseToken==null)       return;
        
        /*Invoke ZAPI to get informations about devices and macros*/
        MultivaluedMap<String, String> params=getAuthenticationParams();
        ClientResponse response=zibaseInfoResource.queryParams(params).get(ClientResponse.class);
        if(response.getClientResponseStatus()!=ClientResponse.Status.OK)            throw new WrongZibaseInitialisationException(this.zibaseID, this.zibaseToken);    

        /*Read the XML information file*/
        SAXBuilder builder=new SAXBuilder();
        InputStream responseStream=response.getEntityInputStream();
        Document descriptorDocument=builder.build(responseStream);
        responseStream.close();
        Element descriptor=descriptorDocument.getRootElement();
        
        List<Element> devicesXML=descriptor.getChildren(ZibaseDeviceElement.ZIBASE_TAG);
        boolean isAdded;
        
        /*Fill devices map*/
        this.devices=new HashMap<String, ZibaseDeviceElement>();
        ZibaseDeviceElement device;
        for(Element deviceXML : devicesXML)
        {
            device=new ZibaseDeviceElementImpl(deviceXML);
            isAdded=addZibaseElement(device);
            if(!isAdded)
            {
                Log.info("Cannot add Zibase device with idModule : "+device.getID()+" an other one already exist");
            }
        }
  
        /*Fill macros map*/
        this.macros=new HashMap<String, ZibaseMacroElement>();
        List<Element> macrosXML=descriptor.getChildren(ZibaseMacroElement.ZIBASE_TAG);
        ZibaseMacroElement macro;
        for(Element macroXML : macrosXML)
        {
            macro=new ZibaseMacroElementImpl(macroXML);
            isAdded=addZibaseElement(macro);
            if(!isAdded)
            {
                Log.info("Cannot add Zibase macro with idMacro : "+macro.getID()+" an other one already exist");
            }
        }
    }

    /**
     * @see org.ow2.frascati.demo.zibase.Zibase#invokeModule(String, String, String, String)
     */
    public String invokeModule(String idModule, String function,String protocol,String dim)
    {
        /*Get the zibase module*/
        ZibaseElement element=getZibaseElement(idModule);
        if(element==null)
        {
            throw new ZibaseElementNotExistException(idModule);
        }
        
        /*Fill invocation parameters*/
        MultivaluedMap<String, String> params=getAuthenticationParams();
        params.add("action", "comfort");
        params.add("actionComfort", "module");
        params.add("idModule", element.getID());
        String actionModule=getActionModuleParam(function, protocol, dim);
        params.add("actionModule", actionModule);
        
        /*Invoke ZAPI*/
        ClientResponse response=zibaseActionResource.queryParams(params).post(ClientResponse.class);
        if(response.getClientResponseStatus()!=ClientResponse.Status.OK)
        {
            throw new MyWebApplicationException("Error "+response.getClientResponseStatus()+" when post the request : "+zibaseActionResource.queryParams(params).getURI());
        }
        
        return response.getEntity(String.class);
    }

    /**
     * Create a Map containing authentication information for request on Zibase API
     * 
     * @return a MultivaluedMap containing authentication parameters
     */
    private MultivaluedMap<String, String> getAuthenticationParams()
    {
        MultivaluedMap<String, String> params=new MultivaluedMapImpl();
        params.add("device", this.zibaseID);
        params.add("token", this.zibaseToken);
        return params;
    }
    
    /**
     * Check and convert function, protocol and dim parameters to actionModule parameter to send to Zibase API
     * actionModule = function + 2^8*protocol + 2^23*dim
     * 
     * @param function : the Zibase function number (OFF : 0, ON : 1, DIM : 2...)
     * @param protocol : the Zibase protocol number (X10 : 0, ZWave : 6....)
     * @param dim : the dim value in percent
     * @return the converted actionModule
     */
    private String getActionModuleParam(String function, String protocol, String dim)
    {
        int functionInt;
        try
        {
            functionInt=Integer.valueOf(function);
        }
        catch(Exception e)
        {
            logger.info("'function' parameter cannot be cast to int, set to default value : "+ZIBASE_FUNCTION_DEFAULT_VALUE);
            functionInt=ZIBASE_FUNCTION_DEFAULT_VALUE;
        }
        
        int protocolInt;
        try
        {
            protocolInt=Integer.valueOf(protocol);
        }
        catch(Exception e)
        {
            logger.info("'protocol' parameter cannot be cast to int, set to default value : "+ZIBASE_DEFAULT_BROADCAST);
            protocolInt=ZIBASE_DEFAULT_BROADCAST;
        }
        
        int dimInt;
        try
        {
            dimInt=Integer.valueOf(dim);
            if (dimInt>100)
            {
                dimInt=100;
            }
            else if(dimInt<0)
            {
                dimInt=0;
            }
        }
        catch(Exception e)
        {
            dimInt=ZIBASE_DIM_DEFAULT_VALUE;
        }
        
        int actionModule;
        actionModule=(int)(functionInt+Math.pow(2, 8)*protocolInt+Math.pow(2, 23)*dimInt);
        
        return String.valueOf(actionModule);
    }

    private boolean addZibaseElement(ZibaseElement zibaseElement)
    {
        if(zibaseElement instanceof ZibaseDeviceElement && !devices.containsKey(zibaseElement.getID()))
        {
            devices.put(zibaseElement.getID(), (ZibaseDeviceElement) zibaseElement);
            return true;
        }
        
        if(zibaseElement instanceof ZibaseMacroElement && !macros.containsKey(zibaseElement.getID()))
        {
            macros.put(zibaseElement.getID(), (ZibaseMacroElement) zibaseElement);
            return true;
        }
        
        return false;
    }
    
    private ZibaseElement getZibaseElement(String idModule)
    {
        ZibaseElement zibaseElement = null;
        
        zibaseElement=this.devices.get(idModule);
        if(zibaseElement!=null) return zibaseElement;
        
        zibaseElement=this.macros.get(idModule);
        return zibaseElement;
    }
    
    public String getZibaseID()
    {
        return zibaseID;
    }

    @Property
    public void setZibaseID(String zibaseID)
    {
        if(this.zibaseID!=null && this.zibaseID.equals(zibaseID))      return;
        this.zibaseID = zibaseID;
        try
        {
            updateZibase();
        } catch (WrongZibaseInitialisationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JDOMException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getZibaseToken()
    {
        return zibaseToken;
    }

    @Property
    public void setZibaseToken(String zibaseToken)
    {
        if(this.zibaseToken!=null && this.zibaseToken.equals(zibaseToken))      return;
        this.zibaseToken = zibaseToken;
        try
        {
            updateZibase();
        } catch (WrongZibaseInitialisationException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JDOMException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
