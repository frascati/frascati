/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.zibase.elements;

import org.jdom.Element;

/**
 *
 */
public class ZibaseMacroElementImpl extends ZibaseElementImpl implements ZibaseMacroElement
{

    /**
     * @param XMLElement
     */
    public ZibaseMacroElementImpl(Element XMLElement)
    {
        super(XMLElement);
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getZibaseTag()
     */
    public String getZibaseTag()
    {
        return ZibaseMacroElement.ZIBASE_TAG;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getIDTag()
     */
    public String getIDTag()
    {
        return ZibaseMacroElement.ZIBASE_ID_TAG;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getLogoTag()
     */
    public String getLogoTag()
    {
        return ZibaseMacroElement.ZIBASE_LOGO_TAG;
    }

    public String toString()
    {
        return "[ZibaseDeviceElement] id : "+this.getID()+" ,name : "+this.getName()+" ,logo : "+this.getLogo();
    }
}
