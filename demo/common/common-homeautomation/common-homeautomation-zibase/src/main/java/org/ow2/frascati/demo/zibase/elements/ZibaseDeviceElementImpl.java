/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.zibase.elements;

import org.jdom.Element;

/**
 *
 */
public class ZibaseDeviceElementImpl extends ZibaseElementImpl implements ZibaseDeviceElement
{

    /**
     * @param XMLElement
     */
    public ZibaseDeviceElementImpl(Element XMLElement)
    {
        super(XMLElement);
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getZibaseTag()
     */
    public String getZibaseTag()
    {
        return ZibaseDeviceElement.ZIBASE_TAG;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getIDTag()
     */
    public String getIDTag()
    {
        return ZibaseDeviceElement.ZIBASE_ID_TAG;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getLogoTag()
     */
    public String getLogoTag()
    {
        return ZibaseDeviceElement.ZIBASE_LOGO_TAG;
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseDeviceElement#getType()
     */
    public String getType()
    {
        if(ZibaseDeviceElement.ZIBASE_TYPE_TAG==null || ZibaseDeviceElement.ZIBASE_TYPE_TAG.equals(""))     return null;
        return this.XMLElement.getAttributeValue(ZibaseDeviceElement.ZIBASE_TYPE_TAG);
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseDeviceElement#getProtocol()
     */
    public String getProtocol()
    {
        if(ZibaseDeviceElement.ZIBASE_PROTOCOL_TAG==null || ZibaseDeviceElement.ZIBASE_PROTOCOL_TAG.equals(""))     return null;
        return this.XMLElement.getAttributeValue(ZibaseDeviceElement.ZIBASE_PROTOCOL_TAG);
    }

    public String toString()
    {
        return "[ZibaseDeviceElement] id : "+this.getID()+" ,name : "+this.getName()+" ,type : "+this.getType()+" ,protocol : "+this.getProtocol()+" ,logo : "+this.getLogo();
    }
    
}
