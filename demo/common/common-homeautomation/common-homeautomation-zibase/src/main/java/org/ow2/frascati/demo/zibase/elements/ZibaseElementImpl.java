/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.zibase.elements;

import java.util.logging.Logger;

import org.jdom.Element;


/**
 *
 */
public abstract class ZibaseElementImpl implements ZibaseElement
{
    protected final static Logger LOG = Logger.getLogger(ZibaseElement.class.getCanonicalName());
    
    protected Element XMLElement;
    
    public ZibaseElementImpl(Element XMLElement)
    {
        if(XMLElement==null)
        {
            LOG.severe("Try to create a new ZibaseElement with a null JDom Element");
            return;
        }
        this.XMLElement=XMLElement;
    }
    
    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getName()
     */
    public String getName()
    {
       Element name=this.XMLElement.getChild(ZIBASE_NAME_TAG);
       if(name==null)   return null;
       return name.getValue();
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getID()
     */
    public String getID()
    {
        String idTag=this.getIDTag();
        if(idTag==null || idTag.equals(""))     return null;
        return this.XMLElement.getAttributeValue(idTag);
    }

    /**
     * @see org.ow2.frascati.demo.zibase.elements.ZibaseElement#getLogo()
     */
    public String getLogo()
    {
        String logoTag=this.getLogoTag();
        if(logoTag==null || logoTag.equals(""))     return null;
        return this.XMLElement.getAttributeValue(logoTag);
    }

    public boolean equals(Object other)
    {
        if(other==null)                             return false;
        if(!(other instanceof ZibaseElement))       return false;
        ZibaseElement ze=(ZibaseElement) other;
        return this.getID().equals(ze.getID());
    }
}
