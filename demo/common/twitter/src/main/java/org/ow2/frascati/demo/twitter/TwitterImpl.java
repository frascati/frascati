/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 * 
 * Contributor(s):
 */


package org.ow2.frascati.demo.twitter;

import org.eclipse.jetty.util.log.Log;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

/**
 *
 * Implementation of TwitterService using properties as authentication keys
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class TwitterImpl implements ITwitter
{	
	/**
	 * @Property
	 * OAuth settings consumer key
	 */
	private String consumerKey;
	
	/**
	 * @Property
	 * OAuth settings consumer secret key
	 */
	private String consumerSecret;
	
	/**
	 * @Property
	 * OAuth settings access key
	 */
	private String accessToken;
	
	/**
	 * @Property
	 * OAuth settings access secret key
	 */
	private String accessTokenSecret;
	
	
	/**
	 * Twitter instance from twitter4j API, use as proxy
	 */
	protected Twitter twitter;
	
	
	
	/**
	 * Update the instance of twitter field, use to initialize the component after property injection or when one of them is set.
	 * Update is proceed only if no property field is null
	 */
	@Init
	public void updateTwitter()
	{
		if(consumerKey!=null && consumerSecret!=null && accessToken!=null && accessTokenSecret!=null)
		{
			TwitterFactory factory = new TwitterFactory();
		    AccessToken access = new AccessToken(accessToken,accessTokenSecret);
		    twitter = factory.getInstance();
		    twitter.setOAuthConsumer(consumerKey,consumerSecret);
		    twitter.setOAuthAccessToken(access);
		}
	}
	
	/**
	 * Update the status of the twitter account
	 * 
	 * @param whatshappening    : the new status
	 * @throws TwitterException : cause by a wrong authentication or an internal twitter server problem
	 */
	public void tweet(String whatshappening)
	{
		try
		{
			twitter.updateStatus(whatshappening);
		}
		catch (TwitterException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Send a direct message to a known receiver
	 * 
	 * @param screeName : the screen name of the receiver
	 * @param message   : the message to send
	 * @throws TwitterException : cause by a wrong authentication or an internal twitter server problem
	 */
	public void sendDirectMessage(String receiver, String message)
	{
		try
		{
		    Log.info("");
			twitter.sendDirectMessage(receiver, message);
		}
		catch (TwitterException e)
		{
			e.printStackTrace();
		}
	}
	
	public String getConsumerKey()
	{
		return consumerKey;
	}

	/**
	 * Update the consumer key property and the twitter instance
	 * @param consumerKey : the new consumerKey value
	 */
	@Property
	public void setConsumerKey(String consumerKey)
	{
		this.consumerKey = consumerKey;
		updateTwitter();
	}

	
	public String getConsumerSecret()
	{
		return consumerSecret;
	}

	
	/**
	 * Update the consumer secret key property and the twitter instance
	 * @param consumerSecret : the new consumerSecret value
	 */
	@Property
	public void setConsumerSecret(String consumerSecret)
	{
		this.consumerSecret = consumerSecret;
		updateTwitter();
	}


	public String getAccessToken()
	{
		return accessToken;
	}

	
	/**
	 * Update the consumer access token property and the twitter instance
	 * @param accessToken : the new accessToken value
	 */
	@Property
	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken;
		updateTwitter();
	}


	public String getAccessTokenSecret()
	{
		return accessTokenSecret;
	}

	/**
	 * Update the consumer access secret token property and the twitter instance
	 * @param accessTokenSecret : the new accessTokenSecret value
	 */
	@Property
	public void setAccessTokenSecret(String accessTokenSecret)
	{
		this.accessTokenSecret = accessTokenSecret;
		updateTwitter();
	}
}