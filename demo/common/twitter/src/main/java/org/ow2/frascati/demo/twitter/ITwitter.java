/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 * 
 * Contributor(s):
 */


package org.ow2.frascati.demo.twitter;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.osoa.sca.annotations.Service;
import twitter4j.TwitterException;

/**
 *
 * Interface of Twitter basics services
 * 
 */
@Service
public interface ITwitter
{

	/**
	 * Update the status of the twitter account
	 * 
	 * @param whatshappening    : the new status
	 * @throws TwitterException : cause by a wrong authentication or an internal twitter server problem
	 */
	@GET
	@Path("/tweet")
	@Produces("text/plain")
	public void tweet(@QueryParam("whatshappening") String whatshappening);
	
	/**
	 * Send a direct message to a known receiver
	 * 
	 * @param screeName : the screen name of the receiver
	 * @param message   : the message to send
	 * @throws TwitterException : cause by a wrong authentication or an internal twitter server problem
	 */
	@POST
	@Path("/sendMessage")
	@Produces("text/plain")
	public void sendDirectMessage(@QueryParam("screenName") String screeName,@QueryParam("message") String message);
}