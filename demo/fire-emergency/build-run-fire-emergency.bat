@echo Building the demo


cd ..\common\twitter
call mvn clean install
@echo Press key to run Twitter
pause
start mvn -Prun,web
cd ..\..\fire-emergency


cd itemis-demo-interface
call mvn clean install
cd ..

cd alarm
call mvn clean install
@echo Press key to run Alarm
pause
start mvn -Prun,web
cd ..

cd turnstile
call mvn clean install
@echo Press key to run Turnstile
pause
start mvn -Prun,web
cd ..


cd fire-control-center
call mvn clean install
@echo Press key to run Fire Control Center
pause
start mvn -Prun,web
cd ..


cd sprinkler
call mvn clean install
@echo Press key to run Sprinkler
pause
start mvn -Prun,web
cd ..


cd fire-controller
call mvn clean install
@echo Press key to run Fire Controller
pause
start mvn -Prun,web
cd ..


cd fire-detector
call mvn clean install
@echo Press key to run Fire Detector
pause
start mvn -Prun
cd ..

cd fire-detector-dicepe
call mvn clean install
@echo Press key to run Fire Detector CEP version
pause
start mvn -Prun
cd ..