/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.fireemergency.alarm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javazoom.jl.player.Player;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;
/**
 *
 */
@EagerInit
@Scope("COMPOSITE")
public class AlarmGUI extends SwOnOffDevice
{
    private static final long serialVersionUID = -4386213286464468825L;

    private Thread t;
    private File soundFile;
    
    public AlarmGUI()
    {
        super();
        try
        {
            URL soundURL = this.getClass().getClassLoader().getResource("sounds/alarm.mp3");
            InputStream in = soundURL.openStream();
            this.soundFile = File.createTempFile("alarm", "");
            OutputStream out = new FileOutputStream(soundFile);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0)
            {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#onAction()
     */
    @Override
    public Boolean onAction()
    {
        if(t!=null)     return false;
        this.t = new Thread(new Runnable()
        {

            public void run()
            {
                try
                {
                    InputStream soundStream=new FileInputStream(soundFile);
                    Player soundPlayer=new Player(soundStream);
                    soundPlayer.play();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        this.t.start();
        return true;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#offAction()
     */
    @Override
    public Boolean offAction()
    {
        this.t.stop();
        this.t=null;
        return true;
    }
    
}
