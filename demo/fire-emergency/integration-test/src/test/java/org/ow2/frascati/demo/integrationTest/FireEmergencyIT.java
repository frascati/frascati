/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.demo.integrationTest;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.binding.factory.AbstractBindingFactoryProcessor;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.resources.Component;
import org.ow2.frascati.remote.introspection.util.FileUtil;
import org.ow2.frascati.util.FrascatiException;

import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 *
 */
public class FireEmergencyIT
{
    /** REST binding URI */
    public static final String BINDING_URI = AbstractBindingFactoryProcessor.BINDING_URI_BASE_DEFAULT_VALUE;

    private static FraSCAti fraSCAti;
    
    /** The deployment client */
    public static Deployment deployment;

    /** The introspection client */
    public static RemoteScaDomain introspection;
    
    @BeforeClass
    public static void init() throws FrascatiException
    {
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTFractalRest");
        fraSCAti=FraSCAti.newFraSCAti();
        deployment = JAXRSClientFactory.create(BINDING_URI + "/deploy", Deployment.class);
        introspection = JAXRSClientFactory.create(BINDING_URI + "/introspection", RemoteScaDomain.class); 
    }
    
    @Test
    public void deploy() throws FrascatiException, InterruptedException
    {
        deployAndTestComposite("common-twitter", "twitter_localhost", "twitter_localhost");
        deployAndTestComposite("turnstile", "turnstile_localhost", "turnstile");
        deployAndTestComposite("fire-control-center", "firecontrolcenter_localhost", "firecontrolcenter");
        deployAndTestComposite("sprinkler", "sprinkler_localhost", "sprinkler");
        deployAndTestComposite("fire-controller", "firecontroller_localhost", "firecontroller");
        deployAndTestComposite("fire-detector", "firedetector_localhost", "fire-detector_localhost");
//        deployAndTestComposite("fire-detector-dicepe", "firedetector_localhost", "fire-detector_localhost");
        
        MultivaluedMap<String, String> params=new MultivaluedMapImpl();
        
//        params.clear();
//        params.add("methodName", "startFireAlert");
//        introspection.invokeMethod("/fire-detector_localhost/FireDetector",params);
        
        params.clear();
        params.add("methodName", "start");
        introspection.invokeMethod("/sprinkler/sprinklerControl",params);

        params.clear();
        params.add("methodName", "isStarted");
        String result=introspection.invokeMethod("/sprinkler/sprinklerControl",params);
        assertEquals(result, "true");
        
//        params.clear();
//        params.add("methodName", "open");
//        introspection.invokeMethod("/turnstile/turnstileControl",params);
        
//        params.clear();
//        params.add("methodName", "end");
//        introspection.invokeMethod("/firecontroller/Emergency",params);
        
    }

    private void deployAndTestComposite(String jarName, String compositeFileName, String compositeName)
    {
        String jar = null;

        try
        {
            jar = FileUtil.getStringFromFile(new File("target/dependency/"+jarName+".jar"));
        } catch (IOException e)
        {
            System.err.println("Cannot read "+jarName+" jar file!");
            e.printStackTrace();
            fail();
        }

        deployment.deployComposite(compositeFileName+".composite", jar);
        Component c = introspection.getComponent(compositeName);
        assertNotNull(c);
    }
    
    @AfterClass
    public static void stop() throws FrascatiException
    {
        fraSCAti.close();
    }
    
}
