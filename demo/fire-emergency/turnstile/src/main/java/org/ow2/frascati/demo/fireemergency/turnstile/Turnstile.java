/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s):  Antonio Souza Neto
 *
 */

package org.ow2.frascati.demo.fireemergency.turnstile;

import itemis.demo.Status;
import itemis.demo.StatusListener;

import java.awt.Color;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;

/**
 * Implementation class of the component 'doors'. (Other components can share
 * this implementation class.)
 */
@EagerInit
@Scope("COMPOSITE")
public class Turnstile extends SwOnOffDevice implements itemis.demo.Turnstile, StatusListener
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Turnstile.class.getName());

    @Reference(required = false)
    private StatusListener fireControlCenter;

    private String deviceID;

    public Turnstile()
    {
        super();
    }

    /**
     * Wrapper for on method
     */
    public void open()
    {
        on();
    }

    /**
     * Wrapper for off method
     */
    public void close()
    {
        off();
    }

    /**
     * OnAction method is call after the graphical changes (set ontext and
     * onImage)
     */
    @Override
    public Boolean onAction()
    {
        logger.log(Level.INFO, "[Doors service] Doors opened");

        if (fireControlCenter == null)
        {
            logger.log(Level.INFO, "[Doors service] FireControllerCenter not connected to the Doors");
        } else
        {
            Status status = new Status();
            status.setId(deviceID);
            status.setStatus("open");
            Calendar cal = GregorianCalendar.getInstance();
            status.setTimestramp(cal.getTime());

            fireControlCenter.notifyStatus(status);

            logger.log(Level.INFO, "[Doors service] FireControllerCenter connected to the Doors send current status: open");
        }

        return true;
    }

    /**
     * OffAction method is call after the graphical changes (set offtext and
     * offImage)
     */
    @Override
    public Boolean offAction()
    {
        logger.log(Level.INFO, "[Doors service] Doors closed");

        Status status = new Status();
        status.setId(deviceID);
        status.setStatus("close");
        Calendar cal = GregorianCalendar.getInstance();
        status.setTimestramp(cal.getTime());

        if (fireControlCenter == null)
        {
            logger.log(Level.INFO, "[Doors service] FireControllerCenter not connected to the Doors");
        } else
        {
            fireControlCenter.notifyStatus(status);
            logger.log(Level.INFO, "[Doors service] FireControllerCenter connected to the Doors send current status : close");
        }

        return true;
    }

    /**
     * This method is compulsory to use the JMS binding, indeed Turnstile publish its new status to Fire control center by publishing a Status object in a JMS queue 
     * 
     * @param status The new status containing the id of the device, its new status and a timestamp  
     */
    public void notifyStatus(Status status)
    {
        logger.log(Level.INFO, "[Doors service] Receive notification status change : id " + status.getId() + " status : " + status.getStatus());
    }

    public String getDeviceID()
    {
        return deviceID;
    }

    @Property
    public void setDeviceID(String deviceID)
    {
        this.deviceID = deviceID;
    }
}
