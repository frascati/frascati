/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.fireemergency.firedetector;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;

import itemis.demo.*;
/**
 *
 */
@Scope("COMPOSITE")
@EagerInit
public class FireDetectorGUI extends SwOnOffDevice implements IFireDetector
{

    private static final long serialVersionUID = -2853480864083787772L;

    private static final Logger logger = Logger.getLogger(FireDetectorGUI.class.getName());
    
    @Reference(required = false)
    public AlarmListener alarmListener;
    
    public FireDetectorGUI()
    {
        super();
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.fireemergency.firedetector.IFireDetector#startFireAlert()
     */
    public void startFireAlert()
    {
        on();
    }
    
    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#onAction()
     */
    @Override
    public Boolean onAction()
    {
        Logger.getLogger(FireDetectorGUI.class.getName()).log(Level.SEVERE, "[FireDetector service] FIRE!");

        Alarm alarm = new Alarm();
        alarm.setId("fire detector 1");
        Calendar cal = GregorianCalendar.getInstance();
        alarm.setTimestramp(cal.getTime());

        Logger.getLogger(FireDetectorGUI.class.getName()).log(Level.SEVERE, "[FireDetector service] Trying to previne fire controler service...");

        if (alarmListener == null)
        {
            logger.log(Level.SEVERE, "[FireDetector service] Impossible to send alarm");
        } else
        {
            alarmListener.notifyAlarm(alarm);
            logger.log(Level.SEVERE, "[FireDetector service] Alarm sent");
        }
        
        return true;
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice#offAction()
     */
    @Override
    public Boolean offAction()
    {
        return onAction();
    } 
}
