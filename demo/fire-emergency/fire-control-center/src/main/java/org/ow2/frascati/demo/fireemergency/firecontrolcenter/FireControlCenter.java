/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio Souza Neto
 *
 * Contributor(s): Gwenael Cattez
 *
 */

package org.ow2.frascati.demo.fireemergency.firecontrolcenter;

import itemis.demo.Alarm;
import itemis.demo.AlarmListener;
import itemis.demo.Emergency;
import itemis.demo.Sprinkler;
import itemis.demo.Status;
import itemis.demo.StatusListener;
import itemis.demo.Turnstile;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.UIManager;
import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
@EagerInit
public class FireControlCenter extends JFrame implements StatusListener, AlarmListener
{
    @Reference(required = false)
    private Emergency fireController;

    @Reference
    private Turnstile turnstile;

    @Reference
    private Sprinkler sprinkler;

    /*
     * Other attributes
     */
    private static final long serialVersionUID = 1L;

    private final static Logger logger = Logger.getLogger(FireControlCenter.class.getName());

    // image icons
    private javax.swing.ImageIcon alertOn;
    private javax.swing.ImageIcon alertOff;
    private javax.swing.ImageIcon sprinklerOn;
    private javax.swing.ImageIcon sprinklerOff;
    private javax.swing.ImageIcon doorOpen;
    private javax.swing.ImageIcon doorClosed;
    private javax.swing.ImageIcon controllerIcon;

    // Components declaration
    private javax.swing.JPanel mainPanel;
    private javax.swing.JLabel controllerLabel;
    private javax.swing.JLabel alertLabel;
    private javax.swing.JPanel alertPanel;
    private javax.swing.JButton closeDoorsButton;
    private javax.swing.JTextArea console;
    private javax.swing.JLabel consoleLabel;
    private javax.swing.JLabel doorsLabel;
    private javax.swing.JPanel doorsPanel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel messagesLabel;
    private javax.swing.JPanel messagesPanel;
    private javax.swing.JButton msgButton1;
    private javax.swing.JButton msgButton2;
    private javax.swing.JButton msgButton3;
    private javax.swing.JButton openDoorsButton;
    private javax.swing.JButton simulateFireButton;
    private javax.swing.JLabel sprinklersLabel;
    private javax.swing.JButton sprinklersOffButton;
    private javax.swing.JButton sprinklersOnButton;
    private javax.swing.JPanel sprinklersPanel;
    private javax.swing.JButton stopAlertButton;

    @Init
    public void init()
    {
        initComponents();
        setVisible(true);
    }

    public void writeMessageConsole(String message)
    {
        console.append(message);
    }

    /**
     * Instantiate the graphical interface
     */
    public void initComponents()
    {
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
        loadImages();

        mainPanel = new javax.swing.JPanel();
        controllerLabel = new javax.swing.JLabel();
        alertPanel = new javax.swing.JPanel();
        simulateFireButton = new javax.swing.JButton();
        stopAlertButton = new javax.swing.JButton();
        alertLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        console = new javax.swing.JTextArea();
        consoleLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        sprinklersPanel = new javax.swing.JPanel();
        sprinklersLabel = new javax.swing.JLabel();
        sprinklersOnButton = new javax.swing.JButton();
        sprinklersOffButton = new javax.swing.JButton();
        doorsPanel = new javax.swing.JPanel();
        doorsLabel = new javax.swing.JLabel();
        openDoorsButton = new javax.swing.JButton();
        closeDoorsButton = new javax.swing.JButton();
        messagesPanel = new javax.swing.JPanel();
        messagesLabel = new javax.swing.JLabel();
        msgButton1 = new javax.swing.JButton();
        msgButton2 = new javax.swing.JButton();
        msgButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ADAM Team - Fire emergency control panel");
        setResizable(false);

        controllerLabel.setIcon(controllerIcon);

        simulateFireButton.setText("Simulate Fire");

        stopAlertButton.setText("Stop Alert");
        stopAlertButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                stopAlertButtonActionPerformed(evt);
            }
        });

        alertLabel.setIcon(alertOff); // NOI18N
        alertLabel.setText("Alert");
        alertLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        alertLabel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout alertPanelLayout = new javax.swing.GroupLayout(alertPanel);
        alertPanel.setLayout(alertPanelLayout);
        alertPanelLayout.setHorizontalGroup(alertPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                alertPanelLayout
                        .createSequentialGroup()
                        .addComponent(alertLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                alertPanelLayout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(stopAlertButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)
                                        .addComponent(simulateFireButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)).addContainerGap()));
        alertPanelLayout.setVerticalGroup(alertPanelLayout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(
                        alertPanelLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(simulateFireButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(stopAlertButton))
                .addComponent(alertLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE));

        console.setColumns(20);
        console.setRows(5);
        jScrollPane1.setViewportView(console);

        consoleLabel.setFont(new java.awt.Font("Ubuntu", 0, 12)); // NOI18N
        consoleLabel.setText("Console:");

        sprinklersLabel.setIcon(sprinklerOff); // NOI18N
        sprinklersLabel.setText("Sprinklers");
        sprinklersLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        sprinklersLabel.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        sprinklersOnButton.setText("On");
        sprinklersOnButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sprinklersOnButtonActionPerformed(evt);
            }
        });

        sprinklersOffButton.setText("Off");
        sprinklersOffButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                sprinklersOffButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout sprinklersPanelLayout = new javax.swing.GroupLayout(sprinklersPanel);
        sprinklersPanel.setLayout(sprinklersPanelLayout);
        sprinklersPanelLayout.setHorizontalGroup(sprinklersPanelLayout
                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(sprinklersLabel)
                .addGroup(
                        javax.swing.GroupLayout.Alignment.TRAILING,
                        sprinklersPanelLayout.createSequentialGroup()
                                .addComponent(sprinklersOnButton, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sprinklersOffButton, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)));
        sprinklersPanelLayout.setVerticalGroup(sprinklersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                sprinklersPanelLayout
                        .createSequentialGroup()
                        .addComponent(sprinklersLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                sprinklersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(sprinklersOnButton)
                                        .addComponent(sprinklersOffButton)).addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));

        doorsLabel.setIcon(doorClosed); // NOI18N
        doorsLabel.setText("Doors");
        doorsLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        doorsLabel.setVerticalTextPosition(javax.swing.SwingConstants.TOP);

        openDoorsButton.setText("Open");
        openDoorsButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                openDoorsButtonActionPerformed(evt);
            }
        });

        closeDoorsButton.setText("Close");
        closeDoorsButton.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                closeDoorsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout doorsPanelLayout = new javax.swing.GroupLayout(doorsPanel);
        doorsPanel.setLayout(doorsPanelLayout);
        doorsPanelLayout.setHorizontalGroup(doorsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                doorsPanelLayout
                        .createSequentialGroup()
                        .addGroup(
                                doorsPanelLayout
                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(doorsLabel)
                                        .addGroup(
                                                doorsPanelLayout
                                                        .createSequentialGroup()
                                                        .addComponent(openDoorsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 133,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(closeDoorsButton, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))).addContainerGap(24, Short.MAX_VALUE)));
        doorsPanelLayout.setVerticalGroup(doorsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                doorsPanelLayout
                        .createSequentialGroup()
                        .addComponent(doorsLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                doorsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(openDoorsButton)
                                        .addComponent(closeDoorsButton)).addContainerGap(12, Short.MAX_VALUE)));

        messagesLabel.setText("Messages Service");

        msgButton1.setText("msg1");
        msgButton2.setText("msg2");
        msgButton3.setText("msg3");

        javax.swing.GroupLayout messagesPanelLayout = new javax.swing.GroupLayout(messagesPanel);
        messagesPanel.setLayout(messagesPanelLayout);
        messagesPanelLayout.setHorizontalGroup(messagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(
                        messagesPanelLayout
                                .createSequentialGroup()
                                .addGroup(
                                        messagesPanelLayout
                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(
                                                        messagesPanelLayout
                                                                .createSequentialGroup()
                                                                .addContainerGap()
                                                                .addGroup(
                                                                        messagesPanelLayout
                                                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                                                                .addComponent(msgButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 184,
                                                                                        Short.MAX_VALUE).addComponent(messagesLabel)))
                                                .addGroup(
                                                        javax.swing.GroupLayout.Alignment.TRAILING,
                                                        messagesPanelLayout
                                                                .createSequentialGroup()
                                                                .addContainerGap(12, Short.MAX_VALUE)
                                                                .addGroup(
                                                                        messagesPanelLayout
                                                                                .createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                .addComponent(msgButton3, javax.swing.GroupLayout.Alignment.LEADING,
                                                                                        javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                                                                                .addComponent(msgButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 184,
                                                                                        Short.MAX_VALUE)))).addContainerGap()));
        messagesPanelLayout.setVerticalGroup(messagesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                messagesPanelLayout.createSequentialGroup().addComponent(messagesLabel).addGap(18, 18, 18).addComponent(msgButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(msgButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(msgButton3).addContainerGap(70, Short.MAX_VALUE)));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(
                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                layout.createSequentialGroup()
                                                                        .addComponent(alertPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addGap(30, 30, 30)
                                                                        .addGap(39, 39, 39)
                                                                        .addGroup(
                                                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                                        .addComponent(consoleLabel)
                                                                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                                                369, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                                layout.createSequentialGroup()
                                                        .addComponent(sprinklersPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(doorsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(messagesPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(21, Short.MAX_VALUE)));
        layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(
                layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(alertPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(
                                                javax.swing.GroupLayout.Alignment.TRAILING,
                                                layout.createSequentialGroup()
                                                        .addComponent(consoleLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                Short.MAX_VALUE)
                                                        .addGap(1, 1, 1)
                                                        .addGroup(
                                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(
                                                                        jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 68,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)).addGap(16, 16, 16)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(
                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(
                                                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(doorsPanel, javax.swing.GroupLayout.Alignment.LEADING,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(sprinklersPanel, javax.swing.GroupLayout.Alignment.LEADING,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(messagesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE,
                                                Short.MAX_VALUE)).addGap(31, 31, 31)));

        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        controllerLabel.setAlignmentX(CENTER_ALIGNMENT);
        getContentPane().add(controllerLabel);
        getContentPane().add(mainPanel);

        pack();

        setLocationRelativeTo(null);
    }

    
    /**
     * Load images for the graphical interface
     */
    private void loadImages()
    {
        alertOn = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/AlertOn.png"));
        alertOff = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/AlertOff.png"));
        sprinklerOn = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/SprinklerOn.jpg"));
        sprinklerOff = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/SprinklerOff.jpg"));
        doorOpen = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/DoorOpen.jpg"));
        doorClosed = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/DoorClosed.jpg"));
        controllerIcon = new javax.swing.ImageIcon(getClass().getClassLoader().getResource("images/controllerPanel.jpg"));
    }

    /**
     * Listener for the stop alert button, call end function on the Fire control center
     * 
     * @param evt The interface event
     */
    private void stopAlertButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        console.append("stop fire simulation...\n");
        if (fireController == null)
        {
            console.append("\nComponent \"Fire controller\" not connected to Control Panel!\n");
        } else
        {
            fireController.end();
            alertLabel.setIcon(alertOff);
            console.append("Stop simulate fire...\n");
        }
    }

    /**
     * Listener for the "sprinkler on" button
     * 
     * @param evt The interface event
     */
    private void sprinklersOnButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        if (sprinkler == null)
        {
            console.append("\nComponent \"Sprinklers\" not connected to Control Panel!\n");
        } else
        {
            sprinkler.start();
            console.append("Opening sprinklers...\n");
        }
    }

    /**
     * Listener for the "sprinkler off" button
     * 
     * @param evt The interface event
     */
    private void sprinklersOffButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        if (sprinkler == null)
        {
            console.append("\nComponent \"Sprinklers\" not connected to Control Panel!\n");
        } else
        {
            sprinkler.stop();
            console.append("Stopping sprinklers...\n");
        }
    }

    /**
     * Listener for the "open doors" button
     * 
     * @param evt The interface event
     */
    private void openDoorsButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        if (turnstile == null)
        {
            console.append("\nComponent \"Doors\" not connected to Control Panel!\n");
        } else
        {
            turnstile.open();
            console.append("Opening doors...\n");
        }
    }

    /**
     * Listener for the "close doors" button
     * 
     * @param evt The interface event
     */
    private void closeDoorsButtonActionPerformed(java.awt.event.ActionEvent evt)
    {
        if (turnstile == null)
        {
            console.append("\nComponent \"Doors\" not connected to Control Panel!\n");
        } else
        {
            turnstile.close();
            console.append("Closing doors...\n");
        }
    }

    /**
     * Method use by the device to notify a status change
     * 
     * @param status The new status containing the id of the device, its new status and a timestamp  
     */
    public void notifyStatus(Status status)
    {
        logger.log(Level.SEVERE,
                "[FireControlerCenter service] FireControlerCenter have been notify by the status " + status.getId() + " on " + status.getStatus());

        if (status.getId().equalsIgnoreCase("fire detector 1"))
        {
            if (status.getStatus().equalsIgnoreCase("fire"))
                alertLabel.setIcon(alertOn);
            else if (status.getStatus().equalsIgnoreCase("no fire"))
                alertLabel.setIcon(alertOff);
        } else if (status.getId().equalsIgnoreCase("sprinkler 1"))
        {
            if (status.getStatus().equalsIgnoreCase("start"))
                sprinklersLabel.setIcon(sprinklerOn);
            else if (status.getStatus().equalsIgnoreCase("stop"))
                sprinklersLabel.setIcon(sprinklerOff);
        } else if (status.getId().equalsIgnoreCase("door 1"))
        {
            if (status.getStatus().equalsIgnoreCase("open"))
                doorsLabel.setIcon(doorOpen);
            else if (status.getStatus().equalsIgnoreCase("close"))
                doorsLabel.setIcon(doorClosed);
        }
    }

    /**
     * Method call when alarm is launch
     * 
     * @param alarm The Alarm containing the id of the fire detector that launch the
     *        alarm, and timestamp
     */
    public void notifyAlarm(Alarm alarm)
    {
        logger.log(Level.SEVERE, "[FireControlerCenter service] FireControlerCenter have been notify by the alarm " + alarm.getId() + " on " + alarm.getTimestramp());
        String alarmId=alarm.getId();
        if(alarmId.contains("fire detector"))
        {
            alertLabel.setIcon(alertOn);
        }
    }
}