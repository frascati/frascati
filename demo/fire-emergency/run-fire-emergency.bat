@echo Building the demo


cd ..\common\twitter
@echo Press key to run Twitter
pause
start mvn -Prun
cd ..\..\fire-emergency


cd turnstile
@echo Press key to run Turnstile
pause
start mvn -Prun
cd ..


cd fire-control-center
@echo Press key to run Fire Control Center
pause
start mvn -Prun
cd ..


cd sprinkler
@echo Press key to run Sprinkler
pause
start mvn -Prun
cd ..


cd fire-controller
@echo Press key to run Fire Controller
pause
start mvn -Prun,web
cd ..


cd fire-detector
@echo Press key to run Fire Detector
pause
start mvn -Prun
cd ..

cd fire-detector-dicepe
@echo Press key to run Fire Detector CEP version
pause
start mvn -Prun
cd ..