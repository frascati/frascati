/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez 
 *
 */

/********************************/
Fire Emergency Demonstration:
/********************************/

This demonstration illustrates the concepts of SCA programming as isolation, interoperability, multi languages.
The scenario is a fire emergency alert, it's composed with 6 component :
	- fire-detector, detects fire and launch an alarm
	- turnstile
	- sprinkler
	- twitter
	- fire-controller, does the control loop when an alarm is launched or stopped (open/close turnstile, turn on/off sprinkler and order a tweet)
	- fire-control-center, the GUI of the demo, let the user control each devices and stop the fire alarm
	
For more details have a look to the sequence diagram.



/********************************/
Requirements
/********************************/

To run this examples you need :
	- Java JRE 1.5 minimum
	- Maven 2.0 minimum : you may have to increase the java heap memory for maven execution, just set the environment variable MAVEN_OPTS=-Xmx1024m -XX:MaxPermSize=128m


/********************************/
Build the demo
/********************************/

You can use the script build-run-fire-emergency (extension depends on your OS) and follow the instructions to build and run the demo or make a mvn clean install at the root of the demo to build and follow the next step. 


/********************************/
Run the demo
/********************************/

The demo is configured to run in a localhost mode, you can start the component in any order you want but turnstile before fire-control-center because turnstile start the JMS server

	- twitter
		cd common/twitter
		mvn -Prun
		wsdl : http://localhost:9007/Twitter?wsdl
	  
	  
	  - turnstile
		cd fire-emergency/turnstile
		mvn -Prun
		wsdl : http://localhost:9001/Turnstile?wsdl
	  	jms  : scn://localhost:16400
	  	
	  
	- fire-control-center
		cd fire-emergency/fire-control-center
		mvn -Prun
		wsdl : http://localhost:9000/StatusListener?wsdl
		wsdl : http://localhost:9005/AlarmListener?wsdl
		jms  : scn://localhost:16400
		
	  		  	  
	- sprinklers
		cd fire-emergency/sprinkler
  		mvn -Prun
 		wsdl : http://localhost:9002/Sprinkler?wsdl


 	- fire-controller
 		cd fire-emergency/fire-controller
  		mvn -Prun
 		wsdl : http://localhost:9003/AlarmListener?wsdl
 		wsdl : http://localhost:9004/Emergency?wsdl
 		
 		
	- fire-detector
		cd fire-emergency/fire-detector
  		mvn -Prun
 		wsdl : http://localhost:9006/FireDetector?wsdl


/********************************/
JMX management
/********************************/

You can manage the different SCA component and JMS server throughn JMX console, to do it just type jconsole in a terminal (JAVA_HOME environment variable must be set), select the right local process (org.codehaus.classWorlds.Launcher -Pexplorer,we,jmx).
Go to the org.ow2.frascati.jmx->FrascatiJmx->Operations, execute load operation, a new node "SCA Domain" appear containing all your components.
You can also see a node JORAM#0 containing all informations about JORAM server, users and destinations.
Additionally in AgentServer->Engine#0->JoramAdminTopic are the differents operations for Administration.


/********************************/
Known bugs
/********************************/

It seems that sometimes on linux OS the enforce of IPv4 stack doesn't work, it lead to troubles with JGroups binding. To fix it add to MAVEN_OPTS environment variable the parameter -Djava.net.preferIPv4Stack=true.

