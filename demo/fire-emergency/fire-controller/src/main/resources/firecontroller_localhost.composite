<!--  Copyright (C) 2008-2012 Inria, University of Lille 							-->
<!--																				-->
<!--  This library is free software; you can redistribute it and/or 				-->
<!--  modify it under the terms of the GNU Lesser General Public 					-->
<!--  License as published by the Free Software Foundation; either 					-->
<!--  version 2 of the License, or (at your option) any later version. 				-->
<!--																				-->
<!--  This library is distributed in the hope that it will be useful, 				-->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of 				-->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 			-->
<!--  Lesser General Public License for more details. 								-->
<!--																				-->
<!--  You should have received a copy of the GNU Lesser General Public 				-->
<!--  License along with this library; if not, write to the Free Software 			-->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 	-->
<!-- 																				-->
<!--  Contact: frascati@ow2.org 													-->	
<!--                                                                                -->
<!--  Author: Gwenael CATTEZ                                                     	-->
<!-- 																				-->
<!--  Contributors : 				                                                -->

<composite xmlns="http://www.osoa.org/xmlns/sca/1.0"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsdli="http://www.w3.org/2004/08/wsdl-instance"
	name="firecontroller">

	<component name="firecontroller">
	
		<implementation.java class="org.ow2.frascati.demo.fireemergency.firecontroller.FireController" />		
	
		<service name="alarmListener">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(AlarmListener)" />
			<gcs:binding.jgroups xmlns:gcs="http://frascati.ow2.org/xmlns/gcs/1.0" cluster="fire_detector"/>
		</service>		
		<service name="emergency">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Emergency)" />
		</service>
		
		<!-- turnstiles -->
		<reference name="turnstile1">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Turnstile)" />
			<binding.ws 	uri="http://localhost:9007/Turnstile"
							wsdlElement="http://demo.itemis/#wsdl.port(TurnstileService/TurnstilePort)" />
		</reference>
		<reference name="turnstile2">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Turnstile)" />
		</reference>
		<reference name="turnstile3">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Turnstile)" />
		</reference>
				
		<!-- sprinklers -->		
		<reference name="sprinkler1">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Sprinkler)" />
			<binding.ws 	uri="http://localhost:9008/Sprinkler"
							wsdlElement="http://demo.itemis/#wsdl.port(SprinklerService/SprinklerPort)" />
		</reference>
		<reference name="sprinkler2">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Sprinkler)" />
		</reference>
		<reference name="sprinkler3">
			<interface.wsdl interface="all-wsdl.wsdl#wsdl.interface(Sprinkler)" />
		</reference>

		<!--  alarms -->
		<reference name="alarm1">
			<interface.wsdl interface="onoffdevice.wsdl#wsdl.interface(IOnOffDevicePortType)" />
			<binding.ws 	uri="http://localhost:9009/AlarmControl"
							wsdlElement="http://onoffdevice.common.demo.frascati.ow2.org/#wsdl.port(IOnOffDevice/IOnOffDevicePort)" />
		</reference>
		<reference name="alarm2">
			<interface.wsdl interface="onoffdevice.wsdl#wsdl.interface(IOnOffDevicePortType)" />
		</reference>
		<reference name="alarm3">
			<interface.wsdl interface="onoffdevice.wsdl#wsdl.interface(IOnOffDevicePortType)" />
		</reference>
			
			
		<reference name="twitter">
			<interface.wsdl interface="Twitter.wsdl#wsdl.interface(ITwitterPortType)" />
			<binding.ws 	uri="http://localhost:9000/Twitter"	
							wsdlElement="http://twitter.demo.frascati.ow2.org/#wsdl.port(ITwitter/ITwitterPort)" />
		</reference>
		
		<property name="messageToTweet">Fire at INRIA</property>
		
	</component>

	<service name="AlarmListener" promote="firecontroller/alarmListener">
		<binding.ws uri= "http://localhost:9003/AlarmListener"/>
	</service>
	<service name="Emergency" promote="firecontroller/emergency">
		<binding.ws uri="http://localhost:9004/Emergency" />
	</service>

</composite>