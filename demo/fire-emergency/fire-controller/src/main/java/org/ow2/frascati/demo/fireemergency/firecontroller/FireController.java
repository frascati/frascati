/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Antonio Souza Neto
 *
 * Contributor(s):  Gwenael Cattez 
 *
 */

package org.ow2.frascati.demo.fireemergency.firecontroller;

import itemis.demo.Alarm;
import itemis.demo.AlarmListener;
import itemis.demo.Emergency;
import itemis.demo.Sprinkler;
import itemis.demo.Turnstile;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.onoffdevice.IOnOffDevicePortType;
import org.ow2.frascati.demo.twitter.ITwitterPortType;

/**
 * Implementation class of the component 'fireControler'. (Other components can
 * share this implementation class.)
 */
@Scope("COMPOSITE")
@EagerInit
public class FireController implements AlarmListener, Emergency
{
    private final static Logger logger = Logger.getLogger(FireController.class.getName());

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm:ss";

    @Reference(required = false)
    private Sprinkler sprinkler1;

    @Reference(required = false)
    private Sprinkler sprinkler2;

    @Reference(required = false)
    private Sprinkler sprinkler3;
    
    @Reference(required = false)
    protected Turnstile turnstile1;

    @Reference(required = false)
    protected Turnstile turnstile2;

    @Reference(required = false)
    protected Turnstile turnstile3;

    @Reference(required = false)    
    protected IOnOffDevicePortType alarm1;

    @Reference(required = false)    
    protected IOnOffDevicePortType alarm2;

    @Reference(required = false)    
    protected IOnOffDevicePortType alarm3;

    @Reference(required = false)
    protected ITwitterPortType twitter;
    
    /**
     * @Property The message to tweet
     */
    private String messageToTweet;

    /**
     * Method call when alarm is launch
     * 
     * @param a The Alarm containing the id of the fire detector that launch the
     *        alarm, and timestamp
     */
    public void notifyAlarm(Alarm a)
    {
        logger.log(Level.SEVERE, "[FireControler service] FireControler have been notify by the alarm " + a.getId() + " on " + a.getTimestramp());

        if (a.getId().contains("fire detector"))
        {
            logger.log(Level.SEVERE, "[FireControler service] Starting actions to control fire and facilitate exit...");

            logger.log(Level.SEVERE, "\n[FireControler service] Starting Sprinklers...");
            
            if(sprinkler1!=null)
            {
                sprinkler1.start();
                logger.log(Level.SEVERE, "[FireControler service] Sprinkler 1 started successfully!");
            }

            if(sprinkler2!=null)
            {
                sprinkler2.start();
                logger.log(Level.SEVERE, "[FireControler service] Sprinkler 2 started successfully!");
            }

            if(sprinkler3!=null)
            {
                sprinkler3.start();
                logger.log(Level.SEVERE, "[FireControler service] Sprinkler 3 started successfully!");
            }

            logger.log(Level.SEVERE, "\n[FireControler service] Opening turnstiles...");
            if(turnstile1!=null)
            {
                turnstile1.open();
                logger.log(Level.SEVERE, "[FireControler service] Turnstile 1 opened successfully!");
            }

            if(turnstile2!=null)
            {
                turnstile2.open();
                logger.log(Level.SEVERE, "[FireControler service] Turnstile 2 opened successfully!");
            }

            if(turnstile3!=null)
            {
                turnstile3.open();
                logger.log(Level.SEVERE, "[FireControler service] Turnstile 3 opened successfully!");
            }
            
            logger.log(Level.SEVERE, "\n[FireControler service] Launching alarms...");
            if(alarm1!=null)
            {
                alarm1.on();
                logger.log(Level.SEVERE, "[FireControler service] Alarm 1 launched successfully!");
            }
            
            if(alarm2!=null)
            {
                alarm2.on();
                logger.log(Level.SEVERE, "[FireControler service] Alarm 2 launched successfully!");
            }

            if(alarm3!=null)
            {
                alarm3.on();
                logger.log(Level.SEVERE, "[FireControler service] Alarm 3 launched successfully!");
            }

            if (twitter == null)
            {
                logger.log(Level.INFO, "[FireControler service] Twitter component not found!");
            } else
            {
                try
                {
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
                    twitter.tweet(sdf.format(cal.getTime()) + " : " + messageToTweet);
                    logger.log(Level.INFO, "[FireControler service] Successfull tweet!");
                } catch (Exception e)
                {
                    logger.log(Level.SEVERE, "An error occured when calling twitter service " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Method used to stop the alert, stop the different devices( sprinklers, turnstile,...)
     */
    public void end()
    {
        logger.log(Level.SEVERE, "[FireControler service] FireControler have been to end Emergency Alert");

        if(sprinkler1!=null)
        {
            sprinkler1.stop();
            logger.log(Level.SEVERE, "[FireControler service] Sprinkler 1 stopped successfully!");
        }

        if(sprinkler2!=null)
        {
            sprinkler2.stop();
            logger.log(Level.SEVERE, "[FireControler service] Sprinkler 2 stopped successfully!");
        }

        if(sprinkler3!=null)
        {
            sprinkler3.stop();
            logger.log(Level.SEVERE, "[FireControler service] Sprinkler 3 stopped successfully!");
        }

        
        if(turnstile1!=null)
        {
            turnstile1.close();
            logger.log(Level.SEVERE, "[FireControler service] Turnstile 1 closed successfully!");
        }

        if(turnstile2!=null)
        {
            turnstile2.close();
            logger.log(Level.SEVERE, "[FireControler service] Turnstile 2 closed successfully!");
        }

        if(turnstile3!=null)
        {
            turnstile3.close();
            logger.log(Level.SEVERE, "[FireControler service] Turnstile 3 closed successfully!");
        }
        
        if(alarm1!=null)
        {
            alarm1.off();
            logger.log(Level.SEVERE, "[FireControler service] Alarm 1 stopped successfully!");
        }
        
        if(alarm2!=null)
        {
            alarm2.off();
            logger.log(Level.SEVERE, "[FireControler service] Alarm 2 stopped successfully!");
        }

        if(alarm3!=null)
        {
            alarm3.off();
            logger.log(Level.SEVERE, "[FireControler service] Alarm 3 stopped successfully!");
        }
    }

    public String getMessageToTweet()
    {
        return messageToTweet;
    }

    @Property
    public void setMessageToTweet(String messageToTweet)
    {
        this.messageToTweet = messageToTweet;
    }
}