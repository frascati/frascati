/**
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s):  Antonio Souza Neto
 *
 */

package org.ow2.frascati.demo.fireemergency.sprinkler;

import itemis.demo.Status;
import itemis.demo.StatusListener;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.osoa.sca.annotations.EagerInit;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.demo.common.onoffdevice.SwOnOffDevice;

/**
 * Implementation class of the component 'sprinklers'. (Other components can
 * share this implementation class.)
 */
@EagerInit
@Scope("COMPOSITE")
public class Sprinkler extends SwOnOffDevice implements itemis.demo.Sprinkler
{
    private static final long serialVersionUID = 1L;
    private static final Logger logger = Logger.getLogger(Sprinkler.class.getName());

    @Reference(required = false)
    private StatusListener fireControlCenter;

    private String deviceID;

    public Sprinkler()
    {
        super();
    }

    /**
     * Wrapper for on method
     */
    public void start()
    {
        on();
    }

    /**
     * Wrapper for off method
     */
    public void stop()
    {
        off();
    }

    /**
     * OnAction method is call after the graphical changes (set ontext and
     * onImage)
     */
    @Override
    public Boolean onAction()
    {
        logger.log(Level.INFO, "[Sprinklers service] Sprinklers opened");

        if (fireControlCenter == null)
        {
            logger.log(Level.INFO, "[Sprinklers service] FireControllerCenter not connected to the Sprinkler");
        } else
        {
            Status status = new Status();
            status.setId(deviceID);
            status.setStatus("start");
            Calendar cal = GregorianCalendar.getInstance();
            status.setTimestramp(cal.getTime());
            fireControlCenter.notifyStatus(status);

            logger.log(Level.INFO, "[Sprinklers service] FireControllerCenter connected to the Sprinkler send current status : start");
        }
        return true;
    }

    /**
     * OffAction method is call after the graphical changes (set offtext and
     * offImage)
     */
    @Override
    public Boolean offAction()
    {
        logger.log(Level.INFO, "[Sprinklers service] Sprinklers closed");

        if (fireControlCenter == null)
        {
            logger.log(Level.INFO, "[Sprinklers service] FireControllerCenter not connected to the Sprinkler");
        } else
        {
            Status status = new Status();
            status.setId(deviceID);
            status.setStatus("stop");
            fireControlCenter.notifyStatus(status);

            logger.log(Level.INFO, "[Sprinklers service] FireControllerCenter connected to the Sprinklers send current status : stop");
        }
        return true;
    }

    public boolean isStarted()
    {
        return getStatus();
    }

    public String getDeviceID()
    {
        return deviceID;
    }

    @Property
    public void setDeviceID(String deviceID)
    {
        this.deviceID = deviceID;
    }
}
