/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.demo.firedector.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import org.junit.Test;
import org.ow2.frascati.firedetector.model.FireDetectorModelItf;
import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 *
 */
public class FireDetectorDICEPETestCase extends FraSCAtiTestCase
{

    @Test
    public void test()
    {
        Runnable fireDetectorModelRun=(Runnable) getService(Runnable.class, "fire-detector-model-run");
        fireDetectorModelRun.run();
        FireDetectorModelItf fireDetectorModel= getService(FireDetectorModelItf.class, "fire-detector-model-service");
        
        Integer newCOValue=new Integer(12);
        fireDetectorModel.setCurrentCOValue(newCOValue);
        Integer currentCOValue=fireDetectorModel.getCurrentCOValue();
        assertEquals(newCOValue, currentCOValue);
        
        Integer newTooHighCOValue=new Integer(100);
        fireDetectorModel.setCurrentCOValue(newTooHighCOValue);
        currentCOValue=fireDetectorModel.getCurrentCOValue();
        assertNotSame(newTooHighCOValue, currentCOValue);
        
        Integer newLowHighCOValue=new Integer(-100);
        fireDetectorModel.setCurrentCOValue(newLowHighCOValue);
        currentCOValue=fireDetectorModel.getCurrentCOValue();
        assertNotSame(newLowHighCOValue, currentCOValue);
        
        Integer newTemperatureValue=new Integer(60);
        fireDetectorModel.setCurrentTemperatureValue(newTemperatureValue);
        Integer currentTemperatureValue=fireDetectorModel.getCurrentTemperatureValue();
        assertEquals(newTemperatureValue, currentTemperatureValue);
        
        Integer newTooHighTemperatureValue=new Integer(100);
        fireDetectorModel.setCurrentTemperatureValue(newTooHighTemperatureValue);
        currentTemperatureValue=fireDetectorModel.getCurrentTemperatureValue();
        assertNotSame(newTooHighTemperatureValue, currentTemperatureValue);
        
        Integer newLowHighTemperatureValue=new Integer(-100);
        fireDetectorModel.setCurrentTemperatureValue(newLowHighTemperatureValue);
        currentTemperatureValue=fireDetectorModel.getCurrentTemperatureValue();
        assertNotSame(newLowHighTemperatureValue, currentTemperatureValue);
        
        Integer newCOAlertValue=new Integer(15);
        fireDetectorModel.setCurrentCOAlertValue(newCOAlertValue);
        Integer currentCOAlertValue=fireDetectorModel.getCurrentCOAlertValue();
        assertEquals(newCOAlertValue, currentCOAlertValue);
        
        Integer newTemperatureAlertValue=new Integer(60);
        fireDetectorModel.setCurrentTemperatureAlertValue(newTemperatureAlertValue);
        Integer currentTemperatureAlertValue=fireDetectorModel.getCurrentTemperatureAlertValue();
        assertEquals(newTemperatureAlertValue, currentTemperatureAlertValue);
    }
    
}
