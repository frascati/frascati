/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.firedetector.model.listeners.gui;

import itemis.demo.Alarm;
import itemis.demo.AlarmListener;

import java.awt.Color;
import java.awt.Container;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.bean.COBean;
import org.ow2.frascati.esper.bean.TemperatureBean;
import org.ow2.frascati.esper.impl.EventListenerAdaptor;
import org.ow2.frascati.firedetector.model.FireDetectorModel;
import org.ow2.frascati.firedetector.model.FireDetectorModelItf;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJFrame;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJSpinnerNumber;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.SpringUtilities;

/**
 *
 */
@Scope("COMPOSITE")
public class FireDetectorControllerGUI extends EventListenerAdaptor implements Runnable
{
    @Reference
    private FireDetectorModelItf fireDetectorModel;

    @Reference(required = false)
    private AlarmListener alarmListener;
    
    private JLabel coLabel;
    private JSpinner coSpinner;
    private JLabel temperatureLabel;
    private JSpinner temperatureSpinner;
    private JLabel coAlertLabel;
    private JSpinner coAlertSpinner;
    private JLabel temperatureAlertLabel;
    private JSpinner temperatureAlertSpinner;
    private JLabel combinedCOLabel;
    private JSpinner combinedCOAlertSpinner;
    private JLabel combinedTemperatureLabel;
    private JSpinner combinedTemperatureAlertSpinner;

    public void run()
    {
        JFrame mainFrame = new MyJFrame("Conditions Simulator",250, 50, 1300, 50);
        Container content = mainFrame.getContentPane();
        Color backgroundColor=Color.decode("0xFFFF33");
        content.setBackground(backgroundColor);
        content.setLayout(new SpringLayout());

        coLabel = new JLabel();
        coLabel.setText("co");
        content.add(coLabel);
        coSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_CO_VALUE, FireDetectorModel.MAX_CO_VALUE, fireDetectorModel.getCurrentCOValue());
        ChangeListener coSpinnerChangeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentCOValue=fireDetectorModel.getCurrentCOValue();
                if(newValue!=currentCOValue)
                {
                    System.out.println("fd listener set co value");
                    currentCOValue = fireDetectorModel.setCurrentCOValue(newValue);
                    coSpinner.setValue(currentCOValue);
                }
            }
        };
        coSpinner.addChangeListener(coSpinnerChangeListener);
        content.add(coSpinner);
        
        temperatureLabel = new JLabel();
        temperatureLabel.setText("temperature");
        content.add(temperatureLabel);
        temperatureSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_TEMPERATURE_VALUE, FireDetectorModel.MAX_TEMPERATURE_VALUE,fireDetectorModel.getCurrentTemperatureValue());
        ChangeListener temperatureSpinnerChangeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentTemperatureValue=fireDetectorModel.getCurrentTemperatureValue();
                if(newValue!=currentTemperatureValue)
                {
                    currentTemperatureValue = fireDetectorModel.setCurrentTemperatureValue(newValue);
                    temperatureSpinner.setValue(currentTemperatureValue);
                }
            }
        };
        temperatureSpinner.addChangeListener(temperatureSpinnerChangeListener);
        content.add(temperatureSpinner);
        
        coAlertLabel = new JLabel();
        coAlertLabel.setText("maximim co");
        content.add(coAlertLabel);
        coAlertSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_CO_VALUE, FireDetectorModel.MAX_CO_VALUE, fireDetectorModel.getCurrentCOAlertValue());
        ChangeListener coAlertSpinnerChangeListener = new ChangeListener()
        {

            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentCOAlertValue = fireDetectorModel.setCurrentCOAlertValue(newValue);
                coAlertSpinner.setValue(currentCOAlertValue);
            }
        };
        coAlertSpinner.addChangeListener(coAlertSpinnerChangeListener);
        content.add(coAlertSpinner);

        
        temperatureAlertLabel = new JLabel();
        temperatureAlertLabel.setText("maximim temperature");
        content.add(temperatureAlertLabel);
        temperatureAlertSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_TEMPERATURE_VALUE, FireDetectorModel.MAX_TEMPERATURE_VALUE,
                fireDetectorModel.getCurrentTemperatureAlertValue());
        ChangeListener temperatureAlertSpinnerChangeListener = new ChangeListener()
        {

            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentTemperatureAlertValue = fireDetectorModel.setCurrentTemperatureAlertValue(newValue);
                temperatureAlertSpinner.setValue(currentTemperatureAlertValue);
            }
        };
        temperatureAlertSpinner.addChangeListener(temperatureAlertSpinnerChangeListener);
        content.add(temperatureAlertSpinner);

        
        combinedCOLabel = new JLabel();
        combinedCOLabel.setText("maximim combined CO");
        content.add(combinedCOLabel);
        combinedCOAlertSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_CO_VALUE, FireDetectorModel.MAX_CO_VALUE, fireDetectorModel.getCurrentCombinedCOAlertValue());
        ChangeListener combinedCOSpinnerChangeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentcombinedCOAlertValue = fireDetectorModel.setCurrentCombinedCOAlertValue(newValue);
                combinedCOAlertSpinner.setValue(currentcombinedCOAlertValue);
            }

        };
        combinedCOAlertSpinner.addChangeListener(combinedCOSpinnerChangeListener);
        content.add(combinedCOAlertSpinner);

        
        combinedTemperatureLabel = new JLabel();
        combinedTemperatureLabel.setText("maximim combined temperature");
        content.add(combinedTemperatureLabel);
        combinedTemperatureAlertSpinner = new MyJSpinnerNumber(FireDetectorModel.MIN_TEMPERATURE_VALUE, FireDetectorModel.MAX_TEMPERATURE_VALUE,
                fireDetectorModel.getCurrentCombinedTemperatureAlertValue());
        ChangeListener combinedTemperatureSpinnerChangeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSpinner source = (JSpinner) e.getSource();
                Integer newValue = (Integer) source.getValue();
                Integer currentcombinedTemperatureAlertValue = fireDetectorModel.setCurrentCombinedTemperatureAlertValue(newValue);
                combinedTemperatureAlertSpinner.setValue(currentcombinedTemperatureAlertValue);
            }

        };
        combinedTemperatureAlertSpinner.addChangeListener(combinedTemperatureSpinnerChangeListener);
        content.add(combinedTemperatureAlertSpinner);

        
        SpringUtilities.makeCompactGrid(content, 6, 2, 0, 0, 0, 0);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#update(java.lang.String, java.util.List)
     */
    public void update(String eventID, List<CEPBean> beans)
    {
        for (CEPBean tmpBean : beans)
        {
            if(FireDetectorModel.CO_STATEMENT_NAME.equals(eventID))
            {
                COBean coBean=(COBean) tmpBean.getUnderlyingBean();
                Integer beanCOValue = (int) coBean.getCoRate();
                Integer currentCoValue=(Integer) coSpinner.getValue();
                if(currentCoValue!=beanCOValue)
                {
                    coSpinner.setValue(beanCOValue);
                }
            }
            if(FireDetectorModel.TEMPERATURE_STATEMENT_NAME.equals(eventID))
            {
                TemperatureBean temperatureBean=(TemperatureBean) tmpBean.getUnderlyingBean();
                Integer beanTemperatureValue = temperatureBean.getTemperature();
                Integer currentTemperatureValue=(Integer) temperatureSpinner.getValue();
                if(currentTemperatureValue!=beanTemperatureValue)
                {
                    temperatureSpinner.setValue(beanTemperatureValue);                    
                }
            }
            else if(eventID.contains("Alert"))
            {
                Alarm alarm=new Alarm();
                alarm.setId("fire detector");
                Calendar calendar=GregorianCalendar.getInstance();
                Date date=calendar.getTime();
                alarm.setTimestramp(date);
                alarmListener.notifyAlarm(alarm);
            }
        }
        
    }
}
