/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.firedetector.model.listeners.gui.utils;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 *
 */
public class MyJFrame extends JFrame
{
    private static final long serialVersionUID = 9084502325796517835L;

    public MyJFrame(String title,int width, int heigth, int locationX, int locationY)
    {
        super();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle(title);
        Dimension frameDimension = new Dimension(width,heigth);
        setMinimumSize(frameDimension);
        setResizable(false);
        setLocation(locationX, locationY);
    }
    
    public MyJFrame(int width, int heigth, int locationX, int locationY)
    {
        this("",width,heigth,locationX,locationY);
    }
}
