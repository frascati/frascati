/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.firedetector.model.listeners;

import java.util.Date;
import java.util.List;

import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.bean.COBean;
import org.ow2.frascati.esper.impl.EventListenerAdaptor;
import org.ow2.frascati.firedetector.model.FireDetectorModel;

/**
 *
 */
@Scope("COMPOSITE")
public class COListener extends EventListenerAdaptor
{
    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#update(java.lang.String, java.util.List)
     */
    public void update(String eventID, List<CEPBean> beans)
    {
        for(CEPBean tmpBean : beans)
        {
            COBean coBean=(COBean) tmpBean.getUnderlyingBean();
            Date date=new Date(coBean.getTimestamp());
            
            String message;
            if(FireDetectorModel.CO_STATEMENT_NAME.equals(eventID))
            {
                message="INFO : ";
            }
            else if(FireDetectorModel.CO_ALERT_STATEMENT_NAME.equals(eventID))
            {
                message="ALERT : ";
            }
            else
            {
                message="Unknown statement : ";
            }
            message+=coBean.getCoRate()+" percent of CO detected on"+date;
            System.out.println(message);
        }
        
    }
}
