/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.firedetector.model;

import java.util.logging.Logger;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.EsperEngine;
import org.ow2.frascati.esper.api.bean.JAXBBean;
import org.ow2.frascati.esper.bean.COBean;
import org.ow2.frascati.esper.bean.TemperatureBean;

/**
 *
 */
@Scope("COMPOSITE")
public class FireDetectorModel implements Runnable,FireDetectorModelItf
{
    private static final Logger logger=Logger.getLogger(FireDetectorModel.class.getName());

    public static final String CO_STATEMENT_NAME="CO";
    public static final int MIN_CO_VALUE=0;
    public static final int MAX_CO_VALUE=20;
    public static final int DEFAULT_CO_ALERT_VALUE=10;
    public static final String CO_ALERT_STATEMENT_NAME="COAlert";
    private static final String CO_ALERT_STATEMENT_VALUE="select * from COBean where coRate > ";
    
    public static final String TEMPERATURE_STATEMENT_NAME="Temperature";
    public static final int MIN_TEMPERATURE_VALUE=15;
    public static final int MAX_TEMPERATURE_VALUE=80;
    private static final int DEFAULT_TEMPERATURE_ALERT_VALUE=50;
    public static final String TEMPERATURE_ALERT_STATEMENT_NAME="TemperatureAlert";
    public static final String TEMPERATURE_ALERT_STATEMENT_VALUE="select * from TemperatureBean where temperature > ";
    
    public static final int DEFAULT_COMBINED_CO_ALERT_VALUE=5;
    public static final int DEFAULT_COMBINED_TEMPERATURE_ALERT_VALUE=40;
    public static final String COMBINED_ALERT_STATEMENT_NAME="CombinedAlert";
    private static final String COMBINED_ALERT_STATEMENT_VALUE1="SELECT * FROM COBean.std:lastevent() AS co, TemperatureBean.std:lastevent() AS heat WHERE ((co.coRate> ";
    private static final String COMBINED_ALERT_STATEMENT_VALUE2=") and (heat.temperature > ";
    private static final String COMBINED_ALERT_STATEMENT_VALUE3="))";
    
    private int currentCOValue;
    private int currentCOAlertValue;
    private int currentCombinedCOAlertValue;

    private int currentTemperatureValue;
    private int currentTemperatureAlertValue;
    private int currentCombinedTemperatureAlertValue;
    
    @Reference
    private EsperEngine esperEngine;
    
    @Init
    public void run()
    {
        this.currentCOValue=FireDetectorModel.MIN_CO_VALUE;
        this.currentCOAlertValue=FireDetectorModel.DEFAULT_CO_ALERT_VALUE;
        this.currentCombinedCOAlertValue=FireDetectorModel.DEFAULT_COMBINED_CO_ALERT_VALUE;
        
        this.currentTemperatureValue=FireDetectorModel.MIN_TEMPERATURE_VALUE;
        this.currentTemperatureAlertValue=FireDetectorModel.DEFAULT_TEMPERATURE_ALERT_VALUE;
        this.currentCombinedTemperatureAlertValue=FireDetectorModel.DEFAULT_COMBINED_TEMPERATURE_ALERT_VALUE;
        updateStatements();
    }
    
    private void sendCOEvent()
    {
        COBean cobean=new COBean("CO",currentCOValue);
        esperEngine.sendEvent(new JAXBBean(cobean));
    }
    
    private void sendTemperatureEvent()
    {
        TemperatureBean temperatureBean=new TemperatureBean("T",currentTemperatureValue);
        esperEngine.sendEvent(new JAXBBean(temperatureBean));
    }
    
    private void updateCOAlertStatement()
    {
        String newStatement=FireDetectorModel.CO_ALERT_STATEMENT_VALUE+currentCOAlertValue;
        esperEngine.updateStatement(FireDetectorModel.CO_ALERT_STATEMENT_NAME, newStatement);
    }
    
    private void updateTemperatureAlertStatement()
    {
        String newStatement=FireDetectorModel.TEMPERATURE_ALERT_STATEMENT_VALUE+currentTemperatureAlertValue;
        esperEngine.updateStatement(FireDetectorModel.TEMPERATURE_ALERT_STATEMENT_NAME, newStatement);
    }
    
    private void updateCombinedCOTemperatureAlertStatement()
    {
        String newStatement=FireDetectorModel.COMBINED_ALERT_STATEMENT_VALUE1+currentCombinedCOAlertValue;
        newStatement+=FireDetectorModel.COMBINED_ALERT_STATEMENT_VALUE2+currentCombinedTemperatureAlertValue;
        newStatement+=FireDetectorModel.COMBINED_ALERT_STATEMENT_VALUE3;
        esperEngine.updateStatement(COMBINED_ALERT_STATEMENT_NAME, newStatement);
    }
    
    public void updateStatements()
    {
        updateCOAlertStatement();
        updateTemperatureAlertStatement();
        updateCombinedCOTemperatureAlertStatement();
    }
    
    public Integer getCurrentCOValue()
    {
        return currentCOValue;
    }

    public Integer setCurrentCOValue(Integer newValue)
    {
        int formatedCOValue=formatCOValue(newValue);
        this.currentCOValue=formatedCOValue;
        sendCOEvent();
        return currentCOValue;
    }

    public Integer getCurrentCOAlertValue()
    {
        return currentCOAlertValue;
    }

    public Integer setCurrentCOAlertValue(Integer newValue)
    {
        int formatedCOAlertValue=formatCOValue(newValue);
        this.currentCOAlertValue=formatedCOAlertValue;
        updateCOAlertStatement();
        sendCOEvent();
        return currentCOAlertValue;
    }

    public Integer getCurrentCombinedCOAlertValue()
    {
        return currentCombinedCOAlertValue;
    }

    public Integer setCurrentCombinedCOAlertValue(Integer newValue)
    {
        int formatedCombinedCOAlertValue=formatCOValue(newValue);
        this.currentCombinedCOAlertValue = formatedCombinedCOAlertValue;
        updateCombinedCOTemperatureAlertStatement();
        sendCOEvent();
        sendTemperatureEvent();
        return currentCombinedCOAlertValue;
    }

    public Integer getCurrentTemperatureValue()
    {
        return currentTemperatureValue;
    }

    public Integer setCurrentTemperatureValue(Integer newValue)
    {
        int formatedTemperatureValue=formatTemperatureValue(newValue);
        this.currentTemperatureValue=formatedTemperatureValue;
        sendTemperatureEvent();
        return currentTemperatureValue;
    }

    public Integer getCurrentTemperatureAlertValue()
    {
        return currentTemperatureAlertValue;
    }

    public Integer setCurrentTemperatureAlertValue(Integer newValue)
    {
        int formatedTemperatureAlertValue=formatTemperatureValue(newValue);
        this.currentTemperatureAlertValue=formatedTemperatureAlertValue;
        updateTemperatureAlertStatement();
        sendTemperatureEvent();
        return currentTemperatureAlertValue;
    }

    public Integer getCurrentCombinedTemperatureAlertValue()
    {
        return currentCombinedTemperatureAlertValue;
    }

    public Integer setCurrentCombinedTemperatureAlertValue(Integer newValue)
    {
        int formatedCombinedTemperatureAlertValue=formatTemperatureValue(newValue);
        this.currentCombinedTemperatureAlertValue = formatedCombinedTemperatureAlertValue;
        updateCombinedCOTemperatureAlertStatement();
        sendCOEvent();
        sendTemperatureEvent();
        return currentCombinedTemperatureAlertValue;
    }

    private int formatValue(int value, int min, int max)
    {
        int correctedValue;
        
        if(value<min)
        {
            correctedValue=min;
            logger.severe("The value "+value+" is to low, set it to minimum : "+min);
        }
        else if(value>max)
        {
            correctedValue=max;
            logger.severe("The value "+value+" is to high, set it to maximum : "+max);
        }
        else
        {
            correctedValue=value;
        }
        
        return correctedValue;
    }
    
    private int formatCOValue(int value)
    {
        return formatValue(value, FireDetectorModel.MIN_CO_VALUE, FireDetectorModel.MAX_CO_VALUE);
    }
    
    private int formatTemperatureValue(int value)
    {
        return formatValue(value, FireDetectorModel.MIN_TEMPERATURE_VALUE, FireDetectorModel.MAX_TEMPERATURE_VALUE);
    }
}
