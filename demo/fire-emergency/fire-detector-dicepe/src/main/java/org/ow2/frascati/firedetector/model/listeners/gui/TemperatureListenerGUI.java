/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.firedetector.model.listeners.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.bean.TemperatureBean;
import org.ow2.frascati.esper.impl.EventListenerAdaptor;
import org.ow2.frascati.firedetector.model.FireDetectorModel;
import org.ow2.frascati.firedetector.model.FireDetectorModelItf;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJFrame;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJSlider;

/**
 *
 */
@Scope("COMPOSITE")
public class TemperatureListenerGUI extends EventListenerAdaptor implements Runnable
{
    @Reference
    private FireDetectorModelItf fireDetectorModel;
    
    private JSlider temperatureValueSlider;
    
    public void run()
    {
        JFrame mainFrame = new MyJFrame(150, 500, 1100, 50);
        Container container = mainFrame.getContentPane();

        temperatureValueSlider = new MyJSlider("Temperature C", FireDetectorModel.MIN_TEMPERATURE_VALUE, FireDetectorModel.MAX_TEMPERATURE_VALUE, fireDetectorModel.getCurrentTemperatureValue());
        temperatureValueSlider.setOrientation(JSlider.VERTICAL);
        ChangeListener changeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider source = (JSlider) e.getSource();
                Integer newValue = source.getValue();
                if (fireDetectorModel.getCurrentTemperatureValue() != newValue)
                {
                    fireDetectorModel.setCurrentTemperatureValue(newValue);
                }
            }
        };
        temperatureValueSlider.addChangeListener(changeListener);
        container.add(temperatureValueSlider, BorderLayout.CENTER);

        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#update(java.lang.String, java.util.List)
     */
    public void update(String eventID, List<CEPBean> beans)
    {
        for (CEPBean tmpBean : beans)
        {
            TemperatureBean temperatureBean = (TemperatureBean) tmpBean.getUnderlyingBean();
            Integer temperature = temperatureBean.getTemperature();
            temperatureValueSlider.setValue(temperature);
            
//            if(FireDetectorModel.TEMPERATURE_STATEMENT_NAME.equals(eventID))
//            {
//                temperatureValueSlider.setBackground(Color.white);
//                temperatureValueSlider.repaint();
//            }
//            else if(FireDetectorModel.TEMPERATURE_ALERT_STATEMENT_NAME.equals(eventID))
//            {
//                temperatureValueSlider.setBackground(Color.red);
//                temperatureValueSlider.repaint();
//            }
        }
    }
}
