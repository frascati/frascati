/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.firedetector.launcher;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

/**
 *
 */
public class FireEmergencyDiCEPeLauncher
{
    public static final String COMPOSITE = "firedetector-dicepe_localhost.composite";
    
    private static FraSCAti frascati;
    private static Component composite;
    
    public static void main(String[] args) throws FrascatiException, InterruptedException
    {
        frascati = FraSCAti.newFraSCAti();
        composite = frascati.getComposite(COMPOSITE);

        System.out.println("********************Get Esper Engine Service********************");
        runService("fire-detector-model-run");
        runService("run-temperature-gui");
        runService("run-co-gui");
        runService("run-controller-gui");
        System.out.println("********************Send Terminal Event********************");
        
        Thread.currentThread().join();
    }
    
    private static void runService(String serviceName) throws FrascatiException, InterruptedException
    {
        Runnable runnableService=(Runnable) frascati.getService(composite,serviceName, Runnable.class);
        runnableService.run();
    }
}
