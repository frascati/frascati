/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.firedetector.model.listeners.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.esper.api.bean.CEPBean;
import org.ow2.frascati.esper.bean.COBean;
import org.ow2.frascati.esper.impl.EventListenerAdaptor;
import org.ow2.frascati.firedetector.model.FireDetectorModel;
import org.ow2.frascati.firedetector.model.FireDetectorModelItf;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJFrame;
import org.ow2.frascati.firedetector.model.listeners.gui.utils.MyJSlider;

/**
 *
 */
@Scope("COMPOSITE")
public class COListenerGUI extends EventListenerAdaptor implements Runnable
{
    @Reference
    private FireDetectorModelItf fireDetectorModel;
    
    private JSlider coValueSlider;
    
    public void run()
    {
        JFrame mainFrame = new MyJFrame(150, 500, 900, 50);
        Container container = mainFrame.getContentPane();

        coValueSlider = new MyJSlider("CO rate", FireDetectorModel.MIN_CO_VALUE, FireDetectorModel.MAX_CO_VALUE, fireDetectorModel.getCurrentCOValue());
        coValueSlider.setOrientation(JSlider.VERTICAL);
        ChangeListener changeListener = new ChangeListener()
        {
            public void stateChanged(ChangeEvent e)
            {
                JSlider source = (JSlider) e.getSource();
                Integer newValue = source.getValue();
                if (fireDetectorModel.getCurrentCOValue() != newValue)
                {
                    fireDetectorModel.setCurrentCOValue(newValue);
                }
            }
        };
        coValueSlider.addChangeListener(changeListener);
        container.add(coValueSlider, BorderLayout.CENTER);

        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    /* (non-Javadoc)
     * @see org.ow2.frascati.esper.api.EventListener#update(java.lang.String, java.util.List)
     */
    public void update(String eventID, List<CEPBean> beans)
    {
        for (CEPBean tmpBean : beans)
        {
            COBean coBean = (COBean) tmpBean.getUnderlyingBean();
            Integer coRate = (int) coBean.getCoRate();
            coValueSlider.setValue(coRate);
            
//            if(FireDetectorModel.CO_STATEMENT_NAME.equals(eventID))
//            {
//                coValueSlider.setBackground(Color.white);
//                coValueSlider.repaint();
//            }
//            else if(FireDetectorModel.CO_ALERT_STATEMENT_NAME.equals(eventID))
//            {
//                coValueSlider.setBackground(Color.red);
//                coValueSlider.repaint();
//            }
        }
    }
}
