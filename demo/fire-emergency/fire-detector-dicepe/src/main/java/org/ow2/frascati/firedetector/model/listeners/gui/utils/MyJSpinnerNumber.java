/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.firedetector.model.listeners.gui.utils;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;

import org.apache.log4j.Logger;

/**
 *
 */
public class MyJSpinnerNumber extends JSpinner
{
    private static final Logger logger=Logger.getLogger(MyJSpinnerNumber.class.getName());
    
    private static final long serialVersionUID = 3693186583385872184L;

    private JFormattedTextField textfield;
    
    private Integer currentValue;
    
    public MyJSpinnerNumber(Integer minimum, Integer maximum, Integer value)
    {
        super();
        SpinnerNumberModel spinnerNumberModel=new SpinnerNumberModel();
        spinnerNumberModel.setMinimum(minimum);
        spinnerNumberModel.setMaximum(maximum);
        spinnerNumberModel.setStepSize(1);
        spinnerNumberModel.setValue(value);
        this.currentValue=value;
        setModel(spinnerNumberModel);
        
        DefaultEditor editor = (DefaultEditor) getEditor();
        this.textfield = editor.getTextField();
        DefaultFormatter formatter=(DefaultFormatter) this.textfield.getFormatter();
        formatter.setCommitsOnValidEdit(false);
        formatter.setAllowsInvalid(true);
        this.textfield.addKeyListener(new KeyAdapter()
        {
            private MyJSpinnerNumber parent=MyJSpinnerNumber.this;
            
            @Override
            public void keyReleased(final KeyEvent e)
            {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                {
                    try
                    {
                        Integer.valueOf(textfield.getText());
                        parent.fireStateChanged();
                    }
                    catch (NumberFormatException nfe)
                    {
                        textfield.setValue(parent.getCurrentValue());
                    }
                }
            }
        });
    }
    
  @Override
  public void addChangeListener(ChangeListener listener)
  {
      MyJSpinnerNumberChangeListener spinnerNumberChangeListener=new MyJSpinnerNumberChangeListener(listener);
      super.addChangeListener(spinnerNumberChangeListener);
  }
  
  @Override
  public void setValue(Object newValue)
  {
      try
      {
          Integer newValueInt=(Integer) newValue;
          super.setValue(newValueInt);
          currentValue=newValueInt;
      }
      catch(ClassCastException cce)
      {
          logger.info("Try to set MyJSpinnerNumber with a Object that is not an Integer");
      }
  }
  
    private boolean isValueChanged()
    {
        Integer value=(Integer) getValue();
        Integer textFieldIntValue=Integer.valueOf(textfield.getText());
        if(currentValue!=value || currentValue!=textFieldIntValue)
        {
            return true;
        }
        return false;
    }

    private void updateCurrentValue()
    {
        String currentTextFieldValue=textfield.getText();
        Integer currentIntegerTextFieldValue=Integer.valueOf(currentTextFieldValue);
        this.currentValue=currentIntegerTextFieldValue;
    }
    
    private Integer getCurrentValue()
    {
        return currentValue;
    }
    
    private class MyJSpinnerNumberChangeListener implements ChangeListener
    {
        private MyJSpinnerNumber parent=MyJSpinnerNumber.this;
        
        private ChangeListener listener;
        
        public MyJSpinnerNumberChangeListener(ChangeListener listener)
        {
            this.listener=listener;
        }
        
        public void stateChanged(ChangeEvent e)
        {
            if(parent.isValueChanged())
            {
                parent.updateCurrentValue();
                listener.stateChanged(e);
            }
        }
    }
}
