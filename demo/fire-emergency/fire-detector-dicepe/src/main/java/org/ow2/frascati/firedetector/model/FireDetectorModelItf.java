/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.firedetector.model;

/**
 * Event Simulator Interface
 */
public interface FireDetectorModelItf
{
    /**
     * @return
     */
    public Integer getCurrentCOValue();
    
    /**
     * Set the current Carbon monoxide value for the event to send
     * 
     * @param newCOValue the new current value of carbon monoxide 
     * @return the current value of carbon monoxide formated
     */
    public Integer setCurrentCOValue(Integer newCOValue);
    
    /**
     * @return
     */
    public Integer getCurrentTemperatureValue();
    
    /**
     * Set the current temperature value for the event to send
     * 
     * @param newTemperatureValue the new current value of temperature
     * @return the current value of temperature formated
     */
    public Integer setCurrentTemperatureValue(Integer newTemperatureValue);
    
    /**
     * @return
     */
    public Integer getCurrentCOAlertValue();
    
    /**
     * Set the threshold CO alert value of the CEP statement
     * 
     * @param newCOAlertValue the value of the CO threshold alert
     * @return the current value of the threshold CO alert formated
     */
    public Integer setCurrentCOAlertValue(Integer newCOAlertValue);

    /**
     * @return
     */
    public Integer getCurrentTemperatureAlertValue();
    
    /**
     * Set the threshold temperature alert value of the CEP statement
     * 
     * @param newTemperatureAlertValue the value of the temperature threshold alert
     * @return the current value of the threshold temperature alert formated
     */
    public Integer setCurrentTemperatureAlertValue(Integer newTemperatureAlertValue);
    
    /**
     * @return
     */
    public Integer getCurrentCombinedCOAlertValue();
    
    /**
     * Set the threshold CO alert value of the CEP statement that combined temperature and CO criteria
     * 
     * @param newCOCombinedAlertValue the value of the CO threshold combined alert
     * @return the current value of the threshold CO combined alert formated
     */
    public Integer setCurrentCombinedCOAlertValue(Integer newCOCombinedAlertValue);

    /**
     * @return
     */
    public Integer getCurrentCombinedTemperatureAlertValue();
    
    /**
     * Set the threshold temperature alert value of the CEP statement that combined temperature and CO criteria
     * 
     * @param newTemperatureCombinedAlertValue the value of the temperature threshold combined alert
     * @return the current value of the threshold temperature combined alert formated
     */
    public Integer setCurrentCombinedTemperatureAlertValue(Integer newTemperatureCombinedAlertValue);
}
