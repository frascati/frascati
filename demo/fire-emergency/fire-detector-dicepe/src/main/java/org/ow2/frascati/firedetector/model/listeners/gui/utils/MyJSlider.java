/**
 * OW2 FraSCAti Assembly Factory
 * Copyright (C) 2008-2012 Inria, University of Lille
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */


package org.ow2.frascati.firedetector.model.listeners.gui.utils;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 */
public class MyJSlider extends JSlider
{

    private static final long serialVersionUID = -644363118330546759L;

    private int currentValue;
    
    public MyJSlider(String title, Integer minimum, Integer maximum, Integer value)
    {
        super();
        TitledBorder coBorder=BorderFactory.createTitledBorder(title);
        this.setBorder(coBorder);
        this.setMinimum(minimum);
        this.setMaximum(maximum);
        this.currentValue=value;
        this.setValue(value);
        this.setMinorTickSpacing(1);
        this.setMajorTickSpacing(5);
        this.setPaintTicks(true);
        this.setPaintLabels(true);
        Color backgroundColor=Color.decode("0xFFFF33");
        this.setBackground(backgroundColor);
    }
    
    @Override
    public void addChangeListener(ChangeListener listener)
    {
        MyJSliderChangeListener sliderChangeListener=new MyJSliderChangeListener(listener);
        super.addChangeListener(sliderChangeListener);
    }
    
    private boolean isChanged()
    {
        if(currentValue!=getValue())
        {
            return true;
        }
        return false;
    }

    private void updateCurrentValue()
    {
        this.currentValue=this.getValue();
    }

    private class MyJSliderChangeListener implements ChangeListener
    {
        private ChangeListener listener;
        
        public MyJSliderChangeListener(ChangeListener listener)
        {
            this.listener=listener;
        }
       
        public void stateChanged(ChangeEvent e)
        {
            if(!MyJSlider.this.getValueIsAdjusting() && MyJSlider.this.isChanged())
            {
                MyJSlider.this.updateCurrentValue();
                listener.stateChanged(e);
            }
        }
    }
    
}
