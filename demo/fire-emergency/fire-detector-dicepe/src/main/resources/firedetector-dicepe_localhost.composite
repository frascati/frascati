<?xml version="1.0" encoding="UTF-8"?>
<!--  Copyright (C) 2008-2012 Inria, University of Lille 							 -->
<!--																				 -->
<!--  This library is free software; you can redistribute it and/or 				 -->
<!--  modify it under the terms of the GNU Lesser General Public 					 -->
<!--  License as published by the Free Software Foundation; either 					 -->
<!--  version 2 of the License, or (at your option) any later version. 				 -->
<!--																				 -->
<!--  This library is distributed in the hope that it will be useful, 				 -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of 				 -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 			 -->
<!--  Lesser General Public License for more details. 								 -->
<!--																				 -->
<!--  You should have received a copy of the GNU Lesser General Public 				 -->
<!--  License along with this library; if not, write to the Free Software 			 -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 	 -->
<!-- 																				 -->
<!--  Contact: frascati@ow2.org 													 -->
<!--                                                                                 -->
<!--  Author: Gwenael CATTEZ                                                     	 -->
<!-- 																				 -->
<!--  Contributors : 				                                                 -->

<composite xmlns="http://www.osoa.org/xmlns/sca/1.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:esper="org/ow2/frascati/esper"
	xmlns:esperEvents="http://frascati.ow2.org/esper" xsi:schemaLocation="http://frascati.ow2.org/esper Event.xsd"
	xmlns:frascati="http://frascati.ow2.org/xmlns/sca/1.1" xmlns:stmt="statements"
	xmlns:lst="listeners" name="firedetector-dicepe_localhost">

	<service name="esper-engine-service" promote="esper-engine/EsperEngine">
	</service>
	<service name="fire-detector-model-run" promote="fire-detector-model/run" />
	<service name="fire-detector-model-service" promote="fire-detector-model/fire-detector-model-service">
		<binding.ws uri="http://localhost:9002/FireDetectorModelService" />
	</service>
	<service name="run-temperature-gui" promote="temperature-listener-gui/run-temperature-gui"></service>
	<service name="run-co-gui" promote="co-listener-gui/run-co-gui"></service>
	<service name="run-controller-gui" promote="fire-detector-controller/run-controller-gui"></service>

	<component name="esper-engine" constrainingType="esper:Engine">
		<implementation.java class="org.ow2.frascati.esper.impl.EsperEngineImpl" />
		<property name="events" type="esperEvents:Events">
			<esperEvents:Events>
				<esperEvents:Event esperEvents:event-type='CLASS'>org.ow2.frascati.esper.bean.COBean</esperEvents:Event>
				<esperEvents:Event esperEvents:event-type='CLASS'>org.ow2.frascati.esper.bean.TemperatureBean</esperEvents:Event>
			</esperEvents:Events>
		</property>
	</component>

	<component name="fire-detector-model">
		<implementation.java class="org.ow2.frascati.firedetector.model.FireDetectorModel" />

		<reference name="esperEngine" target="esper-engine/EsperEngine"/>

		<service name="fire-detector-model-service">
			<interface.java interface="org.ow2.frascati.firedetector.model.FireDetectorModelItf" />
		</service>
		<service name="run">
			<interface.java interface="java.lang.Runnable" />
		</service>
	</component>

	<component name="temperature-statement">
		<implementation.composite name="stmt:temperature-statement" />
	</component>

	<component name="temperature-alert-statement">
		<implementation.composite name="stmt:temperature-alert-statement"/>
	</component>

	<component name="co-statement">
		<implementation.composite name="stmt:co-statement" />
	</component>

	<component name="co-alert-statement">
		<implementation.composite name="stmt:co-alert-statement" />
	</component>

	<component name="combined-alert-statement">
		<implementation.composite name="stmt:combined-alert-statement" />
	</component>

	<component name="temperature-listener">
		<implementation.composite name="lst:temperature-listener" />
	</component>

	<component name="co-listener">
		<implementation.composite name="lst:co-listener" />
	</component>

	<component name="temperature-listener-gui">
		<implementation.composite name="lst:temperature-listener-gui" />
		<reference name="fireDetectorModel" target="fire-detector-model/fire-detector-model-service" />
	</component>

	<component name="co-listener-gui">
		<implementation.composite name="lst:co-listener-gui" />
		<reference name="fireDetectorModel" target="fire-detector-model/fire-detector-model-service" />
	</component>

	<component name="fire-detector-controller">
		<implementation.composite name="lst:fire-detector-controller" />
		<reference name="fireDetectorModel" target="fire-detector-model/fire-detector-model-service" />
	</component>

</composite>