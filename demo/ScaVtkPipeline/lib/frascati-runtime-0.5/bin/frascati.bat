@echo off

rem OW2 FraSCAti distribution
rem Copyright (C) 2008-2009 INRIA, USTL
rem
rem This library is free software; you can redistribute it and/or
rem modify it under the terms of the GNU Lesser General Public
rem License as published by the Free Software Foundation; either
rem version 2 of the License, or (at your option) any later version.
rem
rem This library is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
rem Lesser General Public License for more details.
rem
rem You should have received a copy of the GNU Lesser General Public
rem License along with this library; if not, write to the Free Software
rem Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
rem USA

@setlocal

if defined JAVA_HOME (
  set JAVA=%JAVA_HOME%\bin\java
  set JAVAC=%JAVA_HOME%\bin\javac
  set JAR=%JAVA_HOME%\bin\jar
  set JAVA_OPTS=-Xms16m -Xmx32m
) else (
  echo The JAVA_HOME variable is not set. Please initialize it in your environment
)

if exist %FRASCATI_HOME% goto CONFIG

set FRASCATI_HOME=%~dp0
rem Removes the "/bin/" string at the end of this script path
echo %FRASCATI_HOME%
set FRASCATI_HOME=%FRASCATI_HOME:~0,-5%
echo The FRASCATI_HOME variable is not set. Using %FRASCATI_HOME% as default value.

:CONFIG
set FRASCATI_LIB=%FRASCATI_HOME%\lib
set FRASCATI_MAIN=org.ow2.frascati.factory.FactoryCommandLine
set LOGGING=%FRASCATI_HOME%\conf\logging.properties
set LAUNCHER_MAIN=org.ow2.frascati.Launcher
set LAUNCHER_LIB=%FRASCATI_LIB%\frascati-runtime-0.5.jar

set FRACTAL_PROVIDER=org.objectweb.fractal.julia.Julia
set JULIA_CONFIG=julia.cfg
set ADL_LAUNCHER=org.objectweb.fractal.adl.Launcher
set ADL_FILE=org.ow2.frascati.explorer.FrascatiExplorer(FraSCAti Explorer)

set FRASCATI_CMD=%1
set PARAMS=%*

if "%1" == "compile" (
	echo Compiling ...
    goto COMPILE	
) else if "%1" == "run" (
	echo Running OW2 FraSCAti ...
	"%JAVA%" %JAVA_OPTS% -Djava.security.policy="%FRASCATI_HOME%\conf\java.policy" -Djava.util.logging.config.file="%LOGGING%" -cp "%LAUNCHER_LIB%" %LAUNCHER_MAIN% %FRASCATI_MAIN% -lib "%FRASCATI_LIB%" %PARAMS:~4%
	echo Exiting OW2 FraSCAti ...
	goto END
) else if "%1" == "explorer" (
	echo Running the OW2 FraSCAti Explorer ...
    "%JAVA%" %JAVA_OPTS% -Dfractal.provider=%FRACTAL_PROVIDER% -Djulia.config=%JULIA_CONFIG% -cp "%LAUNCHER_LIB%" %LAUNCHER_MAIN% %ADL_LAUNCHER% -lib "%FRASCATI_LIB%" -fractal "%ADL_FILE%" r
	echo Exiting OW2 FraSCAti Explorer ...
	goto END
) else (
	echo "Usage: frascati {compile|run|explorer}"
	goto END
)

:COMPILE
shift
if "%1"=="" goto COMPILE_USAGE
if "%2"=="" goto COMPILE_USAGE

set CURRENT_DIR=%CD%
set OUTPUT=tmp
mkdir %OUTPUT%

rem Search java sources in .\src\...
setlocal ENABLEDELAYEDEXPANSION
IF ERRORLEVEL 1 (
  echo Impossible d'activer les extensions
  goto END
)
set SRC_FILES=
for /r %1 %%X in (*.java) do (set SRC_FILES=!SRC_FILES! "%%X")
setlocal DISABLEDELAYEDEXPANSION
set SRC_FILES=%SRC_FILES:\=/%
echo %SRC_FILES% > javasrc.tmp~

if ERRORLEVEL 1 (
  echo Cannot find Java source files in %1
  goto END
)

rem Compile Java sources
"%JAVAC%" -d "%OUTPUT%" -cp "%FRASCATI_LIB%\sca-api-0.90-incubating.jar" @javasrc.tmp~
rem Copy composite files
for /r %1 %%X in (*.composite) do (copy /Y "%%X" "%OUTPUT%")

rem Build jar file
cd %OUTPUT%
"%JAR%" cf ../%2.jar *
cd %CURRENT_DIR%
echo Library %2.jar created

rem clean up
rmdir /S/Q "%OUTPUT%"
del /Q javasrc.tmp~

goto END

:COMPILE_USAGE
echo Usage: frascati compile [src] [name]
echo [src]  = directory of the sources to compile
echo [name] = name of the jar to build

:END