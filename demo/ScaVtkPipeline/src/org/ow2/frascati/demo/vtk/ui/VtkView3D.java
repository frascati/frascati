/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.ui;

import java.awt.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;

import vtk.vtkActor;
import vtk.vtkDataSetMapper;
import vtk.vtkDataSetReader;
import vtk.vtkLookupTable;
import vtk.vtkPanel;

/**
 * Implementation based on VTK
 */
public class VtkView3D implements View3D {
	private vtkPanel panel;
	private HashMap<String, vtkDataSetMapper> mappers;
	private HashMap<String, vtkLookupTable> luts;
	private HashMap<String, vtkActor> actors;

	public VtkView3D( vtkPanel panel ) {
		this.panel = panel;
		mappers = new HashMap<String, vtkDataSetMapper>();
		actors = new HashMap<String, vtkActor>();
		luts = new HashMap<String, vtkLookupTable>();
	}

	public Collection<String> getAvailableNodes() {
		return mappers.keySet();
	}

	public void load( String nodeId , byte[] data ) {
		remove(nodeId);
		//
		try {
			File tmpFile = File.createTempFile("vtkDS", ".vtk");
			FileOutputStream fos = new FileOutputStream(tmpFile);
			fos.write(data);
			fos.close();
			//
			vtkLookupTable lut = new vtkLookupTable();
			lut.SetHueRange(0.6666, 0);
			vtkDataSetReader reader = new vtkDataSetReader();
			reader.SetFileName(tmpFile.getAbsolutePath());
			reader.GetOutput().Update();
			vtkDataSetMapper mapper = new vtkDataSetMapper();
			vtkActor actor = new vtkActor();
			actor.SetMapper(mapper);
			mapper.SetInputConnection(reader.GetOutputPort());
			mapper.SetLookupTable(lut);
			panel.GetRenderer().AddActor(actor);
			//
			mapper.SetScalarRange(reader.GetOutput().GetScalarRange());
			lut.SetRange(reader.GetOutput().GetScalarRange());
			//
			mappers.put(nodeId, mapper);
			actors.put(nodeId, actor);
			luts.put(nodeId, lut);
		} catch (Exception e) {
			e.printStackTrace();
		}
		panel.Render();
	}

	public void remove( String nodeId ) {
		if (mappers.containsKey(nodeId)) {
			panel.GetRenderer().RemoveActor(actors.get(nodeId));
			mappers.remove(nodeId);
			actors.remove(nodeId);
			luts.remove(nodeId);
		}
	}

	public void resetView() {
		panel.GetRenderer().ResetCamera();
	}

	public Component getUI() {
		return panel;
	}

}
