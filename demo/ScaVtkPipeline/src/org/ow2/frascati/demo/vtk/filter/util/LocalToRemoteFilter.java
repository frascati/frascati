/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.filter.util;

import java.util.Properties;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.demo.vtk.api.LocalFilter;
import org.ow2.frascati.demo.vtk.api.RemoteFilter;

/**
 * Adapter filter use to convert a local filter to a remote filter.
 */
public class LocalToRemoteFilter implements RemoteFilter {
	private LocalFilter filter;

	public LocalToRemoteFilter() {
	}
	
	@Reference
	public void setLocalFilter( LocalFilter filter ) {
		this.filter = filter;
	}

	public Properties getDefaultProperties() {
		return filter.getDefaultProperties();
	}

	public String getName() {
		return filter.getName();
	}

	public byte[] process( Properties props , byte[] input ) throws Exception {
		return DataConvertorHelper.convert(filter.process(props, DataConvertorHelper.convert(input)));
	}
}
