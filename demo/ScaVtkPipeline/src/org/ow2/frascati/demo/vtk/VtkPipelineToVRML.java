/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;
import javax.vecmath.Vector3f;

import org.ow2.frascati.demo.vtk.api.LocalFilter;
import org.ow2.frascati.demo.vtk.api.Pipeline;

import vtk.vtkActor;
import vtk.vtkDataSet;
import vtk.vtkDataSetMapper;
import vtk.vtkLookupTable;
import vtk.vtkPanel;
import vtk.vtkVRMLExporter;


/**
 * Implementation of a processing pipeline based on VTK data processing.
 */

//@Scope("COMPOSITE")
public class VtkPipelineToVRML implements Pipeline {
	/** The root node. */
	private String rootNode;

	/** The list of listeners. */
	private CopyOnWriteArrayList<TreeModelListener> listeners;

	/** The map of children. */
	private HashMap<String, LinkedList<String>> tree;

	/** The map of parent. */
	private HashMap<String, String> parents;

	/** The map between id of object */
	private HashMap<String, Properties> objectMap;

	/** Map of Filters */
	private HashMap<String, LocalFilter> filters;

	public VtkPipelineToVRML() {
		this.rootNode = ROOT_ID;
		listeners = new CopyOnWriteArrayList<TreeModelListener>();
		tree = new HashMap<String, LinkedList<String>>();
		parents = new HashMap<String, String>();
		filters = new HashMap<String, LocalFilter>();
		//
		tree.put(rootNode, new LinkedList<String>());
		objectMap = new HashMap<String, Properties>();
	}

	// Pipeline management

	public void addNode( String parentId , String nodeId , Properties nodeContent ) {
		parents.put(nodeId, parentId);
		tree.put(nodeId, new LinkedList<String>());
		tree.get(parentId).add(nodeId);
		objectMap.put(nodeId, nodeContent);
		//
		fireNodesInserted(parentId, new int[] { tree.get(parentId).size() - 1 });
	}

	public void removeNode( String nodeId ) {
		if (!isLeaf(nodeId)) {
			for (String child : tree.get(nodeId)) {
				removeNode(child);
			}
		}
		String parent = parents.get(nodeId);
		int index = tree.get(parent).indexOf(nodeId);
		parents.remove(nodeId);
		objectMap.remove(nodeId);
		if (parent != null) {
			tree.get(parent).remove(index);
		}
		fireNodesRemoved(parent, new int[] { index });
	}

	public void updateNode( String nodeId , Properties nodeContent ) {
		objectMap.put(nodeId, nodeContent);
	}

	public Properties getNode( String nodeId ) {
		return objectMap.get(nodeId);
	}

	/**
	 * Returns the path of the given node.
	 * 
	 * @param node
	 *            The given node
	 * @return the path of the node
	 */
	public final TreePath getPath( final String node ) {
		LinkedList<String> path = new LinkedList<String>();
		String currentNode = node;
		while (currentNode != null) {
			path.addFirst(currentNode);
			currentNode = parents.get(currentNode);
		}
		return new TreePath(path.toArray());
	}

	// Filter management
	public void registerFilter( String name , LocalFilter filter ) {
		filters.put(name, filter);
	}

	public Collection<String> getAvailableFilters() {
		return filters.keySet();
	}

	public LocalFilter getFilter( String name ) {
		return filters.get(name);
	}

	public void setDataLoader( LocalFilter filter ) {
		registerFilter(filter.getName(), filter);
	}

	public void setClippingPlaneFilter( LocalFilter filter ) {
		registerFilter(filter.getName(), filter);
	}

	public void setCuttingPlaneFilter( LocalFilter filter ) {
		registerFilter(filter.getName(), filter);
	}

	public void setIsoContourFilter( LocalFilter filter ) {
		registerFilter(filter.getName(), filter);
	}

	// Tree model Management
	public final void addTreeModelListener( final TreeModelListener l ) {
		listeners.add(l);
	}

	public final Object getChild( final Object parent , final int index ) {
		return tree.get(parent).get(index);
	}

	public final int getChildCount( final Object parent ) {
		return tree.get(parent).size();
	}

	public final int getIndexOfChild( final Object parent , final Object child ) {
		return tree.get(parent).indexOf(child);
	}

	public final Object getRoot() {
		return rootNode;
	}

	public final boolean isLeaf( final Object node ) {
		return tree.get(node).size() == 0;
	}

	public final void removeTreeModelListener( final TreeModelListener l ) {
		listeners.remove(l);
	}

	public final void valueForPathChanged( final TreePath path , final Object newValue ) {
		throw new RuntimeException("Not supported yet");
	}

	/**
	 * Tells the listeners that the given node has changed.
	 * 
	 * @param node
	 *            the node that changed
	 */
	public final void fireNodesChanged( final String node ) {
		TreeModelEvent e = new TreeModelEvent(node, getPath(node));
		for (TreeModelListener l : listeners) {
			l.treeNodesChanged(e);
		}
	}

	/**
	 * Reloads the structure.
	 */
	public final void reload() {
		TreeModelEvent e = new TreeModelEvent(rootNode, new TreePath(rootNode));
		for (TreeModelListener l : listeners) {
			l.treeStructureChanged(e);
		}
	}

	/**
	 * Tells the listeners that a node has been added to the given parent node.
	 * 
	 * @param parent
	 *            the parent node
	 * @param childrenIndex
	 *            the index of the inserted node
	 */
	public final void fireNodesInserted( final String parent , final int[] childrenIndex ) {
		TreeModelEvent e = new TreeModelEvent(parent, getPath(parent), childrenIndex, new Object[] { parent });
		for (TreeModelListener l : listeners) {
			l.treeNodesInserted(e);
		}
	}

	/**
	 * Tells the listeners that a node has been removes.
	 * 
	 * @param parent
	 *            the parent node
	 * @param childrenIndex
	 *            the index of the removed node
	 */
	public final void fireNodesRemoved( final String parent , final int[] childrenIndex ) {
		TreeModelEvent e = new TreeModelEvent(parent, getPath(parent), childrenIndex, new Object[] { parent });
		for (TreeModelListener l : listeners) {
			l.treeNodesRemoved(e);
		}
	}

	// Process data

	public byte[] process( String nodeId ) throws Exception {
		if (nodeId.equals(ROOT_ID))
			return null;
		File tmpFile = File.createTempFile("scene3D", ".wrl");
		tmpFile.deleteOnExit();
		vtkPanel panel3D = new vtkPanel();
		vtkLookupTable lut = new vtkLookupTable();
		lut.SetHueRange(.6666, 0);
		vtkVRMLExporter export = new vtkVRMLExporter();
		vtkActor actor = new vtkActor();
		vtkDataSetMapper mapper = new vtkDataSetMapper();
		actor.SetMapper(mapper);
		mapper.SetLookupTable(lut);
		panel3D.GetRenderer().AddActor(actor);
		export.SetRenderWindow(panel3D.GetRenderWindow());
		export.SetFileName(tmpFile.getAbsolutePath());
		//
		TreePath path = getPath(nodeId);
		vtkDataSet result = null;
		double[] range = null;
		for (int i = 1; i < path.getPathCount(); i++) {
			Properties props = objectMap.get(path.getPathComponent(i));
			result = filters.get(props.get(FILTER_KEY)).process(props, result);
			result.Update();
			props.setProperty(GEOMETRY_BOUND, toString(result.GetBounds()));
			props.setProperty(SCALAR_RANGE, toString(result.GetScalarRange()));
			if (range == null)
				range = result.GetScalarRange();
		}
		// Convert in VRML
		mapper.SetInput(result);

		mapper.SetScalarRange(range);
		lut.SetRange(range);
		export.Write();
		// Convert in byte[]
		ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
		FileInputStream fis = new FileInputStream(tmpFile);
		byte[] buffer = new byte[1024 * 1024 * 2];
		int length = 0;
		while ((length = fis.read(buffer)) != -1)
			byteArray.write(buffer, 0, length);
		fis.close();
		return byteArray.toByteArray();
	}

	private String toString( double[] array ) {
		StringBuilder r = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			r.append(Double.toString(array[i]));
			if (i < array.length - 1)
				r.append(";");
		}
		return r.toString();
	}

	public static Vector3f convertToCenter( String[] bounds ) {
		float[] b = new float[bounds.length];
		for (int i = 0; i < b.length; i++)
			b[i] = Float.parseFloat(bounds[i]);
		return new Vector3f((b[1] - b[0]) / 2 + b[0], (b[3] - b[2]) / 2 + b[2], (b[5] - b[4]) / 2 + b[4]);
	}

}
