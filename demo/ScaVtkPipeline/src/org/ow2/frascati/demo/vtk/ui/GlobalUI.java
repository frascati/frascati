/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTree;

import org.ow2.frascati.demo.vtk.api.Pipeline;


/**
 * Global GUI of 3D viewer
 */
public class GlobalUI extends JPanel implements MouseListener {
	private static final long serialVersionUID = 1L;
	private JSplitPane splitTreeAndProps;
	private PropertyEditor propsEditor;
	private JTree tree;
	private TreePopupMenu popupMenu;

	public GlobalUI( Pipeline pipeline, View3D view3D ) {
		super(new BorderLayout());
		// Set UI

		//
		propsEditor = new PropertyEditor(pipeline);
		//
		tree = new JTree(pipeline);
		tree.addMouseListener(this);
		tree.addTreeSelectionListener(propsEditor);
		//
		popupMenu = new TreePopupMenu(tree, pipeline, view3D);
		//
		splitTreeAndProps = new JSplitPane(JSplitPane.VERTICAL_SPLIT, tree, propsEditor);
		splitTreeAndProps.setDividerSize(2);
		splitTreeAndProps.setDividerLocation(.5);
		splitTreeAndProps.setMinimumSize(new Dimension(200, 200));
		Dimension d = splitTreeAndProps.getPreferredSize();
		d.width = 200;
		splitTreeAndProps.setPreferredSize(d);
		//
		view3D.getUI().setMinimumSize(new Dimension(10,10));
		JSplitPane split3D = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,splitTreeAndProps, view3D.getUI() );
		add(split3D, BorderLayout.CENTER);
	}
	
	public void updateFilterList(){
		popupMenu.updateFilterList();
	}

	public void mouseClicked( MouseEvent e ) {
		if (e.isPopupTrigger() && tree.getPathForLocation(e.getX(), e.getY()) != null) {
			popupMenu.show(this, e.getX(), e.getY(), tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent().toString());
		}
	}

	public void mouseEntered( MouseEvent e ) {
	}

	public void mouseExited( MouseEvent e ) {
	}

	public void mousePressed( MouseEvent e ) {
		if (e.isPopupTrigger() && tree.getPathForLocation(e.getX(), e.getY()) != null) {
			popupMenu.show(this, e.getX(), e.getY(), tree.getPathForLocation(e.getX(), e.getY()).getLastPathComponent().toString());
		}
	}

	public void mouseReleased( MouseEvent e ) {
	}

}
