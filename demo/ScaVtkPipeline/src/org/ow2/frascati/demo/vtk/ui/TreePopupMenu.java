/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;

import org.ow2.frascati.demo.vtk.api.Pipeline;


/**
 * Popup menu used to allow user interaction between the pipeline JTree and the
 * 3D view.
 */
public class TreePopupMenu extends JPopupMenu implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Pipeline pipeline;
	private String nodeId;
	private JTree tree;
	private AtomicInteger id;
	private View3D view;
	//
	private JMenu addChild;

	public TreePopupMenu( JTree tree, Pipeline pipeline, View3D view ) {
		id = new AtomicInteger(1);
		this.tree = tree;
		this.pipeline = pipeline;
		this.view = view;
		//
		addChild = new JMenu("Add node");
		for (String filter : pipeline.getAvailableFilters()) {
			JMenuItem f = new JMenuItem(filter);
			f.setActionCommand(filter);
			addChild.add(f);
			f.addActionListener(this);
		}
		JMenuItem show = new JMenuItem("View node");
		show.setActionCommand("SHOW");
		show.addActionListener(this);
		//
		JMenuItem hide = new JMenuItem("Hide node");
		hide.setActionCommand("HIDE");
		hide.addActionListener(this);
		//
		JMenuItem centerRotation = new JMenuItem("Center rotation on node");
		centerRotation.setActionCommand("CENTER_ROTATION");
		centerRotation.addActionListener(this);
		//
		JMenuItem delete = new JMenuItem("Delete node");
		delete.setActionCommand("DELETE");
		delete.addActionListener(this);
		//
		add(addChild);
		addSeparator();
		add(show);
		add(hide);
		addSeparator();
		add(delete);
	}

	public void updateFilterList() {
		addChild.removeAll();
		for (String filter : pipeline.getAvailableFilters()) {
			JMenuItem f = new JMenuItem(filter);
			f.setActionCommand(filter);
			addChild.add(f);
			f.addActionListener(this);
		}
	}

	public void actionPerformed( ActionEvent e ) {
		if (e.getActionCommand().equals("SHOW")) {
			try {
				byte[] data = pipeline.process(nodeId);
				if (data == null)
					return;
				view.load(nodeId, data);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else if (e.getActionCommand().equals("DELETE")) {
			pipeline.removeNode(nodeId);
		} else if (e.getActionCommand().equals("CENTER_ROTATION")) {
			/*
			 * float[] bounds = view.getPipeline().getObjectBoudingBox(nodeId);
			 * System.out.println(nodeId + " : " +Arrays.toString(bounds));
			 * Viewpoint viewpoint = new Viewpoint();
			 * viewpoint.setRotationCenter(new Vector3f((bounds[1]-bounds[0])/2 +
			 * bounds[0],( bounds[3]-bounds[2])/2 +
			 * bounds[2],(bounds[5]-bounds[4])/2 + bounds[4]));
			 * view.getPipeline().setViewpoint(viewpoint);
			 */
		} else if (e.getActionCommand().equals("HIDE")) {
			view.remove(nodeId);
		} else {
			Properties props = pipeline.getFilter(e.getActionCommand()).getDefaultProperties();
			props.setProperty(Pipeline.FILTER_KEY, e.getActionCommand());
			String newNodeId = "(" + id.getAndIncrement() + ") " + e.getActionCommand();
			pipeline.addNode(nodeId, newNodeId, props);
			tree.expandPath(pipeline.getPath(nodeId));
		}
	}

	//
	public void show( Component invoker , int x , int y , String selectedNode ) {
		if (selectedNode != null) {
			nodeId = selectedNode;
			show(invoker, x, y);
		}
	}
}
