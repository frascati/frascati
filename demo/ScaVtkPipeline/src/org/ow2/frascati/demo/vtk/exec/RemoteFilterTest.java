/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.exec;

import java.io.File;
import java.net.URL;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

import vtk.vtkPanel;

public class RemoteFilterTest {

  public static void main(String[] args) throws Exception {
    new vtkPanel();
    URL[] urls = new URL[args.length - 1];
    for (int i = 1; i < args.length; i++)
      urls[i - 1] = new File(args[i]).toURI().toURL();

    Factory factory = new Factory();
    File compositefile = new File(args[0]);
    Component scaDomain = factory.getComposite(
        compositefile.getCanonicalPath(), urls);
    System.in.read();
    TinfiDomain.close(scaDomain);

  }

}
