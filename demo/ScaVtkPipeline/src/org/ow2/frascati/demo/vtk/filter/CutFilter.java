/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.filter;

import java.util.Properties;

import org.ow2.frascati.demo.vtk.api.LocalFilter;

import vtk.vtkCutter;
import vtk.vtkDataSet;
import vtk.vtkPlane;


/**
 * Filter used to extract a plane inside the input geometry.
 */
public class CutFilter implements LocalFilter {
	private Properties defaultProps;
	public final static String NORMAL_X = "cutting.normal.x";
	public final static String NORMAL_Y = "cutting.normal.y";
	public final static String NORMAL_Z = "cutting.normal.z";
	public final static String CENTER_X = "cutting.center.x";
	public final static String CENTER_Y = "cutting.center.y";
	public final static String CENTER_Z = "cutting.center.z";

	public CutFilter() {
		defaultProps = new Properties();
		defaultProps.setProperty(NORMAL_X, "0");
		defaultProps.setProperty(NORMAL_Y, "0");
		defaultProps.setProperty(NORMAL_Z, "1");
		defaultProps.setProperty(CENTER_X, "0");
		defaultProps.setProperty(CENTER_Y, "0");
		defaultProps.setProperty(CENTER_Z, "0");
	}

	public String getName() {
		return "Cutting plane";
	}

	public Properties getDefaultProperties() {
		Properties props = new Properties();
		props.putAll(defaultProps);
		return props;
	}

	public vtkDataSet process( Properties props , vtkDataSet input ) throws Exception {
		vtkPlane plane = new vtkPlane();
		plane.SetNormal(getDouble(NORMAL_X, props), getDouble(NORMAL_Y, props), getDouble(NORMAL_Z, props));
		plane.SetOrigin(getDouble(CENTER_X, props), getDouble(CENTER_Y, props), getDouble(CENTER_Z, props));
		vtkCutter cut = new vtkCutter();
		cut.SetCutFunction(plane);
		cut.SetInput(input);
		cut.GetOutput().Update();
		return cut.GetOutput();
	}

	private double getDouble( String key , Properties prop ) {
		return Double.parseDouble(prop.getProperty(key));
	}

}
