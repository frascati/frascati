/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.filter;

import java.util.Properties;

import org.ow2.frascati.demo.vtk.api.LocalFilter;

import vtk.vtkContourFilter;
import vtk.vtkDataSet;

/**
 * Data processing filter use to generate geometry owning a set of same value.
 */
public class IsoContourFilter implements LocalFilter {
	private Properties defaultProps;
	public final static String VALUES = "iso.contour.values";
	public final static String VALUE_SEPARATOR = ";";

	public IsoContourFilter() {
		defaultProps = new Properties();
		defaultProps.setProperty(VALUES, "16;17;18;19;20;21;22");
	}

	public String getName() {
		return "Iso Contour";
	}

	public Properties getDefaultProperties() {
		Properties props = new Properties();
		props.putAll(defaultProps);
		return props;
	}

	public vtkDataSet process( Properties props , vtkDataSet input ) throws Exception {
		vtkContourFilter iso = new vtkContourFilter();
		double[] values = getDoubleArray(VALUES, props);
		iso.SetNumberOfContours(values.length);
		for (int i = 0; i < values.length; i++)
			iso.SetValue(i, values[i]);
		iso.SetInput(input);
		iso.GetOutput().Update();
		return iso.GetOutput();
	}

	private double[] getDoubleArray( String key , Properties prop ) {
		String[] values = prop.getProperty(key).split(VALUE_SEPARATOR);
		double[] result = new double[values.length];
		for (int i = 0; i < result.length; i++)
			result[i] = Double.parseDouble(values[i]);
		return result;
	}

}
