/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk;

import java.awt.BorderLayout;
import java.util.LinkedList;

import javax.swing.JFrame;

import org.ow2.frascati.demo.vtk.api.LocalFilter;
import org.ow2.frascati.demo.vtk.api.Pipeline;
import org.ow2.frascati.demo.vtk.filter.ClipFilter;
import org.ow2.frascati.demo.vtk.filter.CutFilter;
import org.ow2.frascati.demo.vtk.filter.DataLoaderFilter;
import org.ow2.frascati.demo.vtk.filter.IsoContourFilter;
import org.ow2.frascati.demo.vtk.ui.GlobalUI;
import org.ow2.frascati.demo.vtk.ui.VtkView3D;

import vtk.vtkPanel;


/**
 * Sample executable code based on VTK for the 3D rendering
 */
public class VTKStandaloneClient {
	public static void main( String[] args ) {
		// Init pipeline with frascati
		Pipeline pipeline = new VtkPipelineToVTK();
		//
		LinkedList<LocalFilter> filters = new LinkedList<LocalFilter>();
		filters.add(new DataLoaderFilter());
		filters.add(new CutFilter());
		filters.add(new ClipFilter());
		filters.add(new IsoContourFilter());
		//
		for (LocalFilter f : filters)
			pipeline.registerFilter(f.getName(), f);

		// Init GUI
		GlobalUI ui = new GlobalUI(pipeline, new VtkView3D(new vtkPanel()));
		JFrame frame = new JFrame("Distributed 3D viewer");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(ui, BorderLayout.CENTER);
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
