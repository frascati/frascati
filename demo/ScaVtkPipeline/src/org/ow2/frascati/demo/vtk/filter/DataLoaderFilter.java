/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.filter;

import java.util.Properties;

import org.ow2.frascati.demo.vtk.api.LocalFilter;

import vtk.vtkDataSet;
import vtk.vtkDataSetReader;

/**
 * Filter used to load VTK legacy files.
 */
public class DataLoaderFilter implements LocalFilter {
	private Properties defaultProps;
	public final static String FILE_PATH = "data.loader.filepath";

	public DataLoaderFilter() {
		defaultProps = new Properties();
		defaultProps.setProperty(FILE_PATH, "data/radiateur32.vtk");
	}

	public String getName() {
		return "Data Loader";
	}

	public Properties getDefaultProperties() {
		Properties props = new Properties();
		props.putAll(defaultProps);
		return props;
	}

	public vtkDataSet process( Properties props , vtkDataSet input ) throws Exception {
		vtkDataSetReader reader = new vtkDataSetReader();
		reader.SetFileName(props.getProperty(FILE_PATH));
		reader.Update();
		return reader.GetOutput();
	}
}
