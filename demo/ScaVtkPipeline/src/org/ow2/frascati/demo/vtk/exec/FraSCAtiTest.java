/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 *            Damien Fournier
 *
 */

package org.ow2.frascati.demo.vtk.exec;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.demo.vtk.api.LocalFilter;
import org.ow2.frascati.demo.vtk.api.Pipeline;
import org.ow2.frascati.demo.vtk.api.RemoteFilter;
import org.ow2.frascati.demo.vtk.filter.util.LocalToRemoteFilter;
import org.ow2.frascati.demo.vtk.filter.util.RemoteToLocalFilter;
import org.ow2.frascati.demo.vtk.ui.GlobalUI;
import org.ow2.frascati.demo.vtk.ui.VtkView3D;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.factory.core.instance.binding.ScaBindingUtil;
import org.ow2.frascati.factory.core.instance.runtime.RuntimeFactory;
import org.ow2.frascati.tinfi.TinfiDomain;

import vtk.vtkPanel;


public class FraSCAtiTest {
  private Component scaDomain;
  private Factory factory;
  private GlobalUI globalUI;
  private JMenuBar menu;
  Pipeline pipeline;

  public FraSCAtiTest(String compositeFilePath, URL... lib) throws Exception {

    System.setSecurityManager(new SecurityManager());

    //
    ClassLoader cl = Class.forName(FraSCAtiTest.class.getCanonicalName())
        .getClassLoader();

    factory = new Factory();

    File compositefile = new File(compositeFilePath);
    scaDomain = factory.getComposite(compositefile.getCanonicalPath(), lib);

    pipeline = TinfiDomain.getService(scaDomain, Pipeline.class,
        "PipelineService");

    // Init GUI
    globalUI = new GlobalUI(pipeline, new VtkView3D(new vtkPanel()));
    // Menu bar
    menu = new JMenuBar();
    JMenu addFilter = new JMenu("Add filter");
    menu.add(addFilter);
    JMenuItem registerWS = new JMenuItem("register WS");
    registerWS.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String url = JOptionPane.showInputDialog("The url of the WS");
        // TODO WS
        Component container = null;
        Component newFilter = null;
        try {

          Component supers[] = Fractal.getSuperController(scaDomain)
              .getFcSuperComponents();
          container = supers[0];

          Fractal.getLifeCycleController(container).stopFc();

          String filterServiceName = "filter";
          String filterServItfName = LocalFilter.class.getCanonicalName();

          String filterReferenceName = "remoteFilter";
          String filterRefItfName = RemoteFilter.class.getCanonicalName();

          RuntimeFactory runtime = (RuntimeFactory) factory.getFactory()
              .getFcInterface("runtime");

          InterfaceType servItf = runtime.createFcItfType(filterServiceName,
              filterServItfName, TypeFactory.SERVER, false, false);

          InterfaceType clientItf = runtime.createFcItfType(
              filterReferenceName, filterRefItfName, TypeFactory.CLIENT, false,
              false);

          InterfaceType[] itfList = { clientItf, servItf };

          ComponentType componentType = runtime.createFcType(itfList);

          newFilter = runtime.newFcInstance(componentType, new Object[] {
              runtime.getClassLoader(), "scaPrimitive" },
              RemoteToLocalFilter.class.getCanonicalName());

          Fractal.getNameController(newFilter).setFcName(
              "FilterComponent" + System.currentTimeMillis());

          Fractal.getContentController(scaDomain).addFcSubComponent(newFilter);

          HashMap<String, String> m = new HashMap<String, String>();

          m.put("export.mode", "ws");
          m.put("address", url);
    

          BindingFactory bf = (BindingFactory) factory.getFactory()
              .getFcInterface("binding");
          bf.bind(newFilter, filterReferenceName, m);

        } catch (Exception e1) {
          System.err.println("Invalid given parameter");
          try {
            Fractal.getContentController(scaDomain).removeFcSubComponent(
                newFilter);
          } catch (Exception e2) {
            e2.printStackTrace();
          }
        } finally {
          try {
            Fractal.getLifeCycleController(container).startFc();
            LocalFilter f = (LocalFilter) newFilter.getFcInterface("filter");
            pipeline.registerFilter("(r) " + f.getName(), f);
          } catch (Exception e1) {
          }
        }

        // update popup menu
        globalUI.updateFilterList();
        
      }
    });
    addFilter.add(registerWS);
    JMenuItem registerRMI = new JMenuItem("register RMI");
    registerRMI.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String host = JOptionPane.showInputDialog("The host name");
        String port = JOptionPane.showInputDialog("The port");
        String serviceName = JOptionPane.showInputDialog("The service name");
        // TODO RMI
        Component container = null;
        Component newFilter = null;
        try {

          Component supers[] = Fractal.getSuperController(scaDomain)
              .getFcSuperComponents();
          container = supers[0];

          Fractal.getLifeCycleController(container).stopFc();

          String filterServiceName = "filter";
          String filterServItfName = LocalFilter.class.getCanonicalName();

          String filterReferenceName = "remoteFilter";
          String filterRefItfName = RemoteFilter.class.getCanonicalName();

          RuntimeFactory runtime = (RuntimeFactory) factory.getFactory()
              .getFcInterface("runtime");

          InterfaceType servItf = runtime.createFcItfType(filterServiceName,
              filterServItfName, TypeFactory.SERVER, false, false);

          InterfaceType clientItf = runtime.createFcItfType(
              filterReferenceName, filterRefItfName, TypeFactory.CLIENT, false,
              false);

          InterfaceType[] itfList = { clientItf, servItf };

          ComponentType componentType = runtime.createFcType(itfList);

          newFilter = runtime.newFcInstance(componentType, new Object[] {
              runtime.getClassLoader(), "scaPrimitive" },
              RemoteToLocalFilter.class.getCanonicalName());

          Fractal.getNameController(newFilter).setFcName(
              "FilterComponent" + System.currentTimeMillis());

          Fractal.getContentController(scaDomain).addFcSubComponent(newFilter);

          HashMap<String, String> m = new HashMap<String, String>();

          m.put("plugin.id", "rmi");
          m.put("serviceName", serviceName);
          m.put("port", port);
          m.put("hostAddress", host);

          BindingFactory bf = (BindingFactory) factory.getFactory()
              .getFcInterface("binding");
          bf.bind(newFilter, filterReferenceName, m);

        } catch (Exception e1) {
        	e1.printStackTrace();
          System.err.println("Invalid given parameter");
          try {
            Fractal.getContentController(scaDomain).removeFcSubComponent(
                newFilter);
          } catch (Exception e2) {
            e2.printStackTrace();
          }
        } finally {
          try {
            Fractal.getLifeCycleController(container).startFc();
            LocalFilter f = (LocalFilter) newFilter.getFcInterface("filter");
            pipeline.registerFilter("(r) " + f.getName(), f);
          } catch (Exception e1) {
        	  e1.printStackTrace();
          }
        }
        // update popup menu
        globalUI.updateFilterList();

      }
    });
    addFilter.add(registerRMI);
  }

  public JPanel getUI() {
    return globalUI;
  }

  public JMenuBar getMenu() {
    return menu;
  }

  /**
   * @param args
   */
  public static void main(String[] args) throws Exception {
    URL[] urls = new URL[args.length - 1];
    for (int i = 1; i < args.length; i++)
      urls[i - 1] = new File(args[i]).toURI().toURL();
    FraSCAtiTest test = new FraSCAtiTest(args[0], urls);

    JFrame frame = new JFrame("Distributed 3D viewer");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().setLayout(new BorderLayout());
    frame.getContentPane().add(test.getUI(), BorderLayout.CENTER);
    frame.setJMenuBar(test.getMenu());
    frame.setSize(800, 600);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
  }

}
