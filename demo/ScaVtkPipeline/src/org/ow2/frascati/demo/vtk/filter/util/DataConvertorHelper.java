/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.filter.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import vtk.vtkDataSet;
import vtk.vtkDataSetReader;
import vtk.vtkDataSetWriter;

/**
 * Helper class to convert VTK structure in a serialisable byte[] format.
 */
public class DataConvertorHelper {
	public static vtkDataSet convert( byte[] data ) throws IOException {
		File tmpFile = File.createTempFile("processing-data", ".vtk");

		// Write input data to process
		FileOutputStream fos = new FileOutputStream(tmpFile);
		fos.write(data);
		fos.close();

		// Process data
		vtkDataSetReader reader = new vtkDataSetReader();
		reader.SetFileName(tmpFile.getAbsolutePath());
		reader.Update();

		vtkDataSet dataset = reader.GetOutput();
		dataset.Update();

		// Delete tmp file
		if (!tmpFile.delete())
			tmpFile.deleteOnExit();

		return dataset;
	}

	public static byte[] convert( vtkDataSet data ) throws IOException {
		File tmpFile = File.createTempFile("processing-data", ".vtk");

		vtkDataSetWriter writer = new vtkDataSetWriter();
		writer.SetFileName(tmpFile.getAbsolutePath());
		writer.SetFileTypeToBinary();
		writer.SetInput(data);
		writer.Write();

		// Convert to bytearray
		return convert(tmpFile, true);
	}

	public static byte[] convert( File file , boolean delete ) throws IOException {
		// Convert to bytearray
		FileInputStream in = new FileInputStream(file);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int length = 0;
		byte[] buffer = new byte[1024 * 1024];
		while ((length = in.read(buffer)) != -1)
			out.write(buffer, 0, length);
		in.close();

		// Delete tmp file
		if (delete)
			if (!file.delete())
				file.deleteOnExit();

		return out.toByteArray();
	}

	public static double getDouble( Properties properties , String key ) {
		return Double.parseDouble(properties.getProperty(key));
	}

	public static double[] getDoubleArray( Properties properties , String key ) {
		String[] list = properties.getProperty(key).split(":");
		double[] result = new double[list.length];
		for (int i = 0; i < result.length; i++) {
			result[i] = Double.parseDouble(list[i]);
		}
		return result;
	}
}
