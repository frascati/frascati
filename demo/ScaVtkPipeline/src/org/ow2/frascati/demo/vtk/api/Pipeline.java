/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.api;

import java.util.Collection;
import java.util.Properties;

import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
/**
 * Interface for a processing pipeline.
 */
public interface Pipeline extends TreeModel {
	public final static String FILTER_KEY = "filter.name";
	public final static String GEOMETRY_BOUND = "geometry.bounds";
	public final static String SCALAR_RANGE = "scalar.range";
	public final static String ROOT_ID = "root";

	// Set pipeline
	public void addNode( String parentId , String nodeId , Properties nodeContent );
	public void removeNode( String nodeId );
	public void updateNode( String nodeId , Properties nodeContent );
	public TreePath getPath( final String nodeId );
	public Properties getNode( String nodeId );

	// Dynamic filter
	public Collection<String> getAvailableFilters();
	public void registerFilter( String name , LocalFilter filter );
	public LocalFilter getFilter( String name );

	// Fix filters
	public void setDataLoader( LocalFilter filter );
	public void setCuttingPlaneFilter( LocalFilter filter );
	public void setClippingPlaneFilter( LocalFilter filter );
	public void setIsoContourFilter( LocalFilter filter );

	// Process data
	public byte[] process( String nodeId ) throws Exception;

}
