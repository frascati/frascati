/***
 * OW2 FraSCAti VTK Demo
 * Copyright (C) 2008-2009 INRIA, USTL, Artenum
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author(s): Sebastien Jourdain
 * 
 */

package org.ow2.frascati.demo.vtk.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import org.ow2.frascati.demo.vtk.api.Pipeline;

/**
 * GUI editor of java.util.Properties object.
 */
public class PropertyEditor extends JPanel implements ActionListener, TreeSelectionListener {
	private static final long serialVersionUID = 1L;
	private JTextArea txt;
	private JButton update;
	private String selectedNode;
	private Pipeline pipeline;

	public PropertyEditor( Pipeline pipeline ) {
		super(new BorderLayout());
		this.pipeline = pipeline;
		txt = new JTextArea();
		update = new JButton("Save");
		update.addActionListener(this);
		add(new JScrollPane(txt), BorderLayout.CENTER);
		add(update, BorderLayout.SOUTH);
	}

	public void actionPerformed( ActionEvent e ) {
		Properties newProps = new Properties();
		try {
			BufferedReader reader = new BufferedReader(new StringReader(txt.getText()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.trim().startsWith("#")) {
					String[] keyVal = line.split("=");
					newProps.setProperty(keyVal[0], keyVal[1]);
				}
			}
			reader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		pipeline.updateNode(selectedNode, newProps);
	}

	public void valueChanged( TreeSelectionEvent e ) {
		selectedNode = e.getPath().getLastPathComponent().toString();
		if (selectedNode == null)
			return;
		Properties props = pipeline.getNode(selectedNode);
		if (props == null) {
			txt.setText("");
			return;
		}
		ByteArrayOutputStream writer = new ByteArrayOutputStream();
		try {
			props.store(writer, "");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		txt.setText(writer.toString());
	}
}
