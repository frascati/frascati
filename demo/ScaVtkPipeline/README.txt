===============================================================================
OW2 FraSCAti Sample code based on 3D processing
Copyright (C) 2007-2009 Artenum, INRIA, USTL

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: 

Authors: Sebastien Jourdain   <jourdain@artenum.com>
         Damien Fournier      <Damien.Fournier@inria.fr>

===============================================================================

SCOrWare FraSCAti 3D data processing sample code Standalone Distribution
-----------------------------------------------------

The sample code provided here aims to demonstrate local/remote/distributed 
execution on complexe 3D data structure based on VTK.

Table of content
----------------
  1. Introduction
  2. Requirements
  3. Installation
  4. Building component libraries
  5. Running SCA composites
  6. Links


1. Introduction
---------------

The sample code allow user to load VTK data and process this data in order 
to generate a set of 3D objects and analyse those processed data inside a 3D view.

2. Requirements
---------------

 - Java SE 1.5 (or later) is required.
 - Frascati 0.4 or above
 - VTK 5.x Library with the Java Wrapping 
   (We try to provide several compiled version for each platform but 
   we can not guaranty that it will work on all the system.)

3. Installation
---------------

Unzip sample code distribution archive and you get the following directory
structure:

  /src      -  Source code directory
  /lib      -  Sample code libraries
  
4. Building component libraries
-------------------------------

This distribution provides a build script based on ant in order to ease 
installation, compilation and execution.

To get more help, just type "ant"
In order to build the system, you will need first to install native library 
by typing "ant install" and then type "ant make".


5. Running Code sample
-------------------------

"ant run"

6. Links
--------
 - OW2 FraSCAti : http://frascati.ow2.org
 - Artenum : http://www.artenum.com
 - SCOrWare Consortium : http://www.scorware.org
 - OSOA Consortium : http://www.osoa.org
 - Agence Nationale de la Recherche (ANR) : http://www.agence-nationale-recherche.fr/
