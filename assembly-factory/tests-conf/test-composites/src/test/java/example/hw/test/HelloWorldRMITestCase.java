package example.hw.test;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

public class HelloWorldRMITestCase {

  private static Component rmiClient;
  private static Component rmiServer;
  private static java.lang.Runnable service;

  @BeforeClass
  public static void setUp() throws Exception {
    Factory factory = new Factory();

    rmiServer = factory.getComposite("helloworld-rmi-server");

    rmiClient = factory.getComposite("helloworld-rmi-client");

    service = TinfiDomain.getService(rmiClient, Runnable.class, "r");

  }

  // Test if required service has been created
  @Test
  public void testInit() {
    assertNotNull(rmiClient);
    assertNotNull(rmiServer);
    assertNotNull(service);
  }

  // Test service call
  @Test
  public void testCall() {
    service.run();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    try {

      TinfiDomain.close(rmiClient);
      TinfiDomain.close(rmiServer);

    } catch (java.lang.UnsupportedOperationException e) {
      System.err.println("Excepted exception raised : " + e.getClass() + " : " +e.getLocalizedMessage());
    }

  }

}
