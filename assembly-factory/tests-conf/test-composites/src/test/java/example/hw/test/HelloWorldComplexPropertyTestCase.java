package example.hw.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

import example.hw.Service;

public class HelloWorldComplexPropertyTestCase {

  private static Component scaDomain;
  private static java.lang.Runnable service;
  private static example.hw.Service print;

  @BeforeClass
  public static void setUp() throws Exception {
    Factory factory = new Factory();

    scaDomain = factory.getComposite("helloworld-complex-property");

    service = TinfiDomain.getService(scaDomain, Runnable.class, "r");

    print = TinfiDomain.getService(scaDomain, Service.class, "print");
  }

  // Test if required service has been created
  @Test
  public void testInit() {
    assertNotNull(scaDomain);
    assertNotNull(service);
  }

  // Test service call
  @Test
  public void testCall() {
    String pre = "This is the";
    String post = "test";
    String msg = "print test message";
    assertEquals("Assert message returned by print service", pre+msg+post, print.getMsg(msg));
  }

  @AfterClass
  public static void tearDown() throws Exception {
    TinfiDomain.close(scaDomain);
  }

}
