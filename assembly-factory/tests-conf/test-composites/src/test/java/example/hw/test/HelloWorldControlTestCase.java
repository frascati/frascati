package example.hw.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.objectweb.fractal.util.ContentControllerHelper;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.control.property.SCAPropertyController;

public class HelloWorldControlTestCase {

  private static Component scaDomain;
  private static java.lang.Runnable service;

  @BeforeClass
  public static void setUp() throws Exception {
    Factory factory = new Factory();

    scaDomain = factory.getComposite("helloworld");

    service = TinfiDomain.getService(scaDomain, Runnable.class, "r");

  }

  // Test if required service has been created
  @Test
  public void testInit() {
    assertNotNull(scaDomain);
    assertNotNull(service);
  }
  
  @Test
  public void testRun(){
    service.run();
  }

  // Test component names according to helloworld.composite
  @Test
  public void testCompositeNaming() {

    // Expected component names
    ArrayList<String> list = new ArrayList<String>();
    list.add("client");
    list.add("server");
    list.add("helloworld");

    try {

      String name = Fractal.getNameController(scaDomain).getFcName();
      Assert.assertEquals("Test expected composite name", true, list
          .contains(name));

      Component[] scaComponents = Fractal.getContentController(scaDomain)
          .getFcSubComponents();

      for (Component component : scaComponents) {
        name = null;
        name = Fractal.getNameController(component).getFcName();
        assertEquals("Test expected component name", true, list.contains(name));

      }

    } catch (NoSuchInterfaceException e) {
      Assert.fail(e.getMessage());
    }

  }

  //@Test
  public void testServerGetProperties() {

    try {
      // Expected properties for server component
      ArrayList<String> list = new ArrayList<String>();
      list.add("count");
      list.add("header");

      Component component = ContentControllerHelper.getSubComponentByName(
          scaDomain, "server");

      SCAPropertyController controller = (SCAPropertyController) component
          .getFcInterface("sca-property-controller");

      for (String name : list) {
        assertNotNull("Property '"+name+"' for server component has null value",controller.getValue(name));
      }
    } catch (NoSuchInterfaceException e) {
      Assert.fail(e.getMessage());
    }

  }

  @AfterClass
  public static void tearDown() throws Exception {
    TinfiDomain.close(scaDomain);
  }

}
