package example.hw.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.core.domain.DomainException;
import org.ow2.frascati.factory.runtime.domain.SCADomain;
import org.ow2.frascati.factory.runtime.domain.api.Domain;
import org.ow2.frascati.factory.runtime.domain.api.DomainConfig;
import org.ow2.frascati.tinfi.TinfiDomain;

public class HelloWorldDomainTestCase {

  private static Domain scaDomain;
  private static DomainConfig scaDomainConfig;
  private static java.lang.Runnable service;
  private static Component helloworld;

  @BeforeClass
  public static void setUp() throws Exception {

    SCADomain domain = new SCADomain();
    scaDomain = domain.getDomain();
    scaDomainConfig = domain.getDomainConfig();

    helloworld = scaDomain.getComposite("helloworld");

    service = TinfiDomain.getService(helloworld, java.lang.Runnable.class, "r");
  }
  
  // Test if required service has been created 
  @Test
  public void testInit() {
    assertNotNull(service);
  }
  
  // Test sercice call 
  //@Test
  public void testCall() {
    service.run();
  }
   
  // Test if composite name listing is not null
  @Test
  public void testList() {
    String [] list = scaDomain.getCompositesNames();
    assertNotNull(list);
    assertEquals("Test if composite helloworld exist ", list[0], "helloworld");
  }
  
  // Test if get composite return already loaded components instead of creating new one
  @Test
  public void testGetLoadedComponent() {
    
    Component helloworld2;
    try {
      
      helloworld2 = scaDomain.getComposite("helloworld");
      Assert.assertEquals("Ensure that getComposite() return reference for loaded composite", helloworld2, helloworld);
            
    } catch (DomainException e) {
      e.printStackTrace();
    }
    
  }
  
  // Test if composite name listing is not null
  @Test
  public void testDomainComponentReference() {
    Component domainComponent = scaDomainConfig.getDomainComponent();
    assertNotNull("Ensure that the domain configuration interface allow to retrieve the 'Domain' component", domainComponent);
  }
  
  // Test remove component from domain
  // TODO Not working
  // @Test(timeout=5000)
  public void testRemove() {
    try {
      scaDomain.removeComposite("helloworld");
      String [] list = scaDomain.getCompositesNames();
      for (String string : list) {
        System.out.println(string);
        if(string.equals("helloworld")) Assert.fail("Helloworld composite not removed from Domain");
      }
    } catch (DomainException e) {
      Assert.fail("Cannot remove Helloworld composite from Domain\n"+e.getMessage());
    }
    
  }
  
  

  @AfterClass
  public static void tearDown() throws Exception {
    //Nothing to do
  }

}
