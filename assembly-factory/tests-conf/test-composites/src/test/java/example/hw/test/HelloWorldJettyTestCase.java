package example.hw.test;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;


public class HelloWorldJettyTestCase {

  private static Component scaDomain;
  private static java.lang.Runnable service;

  @BeforeClass
  public static void setUp() throws Exception {
    Factory factory = new Factory();

    scaDomain = factory.getComposite("Jetty");

    service = TinfiDomain.getService(scaDomain, Runnable.class, "r");
  }

  // Test if required service has been created
  @Test
  public void testInit() {
    assertNotNull(scaDomain);
    assertNotNull(service);
  }

  // Test service call
  @Test
  public void testCall() {
    service.run();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    TinfiDomain.close(scaDomain);
  }

}
