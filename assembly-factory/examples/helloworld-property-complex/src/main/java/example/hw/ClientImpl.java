/***
 * OW2 FraSCAti Assembly Factory Examples: HelloWorld with complex property
 * Copyright (C) 2008 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Damien.Fournier@inria.fr
 *
 * Author: Damien Fournier
 */

package example.hw;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;

public class ClientImpl implements Runnable {

  public ClientImpl() {
    System.err.println("CLIENT created");
  }

  @Init
  public void init() {
    System.err.println("CLIENT initialized");
  }

  public void run() {
    s.print("hello world");
  }

  @Reference
  public Service s;
}