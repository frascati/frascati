import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.objectweb.fractal.api.Component;

public class HelloWorldPropertyTestCase {

  private Component scaDomain;
  private java.lang.Runnable service;
  
  @Before
  public void setUp() throws Exception {
    scaDomain = new Factory().getComposite("helloworld-complex-property.composite");

    service = TinfiDomain.getService(scaDomain,
        java.lang.Runnable.class, "r");
  }
  
  // Test if required service has been created 
  @Test
  public void testInit() {
    assertNotNull(scaDomain);
    assertNotNull(service);
  }
  
  // Test sercice call 
  // TODO Use an Assert
  @Test
  public void testCall() {
    service.run();
  }
  
  @After
  public void tearDown() throws Exception {
      TinfiDomain.close(scaDomain);   
  }

}
