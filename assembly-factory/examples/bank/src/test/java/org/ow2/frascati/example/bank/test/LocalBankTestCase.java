/***
 * OW2 FraSCAti Assembly Factory Examples : Bank
 * Copyright (C) 2008-2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Damien Fournier
 */
package org.ow2.frascati.example.bank.test;

import java.util.Random;

import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.ow2.frascati.example.bank.Account;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

public class LocalBankTestCase {

  private static Account bankClient1;
  private static Account bankClient2;

  private static Component bankService;

  @AfterClass
  public static void close() throws Exception {
    TinfiDomain.close(bankService);
  }

  @BeforeClass
  public static void init() throws NoSuchInterfaceException {

    Factory factory = new Factory();

    System.out.println("\nCreate Local Bank Service \n");
    bankService = factory.getComposite("banklocal");

    bankClient1 = TinfiDomain.getService(bankService, Account.class, "client1");

    bankClient2 = TinfiDomain.getService(bankService, Account.class, "client2");

  }

  @Test
  public void testConversation() throws Exception {
    Assert.assertTrue(bankClient1.getAccountNumber() != bankClient2
        .getAccountNumber());
  }

  @Test
  public void testInstances() throws Exception {
    Assert.assertTrue(bankClient1 != bankClient2);
  }

  @Test
  public void testPrintLocal() throws Exception {

    Random r = new Random();

    System.out.println("\n--Operation on Client 1--");
    bankClient1.getBalance();
    bankClient1.deposit(r.nextInt(100));
    bankClient1.getBalance();
    System.out.println("\n--Operation on Client 2--");
    bankClient2.getBalance();
    bankClient2.deposit(r.nextInt(100));
    bankClient2.getBalance();

    System.out.println("\n\n\n");

  }

}
