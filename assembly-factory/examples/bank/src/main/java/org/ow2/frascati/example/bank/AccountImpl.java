/***
 * OW2 FraSCAti Assembly Factory Examples : Bank
 * Copyright (C) 2008-2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Damien Fournier
 */

package org.ow2.frascati.example.bank;

import java.util.Random;

import org.osoa.sca.annotations.Scope;

/**
 * @author dfournie
 */

@Scope("CONVERSATION")
public class AccountImpl implements Account {

  private float balance;
  private int accountNumber;

  public AccountImpl() {
    balance = new Random().nextInt(2000);
    accountNumber = new Random().nextInt(100000);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.ow2.frascati.example.bank.Account#deposit(float)
   */
  public void deposit(float amount) {
    balance = balance + amount;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.ow2.frascati.example.bank.AccountService#getAccountNumber()
   */
  public int getAccountNumber() {
    return accountNumber;
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.ow2.frascati.example.bank.Account#getBalance()
   */
  public String getBalance() {
    System.out.println("Service instance : " + this);
    System.out.println(Thread.currentThread());
    return Float.toString(balance);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.ow2.frascati.example.bank.Account#withdraw(float)
   */
  public void withdraw(float amount) {
    balance = balance - amount;
  }

}
