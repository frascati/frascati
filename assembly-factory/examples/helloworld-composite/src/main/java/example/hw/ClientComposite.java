/***
 * OW2 FraSCAti Assembly Factory Examples : HelloWorld
 * Copyright (C) 2008 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Damien.Fournier@inria.fr
 *
 * Author: Damien Fournier
 */

package example.hw;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

public class ClientComposite {

  public static void main(String args []) {
    Component scaDomain;
    try {

      System.out.println("\n\n\nStarting The Helloworld Composite Example \n");

      scaDomain = new Factory().getComposite("helloworldAssembly");
      
      java.lang.Runnable hello = TinfiDomain.getService(scaDomain,
          java.lang.Runnable.class, "r");
      
      // performs some calls
      hello.run();

      //closing sca domain
      TinfiDomain.close(scaDomain);

      System.out.println("\n\n\n");

   
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

}
