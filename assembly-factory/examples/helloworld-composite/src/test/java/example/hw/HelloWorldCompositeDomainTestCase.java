package example.hw;

import static org.junit.Assert.assertNotNull;
import junit.framework.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.core.domain.DomainException;
import org.ow2.frascati.factory.runtime.domain.SCADomain;
import org.ow2.frascati.factory.runtime.domain.api.Domain;
import org.ow2.frascati.tinfi.TinfiDomain;

public class HelloWorldCompositeDomainTestCase {

  private static Domain scaDomain;
  private static java.lang.Runnable service;
  private static Component helloworld;

  @BeforeClass
  public static void setUp() throws Exception {

    SCADomain domain = new SCADomain();
    scaDomain = domain.getDomain();

    helloworld = scaDomain.getComposite("helloworldAssembly");

    service = TinfiDomain.getService(helloworld, java.lang.Runnable.class, "r");
  }

  @Test
  public void testInit() {

    assertNotNull(service);
  }

  @Test
  public void testCall() {
    service.run();
  }
  
  @Test
  public void testList() {
    String [] list = scaDomain.getCompositesNames();
    System.out.println("\nLoaded composites list");
    for (String string : list) {
      System.out.println(string);
    }
  }
  
  @Test
  public void testGetLoadedComponent() {
    
    Component helloworld2;
    try {
      
      helloworld2 = scaDomain.getComposite("helloworld");
      Assert.assertEquals(helloworld, helloworld2);
            
    } catch (DomainException e) {
      e.printStackTrace();
    }
    
  }
  
  // TODO Not working
  //@Test
  public void testRemove() {
    try {
      System.out.println("\nRemove Helloworld composite");
      scaDomain.removeComposite("helloworld");
      String [] list = scaDomain.getCompositesNames();
      System.out.println("\nLoaded composites list");
      for (String string : list) {
        System.out.println(string);
        if(string.equals("helloworld")) Assert.fail();
      }
    } catch (DomainException e) {
      e.printStackTrace();
    }
    
  }
  
  

  @AfterClass
  public static void tearDown() throws Exception {
    //Nothing to do
  }

}
