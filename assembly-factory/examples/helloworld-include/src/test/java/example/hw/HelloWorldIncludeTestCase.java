package example.hw;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.factory.Factory;
import org.ow2.frascati.tinfi.TinfiDomain;

public class HelloWorldIncludeTestCase {

  private Component scaDomain;
  private java.lang.Runnable service;
  
  @Before
  public void setUp() throws Exception {
    scaDomain = new Factory().getComposite("helloworld.composite");

    service = TinfiDomain.getService(scaDomain,
        java.lang.Runnable.class, "r");
  }

  @Test
  public void testInit() {
    assertNotNull(scaDomain);
    assertNotNull(service);
  }
  
  @Test
  public void testCall() {
    service.run();
  }
  
  @After
  public void tearDown() throws Exception {
      TinfiDomain.close(scaDomain);   
  }

}
