============================================================================
OW2 FraSCAti Assembly Factory
Copyright (C) 2007-2010 INRIA, USTL

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org 

Author: Damien Fournier
				Philippe Merle
				Nicolas Dolet
				Christophe Demarey
				
============================================================================

Assembly Factory 1.3
--------------------

The Assembly Factory provides support for creating SCA components from SCA
assembly definitions which comply with the SCA architecture description
language specification 1.0. The Assembly Factory parses assembly definitions,
validates them, then performs calls to the Tinfi kernel and the Fractal Binding
Factory in order to generate SCA components implementation and/or create
component instances.

The Assembly Factory is implemented as SCA components too.

Table of Content
----------------
  1. Requirements
  2. Introduction
  3. Compiling and installing Assembly Factory artifacts
  4. Running the sample applications
  5. Using the Assembly Factory to generate SCA assemblies
  6. Using the Assembly Factory to instantiate SCA assemblies
  7. References
  
1. Requirements
---------------
Maven 2.0.9 is required to compile and run the Assembly Factory.
See http://maven.apache.org for instructions on downloading and using Maven.


2. Introduction
---------------
The Assembly Factory is composed of three main Maven artifacts:

 - the core-factory : This is the main module containing implementation
   of the assembly factory allowing to build SCA component instances.

 - the generate-factory : This is a personality of the assembly factory
   which allows to generate and compile SCA components. The generate factory
   is compiled as a Maven plugin. It permits to generate easily SCA component
   code. Since the core factory does not generate SCA components, building
   SCA component instances with the core factory always requires to use the
   generate factory plugin in the build of your maven project.

 - the runtime factory : This module combines both the advantages of the core
   and generate modules. It generates, compiles components and executes the
   application.

Several sample applications are also available in the assembly-factory/
examples directory.


3. Compiling and installing Assembly Factory artifacts
------------------------------------------------------
To compile and install the core, generate, and runtime factory modules in your
Maven local repository, from the root directory of the Assembly factory, type:
mvn clean install


4. Running the sample applications
----------------------------------
Sample applications are available in the assembly-factory/examples
directory. Those samples can be installed into your local Maven
directory by typing "mvn install" from the examples directory.
Each sample can be run separately from their directory using the command
"mvn -Pfactory:run"  

TODO: mvn -Pfactory:launcher

5. Using the Assembly Factory to generate SCA assemblies
--------------------------------------------------------
Since the generate-factory plug-in provides support for generating SCA
components source code, it can be used for your own FraSCAti project.
To use the generate-factory plug-in, just add the following lines into
the build description of your Maven project:

01   <plugin>
02     <groupId>org.ow2.frascati.factory</groupId>
03     <artifactId>frascati-factory-tools</artifactId>
04     <version>1.1</version>
05     <executions>
06       <execution>
07         <id>generate-factory</id>
08         <phase>generate-sources</phase>
09          <goals>
10           <goal>compile</goal>
11          </goals>
12        </execution>
13     </executions>
14        <configuration>
15          <factory>GenerateFactory</factory>
16          <srcs>
17            <src>src/main/java</src>
18          </srcs>
19          <libs>
20            <lib>src/main/resources</lib>
21          </libs>
22          <composite>composite</composite>
23        </configuration>
24   </plugin>

Additionally, you have to configure the plugin according to your Maven
project. You have to define your own <configuration> section, five XML
tags can be specified:

- <factory>
  is the name of the factory to use, "GenerateFactory" will only generate
  and compile component code, while "AssemblyFactory" will create component
  instances. "RuntimeFactory" will generate and execute components.
  "GenerateFactory" is taken as default.
  
- <srcs>
  is a list of directories containing the source code of
  the application. Default is "src/main/java"
  
- <libs>
  is a list of directories/jar files which are given as additional
  libraries. Default is "src/main/resources". Note project dependencies
  are automatically added as librairies to be used by frascati factory plug in
  
- <composite>
	is the name or the path to your SCA application main composite.
	
- <service> (Optional)
  A service to retrieve when starting the composite.
  
- <method> (Optional) 
  A method to invoke on the given <service> when starting the composite.
  Will exit FraSCAti once service has been invoked.


6. Using the Assembly Factory to create SCA assembly instances from Java
------------------------------------------------------------------------

The assembly factory (runtime-module) can be called through a Java class.
The factory API offers two methods which create SCA component instances.

new Factory().getComposite(String composite)

Return an instance of the composite specified by the "composite" parameter.
The "composite" parameter value can be a path to a composite name (if the
composite is a resource available in your current Java classpath), a file
on your local directory, or an URL.


new Factory().getComposite(String composite, URL [] jars)

Like the previous method, return an instance of the composite specified by
the "composite" parameter. But also enable to give URL of jar files to be
loaded into the used class loader.

For a detailed example of assembly factory usage, you can take a look at
the HelloWorld in the assembly-factory/examples/helloworld directory.

7. Using the Assembly Factory to load SCA contribution
------------------------------------------------------

The assembly factory (runtime-module) also offers partial support for
loading sca contribution archives (refers to SCA assembly model specification
for more details). Loading contribution archives from a Java program could be
done using :

new Factory().getContribution(String contribution)

Where 'contribution' is the file path to your contribution archive.
Currently the assembly factory support contributions package as zip files.
The archive must at least contains a contribution descriptor named
'sca-contribution.xml'. If jar libraries are present into the contribution
package, they will be automatically added to the assembly factory class loader.
The assembly factory will creates SCA composites instances for 'deployable'
defined in the contribution descriptor.


8. References
-------------
 - FraSCAti : http://frascati.ow2.org
 - Maven    : http://maven.apache.org
 - SCA      : http://www.osoa.org
 - SCOrWare : http://www.scorware.org

For any question concerning the Assembly Factory, please contact
frascati@ow2.org

Date of creation of this file: May 27, 2008.
Last modified: January 20, 2010.
