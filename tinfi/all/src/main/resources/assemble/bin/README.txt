============================================================================
OW2 FraSCAti Tinfi
Copyright (C) 2007-2018 Inria, Univ. Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Lionel Seinturier
============================================================================

Tinfi 1.6
---------

Tinfi is a runtime kernel for the SCA component model. Tinfi provides an
infrastructure for running Java applications which comply with version 1.0 of
the SCA specifications. Tinfi performs compile-time code generation to host
SCA-annotated component implementation classes in a SCA framework. The code
which is being generated conforms with the principles of the Fractal component
model. Tinfi is thus a Fractal personality for SCA. Tinfi extends Juliac which
is a Fractal tool for generating the source code of Fractal components.

The name Tinfi stands for "Tinfi Is Not a Fractal Implementation".

Tinfi is a free software distributed under the terms of the GNU Lesser
General Public license. Tinfi is written in Java.


Table of content
----------------
  1. Requirements
  2. Introduction
  3. Running the sample applications
  4. Compiling and running your own applications
  5. Obtaining the source code of Juliac
  6. References


1. Requirements
---------------
Ant 1.6.5 or superior is required to run applications developed with Tinfi.
See http://ant.apache.org for instructions on downloading and using Ant.


2. Introduction
---------------
This distribution of Tinfi is composed of the following directories:
- examples: some sample applications
- lib: the libraries for running Tinfi

The following samples applications are available in the examples directory:
- helloworld: a SCA Hello World application


3. Running the sample applications
----------------------------------
To compile and run the sample applications, type:
	cd examples/xxx (where xxx is the directory name of the sample)
	ant


4. Compiling and running your own applications
----------------------------------------------
The quickest way for using Tinfi is to mimic the organization of the
examples/helloworld directory. The source and the resources of your program
should be put under respectively, src/main/java and src/main/resources.

The Ant build.xml scripts provides two tasks:
- compile: for compiling the application
- execute: for running the application


5. Obtaining the source code of Tinfi
-------------------------------------
The source code of Tinfi can be obtained by checking out the trunk/runtime/tinfi
module of the FraSCAti SVN.
See
http://svn.forge.objectweb.org/cgi-bin/viewcvs.cgi/frascati/trunk/runtime/tinfi/
for that.


6. References
-------------
- Ant      : http://ant.apache.org
- Fractal  : http://fractal.ow2.org
- Juliac   : http://fractal.ow2.org/juliac
- SCA      : http://www.osoa.org


For any question, please contact: frascati@ow2.org
Date of creation of this file: 29 June 2008.
Last modified: 30 March 2013.
