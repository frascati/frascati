/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.osgi;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.stp.sca.ScaPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.ow2.frascati.model.osgi.OsgiFactory
 * @model kind="package"
 * @generated
 */
public interface OsgiPackage extends EPackage {
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "osgi";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://frascati.ow2.org/osgi";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "osgi";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  OsgiPackage eINSTANCE = org.ow2.frascati.model.osgi.impl.OsgiPackageImpl.init();

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.osgi.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.osgi.impl.DocumentRootImpl
   * @see org.ow2.frascati.model.osgi.impl.OsgiPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 0;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = ScaPackage.DOCUMENT_ROOT__MIXED;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = ScaPackage.DOCUMENT_ROOT__XMLNS_PREFIX_MAP;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = ScaPackage.DOCUMENT_ROOT__XSI_SCHEMA_LOCATION;

  /**
   * The feature id for the '<em><b>Allow</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ALLOW = ScaPackage.DOCUMENT_ROOT__ALLOW;

  /**
   * The feature id for the '<em><b>Base Export</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BASE_EXPORT = ScaPackage.DOCUMENT_ROOT__BASE_EXPORT;

  /**
   * The feature id for the '<em><b>Base Import</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BASE_IMPORT = ScaPackage.DOCUMENT_ROOT__BASE_IMPORT;

  /**
   * The feature id for the '<em><b>Binding</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING = ScaPackage.DOCUMENT_ROOT__BINDING;

  /**
   * The feature id for the '<em><b>Binding Ejb</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_EJB = ScaPackage.DOCUMENT_ROOT__BINDING_EJB;

  /**
   * The feature id for the '<em><b>Binding Jms</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_JMS = ScaPackage.DOCUMENT_ROOT__BINDING_JMS;

  /**
   * The feature id for the '<em><b>Binding Sca</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_SCA = ScaPackage.DOCUMENT_ROOT__BINDING_SCA;

  /**
   * The feature id for the '<em><b>Binding Ws</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_WS = ScaPackage.DOCUMENT_ROOT__BINDING_WS;

  /**
   * The feature id for the '<em><b>Binding Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__BINDING_TYPE = ScaPackage.DOCUMENT_ROOT__BINDING_TYPE;

  /**
   * The feature id for the '<em><b>Callback</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CALLBACK = ScaPackage.DOCUMENT_ROOT__CALLBACK;

  /**
   * The feature id for the '<em><b>Component Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPONENT_TYPE = ScaPackage.DOCUMENT_ROOT__COMPONENT_TYPE;

  /**
   * The feature id for the '<em><b>Composite</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__COMPOSITE = ScaPackage.DOCUMENT_ROOT__COMPOSITE;

  /**
   * The feature id for the '<em><b>Constraining Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CONSTRAINING_TYPE = ScaPackage.DOCUMENT_ROOT__CONSTRAINING_TYPE;

  /**
   * The feature id for the '<em><b>Contribution</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__CONTRIBUTION = ScaPackage.DOCUMENT_ROOT__CONTRIBUTION;

  /**
   * The feature id for the '<em><b>Definitions</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__DEFINITIONS = ScaPackage.DOCUMENT_ROOT__DEFINITIONS;

  /**
   * The feature id for the '<em><b>Deny All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__DENY_ALL = ScaPackage.DOCUMENT_ROOT__DENY_ALL;

  /**
   * The feature id for the '<em><b>Export</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT = ScaPackage.DOCUMENT_ROOT__EXPORT;

  /**
   * The feature id for the '<em><b>Export Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT_JAVA = ScaPackage.DOCUMENT_ROOT__EXPORT_JAVA;

  /**
   * The feature id for the '<em><b>Export Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__EXPORT_RESOURCE = ScaPackage.DOCUMENT_ROOT__EXPORT_RESOURCE;

  /**
   * The feature id for the '<em><b>Implementation</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION;

  /**
   * The feature id for the '<em><b>Implementation Bpel</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_BPEL = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_BPEL;

  /**
   * The feature id for the '<em><b>Implementation Composite</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_COMPOSITE;

  /**
   * The feature id for the '<em><b>Implementation Cpp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_CPP = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_CPP;

  /**
   * The feature id for the '<em><b>Implementation Ejb</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_EJB = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_EJB;

  /**
   * The feature id for the '<em><b>Implementation Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_JAVA = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_JAVA;

  /**
   * The feature id for the '<em><b>Implementation Spring</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_SPRING = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_SPRING;

  /**
   * The feature id for the '<em><b>Implementation Web</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_WEB = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_WEB;

  /**
   * The feature id for the '<em><b>Implementation Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_TYPE = ScaPackage.DOCUMENT_ROOT__IMPLEMENTATION_TYPE;

  /**
   * The feature id for the '<em><b>Import</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT = ScaPackage.DOCUMENT_ROOT__IMPORT;

  /**
   * The feature id for the '<em><b>Import Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT_JAVA = ScaPackage.DOCUMENT_ROOT__IMPORT_JAVA;

  /**
   * The feature id for the '<em><b>Import Resource</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPORT_RESOURCE = ScaPackage.DOCUMENT_ROOT__IMPORT_RESOURCE;

  /**
   * The feature id for the '<em><b>Include</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INCLUDE = ScaPackage.DOCUMENT_ROOT__INCLUDE;

  /**
   * The feature id for the '<em><b>Intent</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTENT = ScaPackage.DOCUMENT_ROOT__INTENT;

  /**
   * The feature id for the '<em><b>Interface</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE = ScaPackage.DOCUMENT_ROOT__INTERFACE;

  /**
   * The feature id for the '<em><b>Interface Cpp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_CPP = ScaPackage.DOCUMENT_ROOT__INTERFACE_CPP;

  /**
   * The feature id for the '<em><b>Interface Java</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_JAVA = ScaPackage.DOCUMENT_ROOT__INTERFACE_JAVA;

  /**
   * The feature id for the '<em><b>Interface Partner Link Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_PARTNER_LINK_TYPE = ScaPackage.DOCUMENT_ROOT__INTERFACE_PARTNER_LINK_TYPE;

  /**
   * The feature id for the '<em><b>Interface Wsdl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__INTERFACE_WSDL = ScaPackage.DOCUMENT_ROOT__INTERFACE_WSDL;

  /**
   * The feature id for the '<em><b>Permit All</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__PERMIT_ALL = ScaPackage.DOCUMENT_ROOT__PERMIT_ALL;

  /**
   * The feature id for the '<em><b>Policy Set</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__POLICY_SET = ScaPackage.DOCUMENT_ROOT__POLICY_SET;

  /**
   * The feature id for the '<em><b>Run As</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__RUN_AS = ScaPackage.DOCUMENT_ROOT__RUN_AS;

  /**
   * The feature id for the '<em><b>Ends Conversation</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ENDS_CONVERSATION = ScaPackage.DOCUMENT_ROOT__ENDS_CONVERSATION;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__REQUIRES = ScaPackage.DOCUMENT_ROOT__REQUIRES;

  /**
   * The feature id for the '<em><b>Anyextension</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__ANYEXTENSION = ScaPackage.DOCUMENT_ROOT__ANYEXTENSION;

  /**
   * The feature id for the '<em><b>Implementation Osgi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__IMPLEMENTATION_OSGI = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = ScaPackage.DOCUMENT_ROOT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.ow2.frascati.model.osgi.impl.OsgiImplementationImpl <em>Implementation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.ow2.frascati.model.osgi.impl.OsgiImplementationImpl
   * @see org.ow2.frascati.model.osgi.impl.OsgiPackageImpl#getOsgiImplementation()
   * @generated
   */
  int OSGI_IMPLEMENTATION = 1;

  /**
   * The feature id for the '<em><b>Policy Sets</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__POLICY_SETS = ScaPackage.IMPLEMENTATION__POLICY_SETS;

  /**
   * The feature id for the '<em><b>Requires</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__REQUIRES = ScaPackage.IMPLEMENTATION__REQUIRES;

  /**
   * The feature id for the '<em><b>Bundle</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__BUNDLE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Any Attribute</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__ANY_ATTRIBUTE = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Group</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION__GROUP = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Implementation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OSGI_IMPLEMENTATION_FEATURE_COUNT = ScaPackage.IMPLEMENTATION_FEATURE_COUNT + 3;


  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.osgi.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see org.ow2.frascati.model.osgi.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the containment reference '{@link org.ow2.frascati.model.osgi.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Implementation Osgi</em>'.
   * @see org.ow2.frascati.model.osgi.DocumentRoot#getImplementationOsgi()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_ImplementationOsgi();

  /**
   * Returns the meta object for class '{@link org.ow2.frascati.model.osgi.OsgiImplementation <em>Implementation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Implementation</em>'.
   * @see org.ow2.frascati.model.osgi.OsgiImplementation
   * @generated
   */
  EClass getOsgiImplementation();

  /**
   * Returns the meta object for the attribute '{@link org.ow2.frascati.model.osgi.OsgiImplementation#getBundle <em>Bundle</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Bundle</em>'.
   * @see org.ow2.frascati.model.osgi.OsgiImplementation#getBundle()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_Bundle();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.osgi.OsgiImplementation#getAnyAttribute <em>Any Attribute</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Any Attribute</em>'.
   * @see org.ow2.frascati.model.osgi.OsgiImplementation#getAnyAttribute()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_AnyAttribute();

  /**
   * Returns the meta object for the attribute list '{@link org.ow2.frascati.model.osgi.OsgiImplementation#getGroup <em>Group</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Group</em>'.
   * @see org.ow2.frascati.model.osgi.OsgiImplementation#getGroup()
   * @see #getOsgiImplementation()
   * @generated
   */
  EAttribute getOsgiImplementation_Group();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  OsgiFactory getOsgiFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals {
    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.osgi.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.osgi.impl.DocumentRootImpl
     * @see org.ow2.frascati.model.osgi.impl.OsgiPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Implementation Osgi</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__IMPLEMENTATION_OSGI = eINSTANCE.getDocumentRoot_ImplementationOsgi();

    /**
     * The meta object literal for the '{@link org.ow2.frascati.model.osgi.impl.OsgiImplementationImpl <em>Implementation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.ow2.frascati.model.osgi.impl.OsgiImplementationImpl
     * @see org.ow2.frascati.model.osgi.impl.OsgiPackageImpl#getOsgiImplementation()
     * @generated
     */
    EClass OSGI_IMPLEMENTATION = eINSTANCE.getOsgiImplementation();

    /**
     * The meta object literal for the '<em><b>Bundle</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__BUNDLE = eINSTANCE.getOsgiImplementation_Bundle();

    /**
     * The meta object literal for the '<em><b>Any Attribute</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__ANY_ATTRIBUTE = eINSTANCE.getOsgiImplementation_AnyAttribute();

    /**
     * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OSGI_IMPLEMENTATION__GROUP = eINSTANCE.getOsgiImplementation_Group();

  }

} //OsgiPackage
