/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.ow2.frascati.model.osgi;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.ow2.frascati.model.osgi.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.ow2.frascati.model.osgi.OsgiPackage#getDocumentRoot()
 * @model extendedMetaData="name='' kind='mixed'"
 * @generated
 */
public interface DocumentRoot extends org.eclipse.stp.sca.DocumentRoot {
  /**
   * Returns the value of the '<em><b>Implementation Osgi</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Implementation Osgi</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Implementation Osgi</em>' containment reference.
   * @see #setImplementationOsgi(OsgiImplementation)
   * @see org.ow2.frascati.model.osgi.OsgiPackage#getDocumentRoot_ImplementationOsgi()
   * @model containment="true" upper="-2" transient="true" volatile="true" derived="true"
   *        extendedMetaData="kind='element' name='implementation.osgi' namespace='##targetNamespace' affiliation='http://www.osoa.org/xmlns/sca/1.0#implementation'"
   * @generated
   */
  OsgiImplementation getImplementationOsgi();

  /**
   * Sets the value of the '{@link org.ow2.frascati.model.osgi.DocumentRoot#getImplementationOsgi <em>Implementation Osgi</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Implementation Osgi</em>' containment reference.
   * @see #getImplementationOsgi()
   * @generated
   */
  void setImplementationOsgi(OsgiImplementation value);

} // DocumentRoot
