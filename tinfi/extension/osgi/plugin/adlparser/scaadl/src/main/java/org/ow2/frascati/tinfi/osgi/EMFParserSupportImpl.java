/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.osgi;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.Implementation;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.juliac.desc.ComponentDesc;
import org.ow2.frascati.model.osgi.OsgiImplementation;
import org.ow2.frascati.model.osgi.OsgiPackage;

/**
 * EMF parser for SCA Assembly Language descriptors with support for the OSGi
 * component implementation type.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.3.1
 */
public class EMFParserSupportImpl
extends org.ow2.frascati.tinfi.emf.EMFParserSupportImpl {

    // ----------------------------------------------------------------------
    // Methods for transforming Composite to ComponentDesc
    // ----------------------------------------------------------------------
    
    @Override
    protected ComponentDesc<?> handleComponent(
        String compositeName, Component sub, String controllerDesc )
    throws IOException {
        
        /*
         * Component name and type.
         */
        String name = sub.getName();
        ComponentType subct = ecore2fractal(sub);        
        Implementation implementation = sub.getImplementation();
        ComponentDesc<?> cdesc = null;
        
        if( implementation instanceof OsgiImplementation ) {

            /*
             * OSGi bundle implementation.
             */
            OsgiImplementation osgiimpl = (OsgiImplementation) implementation;
            controllerDesc = "osgiPrimitive";
            String contentDesc = osgiimpl.getBundle();
            
            String id = "C"+(compidx++);
            cdesc =
                new ComponentDesc<Object>(
                    id,name,name,subct,controllerDesc,contentDesc,null);
        }
        else {
            cdesc = super.handleComponent(compositeName,sub,controllerDesc);
        }

        return cdesc;
    }    

    
    // -------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------

    @Override
    protected void register( ResourceSet resourceSet ) {
        super.register(resourceSet);
        org.eclipse.emf.ecore.EPackage.Registry reg =
            resourceSet.getPackageRegistry();
        reg.put( OsgiPackage.eNS_URI, OsgiPackage.eINSTANCE );        
    }
        
}
