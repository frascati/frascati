package example.hw.server;

import example.hw.itf.Console;
import example.hw.itf.Service;

public class ServerImpl implements Service, ServiceAttributes {
    
    private String header = "->";
    private int count = 1;
    
    public void print(final String msg) {
        Console.get().println("Server: begin printing...");
        for (int i = 0 ; i < (count) ; ++i) {
            Console.get().println(((header) + msg));
        }
        Console.get().println("Server: print done.");
    }
    
    public String getHeader() {
        return header;
    }
    
    public void setHeader(final String header) {
        this.header = header;
    }
    
    public int getCount() {
        return count;
    }
    
    public void setCount(final int count) {
        this.count = count;
    }
}