/***
 * OW2 FraSCAti Tinfi Examples : Jetty OSGi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: Lionel.Seinturier@univ-lille1.fr
 *
 * Author: Lionel Seinturier
 */

package example.jetty;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Dictionary;
import java.util.Hashtable;

import junit.framework.Assert;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;

public class ClientImpl implements Runnable {

    public ClientImpl() {
        System.err.println("CLIENT created");
    }
    
    @Init
    public void init() {
        System.err.println("CLIENT initialized");
    }
    
    @Reference 
    public void setPrintService( ManagedService service ) {
        this.s = service;
    }
    private ManagedService s;

    public void run() {
        int attempt = 0;
        while(true) {
            try {
                innerRun();
                return;
            }
            catch (IOException e) {
                if( e instanceof SocketException ) {
                    /*
                     * Assume that, if we can not connect, the reason is that
                     * Jetty is not yet up and running. Perform up to 5 attempts
                     * every 2 seconds.
                     */
                    if( attempt > 4 ) {
                        throw new RuntimeException(e);
                    }
                    sleep(2000);
                    attempt++;
                }
                else {
                    throw new RuntimeException(e);
                }
            }
            catch (ConfigurationException e) {
                throw new RuntimeException(e);
            }
        }
    }
    
    private void innerRun()
    throws IOException, ConfigurationException {

        /*
         * Update the configuration of Jetty by changing the TCP port number.
         * The default port number (80) is not authorized for user access on
         * Unix systems.
         */
        Dictionary dict = new Hashtable();
        dict.put("org.osgi.service.http.port","8080");
        s.updated(dict);
        
        try {
            load("http://localhost:8080");
            String msg = "Jetty should have thrown FileNotFoundException";
            Assert.fail(msg);
        }
        catch( FileNotFoundException fnfe ) {
            /*
             * Jetty does not have any file to serve so throwing
             * FileNotFoundException is the correct behavior. This means that
             * the HTTP request has been accepted and served with a 404 return
             * code. Else, ConnectException would have been thrown.
             */
        }
        
        /*
         * Update the configuration of Jetty by changing the TCP port number.
         */
        dict = new Hashtable();
        dict.put("org.osgi.service.http.port","8081");
        s.updated(dict);
        
        /*
         * Check that Jetty does no longer serve requests on TCP port number
         * 8080.
         */
        try {
            load("http://localhost:8080");
            String msg = "Jetty should have thrown ConnectException";
            Assert.fail(msg);
        }
        catch( ConnectException ce ) {}

        /*
         * Check that Jetty serves requests on TCP port number 8081.
         */
        try {
            load("http://localhost:8081");
            String msg = "Jetty should have thrown FileNotFoundException";
            Assert.fail(msg);
        }
        catch( FileNotFoundException fnfe ) {}
    }
    
    private void load( String url ) throws IOException {
        System.err.println("Loading URL: "+url);
        URL u = new URL(url);
        URLConnection uc = u.openConnection();
        uc.getInputStream();
    }
    
    private void sleep( long millis ) throws RuntimeException {
        try {
            Thread.sleep(millis);
        }
        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
