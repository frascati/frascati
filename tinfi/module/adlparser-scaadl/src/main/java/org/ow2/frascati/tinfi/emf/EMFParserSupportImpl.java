/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributors: Damien Fournier, Philippe Merle
 */

package org.ow2.frascati.tinfi.emf;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.stp.sca.BaseReference;
import org.eclipse.stp.sca.BaseService;
import org.eclipse.stp.sca.Component;
import org.eclipse.stp.sca.ComponentReference;
import org.eclipse.stp.sca.ComponentService;
import org.eclipse.stp.sca.Composite;
import org.eclipse.stp.sca.DocumentRoot;
import org.eclipse.stp.sca.Implementation;
import org.eclipse.stp.sca.Interface;
import org.eclipse.stp.sca.JavaImplementation;
import org.eclipse.stp.sca.JavaInterface;
import org.eclipse.stp.sca.Multiplicity;
import org.eclipse.stp.sca.PropertyValue;
import org.eclipse.stp.sca.Reference;
import org.eclipse.stp.sca.SCAImplementation;
import org.eclipse.stp.sca.ScaPackage;
import org.eclipse.stp.sca.Service;
import org.eclipse.stp.sca.Wire;
import org.eclipse.stp.sca.util.ScaResourceFactoryImpl;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.julia.type.BasicComponentType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.StringHelper;
import org.objectweb.fractal.juliac.desc.AbstractADLSupportImpl;
import org.objectweb.fractal.juliac.desc.AttributeDesc;
import org.objectweb.fractal.juliac.desc.BindingDesc;
import org.objectweb.fractal.juliac.desc.BindingType;
import org.objectweb.fractal.juliac.desc.ComponentDesc;

/**
 * EMF parser for SCA Assembly Language descriptors.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Damien Fournier <Damien.Fournier@inria.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 1.1
 */
public class EMFParserSupportImpl extends AbstractADLSupportImpl<Composite> {

    // --------------------------------------------------------------------
    // Implementation of the ADLParserSupportItf interface
    // --------------------------------------------------------------------
    
    /**
     * The name of the package where component-based membrane for scaPrimitive,
     * scaComposite, etc. components are defined. See module/opt-comp/core for
     * these definitions.
     */
    final private static String MEMBRANE_PKG_NAME =
        "org.ow2.frascati.tinfi.membrane";

    // Declared protected to enable usage in frascati-tinfi-osgi-scaadl
    protected int compidx = 0;

    public boolean acceptADLDesc( String adl ) {
        /*
         * Do not use File#separatorChar (even on Windows) as it leads to is
         * being null when the resource is stored in a jar file.
         */
        String adlFileName = adl.replace('.','/')+".composite";
        ClassLoader loader = jc.getClassLoader();
        InputStream is = loader.getResourceAsStream(adlFileName);
        boolean exist = (is != null);
        return exist;
    }
    
    
    // ----------------------------------------------------------------------
    // Methods for handling Composite
    // ----------------------------------------------------------------------
    
    protected Composite load( String adl, Map<Object,Object> context )
    throws IOException {
        Composite composite = innerparse(adl,context);
        return composite;
    }
    
    @Override
    protected SourceCodeGeneratorItf getComponentDescFactoryClassGenerator(
        ComponentDesc<?> cdesc, String targetname  ) {
        
        return new ComponentDescFactoryClassGenerator(jc,cdesc,targetname);
    }
    
    /**
     * Parse the specified ADL descriptor and return a model of it.
     * 
     * @param adl      the fully-qualified name of the ADL descriptor
     * @param context  contextual information
     * @return         the model of the specified ADL
     * @since 1.4.5
     */
    /*
     * Declared protected to enable extension in frascati-tinfi-sca-parser.
     * A way to provide backward compatibility with the version of ADLParserItf
     * prior to Tinfi 1.4.5 that frascati-tinfi-sca-parser was using when
     * written.
     */
    protected Composite innerparse( String adl, Map<Object,Object> context )
    throws IOException {
        
        ClassLoader loader = jc.getClassLoader();
        
        String adlFileName = adl.replace('.','/')+".composite";
        URL url = loader.getResource(adlFileName);
        if( url == null ) {
            final String msg = "Cannot load "+adlFileName;
            throw new IOException(msg);
        }
        
        URI uri;
        try {
            uri = url.toURI();
        }
        catch (URISyntaxException e) {
            throw new IOException(e);
        }        
        ResourceSet resourceSet = new ResourceSetImpl();

        /*
         * Register the appropriate resource factory to handle all file
         * extensions.
         */
        Registry resFactReg = resourceSet.getResourceFactoryRegistry();
        Map<String,Object> map = resFactReg.getExtensionToFactoryMap();
        Object scaResFact = new ScaResourceFactoryImpl();
        map.put("*",scaResFact);

        /*
         * Register the EMF packages.
         */
        register(resourceSet);
                
        /*
         * Parse the composite file.
         */
        org.eclipse.emf.common.util.URI resourceUri =
            org.eclipse.emf.common.util.URI.createURI(uri.toString());
        Resource resource = resourceSet.getResource(resourceUri,true);

        /*
         * Get the root node of the composite model.
         */
        EList<EObject> list = resource.getContents();
        DocumentRoot root = (DocumentRoot) list.get(0);
        Composite composite = root.getComposite();
        return composite;
    }

    /**
     * Transform the specified {@link Composite} to {@link ComponentDesc}.
     */
    protected ComponentDesc<Composite> toComponentDesc( Composite composite )
    throws IOException {
        
        /*
         * Naming convention for component-based membranes. The thing is that
         * the grammar of the SCA ADL does not allow specifying a controller
         * descriptor (or could we find a way to specify it with an existing
         * concept?) just like Fractal ADL enables it. We then assume that all
         * parsed ADL with a name that starts with
         * org.ow2.frascati.tinfi.membrane (MEMBRANE_PKG_NAME constant)
         * correspond to a component-based membrane.
         */
        String id = "C"+(compidx++);
        ComponentType ct = ecore2fractal(composite);
        String name = composite.getName();
        String controllerDesc =
            name.startsWith(MEMBRANE_PKG_NAME) ? "mComposite" : "scaComposite";

        ComponentDesc<Composite> cdesc =
            new ComponentDesc<Composite>(
                id,name,name,ct,controllerDesc,null,composite);
        
        /*
         * Subcomponents.
         */
        EList<Component> subs = composite.getComponent();
        Map<String,ComponentDesc<?>> subcdescs =
            new HashMap<String,ComponentDesc<?>>();
        String subControllerDesc =
            name.startsWith(MEMBRANE_PKG_NAME) ? "mPrimitive" : "scaPrimitive";
        for (Component sub : subs) {
            ComponentDesc<?> subcdesc =
                handleComponent(name,sub,subControllerDesc);
            handleProperties(sub,subcdesc);
            cdesc.addSubComponent(subcdesc);
            subcdesc.addSuperComponent(cdesc);
            String subname = sub.getName();
            subcdescs.put(subname,subcdesc);
        }
        
        /*
         * Export bindings.
         * Promotion links between a service declared by a subcomponent and a
         * service declared by the composite.
         */
        EList<Service> services = composite.getService();
        for (Service service : services) {
            handleExport(cdesc,name,service,subcdescs);
        }
        
        /*
         * Import bindings.
         * Promotion links between a reference declared by a subcomponent and a
         * reference declared by the composite.
         */
        EList<Reference> references = composite.getReference();
        for (Reference reference : references) {
            handleImport(cdesc,name,reference,subcdescs);
        }
        
        /*
         * Internal bindings.
         * Internal links between two subcomponents. There are similar to wires
         * except that they are declared with the target parameter of the
         * <reference> tag.
         */
        for (Component sub : subs) {
            handleInternal(name,sub,subcdescs);
        }
                
        /*
         * Wires.
         * Internal links declared with the <wire> tag between two subcomponents
         * of the composite.
         */
        EList<Wire> wires = composite.getWire();
        for (Wire wire : wires) {
            handleWire(wire,subcdescs);
        }
        
        return cdesc;
    }

    // Declared protected to enable extension in frascati-tinfi-osgi-scaadl
    protected ComponentDesc<?> handleComponent(
        String compositeName, Component sub, String controllerDesc )
    throws IOException {
        
        /*
         * Component name and type.
         */
        String name = sub.getName();
        ComponentType subct = ecore2fractal(sub);
        Implementation implementation = sub.getImplementation();
        ComponentDesc<?> cdesc = null;
        
        if( implementation instanceof JavaImplementation ) {

            /*
             * Java implementation.
             */
            JavaImplementation jimpl = (JavaImplementation) implementation;
            String contentDesc = jimpl.getClass_();
            
            String id = "C"+(compidx++);
            cdesc =
                new ComponentDesc<JavaImplementation>(
                    id,name,name,subct,controllerDesc,contentDesc,jimpl);
        }
        else if( implementation instanceof SCAImplementation ) {
            
            /*
             * Composite implementation.
             */
            
            SCAImplementation scaimpl = (SCAImplementation) implementation;
            QName qname = scaimpl.getName();

            String namespaceURI = qname.getNamespaceURI();
            String localpart = qname.getLocalPart();
            if( ! localpart.endsWith(".composite") ) {
                final String msg =
                    "Unexpected name (local part should end with .composite "+
                    qname+" in implementation.composite for subcomponent "+
                    sub.getName()+" in composite "+compositeName;
                throw new IOException(msg);
            }
            // Remove the trailing ".composite"
            String fname = localpart.substring(0,localpart.length()-10);
            String adl = namespaceURI.replace('/','.')+'.'+fname;
            
            cdesc = parse(adl,null);
        }
        else {
            final String msg =
                "Unsupported implementation type "+
                implementation.getClass().getName()+" for subcomponent "+
                sub.getName()+" in composite "+compositeName;
            throw new IOException(msg);
        }
        
        return cdesc;
    }
    
    private void handleExport(
        ComponentDesc<?> cdesc, String compositeName,
        Service service, Map<String,ComponentDesc<?>> subcdescs )
    throws IOException {
        
        String serviceName = service.getName();
        String promote = service.getPromote();
        
        /*
         * I was previously using String#split("/"). Yet component-based
         * membranes define interfaces such as //binding-controller. Referencing
         * such component interfaces gives something like
         * BC///binding-controller which makes String#split("/") no longer
         * appropriate.
         */
        String[] promotes = StringHelper.around(promote,'/');
        
        if( promotes.length != 2 ) {
            final String msg =
                "Illegal service promotion "+promote+" in composite "+
                compositeName;
            throw new IOException(msg);
        }
        
        ComponentDesc<?> target = subcdescs.get(promotes[0]);
        if( target == null ) {
            final String msg =
                "No such target component "+promotes[0]+
                " in service promotion "+promote+" in composite "+compositeName;
            throw new IOException(msg);
        }
        
        createBinding(BindingType.EXPORT,cdesc,serviceName,target,promotes[1]);
    }
    
    private void handleImport(
        ComponentDesc<?> cdesc, String compositeName,
        Reference reference, Map<String,ComponentDesc<?>> subcdescs )
    throws IOException {
        
        String referenceName = reference.getName();
        String promote = reference.getPromote();
        String[] promotes = promote.split(" ");
        for (String pr : promotes) {
            String[] prs = StringHelper.around(pr,'/');
            if( prs.length != 2 ) {
                final String msg =
                    "Illegal reference promotion "+promote+" in composite "+
                    compositeName;
                throw new IOException(msg);
            }
            
            ComponentDesc<?> src = subcdescs.get(prs[0]);
            if( src == null ) {
                final String msg =
                    "No such source component "+prs[0]+
                    " in reference promotion "+promote+" in composite "+
                    compositeName;
                throw new IOException(msg);
            }
            
            createBinding(BindingType.IMPORT,src,prs[1],cdesc,referenceName);
        }
    }
        
    private void handleWire( Wire wire, Map<String,ComponentDesc<?>> subcdescs )
    throws IOException {

        // Wire source
        String source = wire.getSource();
        String[] sources = StringHelper.around(source,'/');
        if( sources.length != 2 ) {
            final String msg = "Illegal source '"+source+"' in wire "+wire;
            throw new IOException(msg);
        }
        ComponentDesc<?> clt = subcdescs.get(sources[0]);
        if( clt == null ) {
            final String msg =
                "No such source component '"+sources[0]+"' in wire "+wire;
            throw new IOException(msg);
        }
        InterfaceType cltItf;
        try {
            ComponentType cltct = clt.getCT();
            cltItf = cltct.getFcInterfaceType(sources[1]);
        }
        catch( NoSuchInterfaceException nsie ) {
            final String msg =
                "No such source component interface '"+sources[1]+"' in wire "+
                wire;
            throw new IOException(msg);
        }
        if( ! cltItf.isFcClientItf() ) {
            final String msg = sources[1]+" is not a client interface in wire "+wire;
            throw new IOException(msg);
        }
        
        // Wire target
        String target = wire.getTarget();
        String[] targets = StringHelper.around(target,'/');
        if( targets.length != 2 ) {
            final String msg = "Illegal target '"+target+"' in wire "+wire;
            throw new IOException(msg);
        }
        ComponentDesc<?> srv = subcdescs.get(targets[0]);
        if( srv == null ) {
            final String msg =
                "No such target component '"+targets[0]+"' in wire "+wire;
            throw new IOException(msg);
        }
        try {
            ComponentType srvct = srv.getCT();
            InterfaceType srvItf = srvct.getFcInterfaceType(targets[1]);
            if( srvItf.isFcClientItf() ) {
                final String msg =
                    targets[1]+" is not a server interface in wire "+wire;
                throw new IOException(msg);
            }
        }
        catch( NoSuchInterfaceException nsie ) {
            // TODO: Is it always an error?
            // Check with all compiled FraSCAti .composite files.
//            String msg =
//                "No such target component interface '"+targets[1]+
//                  "' in wire "+wire;
//            throw new IOException(msg);
        }

        /*
         * When the wire source is a collection client interface, add the name
         * of the target component at the end of the source interface name. This
         * allows to generate an unique source interface name (as component
         * names are unique.)
         */
        if( cltItf.isFcCollectionItf() &&
            cltItf.getFcItfName().equals(sources[1]) ) {
            sources[1] = sources[1] + '-' + targets[0];
        }

        // Create binding
        createBinding(BindingType.NORMAL,clt,sources[1],srv,targets[1]);
    }
    
    private void handleInternal(
        String compositeName, Component sub,
        Map<String,ComponentDesc<?>> subcdescs )
    throws IOException {
        
        String name = sub.getName();
        ComponentDesc<?> clt = subcdescs.get(name);
        
        EList<ComponentReference> references = sub.getReference();
        for (ComponentReference reference : references) {
            
            String target = reference.getTarget();
            if( target!=null && target.length()>0 ) {
                                
                // Target(s)
                String[] targets = target.split(" ");
                int i = 0;
                for (String targ : targets) {
                    
                    // Source
                    String cltItfName = reference.getName();
                    if( targets.length > 1 ) {
                        /*
                         * In case of cardinality 0..n or 1..n for the targets,
                         * add an auto-incremented suffix to the source
                         * interface name to obtain the unique identifier needed
                         * by the Julia Binding Controller.
                         */
                        cltItfName += i;
                    }
                    
                    String[] targs = StringHelper.around(targ,'/');
                    if( targs.length != 2 ) {
                        final String msg =
                            "Illegal reference "+target+" in composite "+
                            compositeName;
                        throw new IOException(msg);
                    }
                    ComponentDesc<?> srv = subcdescs.get(targs[0]);
                    if( srv == null ) {
                        final String msg =
                            "No such target component "+targs[0]+
                            " in reference "+target+" in composite "+
                            compositeName;
                        throw new IOException(msg);
                    }
                    
                    // Create binding
                    createBinding(
                        BindingType.NORMAL,clt,cltItfName,srv,targs[1]);
                    i++;
                }
            }
        }
    }
    
    private void createBinding(
        BindingType bindingType, ComponentDesc<?> clt, String cltItfName,
        ComponentDesc<?> srv, String srvItfName ) {
        
        BindingDesc bdesc =
            new BindingDesc(bindingType,clt,cltItfName,srv,srvItfName);
        clt.putBinding(cltItfName,bdesc);
    }
    
    private void handleProperties( Component sub, ComponentDesc<?> cdesc ) {
        EList<PropertyValue> propvalues = sub.getProperty();
        for (PropertyValue propvalue : propvalues) {
            handleProperty(cdesc,propvalue);                
        }
    }
    
    private void handleProperty(
        ComponentDesc<?> cdesc, PropertyValue propvalue )
    throws IllegalArgumentException {

        String name = propvalue.getName();
        QName typeqname = propvalue.getType();
        String type = null;
        if( typeqname != null ) {
            String ns = typeqname.getNamespaceURI();
            if( ! ns.equals("http://frascati.ow2.org/xmlns/java") ) {
                final String msg = "Unsupported namespace URI: "+ns;
                throw new IllegalArgumentException(msg);
            }
            type = typeqname.getLocalPart();
        }
        String value = propvalue.getValue();
        String setterName =
            "set"+name.substring(0,1).toUpperCase()+name.substring(1);
        AttributeDesc adesc =
            new AttributeDesc(cdesc,null,name,value,type,setterName);
        cdesc.putAttribute(name,adesc);
    }
    
    /**
     * Return the Fractal component type corresponding to the specified SCA
     * ecore composite.
     * 
     * @param composite  the SCA ecore composite
     * @return           the Fractal component type
     * @throws IOException  if the Fractal component type cannot be created
     */
    private ComponentType ecore2fractal( Composite composite )
    throws IOException {
        
        EList<Service> services = composite.getService();
        EList<Reference> references = composite.getReference();
        InterfaceType[] its =
            new InterfaceType[ services.size() + references.size() ];
        int i = 0;
        
        for (Service service : services) {
            its[i] = ecore2fractal(service);
            i++;
        }
        for (Reference reference : references) {
            its[i] = ecore2fractal(reference);
            i++;
        }
        
        ComponentType ct = getComponentTypeIOE(its);
        return ct;
    }
    
    /**
     * Return the Fractal component type corresponding to the specified SCA
     * ecore composite.
     * 
     * @param component  the SCA ecore composite
     * @return           the Fractal component type
     * @throws IOException  if the Fractal component type cannot be created
     */
    // Declared protected to enable call from extension in frascati-tinfi-osgi-scaadl
    protected ComponentType ecore2fractal( Component component )
    throws IOException {
        
        EList<ComponentService> services = component.getService();
        EList<ComponentReference> references = component.getReference();
        InterfaceType[] its =
            new InterfaceType[ services.size() + references.size() ];
        int i = 0;
        
        for (BaseService service : services) {
            its[i] = ecore2fractal(service);
            i++;
        }
        for (BaseReference reference : references) {
            its[i] = ecore2fractal(reference);
            i++;
        }
        
        ComponentType ct = getComponentTypeIOE(its);
        return ct;
    }
    
    /**
     * Return the Fractal interface type corresponding to the specified SCA
     * ecore service.
     * 
     * @param service  the SCA ecore service
     * @return         the Fractal interface type
     */
    private InterfaceType ecore2fractal( BaseService service ) {
        String name = service.getName();
        Interface itf = service.getInterface();
        InterfaceType it =
            ecore2fractal(name,itf,ONE_TO_ONE,TypeFactory.SERVER);
        return it;
    }
    
    /**
     * Return the Fractal interface type corresponding to the specified SCA
     * ecore reference.
     * 
     * @param service  the SCA ecore reference
     * @return         the Fractal interface type
     */
    private InterfaceType ecore2fractal( BaseReference reference ) {
        String name = reference.getName();
        Interface itf = reference.getInterface();
        Multiplicity multiplicity = reference.getMultiplicity();
        String mult = multiplicity.getLiteral();
        InterfaceType it = ecore2fractal(name,itf,mult,TypeFactory.CLIENT);
        return it;
    }
    
    /**
     * Return the component type corresponding to the specified interface types.
     * 
     * @param its  the interface types
     * @return     the component type
     * @throws IOException  if the component type cannot be created
     */
    private ComponentType getComponentTypeIOE( InterfaceType[] its )
    throws IOException {
        try {
            ComponentType ct = new BasicComponentType(its);
            return ct;
        }
        catch (InstantiationException e) {
            throw new IOException(e);
        }
    }
    
    /**
     * Return the Fractal interface type corresponding to the specified SCA
     * ecore interface definition.
     * 
     * @param name      the interface name
     * @param itf       the SCA ecore interface definition
     * @param mult      the interface multiplicity (0..1, 1..1, 0..n or 1..n)
     * @param isClient  <code>true</code> if the interface is required
     *                  <code>false</code> if the interface is provided
     * @return          the corresponding Fractal interface type
     * @throws IllegalArgumentException
     *      if the interface multiplicity if not of the four following legal
     *      values: 0..1, 1..1, 0..n or 1..n 
     */
    private InterfaceType ecore2fractal(
        String name, Interface itf, String mult, boolean isClient )
    throws IllegalArgumentException {

        if( ! cardinalities.containsKey(mult) ) {
            final String msg = "Unsupported interface multiplicity: "+mult;
            throw new IllegalArgumentException(msg);
        }
        
        JavaInterface jitf = (JavaInterface) itf;
        String signature = jitf.getInterface();
        boolean isOptional = contingencies.get(mult);
        boolean isCollection = cardinalities.get(mult);
        
        InterfaceType it =
            new BasicInterfaceType(
                name,signature,isClient,isOptional,isCollection);
        return it;
    }
    
    final private static String ZERO_TO_ONE = "0..1";
    final private static String ONE_TO_ONE = "1..1";
    final private static String ZERO_TO_N = "0..n";
    final private static String ONE_TO_N = "1..n";
    
    final private static Map<String,Boolean> contingencies =
        new HashMap<String,Boolean>() {
            private static final long serialVersionUID = 1306190174200744738L;
        {
            put(ZERO_TO_ONE,TypeFactory.OPTIONAL);
            put(ONE_TO_ONE,TypeFactory.MANDATORY);
            put(ZERO_TO_N,TypeFactory.OPTIONAL);
            put(ONE_TO_N,TypeFactory.MANDATORY);
        }};

    final private static Map<String,Boolean> cardinalities =
        new HashMap<String,Boolean>() {
            private static final long serialVersionUID = 1306190174200744738L;
        {
            put(ZERO_TO_ONE,TypeFactory.SINGLE);
            put(ONE_TO_ONE,TypeFactory.SINGLE);
            put(ZERO_TO_N,TypeFactory.COLLECTION);
            put(ONE_TO_N,TypeFactory.COLLECTION);
        }};
        
        
    // -------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------

    // Declared protected to enable extension in frascati-tinfi-osgi-scaadl
    protected void register( ResourceSet resourceSet ) {
        org.eclipse.emf.ecore.EPackage.Registry reg =
            resourceSet.getPackageRegistry();
        reg.put( ScaPackage.eNS_URI, ScaPackage.eINSTANCE );
    }
}
