<?xml version="1.0" encoding="UTF-8"?>

<!--  OW2 FraSCAti Tinfi                                                            -->
<!--  Copyright (C) 2010-2018 Inria, Univ. Lille 1                                  -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Lionel Seinturier                                                     -->

<composite
      name="org.ow2.frascati.tinfi.membrane.SCAPrimitive"
    xmlns="http://www.osoa.org/xmlns/sca/1.0"
>

    <!-- ================== -->
    <!-- Control interfaces -->
    <!-- ================== -->

    <!-- Fractal control interfaces -->

    <service name="//component" promote="Comp///component" >
        <interface.java interface="org.objectweb.fractal.api.Component" />
    </service>
    <service name="//binding-controller" promote="BC///binding-controller">
        <interface.java interface="org.objectweb.fractal.api.control.BindingController" />
    </service>
    <service name="//lifecycle-controller" promote="LC///lifecycle-controller" >
        <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
    </service>
    <service name="//name-controller" promote="NC///name-controller" >
        <interface.java interface="org.objectweb.fractal.api.control.NameController" />
    </service>
    <service name="//super-controller" promote="SC///super-controller" >
        <interface.java interface="org.objectweb.fractal.julia.control.content.SuperControllerNotifier" />
    </service>
    
    <!-- Koch hidden control interfaces -->

    <service name="///interceptor-controller" promote="IC////interceptor-controller" >
        <interface.java interface="org.objectweb.fractal.koch.control.interceptor.InterceptorController" />
    </service>
    <service name="///membrane-controller" promote="MC////membrane-controller" >
        <interface.java interface="org.objectweb.fractal.koch.control.membrane.MembraneController" />
    </service>
    
    <!-- Tinfi control interfaces -->

    <service name="//sca-component-controller" promote="SCACompCtx///sca-component-controller" >
        <interface.java interface="org.oasisopen.sca.ComponentContext" />
    </service>
    <service name="//sca-intent-controller" promote="SCAIC///sca-intent-controller" >
        <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAIntentController" />
    </service>
    <service name="//sca-property-controller" promote="SCAPC///sca-property-controller" >
        <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAPropertyController" />
    </service>
    <service name="///sca-content-controller" promote="SCACC////sca-content-controller" >
        <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
    </service>


    <!-- ================== -->
    <!-- Control components -->
    <!-- ================== -->

    <!-- Fractal/Julia control components -->

    <component name="Comp">
        <service name="//component">
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </service>
        <implementation.java class="org.objectweb.fractal.julia.ComponentControllerImpl" />
    </component>

    <component name="BC">
        <service name="//binding-controller">
            <interface.java interface="org.objectweb.fractal.api.control.BindingController" />
        </service>
        <reference name="//component" target="Comp///component" >
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </reference>
        <reference name="//super-controller" target="SC///super-controller" >
            <interface.java interface="org.objectweb.fractal.julia.control.content.SuperControllerNotifier" />
        </reference>
        <reference name="//lifecycle-controller" target="LC///lifecycle-controller" >
            <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
        </reference>
        <reference name="///sca-content-controller" target="SCACC////sca-content-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.binding.SCABindingControllerImpl" />
    </component>

    <component name="LC">
        <service name="//lifecycle-controller">
            <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
        </service>
        <reference name="//component" target="Comp///component" >
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </reference>
        <reference name="///sca-content-controller" target="SCACC////sca-content-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.lifecycle.SCALifeCycleControllerImpl" />
    </component>

    <component name="NC">
        <service name="//name-controller">
            <interface.java interface="org.objectweb.fractal.api.control.NameController" />
        </service>
        <implementation.java class="org.objectweb.fractal.julia.control.name.NameControllerImpl" />
    </component>

    <component name="SC">
        <service name="//super-controller">
            <interface.java interface="org.objectweb.fractal.julia.control.content.SuperControllerNotifier" />
        </service>
        <implementation.java class="org.objectweb.fractal.julia.control.content.SuperControllerImpl" />
    </component>
    
    <!-- Koch control components -->

    <component name="IC">
        <service name="///interceptor-controller">
            <interface.java interface="org.objectweb.fractal.koch.control.interceptor.InterceptorController" />
        </service>
        <reference name="//lifecycle-controller" target="LC///lifecycle-controller" >
            <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
        </reference>
        <reference name="///sca-content-controller" target="SCACC////sca-content-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </reference>
        <reference name="//sca-intent-controller" target="SCAIC///sca-intent-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAIntentController" />
        </reference>
        <implementation.java class="org.objectweb.fractal.koch.control.interceptor.InterceptorControllerImpl" />
        <property name="interceptors">(org.ow2.frascati.tinfi.opt.oo.InterceptorClassGenerator org.ow2.frascati.tinfi.opt.oo.SCATinfiInterceptorSourceCodeGenerator org.objectweb.fractal.juliac.proxy.LifeCycleNFASourceCodeGenerator org.ow2.frascati.tinfi.opt.oo.SCAContentInterceptorSourceCodeGenerator org.ow2.frascati.tinfi.opt.oo.SCAIntentInterceptorSourceCodeGenerator)</property>
    </component>

    <component name="MC">
        <service name="///membrane-controller">
            <interface.java interface="org.objectweb.fractal.koch.control.membrane.MembraneController" />
        </service>
        <implementation.java class="org.objectweb.fractal.koch.control.membrane.BasicMembraneControllerImpl" />
    </component>
    
    <!-- Tinfi control components -->

    <component name="SCACompCtx">
        <service name="//sca-component-controller">
            <interface.java interface="org.oasisopen.sca.ComponentContext" />
        </service>
        <reference name="//component" target="Comp///component" >
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </reference>
        <reference name="///sca-content-controller" target="SCACC////sca-content-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </reference>
        <reference name="//sca-property-controller" target="SCAPC///sca-property-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAPropertyController" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.component.ComponentContextImpl" />
    </component>

    <component name="SCAIC">
        <service name="//sca-intent-controller">
            <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAIntentController" />
        </service>
        <reference name="//component" target="Comp///component" >
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </reference>
        <reference name="//lifecycle-controller" target="LC///lifecycle-controller" >
            <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.intent.SCAPrimitiveIntentControllerImpl" />
    </component>

    <component name="SCAPC">
        <service name="//sca-property-controller">
            <interface.java interface="org.ow2.frascati.tinfi.api.control.SCAPropertyController" />
        </service>
        <reference name="///sca-content-controller" target="SCACC////sca-content-controller" >
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </reference>
        <reference name="//name-controller" target="NC///name-controller" >
            <interface.java interface="org.objectweb.fractal.api.control.NameController" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.property.SCAPrimitivePropertyControllerImpl" />
    </component>

    <component name="SCACC">
        <service name="///sca-content-controller">
            <interface.java interface="org.ow2.frascati.tinfi.control.content.SCAExtendedContentController" />
        </service>
        <reference name="//component" target="Comp///component" >
            <interface.java interface="org.objectweb.fractal.api.Component" />
        </reference>
        <reference name="//lifecycle-controller" target="LC///lifecycle-controller" >
            <interface.java interface="org.objectweb.fractal.juliac.control.lifecycle.ExtendedLifeCycleCoordinator" />
        </reference>
        <reference name="//sca-component-controller" target="SCACompCtx///sca-component-controller" >
            <interface.java interface="org.oasisopen.sca.ComponentContext" />
        </reference>
        <implementation.java class="org.ow2.frascati.tinfi.control.content.SCAContentControllerImpl" />
    </component>

</composite>
