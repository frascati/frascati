/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.julia.conform.components;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.util.Fractal;

public abstract class Test {

  protected final static String COMP = "component/org.objectweb.fractal.api.Component/false,false,false";
  protected final static String BC = "binding-controller/org.objectweb.fractal.api.control.BindingController/false,false,false";
  protected final static String LC = "lifecycle-controller/org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator/false,false,false";
  protected final static String CC = "content-controller/org.objectweb.fractal.api.control.ContentController/false,false,false";
  protected final static String SC = "super-controller/org.objectweb.fractal.julia.control.content.SuperControllerNotifier/false,false,false";
  protected final static String NC = "name-controller/org.objectweb.fractal.api.control.NameController/false,false,false";
  protected final static String F = "factory/org.objectweb.fractal.julia.factory.Template/false,false,false";

  protected final static String PKG = "org.objectweb.fractal.julia.conform.components";

  protected void checkInterface (I i) {
    i.m(true);
    i.m((byte)1);
    i.m((char)1);
    i.m((short)1);
    i.m((int)1);
    i.m((long)1);
    i.m((float)1);
    i.m((double)1);
    i.m("1");
    i.m(new String[] { "1" });

    Assert.assertEquals(true, i.n(true, null));
    Assert.assertEquals((byte)1, i.n((byte)1, null));
    Assert.assertEquals((char)1, i.n((char)1, (double)0));
    Assert.assertEquals((short)1, i.n((short)1, (float)0));
    Assert.assertEquals((int)1, i.n((int)1, (long)0));
    Assert.assertEquals((long)1, i.n((long)1, (int)0));
    Assert.assertEquals((float)1, i.n((float)1, (short)0), 0);
    Assert.assertEquals((double)1, i.n((double)1, (char)0), 0);
    Assert.assertEquals("1", i.n("1", (byte)0));
  }

  protected void checkComponent (Component c, Set<String> itfs) {
    Set<String> extItfs = getExternalItfs(c);
    Assert.assertEquals("Wrong external interface list", itfs, extItfs);
    Iterator<String> i = itfs.iterator();
    while (i.hasNext()) {
      String itf = i.next();
      String compItf = null;
      try {
        compItf = getItf((Interface)c.getFcInterface(getItfName(itf)), false);
      } catch (NoSuchInterfaceException e) {
          Assert.fail("Missing external interface: " + itf);
      }
      Assert.assertEquals("Wrong external interface", itf, compItf);
    }

    ContentController cc;
    try {
      cc = Fractal.getContentController(c);
    } catch (NoSuchInterfaceException e) {
      return;
    }

    itfs = new HashSet<String>(itfs);
    i = itfs.iterator();
    while (i.hasNext()) {
      String itf = (String)i.next();
      if (itf.startsWith("component/") || itf.indexOf("-controller/") != -1) {
        i.remove();
      }
    }

    Set<String> intItfs = getInternalItfs(cc);
    Assert.assertEquals("Wrong internal interface list", itfs, intItfs);
    i = itfs.iterator();
    while (i.hasNext()) {
      String itf = i.next();
      String compItf = null;
      try {
        compItf = getItf((Interface)cc.getFcInternalInterface(getItfName(itf)), true);
      } catch (NoSuchInterfaceException e) {
          Assert.fail("Missing internal interface: " + itf);
      }
      Assert.assertEquals("Wrong internal interface", itf, compItf);
    }
  }

  protected Set<String> getExternalItfs (Component c) {
    Set<String> result = new HashSet<String>();
    Object[] extItfs = c.getFcInterfaces();
    for (int i = 0; i < extItfs.length; ++i) {
      String itf = getItf((Interface)extItfs[i], false);
      if (!result.add(itf)) {
          Assert.fail("Duplicated interface: " + itf);
      }
    }
    return result;
  }

  private Set<String> getInternalItfs (ContentController cc) {
    Set<String> result = new HashSet<String>();
    Object[] extItfs = cc.getFcInternalInterfaces();
    for (int i = 0; i < extItfs.length; ++i) {
      String itf = getItf((Interface)extItfs[i], true);
      if (!result.add(itf)) {
        Assert.fail("Duplicated interface: " + itf);
      }
    }
    return result;
  }

  protected static String getItf (Interface itf, boolean internal) {
    InterfaceType itfType = (InterfaceType)itf.getFcItfType();
    return getItf(
      itf.getFcItfName(),
      itfType.getFcItfSignature(),
      itfType.isFcClientItf() ^ internal,
      itfType.isFcOptionalItf(),
      itfType.isFcCollectionItf());
  }

  private static String getItf (
    String name,
    String signature,
    boolean isClient,
    boolean isOptional,
    boolean isCollection)
  {
    return name+'/'+signature+'/'+isClient+','+isOptional+','+isCollection;
  }

  private static String getItfName (String itf) {
    return itf.substring(0, itf.indexOf('/'));
  }
}
