/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.junit.Assert;
import org.junit.Test;
import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the use of the {@link Scope} annotation.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ScopeTestCase {

    /** The delay between two calls to getContent() to simulate concurrency. */
    final private static long DELAY = 100;
    
    @Test
    public void testScopeComposite() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");

        TestScopeCompositeThread t = new TestScopeCompositeThread(itf);
        t.start();
        Object content = itf.getContent(DELAY);
        t.join();
        Assert.assertSame(
            "Content instance should persist for composite-scoped components",
            content, t.content);
    }
    
    /**
     * @since 1.2.1
     */
    @Test
    public void testScopeCompositeEager() throws Exception {
        
        ScopeCompositeImpl.eagerinit = false;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        Component c = TinfiDomain.getComponent(adl);

        Fractal.getLifeCycleController(c).startFc();

        Assert.assertTrue(
            "Content instance should have been eagerly initialized",
            ScopeCompositeImpl.eagerinit);
    }
    
    @Test
    public void testScopeCompositeEagerInitInitAnnotated() throws Exception {
        
        ScopeImpl.initcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        TinfiDomain.getService(adl,ScopeItf.class,"s");

        Assert.assertEquals(
            "@Init annotated methods should be executed when the component is eagerly instantiated",
            1, ScopeImpl.initcounter);
    }
    
    /**
     * @since 1.1.1
     */
    @Test
    public void testScopeCompositeInit() throws Exception {
        
        ScopeImpl.initcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");
        
        itf.run();
        itf.run();

        Assert.assertEquals(
            "@Init annotated methods should be executed only once for scoped-annotated components",
            1, ScopeImpl.initcounter);
    }
    
    /**
     * @since 1.1.1
     */
    @Test
    public void testScopeCompositeDestroy() throws Exception {
        
        ScopeImpl.destroycounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");
        
        itf.run();
        itf.run();

        Assert.assertEquals(
            "@Destroy annotated methods should not be executed for scoped-annotated components",
            0, ScopeImpl.destroycounter);
    }
    
    /**
     * @since 1.3
     */
    @Test
    public void testScopeCompositeStartStop() throws Exception {
        
        ScopeImpl.startcounter = 0;
        ScopeImpl.stopcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        
        // getComponent(String) starts the component
        Component c = TinfiDomain.getComponent(adl);
        Assert.assertEquals(1,ScopeImpl.startcounter);
        Assert.assertEquals(0,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).stopFc();
        Assert.assertEquals(1,ScopeImpl.startcounter);
        Assert.assertEquals(1,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).startFc();
        Assert.assertEquals(2,ScopeImpl.startcounter);
        Assert.assertEquals(1,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).stopFc();
        Assert.assertEquals(2,ScopeImpl.startcounter);
        Assert.assertEquals(2,ScopeImpl.stopcounter);
    }
    
    private static class TestScopeCompositeThread extends Thread {
        private Object content;
        private ScopeItf itf;
        public TestScopeCompositeThread( ScopeItf itf ) {
            this.itf = itf;
        }
        @Override
        public void run() {
            content = itf.getContent(DELAY);
        }        
    }    
}
