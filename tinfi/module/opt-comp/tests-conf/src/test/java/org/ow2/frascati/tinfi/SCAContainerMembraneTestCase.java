/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the scaContainer membrane.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.3.1
 */
public class SCAContainerMembraneTestCase {

    @Test
    public void testRemoveSubComponent()
    throws
        IllegalLifeCycleException, NoSuchInterfaceException,
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        java.lang.InstantiationException, IllegalContentException {
        
        String adl = getClass().getPackage().getName()+".SCAContainerMembrane";
        Component root = TinfiDomain.getComponent(adl);
        ContentController cc = Fractal.getContentController(root);
        Component[] subs = cc.getFcSubComponents();
        Component client = subs[0];
        
        /*
         * Check that removeFcSubComponent does not throw
         * IllegalLifeCycleException.
         */
        cc.removeFcSubComponent(client);
    }    
}
