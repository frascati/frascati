/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi;

import java.util.List;

import org.oasisopen.sca.annotation.PolicySets;
import org.oasisopen.sca.annotation.Reference;

/**
 * Component implementation used for testing intent handlers.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 0.4
 */
@PolicySets(IntentBaseClientItf.COUNT)
public class IntentBaseClientImpl implements IntentBaseClientItf {

    @PolicySets(IntentBaseClientItf.COUNT)
    public void runAnnotatedMethod() {}
    
    public void runUnAnnotatedMethod() {}
    
    /**
     * @since 1.4
     */
    public void run() {
        for (Runnable client : clients) {
            client.run();
        }
    }
    
    /** @since 1.4 */
    @Reference
    public List<Runnable> clients;

    /**
     * @author Philippe Merle <Philippe.Merle@inria.fr>
     * @since 1.4.2
     */
    public void run( Object... args ) {}
}
