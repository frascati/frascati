/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.control.name.UseNameControllerTrait
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerTrait

import scala.collection.mutable.HashMap

/**
 * Trait implementing the {@link SCAPropertyController} control interface
 * functionalities for scaPrimitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCAPropertyPrimitiveTrait extends BasicControllerTrait
with org.ow2.frascati.tinfi.api.control.SCAPropertyControllerTrait
with UseSCAContentControllerTrait {

    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    override def setValue( name: String, value: Object ) = {
        // Re-inject the value on current content instances
        weaveableSCACC.setPropertyValue(name,value)
    }
    
    override def isDeclared( name: String ) : Boolean = {
        val b = weaveableSCACC.containsPropertyName(name)
        return b
    }
    
    override def containsPropertyName( name: String ) : Boolean = {
        val b = weaveableSCACC.containsPropertyName(name)
        return b
    }
    
    override def getPropertyNames : Array[String] = {
        val names = weaveableSCACC.getPropertyNames
        return names
    }    

    override def getType( name: String ) : Class[_] = {
        val typ = weaveableSCACC.getPropertyType(name)
        return typ
    }
}
