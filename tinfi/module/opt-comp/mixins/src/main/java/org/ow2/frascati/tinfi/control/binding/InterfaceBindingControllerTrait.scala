/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2013-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.Interface
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor
import org.ow2.frascati.tinfi.TinfiComponentOutInterface
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerTrait
import org.objectweb.fractal.julia.control.binding.AbstractBindingControllerTrait

/**
 * This trait manages the references hold by client interfaces in relation with
 * the methods of the {@link BindingController} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.6
 */
trait InterfaceBindingControllerTrait extends BasicControllerTrait
with UseComponentTrait with UseSCAContentControllerTrait
with AbstractBindingControllerTrait {

    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------

    override def bindFc( clientItfName: String, serverItf: Object ) = {
      
        super.bindFc( clientItfName, serverItf )

        val ci = weaveableC.getFcInterface(clientItfName).asInstanceOf[ComponentInterface]
        
        // Skip client interceptor
        val interceptor = ci.getFcItfImpl.asInstanceOf[Interceptor]
        interceptor.setFcItfDelegate(serverItf)
        
        // Inject reference into content instances
        val tci = ci.asInstanceOf[TinfiComponentOutInterface[_]]
        val sr = tci.getServiceReference
        weaveableSCACC.setReferenceValue(clientItfName,sr)
    }
    
    override def unbindFc( clientItfName: String ) = {
      
        super.unbindFc( clientItfName )

        val ci = weaveableC.getFcInterface(clientItfName).asInstanceOf[ComponentInterface]
        
        // Skip client interceptor
        val interceptor = ci.getFcItfImpl.asInstanceOf[Interceptor]
        interceptor.setFcItfDelegate(null)
        
        // Inject null into content instances
        weaveableSCACC.unsetReferenceValue(clientItfName)
    }
}
