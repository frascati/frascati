/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.lifecycle

import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.julia.control.content.Util
import org.objectweb.fractal.api.Component
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.api.NoSuchInterfaceException
import org.objectweb.fractal.api.`type`.ComponentType
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.julia.control.binding.ChainedIllegalBindingException
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinatorTrait
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerTrait

/**
 * Trait for managing the EagerInit, the Start and the Stop policies.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCALifeCycleTrait extends LifeCycleCoordinatorTrait
with UseSCAContentControllerTrait {

    // -------------------------------------------------------------------------
    // Implementation of the LifeCycleCoordinator interface
    // -------------------------------------------------------------------------
  
    override def setFcStarted: Boolean = synchronized {
        val b = super.setFcStarted
        if(b) {
            weaveableSCACC.eagerInit
            weaveableSCACC.start
        }
        return b
    }
    
    override def setFcStopped: Boolean = synchronized {
        val b = super.setFcStopped
        if(b) {
            weaveableSCACC.stop
        }
        return b
    }
}
