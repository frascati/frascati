/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control

import java.lang.annotation.Annotation
import java.lang.reflect.Method

import org.ow2.frascati.tinfi.api.IntentHandler
import org.ow2.frascati.tinfi.api.InterfaceFilter
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter

/**
 * Abstract implementation of the {@link SCAIntentController} interface to
 * enable calling super.aMethod in types which use this trait.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCAIntentControllerTrait extends SCAIntentController {
  
    // SCABasicIntentController
    def addFcIntentHandler( handler: IntentHandler ) = {}
    def addFcIntentHandler( handler: IntentHandler, filter: InterfaceFilter ) = {}
    def addFcIntentHandler( handler: IntentHandler, filter: InterfaceMethodFilter )  = {}
    def addFcIntentHandler( handler: IntentHandler, name: String ) = {}
    def addFcIntentHandler( handler: IntentHandler, name: String, method: Method ) = {}
    def listFcIntentHandler( name: String ) : java.util.List[IntentHandler] = { return null }
    def listFcIntentHandler( name: String, method: Method ) : java.util.List[IntentHandler] = { return null }
    def removeFcIntentHandler( handler: IntentHandler ) = {}
    def removeFcIntentHandler( handler: IntentHandler, name: String ) = {}
    def removeFcIntentHandler( handler: IntentHandler, name: String, method: Method ) = {}
    
    // SCAIntentController
    def addFcIntentHandler[T <: Annotation](
        handler: IntentHandler , annotcl: Class[T], value: String ) = {}
}
