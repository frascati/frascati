/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.control.name.UseNameControllerTrait
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException
import org.ow2.frascati.tinfi.api.control.SCAPropertyController
import scala.collection.mutable.HashMap
import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor

/**
 * Trait implementing the property promotion mechanism for the SCA property
 * controller. This mechanism is available with Tinfi since version 0.4.3, but
 * the code has been modularized in this layer only since version 1.4.2. This
 * layer has been refactored as a trait in version 1.4.5.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCAPropertyPromoterTrait extends BasicControllerTrait
with org.ow2.frascati.tinfi.api.control.SCAPropertyControllerTrait
with UseNameControllerTrait {

    /**
     * Property name indexed map of promoters.
     */
    private var promoters = new HashMap[String,SCAPropertyController]
    
    /**
     * Property name indexed map of property names defined at the level of their
     * promoter.
     */
    private var promoterPropNames = new HashMap[String,String]

    
    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    override def setType( name: String, typ: Class[_] ) = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            promoter.setType(promoterPropName,typ)
        }
        else {
            super.setType(name,typ)
        }
    }
    
    override def setValue( name: String, value: Object ) = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            promoter.setValue(promoterPropName,value)
        }
        else {
            super.setValue(name,value)
        }
    }
    
    override def getType( name: String ) : Class[_] = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            val typ = promoter.getType(promoterPropName)
            return typ
        }
        else {
            val typ = super.getType(name)
            return typ;
        }
    }
    
    override def getValue( name: String ) : Object = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            val value = promoter.getValue(promoterPropName)
            return value
        }
        else {
            val value = super.getValue(name)
            return value
        }
    }
    
    override def containsPropertyName( name: String ) : Boolean = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            val b = promoter.containsPropertyName(promoterPropName)
            return b
        }
        else {
            val b = super.containsPropertyName(name)
            return b
        }
    }

    override def hasBeenSet( name: String ) : Boolean = {
        if( promoters.contains(name) ) {
            val promoter = promoters(name)
            val promoterPropName = promoterPropNames(name)
            val b = promoter.hasBeenSet(promoterPropName)
            return b
        }
        else {
            val b = super.hasBeenSet(name)
            return b
        }
    }

    def setPromoter( name: String, promoter: SCAPropertyController ) = {
        promoters += name -> promoter
        promoterPropNames += name -> name
        if( promoter != null ) {
            val peer = promoter.getPromoter(name)
            if( peer == this ) {
                val compname = weaveableNC.getFcName()
                throw new IllegalPromoterException(compname)
            }
        }
        val peer = promoters(name)
        if( peer != null ) {
            /*
             * There is already a promoter. Check for simple cycles.
             * TODO Check for cycles of length > 2 ?
             */
            val peerInterceptor = peer.asInstanceOf[ComponentInterface].getFcItfImpl
            // TODO currently no interceptors on control interfaces in the COMP mode - to be reintroduced when this will be the case
//            val peerImpl = peerInterceptor.asInstanceOf[Interceptor].getFcItfDelegate
            val peerImpl = peerInterceptor
            if( peerImpl == this ) {
                val compname = weaveableNC.getFcName
                throw new IllegalPromoterException(compname)
            }
        }
    }
 
    def getPromoter( name: String ) : SCAPropertyController = {
        if( promoters.contains(name) ) {
            return promoters(name)
        }
        else {
            return null
        }
    }
    
    def setPromoter(
        name: String, promoter: SCAPropertyController,
        promoterPropertyName: String ) = {
      
        setPromoter(name,promoter)
        promoterPropNames += name -> promoterPropertyName
    }
    
    def getPromoterPropertyName( name: String ) : String = {
        return promoterPropNames(name)
    }
    
    def removePromoter( name: String ) = {
        promoters.remove(name)
        promoterPropNames.remove(name)
    }
}
