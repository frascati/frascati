/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor

import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor

/**
 * Trait implementing the functionalities of the {@link InterceptorController}
 * control interface.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait BasicInterceptorControllerTrait extends AbstractInterceptorControllerTrait
with InterceptorDefAttributes {
    
    // -----------------------------------------------------------------
    // Implementation of the InterceptorController interface
    // -----------------------------------------------------------------
    
    /**
     * Appends the specified interceptor at the end of the list of already
     * existing interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     */
    override def addFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) = {

        /*
         * Fetch the last interceptor in the list of interceptors
         * already associated to the specified interface.
         */
        var last: Object = null
        var next = itf.getFcItfImpl
        while( next.isInstanceOf[Interceptor] ) {
            val i = next.asInstanceOf[Interceptor]
            last = next
            next = i.getFcItfDelegate
        }
          
        if( last == null ) {
            // The list of interceptors is empty
            val content = itf.getFcItfImpl
            interceptor.setFcItfDelegate(content)
            itf.setFcItfImpl(interceptor)
        }
        else {
            val i = last.asInstanceOf[Interceptor]
            val content = i.getFcItfDelegate
            interceptor.setFcItfDelegate(content)
            i.setFcItfDelegate(interceptor)
        }
          
        itf.updateFcState
    }

    /**
     * Returns the array of interceptors associated to the specified Fractal
     * interface.
     * 
     * @param itf  the Fractal interface
     * @return     the array of interceptors
     */
    override def getFcInterceptors( itf: ComponentInterface ) : Array[Interceptor] = {
      
        var next = itf.getFcItfImpl
        var size : Integer = 0
        while( next.isInstanceOf[Interceptor] ) {
            val i = next.asInstanceOf[Interceptor]
            next = i.getFcItfDelegate
            size += 1
        }
        
        val result = new Array[Interceptor](size)
        next = itf.getFcItfImpl
        size = 0
        while( next.isInstanceOf[Interceptor] ) {
            val i = next.asInstanceOf[Interceptor]
            result(size) = i
            next = i.getFcItfDelegate
            size += 1
        }
        
        return result
    }
    
    /**
     * Removes the specified interceptor from the list of already existing
     * interceptors for the specified Fractal interface.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     * @return  <code>true</code> if the list contained the specified
     *          interceptor
     */
    override def removeFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) : Boolean = {
      
        var previous : Object = null
        var next = itf.getFcItfImpl
        while( next.isInstanceOf[Interceptor] ) {
            val i = next.asInstanceOf[Interceptor]
            if( next == interceptor ) {
                // The interceptor has been found
                val nextnext = i.getFcItfDelegate
                if( previous == null ) {
                    // The interceptor is the first one in the list
                    itf.setFcItfImpl(nextnext)
                }
                else {
                    val p = previous.asInstanceOf[Interceptor]
                    p.setFcItfDelegate(nextnext)
                }
                itf.updateFcState
                return true
            }
            previous = next
            next = i.getFcItfDelegate
        }
        
        // The interceptor has not been found
        return false
    }

    
    // -----------------------------------------------------------------
    // Implementation of the InterceptorDefAttributes interface
    // -----------------------------------------------------------------
    
    /**
     * @param interceptors
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    override def setInterceptors( interceptors: String ) = {
        this.interceptors = interceptors
    }
    
    /**
     * @return
     *      A stringified representation of a Julia
     *      {@link org.objectweb.fractal.julia.loader.Tree} structure describing
     *      the interceptor class generators associated with this control
     *      membrane.
     */
    override def getInterceptors : String = {
        return interceptors
    }

    private var interceptors : String = "" 
}
