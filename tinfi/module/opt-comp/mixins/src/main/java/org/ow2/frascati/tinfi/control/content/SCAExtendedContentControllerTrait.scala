/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content

import org.oasisopen.sca.RequestContext
import org.oasisopen.sca.ServiceReference

/**
 * Abstract implementation of the {@link SCAExtendedController} interface to
 * enable calling super.aMethod in types which use this trait.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCAExtendedContentControllerTrait extends SCAExtendedContentController {

    // SCAContentController
    def setFcContentClass( c: Class[_] ) = {}
    def getFcContentClass : Class[_] = return null
    def getFcContent : Object = return null
    def setFcContent( content: Object ) = {}

    // SCA ExtendedContentController
    def releaseFcContent( content: Object, isEndMethod: Boolean ) = {}
    def setRequestContext( rc: RequestContext ) = {}
    def getRequestContext : RequestContext = return null
    def eagerInit = {}
    def start = {}
    def stop = {}
    def containsPropertyName( name: String ) : Boolean = return false
    def getPropertyNames : Array[String] = return null
    def getPropertyType( name: String ) : Class[_] = return null
    def setPropertyValue( name: String, value: Object ) = {}
    def setReferenceValue( name: String, value: ServiceReference[_] ) = {}
    def unsetReferenceValue( name: String ) = {}
}
