/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2012-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content

import org.oasisopen.sca.RequestContext
import org.objectweb.fractal.julia.UseComponentTrait
import org.objectweb.fractal.api.Type
import org.objectweb.fractal.api.`type`.ComponentType
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException

/**
 * Request context management.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait TypeCheckSCAContentTrait extends SCABasicContentControllerTrait
with UseComponentTrait {

    // -------------------------------------------------------------------------
    // Implementation of the SCAContentController interface
    // -------------------------------------------------------------------------
    
    override def setFcContentClass( c: Class[_] ) = {
        val typ = weaveableC.getFcType
        checkType(typ,c)
        super.setFcContentClass(c)
    }
    
    override def setFcContent( content: Object ) = {
        val typ = weaveableC.getFcType
        val c = content.getClass
        checkType(typ,c)
        super.setFcContent(content)
    }

    // -------------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------------
    
    private def checkType( typ: Type, contentClass: Class[_] ) = {
      
        /*
         * Check that the content class implements the server interfaces.
         */
        val ct = typ.asInstanceOf[ComponentType]
        val its = ct.getFcInterfaceTypes
        its.foreach( it => {
            
            var continue = false
          
            // Skip client interfaces
            if( it.isFcClientItf ) {
                continue = true
            }
            
            // Skip control interfaces
            val name = it.getFcItfName
            if( name.equals("component") || name.endsWith("-controller") ) {
                continue = true
            }
            
            if( !continue ) {
              
                /*
                 * Workaround since Object.class.getClassLoader() returns null.
                 * Object.class as a content class is not a priori a useful case,
                 * but it uses it in test cases, and anyway the workaround is needed
                 * if Object.class is used elsewhere.
                 */
                val signature = it.getFcItfSignature
                if( contentClass.equals(classOf[Object]) ) {
                    val msg =
                        "Object.class is not adequate since the content class "+
                        "should implement "+signature;
                    throw new ContentInstantiationException(msg)
                }
                
                // Check that the class implements the server interface
                val cl = load(contentClass,signature)
                if( ! cl.isAssignableFrom(contentClass) ) {
                    val msg = contentClass+" should implement "+signature
                    throw new ContentInstantiationException(msg)
                }
            }
        })
    }
    
    /**
     * Load the class whose name is specified with the class loader of the
     * specified class.
     * 
     * @param main  the class used to retrieve the class loader
     * @param name  the name of the class to load
     * @throws ContentInstantiationException  if the class cannot be loader
     */
    private def load( main: Class[_], name: String ) : Class[_] = {
        val loader = main.getClassLoader
        try {
            return loader.loadClass(name);
        } catch {
              case cnfe : ClassNotFoundException =>
                    throw new ContentInstantiationException(cnfe)
        }
    }
}
