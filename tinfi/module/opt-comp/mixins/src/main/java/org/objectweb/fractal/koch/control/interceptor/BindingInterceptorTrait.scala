/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.objectweb.fractal.koch.control.interceptor

import org.objectweb.fractal.julia.ComponentInterface
import org.objectweb.fractal.julia.Interceptor
import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.julia.InitializationContext
import org.objectweb.fractal.api.`type`.InterfaceType
import org.objectweb.fractal.api.control.BindingController

/**
 * Trait implementing the functionalities of the {@link InterceptorController}
 * control interface.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait BindingInterceptorTrait extends BasicControllerTrait
with AbstractInterceptorControllerTrait {
  
    private var fcContent : Object = null
    
    override def initFcController( ic: InitializationContext ) = {
      fcContent = ic.content
      super.initFcController(ic)
    }

    /**
     * Re-inject the dependency towards the target of a binding when an
     * interceptor is added.
     *
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     */
    override def addFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) = {
      
        super.addFcInterceptor(itf,interceptor)
        
        /*
         * For client interfaces owned by a component with a content (e.g.
         * primitive ones), re-inject the dependency towards the target of a
         * binding.
         */
        val it = itf.getFcItfType.asInstanceOf[InterfaceType]
        if( it.isFcClientItf && fcContent != null ) {
          
            val clientItfName = itf.getFcItfName
            val bc = fcContent.asInstanceOf[BindingController]
            val target = bc.lookupFc(clientItfName)
            
            /*
             * When an interceptor has already been added, the target references
             * this interceptor. The current interceptor is added at the end
             * of the chain of interceptors. In this case, there is no need to
             * modify the binding.
             */
            if( target!=null && !(target.isInstanceOf[Interceptor]) ) {
                bc.bindFc(clientItfName, interceptor)
            }
        }
    }

    /**
     * Re-inject the dependency towards the target of a binding when an
     * interceptor is removed.
     * 
     * @param itf          the Fractal interface
     * @param interceptor  the interceptor
     * @return             true if the list contained the specified interceptor
     */
    override def removeFcInterceptor( itf: ComponentInterface, interceptor: Interceptor ) : Boolean = {
      
        val ret = super.removeFcInterceptor(itf,interceptor)
        
        /*
         * For client interfaces owned by a component with a content (e.g.
         * primitive ones), re-inject the dependency towards the target of a
         * binding if the interceptor has effectively been removed.
         */
        val it = itf.getFcItfType.asInstanceOf[InterfaceType]
        if( ret && it.isFcClientItf && fcContent != null ) {
            val clientItfName = itf.getFcItfName
            val bc = fcContent.asInstanceOf[BindingController]
            val delegate = interceptor.getFcItfDelegate
            bc.bindFc(clientItfName,delegate)
        }
        
        return ret
    }
}
