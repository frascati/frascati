/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding

import org.objectweb.fractal.julia.BasicControllerTrait
import org.objectweb.fractal.api.control.BindingController
import org.objectweb.fractal.julia.control.binding.CheckBindingTrait
import org.objectweb.fractal.julia.control.binding.TypeBindingTrait
import org.objectweb.fractal.julia.control.binding.ContentBindingTrait
import org.objectweb.fractal.julia.control.binding.LifeCycleBindingTrait
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerTrait
import org.objectweb.fractal.julia.control.binding.TypeBasicBindingTrait

/**
 * Trait implementing the {@link BindingController} control interface for SCA
 * primitive components.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
trait SCABindingControllerTrait extends BasicControllerTrait
with BasicBindingControllerTrait
with TypeBasicBindingTrait
with CollectionInterfaceBindingControllerTrait
with InterfaceBindingControllerTrait
with CheckBindingTrait
with TypeBindingTrait
with ContentBindingTrait
with LifeCycleBindingTrait
