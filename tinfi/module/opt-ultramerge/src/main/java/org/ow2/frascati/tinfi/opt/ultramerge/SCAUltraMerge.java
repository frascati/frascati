/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.ultramerge;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.JuliacRuntimeException;
import org.objectweb.fractal.juliac.desc.AttributeDesc;
import org.objectweb.fractal.juliac.desc.ComponentDesc;
import org.objectweb.fractal.juliac.opt.ultramerge.AbstractUltraMerge;
import org.objectweb.fractal.juliac.opt.ultramerge.IllegalComponentCodeException;
import org.objectweb.fractal.juliac.opt.ultramerge.UMClass;
import org.objectweb.fractal.juliac.opt.ultramerge.UMField;
import org.objectweb.fractal.juliac.opt.ultramerge.UMMethod;
import org.objectweb.fractal.juliac.spoon.helper.CtNamedElementHelper;
import org.ow2.frascati.tinfi.TinfiRuntimeException;

import spoon.reflect.code.CtAssignment;
import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtExpression;
import spoon.reflect.code.CtFieldAccess;
import spoon.reflect.code.CtStatement;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.factory.AnnotationFactory;
import spoon.reflect.reference.CtFieldReference;
import spoon.reflect.reference.CtTypeReference;

/**
 * Source code generator which merges all component business code in a single
 * class. This source code generator handles component business code annotated
 * with the OSOA SCA API. 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.3.1
 */
public class SCAUltraMerge extends AbstractUltraMerge {

    public SCAUltraMerge( Juliac jc ) {
        super(jc);
    }

    // ----------------------------------------------------------------------
    // Abstract methods
    // ----------------------------------------------------------------------
    
    protected String getRequiredAnnotatedUMFieldName( UMField<?> umfield ) {
        
        CtField<?> ctfield = umfield.getCtField();
        Reference r = ctfield.getAnnotation(Reference.class);
        
        if( r == null ) {
            /*
             * Not a @Reference annotated field.
             * Assume there is a @Reference annotated setter method.
             * Retrieve the corresponding reference name.
             */
            String refName = refNames.get(umfield);
            if( refName == null ) {
                // Defensive programming. Shouldn't occur.
                final String msg =
                    "Unexpected case. No reference name for field: "+
                    CtNamedElementHelper.toEclipseClickableString(ctfield);
                throw new TinfiRuntimeException(msg);
            }
            
            return refName;
        }

        String name = r.name();
        if( name.length() == 0 ) {
            /*
             * No name parameter for the @Requires annotation.
             * Return the field name.
             */
            name = umfield.getSimpleName();
        }

        return name;
    }
    
    protected boolean isRequiredAnnotatedUMField( UMField<?> umfield ) {
        
        CtField<?> ctfield = umfield.getCtField();
        Reference r = ctfield.getAnnotation(Reference.class);
        if( r != null ) {
            // @Reference annotated field
            return true;
        }
        
        // @Reference annotated setter method
        boolean b = refNames.containsKey(umfield);
        return b;
    }

    protected boolean
        isRequiredAnnotatedUMFieldCardinalitySingleton( UMField<?> umfield ) {

        CtField<?> ctfield = umfield.getCtField();
        CtTypeReference<?> tref = ctfield.getType();
        String qname = tref.getQualifiedName();
        Class<?> umclass = jc.loadClass(qname);

        /*
         * Fields of type List hold references for collection references.
         */
        boolean b = !( List.class.isAssignableFrom(umclass) );
        return b;
    }

    protected boolean
        isRequiredAnnotatedUMFieldCardinalityCollection( UMField<?> umfield ) {
        
        CtField<?> ctfield = umfield.getCtField();
        CtTypeReference<?> tref = ctfield.getType();
        String qname = tref.getQualifiedName();
        Class<?> umclass = jc.loadClass(qname);
        
        /*
         * Fields of type List hold references for collection references.
         */
        boolean b = List.class.isAssignableFrom(umclass);
        return b;
    }
    
    protected void annotateRequiredCtField(
        AnnotationFactory af, CtField<?> field, String cltItfName ) {
        af.annotate(field,Reference.class,"name",cltItfName);        
    }
    
    protected boolean isRequiredAnnotation( Annotation a ) {
        boolean b = a instanceof Reference;
        return b;
    }

    /**
     * Return the setter method for the specified attribute name.
     */
    protected UMMethod<?> getAttributeSetterUMMethod(
        UMClass<?> umclass, AttributeDesc adesc ) {
        
        String aname = adesc.getName();
        
        List<UMMethod<?>> ummethods = umclass.getUMMethods();
        for (UMMethod<?> ummethod : ummethods) {
            CtMethod<?> ctmethod = ummethod.getCtMethod();
            Property p = ctmethod.getAnnotation(Property.class);
            if( p != null ) {
                String name = p.name();
                if( aname.equals(name) ) {
                    return ummethod;
                }
            }
        }
        
        return null;
    }
    
    protected boolean isAttributeAnnotatedCtField( CtField<?> ctfield ) {
        Property p = ctfield.getAnnotation(Property.class);
        boolean b = p != null;
        return b;
    }

    protected String getAttributeAnnotatedCtFieldName( CtField<?> ctfield ) {
        Property p = ctfield.getAnnotation(Property.class);
        String name = p.name();
        return name;
    }

    protected CtTypeReference<?> getCollectionFieldType( UMField<?> umfield ) {
        
        CtField<?> ctfield = umfield.getCtField();
        CtTypeReference<?> fieldType = ctfield.getType();
        List<CtTypeReference<?>> genericTypes =
            fieldType.getActualTypeArguments();
        if( genericTypes==null || genericTypes.size()!=1 ) {
            final String msg =
                CtNamedElementHelper.toEclipseClickableString(ctfield)+
                " expected to be of type List<K>";
            throw new JuliacRuntimeException(msg);
        }
        CtTypeReference<?> refType = genericTypes.get(0);
        
        return refType;
    }
        
    /**
     * Return the class for variables which store references for collection
     * interfaces.
     */
    protected Class<?> getCollectionClass() {
        return ArrayList.class;
    }

    
    // ----------------------------------------------------------------------
    // Mergeability check
    // ----------------------------------------------------------------------
    
    @Override
    protected void mergeabilityCheck( ComponentDesc cdesc )
    throws IllegalComponentCodeException {
        
        super.mergeabilityCheck(cdesc);
        
        Collection<UMClass<?>> umclasses = repo.getAllUMClasses();
        for (UMClass<?> umclass : umclasses) {
            mergeabilityCheckComponentScopes(umclass);
            mergeabilityCheckSetterMethod(umclass);
        }
    }
    
    /**
     * Only STATELESS and COMPOSITE scoped-components are supported.
     */
    private void mergeabilityCheckComponentScopes( UMClass<?> umclass )
    throws IllegalComponentCodeException {
        
        CtClass<?> ctclass = umclass.getCtClass();
        String umclassname = ctclass.getQualifiedName();
        Class<?> cl = jc.loadClass(umclassname);
        Scope scope = cl.getAnnotation(Scope.class);
        
        if ( scope != null ) {
            /*
             * scope == null if there is no @Scope annotation.
             * Assume no @Scope == @Scope("STATELESS")
             */
            String value = scope.value();
            if( !value.equals("STATELESS") && !value.equals("COMPOSITE") ) {
                final String msg =
                    "Unsupported scope "+value+" for component class: "+
                    umclassname;
                throw new IllegalComponentCodeException(msg);
            }
        }
    }
    
    /**
     * Check that setter methods contain only one statement of the form
     * <code>this.field = value;</code>. Else retrieving the field holding the
     * reference to the bound component would be too difficult.
     */
    private void mergeabilityCheckSetterMethod( UMClass<?> umclass )
    throws IllegalComponentCodeException {
        
        Map<UMField<?>,String> refNameByUMClass =
            new HashMap<UMField<?>,String>();
        CtClass<?> ctclass = umclass.getCtClass();
        String umclassname = ctclass.getQualifiedName();
        refNamesByUMClass.put(umclassname,refNameByUMClass);
        
        /*
         * Retrieve @Reference annotated methods.
         */
        List<UMMethod<?>> methods = umclass.getUMMethods();
        for (UMMethod<?> method : methods) {
            
            CtMethod<?> ctmethod = method.getCtMethod();
            Reference reference = ctmethod.getAnnotation(Reference.class);
            if( reference == null ) {
                // Not a @Reference annotated method
                continue;
            }

            String refName = reference.name();
            if( refName.length() == 0 ) {
                /*
                 * No name parameter in the annotation.
                 * Retrieve the setter name e.g. name from setName.
                 */
                String methName = method.getSimpleName();
                if( methName.length()<4 || !methName.startsWith("set") ) {
                    final String msg =
                        "Illegal name ("+methName+
                        ") for @Reference annotated setter method: "+
                        CtNamedElementHelper.toEclipseClickableString(ctmethod);
                    throw new IllegalComponentCodeException(msg);
                }
                refName = methName.substring(3,4).toLowerCase();
                if( methName.length() > 4 ) {
                    refName += methName.substring(4);
                }
            }
            
            if( ! isRequiredInterfaceBound(refName,umclass)) {
                // Not a bound reference. Skip it.
                continue;
            }
            
            /*
             * Check that the body of the method is of the form
             *      this.field = value;
             */
            CtBlock<?> body = ctmethod.getBody();  // CtBlock instead of CtBlock<?> for compiling with javac
            List<CtStatement> stats = body.getStatements();
            if( stats.size() != 1 ) {
                final String msg =
                    "Setter method body does not contain one and only one "+
                    "statement: "+
                    CtNamedElementHelper.toEclipseClickableString(ctmethod);
                throw new IllegalComponentCodeException(msg);
            }
            CtStatement stat = stats.get(0);
            
            if( ! (stat instanceof CtAssignment<?,?>) ) {
                final String msg =
                    "Illegal setter method body. Not an assignment. "+stat;
                throw new IllegalComponentCodeException(msg);
            }
            CtAssignment<?,?> assign = (CtAssignment<?,?>) stat;
            CtExpression<?> assigned = assign.getAssigned();
            
            if( ! (assigned instanceof CtFieldAccess<?>) ) {
                if( ! (stat instanceof CtAssignment<?,?>) ) {
                    final String msg =
                        "Illegal setter method body. Not a field access. "+stat;
                    throw new IllegalComponentCodeException(msg);
                }
            }
            CtFieldAccess<?> fa = (CtFieldAccess<?>) assigned;
            CtFieldReference<?> fref = fa.getVariable();
            CtTypeReference<?> tref = fref.getDeclaringType();
            String qname = tref.getQualifiedName();
            
            if( ! qname.equals(umclassname) ) {
                final String msg =
                    "Illegal setter method body. Field from class "+qname+
                    " referenced instead of a field from the current class "+
                    umclassname+". "+stat;
                throw new IllegalComponentCodeException(msg);
            }
            String fname = fref.getSimpleName();
            UMField<?> field = umclass.getUMField(fname);
            
            /*
             * Register the field set by the @Reference annotated setter method.
             */
            refNames.put(field,refName);
            refNameByUMClass.put(field,refName);
            setters.put(umclass,method);
            setfields.put(umclass,field);
        }
    }
    
    /**
     * The names of the references managed by a setter method. The index
     * corresponds to the field where the reference is injected.
     */
    private Map<UMField<?>,String> refNames = new HashMap<UMField<?>,String>();
    
    /**
     * Same data as {@link #refNames} indexed by class names. Indexing by {@link
     * UMClass} instead of class names does not seem to work since it appears
     * that {@link UMClass} instances may be created several times for the same
     * class.
     */
    private Map<String,Map<UMField<?>,String>> refNamesByUMClass =
        new HashMap<String,Map<UMField<?>,String>>();
    
    /**
     * @{@link {@link Reference} annotated setter methods.
     */
    private Map<UMClass<?>,UMMethod<?>> setters =
        new HashMap<UMClass<?>,UMMethod<?>>();
    
    /**
     * Fields set by @{@link {@link Reference} annotated setter methods.
     */
    private Map<UMClass<?>,UMField<?>> setfields =
        new HashMap<UMClass<?>,UMField<?>>();


    // ----------------------------------------------------------------------
    // Removals
    // ----------------------------------------------------------------------
    
    /**
     * Remove code elements unrelevant for the merged code.
     * 
     * @since 1.1.1
     */
    @Override
    protected void removeUnrelevantCodeElements() {
        
        super.removeUnrelevantCodeElements();

        /*
         * Between the building of setters and setfields, the UMClasses
         * repository has been rebuilt and elements (UMClass, UMField, UMMethod)
         * have been recreated. We can not simply remove them for their
         * respective lists.
         */
        
        for (Map.Entry<UMClass<?>,UMMethod<?>> entry : setters.entrySet()) {
            
            UMClass<?> umclass = entry.getKey();
            CtClass<?> ctclass = umclass.getCtClass();
            String umclassname = ctclass.getQualifiedName();

            UMClass<?> cl = repo.get(umclassname);
            List<UMMethod<?>> methods = cl.getUMMethods();
            // Work on a copy to avoid ConcurrentModificationException
            List<UMMethod<?>> copy = new ArrayList<UMMethod<?>>(methods);
            
            UMMethod<?> ummethod = entry.getValue();
            CtMethod<?> ctmethod = ummethod.getCtMethod();
            String ctmethodsig = ctmethod.getSignature();
            
            for (UMMethod<?> method : copy) {
                CtMethod<?> meth = method.getCtMethod();
                String sig = meth.getSignature();
                if( sig.equals(ctmethodsig) ) {
                    methods.remove(method);
                }
            }
        }

        for (Map.Entry<UMClass<?>,UMField<?>> entry : setfields.entrySet()) {
            
            UMClass<?> umclass = entry.getKey();
            CtClass<?> ctclass = umclass.getCtClass();
            String umclassname = ctclass.getQualifiedName();
            
            UMField<?> umfield = entry.getValue();
            String umfieldname = umfield.getSimpleName();
            
            UMClass<?> cl = repo.get(umclassname);
            List<UMField<?>> fields = cl.getUMFields();
            UMField<?> field = cl.getUMField(umfieldname);
            fields.remove(field);
        }
    }
    
    // ----------------------------------------------------------------------
    // Utility methods
    // ----------------------------------------------------------------------
    
    @Override
    protected List<UMField<?>> getBoundReferenceFields( UMClass<?> umclass ) {
        
        // Retrieve @Reference annotated fields
        List<UMField<?>> fields = super.getBoundReferenceFields(umclass);
        
        // Retrieve fields with are set by @Reference annotated setter methods
        CtClass<?> ctclass = umclass.getCtClass();
        String umclassname = ctclass.getQualifiedName();
        Map<UMField<?>,String> refNameByUMClass =
            refNamesByUMClass.get(umclassname);
        if( refNameByUMClass == null ) {
            final String msg =
                "Unexpected exception. No reference names for class: "+umclass;
            throw new TinfiRuntimeException(msg);
        }
        Set<UMField<?>> fs = refNameByUMClass.keySet();
        fields.addAll(fs);
        
        return fields;
    }
}
