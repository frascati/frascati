/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.ultramerge;

import java.io.IOException;

import org.objectweb.fractal.juliac.desc.ComponentDesc;
import org.objectweb.fractal.juliac.opt.ultramerge.AbstractUltraMerge;
import org.ow2.frascati.tinfi.emf.EMFParserSupportImpl;

/**
 * Optimization level source code generator which merges all component business
 * code in a single class. This source code generator handles component business
 * code annotated with the OSOA SCA API. 
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.1
 */
public class FCUltraMergeSourceCodeGenerator extends EMFParserSupportImpl {

    // -----------------------------------------------------------------------
    // Implementation of the FractalADLSupportItf interface
    // -----------------------------------------------------------------------

    /**
     * Generate the source code of a factory class for the specified ADL.
     * 
     * @param adl         the fully-qualified name of the ADL descriptor
     * @param targetname  the fully-qualified name of the factory class
     */
    @Override
    public void generate( String adl, String targetname )
    throws IOException {
        
        ComponentDesc cdesc = parse(adl,null);
        AbstractUltraMerge aum = new SCAUltraMerge(jc);
        aum.generate(cdesc,targetname);
    }
}
