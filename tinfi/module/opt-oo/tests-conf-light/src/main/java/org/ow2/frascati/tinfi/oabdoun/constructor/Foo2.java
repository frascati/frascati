package org.ow2.frascati.tinfi.oabdoun.constructor;

import org.oasisopen.sca.annotation.Constructor;
import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public class Foo2 implements Bar {

    protected String bar;
    
    @Constructor
    public Foo2(@Property(name="bar") String b) {
        this.bar = b;
    }
    
    public String getBar() {
        return this.bar;
    }

}
