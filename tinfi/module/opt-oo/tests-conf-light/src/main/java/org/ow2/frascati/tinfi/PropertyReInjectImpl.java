/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Scope;

/**
 * Component implementation used for testing property re-injection.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.2.1
 */
@Scope("COMPOSITE")
public class PropertyReInjectImpl implements PropertyReInjectItf {
    
    /*
     * Initially this component implementation was conversation scoped in order
     * to be able to retreive easily different instances (each one corresponding
     * to a different conversation), to test property re-injection on several
     * content instances. Since conversation scoped components are not yet
     * supported by the light mode, we use a composite scoped implementation.
     * This leads to a slight loss of safety since we do not test for a bias
     * which could consist for example in updating the property in only one
     * instance, and not in all current ones.
     */
    
    public String getValue() {
        return prop;
    }
    
    @Property
    public void setProp( String value ) {
        this.prop = value;
    }
    private String prop;
    
    public void end() {
        // Indeed nothing. Invoked to end the conversation.
    }
}
