/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.mix;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;

/**
 * Component implementation used for testing assemblies which mix scaPrimitive
 * and primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
@Scope("COMPOSITE")
public class MixSCAPrimitiveImpl implements Runnable {
    
    @Reference(name="c",required=false)
    public Runnable c;
    
    /**
     * If the reference to the primitive component is not null, delegates.
     * Else throws a {@link RuntimeException}.
     */
    public void run() throws RuntimeException {
        if( c == null ) {
            String msg = "Reference c shouldn't be null";
            throw new RuntimeException(msg);
        }
        c.run();
    }    
}
