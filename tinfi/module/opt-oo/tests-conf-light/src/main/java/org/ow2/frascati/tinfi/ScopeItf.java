/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

/**
 * Interface implemented by components for testing scope policies.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public interface ScopeItf extends Runnable {
    
    /**
     * Return the content instance implementing the component. Sleep for a
     * given amount of time before returning to simulate concurrent calls.
     * 
     * @param millis
     *          the time duration in milliseconds before returning the call
     * @return  the instance implementing the component
     */
    public Object getContent( long millis );
    
    /**
     * Invoke the {@link #getContent(long)} operation on itself.
     * 
     * @return
     *      an array of two elements with the instance implementing the
     *      component as the first element, and the value returned by {@link
     *      #getContent(long)}=0 as the second element.
     */
    public Object[] loop();
}
