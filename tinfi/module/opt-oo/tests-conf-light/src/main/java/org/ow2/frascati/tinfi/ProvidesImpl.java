/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.ow2.frascati.tinfi.annotations.Provides;
import org.ow2.frascati.tinfi.opt.oo.SCAPrimitive;

/**
 * Component implementation used for testing
 * {@link org.ow2.frascati.tinfi.annotations.Provides} annotated elements.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.2
 */
@Membrane(controllerDesc=SCAPrimitive.class)
@Service(names="r",value=ProvidesItf.class)
@Scope("COMPOSITE")
public class ProvidesImpl implements ProvidesItf {

    final static public String NAME = "Initiated by a factory method";
    
    @Provides
    public ProvidesImpl getInstance() {
        ProvidesImpl object = new ProvidesImpl();
        object.name = NAME;
        return object;
    }
    
    public String name() {
        return name;
    }
    private String name;
}
