/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.mix;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.factory.GenericFactory;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.api.type.TypeFactory;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.TinfiDomain;

/**
 * Class for testing the use of assemblies which mix scaPrimitive and primitive
 * components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class MixTestCase {

    @Test
    public void testMix() throws Exception {

        /*
         * Retrieve the scaPrimitive component from the assembly.
         */
        String adl = getClass().getPackage().getName()+".Mix";
        Component system = TinfiDomain.getComponent(adl);
        ContentController cc = Fractal.getContentController(system);
        Component[] subs = cc.getFcSubComponents();
        Component scaPrimitive = subs[0];

        /*
         * Instanciate the primitive component.
         */
        Component boot = Fractal.getBootstrapComponent();
        GenericFactory gf = Fractal.getGenericFactory(boot);
        TypeFactory tf = Fractal.getTypeFactory(boot);
        
        ComponentType ct =
            tf.createFcType(
                new InterfaceType[]{
                    tf.createFcItfType(
                        "c",Runnable.class.getName(),false,false,false)
                });
        Component primitive =
            gf.newFcInstance(ct,"primitive",MixPrimitiveImpl.class.getName());
        Runnable primitiveR = (Runnable) primitive.getFcInterface("c");
        
        /*
         * Insert the primitive component in the system composite.
         */
        cc.addFcSubComponent(primitive);
        
        /*
         * Bind the primitive component to the scaPrimitive one.
         */
        BindingController bc = Fractal.getBindingController(scaPrimitive);
        bc.bindFc("c",primitiveR);
        
        /*
         * Start the system.
         */
        Fractal.getLifeCycleController(system).startFc();
        
        /*
         * Invoke the system.
         */
        Runnable r = (Runnable) system.getFcInterface("r");
        r.run();
    }
}
