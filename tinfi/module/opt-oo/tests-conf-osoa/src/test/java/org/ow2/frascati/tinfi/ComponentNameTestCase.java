/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.junit.Assert;
import org.junit.Test;
import org.osoa.sca.annotations.ComponentName;

/**
 * Class for testing {@link ComponentName} annotated elements.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ComponentNameTestCase {

    @Test
    public void testComponentName() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ComponentName";
        String service = "r";
        ComponentNameItf itf = TinfiDomain.getService(adl,ComponentNameItf.class,service);

        String name = itf.name();
        Assert.assertEquals("Wrong component name","server",name);
    }
}
