/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.ConversationEndedException;
import org.osoa.sca.annotations.Scope;

/**
 * Class for testing the use of the {@link Scope} annotation.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ScopeTestCase {

    /** The delay between two calls to getContent() to simulate concurrency. */
    final private static long DELAY = 100;
    
    @Test
    public void testScopeComposite() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");

        TestScopeRequestThread t = new TestScopeRequestThread(itf);
        t.start();
        Object content = itf.getContent(DELAY);
        t.join();
        Assert.assertSame(
            "Content instance should persist for composite-scoped components",
            content, t.content);
    }
    
    /**
     * @since 1.2.1
     */
    @Test
    public void testScopeCompositeEager() throws Exception {
        
        ScopeCompositeImpl.eagerinit = false;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        Component c = TinfiDomain.getComponent(adl);

        Fractal.getLifeCycleController(c).startFc();

        Assert.assertTrue(
            "Content instance should have been eagerly initialized",
            ScopeCompositeImpl.eagerinit);
    }
    
    @Test
    public void testScopeCompositeEagerInitInitAnnotated() throws Exception {
        
        ScopeImpl.initcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        TinfiDomain.getService(adl,ScopeItf.class,"s");

        Assert.assertEquals(
            "@Init annotated methods should be executed when the component is eagerly instantiated",
            1, ScopeImpl.initcounter);
    }
    
    /**
     * @since 1.1.1
     */
    @Test
    public void testScopeCompositeInit() throws Exception {
        
        ScopeImpl.initcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");
        
        itf.run();
        itf.run();

        Assert.assertEquals(
            "@Init annotated methods should be executed only once for scoped-annotated components",
            1, ScopeImpl.initcounter);
    }
    
    /**
     * @since 1.1.1
     */
    @Test
    public void testScopeCompositeDestroy() throws Exception {
        
        ScopeImpl.destroycounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,"s");
        
        itf.run();
        itf.run();

        Assert.assertEquals(
            "@Destroy annotated methods should not be executed for scoped-annotated components",
            0, ScopeImpl.destroycounter);
    }
    
    /**
     * @since 1.3
     */
    @Test
    public void testScopeCompositeStartStop() throws Exception {
        
        ScopeImpl.startcounter = 0;
        ScopeImpl.stopcounter = 0;
        String adl = getClass().getPackage().getName()+".ScopeComposite";
        
        // getComponent(String) starts the component
        Component c = TinfiDomain.getComponent(adl);
        Assert.assertEquals(1,ScopeImpl.startcounter);
        Assert.assertEquals(0,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).stopFc();
        Assert.assertEquals(1,ScopeImpl.startcounter);
        Assert.assertEquals(1,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).startFc();
        Assert.assertEquals(2,ScopeImpl.startcounter);
        Assert.assertEquals(1,ScopeImpl.stopcounter);

        Fractal.getLifeCycleController(c).stopFc();
        Assert.assertEquals(2,ScopeImpl.startcounter);
        Assert.assertEquals(2,ScopeImpl.stopcounter);
    }
    
    @Test
    public void testScopeRequest() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeRequest";
        String service = "s";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,service);

        TestScopeRequestThread t = new TestScopeRequestThread(itf);
        t.start();
        Object content = itf.getContent(DELAY);
        t.join();
        Assert.assertNotSame(
            "Different content instances should be created for request-scoped components",
            content, t.content);
    }
    
    class TestScopeRequestThread extends Thread {
        private Object content;
        private ScopeItf itf;
        public TestScopeRequestThread( ScopeItf itf ) {
            this.itf = itf;
        }
        @Override
        public void run() {
            content = itf.getContent(DELAY);
        }        
    }
    
    @Test
    public void testScopeRequestReentrant() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeRequest";
        String service = "s";
        ScopeItf itf = TinfiDomain.getService(adl,ScopeItf.class,service);

        ScopeImpl.initcounter = 0;
        ScopeImpl.destroycounter = 0;
        
        Object[] contents = itf.loop();
        
        Assert.assertEquals(
            "loop() should return an array of length 2",
            2, contents.length);
        Assert.assertSame(
            "Reentrant calls on a request-scoped component should be handled by the same instance",
            contents[0], contents[1]);
        Assert.assertEquals(
            "@Init annotated method should have been called only once",
            1, ScopeImpl.initcounter);
        Assert.assertEquals(
            "@Destroy annotated method should have been called only once",
            1, ScopeImpl.destroycounter);
    }

    @Test
    public void testScopeConversation() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeConversation";
        String service = "r";
        ScopeConvItf itf = TinfiDomain.getService(adl,ScopeConvItf.class,service);

        Object content1 = itf.first(0);
        Object content2 = itf.second();
        Assert.assertEquals(
            "The same content instance should be used for conversation-scoped component",
            content1, content2);
        itf.third();
        try {
            itf.first(0);
            Assert.fail("Conversation should have ended");
        }
        catch( ConversationEndedException cee ) {}
    }

    @Test
    public void testScopeConversationConcurrent() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeConversation";
        String service = "r";
        ScopeConvItf itf = TinfiDomain.getService(adl,ScopeConvItf.class,service);

        TestScopeConversationThread t = new TestScopeConversationThread(itf);
        t.start();
        Object content = itf.first(DELAY);
        t.join();
        Assert.assertNotSame(
            "Different content instances should be created for conversation-scoped components",
            content, t.content);
    }

    class TestScopeConversationThread extends Thread {
        private Object content;
        private ScopeConvItf itf;
        public TestScopeConversationThread( ScopeConvItf itf ) {
            this.itf = itf;
        }
        @Override
        public void run() {
            content = itf.first(DELAY);
        }        
    }

    @Test
    public void testScopeConversationMaxIdleTime() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeConversation";
        String service = "r";
        ScopeConvItf itf = TinfiDomain.getService(adl,ScopeConvItf.class,service);

        itf.first(0);
        Thread.sleep(1200);
        try {
            itf.second();
            Assert.fail("Conversation should have expired");
        }
        catch( ConversationEndedException cee ) {}
    }

    @Test
    public void testScopeConversationMaxAge() throws Exception {
        
        String adl = getClass().getPackage().getName()+".ScopeConversation";
        String service = "r";
        ScopeConvItf itf = TinfiDomain.getService(adl,ScopeConvItf.class,service);

        itf.first(0);
        Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
        itf.first(0);
        Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
        itf.first(0);
        Thread.sleep(700);  // To prevent expiration by maxIdleTime (1s)
        try {
            itf.second();
            Assert.fail("Conversation should have expired");
        }
        catch( ConversationEndedException cee ) {}
    }
    
    /**
     * @since 1.4
     */
    @Test
    public void testScopeConversationCallback()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {
        
        String adl = getClass().getPackage().getName()+".ScopeConversation";
        String service = "r";
        ScopeConvItf itf =
            TinfiDomain.getService(adl,ScopeConvItf.class,service);
        
        itf.first(0);
        itf.callback();
        itf.callbackEndsConversation();
        try {
            itf.first(0);
            Assert.fail("Conversation should have ended");
        }
        catch( ConversationEndedException cee ) {}
    }
}
