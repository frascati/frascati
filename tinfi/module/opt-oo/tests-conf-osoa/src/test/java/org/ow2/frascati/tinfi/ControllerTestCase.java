/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the features provided by @{link Controller} annotated
 * fields or setter methods.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.1
 */
public class ControllerTestCase {

    protected String adl;
    protected ControllerItf itf;
    
    @Before
    public void setUp()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {
        
        adl = getClass().getPackage().getName()+".Controller";
        String service = "r";
        itf = TinfiDomain.getService(
                adl, ControllerItf.class, service );
    }
    
    @Test
    public void testGetFcComponent() throws NoSuchInterfaceException {
        Component c = itf.getFcComponent();
        String name = Fractal.getNameController(c).getFcName();
        Assert.assertEquals(adl,name);
    }
    
    @Test
    public void testGetFcComponentIsFcInterface() {
        Component c = itf.getFcComponent();
        boolean b = c instanceof Interface;
        Assert.assertEquals(true,b);
    }
}
