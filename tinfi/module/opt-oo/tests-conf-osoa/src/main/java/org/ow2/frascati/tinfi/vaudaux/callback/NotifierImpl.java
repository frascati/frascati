package org.ow2.frascati.tinfi.vaudaux.callback;

import java.util.LinkedList;
import java.util.List;

import org.osoa.sca.annotations.Callback;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Service(interfaces = { NotifierConsumer.class, NotifierProducer.class })
@Scope("CONVERSATION")
public class NotifierImpl implements NotifierProducer , NotifierConsumer {

    private NotifierConsumerCallback notifierConsumerCallback;

    private static List<ClientTopic> callbackRegistered = new LinkedList<ClientTopic>();

    @Callback
    public void setNotifierConsumerCallbackk(NotifierConsumerCallback notifierConsumerCallback) {
        this.notifierConsumerCallback = notifierConsumerCallback;
    }

    public void sendMessage(String topic, String msg) {
        synchronized (callbackRegistered) {    
            for (ClientTopic ct : callbackRegistered) {
                if ((ct.getTopic().compareToIgnoreCase(topic) == 0)) {
                    NotifierConsumerCallback ncb = ct.getCallback();
                    ncb.onMessage(topic, msg);
                }
            }
        }
    }

    public void subscribe(String topic) {
        synchronized (callbackRegistered) {
            boolean alreadySubscribe = false;
            for (ClientTopic ct : callbackRegistered) {
                if ((ct.getCallback() == notifierConsumerCallback)
                        && (ct.getTopic().compareToIgnoreCase(topic) == 0)) {
                    alreadySubscribe = true;
                }
            }
            if (!alreadySubscribe) {
                callbackRegistered.add(new ClientTopic(notifierConsumerCallback, topic));
            }
        }
    }

    public void unsubscribe(String topic) {
        System.out.println("unsubscribe" + topic);
        synchronized (callbackRegistered) {
            ClientTopic toRemove = null;
            for (ClientTopic ct : callbackRegistered) {
                if ((ct.getCallback() == notifierConsumerCallback)
                        && (ct.getTopic().compareToIgnoreCase(topic) == 0)) {
                    toRemove = ct;
                    break;
                }
            }

            if (toRemove != null)
                callbackRegistered.remove(toRemove);
        }
    }

}
