package org.ow2.frascati.tinfi.vaudaux.callback.clients;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierConsumer;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierConsumerCallback;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Service(Runnable.class)
@Scope("COMPOSITE")
public class ClientConsumer implements NotifierConsumerCallback, Runnable {

    public static boolean onMessageCalled = false;
    
    private NotifierConsumer notifierConsumer;

    @Reference(name="notifier")
    public void setNotifierConsumer(NotifierConsumer notifierConsumer){
        this.notifierConsumer = notifierConsumer;
    }
    
    public void onMessage(String topic, String msg) {
        onMessageCalled = true;
    }

    public void run() {
        notifierConsumer.subscribe("topic");        
    }

}
