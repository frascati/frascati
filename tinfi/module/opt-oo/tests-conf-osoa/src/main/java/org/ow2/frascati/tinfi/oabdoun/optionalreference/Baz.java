package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public class Baz implements Bar {

    protected Foo foo = null;
    
    @Reference(name="foo",required=false)
    public void setFoo(Foo f) {
        this.foo = f;
    }
    
    public String getBar() {
        if (this.foo == null)
            return "baz";
        else
            return foo.getFoo();
    }

}
