package org.ow2.frascati.tinfi.vaudaux.callback;

import org.osoa.sca.annotations.Remotable;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Remotable
public interface NotifierConsumerCallback {
    void onMessage(String topic,String msg);
}
