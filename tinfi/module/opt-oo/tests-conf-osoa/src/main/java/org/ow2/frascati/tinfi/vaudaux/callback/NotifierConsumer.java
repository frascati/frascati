package org.ow2.frascati.tinfi.vaudaux.callback;

import org.osoa.sca.annotations.Callback;
import org.osoa.sca.annotations.Conversational;
import org.osoa.sca.annotations.Remotable;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Remotable
@Conversational
@Callback(NotifierConsumerCallback.class)
public interface NotifierConsumer {
    //@OneWay
    void subscribe(String topic);
    //@OneWay
    void unsubscribe(String topic);
}
