package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.osoa.sca.annotations.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Foo.class)
public class FooImpl implements Foo {

    public String getFoo() {
        return "foo";
    }

}
