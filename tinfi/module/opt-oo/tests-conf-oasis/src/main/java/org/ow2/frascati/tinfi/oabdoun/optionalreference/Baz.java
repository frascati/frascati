package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public class Baz implements Bar {

    protected Foo foo = null;
    
    @Reference(name="foo",required=false)
    public void setFoo(Foo f) {
        this.foo = f;
    }
    
    public String getBar() {
        if (this.foo == null)
            return "baz";
        else
            return foo.getFoo();
    }

}
