package org.ow2.frascati.tinfi.oabdoun.constructor;

import org.oasisopen.sca.annotation.Service;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public interface Bar {

    public String getBar();
}
