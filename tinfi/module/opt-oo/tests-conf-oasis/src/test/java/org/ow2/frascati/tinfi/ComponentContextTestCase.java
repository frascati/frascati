/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Class for testing the features provided by the {@link ComponentContext}
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ComponentContextTestCase {
    
    protected Component c;
    protected ComponentContextItf itf;
    
    // TODO tests for createSelfReference
    
    @Before
    public void setUp() throws Exception {
        String adl = getClass().getPackage().getName()+".ComponentContext";
        String service = "r";
        c = TinfiDomain.getComponent(adl);
        itf = TinfiDomain.getService(c,ComponentContextItf.class,service);
    }
    
    @Test
    public void testCast() {
        // sr unused but keep it to check the type of the returned value
        @SuppressWarnings("unused")
        ServiceReference<ComponentContextItf> sr = itf.cast();
    }
    
    @Test
    public void testService() {
        // s unused but keep it to check the type of the returned value
        @SuppressWarnings("unused")
        ComponentContextItf s = itf.getService(ComponentContextItf.class,"s");
    }

    /**
     * @since 1.4
     */
    @Test
    public void testServiceControlLogic() {
        /*
         * Check that the instance returned by getService implements the control
         * logic (e.g. pushing conversations on the conversation stack). This
         * logic is implemented in the generated subclasses of
         * ServiceReferenceImpl and it is expected that getService returns
         * instances of these subclasses.
         */
        ComponentContextItf s = itf.getService(ComponentContextItf.class,"s");        
        boolean b = s instanceof ServiceReference<?>;
        Assert.assertTrue(b);
    }

    @Test
    public void testServiceReference() {
        ServiceReference<ComponentContextItf> sr =
            itf.getServiceReference(ComponentContextItf.class,"s");
        Class <ComponentContextItf> cl = sr.getBusinessInterface();
        Assert.assertEquals(ComponentContextItf.class,cl);
    }

    /**
     * @since 1.4
     */
    @Test
    public void testServiceReferenceControlLogic() {
        /*
         * Check that the instance returned by getService implements the control
         * logic (e.g. pushing conversations on the conversation stack). This
         * logic is implemented in the generated subclasses of
         * ServiceReferenceImpl and it is expected that getService returns
         * instances of these subclasses.
         */
        ServiceReference<ComponentContextItf> sr =
            itf.getServiceReference(ComponentContextItf.class,"s");
        ComponentContextItf s = sr.getService();        
        boolean b = s instanceof ServiceReference<?>;
        Assert.assertTrue(b);
    }

    @Test
    public void testServiceReferenceBadType() {
        try {
            @SuppressWarnings("unused")
            ServiceReference<Runnable> sr =
                itf.getServiceReference(Runnable.class,"s");
            Assert.fail("Reference s is not of type Runnable");
        }
        catch( TinfiRuntimeException tre ) {}  
    }

    @Test
    public void testServiceReferenceNotAReference() {
        try {
            @SuppressWarnings("unused")
            ServiceReference<ComponentContextItf> sr =
                itf.getServiceReference(ComponentContextItf.class,"r");
            Assert.fail("r is not a reference");
        }
        catch( TinfiRuntimeException tre ) {}  
    }
    
    @Test
    public void testGetProperty() throws NoSuchInterfaceException {
        
        Component client =
            Fractal.getContentController(c).getFcSubComponents()[0];
        SCAPropertyController scapc = (SCAPropertyController)
            client.getFcInterface(SCAPropertyController.NAME);
        scapc.setValue("name","Bob");
        
        String value = itf.getProperty(String.class, "name");
        Assert.assertEquals("Bob",value);
    }

    @Test
    public void testServiceName() {
        String sn = itf.getRequestContextServiceName();
        Assert.assertEquals("r",sn);
    }

    @Test
    public void testServiceBusinessInterface() {
        Class<?> bitf = itf.getRequestContextServiceBusinessInterface();
        Assert.assertEquals(ComponentContextItf.class,bitf);
    }

    /**
     * @since 1.4.1
     */
    @Test
    public void testServices() {
        // s unused but keep it to check the type of the returned value
        @SuppressWarnings("unused")
        Collection<ComponentContextItf> s =
            itf.getServices(ComponentContextItf.class,"cols");
    }

    /**
     * @since 1.4.1
     */
    @Test
    public void testServiceReferences() {
        Collection<ServiceReference<ComponentContextItf>> srs =
            itf.getServiceReferences(ComponentContextItf.class,"cols");
        for (ServiceReference<ComponentContextItf> sr : srs) {
            Class <ComponentContextItf> cl = sr.getBusinessInterface();
            Assert.assertEquals(ComponentContextItf.class,cl);
        }
    }
}
