/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import java.lang.reflect.Method;

import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.GenericClassHelper;

/**
 * This class generates interceptor classes. This generator differs from {@link
 * org.objectweb.fractal.juliac.proxy.InterceptorClassGenerator} by generating
 * a call to the proceed() method instead of a call to the delegating instance.
 * These interceptor classes are used to invoke AOP-like advice instances.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 0.4
 */
public class InterceptorClassGenerator
extends org.objectweb.fractal.juliac.proxy.InterceptorClassGenerator {

    public InterceptorClassGenerator() {
        super();
    }

    @Override
    public String getDelegatingInstance( Method proxym ) {
        return "impl";
    }

    @Override
    public void generateProxyMethodBodyDelegatingCode(
        BlockSourceCodeVisitor mv, Method proxym ) {

        GenericClassHelper gc = GenericClassHelper.get(proxycl);
        String rtypename = gc.getGenericReturnType(proxym);

        if( rtypename.equals("void") ) {
            mv.visitln("    ijp.proceed();");
            mv.visitln("    Object ret = null;");
        }
        else {
            Class<?> rtype = proxym.getReturnType();
            if( rtype.isPrimitive() ) {
                mv.visitln("    Object proceed = ijp.proceed();");
                mv.visit  ("    ");
                mv.visit  (rtypename);
                mv.visit  (" ret = (proceed==null) ? ");
                String nul = ClassHelper.getNullValue(rtype);
                mv.visit  (nul);
                mv.visit  (" : (");
                Class<?> boxed = ClassHelper.box(rtype);
                mv.visit  (boxed.getName());
                mv.visitln(")proceed;");
            }
            else {
                mv.visit("    ");
                mv.visit(rtypename);
                mv.visit(" ret = ");
                if( ! rtypename.equals("java.lang.Object") ) {
                    mv.visit("(");
                    mv.visit(rtypename);
                    mv.visit(") ");
                }
                mv.visitln("ijp.proceed();");
            }
        }
    }
    
    /**
     * @since 1.1.1
     */
    @Override
    public void generateFieldImpl( ClassSourceCodeVisitor cv ) {
        /*
         * The implementation of the impl field is inherited from
         * TinfiComponentInterceptor.
         */
    }
    
    /**
     * @since 1.1.1
     */
    @Override
    public void generateMethodGetFcItfDelegate( ClassSourceCodeVisitor cv ) {
        /*
         * The implementation of the getFcItfDelegate method is inherited from
         * TinfiComponentInterceptor.
         */
    }

    /**
     * @since 1.1.1
     */
    @Override
    public void generateMethodSetFcItfDelegate( ClassSourceCodeVisitor cv ) {
        /*
         * The implementation of the setFcItfDelegate method is inherited from
         * TinfiComponentInterceptor.
         */
    }
}
