/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import java.lang.reflect.Modifier;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ThenSourceCodeVisitor;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;

/**
 * The class generates implementations of the SCA content controller specific
 * for a given composite-scoped content class.
 * 
 * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
public class SCAContentControllerCompositeScopeClassGenerator
extends SCAContentControllerClassGenerator {

    protected SCAContentControllerCompositeScopeClassGenerator(
        Juliac jc, String contentClassName, ContentClassMetaData ccmd) {

        super(jc,contentClassName,ccmd);
    }

    @Override
    public void generateMethods( ClassSourceCodeVisitor cv ) {
        super.generateMethods(cv);
        generateMethodSetFcContent(cv);
    }

    protected void generateFieldContent( ClassSourceCodeVisitor cv ) {
        cv.visitField(Modifier.PRIVATE,contentClassName,"content",null);
    }
    
    protected void generateMethodGetFcContent( BlockSourceCodeVisitor mv ) {
        BlockSourceCodeVisitor bv = mv.visitSync("this");
        ThenSourceCodeVisitor tv = bv.visitIf("content == null");
        generateCreateFcInstance(tv);
        generateInitializeFcInstance(tv);
        tv.visitEnd();
        bv.visitEnd();
        mv.visitIns("return content");
    }
    
    protected void generateMethodReleaseFcContent( ClassSourceCodeVisitor cv ) {
        BlockSourceCodeVisitor mv =
            cv.visitMethod(
                Modifier.PUBLIC,null,"void","releaseFcContent",
                new String[]{"Object content","boolean isEndMethod"},null);
        // Indeed nothing
        mv.visitEnd();
    }

    protected void generateMethodGetFcCurrentContents( BlockSourceCodeVisitor mv ) {
        ThenSourceCodeVisitor thenv = mv.visitIf("content == null");
        thenv.visitIns("return new ",contentClassName,"[0]");
        BlockSourceCodeVisitor elsev = thenv.visitElse();
        elsev.visitIns("return new ",contentClassName,"[]{content}");
        elsev.visitEnd();
    }
    
    @Override
    protected void generateMethodEagerInit( BlockSourceCodeVisitor mv ) {                
        mv.visitIns("getFcContent()");
    }
    
    protected void generateMethodSetFcContent( ClassSourceCodeVisitor cv ) {
        
        BlockSourceCodeVisitor mv =
            cv.visitMethod(
                Modifier.PUBLIC,null,"void","setFcContent",
                new String[]{"Object _content"},
                new String[]{
                    IllegalLifeCycleException.class.getName(),
                    ContentInstantiationException.class.getName()});
        
        // Check that the component is stopped
        mv.visitSet("String state","weaveableOptLC.getFcState()");
        ThenSourceCodeVisitor thenv = mv.visitIf("! state.equals(\""+LifeCycleController.STOPPED+"\")");
        thenv.visitIns(
            "throw new ",IllegalLifeCycleException.class.getName(),
            "(\"Component should be stopped\")");
        thenv.visitEnd();
        
        // Check that _content is instance of contentClass
        thenv = mv.visitIf("! (_content instanceof",contentClassName,")");
        thenv.visitIns(
            "throw new ",ContentInstantiationException.class.getName(),
            "(\"Content \"+_content+\" is not instance of \"+",
            contentClassName,".class.getName())");
        thenv.visitEnd();
        
        // Record the transmitted _content instance and initialize it
        mv.visitSet("content","(",contentClassName,")_content");
        
        generateInitializeFcInstance(mv);
        mv.visitEnd();
    }
}
