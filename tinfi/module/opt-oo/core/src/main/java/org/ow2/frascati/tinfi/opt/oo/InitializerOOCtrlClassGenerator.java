/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.helper.JuliacHelper;
import org.objectweb.fractal.juliac.module.FCSourceCodeGeneratorItf;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;

/**
 * This class generates the source code for initializing Fractal components.
 * 
 * This initializer class generator assumes that the content, the interceptors
 * and the controllers are kept in separate classes and that the controllers are
 * implemented with objects.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class InitializerOOCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.oo.InitializerOOCtrlClassGenerator {

    public InitializerOOCtrlClassGenerator(
        Juliac jc, FCSourceCodeGeneratorItf<SimpleMembraneDesc> fcscg,
        SimpleMembraneDesc membraneDesc, ComponentType ct,
        Object contentDesc, Object source ) {
    
        super(jc,fcscg,membraneDesc,ct,contentDesc,source);
    }
        
    @Override
    protected void generateNewFcContentMethod( BlockSourceCodeVisitor mv ) {
        mv.visitIns("return null");
    }

    @Override
    protected void generateContentChecks( BlockSourceCodeVisitor mv ) {
        
        String ctrlDesc = membraneDesc.getDescriptor();
        String contentClassName =
            JuliacHelper.getContentClassName(ctrlDesc,contentDesc);

        if( contentClassName != null ) {
            InterfaceType[] its = ct.getFcInterfaceTypes();
            for (int i = 0; i < its.length; i++) {
                if( ! its[i].isFcClientItf() ) {
                    String signature = its[i].getFcItfSignature();
                    BlockSourceCodeVisitor then =
                        mv.visitIf(
                            "!",signature,".class.isAssignableFrom(",
                            contentClassName,".class)");
                    then.visitVar(
                        "String","msg","\""+contentClassName,"should implement",
                        signature+"\"");
                    then.visitIns(
                        "throw","new",
                        InstantiationException.class.getName()+"(msg)");
                    then.visitEnd();
                }
            }
        }        
    }

    @Override
    public void generateInitializationContextForContent(
        BlockSourceCodeVisitor mv ) {
        
        mv.visitSet(
            "ic.content",
            contentDesc==null ?
                "null" :
                (contentDesc.toString()+".class") );
    }

    @Override
    protected void generateNFICMExternalInterface(
        BlockSourceCodeVisitor mv, InterfaceType it,
        boolean attrimplgenerated ) {
        
        super.generateNFICMExternalInterface(mv,it,attrimplgenerated);

        String itname = it.getFcItfName();
        if( ! itname.equals("attribute-controller") ) {
            mv.visitIns(
                "((",TinfiComponentInterceptor.class.getName(),
                ")intercept).setFcItf(proxy)");
        }
    }

    @Override
    protected MembraneInitializerOOCtrlClassGenerator
    getMembraneInitializerClassGenerator() {
        MembraneInitializerOOCtrlClassGenerator micg =
            new MembraneInitializerOOCtrlClassGenerator(
                jc,fcscg,this,membraneDesc,source);
        return micg;
    }    
}
