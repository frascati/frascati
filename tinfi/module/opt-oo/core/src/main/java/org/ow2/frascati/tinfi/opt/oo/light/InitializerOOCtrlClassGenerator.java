/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import java.lang.reflect.Modifier;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.factory.ChainedInstantiationException;
import org.objectweb.fractal.julia.loader.Initializable;
import org.objectweb.fractal.julia.loader.Tree;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.desc.ControllerDesc;
import org.objectweb.fractal.juliac.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.helper.TreeHelper;
import org.objectweb.fractal.juliac.module.FCSourceCodeGeneratorItf;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.control.content.IllegalContentClassMetaData;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;

/**
 * This class generates the source code for initializing Fractal components.
 * 
 * This initializer class generator assumes that the content, the interceptors
 * and the controllers are kept in separate classes and that the controllers are
 * implemented with objects.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
public class InitializerOOCtrlClassGenerator
extends org.ow2.frascati.tinfi.opt.oo.InitializerOOCtrlClassGenerator {

    public InitializerOOCtrlClassGenerator(
        Juliac jc, FCSourceCodeGeneratorItf<SimpleMembraneDesc> fcscg,
        SimpleMembraneDesc membraneDesc, ComponentType ct,
        Object contentDesc, Object source ) {
    
        super(jc,fcscg,membraneDesc,ct,contentDesc,source);
    }
        
    @Override
    protected void generateExtraCode( ClassSourceCodeVisitor cv ) {
        BlockSourceCodeVisitor mv =
            cv.visitMethod(
                Modifier.PUBLIC, null, Component.class.getName(),
                "newFcControllerInstantiation",
                new String[]{InitializationContext.class.getName()+" ic"},
                new String[]{InstantiationException.class.getName()} );
        generateNewFcControllerInstantiationMethod(mv);
        mv.visitEnd();        
    }
    
    protected void generateNewFcControllerInstantiationMethod( BlockSourceCodeVisitor mv ) {
        
        List<ControllerDesc> ctrlDescs = membraneDesc.getCtrlDescs();
        
        mv.visitln("    Object ctrl = null;");

        for (ControllerDesc ctrlDesc : ctrlDescs) {
            String ctrlImplName = ctrlDesc.getImplName();
            Class<?> cl = jc.loadClass(ctrlImplName);
            if( SCAExtendedContentController.class.isAssignableFrom(cl) ) {
                // SCA Content Controller
                String contentClassName = (String) contentDesc;
                SourceCodeGeneratorItf cg = null;
                try {
                    cg = SCAContentControllerClassGenerator.get(jc,contentClassName);
                }
                catch (IllegalContentClassMetaData e) {
                    throw new TinfiRuntimeException(e);
                }
                ctrlImplName = cg.getTargetTypeName();
            }
            mv.visitln("    ic.controllers.add(ctrl = new "+ctrlImplName+"());");
            if( Initializable.class.isAssignableFrom(cl) ) {
                Tree tree = ctrlDesc.getTree();
                if( tree != null ) {
                    mv.visitln("    "+Tree.class.getName()+" tree = "+TreeHelper.javaify(tree)+';');
                    mv.visitln("    try {");
                    mv.visitln("      (("+Initializable.class.getName()+")ctrl).initialize(tree);");
                    mv.visitln("    }");
                    mv.visitln("    catch( Exception e ) {");
                    mv.visitln("      throw new "+ChainedInstantiationException.class.getName()+"(e,null,\"\");");
                    mv.visitln("    }");
                }
            }
            if( Component.class.isAssignableFrom(cl) ) {
                mv.visitln("    "+Component.class.getName()+" compctrl = ("+Component.class.getName()+")ctrl;");
            }
        }

        mv.visitln("    return compctrl;");
    }

    @Override
    protected MembraneInitializerOOCtrlClassGenerator
    getMembraneInitializerClassGenerator() {
        MembraneInitializerOOCtrlClassGenerator micg =
            new MembraneInitializerOOCtrlClassGenerator(
                jc,fcscg,this,membraneDesc,source);
        return micg;
    }    
}
