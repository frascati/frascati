/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import java.io.IOException;
import java.lang.annotation.Annotation;

import org.oasisopen.sca.annotation.Callback;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.type.BasicInterfaceType;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.generator.ProxyClassGeneratorItf;
import org.objectweb.fractal.juliac.api.generator.SourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.annotation.AnnotationHelper;
import org.objectweb.fractal.juliac.conf.JulietLoader;
import org.objectweb.fractal.juliac.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.module.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.proxy.InterfaceImplementationClassGenerator;

/**
 * This class generates the source code associated to Fractal components.
 * The membrane implementation and the initializer implementation are generated.
 * 
 * The content, the interceptors and the controllers are kept in separate
 * classes and the controllers are implemented with objects.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class FCOOCtrlSourceCodeGenerator
extends org.objectweb.fractal.juliac.opt.oo.FCOOCtrlSourceCodeGenerator {
    
    @Override
    protected void postInit() throws IOException {
        
        /*
         * Don't call super.postInit(). This generator does not use the .cfg
         * based configuration mechanism for defining membranes.
         */
        
        /*
         * Use loadModuleIfNew instead of loadModule, since other modules may
         * retrieve the Juliet module to register membrane definitions. This is
         * the case with the EasyCommons Implementation module.
         * 
         * Use JulietLoader as the service type, instead of MembraneLoaderItf,
         * since we really want an instance of JulietLoader, not of e.g.
         * MembraneLoader that would be the case when e.g. the OO mode is
         * registered before the current one (see the associated JUnit test).
         */
        
        mloader =
            jc.loadModuleIfNew(
                JulietLoader.class.getName(), JulietLoader.class );

        mloader.put(SCAPrimitive.NAME,SCAPrimitive.class);
        mloader.put(SCAComposite.NAME,SCAComposite.class);
        mloader.put(SCAContainer.NAME,SCAContainer.class);
        mloader.put(SCACompositeWithContent.NAME,SCACompositeWithContent.class);
    }

    
    // -----------------------------------------------------------------------
    // Implementation of the FCSourceCodeGeneratorItf interface
    // -----------------------------------------------------------------------

    /**
     * Return the source code generator for the initializer class associated
     * with this component source code generator.
     */
    @Override
    protected InitializerOOCtrlClassGenerator getInitializerClassGenerator(
        Juliac jc, FCSourceCodeGeneratorItf<SimpleMembraneDesc> fcscg,
        SimpleMembraneDesc membraneDesc, ComponentType ct,
        Object contentDesc, Object source ) {

        return new InitializerOOCtrlClassGenerator(
            jc,fcscg,membraneDesc,ct,contentDesc,source);
    }

    /**
     * Return the source code generator for component interfaces.
     */
    @Override
    public ProxyClassGeneratorItf getInterfaceClassGenerator(InterfaceType it) {

        final String signature = it.getFcItfSignature();
        final Class<?> cl = jc.loadClass(signature);
        final String pkgRoot = jc.getPkgRoot();

        ProxyClassGeneratorItf pcg = null;
        String itname = it.getFcItfName();
        
        /*
         * See the comments in method generateInterfaceImpl for the rationale
         * for choosing interface source code generator.
         */
        
        if( itname.endsWith("-controller") || itname.equals("component") ) {
            pcg =
                new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
        }
        else {
            if( it.isFcClientItf() ) {
                /*
                 * We should be returning here two interface source code
                 * generators since the client interface class extends the
                 * server one corresponding to the same type. However, the
                 * current method is mainly used for retrieving the name of the
                 * generated interface class (such as in initializer source code
                 * generators.) It is then sufficient to return only the class
                 * that corresponds to the real implementation.
                 */
                pcg = new ClientInterfaceClassGenerator(it,cl,pkgRoot,false);
            }
            else {
                pcg = new ServerInterfaceClassGenerator(it,cl,pkgRoot,false);                
            }
        }
        
        return pcg;
    }
    
    /**
     * Generate the source code of component interfaces associated with the
     * specified interface type.
     */
    @Override
    protected void generateInterfaceImpl( InterfaceType it, String ctrlDesc )
    throws IOException {

        final String signature = it.getFcItfSignature();
        final Class<?> cl = jc.loadClass(signature);
        final String pkgRoot = jc.getPkgRoot();
        
        String itname = it.getFcItfName();
        if( itname.endsWith("-controller") || itname.equals("component") ) {

            /*
             * Control interfaces.
             * 
             * Keep InterfaceImplementationClassGenerator as a source code
             * generator for control interfaces even though
             * ServerInterfaceClassGenerator could be used. This allows reusing
             * pre-generated implementations for Julia control interfaces.
             */
            SourceCodeGeneratorItf cg = 
                new InterfaceImplementationClassGenerator(it,cl,pkgRoot,false);
            jc.generateSourceCode(cg);
        }
        else {
           
            /*
             * Business interfaces.
             */

            // Server (input) interface implementation
            SourceCodeGeneratorItf cg =
                new ServerInterfaceClassGenerator(it,cl,pkgRoot,false);
            jc.generateSourceCode(cg);
            
            if( it.isFcClientItf() ) {
                /*
                 * Client (output) interface implementations. Server (input)
                 * interface implementations are generated also for client ones
                 * since client interface implementations extend the server
                 * interface implementation corresponding to the same type.
                 */
                cg = new ClientInterfaceClassGenerator(it,cl,pkgRoot,false);
                jc.generateSourceCode(cg);
            }
                
            // ServiceReference implementation
            cg = new ServiceReferenceClassGenerator(it,cl,pkgRoot,null,false);
            jc.generateSourceCode(cg);
            
            // ServiceReference and input interface implementations for callbacks
            Annotation annot =
                ClassHelper.getAnnotation(
                    cl,
                    Callback.class.getName(),
                    "org.osoa.sca.annotations.Callback");
            if( annot != null ) {
                Class<?> cbcl =
                    AnnotationHelper.getAnnotationParamValue(annot,"value");
                InterfaceType cbit =
                    new BasicInterfaceType(
                        "callback", cbcl.getName(), false, false, false );
                cg = new ServiceReferenceClassGenerator(
                        cbit,cbcl,pkgRoot,null,false);
                jc.generateSourceCode(cg);
                
                cg = new CallBackInterfaceClassGenerator(
                        cbit,cbcl,pkgRoot,null,false);
                jc.generateSourceCode(cg);
            }
        }
    }
}
