/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.Juliac;
import org.objectweb.fractal.juliac.api.visit.BlockSourceCodeVisitor;
import org.objectweb.fractal.juliac.desc.SimpleMembraneDesc;
import org.objectweb.fractal.juliac.module.FCSourceCodeGeneratorItf;
import org.objectweb.fractal.juliac.opt.oo.InitializerOOCtrlClassGenerator;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;

/**
 * Generator for object-oriented control membrane initializer. An membrane
 * initializer provides the code which is common to all component initializers
 * for a given membrane descriptor.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.2
 */
public class MembraneInitializerOOCtrlClassGenerator
extends org.objectweb.fractal.juliac.opt.oo.MembraneInitializerOOCtrlClassGenerator {

    public MembraneInitializerOOCtrlClassGenerator(
        Juliac jc, FCSourceCodeGeneratorItf<SimpleMembraneDesc> fcscg,
        InitializerOOCtrlClassGenerator icg, SimpleMembraneDesc membraneDesc,
        Object source ) {
     
        super(jc,fcscg,icg,membraneDesc,source);
    }

    @Override
    protected void generateNCICMProxyCreation(
        BlockSourceCodeVisitor mv, boolean typedef, String fcitfClassname,
        String delegate, String itname, InterfaceType it ) {

        super.generateNCICMProxyCreation(
            mv,typedef,fcitfClassname,delegate,itname,it);

        mv.visitIns(
            "((",TinfiComponentInterceptor.class.getName(),
            ")intercept).setFcItf(proxy)");
    }
}
