/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo;

import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.content.BasicContentControllerMixin;
import org.objectweb.fractal.julia.control.content.BindingContentMixin;
import org.objectweb.fractal.julia.control.content.CheckContentMixin;
import org.objectweb.fractal.julia.control.content.SuperContentMixin;
import org.objectweb.fractal.julia.control.content.TypeContentMixin;

/**
 * Definition of the SCA container membrane.
 * 
 * This is a membrane for SCA composites where subcomponents can be removed
 * without stopping the composite. The idea is to use such composites as simple
 * containers where components are independent from each others and can be added
 * and removed freely.
 * 
 * This membrane provides the same control interfaces as scaComposite. Only the
 * implementation of the content controller differs: lifecycle related
 * contraints are skipped.
 * 
 * This membrane was introduced in Tinfi 1.3.1 and defined with Tinfilet in
 * version 1.4.3.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.3
 */
@Membrane(desc=SCAContainer.NAME)  // generator and interceptors are inherited
public class SCAContainer extends SCAComposite {
    
    final public static String NAME = "scaContainer";
    
    @Controller(
        name="content-controller",
        impl="SCAContainerContentControllerImpl",
        mixins={
            BasicControllerMixin.class,
            UseComponentMixin.class,
            BasicContentControllerMixin.class,
            CheckContentMixin.class,
            TypeContentMixin.class,
            BindingContentMixin.class,
//            UseLifeCycleControllerMixin.class,
//            LifeCycleContentMixin.class,
            SuperContentMixin.class})
    protected ContentController cc;        
}
