/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.opt.oo;

import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.juliac.api.visit.ClassSourceCodeVisitor;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.proxy.InterfaceImplementationClassGenerator;
import org.ow2.frascati.tinfi.TinfiComponentInterface;

/**
 * This class generates the source code of component server (input) interface
 * implementations.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 1.1.1
 */
public class ServerInterfaceClassGenerator
extends InterfaceImplementationClassGenerator {
    
    public ServerInterfaceClassGenerator(
        InterfaceType it, Class<?> cl, String pkgRoot, boolean mergeable ) {
        
        super(it,cl,pkgRoot,mergeable);
    }
    
    /**
     * Return the name of the suffix which is appended to generated class names.
     */
    @Override
    public String getClassNameSuffix() {
        return "FcInItf";
    }
    
    @Override
    public String getSuperClassName() {

        StringBuffer sb = new StringBuffer();
        sb.append(TinfiComponentInterface.class.getName());
        sb.append('<');
        
        /*
         * Add type parameters if any.
         */
        String signature = it.getFcItfSignature();
        sb.append(signature);
        String[] tpnames = ClassHelper.getTypeParameterNames(proxycl);
        if( tpnames.length != 0 ) {
            String s = ClassHelper.getTypeParameterNamesSignature(tpnames);
            sb.append(s);
        }
        
        sb.append('>');
        return sb.toString();
    }

    @Override
    public void generateFieldImpl( ClassSourceCodeVisitor cv ) {
        /*
         * Indeed nothing.
         * The field is managed by TinfiComponentInterface.
         */
    }
    
    @Override
    public void generateMethodGetFcItfImpl( ClassSourceCodeVisitor cv ) {
        /*
         * Indeed nothing.
         * The method is inherited from TinfiComponentInterface.
         */
    }

    @Override
    public void generateMethodSetFcItfImpl( ClassSourceCodeVisitor cv ) {
        /*
         * Indeed nothing.
         * The method is inherited from TinfiComponentInterface.
         */
    }        
}
