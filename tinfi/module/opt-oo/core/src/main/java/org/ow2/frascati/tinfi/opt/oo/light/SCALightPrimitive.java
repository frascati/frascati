/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.opt.oo.light;

import org.oasisopen.sca.ComponentContext;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicComponentMixin;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.TypeComponentMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin;
import org.objectweb.fractal.julia.control.binding.CheckBindingMixin;
import org.objectweb.fractal.julia.control.binding.ContentBindingMixin;
import org.objectweb.fractal.julia.control.binding.LifeCycleBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.julia.control.content.BasicSuperControllerMixin;
import org.objectweb.fractal.julia.control.content.SuperControllerNotifier;
import org.objectweb.fractal.julia.control.content.UseSuperControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.lifecycle.BasicLifeCycleCoordinatorMixin;
import org.objectweb.fractal.julia.control.lifecycle.ContainerLifeCycleMixin;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.objectweb.fractal.julia.control.lifecycle.TypeLifeCycleMixin;
import org.objectweb.fractal.julia.control.lifecycle.UseLifeCycleControllerMixin;
import org.objectweb.fractal.julia.control.name.BasicNameControllerMixin;
import org.objectweb.fractal.julia.control.name.UseNameControllerMixin;
import org.objectweb.fractal.juliac.proxy.LifeCycleSourceCodeGenerator;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.binding.CollectionInterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.binding.InterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.component.ComponentContextMixin;
import org.ow2.frascati.tinfi.control.content.LifeCycleSCAContentMixin;
import org.ow2.frascati.tinfi.control.content.RequestContextSCAContentMixin;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;
import org.ow2.frascati.tinfi.control.content.SCALightContentControllerMixin;
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerMixin;
import org.ow2.frascati.tinfi.control.lifecycle.SCALifeCycleMixin;
import org.ow2.frascati.tinfi.control.property.SCAPrimitivePropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.control.property.SCAPropertyPromoterMixin;
import org.ow2.frascati.tinfi.control.property.UseSCAPropertyControllerMixin;
import org.ow2.frascati.tinfi.opt.oo.SCAContentInterceptorSourceCodeGenerator;
import org.ow2.frascati.tinfi.opt.oo.SCATinfiInterceptorSourceCodeGenerator;

/**
 * Definition of the SCA primitive membrane with no intent controller.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.3
 */
@Membrane(
    desc=SCALightPrimitive.NAME,
    generator=InterceptorNoIntentClassGenerator.class,
    interceptors={
        SCATinfiInterceptorSourceCodeGenerator.class,
        LifeCycleSourceCodeGenerator.class,
        SCAContentInterceptorSourceCodeGenerator.class})
public class SCALightPrimitive {
    
    final public static String NAME = "scaPrimitive";
    
    // ---------------------------------------------------------
    // Julia controllers
    // ---------------------------------------------------------
    @Controller(
        name="component",
        impl="ComponentImpl",
        mixins={
            BasicControllerMixin.class,
            BasicComponentMixin.class,
            TypeComponentMixin.class})
    protected Component comp;
                
    @Controller(
        name="binding-controller",
        impl="SCABindingControllerImpl",
        mixins={
            BasicControllerMixin.class,
            BasicBindingControllerMixin.class,
            TypeBasicBindingMixin.class,
            CollectionInterfaceBindingControllerMixin.class,
            InterfaceBindingControllerMixin.class,
            UseSCAContentControllerMixin.class,
            UseComponentMixin.class,
            CheckBindingMixin.class,
            TypeBindingMixin.class,
            UseSuperControllerMixin.class,
            ContentBindingMixin.class,
            UseLifeCycleControllerMixin.class,
            LifeCycleBindingMixin.class})
    protected BindingController bc;
    
    @Controller(
        name="lifecycle-controller",
        impl="SCALifeCycleControllerImpl",
        mixins={
            BasicControllerMixin.class,
            UseComponentMixin.class,
            BasicLifeCycleCoordinatorMixin.class,
            BasicLifeCycleControllerMixin.class,
            TypeLifeCycleMixin.class,
            ContainerLifeCycleMixin.class,
            UseSCAContentControllerMixin.class,
            SCALifeCycleMixin.class})
    protected LifeCycleCoordinator lc;    
    
    @Controller(
        name="super-controller",
        impl="SuperControllerImpl",
        mixins={
            BasicControllerMixin.class,
            BasicSuperControllerMixin.class})
    protected SuperControllerNotifier sc;
    
    @Controller(
        name="name-controller",
        impl="NameControllerImpl",
        mixins={
            BasicControllerMixin.class,
            BasicNameControllerMixin.class})
    protected NameController nc;
            
    // ---------------------------------------------------------
    // Tinfi controllers
    // ---------------------------------------------------------
    
    @Controller(
        name="sca-component-controller",
        impl="SCAComponentControllerImpl",
        mixins={
            BasicControllerMixin.class,
            UseComponentMixin.class,
            UseSCAContentControllerMixin.class,
            UseSCAPropertyControllerMixin.class,
            ComponentContextMixin.class})
    protected ComponentContext compctx;
        
    @Controller(
        name="sca-property-controller",
        impl="SCAPrimitivePropertyControllerImpl",
        mixins={
            BasicControllerMixin.class,
            UseSCAContentControllerMixin.class,
            UseNameControllerMixin.class,
            SCAPrimitivePropertyControllerMixin.class,
            SCAPropertyControllerMixin.class,
            SCAPropertyPromoterMixin.class})
    protected SCAPropertyController scapc;

    // ---------------------------------------------------------
    // Tinfi hidden controllers
    // ---------------------------------------------------------
    
    @Controller(
        name="/sca-content-controller",
        impl="SCAContentControllerImpl",
        mixins={
            BasicControllerMixin.class,
            RequestContextSCAContentMixin.class,
            UseComponentMixin.class,
            UseSCAPropertyControllerMixin.class,
            SCALightContentControllerMixin.class,
            UseLifeCycleControllerMixin.class,
            LifeCycleSCAContentMixin.class})
    protected SCAExtendedContentController scacc;        
}
