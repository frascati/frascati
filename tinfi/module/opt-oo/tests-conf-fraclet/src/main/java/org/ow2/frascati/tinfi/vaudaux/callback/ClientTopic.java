package org.ow2.frascati.tinfi.vaudaux.callback;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
public class ClientTopic {

    private  NotifierConsumerCallback callback;
    private String topic;
    
    public ClientTopic(NotifierConsumerCallback callback, String topic)
    {
        this.callback = callback;
        this.topic = topic;
    }
    
    public NotifierConsumerCallback getCallback() {
        return callback;
    }

    public String getTopic() {
        return topic;
    }

}
