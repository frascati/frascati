package org.ow2.frascati.tinfi.vaudaux.callback;

import org.oasisopen.sca.annotation.Callback;
import org.oasisopen.sca.annotation.Remotable;
import org.osoa.sca.annotations.Conversational;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Remotable
@Conversational
@Callback(NotifierConsumerCallback.class)
public interface NotifierConsumer {
    //@OneWay
    void subscribe(String topic);
    //@OneWay
    void unsubscribe(String topic);
}
