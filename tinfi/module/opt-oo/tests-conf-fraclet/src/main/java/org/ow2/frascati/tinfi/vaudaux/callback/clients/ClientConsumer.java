package org.ow2.frascati.tinfi.vaudaux.callback.clients;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierConsumer;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierConsumerCallback;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Service(Runnable.class)
@Scope("COMPOSITE")
public class ClientConsumer implements NotifierConsumerCallback, Runnable {

    public static boolean onMessageCalled = false;
    
    @Requires(name="notifier")
    private NotifierConsumer notifierConsumer;

    public void onMessage(String topic, String msg) {
        onMessageCalled = true;
    }

    public void run() {
        notifierConsumer.subscribe("topic");        
    }

}
