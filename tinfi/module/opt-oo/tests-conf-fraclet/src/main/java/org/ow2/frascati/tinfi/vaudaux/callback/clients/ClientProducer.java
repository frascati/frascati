package org.ow2.frascati.tinfi.vaudaux.callback.clients;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.ow2.frascati.tinfi.vaudaux.callback.NotifierProducer;

/**
 * @author Guillaume Vaudaux-Ruth <guillaume.vaudaux-ruth@inria.fr>
 */
@Service(Runnable.class)
@Scope("COMPOSITE")
public class ClientProducer implements Runnable {
    
    @Requires(name="notifier")
    private NotifierProducer notifierProducer;
    
    public void run() {
        notifierProducer.sendMessage("topic", "msg");
    }
}
