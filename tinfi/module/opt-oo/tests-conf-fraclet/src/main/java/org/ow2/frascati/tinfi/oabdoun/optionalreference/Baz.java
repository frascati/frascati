package org.ow2.frascati.tinfi.oabdoun.optionalreference;

import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.annotations.Requires;
import org.objectweb.fractal.fraclet.types.Contingency;

/**
 * @author Olivier Abdoun <Olivier.Abdoun@inria.fr>
 */
@Service(Bar.class)
public class Baz implements Bar {

    @Requires(name="foo",contingency=Contingency.OPTIONAL)
    protected Foo foo = null;
    
    public String getBar() {
        if (this.foo == null)
            return "baz";
        else
            return foo.getFoo();
    }

}
