/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;

/**
 * Class for testing the use of references.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ReferenceTestCase {

    @Test
    public void testMandatory()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {
        
        String adl = getClass().getPackage().getName()+".ReferenceMandatoryNotSet";
        String service = "r";
        Runnable itf = TinfiDomain.getService(adl,Runnable.class,service);

        try {
            itf.run();
            Assert.fail("Mandatory reference shouldn't be allowed to be null");
        }
        catch(TinfiRuntimeException re) {}
    }
    
    @Test
    public void testOptional()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {
        
        String adl = getClass().getPackage().getName()+".ReferenceOptional";
        String service = "r";
        ReferenceOptionalItf itf =
            TinfiDomain.getService(adl,ReferenceOptionalItf.class,service);

        ReferenceOptionalItf opt = itf.opt();
        Assert.assertNull("Unbound optional reference should be null",opt);

        Collection<ReferenceOptionalItf> opts = itf.opts();
        Assert.assertNotNull(
            "Unbound optional multiple reference should not be null",opts);
        int size = opts.size();
        Assert.assertEquals(
            "Unbound optional multiple reference should be an empty array",0,
            size);
    }
    
    @Test
    public void testMultiple()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {
        
        String adl = getClass().getPackage().getName()+".ReferenceMultiple";
        String service = "r";
        Runnable itf = TinfiDomain.getService(adl,Runnable.class,service);

        itf.run();
        Assert.assertSame(2, ReferenceMultipleImpl.count);
    }
    
    /**
     * @since 1.3.1
     */
    @Test
    public void testReInjection()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException, IllegalBindingException {

        String adl = getClass().getPackage().getName()+".ReferenceOptional";
        String service = "r";
        
        Component root = TinfiDomain.getComponent(adl);
        ContentController cc = Fractal.getContentController(root);
        Component c = cc.getFcSubComponents()[0];
        BindingController bc = Fractal.getBindingController(c);
        LifeCycleController lc = Fractal.getLifeCycleController(c);
        ReferenceOptionalItf itf = (ReferenceOptionalItf)
            c.getFcInterface(service);

        // The client collection interface opts is empty
        Collection<ReferenceOptionalItf> opts = itf.opts();
        int size = opts.size();
        Assert.assertEquals(0,size);

        // Add a new binding for opts and check that the dependency is injected
        bc.bindFc("opts0",itf);
        opts = itf.opts();
        size = opts.size();
        Assert.assertEquals(1,size);

        // Add a new binding for opts and check that the dependency is injected
        bc.bindFc("opts1",itf);
        opts = itf.opts();
        size = opts.size();
        Assert.assertEquals(2,size);

        // Remove the first binding
        lc.stopFc();
        bc.unbindFc("opts0");
        lc.startFc();
        opts = itf.opts();
        size = opts.size();
        Assert.assertEquals(1,size);

        // Remove the second binding
        lc.stopFc();
        bc.unbindFc("opts1");
        lc.startFc();
        opts = itf.opts();
        size = opts.size();
        Assert.assertEquals(0,size);
    }
}
