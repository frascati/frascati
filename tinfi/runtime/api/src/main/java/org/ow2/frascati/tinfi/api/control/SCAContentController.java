/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;

/**
 * Content control interface for SCA primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public interface SCAContentController {
    
    /** <code>NAME</code> of the content controller. */
    final public static String NAME = "/sca-content-controller";
    
    /**
     * Declare the content class associated with this component. This operation
     * can only be performed if the component is stopped.
     * 
     * @param c  the content class
     * @throws IllegalLifeCycleException  if the component is not stopped
     * @throws ContentInstantiationException
     *      if the content can not be instantiated
     * @since 1.0
     */
    public void setFcContentClass( Class<?> c )
    throws IllegalLifeCycleException, ContentInstantiationException;

    /**
     * Return the content class associated with this component.
     * 
     * @since 1.4.1
     */
    public Class<?> getFcContentClass();
    
    /**
     * Return the current content instance. Note that depending on the scope
     * policy, several content instances may exist concurrently. However, at
     * most, only one instance can be associated with the requesting thread.
     * 
     * @throws ContentInstantiationException
     *      if the content can not be instantiated
     */
    public Object getFcContent() throws ContentInstantiationException;

    /**
     * For composite-scoped components, specify the content instance to be used.
     * 
     * @param content  the content instance
     * @throws IllegalLifeCycleException  if the component is not stopped
     * @throws ContentInstantiationException
     *         if the content can not be initialized or
     *         if the component is not composite-scoped
     * @since 1.4.3
     */
    public void setFcContent( Object content )
    throws IllegalLifeCycleException, ContentInstantiationException;
}
