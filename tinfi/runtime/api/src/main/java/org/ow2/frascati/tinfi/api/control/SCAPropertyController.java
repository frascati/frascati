/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api.control;

/**
 * Property control interface for SCA primitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public interface SCAPropertyController {
    
    /** <code>NAME</code> of the content controller. */
    final public static String NAME = "sca-property-controller";
    
    /**
     * <p>
     * Set the type of the specified property. If the property type has already
     * been set, the old value is lost, and the new one is recorded.
     * </p>
     * 
     * <p>
     * The rationale for recording property types lies in the fact that complex
     * properties may have a type which differs from the one of the field where
     * their values will be injected. Typically, complex property values will be
     * injected in fields of type Object.
     * </p>
     * 
     * @param name   the property name
     * @param type   the property type
     * @since 1.1.1
     */
    public void setType( String name, Class<?> type );
    
    /**
     * Set the value of the specified property. If the property value has
     * already been set, the old value is lost, and the new one is recorded.
     * 
     * @param name   the property name
     * @param value  the property value
     * @throws IllegalArgumentException
     *         if a type has been defined for the specified property with {@link
     *         #setValue(String, Object)} and if the specified value is not an
     *         instance of this type
     */
    public void setValue( String name, Object value )
    throws IllegalArgumentException;
    
    /**
     * Return the type of the specified property. Return <code>null</code> if
     * the property type has not been set.
     * 
     * @param name  the property name
     * @return      the property value
     * @since 1.1.1
     */
    public Class<?> getType( String name );
    
    /**
     * Return the value of the specified property. Return <code>null</code> if
     * the property value has not been set.
     * 
     * @param name  the property name
     * @return      the property value
     */
    public Object getValue( String name );
    
    /**
     * Return <code>true</code> if the specified property has been set.
     * 
     * @param name  the property name
     * @return      <code>true</code> if the property has been set,
     *              <code>false</code> otherwise
     */
    public boolean containsPropertyName( String name );
    
    /**
     * Return the names of the properties whose values have been set by invoking
     * {@link #setValue(String, Object)}.
     * 
     * @since 1.1.1
     */
    public String[] getPropertyNames();
    
    /**
     * Return <code>true</code> if the specified property is declared by the
     * content class associated with the current component. Else, or if the
     * current component is not associated with a content class, return
     * <code>false</code>.
     * 
     * @param name  the property name
     * @return      <code>true</code> if the property is declared
     * @since 1.4.2
     */
    public boolean isDeclared( String name );
    
    /**
     * Return <code>true</code> if the specified property has been set by
     * calling {@link #setValue(String, Object)}.
     * 
     * @param name  the property name
     * @return      <code>true</code> if the property has been set
     * @since 1.4.2
     */
    public boolean hasBeenSet( String name );
    
    /**
     * Set the reference of the property controller which promotes the specified
     * property to the current property controller.
     * 
     * @param name      the property name
     * @param promoter  the promoter component or
     *                  <code>null</code> to unregister the promoter
     * @throws IllegalPromoterException
     *      when attempting to set a cycle between property promoters
     * @since 0.4.3
     */
    public void setPromoter( String name, SCAPropertyController promoter )
    throws IllegalPromoterException;
    
    /**
     * Return the reference of the property controller which promotes the
     * specified property. Return <code>null</code> if the property is managed
     * locally by the current property controller.
     * 
     * @param name  the property name
     * @return      the promoter component or <code>null</code>
     * @since 0.4.3
     */
    public SCAPropertyController getPromoter( String name );

    /**
     * Set the reference of the property controller which promotes the specified
     * property name to the current property controller and the associated
     * property name at the level of the promoter.
     * 
     * @param name      the property name
     * @param promoter  the promoter component or
     *                  <code>null</code> to unregister the promoter
     * @param promoterPropertyName
     *                     the property name at the level of the promoter
     * @throws IllegalPromoterException
     *      when attempting to set a cycle between property promoters
     * @since 1.4
     */
    public void setPromoter(
        String name, SCAPropertyController promoter,
        String promoterPropertyName )
    throws IllegalPromoterException;    

    /**
     * Return the name of the property defined at the level of the component
     * which promotes the specified property. Return <code>null</code> if the
     * property is managed locally by the current property controller.
     * 
     * @param name  the property name
     * @return      the property name at the level of the promoter or
     *                 <code>null</code>
     * @since 1.4
     */
    public String getPromoterPropertyName( String name );
    
    /**
     * Remove the promoter for the specified property name.
     * 
     * @param name  the property name
     * @since 1.4.5
     */
    public void removePromoter( String name );
}
