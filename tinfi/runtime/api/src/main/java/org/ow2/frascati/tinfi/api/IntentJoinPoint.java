/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.api;

import org.aopalliance.intercept.MethodInvocation;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;

/**
 * Interface for introspecting intent join points. An intent join point
 * corresponds to the interface where a service invocation has been intercepted.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 0.3
 */
public interface IntentJoinPoint extends MethodInvocation {

    /**
     * Return the component for which the intent has been defined.
     * 
     * @since 1.4.1
     */
    public Component getComponent();
    
    /**
     * Return the component interface (service or reference) for which the
     * intent has been defined.
     * 
     * @since 0.4.5
     */
    public Interface getInterface();
}
