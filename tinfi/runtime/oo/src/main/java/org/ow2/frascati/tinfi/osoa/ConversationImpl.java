/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.osoa;

import org.osoa.sca.Conversation;


/**
 * Implementation of the conversation management interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class ConversationImpl implements Conversation {
    
    // ----------------------------------------------------------------------
    // Implementation of the Conversation interface
    // ----------------------------------------------------------------------
    
    private Object conversationId;
    
    public void end() {
        state = ConversationState.STOPPED;
    }

    public Object getConversationID() {
        return conversationId;
    }

    
    // ----------------------------------------------------------------------
    // Management of the ConversationImpl chain.
    //
    // This chain is used by the ConversationManager. Prior to 1.4.5 I was using
    // a Stack in ConversationManager but Romain suggested this smarter
    // implementation.
    // ----------------------------------------------------------------------

    /** @since 1.4.5 */
    private ConversationImpl previous;
    
    /** @since 1.4.5 */
    public void setPrevious( ConversationImpl previous ) {
        this.previous = previous;
    }
    
    /** @since 1.4.5 */
    public ConversationImpl getPrevious() { return previous; }
    
    
    // ----------------------------------------------------------------------
    // Implementation specific
    // ----------------------------------------------------------------------
    
    private ConversationState state = ConversationState.CREATED;
    private long creation = 0;
    private long lastAccess = 0;
    
    public void setConversationID( Object conversationId ) {
        this.conversationId = conversationId;
    }
    
    public ConversationState getFcState() {
        return state;
    }
    
    public long getCreation() {
        return creation;
    }
    
    public long getLastAccess() {
        return lastAccess;
    }
    
    public void start() {
        creation = System.currentTimeMillis();
        lastAccess = creation;
        state = ConversationState.STARTED;
    }
    
    public void access() {
        lastAccess = System.currentTimeMillis();
    }
}
