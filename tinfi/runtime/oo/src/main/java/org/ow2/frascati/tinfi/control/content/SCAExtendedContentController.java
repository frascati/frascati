/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import org.oasisopen.sca.RequestContext;
import org.oasisopen.sca.ServiceReference;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Content control interface for SCA primitive components. This interface
 * extends the regular SCA content control interface with methods which are
 * meant to be used internally, typically by interceptors and controllers.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.1
 */
public interface SCAExtendedContentController extends SCAContentController {
    
    /**
     * Notify the content controller that the specified content instance is no
     * longer needed. If relevant with the the scope policy, this gives the
     * controller the opportunity to call the {@link
     * org.oasisopen.sca.annotation.Destroy} annotated method on the content
     * instance.
     * 
     * @param content  the content instance which is released
     * @param isEndMethod
     *      <code>true</code> if the method which releases the content instance
     *      is annotated with {@link org.osoa.sca.annotations.EndsConversation}
     */
    public void releaseFcContent( Object content, boolean isEndMethod );
    
    /**
     * Set the context associated with the current request.
     * 
     * @param rc  the request context
     * @since 1.4.5
     */
    public void setRequestContext( RequestContext rc );
    
    /**
     * Return the context associated with the current request.
     * 
     * @return  the request context
     * @since 1.4.5
     */
    public RequestContext getRequestContext();
    
    /**
     * Eager initialize the content instance associated with this component.
     * Relevant only for composite-scoped components.
     * 
     * @throws ContentInstantiationException
     *      if the content can not be instantiated or if the component is not
     *      composite-scoped
     */
    public void eagerInit() throws ContentInstantiationException;

    /**
     * Invoke the <code>@Lifecycle(step=Step.START)</code> annotated method on
     * all current content instances associated with the component.
     * 
     * @throws ContentInstantiationException
     *                 in case of exception when invoking the method
     * @since 1.3
     */
    public void start() throws ContentInstantiationException;
    
    /**
     * Invoke the <code>@Lifecycle(step=Step.STOP)</code> annotated method on
     * all current content instances associated with the component.
     * 
     * @throws ContentInstantiationException
     *                 in case of exception when invoking the method
     * @since 1.3
     */
    public void stop() throws ContentInstantiationException;
    
    /**
     * Return <code>true</code> if the specified property is declared by the
     * content class managed by this controller.
     * 
     * @param name  the property name
     * @return      <code>true</code> if the property is declared,
     *              <code>false</code> otherwise
     * @since 1.1.2
     */
    public boolean containsPropertyName( String name );
    
    /**
     * Return the names of the properties declared by the content class
     * managed by this controller.
     * 
     * @return  the propery names
     * @since 1.1.1
     */
    public String[] getPropertyNames();

    /**
     * Return the type of the specified property declared by the content class
     * managed by this controller.
     * 
     * @param name  the property name
     * @return      the property type
     * @since 1.1.2
     */
    public Class<?> getPropertyType( String name );
    
    /**
     * Inject on all current content instances associated with the component,
     * the value of the specified property.
     *  
     * @param name   the property name
     * @param value  the property value
     * @since 1.2.1
     */
    public void setPropertyValue( String name, Object value );

    /**
     * Inject the specified value for the specified reference on all current
     * content instances associated with the component.
     *  
     * @param name   the reference name
     * @param value  the reference value
     * @since 1.3.1
     */
    public void setReferenceValue( String name, ServiceReference<?> value );

    /**
     * Unset the specified reference on all current content instances associated
     * with the component.
     *  
     * @param name   the reference name
     * @since 1.3.1
     */
    public void unsetReferenceValue( String name );
    
    /**
     * Return the value stored in the specified property for all content
     * instances. If the content instances store divergent values, return
     * <code>null</code>.
     * 
     * @param name  the property name
     * @return      the property value
     * @since 1.6
     */
    public Object getPropertyValue( String name );
}
