/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Authors: Lionel Seinturier, Philippe Merle
 */

package org.ow2.frascati.tinfi.control.content.scope;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;

/**
 * Manager for composite-scoped components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public class CompositeScopeManager extends AbstractScopeManager {

    public CompositeScopeManager( Component comp, ContentClassMetaData ccmd ) {
        super(comp,ccmd);
    }
    
    public Object getFcContent() throws ContentInstantiationException {
        synchronized(this) {
            if( contentCompositeScope == null ) {
                contentCompositeScope = createAndInitializeFcInstance();
            }
        }
        return contentCompositeScope;
    }
    
    public void releaseFcContent( Object content, boolean isEndMethod ) {
        /*
         * Indeed nothing.
         * 
         * The content instance is persisting for composite-scoped components.
         */
    }
    
    /**
     * Return the current content instances.
     * 
     * @return  the current content instances
     * @since 1.2.1
     */
    public Object[] getFcCurrentContents() {
        if( contentCompositeScope == null ) {
            return new Object[]{};
        }
        else {
            return new Object[]{contentCompositeScope};
        }
    }
    
    @Override
    public void setFcContent( Object content )
    throws ContentInstantiationException {
        
        List<String> initnames = new ArrayList<String>();
        try {
            synchronized(this) {
                initializeFcInstance(content,initnames);
                contentCompositeScope = content;
            }
        }
        catch (Exception e) {
            if( e instanceof ContentInstantiationException ) {
                throw (ContentInstantiationException) e;
            }
            if( e instanceof RuntimeException ) {
                throw (RuntimeException) e;
            }
            throw new ContentInstantiationException(e);
        }
    }

    /** The content instance for composite scoped components. */
    private Object contentCompositeScope;
}
