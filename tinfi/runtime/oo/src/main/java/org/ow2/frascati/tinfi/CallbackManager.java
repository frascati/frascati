/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi;

import java.util.EmptyStackException;

import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;

/**
 * <p>
 * This class manages a thread-local stack of callback service references.
 * </p>
 * 
 * <p>
 * Prior to 1.4.5 I was using a Stack but Romain suggested a smarter
 * implementation based on a chain stored in {@link ServiceReferenceImpl}.
 * </p>
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Romain Rouvoy <Romain.Rouvoy@lifl.fr>
 */
public class CallbackManager {

    /**
     * Return the singleton instance of this class.
     * 
     * @since 1.1.1
     */
    public static CallbackManager get() { return instance; }
    private static CallbackManager instance = new CallbackManager();
    
    /**
     * The current service reference to invoke when a callback is needed.
     * 
     * @since 1.4.5
     */
    private static ThreadLocal<ServiceReferenceImpl<?>> tl =
        new ThreadLocal<ServiceReferenceImpl<?>>();
    
    /** @since 1.1.1 */
    public void push( ServiceReferenceImpl<?> sr ) {
        ServiceReferenceImpl<?> current = tl.get();
        sr.setPrevious(current);
        tl.set(sr);
    }
    
    /** @since 1.1.1 */
    public ServiceReferenceImpl<?> peek() {
        ServiceReferenceImpl<?> current = tl.get();
        if( current == null ) {
            throw new EmptyStackException();
        }
        return current;
    }

    /** @since 1.1.1 */
    public ServiceReferenceImpl<?> pop() {
        
        ServiceReferenceImpl<?> current = tl.get();
        if( current == null ) {
            throw new EmptyStackException();
        }
        
        ServiceReferenceImpl<?> previous = current.getPrevious();
        current.setPrevious(null);
        tl.set(previous);
        return current;
    }
    
    public boolean isEmpty() {
        ServiceReferenceImpl<?> current = tl.get();
        boolean b = (current == null);
        return b;
    }
}
