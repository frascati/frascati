/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.control.content.scope;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.ServiceReference;
import org.oasisopen.sca.annotation.Property;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.fraclet.types.Contingency;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPoint;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPointFieldImpl;
import org.objectweb.fractal.juliac.commons.lang.annotation.AnnotationHelper;
import org.objectweb.fractal.juliac.commons.lang.reflect.MethodHelper;
import org.ow2.frascati.tinfi.CallbackManager;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;

/**
 * Root class for scope managers.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public abstract class AbstractScopeManager implements ScopeManager {
        
    /**
     * The metadata for the content class associated to the current
     * scoped-component.
     */
    protected ContentClassMetaData ccmd;
    private Component comp;

    public AbstractScopeManager( Component comp, ContentClassMetaData ccmd ) {
        this.comp = comp;
        this.ccmd = ccmd;
    }
    
    /**
     * <code>true</code> if the content class associated with {@link #ccmd} has
     * been instantiated at least once.
     * 
     * This field is used to determined if default expression associated with
     * field-based @{@link Property}s need to be registered with the property
     * controller. This registration should occur only once in order to prevent
     * unwanted loss of values which may been set latter on by invoking the
     * property controller.
     */
    private boolean instantiatedOnce = false;

    public void setFcContent( Object content )
    throws ContentInstantiationException {
        String msg =
            "setFcContent(Object) can only be invoked for composite-scoped "+
            "components";
        throw new ContentInstantiationException(msg);
    }
    
    
    // ----------------------------------------------------------------------
    // Methods used by subclasses
    // ----------------------------------------------------------------------
    
    /**
     * Create and initialize a content instance.
     */
    protected Object createAndInitializeFcInstance()
    throws ContentInstantiationException {
        
        List<String> initnames = new ArrayList<String>();
        
        try {
            Object content = createFcInstance(initnames);
            initializeFcInstance(content,initnames);
            return content;
        }
        catch( Exception e ) {
            if( e instanceof ContentInstantiationException ) {
                throw (ContentInstantiationException) e;
            }
            if( e instanceof RuntimeException ) {
                throw (RuntimeException) e;
            }
            throw new ContentInstantiationException(e);
        }
    }
    
    /**
     * Create a new content instance.
     * 
     * @param initnames
     *      this list is empty when the method is called and populated with the
     *      reference and property names initialized while creating the content
     *      instance with a @{@link Constructor} annotated constructor
     */
    private Object createFcInstance( List<String> initnames )
    throws
        java.lang.InstantiationException, IllegalAccessException,
        InstantiationException, InvocationTargetException,
        NoSuchInterfaceException {
        
        Object content = null;
        
        if( ccmd.constructorAnnotatedElement != null ) {
            
            // @Constructor annotated constructor defined in the content class
            
            ComponentType ct = (ComponentType) comp.getFcType();

            /*
             * Extra property names may be specified by annotating the
             * parameters of the @Constructor annotated contructor.
             */
            Constructor<?> ctor = ccmd.constructorAnnotatedElement;
            Annotation[][] paramsAnnots = ctor.getParameterAnnotations();
            for (Annotation[] paramAnnots : paramsAnnots) {
                for (Annotation pannot : paramAnnots) {
                    if( pannot instanceof Property ) {
                        Property propannot = (Property) pannot;
                        String name = propannot.name();
                        initnames.add(name);
                    }
                }
            }
            
            Object[] values = new Object[initnames.size()];
            for (int i = 0 ; i < initnames.size() ; i++) {
                String name = initnames.get(i);
                try {
                    InterfaceType it = ct.getFcInterfaceType(name);
                    Object sr = getServiceReference(it);
                    values[i] = sr;
                }
                catch( NoSuchInterfaceException nsie ) {
                    /*
                     * Not a reference. Check whether this is a property.
                     */
                    SCAPropertyController scapc = (SCAPropertyController)
                        comp.getFcInterface(SCAPropertyController.NAME);
                    if( ! scapc.containsPropertyName(name) ) {
                        /*
                         * Not a reference, nor a property.
                         */
                        String msg =
                            "Identifier "+name+
                            " in @Constructor attributes for class "+
                            ccmd.fcContentClass+
                            " is neither a reference name, nor a property name";
                        throw new InstantiationException(msg);
                    }
                    values[i] = scapc.getValue(name);
                }
            }
            
            /*
             * Construct the content instance by invoking the constructor with
             * initialization values.
             */
            ctor = ccmd.constructorAnnotatedElement;
            content = ctor.newInstance(values);
        }
        else if( ccmd.providesMethod != null ) {
            
            // @Provides annotated factory method in the content class
            Object obj = ccmd.fcContentClass.newInstance();
            content = ccmd.providesMethod.invoke(obj);
        }
        else {
            content = ccmd.fcContentClass.newInstance();
        }
            
        
        return content;
    }
    
    /**
     * Initialize newly created content instances.
     * 
     * @param content  the content instance
     * @param initnames
     *      the reference and property names which has been already initialized
     *      on the specified content instance
     */
    protected void initializeFcInstance(
        Object content, List<String> initnames )
    throws
        ContentInstantiationException, IllegalAccessException,
        InvocationTargetException, NoSuchInterfaceException {
        
        /*
         * Inject control interfaces.
         */
        for (InjectionPoint<Annotation> ip : ccmd.controllerAnnotatedElements) {
            
            Annotation annot = ip.getAnnotation();
            String name = AnnotationHelper.getAnnotationParamValue(annot,"name");

            /*
             * Do not try to optimize by taking the value of the comp field when
             * the component control interface is requested. The reason follows.
             * 
             * The value stored in comp has been set when mixing the layers and
             * originates from the getInterface() method of
             * InitializationContext. This method skips the component interface
             * and returns the delegate. Here the delegate is the reference of
             * the interceptor. It is much better for the programmer to retrieve
             * in the injection point a reference which can be cast to
             * Interface. We then query comp to retrieve the "real" component
             * interface.
             */
            Object ctrlItf = comp.getFcInterface(name);
            
            Class<?> ctrlItfCl = ctrlItf.getClass();
            Class<?> type = ip.getType();
            if( ! type.isAssignableFrom(ctrlItfCl) ) {
                String msg =
                    "The type "+ctrlItfCl.getName()+" of the "+name+
                    " control interface can not be injected into "+type;
                throw new ContentInstantiationException(msg);
            }
            
            ip.set(content,ctrlItf);
        }
        
        /*
         * Inject the component context.
         */
        if( ccmd.contextAnnotatedElement != null ) {
            ComponentContext compCtx = (ComponentContext)
                comp.getFcInterface("sca-component-controller");
            ccmd.contextAnnotatedElement.set(content,compCtx);
        }
        
        /*
         * Inject the component name.
         */
        if( ccmd.componentNameAnnotatedElement != null ) {
            NameController nc =
                (NameController) comp.getFcInterface("name-controller");
            String name = nc.getFcName();
            ccmd.componentNameAnnotatedElement.set(content,name);
        }
        
        /*
         * Inject annotated references.
         */
        for (Map.Entry<String,InjectionPoint<Annotation>> entry : ccmd.refs.entrySet()) {
            String name = entry.getKey();
            InjectionPoint<Annotation> ip = entry.getValue();
            Class<?> iptype = ip.getType();
            
            if( List.class.isAssignableFrom(iptype) ||
                Map.class.isAssignableFrom(iptype) ) {
                
                // Collection reference
                Object[] itfs = comp.getFcInterfaces();
                Map<String,Object> srs = new HashMap<String,Object>();
                List<Object> l = new ArrayList<Object>();
                for (Object o : itfs) {
                    Interface itf = (Interface) o;
                    String itfname = itf.getFcItfName();
                    if( itfname.startsWith(name) ) {
                        Object sr = getServiceReference(itf);
                        srs.put(itfname,sr);
                        l.add(sr);
                    }
                }
                if( List.class.isAssignableFrom(iptype) ) {
                    // SCA style 
                    ip.set(content,l);
                }
                else {
                    // Fraclet style
                    ip.set(content,srs);
                }
                for (Object o : l) {
                    checkMandatoryReferenceHasBeenSet(o,ip,name);
                }
            }
            else {
                // Singleton reference
                try {
                    Object itf = comp.getFcInterface(name);
                    Object sr = getServiceReference(itf);
                    ip.set(content,sr);
                    checkMandatoryReferenceHasBeenSet(sr,ip,name);
                }
                catch( NoSuchInterfaceException nsie ) {
                    /*
                     * The annotated reference is not declared in the component
                     * type. This may correspond to an annotated class which is
                     * used with different component type. No value needs to be
                     * injected.
                     */
                }
            }            
        }
        
        /*
         * Inject annotated properties.
         */
        SCAPropertyController scapc = (SCAPropertyController)
            comp.getFcInterface(SCAPropertyController.NAME);
        for (Map.Entry<String,InjectionPoint<Annotation>> entry : ccmd.props.entrySet()) {
            String name = entry.getKey();
            InjectionPoint<Annotation> ip = entry.getValue();

            boolean hasBeenSet = scapc.hasBeenSet(name);
            if(!hasBeenSet) {
                /*
                 * Skip property whose value has not been set. Properties may be
                 * either declared in a content class or dynamically defined by
                 * calling setValue() on the property controller. Note that
                 * setValue() may also be called for declared properties. Here
                 * we only want to deal with properties which have been set.
                 */
                continue;
            }
            
            Object value = scapc.getValue(name);
            ip.set(content,value); 
        }
        
        /*
         * Inject POJO defined references and properties.
         * 
         * It may be the case that the setter has been defined for another type
         * that the one of the reference or the property we are trying to
         * inject. In this case, we are simply skipping the injection.
         */
        for (Map.Entry<String,Method> entry : ccmd.setters.entrySet()) {
            String name = entry.getKey();
            Method setter = entry.getValue();
            
            try {
                // Check whether this is a reference
                ComponentType ct = (ComponentType) comp.getFcType();
                InterfaceType it = ct.getFcInterfaceType(name);
                // Should we test that this is a client interface?
                Object sr = getServiceReference(it);
                MethodHelper.invokeSetter(content,setter,sr);
            }
            catch( NoSuchInterfaceException nsie ) {
                // Check whether this is a property
                if( scapc.containsPropertyName(name) ) {
                    Object value = scapc.getValue(name);
                    MethodHelper.invokeSetter(content,setter,value);
                }
                /*
                 * Else, this is neither a reference, nor a property. We do not
                 * inject anything in this setter.
                 */
            }
        }
        
        /*
         * Register all field properties with the property controller.
         * 
         * The idea is that field-based properties may have a default expression
         * in the Java code, and that this value should be registered with the
         * property controller in order for it to be able to return this value
         * when SCAPropertyController#getValue(String) is invoked.
         */
        Set<String> names = ccmd.props.keySet();
        if( ! instantiatedOnce ) {
            for (String name : names) {
                if( ccmd.props.containsKey(name) ) {
                    InjectionPoint<Annotation> ip = ccmd.props.get(name);
                    if( ip instanceof InjectionPointFieldImpl ) {
                        Object value = ip.get(content);
                        /*
                         * value.getClass() has been suggested by Romain on
                         * Nov 9, 2011, while developing FraSCAla and to enable
                         * a smooth integration of Scala, FraSCAti, and Tinfi,
                         * unless the exact reason why we need this is not
                         * completely clear to me ;-)
                         */
                        Class<?> type = value==null ? ip.getType() : value.getClass();
                        scapc.setType(name,type);
                        scapc.setValue(name,value);
                    }
                }
            }
            instantiatedOnce = true;
        }
        
        /*
         * Check that all mandatory properties have been set.
         */
        for (String name : names) {
            if( ! scapc.hasBeenSet(name) ) {
                InjectionPoint<Annotation> ip = ccmd.props.get(name);
                Annotation annot = ip.getAnnotation();
                boolean isRequired =  // <Boolean> for compilation with javac
                    AnnotationHelper.<Boolean>getAnnotationParamValue(annot,"required");
                if(isRequired) {
                    NameController nc =
                        (NameController) comp.getFcInterface("name-controller");
                    String msg =
                        "Mandatory property(s) "+name+" in component "+
                        nc.getFcName()+" should have been set";
                    throw new ContentInstantiationException(msg);
                }
            }
        }
        
        /*
         * Set callbacks.
         */
        if( ccmd.callbacks.length > 0 ) {
            CallbackManager cm = CallbackManager.get();
            if( ! cm.isEmpty() ) {
                ServiceReference<?> cr = cm.peek();
                Class<?> businessItf = cr.getBusinessInterface();
                
                for(InjectionPoint<Annotation> ip : ccmd.callbacks) {
                    Class<?> type = ip.getType();
                    if( type.isAssignableFrom(businessItf) ) {
                        ip.set(content,cr);
                        break;
                    }
                }
            }
        }
        
        /*
         * Invoke the @Init annotated method, if any.
         */
        if( ccmd.initMethod != null ) {
            Method init = ccmd.initMethod;
            init.invoke(content);
        }
    }
    
    /**
     * If the specified reference is mandatory, check that it has been set.
     * 
     * @param sr    the service reference
     * @param name  the name of the service reference
     * @throws ContentInstantiationException  if the reference has not been set
     */
    private void checkMandatoryReferenceHasBeenSet(
        Object sr, InjectionPoint<Annotation> ip, String name )
    throws ContentInstantiationException {
        
        Annotation annot = ip.getAnnotation();
        boolean isRequired =  false;
        try {
            isRequired =  // <Boolean> for compilation with javac
                AnnotationHelper.<Boolean>getAnnotationParamValue(annot,"required");
        }
        catch( NullPointerException npe ) {
            /*
             * NPE occurs when getAnnotationParamValue returns null.
             * null cannot be assigned to boolean, hence the NPE.
             * 
             * Check the Fraclet annotation style with the contingency
             * parameter.
             */
            Contingency contingency =
                AnnotationHelper.getAnnotationParamValue(annot,"contingency");
            if( contingency == null ) {
                /*
                 * Shouldn't occur. The annotation is either SCA style with a 
                 * required parameter or Fraclet style with a contingency
                 * parameter.
                 */
                final String msg =
                    "required or contingency parameter expected on annotation "+
                    annot;
                throw new TinfiRuntimeException(msg);
            }
            isRequired = contingency.equals(Contingency.MANDATORY);
        }
        
        if ( isRequired && sr==null ) {
            String fcname = "null";
            try {
                NameController nc =
                    (NameController) comp.getFcInterface("name-controller");
                fcname = nc.getFcName();
            }
            catch( NoSuchInterfaceException nsie ) {
                /*
                 * Shouldn't occur. Even if it occurs, the degraded mode
                 * consists in leaving fcname as null.
                 */
            }
            final String msg =
                "Mandatory reference " + name + " in component " + fcname +
                "should have been set";
            throw new ContentInstantiationException(msg);
        } 
    }
    
    /**
     * Return the {@link ServiceReference} or the list of {@link
     * ServiceReference}s associated to the specified {@link InterfaceType}.
     */
    private Object getServiceReference( InterfaceType it )
    throws NoSuchInterfaceException {
        
        String name = it.getFcItfName();
        if( it.isFcCollectionItf() ) {
            Object[] itfs = comp.getFcInterfaces();
            Map<String,Object> srs = new HashMap<String,Object>();
            for (Object o : itfs) {
                Interface itf = (Interface) o;
                String itfname = itf.getFcItfName();
                if( itfname.startsWith(name) ) {
                    Object sr = getServiceReference(itf);
                    srs.put(itfname,sr);
                }
            }
            return srs;
        }
        else {
            Object itf = comp.getFcInterface(name);
            Object sr = getServiceReference(itf);
            return sr;
        }
    }
    
    /**
     * Return a {@link ServiceReference} corresponding to the specified client
     * interface or <code>null</code> if the client interface is not bound.
     */
    private Object getServiceReference( Object clientItf ) {
        ComponentInterface itf = (ComponentInterface) clientItf;
        Interceptor outInterceptor = (Interceptor) itf.getFcItfImpl();
        Object delegate = outInterceptor.getFcItfDelegate();
        if( delegate == null ) {
            /*
             * Unbound client interface. At this point this means that this is
             * an optional interface. Mandatory unbound interfaces are detected
             * when starting the component.
             */
            return null;
        }
        TinfiComponentOutInterface<?> tcoi =
            (TinfiComponentOutInterface<?>) clientItf;
        ServiceReference<?> sr = tcoi.getServiceReference();
        return sr;
    }
    
    /**
     * Destroy the specified content instance.
     */
    protected void destroyFcInstance( Object content ) {
                
        /*
         * Invoke the @Destroy method, if any.
         */
        if( ccmd.destroyMethod != null ) {
            Method destroy = ccmd.destroyMethod;
            try {
                destroy.invoke(content);
            }
            catch( Exception e ) {
                throw new TinfiRuntimeException(e);
            }
        }
    }
}
