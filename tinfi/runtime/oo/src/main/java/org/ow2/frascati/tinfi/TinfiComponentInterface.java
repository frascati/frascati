/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2009-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.julia.BasicComponentInterface;

/**
 * The root class for Tinfi component interfaces.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 1.1.1
 */
public class TinfiComponentInterface<T> extends BasicComponentInterface {
    
    public TinfiComponentInterface() {
        super();
    }

    public TinfiComponentInterface(
        Component owner, String name, Type type, boolean isInternal,
        Object impl ) {
        
        super(owner,name,type,isInternal,impl);
    }

    public void setFcItfImpl( Object impl ) {
        /*
         * The impl parameter is not declared to be of type T since the compiler
         * in this case complains that the method does not override setFcItfImpl
         * from the super class.
         */
        this.impl = (T) impl;
    }
    public T getFcItfImpl() { return impl; }
    protected T impl;

    /**
     * @author Philippe Merle <Philippe.Merle@inria.fr>
     * @since 1.4
     */
    @Override
    public Object clone () {
        
        /*
         * The delegate (field impl, of type TinfiComponentInterceptor) for the
         * cloned interface must reference the cloned interface.
         * 
         * This is needed for collection interfaces (anyway, we currently don't
         * use the clone method for other purposes than collection interfaces,
         * do we?). The model interface named "/collection/..." serves as a
         * template for creating (with the clone method) a concrete instance
         * when a binding is created. Hence the need for the delegate in the
         * cloned interface to reference the cloned interface and not the model
         * interface.
         */
        
        TinfiComponentInterface<?> clone =
            (TinfiComponentInterface<?>) super.clone();
        ((TinfiComponentInterceptor<?>)(clone.impl)).setFcItf(clone);
        return clone;
    }
}
