/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Authors: Lionel Seinturier, Philippe Merle
 */

package org.ow2.frascati.tinfi.osoa;

import java.util.HashMap;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.ContentClassMetaData;
import org.ow2.frascati.tinfi.control.content.scope.AbstractScopeManager;

/**
 * Manager for request-scoped components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 */
public class RequestScopeManager extends AbstractScopeManager {

    public RequestScopeManager( Component comp, ContentClassMetaData ccmd ) {
        super(comp,ccmd);
    }
    
    public synchronized Object getFcContent()
    throws ContentInstantiationException {
        
        Thread currentThread = Thread.currentThread();
        InstanceCount ic = null;
        
        if( contentMapRequestScope.containsKey(currentThread) ) {
            ic = contentMapRequestScope.get(currentThread);
        }
        else {
            Object content = createAndInitializeFcInstance();
            ic = new InstanceCount(content,0);
            contentMapRequestScope.put(currentThread,ic);
        }
        
        ic.count++;
        return ic.content;
    }
    
    public synchronized void releaseFcContent(
        Object content, boolean isEndMethod ) {

        Thread currentThread = Thread.currentThread();
        InstanceCount ic = contentMapRequestScope.get(currentThread);
        
        if( ic.count == 1 ) {
            // Return of the first call
            destroyFcInstance(content);            
            contentMapRequestScope.remove(ic);
        }
        else {
            ic.count--;
        }
    }

    public synchronized Object[] getFcCurrentContents() {
        Object[] contents = new Object[contentMapRequestScope.size()];
        int i = 0;
        for (InstanceCount ic : contentMapRequestScope.values()) {
            contents[i] = ic.content;
            i++;
        }
        return contents;
    }

    /**
     * The map of content instances for this request scoped component. The key
     * is the thread executing this component, and the value is the content
     * instance associated with this thread.
     */
    private Map<Thread,InstanceCount> contentMapRequestScope =
        new HashMap<Thread,InstanceCount>();

    /**
     * @author Lionel <Lionel.Seinturier@univ-lille1.fr>
     * @since 1.4.5
     */
    private static class InstanceCount {
        public Object content;
        public int count;
        public InstanceCount( Object content, int count ) {
            this.content = content;
            this.count = count;
        }
    }
}
