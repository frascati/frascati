============================================================================
OW2 FraSCAti Tinfi
Copyright (C) 2007-2018 Inria, Univ. Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Lionel Seinturier
============================================================================

Tinfi 1.6
---------

Tinfi is a runtime kernel for the SCA component model. Tinfi provides an
infrastructure for running Java applications which comply with version 1.1 of
the OASIS SCA specifications. Tinfi generates at compile-time the containers
which host SCA components. Tinfi provides a dual support for OASIS SCA 1.1 and
OSOA SCA 1.0 annotated components. In addition, POJO components are also
supported. The generated code conforms with the API and the principles of the
Fractal component model. Tinfi is thus a SCA personality forthe Fractal
framework. Tinfi extends Juliac which is the Fractal compiler tool.

The name Tinfi stands for "Tinfi Is Not a Fractal Implementation".

Tinfi is a free software distributed under the terms of the GNU Lesser
General Public license. Tinfi is written in Java.


Table of content
----------------
  1. Requirements
  2. Introduction
  3. Compiling and installing Tinfi
  4. Running the Hello World application
  5. Compiling and running your own applications
  6. References


1. Requirements
---------------
Maven 2.0.x is required to compile and run Tinfi.
See http://maven.apache.org for instructions on downloading and using Maven.


2. Introduction
---------------
Tinfi is composed of the following six modules.
- all: the superset of all Tinfi Maven artifacts,
- examples: sample applications and tests,
- extension: extensions to Tinfi,
- mixins: control logic for SCA/Tinfi components,
- module: modules (SCA Assembly Language ADL parser, generators),
- runtime: runtime library.

The following basic sample application is available in the examples module.
- helloworld: the Tinfi Hello World application.

In addition the examples/advanced directory contains some advanced samples. See
the README.txt in the corresponding directories to obtain more information about
these samples.
- foobar-cases: some FooBar examples,
- helloworld-bin: binary components (compiled classes) with Tinfi,
- helloworld-composite: SCA composite implementation,
- helloworld-jdk13: Hello World example for JDK 1.3,
- helloworld-light: light mode (no reflection, no intent controller),
- helloworld-pojo: POJO component implementations,
- helloworld-tinfilet: Tinfilet optimization level source code generator ,
- helloworld-with-intent: policy sets,
- scala: components implemented with Scala (contributed by Romain),
- ultra-merge-helloworld: ultra-merge optimization level.

The extension directory contains some extensions to Tinfi for the following
frameworks:
- osgi: the OSGi Equinox and Felix frameworks.


3. Compiling and installing Tinfi
----------------------------------
To compile and install Tinfi in your Maven local repository, from the root
directory of Tinfi, type:
	mvn install


4. Running the Hello World application
--------------------------------------
To compile and run the helloworld sample application, type:
	cd examples/helloworld
	mvn -Ptinfi:run


5. Compiling and running your own applications
----------------------------------------------
The quickest way for using Tinfi is to mimic the organization of the
examples/helloworld module. The source and the resources of your program should
be put under respectively, src/main/java and src/main/resources.

5.1 Compiling
-------------
A maven plugin is available for compiling your application with Tinfi. As
illustrated in the examples/helloworld module, the plugin can be configured as
follows:

01 <build>
02   <plugins>
03     <plugin>
04       <groupId>org.objectweb.fractal.juliac.plugin</groupId>
05       <artifactId>maven-juliac-plugin</artifactId>
06       <version>2.5</version>
07       <executions>
08         <execution>
09           <id>juliac-compile</id>
10           <phase>generate-sources</phase>
11           <goals><goal>compile</goal></goals>
12         </execution>
13       </executions>
14       <configuration>
15         <srcs><src>src/main/java</src></srcs>
16         <adls><adl>HelloWorld</adl></adls>
17         <modules>
18           <module>JDK6</module>
19           <module>org.ow2.frascati.tinfi.emf.EMFParserSourceCodeGenerator</module>
20           <module>org.ow2.frascati.tinfi.opt.oo.FCOOCtrlSourceCodeGenerator</module>
21         </modules>
22       </configuration>
23       <dependencies>
24         <dependency>
25           <groupId>org.ow2.frascati.tinfi</groupId>
26           <artifactId>frascati-tinfi-oo</artifactId>
27           <version>${project.version}</version>
28         </dependency>
29         <dependency>
30           <groupId>org.ow2.frascati.tinfi</groupId>
31           <artifactId>frascati-tinfi-scaadl</artifactId>
32           <version>${project.version}</version>
33         </dependency>
34       </dependencies>
35     </plugin>
36   </plugins>
37 </build>

Lines 14 to 22 in the <configuration> section particularily matter. The
following tags are supported.
- <srcs> list of directories containing the source code of the application,
- <adls> list of Fractal-ADL types to be compiled by Juliac,
- <modules> list of modules to be loaded by Juliac,
- <artifacts> list of artifacts containing the modules.

5.2 Running
-----------
Once compiled with Tinfi, applications can be runned with the
org.objectweb.fractal.juliac.runtime.Juliac Fractal provider class. The
<tinfi.run.component> and <tinfi.run.interface> properties allow specifying
respectively, the component and the interface to invoke. These properties
configure the tinfi:run profile which is defined in tinfi/pom.xml.

For example, when configured with:
	<tinfi.run.component>HelloWorld</tinfi.run.component>
	<tinfi.run.interface>r</tinfi.run.interface>
the command:
	mvn -Ptinfi:run
launches the HelloWorld component by invoking the interface named r.


6. References
-------------
- Equinox		: http://www.eclipse.org/equinox
- Felix         : http://felix.apache.org
- Fractal  		: http://fractal.ow2.org
- Juliac   		: http://fractal.ow2.org/juliac
- Maven    		: http://maven.apache.org
- OASIS			: http://www.oasis-opencsa.org
- OSGi Alliance	: http://www.osgi.org
- OSOA     		: http://www.osoa.org
- Scala			: http://www.scala-lang.org


For any question, please contact: frascati@ow2.org
Date of creation of this file: 24 June 2007.
Last modified: 21 March 2015.
