/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2011-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.fraclet.extensions.Controller;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.julia.BasicControllerMixin;
import org.objectweb.fractal.julia.UseComponentMixin;
import org.objectweb.fractal.julia.control.binding.BasicBindingControllerMixin;
import org.objectweb.fractal.julia.control.binding.CheckBindingMixin;
import org.objectweb.fractal.julia.control.binding.ContentBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBasicBindingMixin;
import org.objectweb.fractal.julia.control.binding.TypeBindingMixin;
import org.objectweb.fractal.julia.control.content.UseSuperControllerMixin;
import org.ow2.frascati.tinfi.control.binding.CollectionInterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.binding.InterfaceBindingControllerMixin;
import org.ow2.frascati.tinfi.control.content.UseSCAContentControllerMixin;
import org.ow2.frascati.tinfi.opt.oo.SCAPrimitive;

/**
 * Definition of the membrane for SCA primitive components. This membrane
 * implementation differs from the standard one by using the {@link
 * SCAMyPrimitiveBindingControllerImpl} binding controller implementation which
 * does not perform lifecycle checks when binding and unbinding components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.2
 */
@Membrane(desc="mySCAPrimitive")
public class MySCAPrimitive extends SCAPrimitive {
    
    @Controller(
        name="binding-controller",
        impl="SCAMyBindingControllerImpl",
        mixins={
            BasicControllerMixin.class,
            BasicBindingControllerMixin.class,
            TypeBasicBindingMixin.class,
            CollectionInterfaceBindingControllerMixin.class,
            InterfaceBindingControllerMixin.class,
            UseSCAContentControllerMixin.class,
            UseComponentMixin.class,
            CheckBindingMixin.class,
            TypeBindingMixin.class,
            UseSuperControllerMixin.class,
            ContentBindingMixin.class,
//            UseLifeCycleControllerMixin.class,
//            LifeCycleBindingMixin.class
        })
    protected BindingController bc;
}
