package example.hw;

import org.oasisopen.sca.annotation.Init;
import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.objectweb.fractal.juliac.commons.io.Console;

@Service(value=Runnable.class,names="r")
@Membrane(controllerDesc=MySCAPrimitive.class)
public class ClientImpl implements Runnable {

    public ClientImpl() {
        Console console = Console.getConsole("hw-tinfilet-");
        console.println("CLIENT created");
    }
    
    @Init
    public void init() {
        Console console = Console.getConsole("hw-tinfilet-");
        console.println("CLIENT initialized");
    }
    
    public void run() {
        s.print("hello world");
    }
    
    @Reference
    private PrinterItf s;
}
