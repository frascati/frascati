package example.hw;

import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.extensions.Membrane;

@Service(value=Runnable.class,names="r")
@Membrane(controller="scaComposite")
public class HelloWorldImpl implements Runnable {
    public void run() {
        // Indeed nothing. Just an empty container.
    }
}
