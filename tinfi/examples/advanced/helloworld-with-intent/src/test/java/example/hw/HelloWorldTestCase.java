/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.util.Fractal;
import org.osoa.sca.annotations.PolicySets;
import org.ow2.frascati.tinfi.TinfiDomain;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;

/**
 * Automate the launching of the HelloWorld example and check that the example
 * runs as expected.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 0.3
 */
public class HelloWorldTestCase {
    
    private Component client, server;
    
    @Before
    public void setUp()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        IllegalLifeCycleException, NoSuchInterfaceException,
        java.lang.InstantiationException {

        // Instantiate the HelloWorld composite
        Component root = TinfiDomain.getComponent("example.hw.HelloWorld");
        
        // Retrieve the client and server components
        Component[] subs = Fractal.getContentController(root).getFcSubComponents();
        client = subs[0];
        server = subs[1];
    }

    @Test
    public void serverHelloWorld()
    throws
        InstantiationException, NoSuchInterfaceException,
        ClassNotFoundException, java.lang.InstantiationException,
        IllegalAccessException, IllegalLifeCycleException, MyException {
        
        /*
         * Use the Console class to direct the output of the example to a
         * temporary file.
         */
        Console.init(
            System.err,
            new String[]{
                "<< debug1: before print",
                "<< debug2: before",
                "Server: begin printing...",
                "->hello world",
                "Server: print done.",
                "<< debug2: after",
                "<< debug1: after"
            });
        
        /*
         * Create the intent handler components.
         */
        IntentHandler h1 = TinfiDomain.getService("example.hw.handler.PolicySetDebug1",IntentHandler.class,"h");
        IntentHandler h2 = TinfiDomain.getService("example.hw.handler.PolicySetDebug2",IntentHandler.class,"h");
        
        /*
         * Register the intent handlers with the server component. The h1 intent
         * handler is registered with all interfaces of the component. The h2
         * intent handler is registered with all methods of the component
         * implementation annotated with @PolicySets("debug2").
         */
        SCAIntentController ic = (SCAIntentController) server.getFcInterface(SCAIntentController.NAME);
        Fractal.getLifeCycleController(server).stopFc();
        ic.addFcIntentHandler(h1);
        ic.addFcIntentHandler(h2,PolicySets.class,"debug2");
        Fractal.getLifeCycleController(server).startFc();
        
        /*
         * Run the test.
         */
        Service s = (Service) server.getFcInterface("s");
        s.print("hello world");
        
        System.err.println();
    }

    @Test
    public void serverFoo()
    throws
        InstantiationException, NoSuchInterfaceException,
        ClassNotFoundException, java.lang.InstantiationException,
        IllegalAccessException, IllegalLifeCycleException, MyException {
        
        /*
         * Use the Console class to direct the output of the example to a
         * temporary file.
         */
        Console.init(
            System.err,
            new String[]{
                "<< debug1: before print",
                "<< debug2: before",
                "<< MyException thrown with message: foo not supported"
            });
        
        /*
         * Create the intent handler components.
         */
        IntentHandler h1 = TinfiDomain.getService("example.hw.handler.PolicySetDebug1",IntentHandler.class,"h");
        IntentHandler h2 = TinfiDomain.getService("example.hw.handler.PolicySetDebug2",IntentHandler.class,"h");
        
        /*
         * Register the intent handlers with the server component. The h1 intent
         * handler is registered with all interfaces of the component. The h2
         * intent handler is registered with all methods of the component
         * implementation annotated with @PolicySets("debug2").
         */
        SCAIntentController ic = (SCAIntentController) server.getFcInterface(SCAIntentController.NAME);
        Fractal.getLifeCycleController(server).stopFc();
        ic.addFcIntentHandler(h1);
        ic.addFcIntentHandler(h2,PolicySets.class,"debug2");
        Fractal.getLifeCycleController(server).startFc();
        
        /*
         * Run the test.
         */
        Service s = (Service) server.getFcInterface("s");
        s.print("foo");
        
        System.err.println();
    }

    @Test
    public void clientHelloWorld()
    throws
        InstantiationException, NoSuchInterfaceException,
        ClassNotFoundException, java.lang.InstantiationException,
        IllegalAccessException, IllegalLifeCycleException, MyException,
        NoSuchInterfaceException {
        
        /*
         * Use the Console class to direct the output of the example to a
         * temporary file.
         */
        Console.init(
            System.err,
            new String[]{
                "<< debug1: before print",
                "Server: begin printing...",
                "->hello world",
                "Server: print done.",
                "<< debug1: after",
                "Server returned: true"
            });
        
        /*
         * Create the intent handler component.
         */
        IntentHandler h1 =
            TinfiDomain.getService(
                "example.hw.handler.PolicySetDebug1",IntentHandler.class,"h");
        
        /*
         * Register the intent handler with the s interface of the client
         * component.
         */
        SCAIntentController ic = (SCAIntentController) client.getFcInterface(SCAIntentController.NAME);
        Fractal.getLifeCycleController(client).stopFc();
        ic.addFcIntentHandler(h1,"s");
        Fractal.getLifeCycleController(client).startFc();
        
        /*
         * Run the test.
         */
        Service s = (Service) client.getFcInterface("s");
        s.print("hello world");
        
        System.err.println();
    }
}
