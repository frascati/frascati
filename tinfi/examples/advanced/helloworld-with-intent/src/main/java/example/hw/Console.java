/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import java.io.PrintStream;

/**
 * This class provides a way for printing characters to a {@link PrintStream}
 * while comparing with some expected characters.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public class Console {
    
    // ---------------------------------------------------------------------
    // Public static interface for initializing and retrieving the singleton
    // instance of this class
    // ---------------------------------------------------------------------

    /** Singleton instance of this class. */
    private static Console console;
    
    /**
     * Initialize the console.
     * 
     * @param ps        the {@link PrintStream} where characters are outputted
     * @param expected  the expected characters
     */
    public static void init( PrintStream ps, String[] expected ) {
        console = new Console(ps,expected);
    }
    
    /**
     * Return the singleton instance of this class.
     * 
     * @throws IllegalStateException
     *      if the instance has not been initialized with
     *      {@link #init(PrintStream, String[])}
     */
    public static Console get() throws IllegalStateException {
        if( console == null ) {
            String msg = "Console has not been initialized";
            throw new IllegalStateException(msg);
        }
        return console;
    }

    
    // ---------------------------------------------------------------------
    // Constructor and instance variables
    // ---------------------------------------------------------------------

    private PrintStream ps;
    private String[] expected;
    
    private Console( PrintStream ps, String[] expected ) {
        this.ps = ps;
        this.expected = expected;
    }

    
    // ---------------------------------------------------------------------
    // Printing methods
    // ---------------------------------------------------------------------

    public void println( String s ) {
        print(s);
        print('\n');
    }

    public void print( String s ) {
        for( int i=0 ; i < s.length() ; i++ ) {
            print(s.charAt(i));
        }
    }
    
    public void print( char c ) {
        
        // Manage line and column indexes
        if( c == '\n' ) {
            line++;
            col=1;
        }
        else {
            // Check whether there are still some expected characters
            if( ei >= expected.length ) {
                fail();
            }
            
            // Get the expected character
            char e = expected[ei].charAt(ej);
            
            // Compare with the actual character
            if( c != e ) {
                fail();
            }

            // Move the indexes to the next expected character
            ej++;
            if( ej >= expected[ei].length() ) {
                ei++;
                ej = 0;
            }
            
            // Move to the next column
            col++;
        }
        
        // Propagate the printing of the character
        ps.print(c);        
    }
    
    private int ei = 0;
    private int ej = 0;
    private int line = 1;
    private int col = 1;

    
    // ---------------------------------------------------------------------
    // Notify a difference between expected and actual characters
    // ---------------------------------------------------------------------

    /**
     * @throws IllegalArgumentException
     *      to notify a difference between the expected and the actual character
     */
    private void fail() throws IllegalArgumentException {
        
        ps.println();
        ps.println();
        ps.println("Expected output is: ");
        for (String expect : expected) {
            ps.println(expect);
        }
        
        String msg = "Unexpected character at line "+line+", column "+col;
        throw new IllegalArgumentException(msg);
    }
}
