package example.hw;

import org.osoa.sca.annotations.PolicySets;
import org.osoa.sca.annotations.Property;

@PolicySets("debug1")
public class ServerImpl implements Service {
    
    private String header = "->";
    private int count = 1;
    
    @PolicySets("debug2")
    public boolean print(final String msg) throws MyException {
        
        if( msg.equals("foo") ) {
            throw new MyException("foo not supported");
        }
        
        Console.get().println("Server: begin printing...");
        for (int i = 0 ; i < (count) ; ++i) {
            Console.get().println(((header) + msg));
        }
        Console.get().println("Server: print done.");
        return true;
    }
    
    public String getHeader() {
        return header;
    }
    
    @Property
    public void setHeader(final String header) {
        this.header = header;
    }
    
    public int getCount() {
        return count;
    }
    
    @Property
    public void setCount(final int count) {
        this.count = count;
    }
}