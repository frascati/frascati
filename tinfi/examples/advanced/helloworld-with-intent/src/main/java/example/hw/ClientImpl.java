package example.hw;

import org.osoa.sca.annotations.Reference;

public class ClientImpl implements Runnable {

    public void run() {
        try {
            boolean ret = s.print("hello world");
            Console.get().println("Server returned: "+ret);
            s.print("foo");
        }
        catch (MyException e) {}
    }
    
    @Reference
    private Service s;
}
