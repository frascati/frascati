package example.hw;


public interface Service {
    boolean print(String msg) throws MyException;
}