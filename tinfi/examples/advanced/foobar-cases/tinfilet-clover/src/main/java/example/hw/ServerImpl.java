package example.hw;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.juliac.commons.io.Console;

@Service(value=PrinterItf.class,names="s")
public class ServerImpl implements PrinterItf {
    
    private String header;
    private int count;
    
    public ServerImpl() {
        Console console = Console.getConsole("hw-tinfilet-");
        console.println("SERVER created");
    }
    
    public void print( String msg ) {
        Console console = Console.getConsole("hw-tinfilet-");
        console.println("Server: begin printing...");
        for (int i = 0 ; i < count ; ++i) {
            console.println(header+msg);
        }
        console.println("Server: print done.");
    }
    
    @Property
    public void setHeader( String header ) {
        this.header = header;
    }
    
    @Property
    public void setCount( int count ) {
        this.count = count;
    }
}
