package example.hw;

import org.oasisopen.sca.annotation.Service;

/*
 * This component implementation is defined to test code generation with Juliac
 * both in src/main/java (Maven phase generate-sources) and in src/test/java
 * (Maven phase generate-test-sources.)
 */

@Service(names="r",value=Runnable.class)
public class ServerTestImpl implements Runnable {

    public void run() {}
}
