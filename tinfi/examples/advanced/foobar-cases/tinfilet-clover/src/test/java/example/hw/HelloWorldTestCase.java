/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package example.hw;

import junit.framework.TestCase;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalContentException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.juliac.commons.io.Console;
import org.objectweb.fractal.juliac.runtime.Factory;
import org.objectweb.fractal.util.Fractal;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Automate the launching of the HelloWorld example and check that the example
 * runs as expected.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.1
 */
public class HelloWorldTestCase extends TestCase {

    @Test
    @SuppressWarnings("unchecked")
    public void testHelloWorld()
    throws
        ClassNotFoundException, InstantiationException, IllegalAccessException,
        InstantiationException, java.lang.InstantiationException,
        NoSuchInterfaceException, IllegalContentException,
        IllegalLifeCycleException, IllegalBindingException {

        // Load factory classes
        Class<Factory> hwfcl =
            (Class<Factory>) Class.forName("example.hw.HelloWorldImplFactory");
        Class<Factory> cltfcl =
            (Class<Factory>) Class.forName("example.hw.ClientImplFactory");
        Class<Factory> srvfcl =
            (Class<Factory>) Class.forName("example.hw.ServerImplFactory");
        
        // Instantiate factory classes
        Factory hwf = hwfcl.newInstance();
        Factory cltf = cltfcl.newInstance();
        Factory srvf = srvfcl.newInstance();
        
        // Instantiate components
        Component hw = hwf.newFcInstance();
        Component clt = cltf.newFcInstance();
        Component srv = srvf.newFcInstance();
        
        // Add clt and srv in hw
        ContentController cc = Fractal.getContentController(hw);
        cc.addFcSubComponent(clt);
        cc.addFcSubComponent(srv);
        
        // Bind hw to clt and clt to srv
        Fractal.getBindingController(hw).bindFc("r",clt.getFcInterface("r"));
        Fractal.getBindingController(clt).bindFc("s",srv.getFcInterface("s"));
        
        // Set properties associated with srv
        SCAPropertyController scapc = (SCAPropertyController)
            srv.getFcInterface(SCAPropertyController.NAME);
        scapc.setValue("header","-> ");
        scapc.setValue("count",1);
        
        // Direct the output of the example to a temporary file
        Console console = Console.getConsole("hw-tinfilet-");
        
        // Invoke the run() operation on the component
        LifeCycleController lc = Fractal.getLifeCycleController(hw);
        Runnable r = (Runnable) hw.getFcInterface("r");
        lc.startFc();
        r.run();
        lc.stopFc();
        
        // Compare the result with the expected result
        console.close();
        console.dump(System.err);
        console.assertEquals(expecteds);
    }
    
    final static private String[] expecteds =
        new String[]{
            "CLIENT created",
            "CLIENT initialized",
            "SERVER created",
            "Server: begin printing...",
            "-> hello world",
            "Server: print done.",
        };
}
