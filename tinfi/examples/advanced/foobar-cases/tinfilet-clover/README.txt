This example illustrates the use of the Juliac and Clover Maven plugins with the
Tinfilet optimization level source code generator. This example has been added
to avoid regressions following a problem reported by Nicolas Salatge from Petals
Link.

To compile and run the example:
	mvn test


References
----------
- Clover		http://www.atlassian.com/software/clover
