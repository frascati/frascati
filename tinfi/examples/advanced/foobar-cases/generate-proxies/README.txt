Test code generation for a specified interface signature and controller
descriptor.
Feature introduced in Juliac 2.3.1.
