Test various issues related with handling parameters when generating proxy
classes.

* inner types: check that inner types are generated as outter.inner (which is
  the source code syntax) and not as outter$inner (which the binary form) which
  was the case prior to Tinfi 0.3.1/Juliac 2.0.1.

* throws clause: check that thrown exceptions with exactly the type Exception
  (not a subclass) do not generate compile errors because the catch clause can
  never be reached. This bug is fixed by Tinfi 0.4 which relies on if statements
  to propagate caught exceptions instead of catch clauses.
