package example.proxyparameters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class CImpl<WW extends Runnable & Comparable<? super WW>,
                   KK extends Serializable,
                   ZZ>
implements
    I<WW,KK,ZZ>, ImplementationFactory<Implementation>,
    MetamodelProcessor<EPackage> {

    @SuppressWarnings("unused")
    public CImpl() throws Throwable {}
    
    public void foo1(Param.Inner pi) {}
    public byte[] foo2(Object o) { return null; }
    public void foo3() {}
    public float foo4() { return 0.0f; }

    I<WW,KK,ZZ> impl;

    public <B,
            R extends J<B>,
            E extends Throwable,
            V extends Serializable & Comparable<V>,
            T extends Comparable<? super T>>
    R cast( B target, V[] param) throws E, Throwable {
        if (target == null) {
            @SuppressWarnings("unchecked")
            E e = (E) new Exception();
            throw e;
        }
        impl.cast(target, param);
        return null;
    }

    public <B> B[] cast(Collection<B> c, List<Integer> l, Set<?> s) {
        return null;
    }

    public <B,R extends J<B>> R cast( B target ) throws IllegalArgumentException {
        return impl.cast(target);
    }

    public void foo() {}
    public Object[] bar( J.Inner ji ) { return null; }

    public ArrayList<String> sub1() { return null; }
    public List<String> sub2() { return null; }
    public String sub3() { return null; }

    public void createImplementation(Implementation implementation) {}

    public EPackage getEPackage() { return null; }
}
