package example.proxyparameters;

import org.osoa.sca.annotations.Service;

/**
 * @author Philippe Merle <philippe.merle@inria.fr>
 * @since 1.3
 */
@Service
public interface MetamodelProcessor<PackageType extends EPackage> {
    public PackageType getEPackage();
}

interface EPackage {}
