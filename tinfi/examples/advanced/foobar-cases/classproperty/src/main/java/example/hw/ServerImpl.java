package example.hw;

import org.oasisopen.sca.annotation.Property;

public class ServerImpl implements ClassItf {

    @Property(name="classprop")
    private Class<?> cl;
    
    public Class<?> getClassProperty() {
        return cl;
    }
}
