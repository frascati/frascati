package example.hw;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;

@Scope("COMPOSITE")
public class ServerImpl implements PrinterItf {
    
    private PrinterItf s2;    
    private String header;
    private int count;
    
    public ServerImpl() {}
    
    public void print(final String msg) {
        String header = getHeader();
        Console.println("Server: begin printing...");
        for (int i = 0 ; i < (count) ; ++i) {
            Console.println(header + msg);
        }
        Console.println("Server: print done.");
        s2.print(msg);
    }
    
    @Reference
    public void setS2( PrinterItf s2 ) {
        this.s2 = s2;
    }
    
    public String getHeader() {
        return header;
    }
    
    @Property
    public void setHeader(final String header) {
        this.header = header;
    }
    
    public int getCount() {
        return count;
    }
    
    @Property
    public void setCount(final String count) {
        this.count = Integer.valueOf(count);
    }
}