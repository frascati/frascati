package example.hw;

import org.oasisopen.sca.annotation.Scope;

import example.hw.clt.AbstractClient;

@Scope("COMPOSITE")
public class ClientImpl extends AbstractClient {

    public ClientImpl() {}    

    public void run() {
        super.run();
    }
}