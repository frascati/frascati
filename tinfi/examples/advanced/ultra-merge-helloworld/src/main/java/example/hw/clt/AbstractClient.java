package example.hw.clt;

import java.util.ArrayList;
import java.util.List;

import org.oasisopen.sca.annotation.Reference;

import example.hw.PrinterItf;

public class AbstractClient implements Runnable {

    protected String header2 = "foobar";

    public void run() {
        for (PrinterItf m : ms) {
            m.print("hello world");
        }
    }
    
    @Reference(name="servers")
    public List<PrinterItf> ms = new ArrayList<PrinterItf>();
}
