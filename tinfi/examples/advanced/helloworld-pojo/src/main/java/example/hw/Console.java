package example.hw;

import java.io.PrintStream;

public class Console {

    public static PrintStream ps = System.err;
    
    public static void println( String msg ) {
        ps.println(msg);
    }
    
    public static void print( String msg ) {
        ps.print(msg);
    }
}
