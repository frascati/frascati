/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Implementation of the {@link SCAPropertyController} control interface shared
 * by scaPrimitive and scaComposite components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public abstract class SCAPropertyControllerMixin
implements SCAPropertyController {
    
    private Map<String,Object> values = new HashMap<String,Object>();
    private Map<String,Class<?>> types = new HashMap<String,Class<?>>();
    
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    public void setType( String name, Class<?> type ) {
        types.put(name,type);
    }
    
    public void setValue( String name, Object value )
    throws IllegalArgumentException {
        
        // Record the new value
        values.put(name,value);
        
        // Re-inject the value on current content instances
        _super_setValue(name,value);
    }
    
    public Class<?> getType( String name ) {
        if( types.containsKey(name) ) {
            Class<?> type = types.get(name);
            return type;
        }
        else {
            Class<?> type = _super_getType(name);
            return type;
        }
    }
    
    public Object getValue( String name ) {
        
        Object value = values.get(name);
        
        Object v = _super_getValue(name);
        if( v == null ) {
            // The values stored in the different content instances differ
            return value;
        }
        
        if( v != value ) {
            /*
             *  The value stored in the content instances differs from the one
             *  stored in the property controller. Assume that the value from
             *  the content instances has been updated by the implementation of
             *  the component and is more up to date. Record it as the new one.
             */
            values.put(name,v);
        }
        
        return v;
    }
    
    public boolean containsPropertyName( String name ) {
        boolean b = values.containsKey(name);
        if(!b) {
            b = _super_containsPropertyName(name);
        }
        return b;
    }

    public String[] getPropertyNames() {
        /*
         * The set returned by keySet() does not provide an implementation for
         * method add which is used by method addAll.
         */
        Set<String> keys = new HashSet<String>(values.keySet());
        String[] declared = _super_getPropertyNames();
        keys.addAll( Arrays.asList(declared) );
        String[] names = keys.toArray( new String[keys.size()] );
        return names;
    }
    
    public boolean hasBeenSet( String name ) {
        boolean b = values.containsKey(name);
        return b;
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    protected abstract void _super_setValue( String name, Object value )
    throws IllegalArgumentException;
    protected abstract Object _super_getValue( String name );
    protected abstract Class<?> _super_getType( String name );
    protected abstract String[] _super_getPropertyNames();
    protected abstract boolean _super_containsPropertyName( String name );
}
