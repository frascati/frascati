/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2013-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding;

import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.control.IllegalBindingException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.InterfaceType;

/**
 * This mixin layer removes collection interface instances from the map of
 * bindings.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.6
 */
public abstract class CollectionInterfaceBindingControllerMixin
implements BindingController {

    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------
    
    public void unbindFc( String clientItfName )
    throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException {

        Interface itf = (Interface) _this_weaveableC.getFcInterface(clientItfName);
        InterfaceType it = (InterfaceType) itf.getFcItfType();
        
        if( it.isFcCollectionItf() ) {
            _this_fcBindings.remove(clientItfName);
        }
        else {
            _super_unbindFc(clientItfName);
        }
    }

    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    private Component _this_weaveableC;
    private Map<String,Object> _this_fcBindings;

    abstract protected void _super_unbindFc( String clientItfName )
    throws NoSuchInterfaceException, IllegalBindingException, IllegalLifeCycleException;
}
