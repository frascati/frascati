/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.binding;

import org.oasisopen.sca.ServiceReference;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.BindingController;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;

/**
 * Provides a basic implementation of the {@link BindingController} interface.
 * This mixin manages the references hold by client interfaces.
 * 
 * Since Tinfi 1.4.6, this mixin is no longer used by Tinfi itself (the
 * implementation of the binding controller for scaPrimitive and scaComposite
 * membranes has been fixed). Yet this mixin is still used by the easy*
 * membranes of EasyViper and is thus kept here for this purpose.
 * 
 * TODO the easy* membranes should be updated as this has been the case for
 * scaPrimitive and scaComposite.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public abstract class BasicBindingControllerMixin implements BindingController {

    // -------------------------------------------------------------------------
    // Implementation of the BindingController interface
    // -------------------------------------------------------------------------

    public String[] listFc() {
        
        Object[] itfs = _this_weaveableC.getFcInterfaces();
        int size = 0;
        for (Object o : itfs) {
            Interface itf = (Interface)o;
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            if( it.isFcClientItf() ) {
                size++;
            }
        }
        
        String[] names = new String[size];
        int i = 0;
        for (Object o : itfs) {
            Interface itf = (Interface)o;
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            if( it.isFcClientItf() ) {
                names[i] = itf.getFcItfName();
                i++;
            }
        }
        
        return names;
    }

    public Object lookupFc( final String clientItfName )
    throws NoSuchInterfaceException {
        
        ComponentInterface ci = (ComponentInterface)
            _this_weaveableC.getFcInterface(clientItfName);
        
        // Skip client interceptor
        Interceptor interceptor = (Interceptor) ci.getFcItfImpl();
        Object dst = interceptor.getFcItfDelegate();
        
        return dst;
    }

    public void bindFc( final String clientItfName, final Object serverItf )
    throws NoSuchInterfaceException {
        
        ComponentInterface ci = (ComponentInterface)
            _this_weaveableC.getFcInterface(clientItfName);
        
        // Skip client interceptor
        Interceptor interceptor = (Interceptor) ci.getFcItfImpl();
        interceptor.setFcItfDelegate(serverItf);
        
        // Inject reference into content instances
        TinfiComponentOutInterface<?> tci =
            (TinfiComponentOutInterface<?>) ci;
        ServiceReference<?> sr = tci.getServiceReference();
        _this_weaveableSCACC.setReferenceValue(clientItfName,sr);
    }

    public void unbindFc( final String clientItfName )
    throws NoSuchInterfaceException {
        
        ComponentInterface ci = (ComponentInterface)
            _this_weaveableC.getFcInterface(clientItfName);
        
        // Skip client interceptor
        Interceptor interceptor = (Interceptor) ci.getFcItfImpl();
        interceptor.setFcItfDelegate(null);
        
        // Inject null into content instances
        _this_weaveableSCACC.unsetReferenceValue(clientItfName);
    }

    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    public Component _this_weaveableC;
    public SCAExtendedContentController _this_weaveableSCACC;
}
