/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.oasisopen.sca.ServiceReference;
import org.oasisopen.sca.annotation.Property;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Mixin layer for implementing the {@link SCAExtendedContentController}
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
public abstract class SCALightContentControllerMixin
implements SCAExtendedContentController {
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAContentController interface
    // -------------------------------------------------------------------------
    
    public void setFcContentClass( Class<?> c )
    throws IllegalLifeCycleException, ContentInstantiationException {
        /*
         * This method is not supported by this version of the SCA Content
         * Controller.
         */
        throw new UnsupportedOperationException();
    }

    public Class<?> getFcContentClass() {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public Object getFcContent() throws ContentInstantiationException {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }

    public void setFcContent( Object content )
    throws IllegalLifeCycleException, ContentInstantiationException {
        String msg =
            "setFcContent(Object) can only be invoked for composite-scoped "+
            "components";
        throw new ContentInstantiationException(msg);
    }

    
    // -------------------------------------------------------------------------
    // Implementation of the SCAExtendedContentController interface
    // -------------------------------------------------------------------------
    
    public void releaseFcContent( Object content, boolean isEndMethod ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public void eagerInit() throws ContentInstantiationException {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         * 
         * We do not throw UnsupportedOperationException as in other similar
         * methods in this class: this method is invoked in all cases, i.e.
         * whether the component is eager initialized or not, by {@link
         * SCALifeCycleMixin#setFcContentState(boolean)}.
         */
    }
    
    public void start() {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }

    public void stop() {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }

    public boolean containsPropertyName( String name ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public String[] getPropertyNames() {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public Class<?> getPropertyType( String name ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }

    public void setPropertyValue( String name, Object value ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public Object getPropertyValue( String name ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    public void setReferenceValue( String name, ServiceReference<?> value ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }

    public void unsetReferenceValue( String name ) {
        /*
         * This method is implemented by generated subclasses. Yet we keep it
         * here as the mixed super class (SCAContentControllerImpl) cannot be
         * declared abstract. It remains to be seen under which conditions we
         * could get rid of this mixed super class.
         */
        throw new UnsupportedOperationException();
    }
    
    
    // -------------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------------

    /**
     * Return the reference or property values associated with the specified
     * names. This method is used by generated subclasses to retrieve the values
     * for invoking @{@link Property}-annotated constructor parameters.
     * 
     * @param contentClassName  the content class
     * @param initNames         the reference or property names
     * @return                  the corresponding values
     */
    protected Object[] getInitValues(
        String contentClassName, String... initNames )
    throws ContentInstantiationException {
        
        ComponentType ct = (ComponentType) _this_weaveableC.getFcType();
        
        Object[] values = new Object[initNames.length];
        for (int i = 0 ; i < initNames.length ; i++) {
            String name = initNames[i];
            try {
                InterfaceType it = ct.getFcInterfaceType(name);
                Object sr = getServiceReference(it);
                values[i] = sr;
            }
            catch( NoSuchInterfaceException nsie ) {
                // Not a reference. Check whether this is a property.
                if( ! _this_weaveableSCAPC.containsPropertyName(name) ) {
                    // Neither a reference, nor a property
                    String msg =
                        "Identifier "+name+
                        " in @Constructor attributes for class "+
                        contentClassName+
                        " is neither a reference name, nor a property name";
                    throw new ContentInstantiationException(msg);
                }
                values[i] = _this_weaveableSCAPC.getValue(name);
            }
        }
        
        return values;
    }
    
    /**
     * Return a list for the interfaces associated with the specified collection
     * interface type name.
     * 
     * @param name  the collection interface type name
     * @return      the associated interfaces
     */
    protected List<Object> getCollectionInterfacesAsList( String name ) {
        Object[] itfs = _this_weaveableC.getFcInterfaces();
        List<Object> srs = new ArrayList<Object>();
        for (Object o : itfs) {
            Interface itf = (Interface) o;
            String itfname = itf.getFcItfName();
            if( itfname.startsWith(name) ) {
                Object sr = getServiceReference(itf);
                srs.add(sr);
            }
        }
        return srs;
    }

    /**
     * Return a map for the interfaces associated with the specified collection
     * interface type name.
     * 
     * @param name  the collection interface type name
     * @return      the associated interfaces
     */
    protected Map<String,Object> getCollectionInterfacesAsMap( String name ) {
        Object[] itfs = _this_weaveableC.getFcInterfaces();
        Map<String,Object> srs = new HashMap<String,Object>();
        for (Object o : itfs) {
            Interface itf = (Interface) o;
            String itfname = itf.getFcItfName();
            if( itfname.startsWith(name) ) {
                Object sr = getServiceReference(itf);
                srs.put(itfname,sr);
            }
        }
        return srs;
    }

    /**
     * Return the {@link ServiceReference} (or the list of {@link
     * ServiceReference}s) associated to the specified {@link InterfaceType}.
     */
    private Object getServiceReference( InterfaceType it )
    throws NoSuchInterfaceException {
        
        String name = it.getFcItfName();
        if( it.isFcCollectionItf() ) {
            Object[] itfs = _this_weaveableC.getFcInterfaces();
            Map<String,Object> srs = new HashMap<String,Object>();
            for (Object o : itfs) {
                Interface itf = (Interface) o;
                String itfname = itf.getFcItfName();
                if( itfname.startsWith(name) ) {
                    Object sr = getServiceReference(itf);
                    srs.put(itfname,sr);
                }
            }
            return srs;
        }
        else {
            Object itf = _this_weaveableC.getFcInterface(name);
            Object sr = getServiceReference(itf);
            return sr;
        }
    }
    
    /**
     * Return a {@link ServiceReference} corresponding to the specified client
     * interface or <code>null</code> if the client interface is not bound.
     */
    protected static Object getServiceReference( Object clientItf ) {
        ComponentInterface itf = (ComponentInterface) clientItf;
        Interceptor outInterceptor = (Interceptor) itf.getFcItfImpl();
        Object delegate = outInterceptor.getFcItfDelegate();
        if( delegate == null ) {
            /*
             * Unbound client interface. At this point this means that this is
             * an optional interface. Mandatory unbound interfaces are detected
             * when starting the component.
             */
            return null;
        }
        TinfiComponentOutInterface<?> tcoi =
            (TinfiComponentOutInterface<?>) clientItf;
        ServiceReference<?> sr = tcoi.getServiceReference();
        return sr;
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    protected abstract void _super_initFcController(InitializationContext ic)
    throws InstantiationException;
    
    private Component _this_weaveableC;
    private SCAPropertyController _this_weaveableSCAPC;
}
