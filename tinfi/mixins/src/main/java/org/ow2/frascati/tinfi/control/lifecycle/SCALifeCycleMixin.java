/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.lifecycle;

import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.julia.control.lifecycle.LifeCycleCoordinator;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;

/**
 * Mixin layer for managing the EagerInit, the Start and the Stop policies.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public abstract class SCALifeCycleMixin implements LifeCycleCoordinator {
    
    // -------------------------------------------------------------------------
    // Implementation of the LifeCycleCoordinator interface
    // -------------------------------------------------------------------------

    public boolean setFcStarted() throws IllegalLifeCycleException {
        synchronized (this) {
            boolean b = _super_setFcStarted();
            if (b) {
                try {
                    _this_weaveableSCACC.eagerInit();
                    _this_weaveableSCACC.start();
                }
                catch( ContentInstantiationException ie ) {
                    IllegalLifeCycleException icle =
                        new IllegalLifeCycleException(ie.getMessage());
                    icle.initCause(ie);
                    throw icle;
                }
            }
            return b;
        }
    }

    public boolean setFcStopped() throws IllegalLifeCycleException {
        synchronized (this) {
            boolean b = _super_setFcStopped();
            if (b) {
                try {
                    _this_weaveableSCACC.stop();
                }
                catch( ContentInstantiationException ie ) {
                    IllegalLifeCycleException icle =
                        new IllegalLifeCycleException(ie.getMessage());
                    icle.initCause(ie);
                    throw icle;
                }
            }
            return b;
        }
    }


    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    public abstract boolean _super_setFcStarted() throws IllegalLifeCycleException;
    public abstract boolean _super_setFcStopped() throws IllegalLifeCycleException;
    public SCAExtendedContentController _this_weaveableSCACC;
}
