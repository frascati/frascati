/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.intent;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.juliac.commons.lang.ClassHelper;
import org.objectweb.fractal.juliac.commons.lang.reflect.AnnotatedElementFilter;
import org.objectweb.fractal.juliac.commons.util.function.Predicate;
import org.objectweb.fractal.juliac.commons.util.function.Filters;
import org.objectweb.fractal.juliac.runtime.ClassLoaderFcItf;
import org.objectweb.fractal.juliac.runtime.ClassLoaderItf;
import org.objectweb.fractal.juliac.runtime.Juliac;
import org.ow2.frascati.tinfi.TinfiComponentInterceptor;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;

/**
 * Mixin layer for implementing the {@link SCAIntentController} interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 0.3
 */
public abstract class SCAIntentControllerMixin
implements Controller, SCAIntentController {
    
    // -------------------------------------------------------------------------
    // Private constructor
    // -------------------------------------------------------------------------

    private SCAIntentControllerMixin () {}

    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------

    /**
     * Initialize the fields of this mixin and then calls the overriden method.
     *
     * @param ic information about the component to which this controller object
     *      belongs.
     * @throws InstantiationException if the initialization fails.
     */
    public void initFcController( final InitializationContext ic )
    throws InstantiationException {
        
        cl = (Class<?>) ic.content;

        /*
         * Propagate the invocation to other mixin layers.
         */
        _super_initFcController(ic);
    }
    
    private Class<?> cl;
    
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAIntentController interface
    // -------------------------------------------------------------------------
    
    /**
     * <p>
     * Add the specified intent handler on all service methods of the current
     * component which satisfy the following conditions:
     * </p>
     * 
     * <ul>
     * <li>the implementation method in the component class is associated with
     *     an annotation of the specified class,</li>
     * <li>the annotation provides a <code>String[] value()</code> method,</li>
     * <li>one of the returned strings when calling <code>value()</code>is equal
     *     to the specified value.</li>
     * </ul>
     * 
     * @param handler  the intent handler to add
     * @param annotcl  the searched for annotation class
     * @param value    the searched for parameter value
     */
    public <T extends Annotation>
        void addFcIntentHandler(
            IntentHandler handler, Class<T> annotcl, String value ) {
        
        List<Method> methods = getFcMatchingMethods(annotcl,value);
        for (Method method : methods) {
            TinfiComponentInterceptor<?> tci = getFcInterceptor(method);
            Method m = getFcItfMethod(method);
            try {
                tci.addIntentHandler(handler,m);
            }
            catch (NoSuchMethodException e) {
                /*
                 * Shoudln't occur. m has been computed to be a method of the
                 * interface managed by the interceptor.
                 */
                throw new TinfiRuntimeException(e);
            }
        }
    }

    
    // -------------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------------
    
    /**
     * <p>
     * Return the list of methods implemented by the component class which
     * satisfy the following conditions:
     * </p>
     * 
     * <ul>
     * <li>the method is associated with an annotation of the specified class,</li>
     * <li>the annotation provides a <code>String[] value()</code> method,</li>
     * <li>one of the returned strings when calling <code>value()</code> is
     *     equal to the specified value.</li>
     * </ul>
     */
    private <T extends Annotation>
        List<Method> getFcMatchingMethods( Class<T> annotcl, String value ) {
        
        List<Method> ret = new ArrayList<Method>();
        
        // Retrieve all methods annotated with annotcl
        Method[] methods = ClassHelper.getAllMethods(cl);
        Predicate<AnnotatedElement> filter =
            new AnnotatedElementFilter(annotcl.getName());
        Method[] ms = Filters.filter(methods,filter);
        
        for (Method method : ms) {
            
            // Retrieve the corresponding annotation
            T annot = method.getAnnotation(annotcl);
            
            // Check whether the annotation provides a value() method
            if( annot != null ) {
                Class<?> cl = annot.getClass();
                try {
                    Method mvalue = cl.getMethod("value");
                    String[] values = null;
                    try {
                        // Invoke value()
                        values = (String[]) mvalue.invoke(annot);
                    }
                    catch (Exception e) {
                        throw new TinfiRuntimeException(e);
                    }
                    
                    // Check that one of the returned values is equal to value
                    for (String v : values) {
                        if( v.equals(value) ) {
                            ret.add(method);
                        }
                    }
                }
                catch (NoSuchMethodException e) {
                    // No such method value()
                }
            }
        }
        
        return ret;
    }

    /**
     * Return the interceptor for the specified component method.
     */
    private TinfiComponentInterceptor<?> getFcInterceptor( Method method ) {
        if( mtcis == null ) {
            initFcMaps();
        }
        if( ! mtcis.containsKey(method) ) {
            /*
             * Shouldn't occur.
             */
            String msg =
                "No interceptor for component method: "+method.toString();
            throw new TinfiRuntimeException(msg);
        }
        TinfiComponentInterceptor<?> tci = mtcis.get(method);
        return tci;
    }
    
    /**
     * Return the interface method for the specified component method.
     */
    private Method getFcItfMethod( Method method ) {
        if( mms == null ) {
            initFcMaps();
        }
        if( ! mms.containsKey(method) ) {
            /*
             * Shouldn't occur.
             */
            String msg =
                "No interface method for component method: "+method.toString();
            throw new TinfiRuntimeException(msg);
        }
        Method m = mms.get(method);
        return m;
    }
    
    private Map<Method,TinfiComponentInterceptor<?>> mtcis;
    private Map<Method,Method> mms;
    
    /**
     * Initialize the {@link #mtcis} and {@link #mms} maps.
     */
    private void initFcMaps() {
        
        mtcis = new HashMap<Method,TinfiComponentInterceptor<?>>();
        mms = new HashMap<Method,Method>();

        /*
         * Retrieve all public methods of the component class.
         */
        Method[] methods = cl.getMethods();

        /*
         * Iterate on all business interfaces of the component.
         */
        Object[] itfs = _this_weaveableOptC.getFcInterfaces();
        for (Object o : itfs) {
            Interface itf = (Interface) o;
            String name = itf.getFcItfName();
            
            // Skip control interfaces
            if( name.endsWith("-controller") || name.equals("component") ) {
                continue;
            }
            
            // Retrieve the corresponding interceptor
            Object i = ((ComponentInterface)itf).getFcItfImpl();
            TinfiComponentInterceptor<?> tci = (TinfiComponentInterceptor<?>) i;
            
            // Retrieve the interface methods
            InterfaceType it = (InterfaceType) itf.getFcItfType();
            String signature = it.getFcItfSignature();
            Class<?> business = loadFcClass(signature);
            Method[] ms = business.getMethods();
            
            // Check if some component methods match the interface methods
            for (Method method : methods) {
                for (Method m : ms) {
                    if( areSame(m,method) ) {
                        mtcis.put(method,tci);
                        mms.put(method,m);
                    }
                }
            }
        }
    }

    /**
     * Load, with the Juliac classloader the class whose name is specified.
     */
    private static Class<?> loadFcClass( String name ) {
        
        Component boot = new Juliac().newFcInstance();
        ClassLoaderItf loader = null;
        try {
            loader = (ClassLoaderItf)
                boot.getFcInterface(ClassLoaderFcItf.NAME);
        }
        catch( NoSuchInterfaceException nsie ) {
            /*
             * Shouldn't occur since Juliac provides a classloader interface.
             */
            throw new RuntimeException(nsie);
        }

        try {
            Class<?> cl = loader.loadClass(name);
            return cl;
        }
        catch( ClassNotFoundException cnfe ) {
            throw new TinfiRuntimeException(cnfe);
        }
    }
    
    /**
     * Return <code>true</code> if the specified methods are the same: same name
     * and same parameters.
     */
    private static boolean areSame( Method m1, Method m2 ) {
        
        String name1 = m1.getName();
        String name2 = m2.getName();
        if( ! name1.equals(name2) ) {
            return false;
        }
        
        Class<?>[] ptypes1 = m1.getParameterTypes();
        Class<?>[] ptypes2 = m2.getParameterTypes();
        if( ptypes1.length != ptypes2.length ) {
            return false;
        }
        
        for (int i = 0; i < ptypes1.length; i++) {
            Class<?> ptype1 = ptypes1[i];
            Class<?> ptype2 = ptypes2[i];
            if( ! ptype1.equals(ptype2) ) {
                return false;
            }
        }
        
        return true;
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    /**
     * The <tt>weaveableOptC</tt> field required by this mixin. This field is
     * supposed to reference the {@link Component} interface of the component to
     * which this controller object belongs.
     */
    public Component _this_weaveableOptC;

    /**
     * The {@link Controller#initFcController initFcController} method overriden
     * by this mixin.
     *
     * @param ic information about the component to which this controller object
     *      belongs.
     * @throws InstantiationException if the initialization fails.
     */
    public abstract void _super_initFcController( InitializationContext ic )
    throws InstantiationException;    
}
