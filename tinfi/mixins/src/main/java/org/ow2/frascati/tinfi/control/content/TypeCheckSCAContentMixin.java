/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2012-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.content;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Type;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.api.control.SCAContentController;

/**
 * Type checking management.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.5
 */
public abstract class TypeCheckSCAContentMixin
implements SCAContentController {
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAContentController interface
    // -------------------------------------------------------------------------
    
    public void setFcContentClass( Class<?> c )
    throws IllegalLifeCycleException, ContentInstantiationException {
        Type type = _this_weaveableC.getFcType();
        checkType(type,c);
        _super_setFcContentClass(c);        
    }
    
    public void setFcContent( Object content )
    throws IllegalLifeCycleException, ContentInstantiationException {
        Type type = _this_weaveableC.getFcType();
        Class<?> c = content.getClass();
        checkType(type,c);
        _super_setFcContent(content);
    }

    
    // -------------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------------
    
    private void checkType( Type type, Class<?> contentClass )
    throws ContentInstantiationException {
        
        /*
         * Check that the content class implements the server interfaces.
         */
        ComponentType ct = (ComponentType) type;
        InterfaceType[] its = ct.getFcInterfaceTypes();
        for (InterfaceType it : its) {
            
            // Skip client interfaces
            if( it.isFcClientItf() ) {
                continue;
            }
            
            // Skip control interfaces
            String name = it.getFcItfName();
            if( name.equals("component") || name.endsWith("-controller") ) {
                continue;
            }
            
            /*
             * Workaround since Object.class.getClassLoader() returns null.
             * Object.class as a content class is not a priori a useful case,
             * but it uses it in test cases, and anyway the workaround is needed
             * if Object.class is used elsewhere.
             */
            String signature = it.getFcItfSignature();
            if( contentClass.equals(Object.class) ) {
                String msg =
                    "Object.class is not adequate since the content class "+
                    "should implement "+signature;
                throw new ContentInstantiationException(msg);
            }
            
            // Check that the class implements the server interface
            Class<?> cl = load(contentClass,signature);
            if( ! cl.isAssignableFrom(contentClass) ) {
                String msg = contentClass+" should implement "+signature;
                throw new ContentInstantiationException(msg);
            }
        }
        
    }
    
    /**
     * Load the class whose name is specified with the class loader of the
     * specified class.
     * 
     * @param main  the class used to retrieve the class loader
     * @param name  the name of the class to load
     * @throws ContentInstantiationException  if the class cannot be loader
     */
    private Class<?> load( Class<?> main, String name )
    throws ContentInstantiationException {
        ClassLoader loader = main.getClassLoader();
        try {
            return loader.loadClass(name);
        }
        catch( ClassNotFoundException cnfe ) {
            throw new ContentInstantiationException(cnfe);
        }
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    protected abstract void _super_setFcContentClass(Class<?> c)
    throws IllegalLifeCycleException, ContentInstantiationException;
    protected abstract void _super_setFcContent(Object content)
    throws IllegalLifeCycleException, ContentInstantiationException;
    
    private Component _this_weaveableC;
}
