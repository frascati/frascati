/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 *
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.control.content;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.oasisopen.sca.ServiceReference;
import org.oasisopen.sca.annotation.Property;
import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.factory.InstantiationException;
import org.objectweb.fractal.api.type.ComponentType;
import org.objectweb.fractal.api.type.InterfaceType;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Controller;
import org.objectweb.fractal.julia.InitializationContext;
import org.objectweb.fractal.julia.Interceptor;
import org.objectweb.fractal.juliac.commons.ipf.CompositeInjectionPointHashMap;
import org.objectweb.fractal.juliac.commons.ipf.InjectionPoint;
import org.ow2.frascati.tinfi.TinfiComponentOutInterface;
import org.ow2.frascati.tinfi.TinfiRuntimeException;
import org.ow2.frascati.tinfi.api.control.ContentInstantiationException;
import org.ow2.frascati.tinfi.control.content.scope.CompositeScopeManager;
import org.ow2.frascati.tinfi.control.content.scope.ScopeManager;
import org.ow2.frascati.tinfi.control.content.scope.StatelessScopeManager;
import org.ow2.frascati.tinfi.oasis.ServiceReferenceImpl;
import org.ow2.frascati.tinfi.osoa.ConversationScopeManager;
import org.ow2.frascati.tinfi.osoa.RequestScopeManager;

/**
 * Mixin layer for implementing the {@link SCAExtendedContentController}
 * interface.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 */
public abstract class SCAContentControllerMixin
implements Controller, SCAExtendedContentController {
    
    /**
     * The metadata for the content class associated to the component controlled
     * by the current controller.
     */
    private ContentClassMetaData ccmd;
    
    /**
     * The scope manager for the component controlled by the current controller.
     */
    private ScopeManager sm;

    /**
     * The properties declared by the content class associated with this
     * controller. The index is the property name and the value is the property
     * type.
     */
    private Map<String,Class<?>> props = new HashMap<String,Class<?>>();

    
    // -------------------------------------------------------------------------
    // Implementation of the Controller interface
    // -------------------------------------------------------------------------

    public void initFcController( final InitializationContext ic )
    throws InstantiationException {
        
        /*
         * Propagate the invocation to other mixin layers.
         */
        _super_initFcController(ic);
        
        /*
         * Initialize this layer.
         * Do it after the propagation in order for _this_XXX fields to be
         * initialized.
         */
        Class<?> c = (Class<?>) ic.content;
        try {
            innerSetFcContentClass(c);
        }
        catch (IllegalContentClassMetaData eccmd) {
            InstantiationException ie =
                new InstantiationException(eccmd.getMessage());
            ie.initCause(eccmd);
            throw ie;
        }
    }
  
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAContentController interface
    // -------------------------------------------------------------------------
    
    public void setFcContentClass( Class<?> c )
    throws IllegalLifeCycleException, ContentInstantiationException {
        
        try {
            // Content class meta data and scope manager retrievals
            innerSetFcContentClass(c);
        }
        catch (IllegalContentClassMetaData e) {
            throw new ContentInstantiationException(e);
        }
    }

    public Class<?> getFcContentClass() {
        return ccmd.fcContentClass;
    }
    
    public Object getFcContent() throws ContentInstantiationException {
        return sm.getFcContent();
    }

    public void setFcContent( Object content )
    throws IllegalLifeCycleException, ContentInstantiationException {

        // Set the new content instance
        sm.setFcContent(content);
    }

    
    // -------------------------------------------------------------------------
    // Implementation of the SCAExtendedContentController interface
    // -------------------------------------------------------------------------
    
    public void releaseFcContent( Object content, boolean isEndMethod ) {
        sm.releaseFcContent(content,isEndMethod);

        //
        // See issue http://jira.ow2.org/browse/FRASCATI-105
        //
        // Unset the request context to avoid a memory leak.
        // This memory leak appears on the log files of Apache Tomcat 6.0.x.
        // 
        setRequestContext(null);        
    }
    
    public void eagerInit() throws ContentInstantiationException {
        
        if( ! ccmd.eagerinit ) {
            return;
        }
        
        if( !(sm instanceof CompositeScopeManager) ) {
            String msg =
                "Eager initialization is meaningless in scopes other than "+
                "COMPOSITE (scope is: "+ccmd.scope+")";
            throw new ContentInstantiationException(msg);
        }
        
        // Retrieve the content to trigger its initialization
        sm.getFcContent();
    }
    
    public void start() throws ContentInstantiationException {
        
        if( ccmd.startMethod == null ) {
            return;
        }
        
        Object[] contents = sm.getFcCurrentContents();
        for (Object content : contents) {
            try {
                ccmd.startMethod.invoke(content);
            }
            catch (IllegalAccessException e) {
                throw new ContentInstantiationException(e);
            }
            catch (InvocationTargetException e) {
                throw new ContentInstantiationException(e);
            }
        }
    }

    public void stop() throws ContentInstantiationException {
        
        if( ccmd.stopMethod == null ) {
            return;
        }
        
        Object[] contents = sm.getFcCurrentContents();
        for (Object content : contents) {
            try {
                ccmd.stopMethod.invoke(content);
            }
            catch (IllegalAccessException e) {
                throw new ContentInstantiationException(e);
            }
            catch (InvocationTargetException e) {
                throw new ContentInstantiationException(e);
            }
        }
    }

    public boolean containsPropertyName( String name ) {
        boolean b = props.containsKey(name);
        return b;
    }
    
    public String[] getPropertyNames() {
        Set<String> propnames = props.keySet();
        String[] names = propnames.toArray( new String[propnames.size()] );
        return names;
    }
    
    public Class<?> getPropertyType( String name ) {
        Class<?> type = props.get(name);
        return type;
    }

    public void setPropertyValue( String name, Object value ) {
        
        InjectionPoint<?> ip = ccmd.props.get(name);
        if( ip == null ) {
            // No such property. Do nothing.
            return;
        }
        
        Object[] contents = sm.getFcCurrentContents();        
        for (Object content : contents) {
            set(ip,content,value);
        }
    }
    
    public Object getPropertyValue( String name ) {
        
        InjectionPoint<?> ip = ccmd.props.get(name);
        if( ip == null ) {
            // No such property. Do nothing.
            return null;
        }
        
        Object[] contents = sm.getFcCurrentContents();
        if( contents.length == 0 ) {
            return null;
        }
        
        Object value = get(ip,contents[0]);
        for ( int i=1 ; i<contents.length ; i++ ) {
            Object content = contents[i];
            Object v = get(ip,content);
            if( v != value ) {
                return null;
            }
        }

        return value;
    }
    
    public void setReferenceValue( String name, ServiceReference<?> value ) {

        InjectionPoint<?> ip = getInjectionPoint(name);
        if( ip == null ) {
            /*
             * No injection point. This may be the case for references which are
             * injected with the @Constructor annotation. See for example
             * ConstructorClientImpl. Do nothing.
             */
            return;
        }
        
        Class<?> iptype = ip.getType();
        Object[] contents = sm.getFcCurrentContents();        
        for (Object content : contents) {
            
            if( List.class.isAssignableFrom(iptype) ) {
                
                // SCA style collection reference
                
                List<ServiceReference<?>> srs =
                    (List<ServiceReference<?>>) get(ip,content);
                
                /*
                 * If a service reference has already been recorded for the same
                 * name, we don't need to add it again. The binding may have
                 * changed but in this case, this is the delegate field of the
                 * component output interceptor which has been updated. The
                 * service reference object which is injected in the injection
                 * point does not need to be changed. The chain service
                 * reference > component interface > component interceptor is
                 * still valid.
                 */
                for (ServiceReference<?> sr : srs) {
                    ServiceReferenceImpl<?> sri = (ServiceReferenceImpl<?>)sr;
                    Object o = sri._getDelegate();
                    Interface itf = (Interface) o;
                    String itfname = itf.getFcItfName();
                    if( itfname.equals(name) ) {
                        return;
                    }
                }
                
                srs.add(value);
                
                /*
                 * Re-inject the reference of the list even if this is not
                 * needed. This follows a discussion with Philippe on 7 March
                 * 2013 that needs a way to be notified that something has
                 * changed. Reinjecting has the side-effect of invoking the
                 * setter (in the case the injection point corresponds to a
                 * setter/getter), and enables to be notified that something has
                 * changed in the list of references.
                 */
                set(ip,content,srs);
            }
            else if( Map.class.isAssignableFrom(iptype) ) {
                
                // Fraclet style collection reference
                
                Map<String,ServiceReference<?>> srs =
                    (Map<String,ServiceReference<?>>) get(ip,content);
                srs.put(name,value);
                
                // Same comment as for SCA style collection references
                set(ip,content,srs);
            }
            else {
                // Singleton reference
                set(ip,content,value);
            }
        }
    }

    public void unsetReferenceValue( String name ) {
        
        InjectionPoint<?> ip = getInjectionPoint(name);
        if( ip == null ) {
            /*
             * No injection point. This may be the case for references which are
             * injected with the @Constructor annotation. See for example
             * ConstructorClientImpl. Do nothing.
             */
            return;
        }
        
        Class<?> iptype = ip.getType();        
        Object[] contents = sm.getFcCurrentContents();        
        for (Object content : contents) {
            
            if( List.class.isAssignableFrom(iptype) ) {
                
                // SCA style collection reference
                
                List<ServiceReference<?>> srs =
                    (List<ServiceReference<?>>) get(ip,content);
                
                ServiceReference<?> toBeRemoved = null;
                for (ServiceReference<?> sr : srs) {
                    ServiceReferenceImpl<?> sri = (ServiceReferenceImpl<?>)sr;
                    Object o = sri._getDelegate();
                    Interface itf = (Interface) o;
                    String itfname = itf.getFcItfName();
                    if( itfname.equals(name) ) {
                        toBeRemoved = sr;
                    }
                }
                
                srs.remove(toBeRemoved);
                
                /*
                 * Re-inject the reference of the list even if this is not
                 * needed. This follows a discussion with Philippe on 7 March
                 * 2013 that needs a way to be notified that something has
                 * changed. Reinjecting has the side-effect of invoking the
                 * setter (in the case the injection point corresponds to a
                 * setter/getter), and enables to be notified that something has
                 * changed in the list of references.
                 */
                set(ip,content,srs);
            }
            else if( Map.class.isAssignableFrom(iptype) ) {
                
                // Fraclet style collection reference
                
                Map<String,ServiceReference<?>> srs =
                    (Map<String,ServiceReference<?>>) get(ip,content);
                srs.remove(name);
                
                // Same comment as for SCA style collection references
                set(ip,content,srs);
            }
            else {
                // Singleton reference
                set(ip,content,null);
            }
        }
    }
    
    
    // -------------------------------------------------------------------------
    // Implementation specific
    // -------------------------------------------------------------------------

    /**
     * Declare the content class which should be used.
     * 
     * This operation can only be performed if the component is stopped.
     * This method is synchronized to prevent inconsistent concurrent changes.
     * 
     * @since 1.0
     */
    private synchronized void innerSetFcContentClass( Class<?> c )
    throws IllegalContentClassMetaData {
        
        /*
         * Content class meta data and scope manager retrievals.
         */
        ccmd = ContentClassMetaData.get(c);
        
        String scope = ccmd.scope==null ? null : ccmd.scope.toUpperCase();
        if( scope == null || scope.equals("STATELESS") ) {
            sm = new StatelessScopeManager(_this_weaveableC,ccmd);
        }
        else if( scope.equals("REQUEST") ) {
            sm = new RequestScopeManager(_this_weaveableC,ccmd);
        }
        else if( scope.equals("COMPOSITE") ) {
            sm = new CompositeScopeManager(_this_weaveableC,ccmd);
        }
        else if( scope.equals("CONVERSATION") ) {
            sm = new ConversationScopeManager(_this_weaveableC,ccmd);
        }
        else {
            String msg = "Unsupported scope: "+ccmd.scope;
            throw new IllegalContentClassMetaData(msg);
        }
        
        /*
         * Initialize property names and types.
         * 
         * Firstly, search in the @Property annotated elements (fields and
         * setters.)
         */
        props = new HashMap<String,Class<?>>();
        CompositeInjectionPointHashMap ipm = ccmd.props;
        Set<String> ipnames = ipm.keySet();
        for (String ipname : ipnames) {
            InjectionPoint<?> ip = ipm.get(ipname);
            Class<?> type = ip.getType();
            props.put(ipname,type);
        }
        
        /*
         * Secondly, search in the @Property annotated parameters of the
         * @Constructor annotated constructor.
         */
        Constructor<?> ctr = ccmd.constructorAnnotatedElement;
        if( ctr != null ) {
                        
            Class<?>[] ptypes = ctr.getParameterTypes();
            Annotation[][] psannots = ctr.getParameterAnnotations();            
            for (int i = 0; i < ptypes.length; i++) {
                // By definition, ptypes.length == psannots.length
                Class<?> ptype = ptypes[i];
                Annotation[] pannots = psannots[i];
                for (Annotation pannot : pannots) {
                    if( pannot instanceof Property ) {
                        Property prop = (Property) pannot;
                        String propname = prop.name();
                        props.put(propname,ptype);
                    }
                }
            }
        }
    }
    
    private InjectionPoint<?> getInjectionPoint( String name ) {
        /*
         * Do not mimic setPropertyValue which iterates on ccmd.props and
         * iterate on ccmd.refs. The reason is that one may find the case where
         * the component defines e.g. an opt singleton interface and an opts
         * collection interfaces. When searching e.g. for opts0 both injection
         * point will match and one may ends up with opt instead of opts. 
         */
        
        ComponentType ct = (ComponentType) _this_weaveableC.getFcType();
        InterfaceType it = null;
        try {
            it = ct.getFcInterfaceType(name);
        }
        catch (NoSuchInterfaceException e) {
            // No such interface, neither singleton, nor collection.
            return null;
        }
        String itname = it.getFcItfName();
        
        /*
         * For singleton interfaces, itname equals name.
         * For collection interfaces, itname equals the prefix of the collection
         * for name.
         */
        InjectionPoint<?> ip = ccmd.refs.get(itname);
        return ip;
    }
    
    /**
     * Wraps exception thrown by {@link InjectionPoint#get(Object)} into {@link
     * TinfiRuntimeException}s.
     * 
     *  @since 1.3.1
     */
    private static Object get( InjectionPoint<?> ip, Object content )
    throws TinfiRuntimeException {
        
        try {
            return ip.get(content);
        }
        catch (IllegalAccessException e) {
            throw new TinfiRuntimeException(e);
        }
        catch (InvocationTargetException e) {
            throw new TinfiRuntimeException(e);
        }
    }
    
    /**
     * Wraps exception thrown by {@link InjectionPoint#set(Object, Object)} into
     * {@link TinfiRuntimeException}s.
     * 
     *  @since 1.3.1
     */
    private static void set( InjectionPoint<?> ip, Object content, Object value )
    throws TinfiRuntimeException {
        
        try {
            ip.set(content,value);
        }
        catch (IllegalAccessException e) {
            throw new TinfiRuntimeException(e);
        }
        catch (InvocationTargetException e) {
            throw new TinfiRuntimeException(e);
        }
    }
    
    /**
     * Return a {@link ServiceReference} corresponding to the specified client
     * interface or <code>null</code> if the client interface is not bound.
     * 
     * @since 1.4.5
     */
    protected static Object getServiceReference( Object clientItf ) {
        ComponentInterface itf = (ComponentInterface) clientItf;
        Interceptor outInterceptor = (Interceptor) itf.getFcItfImpl();
        Object delegate = outInterceptor.getFcItfDelegate();
        if( delegate == null ) {
            /*
             * Unbound client interface. At this point this means that this is
             * an optional interface. Mandatory unbound interfaces are detected
             * when starting the component.
             */
            return null;
        }
        TinfiComponentOutInterface<?> tcoi =
            (TinfiComponentOutInterface<?>) clientItf;
        ServiceReference<?> sr = tcoi.getServiceReference();
        return sr;
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    protected abstract void _super_initFcController(InitializationContext ic)
    throws InstantiationException;
    
    private Component _this_weaveableC;
}
