/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.tinfi.control.property;

import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Implementation of the {@link SCAPropertyController} control interface for
 * scaComposite components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @author Philippe Merle <Philippe.Merle@inria.fr>
 * @since 1.1.2
 */
public abstract class SCACompositePropertyControllerMixin
implements SCAPropertyController {
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    public boolean containsPropertyName( String name ) {
        // No content class for a composite, hence no declared property
        return false;
    }
    
    public String[] getPropertyNames() {
        // No content class for a composite, hence no declared property
        return new String[0];
    }
    
    /**
     * @since 1.4.2
     */
    public boolean isDeclared( String name ) {
        // No content class for a composite, hence no declared property
        return false;
    }
}
