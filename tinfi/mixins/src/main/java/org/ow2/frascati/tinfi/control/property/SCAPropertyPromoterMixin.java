/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2010-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.objectweb.fractal.api.control.NameController;
import org.objectweb.fractal.julia.ComponentInterface;
import org.objectweb.fractal.julia.Interceptor;
import org.ow2.frascati.tinfi.api.control.IllegalPromoterException;
import org.ow2.frascati.tinfi.api.control.SCAPropertyController;

/**
 * Layer implementing the property promotion mechanism for the SCA property
 * controller. This mechanism is available with Tinfi since version 0.4.3, but
 * the code has been modularized in this layer only since version 1.4.2.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4.2
 */
public abstract class SCAPropertyPromoterMixin
implements SCAPropertyController {
    
    /**
     * Property name indexed map of promoters.
     */
    private Map<String,SCAPropertyController> promoters =
        new HashMap<String,SCAPropertyController>();
    
    /**
     * Property name indexed map of property names defined at the level of their
     * promoter.
     * 
     * @since 1.4
     */
    private Map<String,String> promoterPropNames = new HashMap<String,String>();

    
    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    public void setType( String name, Class<?> type ) {
        
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            _super_setType(name,type);
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            promoter.setType(promoterPropName,type);
        }
    }
    
    public void setValue( String name, Object value )
    throws IllegalArgumentException {
        
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            _super_setValue(name,value);
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            promoter.setValue(promoterPropName,value);
        }
    }
    
    public Class<?> getType( String name ) {
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            Class<?> type = _super_getType(name);
            return type;
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            Class<?> type = promoter.getType(promoterPropName);
            return type;
        }
    }
    
    public Object getValue( String name ) {
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            Object value = _super_getValue(name);
            return value;
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            Object value = promoter.getValue(promoterPropName);
            return value;
        }
    }
    
    public boolean containsPropertyName( String name ) {
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            boolean b = _super_containsPropertyName(name);
            return b;
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            boolean b = promoter.containsPropertyName(promoterPropName);
            return b;
        }
    }

    public String[] getPropertyNames() {
        String[] declared = _super_getPropertyNames();
        Set<String> keys = new HashSet<String>(promoters.keySet());
        keys.addAll( Arrays.asList(declared) );
        String[] names = keys.toArray( new String[keys.size()] );
        return names;
    }

    public boolean hasBeenSet( String name ) {
        SCAPropertyController promoter = promoters.get(name);
        if( promoter == null ) {
            boolean b = _super_hasBeenSet(name);
            return b;
        }
        else {
            String promoterPropName = promoterPropNames.get(name);
            boolean b = promoter.hasBeenSet(promoterPropName);
            return b;
        }
    }

    public void setPromoter( String name, SCAPropertyController promoter )
    throws IllegalPromoterException {
        
        promoters.put(name,promoter);
        promoterPropNames.put(name,name);

        SCAPropertyController peer = promoter.getPromoter(name);
        if( peer != null ) {
            /*
             * There is already a promoter. Check for simple cycles.
             * TODO Check for cycles of length > 2 ?
             */
            Object peerInterceptor = ((ComponentInterface)peer).getFcItfImpl();
            Object peerImpl = ((Interceptor)peerInterceptor).getFcItfDelegate();
            if( peerImpl == this ) {
                String compname = _this_weaveableOptNC.getFcName();
                throw new IllegalPromoterException(compname);
            }
        }
    }
    
    public SCAPropertyController getPromoter( String name ) {
        return promoters.get(name);
    }

    public void setPromoter(
        String name, SCAPropertyController promoter,
        String promoterPropertyName )
    throws IllegalPromoterException {
        
        setPromoter(name,promoter);
        promoterPropNames.put(name,promoterPropertyName);
    }
    
    public String getPromoterPropertyName( String name ) {
        return promoterPropNames.get(name);
    }
    
    public void removePromoter( String name ) {
        promoters.remove(name);
        promoterPropNames.remove(name);
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    protected abstract void _super_setType( String name, Class<?> type );
    protected abstract void _super_setValue( String name, Object value )
    throws IllegalArgumentException;
    protected abstract Class<?> _super_getType( String name );
    protected abstract Object _super_getValue( String name );
    protected abstract boolean _super_containsPropertyName( String name );
    protected abstract boolean _super_hasBeenSet( String name );
    protected abstract String[] _super_getPropertyNames();
    
    private NameController _this_weaveableOptNC;
}
