/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2008-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.intent;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.objectweb.fractal.api.NoSuchInterfaceException;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;
import org.objectweb.fractal.api.control.NameController;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.InterfaceFilter;
import org.ow2.frascati.tinfi.api.InterfaceMethodFilter;
import org.ow2.frascati.tinfi.api.control.SCAIntentController;

/**
 * Provides lifecycle related checks to a {@link SCAIntentController}. Check
 * that the component is stopped and then call the overriden method.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 0.3
 */

public abstract class LifeCycleIntentMixin implements SCAIntentController {

    // -----------------------------------------------------------------------
    // Implementation of the SCAIntentController interface
    // -----------------------------------------------------------------------

    public <T extends Annotation>
        void addFcIntentHandler(
            IntentHandler handler, Class<T> annotcl, String value )
    throws IllegalLifeCycleException {
        checkFcStopped();
        _super_addFcIntentHandler(handler,annotcl,value);
    }

    // -----------------------------------------------------------------------
    // Implementation of the SCABasicIntentController interface
    // -----------------------------------------------------------------------

    public void addFcIntentHandler( IntentHandler handler )
    throws IllegalLifeCycleException {
        checkFcStopped();
        _super_addFcIntentHandler(handler);
    }

    public void addFcIntentHandler(
        IntentHandler handler, InterfaceFilter filter )
    throws IllegalLifeCycleException {
        checkFcStopped();
        _super_addFcIntentHandler(handler,filter);        
    }
    
    public void addFcIntentHandler(
        IntentHandler handler, InterfaceMethodFilter filter )
    throws IllegalLifeCycleException {
        checkFcStopped();
        _super_addFcIntentHandler(handler,filter);        
    }
    
    public void addFcIntentHandler( IntentHandler handler, String name )
    throws NoSuchInterfaceException, IllegalLifeCycleException {
        checkFcStopped();
        _super_addFcIntentHandler(handler,name);
    }
    
    public void addFcIntentHandler(
        IntentHandler handler, String name, Method method )
    throws
        NoSuchInterfaceException, NoSuchMethodException,
        IllegalLifeCycleException {
        
        checkFcStopped();
        _super_addFcIntentHandler(handler,name,method);        
    }

    public void removeFcIntentHandler( IntentHandler handler )
    throws IllegalLifeCycleException {
        checkFcStopped();
        _super_removeFcIntentHandler(handler);
    }

    public void removeFcIntentHandler( IntentHandler handler, String name )
    throws NoSuchInterfaceException, IllegalLifeCycleException {
        checkFcStopped();
        _super_removeFcIntentHandler(handler,name);
    }

    public void removeFcIntentHandler(
        IntentHandler handler, String name, Method method )
    throws
        NoSuchInterfaceException, NoSuchMethodException,
        IllegalLifeCycleException {
        
        checkFcStopped();
        _super_removeFcIntentHandler(handler,name,method);
    }
    
    // -----------------------------------------------------------------------
    // Implementation specific
    // -----------------------------------------------------------------------

    private void checkFcStopped() throws IllegalLifeCycleException {
        if( _this_weaveableOptLC != null ) {
            String state = _this_weaveableOptLC.getFcState();
            if( ! LifeCycleController.STOPPED.equals(state) ) {
                String msg =
                    "Component " + _this_weaveableOptNC.getFcName() +
                    " is not stopped";
                throw new IllegalLifeCycleException(msg);
            }
        }
    }
    
    // -----------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -----------------------------------------------------------------------

    public NameController _this_weaveableOptNC;
    public LifeCycleController _this_weaveableOptLC;

    public abstract <T extends Annotation>
        void _super_addFcIntentHandler(
            IntentHandler handler, Class<T> annotcl, String value );

    public abstract void _super_addFcIntentHandler( IntentHandler handler );
    public abstract void _super_addFcIntentHandler( IntentHandler handler, InterfaceFilter filter );
    public abstract void _super_addFcIntentHandler( IntentHandler handler, InterfaceMethodFilter filter );
    public abstract void _super_addFcIntentHandler( IntentHandler handler, String name );
    public abstract void _super_addFcIntentHandler( IntentHandler handler, String name, Method method );

    public abstract void _super_removeFcIntentHandler( IntentHandler handler );
    public abstract void _super_removeFcIntentHandler( IntentHandler handler, String name );
    public abstract void _super_removeFcIntentHandler( IntentHandler handler, String name, Method method );
}
