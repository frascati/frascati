/***
 * OW2 FraSCAti Tinfi
 * Copyright (C) 2007-2018 Inria, Univ. Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Lionel Seinturier
 */

package org.ow2.frascati.tinfi.control.property;

import org.ow2.frascati.tinfi.api.control.SCAPropertyController;
import org.ow2.frascati.tinfi.control.content.SCAExtendedContentController;

/**
 * Implementation of the {@link SCAPropertyController} control interface for
 * scaPrimitive components.
 * 
 * @author Lionel Seinturier <Lionel.Seinturier@univ-lille1.fr>
 * @since 1.4
 */
public abstract class SCAPrimitivePropertyControllerMixin
implements SCAPropertyController {
    
    // -------------------------------------------------------------------------
    // Implementation of the SCAPropertyController interface
    // -------------------------------------------------------------------------

    public void setValue( String name, Object value )
    throws IllegalArgumentException {

        // Re-inject the value on current content instances
        _this_weaveableSCACC.setPropertyValue(name,value);
    }
    
    public Object getValue( String name ) {
        return null;
    }
        
    public boolean isDeclared( String name ) {
        boolean b = _this_weaveableSCACC.containsPropertyName(name);
        return b;
    }
    
    public boolean containsPropertyName( String name ) {
        boolean b = _this_weaveableSCACC.containsPropertyName(name);
        return b;
    }
    
    public String[] getPropertyNames() {
        String[] names = _this_weaveableSCACC.getPropertyNames();
        return names;
    }
    
    public Class<?> getType( String name ) {
        Class<?> type = _this_weaveableSCACC.getPropertyType(name);
        return type;
    }
    
    
    // -------------------------------------------------------------------------
    // Fields and methods required by the mixin class in the base class
    // -------------------------------------------------------------------------

    private SCAExtendedContentController _this_weaveableSCACC;
}
