# MySQL Navigator Xport
# Database: store
# admin@localhost

DROP DATABASE IF EXISTS store;
CREATE DATABASE store;
USE store;

DROP TABLE IF EXISTS album;
DROP TABLE IF EXISTS artist;

#
# Table structure for table 'artist'
#

CREATE TABLE `artist` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Table structure for table 'album'
#

CREATE TABLE `album` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(30) NOT NULL,
  `genre` varchar(30),
  `year` int(11),
  `artist_id` int(11) NOT NULL,
  `price` float NOT NULL default '15',
  `image` varchar(30),
  PRIMARY KEY  (`id`),
  KEY `artist_fk_constraint` (`artist_id`),
  CONSTRAINT `artist_fk_constraint` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Dumping data for table 'artist'
#

INSERT INTO artist VALUES (1,'James Blunt');
INSERT INTO artist VALUES (2,'Ayo');
INSERT INTO artist VALUES (3,'Bruce Springsteen');
INSERT INTO artist VALUES (4,'Ben Harper');


#
# Dumping data for table 'album'
#

INSERT INTO album(title, genre, year, artist_id, price,image) VALUES ('All_The_Lost_Souls','Rock',2007,1,12.99,'blunt.jpeg');
INSERT INTO album(title, genre, year, artist_id, price,image) VALUES ('Joyful','Soul',2007,2,11.95,'ayo.jpg');
INSERT INTO album(title, genre, year, artist_id, price,image) VALUES ('Magic','Rock',2007,3,9.99,'bruce_magic.jpg');
INSERT INTO album(title, genre, year, artist_id, price,image) VALUES ('Lifeline','Rock',2007,4,9.99,'harper.jpg');

