package launch;

import org.apache.tuscany.sca.host.embedded.SCADomain;

public class Run implements Runnable {

  public void run() {
    
    synchronized (this) {
      System.out.println("Starting ...");
      SCADomain scaDomain = SCADomain.newInstance("catalog.composite");
      System.out.println("catalog.composite ready for big business !!");
      
      while (true) {
        try {
          System.in.read();
          wait();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      
    }

  }

}
