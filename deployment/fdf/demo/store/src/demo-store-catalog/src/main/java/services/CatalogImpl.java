/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.    
 */

package services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.derby.tools.sysinfo;
import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Service;

@Service(Catalog.class)
public class CatalogImpl implements Catalog {

  protected Connection con;

  @Property
  public String databaseUrl = "jdbc:mysql://hoeve:7080/store";
    
  @Property
  public String currencyCode = "USD";

  @Init
  public void init() {

  }

  public synchronized String[][] get() {
    System.out.println("Retreiving Catalog...");
    
    String [][] catalogArray = null;
    
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
      con = DriverManager.getConnection(databaseUrl,
          "admin", "admin");

      if (!con.isClosed())
        System.out.println("Successfully connected to "
            + "MySQL server using TCP/IP...");

      Statement stmt = con.createStatement();
      ResultSet rs = stmt
          .executeQuery("select album.image, artist.name, album.title, album.genre, album.year, album.price from album, artist where artist.id = album.id");
      
      rs.last();
      int rsSize = rs.getRow();
      rs.beforeFirst();
      
      catalogArray = new String [6][rsSize];
      
      while (rs.next()) {
        for(int i=0; i<6 ;i++) {
          String s = rs.getString(i+1);
          catalogArray [i][rs.getRow()-1] = s;
        }
      }
      
      stmt.close();
      rs.close() ;
      con.close() ;
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    
//    System.out.println(catalog.size());
//    System.out.println(catalogArray.length);
    return catalogArray;
  }
}
