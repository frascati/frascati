<!--
    * Licensed to the Apache Software Foundation (ASF) under one
    * or more contributor license agreements.  See the NOTICE file
    * distributed with this work for additional information
    * regarding copyright ownership.  The ASF licenses this file
    * to you under the Apache License, Version 2.0 (the
    * "License"); you may not use this file except in compliance
    * with the License.  You may obtain a copy of the License at
    * 
    *   http://www.apache.org/licenses/LICENSE-2.0
    * 
    * Unless required by applicable law or agreed to in writing,
    * software distributed under the License is distributed on an
    * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    * KIND, either express or implied.  See the License for the
    * specific language governing permissions and limitations
    * under the License.    
-->

<%@ page import="org.apache.tuscany.sca.host.embedded.SCADomain"%>
<%@ page import="services.ProxyService"%>
<%@ page import="services.Catalog"%>
<%@ page import="converter.CurrencyConverter"%>
<%@ page import="java.rmi.Naming"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%!public int i;%>


<%
          SCADomain scaDomain = (SCADomain) application
          .getAttribute("org.apache.tuscany.sca.SCADomain");
      //Catalog catalog = (Catalog) Naming.lookup("rmi://localhost:2000/cat");
      //CatalogProxyService catalog = (CatalogProxyService) Naming.lookup("rmi://localhost:2050/CatalogProxy");
      ProxyService proxy = (ProxyService)scaDomain.getService(ProxyService.class, "Proxy");
      //CurrencyConverter converter = (CurrencyConverter) Naming.lookup("rmi://localhost:3000/converter");
%>

<html>

<head>
<title>Store</TITLE>

<script type="text/javascript" src="binding-atom.js"></script>
<script language="JavaScript">

	//Reference
	
	//catalog = (new JSONRpcClient("http://localhost:8090/Catalog")).Catalog;
	
	//Reference
	
	shoppingCart = new AtomClient("../store/ShoppingCart");
	
	function shoppingCart_getResponse(feed) {
		if (feed != null) {
			var entries = feed.getElementsByTagName("entry");              
			var list = "";
			for (var i=0; i<entries.length; i++) {
				var item = entries[i].getElementsByTagName("content")[0].firstChild.nodeValue;
				list += item + ' <br>';
			}
			document.getElementById("shoppingCart").innerHTML = list;
			document.getElementById('total').innerHTML = feed.getElementsByTagName("subtitle")[0].firstChild.nodeValue;
		}
	}
	function shoppingCart_postResponse(entry) {
		shoppingCart.get("", shoppingCart_getResponse);
	}				


	function addToCart() {
		var items  = document.catalogForm.items;
		var j = 0;
		for (var i=0; i<items.length; i++)
			if (items[i].checked) {
				var entry = '<entry xmlns="http://www.w3.org/2005/Atom"><title>cart-item</title><content type="text">'+items[i].value+'</content></entry>'
				shoppingCart.post(entry, shoppingCart_postResponse);
				items[i].checked = false;
			}
	}
	function checkoutCart() {
		document.getElementById('store').innerHTML='<h2>' +
				'Thanks for Shopping With Us!</h2>'+
				'<h2>Your Order</h2>'+
				'<form name="orderForm" action="/store/store.jsp">'+
					document.getElementById('shoppingCart').innerHTML+
					'<br>'+
					document.getElementById('total').innerHTML+
					'<br>'+
					'<br>'+
					'<input type="submit" value="Continue Shopping">'+ 
				'</form>';
		shoppingCart.delete("", null);
	}
	
	function deleteCart() {
		shoppingCart.delete("", null);
		document.getElementById('shoppingCart').innerHTML = "";
		document.getElementById('total').innerHTML = "";	
	}	

</script>

<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
<div id="title">
<h1>Music Store</h1>
</div>
 <div id="note">
  <img src="img/icon.png"  style="width: 150px; height: 150px;" />
 </div>
<div id="store">
<h2>Catalog</h2>
<form name="catalogForm">
<div id="catalog">
<table width="80%" align="center">
	<tr>
		<td width="3"></td>
		<td width="3"></td>
		<td width="20%">Artist</td>
		<td width="25%">Album</td>
		<td>genre</td>
		<td>year</td>
		<td>price</td>
		<%
		      String[][] items = proxy.get();
		      String currency = "USD";

		      for (int i = 0; i < items[0].length; i++) {
		        out
		            .write("<tr><a><td width=\"3\"><input name=\"items\" type=\"checkbox\" value=\"");
		        out.print(items[2][i]);
		        out.write(" - ");
		        out.print(proxy.getCurrencySymbol(currency));
		        out.write(" ");
		        out.print(items[5][i]);
		        out.write("\"/>");
		        out.write("</td>");
		        out.write("<td width=\"3\">");
		        out.write("<img src=\"img/");
		        out.print(items[0][i]);
		        out.write("\" width=64 height=64 />");
		        out.write("</td>");
		        for (int j = 1; j < items.length - 1; j++) {
		          out.write("<td>");
		          out.print(items[j][i]);
		          out.write("</td>");
		        }
		        out.write("<td>");
		        out.print(proxy.getConversion("USD",currency,Float.valueOf(items[items.length - 1][i])));
		        out.write(" ");
		        out.print(proxy.getCurrencySymbol(currency));
		        out.write("</td>");
		        out.write("</a></tr>");
		      }
		%>
	
</table>
</div>
<br>
<input type="button" onClick="addToCart()" value="Add to Cart">
</form>

<br>

<h2>Your Shopping Cart</h2>
<form name="shoppingCartForm">
<div id="shoppingCart"></div>
<br>
<div id="total"></div>
<br>
<input type="button" onClick="checkoutCart()" value="Checkout">
<input type="button" onClick="deleteCart()" value="Empty"> <a
	href="../store/ShoppingCart/">(feed)</a></form>
</div>
</body>
</html>
