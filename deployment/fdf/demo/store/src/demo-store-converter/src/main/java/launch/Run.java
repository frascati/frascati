package launch;

import org.apache.tuscany.sca.host.embedded.SCADomain;

public class Run implements Runnable {

  public void run() {

    synchronized (this) {
      System.out.println("Starting ...");
      SCADomain scaDomain = SCADomain.newInstance("converter.composite");
      System.out.println("converter.composite ready for big business !!!");
      
      while (true) {
        try {
          System.in.read();
          wait();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

    }

  }

}
