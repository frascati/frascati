import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;

/**
 * Is the FDF launcher.
 * 
 * This launcher loads FDF with a set of jars located in a given directory.
 */
public class Launcher {

  /**
   * The main of the FDF launcher.
   */
  @SuppressWarnings("unchecked")
  public static void main(String[] args) throws Throwable {

    System.out.println("Starting the software..........");
    
    synchronized(Launcher.class) {
    
//    for (int i = 0; i < args.length; i++) {
//      System.out.println("+++++++++  args[" + i + "]=" + args[i]);
//   }
    
    // Arguments to give to the delegated launcher
    String[] fcArgs;

    ArrayList<String> fcList = new ArrayList<String>();
    String delegatedLauncher = args[args.length - 1];

    String libDir = null;

    boolean shift = false;
    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-lib")) {
        libDir = (i + 1 < args.length) ? args[i + 1] : "";
        // skip next element
        i++;
        shift = true;
      } else {
        fcList.add(args[i]);
      }
    }

    Class launcher = null;

    if (libDir != null) { // Add all jars to the classloader
      if (libDir.equals("")) {
        System.out.println("Please set the library directory");
        System.exit(1);
      }

      File[] libs = new File(libDir).listFiles();

      if (libs == null) {
        System.out.println("Can't list files of '" + libDir + "'");
        System.out.println("Please check arguments.");
        System.exit(1);
      } else {
        URL[] urls = new URL[libs.length];
        // Fill urls
        for (int j = 0; j < libs.length; j++) {
          urls[j] = libs[j].toURL();
//          System.out.println(libs[j].toString());
        }

        ClassLoader libClassLoader = new URLClassLoader(urls, Thread
            .currentThread().getContextClassLoader());

        Thread.currentThread().setContextClassLoader(libClassLoader);

        launcher = Class.forName(delegatedLauncher, true, libClassLoader);

      }
    } else { // Use the default classloader
      launcher = Class.forName(delegatedLauncher);
    }

    fcArgs = fcList.toArray(new String[] {});

    // Run the FDF ADL Launcher
    Class[] types = new Class[] { String[].class };
    Method method = launcher.getMethod("main", types);
    method.invoke(null, new Object[] { fcArgs });
    }
  }

  @SuppressWarnings("unused")
  private static void parseError() {
    System.out
        .println("Usage: Launcher <delegatedLauncher> -lib <libDir> <params>");
    System.out
        .println("where <delegatedLauncher> is the name of the launcher to use.");
    System.out
        .println("      <libDir> is the name of the directory containing libraries to load.");
    System.out
        .println("      <params> are the arguments to give to 'delegatedLauncher'.");
    System.exit(1);
  }
}