============================================================================

 ObjectWeb Fractal Deployment Framework
 Copyright (C) 2008 INRIA & LIFL (UMR USTL-CNRS 8022)

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 USA

 Contact: fdf-users@lists.gforge.inria.fr

 Author: Nicolas Dolet

 Contributors:
============================================================================

This modules aims to use FDF/DeployWare to deploy the FraSCAti runtime
distribution.

Requirements:
-------------
 * Java 5 (for instance, FDF doesn't support Java 6)
 * A well-formed FDF description (cf. examples/MyFrascati.fdf)

Usage:
------
First of all, edit MyFrascati.fdf and fix properties of the targeted system
(machine's name / IP, user login / password / key, path to archives of the
system to deploy, ...). 

Then run FDF, with the following command:

 > mvn -Pexplorer

FDF Explorer starts. Then load your FDF description, and run the deployment...

Link:
------
 http://fdf.gforge.inria.fr
