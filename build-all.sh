#!/bin/sh

export MAVEN_OPTS="-Xmx512M -XX:MaxPermSize=512M"
mvn -f org.eclipse.stp.sca.model/pom.xml clean install
mvn -f tinfi/pom.xml clean install
mvn clean install
