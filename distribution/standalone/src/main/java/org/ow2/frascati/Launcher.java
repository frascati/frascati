/***
 * OW2 FraSCAti distribution
 * Copyright (C) 2008-2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *                 Christophe Demarey.
 * 
 */

package org.ow2.frascati;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Is the FraSCAti launcher to run a Java main class in the context of a class
 * loader initialized with a set of jars located in a given directory.
 * 
 * Usage: org.ow2.frascati.Launcher <mainJavaClass> -lib <libDir> <params>
 * where <javaMainClass> is the name of the Java class to run. <libDir> is the
 * directory containing jars to load. <params> are the arguments to give to
 * 'javaMainClass'.
 */
public class Launcher {
  
  /**
   * Build a list of URL from specified directories.
   * 
   * @param dirs A comma separated list of directory to search in.
   * @throws MalformedURLException 
   */
  public static URL[] getLauncherClasspath(String dirs) throws MalformedURLException {
    List<URL> urls = new ArrayList<URL>();
    StringTokenizer st = new StringTokenizer(dirs, ",");
    
    while (st.hasMoreTokens()) {
      String dirName = st.nextToken();
      File dir = new File(dirName);
      File[] libs = dir.listFiles();
      
      if (libs == null) {
        System.err.println("Can't list files of '" + dirName + "'");
        System.err.println("Please check arguments.");
        System.exit(1);
      }
      // Fill urls
      for (File f : libs)
	if (f.getName().endsWith(".jar"))
	  urls.add(f.toURI().toURL());
      urls.add(dir.toURI().toURL());
    }
    
    return urls.toArray(new URL[0]);
  }
  
  /**
   * The main of the FraSCAti launcher.
   */
  public static void main(String[] args) throws Throwable {
    // Arguments to give to the Java class to run.
    ArrayList<String> fcList = new ArrayList<String>();

    String libDir = null;

    for (int i = 0; i < args.length; i++) {
      if (args[i].equals("-lib")) {
        libDir = (i + 1 < args.length) ? args[i + 1] : null;
        // skip next element
        i++;
      } else {
        fcList.add(args[i]);
      }
    }

    String mainJavaClassName = fcList.remove(0);
    Class mainJavaClass = null;

    if (libDir != null) { // Add all jars to the classloader
      ClassLoader libClassLoader = new URLClassLoader( getLauncherClasspath(libDir) , 
                                     Thread.currentThread().getContextClassLoader() );
      Thread.currentThread().setContextClassLoader(libClassLoader);
      mainJavaClass = Class.forName(mainJavaClassName, true, libClassLoader);
    } else { // Use the default classloader
      mainJavaClass = Class.forName(mainJavaClassName);
    }

    // Run the Java main class.
    mainJavaClass.getMethod("main", new Class[] { String[].class }).invoke(
        null, new Object[] { fcList.toArray(new String[fcList.size()]) });
  }

  // TODO: This method is never called.
  private static void parseError() {
    System.out.println("Usage: " + Launcher.class.getName()
        + " <javaMainClass> -lib <libDir> <params>");
    System.out
        .println("where <javaMainClass> is the name of the Java class to run.");
    System.out
        .println("      <libDir> is the directory containing jars to load.");
    System.out
        .println("      <params> are the arguments to give to 'javaMainClass'.");
    System.exit(1);
  }
}
