<?xml version="1.0"?>

<!--  OW2 FraSCAti                                                                  -->
<!--  Copyright (C) 2008-2009 INRIA, USTL                                           -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Damien Fournier                                                       -->

<%@ page
	import="org.apache.commons.fileupload.*,org.apache.commons.fileupload.servlet.ServletFileUpload,org.apache.commons.fileupload.disk.DiskFileItemFactory,org.apache.commons.io.FilenameUtils,java.util.*,java.io.File,java.lang.Exception"%>
<%@ page import="java.net.URL"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Frascati server</title>
</head>
<script type="text/javascript">
	function delayedRedirect() {
		window.location = "main.jsp"
	}
</script>
<body onLoad="setTimeout('delayedRedirect()', 5000)">
<p>
<%
  ServletContext context = getServletContext();

  org.ow2.frascati.factory.runtime.domain.SCADomain scaDomain = (org.ow2.frascati.factory.runtime.domain.SCADomain) context
      .getAttribute("scaDomain");

  org.ow2.frascati.factory.runtime.domain.api.Domain domain = scaDomain.getDomain();

  if (ServletFileUpload.isMultipartContent(request)) {
    ServletFileUpload servletFileUpload = new ServletFileUpload(
        new DiskFileItemFactory());
    List fileItemsList = servletFileUpload.parseRequest(request);

    String optionalFileName = "";
    FileItem fileItem = null;

    Iterator it = fileItemsList.iterator();
    while (it.hasNext()) {
      FileItem fileItemTemp = (FileItem) it.next();
      if (fileItemTemp.isFormField()) {
        if (fileItemTemp.getFieldName().equals("filename"))
          optionalFileName = fileItemTemp.getString();
      } else
        fileItem = fileItemTemp;
    }

    if (fileItem != null) {
      String fileName = fileItem.getName();

      /* Save the uploaded file */
      if (fileItem.getSize() > 0) {
        if (optionalFileName.trim().equals(""))
          fileName = FilenameUtils.getName(fileName);
        else
          fileName = optionalFileName;

        String dirName = System.getProperty("java.io.tmpdir");
        File saveTo = new File(dirName + "/" + fileName);

        try {
          fileItem.write(saveTo);
          if (fileName.endsWith("jar")) {
            domain.addLibraries(new URL[] { saveTo.toURI().toURL() });
            out.println("Library " + fileName + " Loaded");
          }
          if (fileName.endsWith("composite")) {
            domain.getComposite(saveTo.getCanonicalPath());
            out.println("Composite " + fileName + " Loaded");
          }
        } catch (Exception e) {
          out.println("Error when loading " + fileName);
          out.println(e.getMessage());
        }
      }
    }
  }
%>

</body>
</html>
