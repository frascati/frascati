<?xml version="1.0"?>

<!--  OW2 FraSCAti                                                                  -->
<!--  Copyright (C) 2008-2009 INRIA, USTL                                           -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Damien Fournier                                                       -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="org.ow2.frascati.*"%>
<%@ page import="java.net.URL"%>
<%@ page import="java.net.URLClassLoader"%>
<%@ page import="org.objectweb.fractal.*"%>
<%@ page import="org.objectweb.fractal.*"%>
<%@ page import="org.objectweb.fractal.api.*"%>
<%@ page import="org.objectweb.fractal.api.control.*"%>
<%@ page import="org.objectweb.fractal.api.factory.*"%>
<%@ page import="org.objectweb.fractal.api.type.*"%>
<%@ page import="org.objectweb.fractal.util.Fractal"%>
<%@ page import="java.util.List"%>


<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FraSCAti server</title>
</head>
<body>
<big><b>FraSCAti Server<br>
</b></big>

<%
  ServletContext context = getServletContext();
  String lib = request.getParameter("lib");

  System.setProperty("fractal.provider",
       "org.objectweb.fractal.util.ChainedProvider");
  System.setProperty("fractal.providers",
      "org.objectweb.fractal.juliac.runtime.Juliac,org.objectweb.fractal.julia.Julia");
  System.setProperty("julia.config", "tinfi-bundled.cfg");

  org.ow2.frascati.factory.runtime.domain.SCADomain scaDomain = (org.ow2.frascati.factory.runtime.domain.SCADomain) context
      .getAttribute("scaDomain");
  
  org.ow2.frascati.factory.runtime.domain.api.Domain domain = scaDomain.getDomain();

  // context.setAttribute("scaDomain", domain);
%>

<table border="0" cellspacing="0" style="width: 90%" align="center">
	<tr>
		<td style="width: 30%" align="center">Loaded Composites
		<form><select name="composites" size="10" style="width: 90%">
			<%
			  String [] list = domain.getCompositesNames();
			  for (String loadedComposite : list) {
			    out.println("<OPTION>" + loadedComposite);
			  }
			%>
		</select></form>
		</td>
		<td valign="top">
		<form name="load" method="post" enctype="multipart/form-data"
			action="formLoad.jsp">Load Composite/Library <input
			type="file" name="file" value="filename" size="40"> <input
			type="submit"></form>
		</td>
	</tr>
</table>
</body>
</html>