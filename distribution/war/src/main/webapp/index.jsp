<?xml version="1.0"?>

<!--  OW2 FraSCAti                                                                  -->
<!--  Copyright (C) 2008-2009 INRIA, USTL                                           -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Damien Fournier                                                       -->

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="org.ow2.frascati.*"%>
<%@ page import="org.objectweb.fractal.*"%>
<%@ page import="java.io.*"%>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FraSCAti</title>
</head>
<body>
<b><big>Loading FraSCAti Server ...</big></b>
<%
  ServletContext context = getServletContext();
  org.ow2.frascati.factory.runtime.domain.SCADomain scaDomain= (org.ow2.frascati.factory.runtime.domain.SCADomain) context
      .getAttribute("scaDomain");

  if (scaDomain == null) {

    System.setProperty("fractal.provider",
       "org.objectweb.fractal.util.ChainedProvider");
    System.setProperty("fractal.providers",
       "org.scorware.tinfi.Tinfi,org.objectweb.fractal.julia.Julia");
    System.setProperty("julia.config", "tinfi.cfg");

    scaDomain = new org.ow2.frascati.factory.runtime.domain.SCADomain();
    org.ow2.frascati.factory.runtime.domain.api.DomainConfig conf = scaDomain.getDomainConfig();
    
    
    conf.setOutput(new File(System.getProperty("java.io.tmpdir")));

    context = getServletContext();
    context.setAttribute("scaDomain", scaDomain);

  }
%>

<jsp:forward page="main.jsp" />

</body>
</html>