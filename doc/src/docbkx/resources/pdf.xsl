<?xml version="1.0"?>
<!DOCTYPE xsl:stylesheet [
     <!ENTITY db_xsl_path "../../tools/docbook-xsl-1.73.2">
     <!ENTITY admon_gfx_path "../../../../tools/docbook-xsl-1.73.2/images/">
 ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>
    <xsl:import href="urn:docbkx:stylesheet"/>

    <!-- Allow to wrap long lines for program listing -->
    <xsl:param name="hyphenate.verbatim" select="1"/>
    <xsl:attribute-set name="monospace.verbatim.properties">
        <xsl:attribute name="wrap-option">wrap</xsl:attribute>
        <xsl:attribute name="hyphenation-character">\</xsl:attribute>
    </xsl:attribute-set>

</xsl:stylesheet>

