<?xml version="1.0"?>
<!--
  * Eclipse STP SCA Tools
  *
  * Copyright (c) 2009-2011 INRIA, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
-->

<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.ow2.frascati.model</groupId>
  <artifactId>model-parent</artifactId>
  <version>2.0.1.3</version>
  <packaging>pom</packaging>

  <parent>
    <groupId>org.ow2</groupId>
    <artifactId>ow2</artifactId>
    <version>1.3</version>
    <relativePath></relativePath>
  </parent>

  <name>OW2 FraSCAti SCA model (relies on Eclipse STP SCA Tools)</name>

  <!-- ========== -->
  <!-- Properties -->
  <!-- ========== -->
  
  <properties>
    <stp.scm.url>svn://dev.eclipse.org/svnroot/soa/org.eclipse.stp.sca-tools/org.eclipse.stp.sca/tags/3.5SR1</stp.scm.url>
    <!-- Eclipse EMF Artifacts version -->
    <emf.version>2.4.0</emf.version>
    <eclipse.version>3.4.0</eclipse.version>
  </properties>

  <!-- ======= -->
  <!-- Modules -->
  <!-- ======= -->

  <modules>
    <module>org.eclipse.stp.sca</module>
    <module>org.eclipse.stp.sca.csa</module>
    <module>org.eclipse.stp.sca.osoa.java</module>      
    <module>org.eclipse.stp.sca.domainmodel.frascati</module>  
    <module>org.eclipse.stp.sca.domainmodel.tuscany</module>
    <module>org.eclipse.stp.sca.introspection</module>      
  </modules>
  
  <!-- ====== -->
  <!-- Build. -->
  <!-- ====== -->

  <build>
    <defaultGoal>install</defaultGoal>

    <!-- ================= -->
    <!-- Plugin management -->
    <!-- ================= -->
    
    <pluginManagement>
      <plugins>
      
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-scm-plugin</artifactId>
          <version>1.6</version>
          <executions>
            <execution>
              <id>checkout</id>
              <phase>generate-sources</phase>
              <goals>
                <goal>checkout</goal>
              </goals>
            </execution>
          </executions>
          <configuration>
            <goals>compile</goals>
          </configuration>
        </plugin>
      
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>2.3.2</version>
          <configuration>
            <source>1.5</source>
            <target>1.5</target>
          </configuration>
        </plugin>
      
        <plugin>
          <groupId>org.codehaus.mojo</groupId>
          <artifactId>build-helper-maven-plugin</artifactId>
          <version>1.7</version>
          <executions>
            <execution>
              <id>add-source</id>
              <phase>generate-sources</phase>
              <goals>
                <goal>add-source</goal>
              </goals>
              <configuration>
                <sources>
                  <source>${basedir}/target/checkout</source>
                </sources>
              </configuration>
            </execution>
          </executions>
        </plugin>
        
      </plugins>
    </pluginManagement>
  </build>

    <!-- ======================= -->
    <!-- Dependencies management -->
    <!-- ======================= -->
    <!--
        specify which versions are used across all modules. single
        modules are still required to declare their dependency, but not
        the version, which is handled globally by the
        dependencyManagement section.
    -->
    <dependencyManagement>
      <dependencies>
      
        <!-- EMF -->

        <dependency>
          <groupId>org.eclipse.emf</groupId>
          <artifactId>ecore</artifactId>
          <version>${emf.version}</version>
        </dependency>

        <dependency>
          <groupId>org.eclipse.emf.ecore</groupId>
          <artifactId>xmi</artifactId>
          <version>${emf.version}</version>
        </dependency>

        <dependency>
          <groupId>org.eclipse.emf</groupId>
          <artifactId>common</artifactId>
          <version>${emf.version}</version>
        </dependency>

        <dependency>
          <groupId>org.eclipse</groupId>
          <artifactId>osgi</artifactId>
          <version>3.3.0-v20070530</version>
        </dependency>
        
        <dependency>
          <groupId>org.eclipse.core</groupId>
          <artifactId>resources</artifactId>
          <version>${eclipse.version}</version>
          <scope>provided</scope>
        </dependency>

        <dependency>
          <groupId>org.eclipse.core</groupId>
          <artifactId>runtime</artifactId>
          <version>${eclipse.version}</version>
          <scope>provided</scope>
        </dependency>
        
      </dependencies>
    </dependencyManagement>
    
    <!-- ============= -->
    <!-- Repositories  -->
    <!-- ============= -->
    
    <repositories>
      <repository>
        <id>frascati-repo</id>
        <name>FraSCAti Nexus repository</name>
        <url>https://frascati-repo.inria.fr/nexus/content/groups/public</url>
        <releases>
          <enabled>true</enabled>
        </releases>
        <snapshots>
          <enabled>true</enabled>
        </snapshots>
      </repository>    
    </repositories>

    <pluginRepositories>
      <pluginRepository>
        <id>frascati-repo</id>
        <name>FraSCAti Nexus repository</name>
        <url>https://frascati-repo.inria.fr/nexus/content/groups/public</url>
        <releases>
          <enabled>true</enabled>
        </releases>
        <snapshots>
          <enabled>true</enabled>
        </snapshots>
      </pluginRepository>    
    </pluginRepositories>

    <!-- ======================================================= -->
    <!-- Profile used to deploy on the FraSCAti Nexus repository -->
    <!-- ======================================================= -->
    
    <profiles>
      <profile>
        <id>frascati-repo</id>
          <distributionManagement>
            <repository>
              <id>frascati-release</id>
              <name>OW2 FraSCAti release repository</name>
              <url>https://frascati-repo.inria.fr/nexus/content/repositories/releases</url>
            </repository>
            <snapshotRepository>
              <id>frascati-snapshot</id>
              <name>OW2 FraSCAti snapshot repository</name>
              <url>https://frascati-repo.inria.fr/nexus/content/repositories/snapshots</url>
            </snapshotRepository>
          </distributionManagement>
      </profile>
    </profiles>

</project>
