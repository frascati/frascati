package fr.inria.galaxy.fpbindings.client;

import java.util.ArrayList;
import java.util.List;

import org.osoa.sca.annotations.Reference;

import functionaltests.component.wsbindings.frascati.AnObject;
import functionaltests.component.wsbindings.frascati.ServicesPortType;


public class ClientImpl implements Runnable {
    @Reference
    private ServicesPortType services;

    public ClientImpl() {
    }

    public void run() {
        if (services != null) {
            try {
                services.doNothing();
                System.err.println("-------> doNothing called");
                System.err.println();

                int i = 1;
                i = services.incrementInt(i);
                System.err.println("-------> incrementInt called: " + i);
                System.err.println();

                List<Double> listDouble = new ArrayList<Double>();
                for (int j = 0; j < 5; j++) {
                    listDouble.add(j + (0.1 * j));
                }
                listDouble = services.decrementArrayDouble(listDouble);
                System.err.println("-------> decrementArrayDouble called:");
                for (int j = 0; j < listDouble.size(); j++) {
                    System.err.println("listDouble[" + j + "]=" + listDouble.get(j));
                }
                System.err.println();

                String string = "FraSCAti";
                string = services.modifyString(string);
                System.err.println("-------> modifyString called: " + string);
                System.err.println();

                string = "Hello ProActive !";
                String[] stringSplit = services.splitString(string).toArray(new String[] {});
                System.err.println("-------> splitString called:");
                for (int j = 0; j < stringSplit.length; j++) {
                    System.err.println("helloStringSplit[" + j + "]= " + stringSplit[j]);
                }
                System.err.println();

                AnObject anObject = new AnObject();
                anObject.setId("Id" + ((int) (Math.random() * 100)));
                anObject.setIntField((int) (Math.random() * 100));
                AnObject object = services.modifyObject(anObject);
                System.err.println("-------> modifyObject called: " + object.getId() + " - " +
                    object.getIntField());
                System.err.println();

                List<AnObject> listObject = new ArrayList<AnObject>();
                for (int j = 0; j < 5; j++) {
                    anObject = new AnObject();
                    anObject.setId("Id" + ((int) (Math.random() * 100)));
                    anObject.setIntField((int) (Math.random() * 100));
                    listObject.add(anObject);
                }
                listObject = services.modifyArrayObject(listObject);
                System.err.println("-------> modifyArrayObject called:");
                for (int j = 0; j < listObject.size(); j++) {
                    System.err.println("arrayObject[" + j + "] = " + listObject.get(j).getId() + " - " +
                        listObject.get(j).getIntField());
                }
                System.err.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }
}