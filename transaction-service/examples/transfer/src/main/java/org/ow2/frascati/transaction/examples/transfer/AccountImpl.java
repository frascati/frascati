/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2008-2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */

package org.ow2.frascati.transaction.examples.transfer;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.XADataSource;
import javax.transaction.TransactionManager;

import org.enhydra.jdbc.standard.StandardXADataSource;
import org.osoa.sca.annotations.Property;
import org.ow2.frascati.transaction.TransactionManagerHelper;

/**
 * Implementation of the {@link Account} component
 * 
 * @author <a href="mailto:Nicolas.Dolet@inria.fr">Nicolas Dolet</a>
 */
public class AccountImpl implements Account {

    /**
     * The id of the account
     */
    @Property
    public String id;

    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

    /**
     * The connection to the database
     */
    private Connection conn = null;

    // --------------------------------------------------------------------------
    // Implementation of the Account interface
    // --------------------------------------------------------------------------

    /**
     * @see Account#credit(float)
     */
    public void credit(float amount) {
        System.out.println("Credit " + amount + " on account number " + id);
        try {
            float oldBalance = getDBBalance();
            float newBalance = oldBalance + amount;
            setDBBalance(newBalance);
        } catch (SQLException e) {
            System.err.println("Can't retrieve balance of account " + id + ": "
                    + "error during database access!");
        }
    }

    /**
     * @see Account#debit(float)
     */
    public void debit(float amount) throws InsufficientAmountException {
        System.out.println("Debit " + amount + " from account number " + id);
        try {
            float oldBalance = getDBBalance();
            if (amount > oldBalance) {
                throw new InsufficientAmountException("Can't debit " + amount
                        + " from account " + id);
            }
            float newBalance = oldBalance - amount;
            setDBBalance(newBalance);
        } catch (SQLException e) {
            System.err.println("Can't retrieve balance of account " + id + ": "
                    + "error during database access!");
        }
    }

    /**
     * @see Account#toString()
     */
    public String toString() {
        try {
            return "Account '" + this.id + "': " + getDBBalance();
        } catch (SQLException e) {
            return "Account '" + this.id + "': Unknown balance: "
                    + "database unreachable.";
        }
    }

    // --------------------------------------------------------------------------
    // Getter / Setter for the balance property
    // --------------------------------------------------------------------------

    /**
     * Get the balance of the account
     * @return a String representing the balance
     */
    @Property
    public String getBalance() {
        try {
            return Float.toString(getDBBalance());
        } catch (SQLException e) {
            return null;
        }
    }

    /**
     * Set the balance of the account
     * @param balance the String representing the balance to set
     */
    @Property
    public void setBalance(String balance) {
        // this.internalBalance = Float.valueOf(balance);
        setDBBalance(Float.valueOf(balance));
    }

    // --------------------------------------------------------------------------
    // Private methods for database connection
    // --------------------------------------------------------------------------

    /**
     * Make the connection to the database
     */
    private void connectDB() {
        try {
            String login = "frascati";
            String password = "frascati";
            
            XADataSource xads = new StandardXADataSource();
            
                ((StandardXADataSource) xads)
                        .setDriverName("com.mysql.jdbc.Driver");
                ((StandardXADataSource) xads)
                        .setUrl("jdbc:mysql://localhost/Accounts");
                
                TransactionManager tm = TransactionManagerHelper.getTransactionManager();
                if(tm != null)
                    ((StandardXADataSource) xads).setTransactionManager(tm);
                
                conn = xads.getXAConnection(login, password).getConnection();
            
        } catch (Exception e) {
            System.err.println("Cannot connect to database server");
            e.printStackTrace();
        }
    }

    /**
     * Close the connection to the database
     */
    private void disconnectDB() {
        if (conn != null) {
            try {
                conn.close();
                // System.out.println("Database connection terminated");
            } catch (Exception e) {
                System.err.println("Cannot close the database connection");
                e.printStackTrace();
            }
        }
    }

    /**
     * Get the balance stored in the database
     * @return the balance of the account
     * @throws SQLException if an error occurs during database access
     */
    private float getDBBalance() throws SQLException {

        connectDB();

        Statement s;
        long result;
        s = conn.createStatement();
        s.executeQuery("SELECT balance FROM account WHERE account=\"" + id
                + "\"");

        ResultSet rs = s.getResultSet();
        if (rs.next()) {
            result = rs.getLong("balance");
        } else {
            throw new SQLException("No account " + id
                    + " found in the database!");
        }
        rs.close();
        s.close();

        disconnectDB();

        return result;
    }

    /**
     * Update the balance stored in the database
     * @param balance the balance to set
     */
    private void setDBBalance(float balance) {
        connectDB();

        Statement s;
        try {
            s = conn.createStatement();
            s.executeUpdate("UPDATE account set balance = " + balance
                    + " where account = \"" + id + "\"");
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnectDB();

    }

}
