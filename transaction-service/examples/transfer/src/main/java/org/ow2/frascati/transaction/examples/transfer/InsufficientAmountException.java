/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2008-2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */

package org.ow2.frascati.transaction.examples.transfer;

/**
 * Exception to throw when trying to debit more money than the account's balance
 * 
 * @author <a href="mailto:Nicolas.Dolet@inria.fr">Nicolas Dolet</a>
 */
public class InsufficientAmountException
     extends Exception {

    private static final long serialVersionUID = -4951925869024581896L;

    /**
     * The default constructor
     * @param message the message
     */
    public InsufficientAmountException(String message) {
        super(message);
    }
}
