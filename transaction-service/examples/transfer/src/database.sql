-- * OW2 FraSCAti Transaction service
-- * Copyright (C) 2008 INRIA
-- *
-- * This library is free software; you can redistribute it and/or
-- * modify it under the terms of the GNU Lesser General Public
-- * License as published by the Free Software Foundation; either
-- * version 2 of the License, or (at your option) any later version.
-- *
-- * This library is distributed in the hope that it will be useful,
-- * but WITHOUT ANY WARRANTY; without even the implied warranty of
-- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- * Lesser General Public License for more details.
-- *
-- * You should have received a copy of the GNU Lesser General Public
-- * License along with this library; if not, write to the Free Software
-- * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
-- * USA
-- *
-- * Contact: frascati@ow2.org
-- *
-- * Author: Nicolas Dolet

CREATE USER frascati IDENTIFIED BY 'frascati';

create database Accounts;

GRANT ALL PRIVILEGES ON Accounts.* TO 'frascati'@'localhost' IDENTIFIED BY 'frascati' WITH GRANT OPTION;

use Accounts;

create table account (
  account char(4) not null primary key,
  balance decimal)type=InnoDB;

insert into account values("A001", 1000);
insert into account values("A002", 500);
insert into account values("A003", 5000);
