/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.transaction.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;

public class GlobalTransactionTestCase {

  private FraSCAti frascati;
  private Component scaDomain;
  private Runnable r;

  @Before
  public void setUp() throws Exception {
    frascati = FraSCAti.newFraSCAti();
    scaDomain = frascati.getComposite("testManagedGlobalTransactionIntent");
    r = frascati.getService(scaDomain, "t", java.lang.Runnable.class);
  }

  @Test
  public void testManagedGlobalTransaction() throws InterruptedException {
    r.run();
  }

  @After
  public void tearDown() throws Exception {
    frascati.close(scaDomain);
  }

}
