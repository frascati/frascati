/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.transaction.test;

import java.lang.reflect.Field;

import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.ow2.frascati.transaction.TransactionManager;

public class GlobalTransaction
  implements Runnable {
  
  // --------------------------------------------------------------------------
  // Implementation of java.lang.Runnable
  // --------------------------------------------------------------------------
  
  public void run() {
    System.out.println("GlobalTransaction...");
    try {
      Transaction t = TransactionManager.getInstance().getTransaction();
      int s = t.getStatus();
      String status = "Invalid status value";
      Field[] fields = Status.class.getDeclaredFields();
      for(Field field : fields) {
        if(field.getInt(null) == s)
          status = field.getName();
      }
      System.out.println("Current transaction status is: " + status);
    } catch (SystemException e) {
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    }
  }

}
