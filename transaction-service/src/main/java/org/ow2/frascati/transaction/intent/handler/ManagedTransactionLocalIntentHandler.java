/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2008-2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.transaction.intent.handler;

import javax.transaction.Status;
import javax.transaction.Transaction;

import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

@Scope("COMPOSITE")
public class ManagedTransactionLocalIntentHandler
     extends AbstractTransactionIntentHandler {
    
    // --------------------------------------------------------------------------
    // Implementation of the IntentHandler interface
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.tinfi.control.intent.IntentHandler#invoke(IntentJoinPoint)
     */
    public Object invoke(IntentJoinPoint ijp) throws Throwable {
        // If there is no transaction: idem than managedTransactionGlobal
        // Else:
        //  * suspend the current transaction
        //  * begin a new transaction
        //  * proceed business logic
        //  * commit/rollback the new transaction
        //  * resume the suspended transaction
        if (transactionManager.getStatus() == Status.STATUS_NO_TRANSACTION) {
            transactionManager.begin();
            Object ret = null;
            try {
                ret = ijp.proceed();
            } catch (Throwable t) {
                transactionManager.rollback();
                throw t;
            }
            transactionManager.commit();
            return ret;
        } else {
            Transaction suspended = transactionManager.suspend();
            transactionManager.begin();
            Object ret = null;
            try {
                ret = ijp.proceed();
            } catch (Throwable t) {
                transactionManager.rollback();
                transactionManager.resume(suspended);
                throw t;
            }
            transactionManager.commit();
            transactionManager.resume(suspended);
            return ret;
        }
    }

}
