/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2008-2009 INRIA
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */

package org.ow2.frascati.transaction;

import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.InvalidTransactionException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.objectweb.jotm.Current;
import org.osoa.sca.annotations.Scope;

/**
 * The Frascati Transaction Manager
 * 
 * @author <a href="mailto:Nicolas.Dolet@inria.fr">Nicolas Dolet</a>
 */
@Scope("COMPOSITE")
public class TransactionManager
  implements javax.transaction.TransactionManager {

    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

    /**
     * The singleton
     */
    private static org.ow2.frascati.transaction.TransactionManager instance;

    /**
     * The wrapped transaction manager
     */
    protected static Current current;

    /**
     * Logger for this class 
     */
    protected static java.util.logging.Logger log;

    // --------------------------------------------------------------------------
    // Constructor
    // --------------------------------------------------------------------------

    /**
     * The default constructor
     */
    public TransactionManager() {
        if(current==null) {
            current = new Current();
            log = java.util.logging.Logger
            .getLogger(TransactionManager.class.getCanonicalName());
            log.info("Transaction Manager created.");
        }
    }

    // --------------------------------------------------------------------------
    // Getter for the singleton
    // --------------------------------------------------------------------------

    public static javax.transaction.TransactionManager getInstance() {
        if(instance == null) {
            instance = new org.ow2.frascati.transaction.TransactionManager();
        }
        return instance;
    }

    // --------------------------------------------------------------------------
    // Implementation of javax.transaction.TransactionManager
    // --------------------------------------------------------------------------

    /**
     * @see javax.transaction.TransactionManager#begin()
     */
    public void begin() throws NotSupportedException, SystemException {
        log.info("Begin a transaction...");
        current.begin();
    }

    /**
     * @see javax.transaction.TransactionManager#commit()
     */
    public void commit() throws RollbackException, HeuristicMixedException,
            HeuristicRollbackException, SecurityException,
            IllegalStateException, SystemException {
        log.info("Commit the current transaction");
        current.commit();
    }

    /**
     * @see javax.transaction.TransactionManager#getStatus()
     */
    public int getStatus() throws SystemException {
        return current.getStatus();
    }

    /**
     * @see javax.transaction.TransactionManager#getTransaction()
     */
    public Transaction getTransaction() throws SystemException {
        return current.getTransaction();
    }

    /**
     * @see javax.transaction.TransactionManager#resume(Transaction)
     */
    public void resume(Transaction t) throws InvalidTransactionException,
            IllegalStateException, SystemException {
        current.resume(t);
    }

    /**
     * @see javax.transaction.TransactionManager#rollback()
     */
    public void rollback() throws IllegalStateException, SecurityException,
            SystemException {
        log.info("Rollback the current transaction");
        current.rollback();
    }

    /**
     * @see javax.transaction.TransactionManager#setRollbackOnly()
     */
    public void setRollbackOnly() throws IllegalStateException, SystemException {
        current.setRollbackOnly();
    }

    /**
     * @see javax.transaction.TransactionManager#setTransactionTimeout(int)
     */
    public void setTransactionTimeout(int timeout) throws SystemException {
        current.setTransactionTimeout(timeout);
    }

    /**
     * @see javax.transaction.TransactionManager#suspend()
     */
    public Transaction suspend() throws SystemException {
        return current.suspend();
    }

}
