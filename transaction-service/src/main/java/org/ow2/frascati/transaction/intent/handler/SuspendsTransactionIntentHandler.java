/***
 * OW2 FraSCAti Transaction service
 * Copyright (C) 2008-2009 INRIA, USTL
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 */
package org.ow2.frascati.transaction.intent.handler;

import javax.transaction.Transaction;

import org.osoa.sca.annotations.Scope;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

@Scope("COMPOSITE")
public class SuspendsTransactionIntentHandler
     extends AbstractTransactionIntentHandler {
    
    // --------------------------------------------------------------------------
    // Implementation of the IntentHandler interface
    // --------------------------------------------------------------------------

    /**
     * @see org.ow2.frascati.tinfi.control.intent.IntentHandler#invoke(IntentJoinPoint)
     */
    public Object invoke(IntentJoinPoint ijp) throws Throwable {
        /**
         * An invocation on this intent will:
         *  1) suspend the current transaction (if exists)
         *  2) delegate the invocation to the intent jointpoint
         *  3) resume the transaction after business delegation
         */
        Transaction tx = transactionManager.suspend();
        Object ret = null;
        try {
            ret = ijp.proceed();
        } catch (Throwable t) {
            transactionManager.setRollbackOnly();
            throw t;
        } finally {
            if(tx!=null) {
                transactionManager.resume(tx);
            }
        }
        return ret;
    }

}
