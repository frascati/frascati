/**
 * OW2 FraSCAti Examples: Contextual properties
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.contextual_properties;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class Runner implements Runnable
{
	@ComponentName
    private String componentName;

    @Property
    private String user;

    @Property
    private String home;

    @Property
    private String os;

    @Property
    private String foo;

    @Property
    private String bar;

    public void run()
    {
      System.out.println("Component name=" + this.componentName);
      System.out.println("Property 'user'=" + this.user);
      System.out.println("Property 'home'=" + this.home);
      System.out.println("Property 'os'=" + this.os);
      System.out.println("Property 'foo'=" + this.foo);
      System.out.println("Property 'bar'=" + this.bar);
    }
}
