/**
 * OW2 FraSCAti Examples: Contextual properties
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 */

package org.ow2.frascati.examples.contextual_properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.Test;

import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.util.context.ContextualProperties;

public class ContextualPropertiesTestCase
     extends FraSCAtiTestCase
{
  /**
   * @see FraSCAtiTestCase#IinitializeContextualProperties(ContextualProperties)
   */
  @Override
  protected void initializeContextualProperties(ContextualProperties contextualProperties)
  {
    // Set the 'component-name' contextual property.
    contextualProperties.setContextualProperty("component-name", "runner");

    // Set the 'foo' contextual property.
    contextualProperties.setContextualProperty("foo", "foo");

    // Create a Properties instance.
    Properties properties = new Properties();
    try {
      // Load it from the bar.properties file.
      properties.load(new FileInputStream("bar.properties"));
    } catch(IOException ioe) {
      throw new Error(ioe);
    }

    // Set the 'bar' contextual property.
    contextualProperties.setContextualProperty("bar", properties);
  }

  @Test
  public final void testRun()
  {
    getService(Runnable.class, "Runnable").run();
  }
}
