<?xml version="1.0" encoding="ISO-8859-15"?>
<!--  OW2 FraSCAti Examples : Fscript reconfiguration                               -->
<!--  Copyright (C) 2008-2012 Inria, University of Lille 1                          -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Christophe Demarey                                                    -->
<!--                                                                                -->
<!--  Contributor(s): Philippe Merle                                                -->

<composite xmlns="http://www.osoa.org/xmlns/sca/1.0"
           xmlns:frascati="http://frascati.ow2.org/xmlns/sca/1.1"
           name="converter">
  <service name="currencyConverter" promote="currency-converter/currencyConverter">
    <frascati:binding.rest uri="http://localhost:8888/converter"/>
    <binding.ws uri="http://localhost:8765/converter-ws"/>
  </service>
  
  <service name="exchangeRateUpdater"       promote="exchange-rate-updater/exchangeRateUpdater"/>
  <service name="exchangeRateUpdaterScript" promote="exchange-rate-updater-script/exchangeRateUpdater"/>
  
  <component name="currency-converter">
    <implementation.java class="org.ow2.frascati.examples.reconfig.converter.CurrencyConverterImpl"/>
    
    <service name="currencyConverter">
      <interface.java interface="org.ow2.frascati.examples.reconfig.converter.CurrencyConverter"/>
    </service>
    
    <property name="dollarExchangeRate">0.75</property>
    <property name="pesoExchangeRate">2751.43358</property>
  </component>


  <component name="exchange-rate-updater">
    <implementation.java class="org.ow2.frascati.examples.reconfig.converter.fscript.ExchangeRateUpdaterImpl"/>
    
    <service name="exchangeRateUpdater">
      <interface.java interface="org.ow2.frascati.examples.reconfig.converter.fscript.ExchangeRateUpdater"/>
    </service>
    
    <property name="reconfigurationScript">reconfig.fscript</property>
  </component>

  <component name="exchange-rate-updater-script">
    <frascati:implementation.script script="reconfig.fscript"/>
    
    <service name="exchangeRateUpdater">
      <interface.java interface="org.ow2.frascati.examples.reconfig.converter.fscript.ExchangeRateUpdater"/>
    </service>
  </component>
</composite>
