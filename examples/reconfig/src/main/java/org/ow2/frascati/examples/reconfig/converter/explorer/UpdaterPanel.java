/**
 * OW2 FraSCAti Examples: Fscript reconfiguration
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.examples.reconfig.converter.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.script.ScriptException;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.ow2.frascati.examples.reconfig.converter.fscript.ExchangeRateUpdater;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link ExchangeRateUpdater} instances.
 *
 * @author Christophe Demarey
 */
@SuppressWarnings("serial")
public class UpdaterPanel
     extends AbstractSelectionPanel<ExchangeRateUpdater>
{
  private NumberFormat nf; // used to format double display
  private JTextField jTextFieldValue;
  private JButton jButtonUpdate;

  /**
   * Default constructor creates the panel.
   */
  public UpdaterPanel()
  {
    super();
    
    nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(4);

    jTextFieldValue = new JTextField();
    jTextFieldValue.setColumns(10);
    jButtonUpdate = new JButton("Update");

    // layout
    this.add(jTextFieldValue);
    this.add(jButtonUpdate);
    
    // actions
    jButtonUpdate.addActionListener( new ActionListener() {        
        public final void actionPerformed(ActionEvent e) {
            if (jTextFieldValue.getText().length() > 0) {
                double newValue = Double.parseDouble( jTextFieldValue.getText() );
                try {
                    selected.updateDollarExchangeRate(newValue);
                } catch (ScriptException e1) {
                    System.out.println("Reconfiguration failed!");
                    e1.printStackTrace();
                }
            }
        }
    });

  }
  
}
