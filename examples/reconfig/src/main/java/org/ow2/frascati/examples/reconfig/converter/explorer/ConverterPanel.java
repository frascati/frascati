/**
 * OW2 FraSCAti Examples: Fscript reconfiguration
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.examples.reconfig.converter.explorer;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.util.Locale;

import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.jdesktop.layout.GroupLayout;

import org.ow2.frascati.examples.reconfig.converter.CurrencyConverter;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link CurrencyConverter} instances.
 *
 * @author Christophe Demarey
 */
@SuppressWarnings("serial")
public class ConverterPanel
     extends AbstractSelectionPanel<CurrencyConverter>
{
  private NumberFormat nf; // used to format double display
  private JComboBox jComboBoxCurrency;
  private JLabel jLabelEuro;
  private JFormattedTextField jTextFieldDollar;
  private JFormattedTextField jTextFieldEuro;
  private NumberFormat euroFormat;
  private NumberFormat dollarFormat;

  /**
   * Default constructor creates the panel.
   */
  public ConverterPanel()
  {
    super();
    
    nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(4);

    jLabelEuro = new JLabel();
    jComboBoxCurrency = new JComboBox( new String[] { "Dollar", "Colombian Peso" } );
    euroFormat = NumberFormat.getCurrencyInstance(Locale.FRANCE);
    dollarFormat = NumberFormat.getCurrencyInstance(Locale.US);
    jTextFieldEuro = new JFormattedTextField(euroFormat);
    jTextFieldDollar = new JFormattedTextField(dollarFormat);

    jLabelEuro.setText("Euro");
    jLabelEuro.setName("jLabelEuro");
    jComboBoxCurrency.setName("jComboBoxCurrency");
    jTextFieldEuro.setName("jTextFieldEuro");
    jTextFieldDollar.setName("jTextFieldDollar");

    // layout
    GroupLayout mainPanelLayout = new GroupLayout(this);
    this.setLayout(mainPanelLayout);
    mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabelEuro)
                    .add(jTextFieldEuro, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE))
                .add(23, 23, 23)
                .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jTextFieldDollar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                    .add(jComboBoxCurrency, 0, 140, Short.MAX_VALUE))
                .addContainerGap(520, Short.MAX_VALUE))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(mainPanelLayout.createSequentialGroup()
                .add(21, 21, 21)
                .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jComboBoxCurrency, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabelEuro))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextFieldDollar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jTextFieldEuro, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(556, Short.MAX_VALUE))
        );
    
    // actions
    jTextFieldEuro.addKeyListener( new KeyListener() {
        public final void keyPressed(KeyEvent e) { }

        public final void keyTyped(KeyEvent e) { }

        public final void keyReleased(KeyEvent e) {
            if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                String text = jTextFieldEuro.getText();
                if (text.length() > 0) {
                    Double value = parseDouble(text);
                    if (value != null) {
                        Double result;
                        
                        jTextFieldEuro.setValue(value);
                        if (jComboBoxCurrency.getSelectedIndex() == 0) {
                            result = selected.euroToDollar(value);
                        } else {
                            result = selected.euroToPeso(value);
                        }
                        jTextFieldDollar.setValue(result);
                    }
                }
            }
        } 
      }
    );
    jTextFieldDollar.addKeyListener( new KeyListener() {
        public final void keyPressed(KeyEvent e) { }

        public final void keyTyped(KeyEvent e) { }

        public final void keyReleased(KeyEvent e) {
            if ( e.getKeyCode() == KeyEvent.VK_ENTER ) {
                String text = jTextFieldDollar.getText();
                if (text.length() > 0) {
                    Double result;
                    
                    Double value = parseDouble(text);
                    jTextFieldDollar.setValue(value);
                    if (jComboBoxCurrency.getSelectedIndex() == 0) {
                        result = selected.dollarToEuro(value);
                    } else {
                        result = selected.pesoToEuro(value);
                    }
                    jTextFieldEuro.setValue(result);
                }
            }
        }
      }
    );
    jTextFieldEuro.addFocusListener( new FocusListener() {
        public final void focusLost(FocusEvent e) { }
        
        public final void focusGained(FocusEvent e) {
            jTextFieldEuro.setText("");
        }
      }
    );
    jTextFieldDollar.addFocusListener( new FocusListener() {
        public final void focusLost(FocusEvent e) { }
        
        public final void focusGained(FocusEvent e) {
            jTextFieldDollar.setText("");
        }
      }
    );
  }
  
  private Double parseDouble(String s)
  {
      try {
          return Double.parseDouble(s);
      } catch (NumberFormatException nfe) {
          JOptionPane.showMessageDialog(this, "Input type must be a double value!");
      }
      return null;
  }
  
}
