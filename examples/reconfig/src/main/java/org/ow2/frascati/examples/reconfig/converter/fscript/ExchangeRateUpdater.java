/**
 * OW2 FraSCAti Examples : Fscript reconfiguration
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Christophe Demarey
 */
package org.ow2.frascati.examples.reconfig.converter.fscript;

import javax.script.ScriptException;

/**
 * This service allows to dynamically update the exchange rate of the currency
 * converter component.
 * 
 * @author Christophe Demarey
 */
public interface ExchangeRateUpdater
{
    /** 
     * Update the exchange rate of the converter with the help of fscript.
     * 
     * @param newValue The new exchange rate value.
     */
    void updateDollarExchangeRate(double newValue) throws ScriptException;
}
