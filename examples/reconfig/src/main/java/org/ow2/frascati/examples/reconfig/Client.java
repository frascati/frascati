/**
 * OW2 FraSCAti Examples : Fscript reconfiguration
 * Copyright (C) 2008-2010 INRIA, Univeristy of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.reconfig;

import javax.script.ScriptException;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.examples.reconfig.converter.CurrencyConverter;
import org.ow2.frascati.examples.reconfig.converter.fscript.ExchangeRateUpdater;

/**
 * A simple client providing a Runnable interface to easily run the whole demo.
 * 
 * @author Christophe Demarey
 */
public class Client
  implements Runnable
{
    @Reference
    private CurrencyConverter converter;

    @Reference
    private ExchangeRateUpdater updater;

    @Reference
    private ExchangeRateUpdater updaterScript;

    private static final double NEW_RATE = 0.823;
    private static final double NEW_RATE_V2 = 0.698177756;
    
    /**
     * Default constructor.
     */
    public Client() { }
    
    /**
     * Run the client.
     */
    public final void run()
    {
        double result;
        
        result = converter.dollarToEuro(100);
        java.io.PrintStream out = System.out;
        out.println("100$ are worth about "+result+"€.");
        
        out.println("Updating exchange rate to "+NEW_RATE+" ...");
        try {
            updater.updateDollarExchangeRate(NEW_RATE);
            out.println(" Done!");
            result = converter.dollarToEuro(100);
            out.println("100$ are now worth about "+result+"€.");
        } catch (ScriptException e) {
            out.println("Reconfiguration failed!");
            e.printStackTrace();
        }
        
        out.println("Updating exchange rate to "+NEW_RATE_V2+" ...");
        try {
        	updaterScript.updateDollarExchangeRate(NEW_RATE_V2);
            out.println("Done!");
            result = converter.dollarToEuro(100);
            out.println("100$ are now worth about "+result+"€.");        
        } catch (ScriptException e) {
            out.println("Reconfiguration failed!");
            e.printStackTrace();
        }
    }
    
}
