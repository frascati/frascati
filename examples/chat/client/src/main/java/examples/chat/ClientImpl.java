/**
 * OW2 FraSCAti Examples: Chat
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org 
 *
 * Author(s): Christophe Demarey
 */

package examples.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * A chat client.
 * 
 * @author Christophe Demarey.
 */
@Scope("COMPOSITE")
public class ClientImpl
  implements Runnable
{
    // Attributes
    /** The header that will be displayed in front of each message. */
    @Property
    private String header = "> ";
    @Reference
    private Chat chatService;
    
    // Constructors
    public ClientImpl() { }
    
    // Methods
    private void printMessages(String messages)
    {
      StringTokenizer st = new StringTokenizer(messages, "\n");
      
      while (st.hasMoreTokens()) {
        System.out.println(this.header + st.nextToken());
      }
    }
    
    /**
     * Run method launching the application.
     */
    public final void run()
    {
        //  open up standard input
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String msg = null,
               previousMessages = "";
        
        System.out.println("Chat client ready!");
        do {
            //  read the message from the command-line
            try {
               msg = br.readLine();
               chatService.sendMessage( System.getProperty("user.name"), msg);
            } catch (IOException ioe) {
               System.out.println("IO error!");
               break; // the do/while loop.
            }
            
            msg = chatService.getMessages();
            printMessages( msg.substring(previousMessages.length()) );
            previousMessages = msg;
        } while(msg != null);
    }
    
}
