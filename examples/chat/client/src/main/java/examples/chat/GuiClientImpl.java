/**
 * OW2 FraSCAti Examples: Chat
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org 
 *
 * Author(s): Christophe Demarey
 */

package examples.chat;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

/**
 * This component acts like a proxy for the Chat service.
 * This is necessary because SwingAppFramework has its own
 * instantiation mechanism, confusing with the SCA scope/instantiation
 * mechanism. 
 * 
 * @author Christophe Demarey.
 */
@Scope("COMPOSITE")
public class GuiClientImpl
  implements Chat
{
    // Attributes
    /** The header that will be displayed in front of each message. */
    @Property
    private String header;
    @Reference
    private Chat chatService;
    @Reference
    private ChatManagement chatManagementService;
    
    // Constructors
    public GuiClientImpl() { }
    
    // Methods from the ChatClient interface
    
    /**
     * @see examples.chat.Chat#connect(String)
     */
    public final void connect() {
    	chatManagementService.connect();
    }
    
    /**
     * @see examples.chat.Chat#disconnect()
     */
    public final void disconnect() {
    	chatManagementService.disconnect();
    }

    /**
     * @see examples.chat.Chat#getMessages()
     */
    public final String getMessages() {
      return chatService.getMessages();
    }

    /**
     * @see examples.chat.Chat#sendMessage(String)
     */
    public final void sendMessage(String pseudo, String msg) {
      chatService.sendMessage(pseudo, msg);
    }    
}
