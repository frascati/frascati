/**
 * OW2 FraSCAti Examples : chat
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Contact: frascati@ow2.org 
 *
 * Author(s): Christophe Demarey
 * 
 * Contributor(s): Nicolas Dolet
 */

package examples.chat.client.gui;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.annotations.Scope;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

import examples.chat.Chat;

/**
 * The main class of the GUI.
 * It implements the ChatServer interface in order to provide
 * SCA service to the GUI.
 */
@Scope("COMPOSITE")
public class ChatGUI 
     extends SingleFrameApplication 
     implements Runnable
{
    /** A reference to the Chat service. */
    private static Chat service;

    /**
     * Default constructor.
     */
    public ChatGUI() { }
    
    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected final void startup() {
      show(new ChatView(this));
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected final void configureWindow(java.awt.Window root)
    {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of ChatGUI
     */
    public static ChatGUI getApplication()
    {
      return Application.getInstance(ChatGUI.class);
    }

    /**
     * A convenient static getter for the chat service.
     * @return the reference for the chat service.
     */
    public static Chat getChatService()
    {
      return getApplication().service;
    }

    /**
     * 
     */
    public final void run()
    {
      launch(ChatGUI.class, new String[0]);  
    }

    /**
     * Main method launching the application.
     * @throws FrascatiException if the SCA factory or the composite cannot be loaded
     */
    public static void main(String[] args) throws FrascatiException
    {
      // Instantiate the client composite
      String compositeName = "chat-gui-client",
             serviceName = "chatService";
    
      FraSCAti frascati = FraSCAti.newFraSCAti();
      Component c = frascati.getComposite(compositeName);

      service = frascati.getService(c, serviceName, Chat.class);
      
      // Run the GUI
      launch(ChatGUI.class, args);
    }

}
