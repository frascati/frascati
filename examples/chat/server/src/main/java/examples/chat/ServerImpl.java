/**
 * OW2 FraSCAti Examples: Chat Application
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 *
 */

package examples.chat;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

/**
 * The chat Server implementation.
 * 
 * @author Christophe Demarey.
 */
@Scope("COMPOSITE")
@Service(interfaces={examples.chat.Chat.class, examples.chat.ChatManagement.class})
public class ServerImpl
  implements Chat
{
    // Attributes
    /**
     * The header that will be displayed in front of each message. 
     */
    @Property
    private String header = ":";

    /**
     * The list of messages received by the server.
     */
    private StringBuffer messages;
    
    // Constructors
    public ServerImpl()
    {
        messages = new StringBuffer();
    }
    
    // Methods

    /**
     * @see examples.chat.Chat#connect(String)
     */
    public final void connect() { }
    
    /**
     * @see examples.chat.Chat#disconnect()
     */
    public final void disconnect() { }

    /**
     * @see examples.chat.Chat#getMessages()
     */
    public final String getMessages()
    {
        return messages.toString();
    }

    /**
     * @see examples.chat.Chat#sendMessage(String)
     */
    public final void sendMessage(String pseudo, String msg)
    {
        messages.append(pseudo).append(header).append(msg).append("\n"); 
    }
}
