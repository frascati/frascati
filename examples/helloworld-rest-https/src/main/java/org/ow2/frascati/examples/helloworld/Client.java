/**
 * OW2 FraSCAti Examples: HelloWorld REST with HTTPS
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld;

import org.oasisopen.sca.annotation.Init;
import org.oasisopen.sca.annotation.Reference;

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
public class Client
  implements Runnable
{
  // --------------------------------------------------------------------------
  // SCA Reference.
  // --------------------------------------------------------------------------

  private HelloService helloService;
  
  @Reference(name="HelloService") 
  public final void setHelloService(HelloService helloService)
  {
    this.helloService = helloService;
    System.out.println("Client setHelloService(" + this.helloService + ").");
  }

  //--------------------------------------------------------------------------
  // Default constructor.
  // --------------------------------------------------------------------------

  public Client()
  {
    System.out.println("Client created.");
  }

  @Init
  public final void init()
  {
    System.out.println("Client initialized.");
  }

  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface.
  // --------------------------------------------------------------------------

  public final void run()
  {
    System.out.println("Call the HelloService...");
    helloService.print("hello world");
  }
}
