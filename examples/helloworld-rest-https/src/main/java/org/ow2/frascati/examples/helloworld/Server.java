/**
 * OW2 FraSCAti Examples: HelloWorld REST with HTTPS
 * Copyright (C) 2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld;

import org.osoa.sca.annotations.Property;
//import org.oasisopen.sca.annotation.Property; // BUG: property 'count' is mandatory.

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(HelloService.class)
public class Server
  implements HelloService
{
  // --------------------------------------------------------------------------
  // SCA Properties.
  // --------------------------------------------------------------------------

  private String header = "->";

  private int count = 1;

  public final String getHeader()
  {
    return header;
  }

  @Property
  public final void setHeader(final String header)
  {
    this.header = header;
  }

  public final int getCount()
  {
    return count;
  }

  @Property
  public final void setCount(final int count)
  {
    this.count = count;
  }

  // --------------------------------------------------------------------------
  // Default constructor.
  // --------------------------------------------------------------------------

  public Server()
  {
    System.out.println("Server created.");
  }

  // --------------------------------------------------------------------------
  // Implementation of the HelloService interface.
  // --------------------------------------------------------------------------

  public final void print(final String msg)
  {
    System.out.println("Server: begin printing...");
    for (int i = 0; i < (count); ++i) {
      System.out.println(((header) + msg));
    }
    System.out.println("Server: print done.");
  }

}
