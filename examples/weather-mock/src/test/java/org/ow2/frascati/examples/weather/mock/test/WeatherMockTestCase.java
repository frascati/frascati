/**
 * OW2 FraSCAti Examples: Weather Mock
 * Copyright (C) 2011-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.examples.weather.mock.test;

import java.net.URL;

import net.webservicex.GlobalWeather;
import net.webservicex.GlobalWeatherSoap;

import org.junit.Test;

import org.ow2.frascati.test.FraSCAtiTestCase;

public class WeatherMockTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testService() throws Exception
  {
	// Call the original Web service.
    GlobalWeather gw = new GlobalWeather();
    GlobalWeatherSoap gws = gw.getGlobalWeatherSoap();
    System.out.println("Result from the original Web service: " + gws.getWeather("Paris", "France"));

	// Call the mocked Web service.
	gw = new GlobalWeather(new URL("http://localhost:8765/Weather?wsdl"));
    gws = gw.getGlobalWeatherSoap();
    System.out.println("Result from the mocked Web service: " + gws.getWeather("Paris", "France"));
  }
}
