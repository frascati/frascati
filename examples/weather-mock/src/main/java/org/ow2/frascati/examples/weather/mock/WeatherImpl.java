/**
 * OW2 FraSCAti Examples: Weather Mock
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): 
 *
 */
package org.ow2.frascati.examples.weather.mock;

import java.util.Date;

import net.webservicex.GlobalWeatherSoap;

import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.oasisopen.sca.annotation.Property;

@Scope("COMPOSITE")
@Service(GlobalWeatherSoap.class)
public class WeatherImpl implements GlobalWeatherSoap
{
    // ---------------------------------------------------------------------------
	// Internal state.
    // ---------------------------------------------------------------------------

	/**
	 * Cities property.
	 */
	@Property(name = "Cities")
	private String cities;

	/**
	 * Wind property.
	 */
	@Property(name = "Wind")
	private String wind;

	/**
	 * Visibility property.
	 */
	@Property(name = "Visibility")
	private String visibility;

	/**
	 * SkyConditions property.
	 */
	@Property(name = "SkyConditions")
	private String skyConditions;

	/**
	 * Temperature property.
	 */
	@Property(name = "Temperature")
	private String temperature;

	/**
	 * DewPoint property.
	 */
	@Property(name = "DewPoint")
	private String dewPoint;

	/**
	 * RelativeHumidity property.
	 */
	@Property(name = "RelativeHumidity")
	private int relativeHumidity;

	/**
	 * Pressure property.
	 */
	@Property(name = "Pressure")
	private String pressure;

	// ---------------------------------------------------------------------------
	// Internal methods.
    // ---------------------------------------------------------------------------

    // ---------------------------------------------------------------------------
	// Public methods.
    // ---------------------------------------------------------------------------

	/**
	 * @see GlobalWeatherSoap#getCitiesByCountry(String)
	 */
	public String getCitiesByCountry(String countryName)
    {
      StringBuilder sb = new StringBuilder();
      sb.append("<NewDataSet>\n");
      for(String cityName : cities.split(":")) {
        sb.append("<Table>\n");
        sb.append("  <Country>");
        sb.append(countryName);
        sb.append("</Country>\n");
        sb.append("  <City>");
        sb.append(cityName);
        sb.append("</City>\n");
        sb.append("</Table>\n");
    	  
      }
      sb.append("</NewDataSet>\n");
      return sb.toString();
    }

	/**
	 * @see GlobalWeatherSoap#getWeather(String, String)
	 */
    public String getWeather(String cityName, String countryName)
    {
      StringBuilder sb = new StringBuilder();
      sb.append("<?xml version=\"1.0\" encoding=\"utf-16\"?>\n");
      sb.append("<CurrentWeather>\n");
      sb.append("  <Location>");
      sb.append(cityName);
      sb.append(',');
      sb.append(countryName);
      sb.append(" (LFQQ) 50-34N 003-06E 52M</Location>\n");
      sb.append("  <Time>");
      sb.append(new Date().toString());
      sb.append("</Time>\n");
      sb.append("  <Wind>");
      sb.append(this.wind);
      sb.append("</Wind>\n");
      sb.append("  <Visibility>");
      sb.append(this.visibility);
      sb.append("</Visibility>\n");
      sb.append("  <SkyConditions>");
      sb.append(this.skyConditions);
      sb.append("</SkyConditions>\n");
      sb.append("  <Temperature>");
      sb.append(this.temperature);
      sb.append("</Temperature>\n");
      sb.append("  <DewPoint>");
      sb.append(this.dewPoint);
      sb.append("</DewPoint>\n");
      sb.append("  <RelativeHumidity>");
      sb.append(this.relativeHumidity);
      sb.append("%</RelativeHumidity>\n");
      sb.append("  <Pressure>");
      sb.append(this.pressure);
      sb.append("</Pressure>\n");
      sb.append("  <Status>Success</Status>\n");
      sb.append("</CurrentWeather>\n");
      return sb.toString();
    }
}
