/**
 * OW2 FraSCAti Examples: HelloWorld Web Service Failover
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.ws.failover;

import org.apache.hello_world_soap_http.Greeter;

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(Runnable.class)
public class ClientImpl
  implements Runnable
{
    // --------------------------------------------------------------------------
    // SCA
    // --------------------------------------------------------------------------

    @org.oasisopen.sca.annotation.Reference
    private Greeter greeter;

    @org.oasisopen.sca.annotation.Property
    private int numberOfCallsToGreeters = 10;

    // --------------------------------------------------------------------------
    // Implementation of the Runnable interface
    // --------------------------------------------------------------------------

    public final void run()
    {
      for(int i=0; i<numberOfCallsToGreeters; i++) {
        System.out.println("ClientImpl - Call greetMe(FraSCAti)...");
        String greetMe = greeter.greetMe("FraSCAti");
        System.out.println("ClientImpl - Received " + greetMe);
      }
    }
}
