/**
 * OW2 FraSCAti Examples: HelloWorld Web Service Addressing
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld.wsa;

import org.apache.hello_world_soap_http.Greeter;
import org.apache.hello_world_soap_http.PingMeFault;

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(Runnable.class)
public class ClientImpl
  implements Runnable
{
  // --------------------------------------------------------------------------
  // SCA Reference
  // --------------------------------------------------------------------------

  @org.oasisopen.sca.annotation.Reference
  private Greeter greeter;
  
  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------

  public final void run()
  {
    System.out.println("ClientImpl - Call pingMe()...");
    try {
      greeter.pingMe();
    } catch(PingMeFault pmf) {
      System.out.println("ClientImpl - Received " + pmf);
    }
    
    System.out.println("ClientImpl - Call sayHi()...");
    String sayHi = greeter.sayHi();
    System.out.println("ClientImpl - Received " + sayHi);

    System.out.println("ClientImpl - Call greetMeOneWay(FraSCAti)...");
    greeter.greetMeOneWay("FraSCAti");

    System.out.println("ClientImpl - Call greetMe(FraSCAti)...");
    String greetMe = greeter.greetMe("FraSCAti");
    System.out.println("ClientImpl - Received " + greetMe);
  }
}
