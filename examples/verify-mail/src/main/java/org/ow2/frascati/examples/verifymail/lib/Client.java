/**
 * OW2 FraSCAti Examples: Verify mail
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.examples.verifymail.lib;

import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.examples.verifymail.api.Verifier;

import com.xwebservices.ws.xwebemailvalidation.emailvalidation.v2.XWebEmailValidationInterface;
import com.xwebservices.ws.xwebemailvalidation.emailvalidation.v2.messages.ObjectFactory;
import com.xwebservices.ws.xwebemailvalidation.emailvalidation.v2.messages.ValidateEmailRequest;
import com.xwebservices.ws.xwebemailvalidation.emailvalidation.v2.messages.ValidateEmailResponse;

/**
 * SCA Java Component implementation using a mail verification web service
 */
public class Client
  implements Verifier
{
  @Reference
  private XWebEmailValidationInterface mailValidator;
  
  // --------------------------------------------------------------------------
  // Implementation of the Verifier interface
  // --------------------------------------------------------------------------

  public final String verify(String mail)
  {
    ValidateEmailRequest request = new ObjectFactory().createValidateEmailRequest();
    request.setEmail(mail);
    ValidateEmailResponse response = mailValidator.validateEmail(request);
    String result = response.getStatus();
    System.out.println("The mail " + mail + " is " + result);
    return result;
  }

}
