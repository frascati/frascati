/**
 * OW2 FraSCAti Examples: HelloWorld with JNA
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Romain Rouvoy
 *         
 * Contributor(s): Philippe Merle
 *                 
 */
package native_.hw;

import org.osoa.sca.annotations.Reference;

import stdio.StdioLibrary;
 
/**
 * Simple example of a SCA component declaring and using a native library to
 * display an hello, world! message.
 *
 * @Author <a href="romain.rouvoy@lifl.fr">Romain Rouvoy</a>
 */
public class Client
  implements Runnable
{
    @Reference
    private StdioLibrary service ;
     
    public final void run()
    {
        service.printf("Hello, World!\n");
    } 
}
