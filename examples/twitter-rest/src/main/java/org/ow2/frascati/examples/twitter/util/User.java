/**
 * OW2 FraSCAti Examples: Twitter
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.examples.twitter.util;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Philippe Merle
 */
@XmlRootElement
public class User
{
    @XmlElement(name="id")
    private String id;
    @XmlElement(name="name")
    private String name;
    @XmlElement(name="screen_name")
    private String screenName;
    @XmlElement(name="location")
    private String location;
    @XmlElement(name="description")
    private String description;
    @XmlElement(name="profile_image_url")
    private String profileImageUrl; // TODO: get as a URL also.
    @XmlElement(name="url")
    private String url; // TODO: get as a URL also.
    @XmlElement(name="protected")
    private boolean isProtected;
    @XmlElement(name="followers_count")
    private int followersCount;
    @XmlElement(name="profile_background_color")
    private String profileBackgroundColor; // COLOR
    @XmlElement(name="profile_text_color")
    private String profileTextColor; // COLOR
    @XmlElement(name="profile_link_color")
    private String profileLinkColor; // COLOR
    @XmlElement(name="profile_sidebar_fill_color")
    private String profileSidebarFillColor; // COLOR
    @XmlElement(name="profile_sidebar_border_color")
    private String profileSidebarBorderColor; // COLOR
    @XmlElement(name="friends_count")
    private int friendsCount;
    @XmlElement(name="created_at")
    private String createdAt; // TODO: get as a DATE also.
    @XmlElement(name="favourites_count")
    private int favouritesCount;
    @XmlElement(name="utc_offset")
    private int utcOffset;
    @XmlElement(name="time_zone")
    private String timeZone;
    @XmlElement(name="profile_background_image_url")
    private String profileBackgroundImageUrl; // TODO: get as a URL also.
    @XmlElement(name="profile_background_tile")
    private boolean profileBackgroundTile;
    @XmlElement(name="statuses_count")
    private int statusesCount;
// TO BE MAPPED   <notifications></notifications>
    @XmlElement(name="verified")
    private boolean verified;
// TO BE MAPPED    <following></following>
    @XmlElement(name="status")
    private Status status;

    public final String getId()
    {
      return id;
    }

    public final String getName()
    {
      return name;
    }

    public final String getScreenName()
    {
      return screenName;
    }

    public final String getLocation()
    {
      return location;
    }

    public final String getDescription()
    {
      return description;
    }

    public final String getProfileImageUrl()
    {
      return profileImageUrl;
    }

    public final String getUrl()
    {
      return url;
    }

    public final boolean isProtected()
    {
      return isProtected;
    }

    public final int getFollowersCount()
    {
      return followersCount;
    }

    public final String getProfileBackgroundColor()
    {
      return profileBackgroundColor;
    }

    public final String getProfileTextColor()
    {
      return profileTextColor;
    }

    public final String getProfileLinkColor()
    {
      return profileLinkColor;
    }

    public final String getProfileSidebarFillColor()
    {
      return profileSidebarFillColor;
    }

    public final String getProfileSidebarBorderColor()
    {
      return profileSidebarBorderColor;
    }

    public final int getFriendsCount()
    {
      return friendsCount;
    }

    public final String getCreatedAt()
    {
      return createdAt;
    }

    public final int getFavouritesCount()
    {
      return favouritesCount;
    }

    public final int getUtcOffset()
    {
      return utcOffset;
    }

    public final String getTimeZone()
    {
      return timeZone;
    }

    public final String getProfileBackgroundImageUrl()
    {
      return profileBackgroundImageUrl;
    }

    public final boolean getProfileBackgroundTile()
    {
      return profileBackgroundTile;
    }

    public final int getStatusesCount()
    {
      return statusesCount;
    }

    public final boolean isVerified()
    {
      return verified;
    }

    public final Status getStatus()
    {
      return status;
    }
}
