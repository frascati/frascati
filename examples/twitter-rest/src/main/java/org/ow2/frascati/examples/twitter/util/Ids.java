/**
 * OW2 FraSCAti Examples: Twitter
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.examples.twitter.util;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Philippe Merle
 */
@XmlRootElement
public class Ids
{
    private Collection<String> ids;

    public final Collection<String> getId() {
        return ids;
    }

    public final void setId(Collection<String> c) {
        this.ids = c;
    }
}
