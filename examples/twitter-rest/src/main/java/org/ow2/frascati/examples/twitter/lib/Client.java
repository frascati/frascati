/**
 * OW2 FraSCAti Examples: Twitter
 * Copyright (C) 2009-2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.examples.twitter.lib;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.twitter.api.Twitter;
import org.ow2.frascati.examples.twitter.util.Ids;
import org.ow2.frascati.examples.twitter.util.User;
import org.ow2.frascati.examples.twitter.util.Status;

/**
 * A simple client for Twitter.
 * 
 * @author Philippe Merle
 */
@Service(Runnable.class)
public class Client implements Runnable {

  /** Reference to the Twitter services. */
  @Reference
  private Twitter twitter;

  /**
   * @see java.lang.Runnable#run()
   */
  public final void run() {
    final String userName = "vschiavoni";
    getUser(userName);
// Get Friends seems to require authentication.
//    getFriends(userName);
  }
  
  private void getUser(String userName) {
    java.io.PrintStream out = System.out;
    out.println("getUserInXml(" + userName + "): " + twitter.getUserInXml(userName));
    out.println("getUserInJSON(" + userName + "): " + twitter.getUserInJSON(userName));
    User user = twitter.getUser(userName);
    out.println(userName + ".id=" + user.getId());
    out.println(userName + ".name=" + user.getName());
    out.println(userName + ".screen_name=" + user.getScreenName());
    out.println(userName + ".location=" + user.getLocation());
    out.println(userName + ".description=" + user.getDescription());
    out.println(userName + ".profile_image_url=" + user.getProfileImageUrl());
    out.println(userName + ".url=" + user.getUrl());
    out.println(userName + ".protected=" + user.isProtected());
    out.println(userName + ".followers_count=" + user.getFollowersCount());
    out.println(userName + ".profile_background_color=" + user.getProfileBackgroundColor());
    out.println(userName + ".profile_text_color=" + user.getProfileTextColor());
    out.println(userName + ".profile_link_color=" + user.getProfileLinkColor());
    out.println(userName + ".profile_sidebar_fill_color=" + user.getProfileSidebarFillColor());
    out.println(userName + ".profile_sidebar_border_color=" + user.getProfileSidebarBorderColor());
    out.println(userName + ".friends_count=" + user.getFriendsCount());
    out.println(userName + ".created_at=" + user.getCreatedAt());
    out.println(userName + ".favourites_count=" + user.getFavouritesCount());
    out.println(userName + ".utc_offset=" + user.getUtcOffset());
    out.println(userName + ".time_zone=" + user.getTimeZone());
    out.println(userName + ".profile_background_image_url=" + user.getProfileBackgroundImageUrl());
    out.println(userName + ".profile_background_tile=" + user.getProfileBackgroundTile());
    out.println(userName + ".statuses_count=" + user.getStatusesCount());
    out.println(userName + ".verified=" + user.isVerified());
    Status status = user.getStatus();
    out.println(userName + ".status.created_at=" + status.getCreatedAt());
    out.println(userName + ".status.id=" + status.getId());
    out.println(userName + ".status.text=" + status.getText());
    out.println(userName + ".status.source=" + status.getSource());
    out.println(userName + ".status.truncated=" + status.isTruncated());
    out.println(userName + ".status.in_reply_to_status_id=" + status.getInReplyToStatusId());
    out.println(userName + ".status.in_reply_to_user_id=" + status.getInReplyToUserId());
    out.println(userName + ".status.favorited=" + status.isFavorited());
    out.println(userName + ".status.in_reply_to_screen_name=" + status.getInReplyToScreenName());
  }

  private void getFriends(String userName) {
    java.io.PrintStream out = System.out;
      out.println("getFriendsInXml(" + userName + "): " + twitter.getFriendsInXml(userName));
      out.println("getFriendsInJSON(" + userName + "): " + twitter.getFriendsInJSON(userName));
      Ids friends = twitter.getFriends(userName);
      out.println("getFriends(" + userName + "): " + friends.getId());
/* Removed as running this implies too many trafic.
      for(String id : friends.getId()) {
        getUser(id);
      }
*/
    }

}
