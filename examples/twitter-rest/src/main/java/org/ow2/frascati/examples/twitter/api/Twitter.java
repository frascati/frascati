/**
 * OW2 FraSCAti Examples: Twitter
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.examples.twitter.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.ow2.frascati.examples.twitter.util.Ids;
import org.ow2.frascati.examples.twitter.util.User;

/**
 * A simple API for Twitter.
 *
 * TODO: Must be extended with other RESTful methods of Twitter.
 *
 * @author Philippe Merle
 */
public interface Twitter
{
  /**
   * Returns extended information of a given user in XML format.
   */
  @GET
  @Path("/users/{id}.xml")
  @Produces("text/xml")
  String getUserInXml(@PathParam("id") String id);

  /**
   * Returns extended information of a given user in JSON format.
   */
  @GET
  @Path("/users/{id}.json")
  @Produces("application/json")
  String getUserInJSON(@PathParam("id") String id);

  /**
   * Returns extended information of a given user.
   */
  @GET
  @Path("/users/{id}.xml")
  User getUser(@PathParam("id") String id);

  /**
   * Returns the list of friends of a given user in XML format.
   */
  @GET
  @Path("/friends/ids/{id}.xml")
  @Produces("text/xml")
  String getFriendsInXml(@PathParam("id") String id);

  /**
   * Returns the list of friends of a given user in JSON format.
   */
  @GET
  @Path("/friends/ids/{id}.json")
  @Produces("application/json")
  String getFriendsInJSON(@PathParam("id") String id);

  /**
   * Returns the list of friends of a given user.
   */
  @GET
  @Path("/friends/ids/{id}.xml")
  Ids getFriends(@PathParam("id") String id);
}
