/**
 * OW2 FraSCAti Examples: Twitter
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.twitterrest;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;

public class TwitterRestTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testService()
  {
    // Try 3 times to reach the service before giving up
    int nbTries = 3;
    boolean success = false;
    while (nbTries > 0 && !success) {
      try {
        getService(Runnable.class,"r").run();
        success = true;
      } catch (RuntimeException e) {
        System.err.println("Cannot get the service response...");
        System.err.println("Number of tries left: " + (nbTries - 1));
        // Sleep 15 seconds
        try {
          Thread.currentThread().sleep(15000);
        } catch (InterruptedException ie) {
          throw new Error(ie);
        }
      }
      nbTries--;
    }
    Assert.assertNotSame("Service unavailable after 3 tries", 0, nbTries);
  }
}
