============================================================================
OW2 FraSCAti Examples: Twitter and Weather Orchestration
Copyright (C) 2009-2012 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Nicolas Dolet

Contributor(s): Philippe Merle

============================================================================

Twitter and Weather Orchestration Client:
-----------------------------------------

This example shows how to orchestrate the Twitter RESTful service with the
Weather Web service.

The orchestration component retrieves the city name of a given Twitter user,
then calls the Weather service for the user city.

The twitter-weather SCA composite also exposes its orchestration as an SCA
service, using two different protocols: SOAP and RESTful.

Requirements:
-------------
JDK >= 1.6

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun
  
  This command launches the twitter-weather SCA composite and call the 'tw'
  SCA service exposed with the SOAP and RESTful bindings. That's why this
  command is not giving back control until you press Ctrl+C.
  
  These services are now accessible:

    1) from an external SOAP client with WSDL located at  http://localhost:18000/ws/TwitterWeather?wsdl

    2) as a standalone RESTful service available at http://localhost:18000/rest/TwitterWeather
       and providing the following method:
         Resource/Operation: /weather/<userId>
             Description GET: Return the weather for a given Twitter user
             Query parameters: 
               * userId - the id of the Twitter user
             Result representation: Text 

  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Test the service with an HTML page:
-----------------------------------
  In a Web browser, open the index.html file located in this directory, then
  fill the form to use the TwitterWeather RESTful service.

Compilation and execution with the FraSCAti script:
---------------------------------------------------
  frascati wsdl2java -u http://www.webservicex.net/globalweather.asmx?wsdl -o src/main/java
  frascati compile src twitterweather
  frascati run twitter-weather -libpath twitterweather.jar -s tw -m getWeatherForUser -p vschiavoni
