/**
 * OW2 FraSCAti Examples: Twitter and Weather orchestration
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 *
 */

package org.ow2.frascati.examples.twitterweather.xml;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author Philippe Merle
 */
@XmlRootElement
public class Status
{
    @XmlElement(name="created_at")
    private String createdAt; // TODO: get as a DATE also.
    @XmlElement(name="id")
    private String id;
    @XmlElement(name="text")
    private String text;
    @XmlElement(name="source")
    private String source;
    @XmlElement(name="truncated")
    private boolean truncated;
    @XmlElement(name="in_reply_to_status_id")
    private String inReplyToStatusId;
    @XmlElement(name="in_reply_to_user_id")
    private String inReplyToUserId;
    @XmlElement(name="favorited")
    private boolean favorited;
    @XmlElement(name="in_reply_to_screen_name")
    private String inReplyToScreenName;

    public final String getCreatedAt()
    {
      return createdAt;
    }

    public final String getId()
    {
      return id;
    }

    public final String getText()
    {
      return text;
    }

    public final String getSource()
    {
      return source;
    }

    public final boolean isTruncated()
    {
      return truncated;
    }

    public final String getInReplyToStatusId()
    {
      return inReplyToStatusId;
    }

    public final String getInReplyToUserId()
    {
      return inReplyToUserId;
    }

    public final boolean isFavorited()
    {
      return favorited;
    }

    public final String getInReplyToScreenName()
    {
      return inReplyToScreenName;
    }
}
