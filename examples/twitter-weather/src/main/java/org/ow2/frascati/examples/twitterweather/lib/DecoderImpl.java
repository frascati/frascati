/**
 * OW2 FraSCAti Examples: Twitter and Weather orchestration
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.examples.twitterweather.lib;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.ow2.frascati.examples.twitterweather.api.Decoder;
import org.ow2.frascati.examples.twitterweather.xml.Locations;

/**
 * Simple implementation {@link Decoder}.
 * 
 * @author Nicolas Dolet
 *
 */
public class DecoderImpl
  implements Decoder
{
  /**
   * @see Decoder#decode(String)
   */
  public final Locations decode(String message) {
    InputStream is = new ByteArrayInputStream(message.getBytes());
    try {
      JAXBContext context = JAXBContext.newInstance(Locations.class);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      return (Locations) unmarshaller.unmarshal(is);
    } catch (JAXBException e) {
      System.err.println("Unable to decode server message.");
      return null;
    }
  }

}
