/**
 * OW2 FraSCAti Examples: Twitter and Weather orchestration
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.examples.twitterweather.api;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.osoa.sca.annotations.Service;

/**
 * Simple interface for Twitter and Weather services orchestration
 * 
 * @author Nicolas Dolet
 *
 */
@Service
@WebService
public interface TwitterWeather
{  
  /**
   * 
   * Return the weather for a given Twitter user
   * @param userId the id of the Twitter user
   * @return The weather in <code>userId</code> city
   */
  @GET
  @Path("/weather/{userId}")
  @Produces("text/plain")
  @WebMethod
  String getWeatherForUser(@PathParam("userId") String userId);
  
}
