/**
 * OW2 FraSCAti Examples: Twitter and Weather orchestration
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 * 
 * Contributor: Philippe Merle
 */

package org.ow2.frascati.examples.twitterweather.lib;

import java.util.Locale;

import net.webservicex.GlobalWeatherSoap;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.examples.twitterweather.api.Decoder;
import org.ow2.frascati.examples.twitterweather.api.Twitter;
import org.ow2.frascati.examples.twitterweather.api.TwitterWeather;
import org.ow2.frascati.examples.twitterweather.xml.Location;
import org.ow2.frascati.examples.twitterweather.xml.Locations;
import org.ow2.frascati.examples.twitterweather.xml.User;

/**
 * A client that orchestrates the Weather service with the Twitter one.
 * 
 * @author Nicolas Dolet
 */
public class Orchestration
  implements TwitterWeather
{
  /**
   * Reference to the Twitter service.
   */
  @Reference private Twitter twitter;
  
  /**
   * Refernce to the decoder service
   */
  @Reference private Decoder decoder;
  
  /**
   * Reference to the Weather service.
   */
  @Reference private GlobalWeatherSoap weather;
  
  /**
   * @see TwitterWeather#getWeatherForUser(String)
   */
  public final String getWeatherForUser(String userId)
  {
    // Call the twitter RESTful service
    User user = twitter.getUser(userId);
    if (user.getLocation().equals("")) {
      System.err.println("The user " + userId
          + " has not filled in his location");
      return "N/A";
    }

    // Compute the twitter response
    // The location is the city name and the country name, separated by a comma
    String[] location = user.getLocation().split("[\\s]*,[\\s]*", 2);
    String cityName = location[0];
    String countryName = location[1];
    System.out.println("User '" + userId + "' is living in " + cityName
        + " (" + countryName + ")");
    
    // Call the Weather Web service to get available cities in the country of
    // the Twitter user
    String cities = weather.getCitiesByCountry(countryName);
    Locations l = decoder.decode(cities);
    
    // For each weather city, display its weather if its name matches with the
    // user city name
    StringBuffer result = new StringBuffer();
    if (l != null) {
      boolean done = false;
      for (Location loc : l.location) {
        if (loc.city.value.toLowerCase(Locale.US).contains(cityName.toLowerCase(Locale.US))) {
          result.append("Current weather in ").append(loc.city.value).append(":\n");
          result.append(weather.getWeather(loc.city.value, countryName)).append('\n');
          done = true;
        }
      }
      if (!done) {
        System.err.println("Unable to find '" + cityName
            + "' in available cities of the weather service");
      }
    }
    return result.toString();
  }
  
}
