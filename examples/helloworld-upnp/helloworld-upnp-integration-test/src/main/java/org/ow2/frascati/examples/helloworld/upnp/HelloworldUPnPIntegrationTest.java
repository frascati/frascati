/**
 * OW2 FraSCAti 
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): 
 *
 */

package org.ow2.frascati.examples.helloworld.upnp;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.remote.introspection.Deployment;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.remote.introspection.stringlist.StringList;
import org.ow2.frascati.remote.introspection.util.FileUtil;

/**
 *
 */
public class HelloworldUPnPIntegrationTest implements Runnable
{
    private static final String RESOURCES_FOLDER="target/resource-classes/";
    
    @Reference(name="deployment")
    private Deployment deployment;

    @Reference(name="introspection")
    private RemoteScaDomain introspection;
    
    /**
     * @see java.lang.Runnable#run()
     */
    public void run()
    {
        try
        {
            this.deployComposite("helloworld-upnp-server", "helloworld-upnp-server");
            this.deployComposite("helloworld-upnp-client", "helloworld-upnp-client");
            MultivaluedMap<String, String> invokationParams = new MetadataMap<String, String>();
            invokationParams.add("methodName", "run");
            introspection.invokeMethod("helloworld-upnp-client/r", invokationParams);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * @see org.ow2.frascati.examples.helloworld.upnp.HelloworldUPnPIntegrationTestItf#deployContribution(java.lang.String)
     */
    public StringList deployContribution(String contributionFileName) throws IOException
    {
        File composite=getResource(contributionFileName+".zip");
        String encodedContributionFile = FileUtil.getStringFromFile(composite);
        
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("contribution", encodedContributionFile);
        return deployment.deployContribution(params);
    }

    /***
     * @throws IOException 
     * @see org.ow2.frascati.examples.helloworld.upnp.HelloworldUPnPIntegrationTestItf#deployComposite(java.lang.String, java.lang.String)
     */
    public String deployComposite(String compositeFinaleName, String compositeName) throws IOException
    {
        File composite=getResource(compositeFinaleName+".jar");
        String encodedCompositeFile = FileUtil.getStringFromFile(composite);
        
        MultivaluedMap<String, String> params=new MetadataMap<String, String>();
        params.add("compositeName", compositeName);
        params.add("jar", encodedCompositeFile);
        return deployment.deployComposite(params);
    }
    
    private File getResource(String resourceName)
    {
        String resourcePath=RESOURCES_FOLDER+resourceName;
        File resource=new File(resourcePath);
        if(!resource.exists())
        {
            resourcePath="src/main/resources/"+resourceName;
            resource=new File(resourcePath);
        }
        return resource;
    }
}
