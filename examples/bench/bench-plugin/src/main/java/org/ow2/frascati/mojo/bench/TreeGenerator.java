/**
 * OW2 FraSCAti : Tree composite generator for bench mojo
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 */

package org.ow2.frascati.mojo.bench;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.maven.project.MavenProject;

public class TreeGenerator {
	private static void leaf(int current, int max, OutputStream outputStream)
			throws IOException {
		if (current < max) {
			String component = "<component name=\"Component"
					+ current
					+ "\"><implementation.java class=\"org.ow2.frascati.examples.bench.ServiceComponent\" /><service name=\"Service\"><interface.java interface=\"org.ow2.frascati.examples.bench.Service\" /></service>"
					+ "<reference name=\"ref\" multiplicity=\"0..n\"><interface.java interface=\"org.ow2.frascati.examples.bench.Service\"></interface.java></reference></component>";

			if ((2 * current) + 1 < max) {
				component = component + "<wire source=\"Component" + current
						+ "/ref\" target=\"Component" + ((2 * current) + 1)
						+ "/Service\"/>";

			}

			if ((2 * current) + 2 < max) {
				component = component + "<wire source=\"Component" + current
						+ "/ref\" target=\"Component" + ((2 * current) + 2)
						+ "/Service\"/>";
			}

			// component = component + "</component>";

			outputStream.write(component.getBytes());

			leaf((2 * current) + 1, max, outputStream);
			leaf((2 * current) + 2, max, outputStream);

		}

	}

	public static void generate(SCABenchGenerator generator) throws IOException {
		// Maven Project
		MavenProject project = generator.getProject();

		// Composite Size
		int size = generator.getCurrentSize();

		// Composite file name
		String filename;
		if (generator.getSize().length < 2)
			filename = generator.getName() + ".composite";
		else
			filename = generator.getName() + size + ".composite";

		// Get Base directory
		File baseDir = project.getBasedir();

		// Get Target directory
		File targetDir = new File(baseDir.getAbsolutePath() + File.separator
				+ "src" + File.separator + "main" + File.separator
				+ "resources");

		// Create Target directory if needed
		if (!targetDir.exists())
			targetDir.mkdirs();

		File ouputFile = new File(targetDir, filename);
		ouputFile.createNewFile();
		FileOutputStream outputStream = new FileOutputStream(ouputFile);

		String header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><composite xmlns=\""
				+ generator.getNamespace()
				+ "\" targetNamespace=\"http://bench\" name=\""
				+ generator.getName() + "\">";
		outputStream.write(header.getBytes());

		leaf(0, size, outputStream);

		String end = "<service name=\"s\" promote=\"Component0/Service\"><interface.java interface=\"org.ow2.frascati.examples.bench.Service\"/></service></composite>";

		outputStream.write(end.getBytes());
		outputStream.flush();
		outputStream.close();
	}
}
