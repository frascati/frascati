/**
 * OW2 FraSCAti : Maven mojo to generate SCA composite for benchmarking
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 */

package org.ow2.frascati.mojo.bench;

import java.io.IOException;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * A maven plugin for generating SCA composite for benchmark
 * 
 * @execute phase="initialize"
 * @requiresDependencyResolution runtime
 * @goal compile
 * @since 1.1
 */

public class SCABenchGenerator extends AbstractMojo {
	/**
	 * Name for the resulting composite file
	 * 
	 * @parameter expression="${parameters}" default-value="BenchMark"
	 * @since 1.3
	 */
	private String name;

	/**
	 * The architecture type to generate
	 * 
	 * @parameter expression="${parameters}" default-value="tree"
	 * @since 1.1
	 */
	private String type;

	/**
	 * Number of component for the generated sca file
	 * 
	 * @parameter expression="${parameters}"
	 * @since 1.4
	 */
	private Integer [] sizes;

	/**
	 * Default namespace for the generated composite
	 * 
	 * @parameter expression="${parameters}"
	 *            default-value="http://www.osoa.org/xmlns/sca/1.0"
	 * @since 1.3
	 */
	private String namespace;

	/**
	 * The Maven project reference.
	 * 
	 * @parameter expression="${project}"
	 * @required
	 * @since 1.0
	 */
	private MavenProject project;

	/**
	 * Current element size
	 */
	private int currentsize;

	public void execute() throws MojoExecutionException, MojoFailureException {
		for (int elt : sizes) {

			currentsize = elt;

			try {
				if (type.equalsIgnoreCase("chain")) {
					ChainGenerator.generate(this);
				}

				if (type.equalsIgnoreCase("tree")) {
					TreeGenerator.generate(this);
				}
				if (type.equalsIgnoreCase("pool")) {
					PoolGenerator.generate(this);
				}

				getLog()
						.info(
								"Generated " + name + " ( '"
										+ type.toLowerCase()
										+ "' architecture) with " + currentsize
										+ " component");

			} catch (IOException e) {
				getLog().error(e.getMessage());
			}

		}
	}

	/**
	 * Get composite file name
	 * 
	 * @since 1.3
	 */
	protected String getName() {
		return name;
	}

	/**
	 * Get maven project
	 * 
	 * @since 1.3
	 */
	protected MavenProject getProject() {
		return project;
	}

	/**
	 * Get desired composites sizes
	 * 
	 * @since 1.4
	 */
	protected Integer [] getSize() {
		return sizes;
	}
	
	/**
	 * Get desired current composite size
	 * 
	 * @since 1.4
	 */
	protected int getCurrentSize() {
		return currentsize;
	}

	/**
	 * Get composite namespace
	 * 
	 * @since 1.3
	 */
	protected String getNamespace() {
		return namespace;
	}
}
