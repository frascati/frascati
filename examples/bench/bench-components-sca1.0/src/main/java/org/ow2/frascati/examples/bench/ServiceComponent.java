/**
 * OW2 FraSCAti : Benchmark components implementation
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 */

package org.ow2.frascati.examples.bench;

import java.util.List;

import org.osoa.sca.annotations.Reference;

@org.osoa.sca.annotations.Service(Service.class)
public class ServiceComponent implements Service 
{
  @Reference(required = false)
  public List<Service> ref;

  public void call()
  {
	  for (Service s : ref) {
		  if (s !=null) s.call();
      }
  }
}
