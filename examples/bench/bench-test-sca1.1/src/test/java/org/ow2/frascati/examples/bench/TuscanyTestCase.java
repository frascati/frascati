/**
 * OW2 FraSCAti : Benchmark test for Tuscany Runtime
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 */

package org.ow2.frascati.examples.bench;

import org.apache.tuscany.sca.node.Client;
import org.apache.tuscany.sca.node.Contribution;
import org.apache.tuscany.sca.node.ContributionLocationHelper;
import org.apache.tuscany.sca.node.Node;
import org.apache.tuscany.sca.node.NodeFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.ow2.frascati.examples.bench.Service;

public class TuscanyTestCase
{
	private Service service;
	private Node node;
	private NodeFactory factory; 

	long startRuntime;
	long endRuntime;
	long startLoadingComposite;
	long endLoadingComposite;
	long startCall;
	long endCall;
	long end;

	@Before
	public void setUp() throws Exception
	{
		// Warmup
		factory = NodeFactory.newInstance();
		String contribution = ContributionLocationHelper.getContributionLocation(Service.class);
		System.out.println(contribution);
        //node = factory.createNode("BenchMark.composite", new Contribution("benchmark", contribution));
        node = factory.createNode("BenchMark.composite", getClass().getClassLoader());
		node.start();
		node.stop();
	}

	@After
	public void tearDown() throws Exception
	{
		System.out.println("\n######## Test Results ########");
		System.out.println("Time taken for creating runtime instance		: " + (endRuntime - startRuntime) * 1E-6 + "	ms");
		System.out.println("Time taken for creating composite instance		: " + (endLoadingComposite - startLoadingComposite) * 1E-6 + "	ms");
		System.out.println("Time taken for calling composite service interface	: " + (endCall - startCall) * 1E-6 + "	ms");
		System.out.println("Total time taken for running with Tuscany Runtime	: " + (end - startRuntime) * 1E-6 + "	ms\n");
	}

	@Test
	public void testCall()
	{
		// Start Test
		startRuntime = System.nanoTime();
		factory = NodeFactory.newInstance();
		endRuntime = System.nanoTime();
		
		startLoadingComposite = System.nanoTime();
		String contribution = ContributionLocationHelper.getContributionLocation(Client.class);
		node = factory.createNode("BenchMark.composite", getClass().getClassLoader());
		node.start();
		endLoadingComposite = System.nanoTime();

		service = ((Client) node).getService(Service.class, "Component0");
		
		startCall = System.nanoTime();
		service.call();
		endCall = System.nanoTime();
		
		node.stop();
		end = System.nanoTime();
	}
}
