/**
 * OW2 FraSCAti Examples: Echo BPEL
 * Copyright (C) 2010-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.bpel.echo.test;

import static org.junit.Assert.*;
import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;

import org.ow2.frascati.examples.bpel.echo.Echo;

/**
 * JUnit test case for OW2 FraSCAti class. 
 *
 * @author Philippe Merle.
 */
public class EchoTest extends FraSCAtiTestCase
{
    @Test
    public void runEchoBpelComposite() throws Exception {
      Echo echo = getService(Echo.class, "echo");
      String echoRequest = "Philippe Merle";
      String echoResponse = echo.process(echoRequest);
      assertEquals("echo.process(" + echoRequest + ")", echoRequest, echoResponse);
    }
}
