/**
 * OW2 FraSCAti Examples: Availability Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.intent.availability.test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import org.objectweb.fractal.api.Component;

import org.osoa.sca.ServiceUnavailableException;

import org.ow2.frascati.FraSCAti;

public class AvailabilityTestCase
{
  private static FraSCAti frascati;
  
  @BeforeClass
  public static void init() throws Exception {
    frascati = FraSCAti.newFraSCAti();
  }

  @Test
  public final void availability() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);

//    workThrownServiceUnavailableException(si, 2000);
    workThrownServiceUnavailableException(si, 2500);
    workThrownServiceUnavailableException(si, 3000);
    workThrownServiceUnavailableException(si, 4000);
    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability1s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-1s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);

//    workThrownServiceUnavailableException(si, 1000);
    workThrownServiceUnavailableException(si, 1500);
    workThrownServiceUnavailableException(si, 2000);
    workThrownServiceUnavailableException(si, 2500);
    workThrownServiceUnavailableException(si, 3000);
    workThrownServiceUnavailableException(si, 4000);
    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability2s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-2s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);

//    workThrownServiceUnavailableException(si, 2000);
    workThrownServiceUnavailableException(si, 2500);
    workThrownServiceUnavailableException(si, 3000);
    workThrownServiceUnavailableException(si, 4000);
    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability3s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-3s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);
    work(si, 2000);
    work(si, 2500);

 //   workThrownServiceUnavailableException(si, 3000);
    workThrownServiceUnavailableException(si, 3500);
    workThrownServiceUnavailableException(si, 4000);
    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability4s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-4s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);
    work(si, 2000);
    work(si, 2500);
    work(si, 3000);
    work(si, 3500);

//    workThrownServiceUnavailableException(si, 4000);
    workThrownServiceUnavailableException(si, 4500);
    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability5s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-5s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);
    work(si, 2000);
    work(si, 2500);
    work(si, 3000);
    work(si, 3500);
    work(si, 4000);
    work(si, 4500);

//    workThrownServiceUnavailableException(si, 5000);
    workThrownServiceUnavailableException(si, 6000);
    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 15000);
  }

  @Test
  public final void availability10s() throws Exception {
    // Load the availability-test composite.
    Component scaComposite = frascati.getComposite("availability-10s-test");
    // Obtain the service to test.
    ServiceInterface si = frascati.getService(scaComposite, "Service", ServiceInterface.class);

    workThrownIllegalArgumentException(si, -1000);
    workThrownIllegalArgumentException(si, -500);

    work(si, 0);
    work(si, 500);
    work(si, 1000);
    work(si, 1500);
    work(si, 2000);
    work(si, 2500);
    work(si, 3000);
    work(si, 3500);
    work(si, 4000);
    work(si, 4500);
    work(si, 5000);

//    workThrownServiceUnavailableException(si, 10000);
    workThrownServiceUnavailableException(si, 11000);
    workThrownServiceUnavailableException(si, 15000);
  }

  private void workThrownIllegalArgumentException(ServiceInterface si, long milliseconds) {
    try {
        si.work(milliseconds);
        Assert.fail("IllegalArgumentException must be thrown!");
    } catch(IllegalArgumentException iae) {
        System.out.println("IllegalArgumentException thrown.");
    }
  }

  private void work(ServiceInterface si, long milliseconds) {
    try {
        long response = si.work(milliseconds);
        Assert.assertEquals("Incorrect response!", milliseconds, response);
    } catch(Exception exc) {
        exc.printStackTrace(System.err);
        Assert.fail("No exception must be thrown!");
    }
  }

  private void workThrownServiceUnavailableException(ServiceInterface si, long milliseconds) {
    try {
        si.work(milliseconds);
        Assert.fail("ServiceUnavailableException must be thrown!");
    } catch(ServiceUnavailableException sue) {
    	System.out.println(sue.getMessage() + " during working " + milliseconds + "!");
    }
  }
}
