/**
 * OW2 FraSCAti Examples: Availability Intent
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Mahmoud Ben Hassine
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.intent.availability;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.ServiceUnavailableException;
import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

@Scope("COMPOSITE")
@Service(IntentHandler.class)
public class AvailabilityIntentHandler
  implements IntentHandler
{
    /**
     * Default timeout if not set.
     */
    private static final long DEFAULT_TIMEOUT = 2000;

    /**
     * Timeout for the proceed thread.
     */
    private long timeout = DEFAULT_TIMEOUT;

    /**
     * Setter for the SCA timeout property.
     *
     * @param timeout the timeout.
     */
    @Property
	public final void setTimeout(final long timeout)
    {
      System.out.println(this + ".setTimeout(" + timeout + ")");
      this.timeout = timeout;
    }

	@Override
    public final Object invoke(final IntentJoinPoint ijp)
      throws Throwable
    {
      // call the proceed in a separate thread.
      ProceedThread pt = new ProceedThread(ijp);

      // start the proceed thread.
      pt.start();

      // wait the proceed thread to return for maximum timeout value.
      pt.join(timeout);

      // timeout exceeded and the thread didn't returned yet.
      if (pt.isAlive()) {
        // don't interrupt the proceed thread because we don't know in which state it is.

        throw new ServiceUnavailableException("Timeout of " + timeout + " ms exceeded");
      }

      // throw the throwable of the proceed thread.
      if(pt.getThrowable() != null) {
        throw pt.getThrowable();
      }

      // the thread returned before timeout then return the thread's result.
      return pt.getResult();
    }
}

class ProceedThread extends Thread
{
    /**
     * The intent join point to proceed.
     */
    private IntentJoinPoint ijp;

    /**
     * The result returned by proceed().
     */
    private Object result = null;

    /**
     * Throwable thrown by proceed().
     */
    private Throwable throwable = null;

    /**
     * Constructs with a given intent join point.
     */
    public ProceedThread(IntentJoinPoint ijp)
    {
      this.ijp = ijp;
    }

    /**
     * #see Runnable#run()
     */
    public final void run()
    {
      try {
        result = ijp.proceed();
      } catch (Throwable e) {
        throwable = e;
      }
    }

    /**
     * Get the throwable.
     */
    public final Throwable getThrowable()
    {
      return this.throwable;
    }

    /**
     * Get the result.
     */
    public final Object getResult()
    {
      return this.result;
    }
}
