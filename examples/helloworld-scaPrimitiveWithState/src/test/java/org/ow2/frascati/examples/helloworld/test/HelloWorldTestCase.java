/**
 * OW2 FraSCAti Examples: HelloWorld with membrane scaPrimitiveWithState
 * Copyright (C) 2012-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld.test;

import org.junit.Assert;
import org.junit.Test;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.control.ContentController;
import org.objectweb.fractal.api.control.IllegalLifeCycleException;
import org.objectweb.fractal.api.control.LifeCycleController;

import org.ow2.frascati.membrane.state.api.StateController;
import org.ow2.frascati.membrane.state.api.State;
import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 * Test HelloWorld with membrane scaPrimitiveWithState.
 */
public class HelloWorldTestCase
     extends FraSCAtiTestCase
{
  @Test
  public void test() throws Exception
  {
	// Get the service 'Runnable' of the SCA composite.
    Runnable runnable = getService(Runnable.class, "Runnable");

    // Call 5 times the service then the 'counter' state field of each subcomponent is equals to 5.
    for(int i=0; i<5; i++) {
      runnable.run();
    }

    for(Component component : ((ContentController)getMainComposite().getFcInterface("content-controller")).getFcSubComponents()) {
      // Get the state controller of each sub component.
      StateController stateController = (StateController)component.getFcInterface(StateController.NAME);

      State state = null;

      // Try to get the state of each sub component without stopping it before.
      try {
        state = stateController.getState();
        Assert.fail("Not excepted as the component is not stopped!");
      } catch(IllegalLifeCycleException ilce) {
        // This is excepted as the component is not stopped.
      }

      // Try to set the state of each sub component without stopping it before.
      try {
        stateController.setState(state);
        Assert.fail("Not excepted as the component is not stopped!");
      } catch(IllegalLifeCycleException ilce) {
        // This is excepted as the component is not stopped.
      }

      // Stop the component.
      ((LifeCycleController)component.getFcInterface("lifecycle-controller")).stopFc();

      // Get the state of each sub component.
      try {
        state = stateController.getState();
        // Ok as the component is stopped.
      } catch(IllegalLifeCycleException ilce) {
        Assert.fail("Not excepted as the component is stopped!");
      }

      // Read the foo state field.
      int foo = state.getField("foo", int.class);

      // Check that the counter state field is equals to 5.
      Assert.assertEquals((long)5, (long)state.getField("counter", int.class));

      // Reset the counter state field.
      state.setField("counter", 0);

      // Set the state of each sub component.
      try {
        stateController.setState(state);
        // Ok as the component is stopped.
      } catch(IllegalLifeCycleException ilce) {
        Assert.fail("Not excepted as the component is stopped!");
      }

      // Get the state of each sub component.
      try {
        state = stateController.getState();
        // Ok as the component is stopped.
      } catch(IllegalLifeCycleException ilce) {
        Assert.fail("Not excepted as the component is stopped!");
      }

      // Check that the counter state field was really updated.
      Assert.assertEquals((long)0, (long)state.getField("counter", int.class));
    }
  }
}
