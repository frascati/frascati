/**
 * OW2 FraSCAti Examples: HelloWorld with membrane scaPrimitiveWithState
 * Copyright (C) 2012 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld;

import org.oasisopen.sca.annotation.Init;
import org.osoa.sca.annotations.Property;
//import org.oasisopen.sca.annotation.Property; // BUG: property 'count' is mandatory.
import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.objectweb.fractal.fraclet.extensions.Membrane;
import org.ow2.frascati.membrane.state.annotation.StateField;

/**
 * Server component implementation.
 *
 * @author Philippe Merle at Inria
 */
@Scope("COMPOSITE")
@Service(HelloService.class)
@Membrane(desc="scaPrimitiveWithState") // Server components have a state controller.
public class Server
  implements HelloService
{
  // --------------------------------------------------------------------------
  // SCA Properties.
  // --------------------------------------------------------------------------

  private String header = "->";

  private int count = 1;

  public final String getHeader()
  {
    return header;
  }

  @Property
  public final void setHeader(final String header)
  {
    this.header = header;
  }

  public final int getCount()
  {
    return count;
  }

  @Property
  public final void setCount(final int count)
  {
    this.count = count;
  }

  // --------------------------------------------------------------------------
  // Internal state accessible via the StateController interface.
  // --------------------------------------------------------------------------

  @StateField
  private int foo;

  @StateField
  private int counter;

  // --------------------------------------------------------------------------
  // Default constructor.
  // --------------------------------------------------------------------------

  public Server()
  {
    System.out.println("Server created.");
  }

  @Init
  public final void init()
  {
    System.out.println("Server initialized.");
  }

  // --------------------------------------------------------------------------
  // Implementation of the HelloService interface.
  // --------------------------------------------------------------------------

  public final void print(final String msg)
  {
    System.out.println("Server: begin printing...");
    for (int i = 0; i < (count); ++i) {
      System.out.println(((header) + msg));
    }
    // Increments the controlled state.
    counter++;
    System.out.println("Server: print done.");
  }

}
