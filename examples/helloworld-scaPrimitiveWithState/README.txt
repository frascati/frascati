============================================================================
OW2 FraSCAti Examples: HelloWorld with membrane scaPrimitiveWithState
Copyright (C) 2012 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s): 

============================================================================

HelloWorld with membrane scaPrimitiveWithState:
-----------------------------------------------

This example illustrates how using the scaPrimitiveWithState membrane.

The scaPrimitiveWithState membrane adds a new controller to SCA components.
This new controller manages the state of the component.
Have a look to Client and Server classes where:
  - @Membrane(desc="scaPrimitiveWithState") defines which membrane is used.
  - @StateField defines which class fields are part of the state of the component.
Have a look to the HelloWorldTestCase class to see how to manage component states
via StateController and State interfaces.

The scaPrimitiveWithState membrane is available into the following module:

    <dependency>
      <groupId>org.ow2.frascati</groupId>
      <artifactId>frascati-membrane-scaPrimitiveWithState</artifactId>
      <version>${frascati.version}</version>
    </dependency>

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Compilation and execution with the FraSCAti script:
---------------------------------------------------
TBD
