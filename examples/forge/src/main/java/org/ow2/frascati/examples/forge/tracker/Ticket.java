/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 */
package org.ow2.frascati.examples.forge.tracker;

import java.io.Serializable;

public class Ticket
  implements Serializable
{
	private static final long serialVersionUID = 1L;

	private int id;
	private String summary;
	private String description;
	private boolean open;
	
	Ticket(int id) {
		this.id = id;
		this.summary = null;
		this.description = null;
		this.open = true;
	}
	
	Ticket(Ticket t) {
		this.id = t.id;
		this.summary = t.summary;
		this.description = t.description;
		this.open = t.open;
	}

	public final String getDescription() {
		return this.description;
	}

	public final void setDescription(String description) {
		this.description = description;
	}

	public final String getSummary() {
		return this.summary;
	}

	public final void setSummary(String summary) {
		this.summary = summary;
	}

	public final int getId() {
		return this.id;
	}

	public final boolean isOpen() {
		return this.open;
	}
	
	public final void close() {
		this.open = false;
	}
	
	public final void reopen() {
		this.open = true;
	}
}
