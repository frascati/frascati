/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 */
package org.ow2.frascati.examples.forge.tracker;

import java.util.HashMap;
import java.util.Map;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

@Service(Tracker.class)
@Scope("COMPOSITE")
public class TrackerImpl
  implements Tracker
{
	private int sequence = 1;
	private Map<Integer, Ticket> tickets = new HashMap<Integer, Ticket>();

	public final Ticket createTicket() {
		Ticket nt = new Ticket(this.sequence++);
		this.tickets.put(nt.getId(), nt);
		return new Ticket(nt);
	}

	public final void dropTicket(int id) {
		this.tickets.remove(id);
	}

	public final Ticket getTicket(int id) {
		if (this.tickets.containsKey(id)) {
			return new Ticket(this.tickets.get(id));
		} else {
			return null;
		}
	}

	public final void updateTicket(Ticket t) {
		if (this.tickets.containsKey(t.getId())) {
			this.tickets.put(t.getId(), new Ticket(t));
		}
	}

}
