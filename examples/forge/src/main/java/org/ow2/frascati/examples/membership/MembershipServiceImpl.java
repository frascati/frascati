/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.membership;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class MembershipServiceImpl
  implements MembershipService
{
	private Set<Member> members = new HashSet<Member>();
	private Set<MembershipEventListener> listeners = new HashSet<MembershipEventListener>();
	
	public final boolean addMember(Member m) {
		if (this.members.add(m)) {
			for (MembershipEventListener mel : this.listeners) {
				mel.memberAdded(m);
			}
			return true;
		} else {
			return false;
		}
	}

	public final Member getMember(String id) {
		Member m = null;
		
		for (Member x : this.members) {
			if (x.getId().equals(id)) {
				m = x;
				break;
			}
		}
		return m;
	}

	public final boolean isMember(String id) {
		return (this.getMember(id) != null);
	}

	public final Set<Member> listMembers() {
		return Collections.unmodifiableSet(this.members);
	}

	public final void registerListener(MembershipEventListener mel) {
		this.listeners.add(mel);
	}

	public boolean removeMember(Member m) {
		if (this.members.remove(m)) {
			for (MembershipEventListener mel : this.listeners) {
				mel.memberRemoved(m);
			}
			return true;
		} else {
			return false;
		}
	}

	public final void unregisterListener(MembershipEventListener mel) {
		this.listeners.remove(mel);
	}

}
