/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.examples.forge.assembly;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.examples.forge.scm.SourceCodeManager;
import org.ow2.frascati.examples.forge.tracker.Ticket;
import org.ow2.frascati.examples.forge.tracker.Tracker;

@Service(Runnable.class)
public class Client
  implements Runnable
{

	@Reference
	private SourceCodeManager scm;
	
	@Reference
	private Tracker trac;

	public final void run()
	{
		// performs some calls

		Ticket t = trac.createTicket();
		int ticketID = t.getId();
		
		System.out.println("\n\nTicket " + ticketID + " has been created");
		t.setSummary("This is the first sample ticket");
		System.out.println("Perform CheckIn");
		scm.checkIn("foo" + ticketID, "this patch close#" + ticketID + " at last !");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Current version is : " + scm.getLastVersion());
		t = trac.getTicket(ticketID);
		if (!t.isOpen())
			System.out.println("Ticket " + t.getId() + " has been closed\n\n");

		System.out.println("Perform CheckOut\n ");
		String check = scm.checkOut(scm.getLastVersion());
		if (check != null) {
			System.out.println(check);
			System.out.println("CheckOut successful");
		}
	}
}
