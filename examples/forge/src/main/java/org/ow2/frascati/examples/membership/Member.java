/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.membership;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class Member
{
	private String id = null;
	private String firstName = null;
	private String lastName = null;
	private Set<Address> addresses = new HashSet<Address>();
	
	public final String getFirstName() {
		return this.firstName;
	}
	
	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final void setId(String id) {
		this.id = id;
	}
	
	public final String getLastName() {
		return this.lastName;
	}
	
	public final void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public final String getName() {
		return this.firstName + ' ' + this.lastName;
	}
	
	public final void addAddress(Address add) {
		this.addresses.add(add);
	}
	
	public final void removeAddress(Address add) {
		this.addresses.remove(add);
	}
	
	public final Collection<Address> getAddresses(Class<?> c) {
		if (c == null) {
			return Collections.unmodifiableSet(this.addresses);
		} else {
			Vector<Address> adds = new Vector<Address>();
			
			for (Address a : this.addresses) {
				if (c.isInstance(a)) {
					adds.add(a);
				}
			}
			return adds;
		}
	}

	@Override
	public final boolean equals(Object obj) {
		return ((obj instanceof Member)
				&& ((Member)obj).id.equals(this.id));
	}

	@Override
	public int hashCode() {
		return (this.id == null) ? 0 : this.id.hashCode();
	}
}
