/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.forge.scm;

import java.util.List;
import java.util.Vector;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

@Service(SourceCodeManager.class)
@Scope("COMPOSITE")
public class SourceCodeManagerImpl 
  implements SourceCodeManager
{
	private List<String> history = new Vector<String>();
	
	public final int checkIn(String patch, String comment) {
		this.history.add(patch);
		return this.history.size();
	}

	public final String checkOut(int version) {
		if ((version > 0) && (version <= this.history.size())) {
			StringBuilder buff = new StringBuilder();
			for (int i = 0; i < version; i++) {
				buff.append(this.history.get(i)).append('\n');
			}
			return buff.toString();
		} else {
			return null;
		}
	}

	public final int getLastVersion() {
		return this.history.size();
	}

}
