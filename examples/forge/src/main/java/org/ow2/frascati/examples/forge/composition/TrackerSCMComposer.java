/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Thomas Darbois
 *
 * Contributor(s): Philippe Merle
 */
package org.ow2.frascati.examples.forge.composition;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.examples.forge.scm.SourceCodeManager;
import org.ow2.frascati.examples.forge.tracker.Ticket;
import org.ow2.frascati.examples.forge.tracker.Tracker;

@Service(SourceCodeManager.class)
public class TrackerSCMComposer implements SourceCodeManager {
	
	private SourceCodeManager scm = null;
	private Tracker tracker = null;
	private static Pattern closeTicket = Pattern.compile(".*\\bclose#(\\d+)\\b.*");

	public final SourceCodeManager getSourceCodeManager() {
		return this.scm;
	}

	@Reference
	public final void setSourceCodeManager(SourceCodeManager scm) {
		this.scm = scm;
	}

	public final Tracker getTracker() {
		return this.tracker;
	}

	@Reference
	public final void setTracker(Tracker tracker) {
		this.tracker = tracker;
	}

	public final int checkIn(String patch, String comment) {
		int revision = this.scm.checkIn(patch, comment);
		Matcher m = closeTicket.matcher(comment);
		
		if (m.matches()) {
			Ticket t = this.tracker.getTicket(Integer.parseInt(m.group(1)));
			
			t.close();
			this.tracker.updateTicket(t);
		}
		return revision;
	}

	public final String checkOut(int version) {
		return this.scm.checkOut(version);
	}

	public final int getLastVersion() {
		return this.scm.getLastVersion();
	}

}
