/**
 * OW2 FraSCAti : Forge Example
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.forge;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.frascati.examples.forge.scm.SourceCodeManager;
import org.ow2.frascati.examples.forge.tracker.Ticket;
import org.ow2.frascati.examples.forge.tracker.Tracker;
import org.ow2.frascati.test.FraSCAtiTestCase;

public class ForgeTest
     extends FraSCAtiTestCase
{
	@Test
	public final void testForge() throws InterruptedException
	{
		// getting services
		SourceCodeManager scm = getService(SourceCodeManager.class,
				"TrackerSCMComposerComponent");

		Tracker trac = getService(Tracker.class, "TrackerComponent");

		// performs some calls

		Ticket t;
		int i;

		for (i = 1; i < 10; i++) {
			t = trac.createTicket();
			System.out.println("Ticket " + t.getId() + " has been created");
			t.setSummary("This is the first sample ticket");
			System.out.println("Perform CheckIn");
			scm.checkIn("foo" + i, "this patch close#" + i + " at last !");
			Thread.sleep(500);
			System.out.println("Current version is : " + scm.getLastVersion());
			t = trac.getTicket(i);
			if (!t.isOpen()) {
				System.out.println("Ticket " + t.getId()
						+ " has been closed\n\n");
			}
		}

		t = trac.createTicket();
		System.out.println("Perform CheckOut\n ");
		String check = scm.checkOut(scm.getLastVersion());
		if (check != null) {
			System.out.println(check);
			System.out.println("CheckOut successful");
		} else {
			Assert.fail("Forge CheckOut not successful");
		}
	}
}
