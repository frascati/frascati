============================================================================
OW2 FraSCAti Examples: Google Fusion Tables 
Copyright (C) 2013 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

Google Fusion Tables:
---------------------

This example illustrates how to use Google Fusion Tables API from an
SCA application. Google Fusion Tables API is described at:
https://developers.google.com/fusiontables/

This example also illustrates the usage of both ImplicitQueryParamIntent 
and Jackson intents.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------

  mvn -Prun 
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)
  mvn -Prun,web                  (with FraSCAti Web Explorer)

Several public Google Fusion Tables can be consulted:

* A table storing Web Services (address and type).
  Type:
    mvn -PCloudWebServicesRegistry 

* A Store Inventory.
  Type:
    mvn -PStoreInventory 

* The Store Locations - John's Pizza Chain.
  Type:
    mvn -PStoreLocationsJohnPizzaChain
