/**
 * OW2 FraSCAti Examples: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.google.fusion.tables.v1.client;

import java.util.List;

public class StoreInventoryClient extends Client
{
    public void run()
    {
      super.run();

      List<StoreInventory> items = query("select * from " + this.table, StoreInventory.class);
      for(StoreInventory item : items) {
        System.out.println("* StoreInventory[product=" + item.getProduct() + ", inventory=" + item.getInventory() + "]");
      }
    }
}
