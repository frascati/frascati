/**
 * OW2 FraSCAti Examples: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.google.fusion.tables.v1.client;

/**
 * Represents a row of the Google Fusion Tables's StoreInventory table.
 *
 * @author Philippe Merle at Inria
 * @since 1.5
 */
public class CloudWebServicesRegistryEntry
{
    private String type;

    private String address;

    public String getType()
    {
      return this.type;
    }

    public void setType(String type)
    {
      this.type = type;
    }

    public String getAddress()
    {
      return this.address;
    }

    public void setAddress(String address)
    {
      this.address = address;
    }
}
