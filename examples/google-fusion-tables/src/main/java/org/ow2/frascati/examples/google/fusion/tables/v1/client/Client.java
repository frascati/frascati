/**
 * OW2 FraSCAti Examples: Google Fusion Tables
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.google.fusion.tables.v1.client;

import java.util.List;

import org.osoa.sca.annotations.Property;
import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Service;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.google.fusion.tables.v1.api.GoogleFusionTablesAPI;
import org.ow2.frascati.google.fusion.tables.v1.api.Column;
import org.ow2.frascati.google.fusion.tables.v1.api.Columns;
import org.ow2.frascati.google.fusion.tables.v1.api.Table;
import org.ow2.frascati.google.fusion.tables.v1.api.QueryResult;

@Scope("COMPOSITE")
@Service(Runnable.class)
public class Client implements Runnable
{
    @Property(name="table")
    protected String table;

    @Property(name="columns")
    private String columnNames;

    @Reference(name="google-fusion-tables")
    protected GoogleFusionTablesAPI googleFusionTables;

    public void run()
    {
      table();
      columns();
      column(0);
      column(1);
    
      query("select * from " + this.table);
      query("select rowid from " + this.table);
      query("select rowid, " + this.columnNames + " from " + this.table);
    }

    private void table()
    {
      Table table = this.googleFusionTables.getTable(this.table);
      System.out.println("Google Fusion Tables - getTable " + this.table);
      System.out.println("Kind: " + table.getKind());
      System.out.println("TableId: " + table.getTableId());
      System.out.println("Name: " + table.getName());
      System.out.println("Columns:");
      for(Column column : table.getColumns()) {
        System.out.println(" * Kind:     " + column.getKind());
        System.out.println("   ColumnId: " + column.getColumnId());
        System.out.println("   Name:     " + column.getName());
        System.out.println("   Type:     " + column.getType());
      }
      System.out.println("Description: " + table.getDescription());
      System.out.println("IsExportable: " + table.isExportable());
      System.out.println();
  	}
    
    private void columns()
    {
      Columns columns = this.googleFusionTables.getColumns(this.table);
      System.out.println("Google Fusion Tables - getColumns " + this.table);
      System.out.println("Kind: " + columns.getKind());
      System.out.println("TotalItems: " + columns.getTotalItems());
      System.out.println("Items:");
      for(Column item : columns.getItems()) {
        System.out.println(" * Kind:     " + item.getKind());
        System.out.println("   ColumnId: " + item.getColumnId());
        System.out.println("   Name:     " + item.getName());
        System.out.println("   Type:     " + item.getType());
      }
      System.out.println();
    }
    
    private void column(int columnId)
    {
      Column column = this.googleFusionTables.getColumn(this.table, columnId);
      System.out.println("Google Fusion Tables - getColumn " + this.table + " " + columnId);
      System.out.println("Kind:     " + column.getKind());
      System.out.println("ColumnId: " + column.getColumnId());
      System.out.println("Name:     " + column.getName());
      System.out.println("Type:     " + column.getType());
      System.out.println();
    }
    
    private void query(String sql)
    {
      QueryResult result = this.googleFusionTables.query(sql);
      System.out.println("Google Fusion Tables - query " + sql);
      System.out.println("Kind: " + result.getKind());
      System.out.println("Columns:");
      for(String column : result.getColumns()) {
        System.out.println(" * " + column);
      }
      System.out.println("Rows:");
      for(List<Object> row : result.getRows()) {
        System.out.print(" *");
        for(Object item : row) {
          System.out.print(' ' + item.toString());
        }
        System.out.println();
      }
      System.out.println();
    }

    /**
     * Experimental typed query method returning a typed instance instead of a generic QueryResult instance.
     *
     */
    protected <TYPE> List<TYPE> query(String sql, Class<TYPE> clazz)
    {
      // Send the query to Google Fusion Tables.
      QueryResult queryResult = this.googleFusionTables.query(sql);

      // Create a map containing all <methodName, method> for the given class.
      java.util.Map<String, java.lang.reflect.Method> classMethods = new java.util.HashMap<String, java.lang.reflect.Method>();
      for(java.lang.reflect.Method method : clazz.getMethods()) {
        classMethods.put(method.getName(), method);
      }

      // Compute an array of the setter method associated to each column.
      java.lang.reflect.Method[] setterMethods = new java.lang.reflect.Method[queryResult.getColumns().size()];
      int columnIndex = 0;
      for(String column : queryResult.getColumns()) {
    	String setterMethodName = "set" + column;
    	setterMethods[columnIndex] = classMethods.get(setterMethodName);
    	// TODO: check that the setter method has just one parameter.
    	columnIndex++;
      }

      // Transform the generic QueryResult instance into a collection of typed instances.
      List<TYPE> result = new java.util.ArrayList<TYPE>();
      // Traverse all the rows of the query result.
      for(List<Object> row : queryResult.getRows()) {
    	try {
    	  // for each row allocate a typed object.
    	  TYPE returnedObject = clazz.newInstance();
          // TODO: when newInstance() throws exceptions.

    	  // traverse all the columns of the current row.
    	  columnIndex = 0;
          for(Object item : row) {
        	// invoke the setter method for the current column.
        	// TODO: if setterMethods[columnIndex] is null
            // TODO: convert item to the type expected by the setter method.
            //       e.g. String -> int ; String -> double ; String -> boolean ; etc.
            setterMethods[columnIndex].invoke(returnedObject, item);
            // TODO: when invoke() throws exceptions.
            columnIndex++;
          }
          result.add(returnedObject);
    	} catch(Exception exc) {
          // TODO: Really experimental.
    	  exc.printStackTrace();
    	}
      }

      return result;
    }
}
