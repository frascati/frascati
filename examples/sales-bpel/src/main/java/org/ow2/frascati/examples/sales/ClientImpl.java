/**
 * OW2 FraSCAti: Sales BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.sales;

import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

/**
 * @author Philippe Merle - INRIA
 */
@Scope("COMPOSITE") // TODO: Perhaps conversation scope is more appropriate?
@Service(interfaces={Runnable.class, SalesAsyncCallback.class})
public class ClientImpl
  implements Runnable, SalesAsyncCallback
{
  private Logger log = Logger.getLogger(Stock.class.getCanonicalName());

  @Property(name = "refId")
  private int refId;

  @Property(name = "name")
  private String name;

  @Property(name = "quantity")
  private int quantity;

  @Reference(name = "sales")
  private Sales sales;

  /**
   * Semaphore for synchonization between run() and onResultSales().
   */
  private Semaphore semaphore = new Semaphore(0);

  public final void run()
  {
    log.info("CLIENT: run() called.");
    SalesRequest salesRequest = new SalesRequest();
    log.info("CLIENT: Preparing the SalesRequest...");
    SalesItem salesItem = new SalesItem();
    salesItem.setRefId(this.refId);
    salesItem.setName(this.name);
    salesItem.setQuantity(this.quantity);
    salesRequest.setArticle(salesItem);
    log.info("CLIENT: Calling sales.initiate(" + salesRequest.toString() + ")...");
    this.sales.initiate(salesRequest);
    log.info("CLIENT: Waiting the SalesResponse...");
    try {
      this.semaphore.acquire();
    } catch(InterruptedException exc) {
      log.info("CLIENT: run() interrupted.");
      return;
    }
    log.info("CLIENT: run() finished.");
  }

  public final void onResultSales(SalesResponse salesResponse)
  {
    log.info("CLIENT: onResultSales(" + salesResponse.toString() + ") called.");
    this.semaphore.release();
    log.info("CLIENT: onResultSales(" + salesResponse.toString() + ") finished.");
  }

}
