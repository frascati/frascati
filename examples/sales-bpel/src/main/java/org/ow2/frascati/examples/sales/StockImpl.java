/**
 * OW2 FraSCAti: Sales BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.sales;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Property;

/**
 * @author Philippe Merle - INRIA
 */
@Scope("COMPOSITE")
@Service(Stock.class)
public class StockImpl
  implements Stock
{
  private Logger log = Logger.getLogger(Stock.class.getCanonicalName());

  @Reference(name = "process")
  private Sales process;

  @Property(name = "numberOfWorkIterations")
  private int numberOfWorkIterations = 5;
  
  @Property(name = "iterationDuration")
  private int iterationDuration = 100;

  private Map<Integer, Integer> stock = new HashMap<Integer, Integer>() {
	{
      // Init the stock.
	  for(int i=0; i<100; i++) {
        put(i, 100);
	  }
	}
  };

  public final void initiate(StockRequest stockRequest)
  {
    log.info("STOCK: initiate(" + stockRequest.toString() + ") called.");
	StockItem articleIn = stockRequest.getArticle();

    for(int i=1; i<=this.numberOfWorkIterations; i++) {
      log.info("STOCK: initiate(" + stockRequest.toString() + ") works (" + i + "/" + numberOfWorkIterations + ")...");
      try { Thread.sleep(this.iterationDuration); } catch(Exception e) { }
    }

    StockResponse stockResponse = new StockResponse();
    stockResponse.setRefId(articleIn.getRefId());
    Integer availability = this.stock.get(articleIn.getRefId());
    if(availability != null && availability.intValue() >= articleIn.getQuantity()) {
      stockResponse.setResult(true);
      // Update the stock.
      this.stock.put(articleIn.getRefId(), availability.intValue() - articleIn.getQuantity());
    } else {
      stockResponse.setResult(false);
    }

    log.info("STOCK: Calling process.onResultStock(" + stockResponse.toString() + ")...");
    this.process.onResultStock(stockResponse);

    log.info("STOCK: initiate(" + stockRequest.toString() + ") finished.");
  }

}
