package org.ow2.frascati.examples.helloworld.auth.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.security.Principal;
import java.util.Date;
import java.util.HashSet;

import javax.naming.AuthenticationException;
import javax.security.auth.Subject;

import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.examples.helloworld.auth.ExamplePrincipal;
import org.ow2.frascati.examples.helloworld.auth.services.PrintCapService;
import org.ow2.frascati.examples.helloworld.auth.services.PrintService;
import org.ow2.frascati.tinfi.SecuritySubjectManager;
import org.ow2.frascati.util.FrascatiException;

public class AuthTest {
	
	private Component helloworldAuth;
	
	private Component helloworldFullAuth;
	
	private FraSCAti fraSCAti;

	public AuthTest() throws FrascatiException{
	
		this.fraSCAti = FraSCAti.newFraSCAti();
	
		this.helloworldAuth = this.fraSCAti.getComposite("helloworld-auth");
		
		this.helloworldFullAuth = 
			this.fraSCAti.getComposite("helloworld-full-auth");
		
	}
	
	@Test
	public void testServer() throws InterruptedException, FrascatiException{

		PrintService printService = this.fraSCAti.
				getService(helloworldAuth, "print", PrintService.class);
		PrintCapService printCapService = this.fraSCAti.
				getService(helloworldAuth, "print-cap", PrintCapService.class);
		
		// at first we call the method without being authenticated
		try {
			printService.print("hello");
			
			// we're waiting for an exception so it's not a 
			// good idea to still being there
			fail();		
			
		} catch (Exception e){			
			// ok, exception thrown
			assertTrue(e.getCause().getClass().
				equals(AuthenticationException.class));
		}
		
		// this service should process because it does not need authentication
		printCapService.printCapitalize("hello world");
		
		// now, we set the subject
		// this operation should be done by an authentication service
		HashSet<Principal> principals = new HashSet<Principal>();
		Principal p = new ExamplePrincipal("FraSCAti example user");
		principals.add(p);
		
		HashSet<Object> pubCredentials = new HashSet<Object>();
		pubCredentials.add(new Date());
		
		HashSet<?> privCredentials = new HashSet<String>();

		Subject subject = new Subject(true, principals, pubCredentials,
				privCredentials); 
    	
    	SecuritySubjectManager.get().setSecuritySubject(subject);
    	
    	// this call should be all right
    	printService.print("hello");
    	
    	// we wait for a few seconds
    	// the time limit should avoid the call
    	Thread.sleep(4000);
    	
		try {
			printService.print("hello");
			fail();		
		} catch (Exception e){			
			assertTrue(e.getCause().getClass().
					equals(AuthenticationException.class));
		}
    	
	}
	
	@Test
	public void testServerAuth() throws FrascatiException{
		PrintService printService = this.fraSCAti.
				getService(helloworldFullAuth, "print", PrintService.class);
		PrintCapService printCapService = this.fraSCAti.
			getService(helloworldFullAuth, "print-cap", PrintCapService.class);
		
		// this time, every operations require authentication, 
		// because the annotation is set 
		// on the component (@see impl.ServerAuth)
		try {
			printCapService.printCapitalize("hello world");
			fail();					
		} catch (Exception e){			
			// ok, exception thrown
			assertTrue(e.getCause().getClass().
					equals(AuthenticationException.class));
		}
		
		// we set the subject
		HashSet<Principal> principals = new HashSet<Principal>();
		Principal p = new ExamplePrincipal("FraSCAti example user");
		principals.add(p);
		
		HashSet<Object> pubCredentials = new HashSet<Object>();
		pubCredentials.add(new Date());
		
		HashSet<?> privCredentials = new HashSet<String>();

		Subject subject = new Subject(true, principals, pubCredentials,
				privCredentials); 
    	
    	SecuritySubjectManager.get().setSecuritySubject(subject);
		
    	// and it should be ok now
    	printCapService.printCapitalize("hello world");
    	printService.print("hello");
	}
	
}
