/**
 * OW2 FraSCAti Examples: Helloworld authenticated
 * Copyright (C) 2009-2010 INRIA, University of Lille 1, Orange Labs
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Rémi Mélisson
 *
 * Contributor(s): 
 */

package org.ow2.frascati.examples.helloworld.auth.impl;

import java.security.Principal;

import javax.security.auth.Subject;

import org.oasisopen.sca.ComponentContext;
import org.oasisopen.sca.annotation.Context;
import org.oasisopen.sca.annotation.Scope;
import org.ow2.frascati.examples.helloworld.auth.ExamplePrincipal;
import org.ow2.frascati.examples.helloworld.auth.services.PrintCapService;
import org.ow2.frascati.examples.helloworld.auth.services.PrintService;


/**
 * The print service implementation.
 */
@Scope("COMPOSITE")
public class Server
  implements PrintService, PrintCapService
{
	
	@Context
	ComponentContext context;
	
    /**
     * Default constructor.
     */
    public Server()
    {
        System.out.println("SERVER created.");
    }
    
    /**
     * PrintService implementation.
     */
    public void print(String msg)
    {
    	Subject subject = context.getRequestContext().getSecuritySubject();
    	
    	String userName = "";
    	
    	for (Principal principal : subject.getPrincipals()){
    		if (principal instanceof ExamplePrincipal) {
    			userName = ((ExamplePrincipal) principal).getUserName();
    		}
    	}    
    	
    	System.out.println(msg + " " + userName);
    }

	public void printCapitalize(String msg) {
		System.out.println(msg.toUpperCase());
	}

}