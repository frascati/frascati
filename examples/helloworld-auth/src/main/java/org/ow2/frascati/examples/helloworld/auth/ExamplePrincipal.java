/**
 * OW2 FraSCAti Examples: Helloworld authenticated
 * Copyright (C) 2009-2010 INRIA, University of Lille 1, Orange Labs
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Rémi Mélisson
 *
 * Contributor(s): 
 */

package org.ow2.frascati.examples.helloworld.auth;

import java.security.Principal;

/**
 * Example of a principal implementation.
 * This principal will be included into
 * the subject, and it aims to contain information
 * relatives to the subject.
 * @see java.security.Principal
 * 
 * @author Rémi Mélisson
 *
 */
public class ExamplePrincipal implements Principal {

	public static String NAME = "user-name";
	
	private String userName;
	
	/**
	 * Return the name of the principal in order
	 * to allow its identification
	 */
	public String getName() {
		return ExamplePrincipal.NAME;
	}
	
	public String getUserName(){
		return this.userName;
	}
	
	public ExamplePrincipal(String userName){
		this.userName = userName;
	}
	
}