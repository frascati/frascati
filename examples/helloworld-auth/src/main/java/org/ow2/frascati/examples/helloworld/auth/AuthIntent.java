/**
 * OW2 FraSCAti Examples: Helloworld authenticated
 * Copyright (C) 2009-2010 INRIA, University of Lille 1, Orange Labs
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Rémi Mélisson
 *
 * Contributor(s): 
 */

package org.ow2.frascati.examples.helloworld.auth;

import java.util.Date;
import java.util.Set;

import javax.security.auth.Subject;

import org.oasisopen.sca.annotation.Property;
import org.ow2.frascati.intent.authentication.AbstractAuthenticationIntent;

public class AuthIntent extends AbstractAuthenticationIntent {
	
	/*
	 * Validity of the subject, in millisecond
	 */
	@Property
	int validity;
	
	/**
	 * 
	 * This method check the validity of the subject according to
	 * the rules of the application
	 * Here, we only look if the creation date is not too long, in regard
	 * of the specified validity
	 * 
	 * @param subject
	 * @return
	 */
	public boolean isAuthenticated(Subject subject) { 
		
		Set<Object> pubCredentials = subject.getPublicCredentials();
		
		for (Object o : pubCredentials){
			if (o instanceof Date) {
				// interval between now and the subject creation
				long interval = (new Date()).getTime() - ((Date) o).getTime();
				return interval < this.validity;
			}
		}
		
		return false;
	}
	
}
