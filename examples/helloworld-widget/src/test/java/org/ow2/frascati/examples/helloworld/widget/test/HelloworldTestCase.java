/**
 * OW2 FraSCAti Examples: HelloWorld Widget
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld.widget.test;

import static org.junit.Assert.assertNotNull;

import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.examples.helloworld.widget.SayHello;
import org.ow2.frascati.remote.introspection.RemoteScaDomain;
import org.ow2.frascati.test.util.FraSCAtiTestUtils;
import org.ow2.frascati.util.FrascatiException;

public class HelloworldTestCase
{
    private static FraSCAti frascati;

    /** The Frascati Domain */
    private static RemoteScaDomain domain;

    private static Component helloworldWidgetComponent;

    @BeforeClass
    public static void beforeClass() throws FrascatiException
    {
        System.setProperty("org.ow2.frascati.bootstrap", "org.ow2.frascati.bootstrap.FraSCAtiJDTRest");
        frascati = FraSCAti.newFraSCAti();
        assertNotNull(frascati);

        String domainURI = FraSCAtiTestUtils.completeBindingURI("/introspection");
        domain = JAXRSClientFactory.create(domainURI, RemoteScaDomain.class);
        helloworldWidgetComponent = frascati.getComposite("helloworld-widget");
        assertNotNull(helloworldWidgetComponent);
        Component helloworldRESTWidgetComponent = frascati.getComposite("helloworld-rest-widget");
        assertNotNull(helloworldRESTWidgetComponent);
    }

    // @Test
    public final void testWidgetService() throws FrascatiException
    {
        SayHello sayHello = frascati.getService(helloworldWidgetComponent, "SayHello", SayHello.class);
        sayHello.sayHello("Philippe");
    }

    @Test
    public final void testWidgetRestService()
    {
        String widgetRestServiceURI = FraSCAtiTestUtils.completeBindingURI("/SayHelloRest");
        FraSCAtiTestUtils.assertWADLExist(true, widgetRestServiceURI);
        domain.stopComponent("/helloworld-rest-widget");
        FraSCAtiTestUtils.assertWADLExist(false, widgetRestServiceURI);
        domain.startComponent("/helloworld-rest-widget");
        FraSCAtiTestUtils.assertWADLExist(true, widgetRestServiceURI);
    }
}
