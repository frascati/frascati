============================================================================
OW2 FraSCAti Examples: HelloWorld Widget
Copyright (C) 2012 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

HelloWorld Widget:
------------------

This example illustrates how to invoke an SCA-based Web service from
an HTML/JavaScript page (i.e., a widget).

Have a look to src/main/resources/helloworld-widget.composite and
src/main/resources/widget/widget.html to see how the widget is implemented.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)
  mvn -Prun,web                  (with FraSCAti Web Explorer)

Using the HelloWorld Widget:
----------------------------

Open your favorite Web browser and go to:
* http://localhost:8765/SayHello?wsdl to obtain the WSDL describing the
  SayHello Web Service. To invoke this service, use soapUI for instance, or
* http://localhost:8765/SayHelloWidget/widget.html to obtain a dynamic HTML page
  allowing to invoke the SayHello Web service.
