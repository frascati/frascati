============================================================================
OW2 FraSCAti in Google App Engine
Copyright (C) 2011 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):
 
============================================================================

OW2 FraSCAti in Google App Engine:
----------------------------------

This is the source code of OW2 FraSCAti in Google App Engine available at
http://ow2-frascati.appspot.com/

Compilation with Maven:
-----------------------
  mvn install

Deploying the WAR in an embedded Web Application Server:
--------------------------------------------------------

   mvn jetty:run
or mvn tomcat:run
  
This command starts a standalone embedded Jetty or Tomcat server on port 8080.
This server loads OW2 FraSCAti in Google App Engine Web Application on the 
context path '/frascati-gae'.

Open your favorite Web browser and go to http://localhost:8080/frascati-gae/

Deploying the WAR on a Web Application server:
----------------------------------------------
Install target/frascati-gae-<version>.war on your favorite
Web Application server, e.g., Jetty, Tomcat, JBoss, Geronimo, etc. 

For instance, for Apache Tomcat:
* copy the WAR to the Tomcat webapps directory:
  cp target/frascati-gae-<version>.war $CATALINA_HOME/webapps/frascati-gae.war
* Use the Tomcat Manager page to start the war.

For other Web servers, please refer to the documentation of your favorite server.

Deploying on Google App Engine:
-------------------------------

  $ mvn clean install
  $ cp jars/* target/frascati-gae-1.5-SNAPSHOT/WEB-INF/lib/
  $ /appengine-java-sdk-1.5.4/bin/appcfg.sh update target/frascati-gae-1.5-SNAPSHOT

Currently, OW2 FraSCAti in Google App Engine requires some patched jars:
* As GAE does not support signed jars:
  - common-2.4.0.jar
  - common-3.4.0.jar
  - ecore-2.4.0.jar
  - runtime-3.4.0.jar
  - xmi-2.4.0.jar
  are non signed jars for Eclipse Modeling Framework and Equinox.
* cxf-rt-frontend-jaxws-2.4.2.jar includes a patch to deal with a GAE issue reported at
  http://code.google.com/p/googleappengine/issues/detail?id=5505
