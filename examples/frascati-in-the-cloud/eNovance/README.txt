============================================================================
OW2 FraSCAti in eNovance
Copyright (C) 2011 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):
 
============================================================================

OW2 FraSCAti in eNovance:
-------------------------

This is the source code of OW2 FraSCAti in eNovance available at 
http://94.143.114.166:8080/

Compilation with Maven:
-----------------------
  mvn install

Deploying the WAR in an embedded Web Application Server:
--------------------------------------------------------

   mvn jetty:run
or mvn tomcat:run
  
This command starts a standalone embedded Jetty or Tomcat server on port 8080.
This server loads OW2 FraSCAti in eNovance Web Application
on the context path '/frascati-enovance'.

Open your favorite Web browser and go to 
http://localhost:8080/frascati-enovance/

Deploying the WAR on a Web Application server:
----------------------------------------------
Install target/frascati-enovance-<version>.war on your favorite
Web Application server, e.g., Jetty, Tomcat, JBoss, Geronimo, etc. 

For instance, for Apache Tomcat:
* copy the WAR to the Tomcat webapps directory:
  cp target/frascati-enovance-<version>.war $CATALINA_HOME/webapps/frascati-enovance.war
* Use the Tomcat Manager page to start the war.

For other Web servers, please refer to the documentation of your favorite server.

Deploying the WAR on OW2 FraSCAti in eNovance:
----------------------------------------------

scp -i enovance.pem target/frascati-enovance-1.5-SNAPSHOT.war ubuntu@94.143.114.166:software/apache-tomcat-7.0.20/webapps/ROOT.war

ssh -i enovance.pem ubuntu@94.143.114.166
