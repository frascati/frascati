/**
 * OW2 FraSCAti Examples: Peer-to-Peer Network in the Cloud
 * Copyright (C) 2011 INRIA, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.p2p.lib;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Property;

import org.ow2.frascati.examples.p2p.api.Geolocation;
import org.ow2.frascati.examples.p2p.api.Location;
import org.ow2.frascati.examples.p2p.api.Peer;
import org.ow2.frascati.examples.p2p.api.PeerInfo;

/**
 * Implementation of the Peer component.
 * 
 * @author Philippe Merle
 */
@Scope("COMPOSITE")
public class PeerImpl
  implements Peer
{
	/**
	 * Store a text describing who am i.
	 */
	@Property(name="whoami")
	protected String whoami;

	/**
	 * My URL.
	 */
	@Property(name="url")
	protected String url;

	/**
	 * Reference to geolocation service.
	 */
	@Reference(name="geolocation")
	protected Geolocation geolocation;

	/**
	 * My Location.
	 */
	protected Location location;

	/**
	 * Delay before replying to requests.
	 */
	@Property(name="delay")
	protected long delay;

	/**
     * @see org.ow2.frascati.examples.p2p.api.Peer#getPeerInfo()
     */
    public PeerInfo getPeerInfo()
    {
      // Introduce a delay before replying.
      if(this.delay > 0) {
        try {
          Thread.sleep(this.delay);
        } catch(InterruptedException ie) {
          // do nothing
        }
      }

      // Prepare the peer info.
      PeerInfo peerInfo = new PeerInfo();
      peerInfo.setWhoAmI(this.whoami);
      peerInfo.setUrl(this.url);

      // Get geolocation.
      if(this.location == null) {
        this.location = this.geolocation.getLocation("");
        // Sometimes the service does not work correctly!
        if(this.location.getLatitude().equals("0") && this.location.getLongitude().equals("0")) {
          this.location = null;
        }
      }
      peerInfo.setLocation(this.location);

      try {
        InetAddress inetAddress = InetAddress.getLocalHost();
        peerInfo.setCanonicalHostName(inetAddress.getCanonicalHostName());
        peerInfo.setHostName(inetAddress.getHostName());
        peerInfo.setIp(inetAddress.getHostAddress());
      } catch(UnknownHostException uhe) {
        peerInfo.setCanonicalHostName("UNKNOWN");
        peerInfo.setHostName("UNKNOWN");
        peerInfo.setIp("UNKNOWN");
      }
      peerInfo.setDate(new Date().toString());
      Runtime runtime = Runtime.getRuntime();
      peerInfo.setAvailableProcessors(runtime.availableProcessors());
      peerInfo.setFreeMemory(runtime.freeMemory());
      peerInfo.setTotalMemory(runtime.totalMemory());
      peerInfo.setMaxMemory(runtime.maxMemory());
      peerInfo.setLatency(0);
      return peerInfo;
    }
}
