/**
 * OW2 FraSCAti Examples: Peer-to-Peer Network in the Cloud
 * Copyright (C) 2011 INRIA, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.p2p.lib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.oasisopen.sca.annotation.Property;
import org.oasisopen.sca.annotation.Scope;
import org.oasisopen.sca.annotation.Reference;

import org.ow2.frascati.examples.p2p.api.Peer;
import org.ow2.frascati.examples.p2p.api.PeerCollection;
import org.ow2.frascati.examples.p2p.api.PeerInfo;

/**
 * Implementation of the PeerCollection component.
 * 
 * @author Philippe Merle
 */
@Scope("COMPOSITE")
public class PeerCollectionImpl
  implements PeerCollection
{
	/**
	 * List of peer references.
	 */
	@Reference(name="peer")
	protected List<Peer> peers;

	/**
	 * Minimal latency for cache peer infos.
	 */
	@Property(name="minimal-latency-before-caching")
	protected int minimalLatencyBeforeCaching;

	/**
	 *
	 */
    protected ArrayList<PeerInfo> peerInfos;

    /**
     * @see org.ow2.frascati.examples.p2p.api.PeerCollection#getPeerInfos()
     */
    public Collection<PeerInfo> getPeerInfos()
    {
      if(this.peerInfos == null) {
        ArrayList<PeerInfo> tmp = new ArrayList<PeerInfo>();
        for(Peer peer : peers) {
          tmp.add(new PeerInfo());
        }
        this.peerInfos = tmp;
      }
      for(int peerIndex=0; peerIndex<peers.size(); peerIndex++) {
        PeerInfo peerInfo = this.peerInfos.get(peerIndex);
        if(peerInfo.getLatency() < this.minimalLatencyBeforeCaching) {
          long timeBefore = System.currentTimeMillis();
          try {
            peerInfo = this.peers.get(peerIndex).getPeerInfo();
          } catch(Exception e) {
            peerInfo = new PeerInfo();
            peerInfo.setWhoAmI(e.getClass().getName() + " - " + e.getMessage());
            peerInfo.setCanonicalHostName("");
            peerInfo.setHostName("");
            peerInfo.setIp("");
            peerInfo.setDate("");
            peerInfo.setAvailableProcessors(0);
            peerInfo.setFreeMemory(0L);
            peerInfo.setTotalMemory(0L);
            peerInfo.setMaxMemory(0L);
          }
          long timeAfter = System.currentTimeMillis();
          peerInfo.setLatency(timeAfter - timeBefore);
          this.peerInfos.set(peerIndex, peerInfo);
        }
      }
      return this.peerInfos;
    }

    /**
     * @see org.ow2.frascati.examples.p2p.api.PeerCollection#resetIt()
     */
    public void resetIt()
    {
      this.peerInfos = null;
    }
}
