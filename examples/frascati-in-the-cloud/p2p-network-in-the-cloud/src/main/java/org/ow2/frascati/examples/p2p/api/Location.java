/**
 * OW2 FraSCAti Examples: Peer-to-Peer Network in the Cloud
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */
package org.ow2.frascati.examples.p2p.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data returned by the freegeoip.net service.
 *
 * @author Philippe Merle - INRIA
 */
@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Location", propOrder = {
    "Ip",
    "CountryCode",
    "CountryName",
    "RegionCode",
    "RegionName",
    "City",
    "ZipCode",
    "Latitude",
    "Longitude",
    "MetroCode"
})
public class Location
{
    @XmlElement(name="Ip")
    private String Ip;

    @XmlElement(name="CountryCode")
    private String CountryCode;

    @XmlElement(name="CountryName")
    private String CountryName;

    @XmlElement(name="RegionCode")
    private String RegionCode;

    @XmlElement(name="RegionName")
    private String RegionName;

    @XmlElement(name="City")
    private String City;

    @XmlElement(name="ZipCode")
    private String ZipCode;

    @XmlElement(name="Latitude")
    private String Latitude;

    @XmlElement(name="Longitude")
    private String Longitude;

    @XmlElement(name="MetroCode")
    private String MetroCode;

    public final String getIp()
    {
      return this.Ip;
    }

    public final void setIp(String ip)
    {
      this.Ip = ip;
    }

    public final String getCountryCode()
    {
      return this.CountryCode;
    }

    public final void setCountryCode(String countryCode)
    {
      this.CountryCode = countryCode;
    }

    public final String getCountryName()
    {
      return this.CountryName;
    }

    public final void setCountryName(String countryName)
    {
      this.CountryName = countryName;
    }

    public final String getRegionCode()
    {
      return this.RegionCode;
    }

    public final void setRegionCode(String regionCode)
    {
      this.RegionCode = regionCode;
    }

    public final String getRegionName()
    {
      return this.RegionName;
    }

    public final void setRegionName(String regionName)
    {
      this.RegionName = regionName;
    }

    public final String getCity()
    {
      return this.City;
    }

    public final void setCity(String city)
    {
      this.City = city;
    }

    public final String getZipCode()
    {
      return this.ZipCode;
    }

    public final void setZipCode(String zipCode)
    {
      this.ZipCode = zipCode;
    }

    public final String getLatitude()
    {
      return this.Latitude;
    }

    public final void setLatitude(String latitude)
    {
      this.Latitude = latitude;
    }

    public final String getLongitude()
    {
      return this.Longitude;
    }

    public final void setLongitude(String longitude)
    {
      this.Longitude = longitude;
    }

    public final String getMetroCode()
    {
      return this.MetroCode;
    }

    public final void setMetroCode(String metroCode)
    {
      this.MetroCode = metroCode;
    }
}
