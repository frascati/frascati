/**
 * OW2 FraSCAti Examples: Peer-to-Peer Network in the Cloud
 * Copyright (C) 2011 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */
package org.ow2.frascati.examples.p2p.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data returned by both Peer and PeerCollection REST services.
 *
 * @author Philippe Merle - INRIA
 */
@XmlRootElement(name = "PeerInfo")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PeerInfo", propOrder = {
    "whoami",
    "url",
    "location",
    "canonicalHostName",
    "hostName",
    "ip",
    "date",
    "availableProcessors",
    "freeMemory",
    "totalMemory",
    "maxMemory",
    "latency"
})
public class PeerInfo
{
    @XmlElement(name="whoami")
    private String whoami;

    @XmlElement(name="url")
    private String url;

    @XmlElement(name="location")
    private Location location;

    @XmlElement(name="canonicalHostName")
    private String canonicalHostName;

    @XmlElement(name="hostName")
    private String hostName;

    @XmlElement(name="ip")
    private String ip;

    @XmlElement(name="date")
    private String date;

    @XmlElement(name="availableProcessors")
    private int availableProcessors;

    @XmlElement(name="freeMemory")
    private long freeMemory;

    @XmlElement(name="totalMemory")
    private long totalMemory;

    @XmlElement(name="maxMemory")
    private long maxMemory;

    @XmlElement(name="latency")
    private long latency;

    public final String getWhoAmI()
    {
      return this.whoami;
    }

    public final void setWhoAmI(String whoami)
    {
      this.whoami = whoami;
    }

    public final String getUrl()
    {
      return this.url;
    }

    public final void setUrl(String url)
    {
      this.url = url;
    }

    public final Location getLocation()
    {
      return this.location;
    }

    public final void setLocation(Location location)
    {
      this.location = location;
    }

    public final String getCanonicalHostName()
    {
      return this.canonicalHostName;
    }

    public final void setCanonicalHostName(String canonicalHostName)
    {
      this.canonicalHostName = canonicalHostName;
    }

    public final String getHostName()
    {
      return this.hostName;
    }

    public final void setHostName(String hostName)
    {
      this.hostName = hostName;
    }

    public final String getIp()
    {
      return this.ip;
    }

    public final void setIp(String ip)
    {
      this.ip = ip;
    }

    public final String getDate()
    {
      return this.date;
    }

    public final void setDate(String date)
    {
      this.date = date;
    }

    public final int getAvailableProcessors()
    {
      return this.availableProcessors;
    }

    public final void setAvailableProcessors(int availableProcessors)
    {
      this.availableProcessors = availableProcessors;
    }

    public final long getFreeMemory()
    {
      return this.freeMemory;
    }

    public final void setFreeMemory(long freeMemory)
    {
      this.freeMemory = freeMemory;
    }

    public final long getTotalMemory()
    {
      return this.totalMemory;
    }

    public final void setTotalMemory(long totalMemory)
    {
      this.totalMemory = totalMemory;
    }

    public final long getMaxMemory()
    {
      return this.maxMemory;
    }

    public final void setMaxMemory(long maxMemory)
    {
      this.maxMemory = maxMemory;
    }

    public final long getLatency()
    {
      return this.latency;
    }

    public final void setLatency(long latency)
    {
      this.latency = latency;
    }
}
