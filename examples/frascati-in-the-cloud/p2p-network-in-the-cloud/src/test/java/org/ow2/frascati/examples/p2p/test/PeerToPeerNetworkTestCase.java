/**
 * OW2 FraSCAti Examples: Peer-to-Peer Network in the Cloud
 * Copyright (C) 2011 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */
package org.ow2.frascati.examples.p2p.test;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

import org.ow2.frascati.examples.test.FraSCAtiTestCase;
import org.ow2.frascati.examples.p2p.api.Peer;
import org.ow2.frascati.examples.p2p.api.PeerCollection;
import org.ow2.frascati.examples.p2p.api.PeerInfo;

public class PeerToPeerNetworkTestCase
     extends FraSCAtiTestCase
{
  public final String getComposite()
  {
    return "peer";
  }
  
  @Test
  public final void testService()
  {
    Peer peer = getService(Peer.class, "Peer");
    PeerInfo peerInfo = peer.getPeerInfo();
    PeerCollection peerCollection = getService(PeerCollection.class, "PeerCollection");
    Collection<PeerInfo> peerInfos = peerCollection.getPeerInfos();
    Assert.assertEquals("peerInfos.size()", 13 /*16*/, peerInfos.size());
  }
}
