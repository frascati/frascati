============================================================================
OW2 FraSCAti in Cloud Foundry
Copyright (C) 2011 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):
 
============================================================================

OW2 FraSCAti in Cloud Foundry:
------------------------------

This is the source code of OW2 FraSCAti in Cloud Foundry available at
http://frascati.frascati.cloudfoundry.com/

Compilation with Maven:
-----------------------
  mvn install

Deploying the WAR in an embedded Web Application Server:
--------------------------------------------------------

   mvn jetty:run
or mvn tomcat:run
  
This command starts a standalone embedded Jetty or Tomcat server on port 8080.
This server loads OW2 FraSCAti in Cloud Foundry Web Application on the context path
'/frascati-cloud-foundry'.

Open your favorite Web browser and go to
http://localhost:8080/frascati-cloud-foundry/

Deploying the WAR on a Web Application server:
----------------------------------------------
Install target/frascati-cloud-foundry-<version>.war on your favorite
Web Application server, e.g., Jetty, Tomcat, JBoss, Geronimo, etc. 

For instance, for Apache Tomcat:
* copy the WAR to the Tomcat webapps directory:
  cp target/frascati-cloud-foundry-<version>.war $CATALINA_HOME/webapps/frascati-cloud-foundry.war
* Use the Tomcat Manager page to start the war.

For other Web servers, please refer to the documentation of your favorite server.

Deploying the WAR on Cloud Foundry:
-----------------------------------

  $ mvn clean install
  $ cd target
  $ vmc update frascati
