/**
 * OW2 FraSCAti Examples: Counter
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s): Nicolas Dolet
 */

package org.ow2.frascati.examples.counter.lib;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;
import org.ow2.frascati.examples.counter.api.CounterService;

/**
 * Implementation of a {@link CounterService} client component.
 * 
 * @author Philippe Merle
 */
@Service(Runnable.class)
public class Client
  implements Runnable
{
  /** The counter service to invoke. */
  @Reference
  private CounterService counter;

  /**
   * @see java.lang.Runnable#run()
   */
  public final void run()
  {
    java.io.PrintStream out = System.out;
    out.println("CounterService getValue: " + counter.getValue());
    out.println("CounterService increment(10)...");
    counter.increment(10);
    out.println("CounterService getValue: " + counter.getValue());
    out.println("CounterService decrement(10)...");
    counter.decrement(10);
    out.println("CounterService getValue: " + counter.getValue());
  }
}
