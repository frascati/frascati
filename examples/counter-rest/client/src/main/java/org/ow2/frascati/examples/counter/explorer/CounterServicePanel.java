/**
 * OW2 FraSCAti Examples: RESTful Counter
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.examples.counter.explorer;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;
import org.objectweb.util.explorer.api.TreeView;
import org.ow2.frascati.examples.counter.api.CounterService;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to interact with {@link CounterService} instances.
 *
 * @author Philippe Merle
 */
@SuppressWarnings("serial")
public class CounterServicePanel
     extends AbstractSelectionPanel<CounterService>
{
  /**
   * The label displaying the current value of the selected counter.
   */
  private JLabel valueLabel;

  /**
   * The default constructor creates the panel.
   */
  public CounterServicePanel()
  {
    super();
    this.setLayout(new GridLayout(4, 1));

    JPanel panel1 = new JPanel();
    this.add(panel1);
    panel1.add(new JLabel("Current counter value: "));
    valueLabel = new JLabel();
    panel1.add(valueLabel);

    JPanel panel2 = new JPanel();
    this.add(panel2);
    JButton incrementButton = new JButton("Increment");
    panel2.add(incrementButton);
    panel2.add(new JLabel("with: "));
    final JTextField incrementField = new JTextField(5);
    panel2.add(incrementField);
    incrementButton.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            try {
                // invoke the counter service.
                getSelection().increment(
                    Integer.parseInt(incrementField.getText())
                );
                // refresh the value of the counter.
               refreshValueLabel();
            } catch(NumberFormatException nfe) {
            	throw new Error(nfe);
            }
        }
      }
    );

    JPanel panel3 = new JPanel();
    this.add(panel3);
    JButton decrementButton = new JButton("Decrement");
    panel3.add(decrementButton);
    panel3.add(new JLabel("with: "));
    final JTextField decrementField = new JTextField(5);
    panel3.add(decrementField);
    decrementButton.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            try {
                // invoke the counter service.
                getSelection().decrement(
                    Integer.parseInt(decrementField.getText())
                );
                // refresh the value of the counter.
               refreshValueLabel();
            } catch(NumberFormatException nfe) {
                throw new Error(nfe);
            }
        }
      }
    );
 
    JPanel panel4 = new JPanel();
    this.add(panel4);
    JButton resetButton = new JButton("Reset the counter value");
    panel4.add(resetButton);
    resetButton.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            // invoke the counter service.
        	getSelection().resetIt();
            // refresh the value of the counter.
            refreshValueLabel();
        }
      }
    );
  }

  private void refreshValueLabel()
  {
    // display the current value of the counter.
    valueLabel.setText(String.valueOf(this.getSelection().getValue()));
  }

  /**
   * @see org.objectweb.util.explorer.api.Panel#selected(org.objectweb.util.explorer.api.TreeView)
   */
  @Override
  public final void selected(TreeView treeView)
  {
    // this sets the counter service returned by this.getSelection().
    super.selected(treeView);
    // refresh the value of the counter.
    refreshValueLabel();
  }
}
