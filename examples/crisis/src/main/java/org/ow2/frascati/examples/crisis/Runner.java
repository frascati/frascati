/**
 * OW2 FraSCAti: Crisis BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.crisis;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.examples.crisis.CrisisOrchestrator;
import org.ow2.frascati.examples.crisis.CrisisOrchestratorRequest;
import org.ow2.frascati.examples.crisis.CrisisOrchestratorResponse;

@Scope("COMPOSITE")
public class Runner
  implements Runnable
{

  @Reference(name = "crisis-orchestrator")
  private CrisisOrchestrator crisisOrchestrator;

  public final void run() {
	CrisisOrchestratorRequest request = new CrisisOrchestratorRequest();
    request.setInput("start isycri process");
    System.out.println("RUNNER: Calling CrisisOrchestrator.process(" + request + ")...");
    CrisisOrchestratorResponse response = crisisOrchestrator.process(request);
    System.out.println("RUNNER: Result = " + response.getResult());
  }

}
