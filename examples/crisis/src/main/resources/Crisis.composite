<?xml version="1.0" encoding="UTF-8"?>
<!--  OW2 FraSCAti: Crisis BPEL                                                     -->
<!--  Copyright (C) 2010 INRIA, University of Lille 1                               -->
<!--                                                                                -->
<!--  This library is free software; you can redistribute it and/or                 -->
<!--  modify it under the terms of the GNU Lesser General Public                    -->
<!--  License as published by the Free Software Foundation; either                  -->
<!--  version 2 of the License, or (at your option) any later version.              -->
<!--                                                                                -->
<!--  This library is distributed in the hope that it will be useful,               -->
<!--  but WITHOUT ANY WARRANTY; without even the implied warranty of                -->
<!--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU             -->
<!--  Lesser General Public License for more details.                               -->
<!--                                                                                -->
<!--  You should have received a copy of the GNU Lesser General Public              -->
<!--  License along with this library; if not, write to the Free Software           -->
<!--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA     -->
<!--                                                                                -->
<!--  Contact: frascati@ow2.org                                                     -->
<!--                                                                                -->
<!--  Author: Philippe Merle                                                        -->
<!--                                                                                -->
<!--  Contributor(s):                                                               -->
<!--                                                                                -->
<!--                                                                                -->
<composite xmlns="http://www.osoa.org/xmlns/sca/1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:frascati="http://frascati.ow2.org/xmlns/sca/1.1" xmlns:ns="classpath:" name="Crisis">
  <service name="runnable" promote="Runner/runnable">
    <interface.java interface="java.lang.Runnable"/>
  </service>
  <component name="Runner">
    <implementation.java class="org.ow2.frascati.examples.crisis.Runner"/>
    <service name="runnable">
      <interface.java interface="java.lang.Runnable"/>
    </service>
    <reference name="crisis-orchestrator" target="CrisisOrchestrator/client">
      <interface.wsdl interface="CrisisOrchestrator.wsdl#wsdl.interface(CrisisOrchestrator)"/>
    </reference>
  </component>
  <component name="RunnerScript">
    <service name="runnable">
      <interface.java interface="java.lang.Runnable"/>
    </service>
    <reference name="crisisOrchestrator" target="CrisisOrchestrator/client">
      <interface.wsdl interface="CrisisOrchestrator.wsdl#wsdl.interface(CrisisOrchestrator)"/>
    </reference>
    <frascati:implementation.script script="Runner.bsh"/>
  </component>
  <component name="CrisisOrchestrator">
    <implementation.bpel process="ns:CrisisOrchestrator.bpel"/>
    <service name="client">
      <interface.wsdl interface="CrisisOrchestrator.wsdl#wsdl.interface(CrisisOrchestrator)"/>
    </service>
    <reference name="policemen" target="Policemen/policemen">
      <interface.wsdl interface="Policemen.wsdl#wsdl.interface(Policemen)"/>
    </reference>
    <reference name="firemen" target="Firemen/firemen">
      <interface.wsdl interface="Firemen.wsdl#wsdl.interface(Firemen)"/>
    </reference>
    <reference name="redcross" target="Redcross/redcross">
      <interface.wsdl interface="Redcross.wsdl#wsdl.interface(Redcross)"/>
    </reference>
    <reference name="ems" target="Ems/ems">
      <interface.wsdl interface="Ems.wsdl#wsdl.interface(Ems)"/>
    </reference>
  </component>
  <component name="Policemen">
    <service name="policemen">
      <interface.wsdl interface="Policemen.wsdl#wsdl.interface(Policemen)"/>
    </service>
    <frascati:implementation.script script="Policemen.bsh"/>
  </component>
  <component name="Firemen">
    <service name="firemen">
      <interface.wsdl interface="Firemen.wsdl#wsdl.interface(Firemen)"/>
    </service>
    <frascati:implementation.script script="Firemen.bsh"/>
    <property name="numberOfPeople" type="xsd:int">10</property>
  </component>
  <component name="Redcross">
    <service name="redcross">
      <interface.wsdl interface="Redcross.wsdl#wsdl.interface(Redcross)"/>
    </service>
    <frascati:implementation.script script="Redcross.bsh"/>
  </component>
  <component name="Ems">
    <service name="ems">
      <interface.wsdl interface="Ems.wsdl#wsdl.interface(Ems)"/>
    </service>
    <frascati:implementation.script script="Ems.bsh"/>
  </component>
</composite>