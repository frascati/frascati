/**
 * OW2 FraSCAti Examples: Callback
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.callback.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.ow2.frascati.examples.callback.api.Notifier;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link Notifier} instances.
 *
 * @author Philippe Merle - INRIA
 */
@SuppressWarnings("serial")
public class NotifierPanel
     extends AbstractSelectionPanel<Notifier>
{
  /**
   * Default constructor to create the panel.
   */
  public NotifierPanel()
  {
    super();
    JButton button = new JButton("Send");
    add(button);
    final JTextField valueField = new JTextField(15);
    add(valueField);
    button.addActionListener( new ActionListener() {
      public final void actionPerformed(ActionEvent e) {
        // Call the selected notifier interface.
        selected.send(valueField.getText());
      }
    });
  }
}
