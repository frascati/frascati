/**
 * OW2 FraSCAti Examples : CallBack
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.callback.impl;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.examples.callback.api.Notifier;
import org.ow2.frascati.examples.callback.api.NotifierCallBack;

/**
 * Notification client Implementation.
 */
public class NotifierClient
  implements Notifier, NotifierCallBack
{
  /**
   * Reference to the notification server.
   */
  @Reference
  private Notifier notify;

  /**
   * Component name for this notification client.
   */
  @ComponentName
  private String componentName;

  /**
   * Print received notifications.
   */
  public final void onMessage(String msg)
  {
    System.out.println(componentName + ": receive onMessage '" + msg + "'");
  }

  /**
   * Send a message to the notify reference.
   */
  public final void send(String msg)
  {
    System.out.println(componentName +": receive '" + msg + "'");
    String newMsg = msg + " from " + componentName;
	System.out.println(componentName +": send '" + newMsg + "'");
    notify.send(newMsg);
  }

}
