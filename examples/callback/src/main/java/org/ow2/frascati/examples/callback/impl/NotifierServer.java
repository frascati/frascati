/**
 * OW2 FraSCAti Examples : CallBack
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.callback.impl;

import org.osoa.sca.annotations.Callback;
import org.osoa.sca.annotations.ComponentName;

import org.ow2.frascati.examples.callback.api.Notifier;
import org.ow2.frascati.examples.callback.api.NotifierCallBack;

/**
 * Notification server Implementation.
 */
public class NotifierServer
  implements Notifier
{
  /**
   * Component name for this notification server.
   */
  @ComponentName
  private String componentName;

  private NotifierCallBack notifierCallback;

  @Callback
  public final void setNotifierCallBack(NotifierCallBack notifierCallback)
  {
    System.out.println(componentName + ": setNotifierCallBack " + notifierCallback);
    this.notifierCallback = notifierCallback;
  }

  /**
   * @see org.ow2.frascati.examples.callback.Notifier#send(String)
   */
  public final void send(String msg)
  {
    System.out.println(componentName + ": receive '" + msg + "'");
    System.out.println(componentName + ": notifying caller...");
    notifierCallback.onMessage(msg);
    System.out.println(componentName + ": end receive");
  }

}
