/**
 * OW2 FraSCAti Examples : CallBack
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Damien Fournier
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.callback.test;

import org.junit.Test;

import org.ow2.frascati.test.FraSCAtiTestCase;

import org.ow2.frascati.examples.callback.api.Notifier;

public class CallbackTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testCallback() throws Exception
  {
    Notifier client1 = getService(Notifier.class, "client1");
    client1.send("Hello");
    Notifier client2 = getService(Notifier.class, "client2");
    client2.send("Hello");
  }

}
