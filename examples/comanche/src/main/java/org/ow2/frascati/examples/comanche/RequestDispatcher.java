/**
 * OW2 FraSCAti Examples: Comanche
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Pessemier
 * 
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 */

package org.ow2.frascati.examples.comanche;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.osoa.sca.annotations.Init;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class RequestDispatcher
  implements RequestHandler
{

  // handlers
  @Reference
  private List<RequestHandler> rh = new ArrayList<RequestHandler>();

  @Init
  public final void init()
  {
    // Sort connexions : first files, then errors, and others...
    List<RequestHandler> fileList = new ArrayList<RequestHandler>();
    List<RequestHandler> errorList = new ArrayList<RequestHandler>();
    List<RequestHandler> othersList = new ArrayList<RequestHandler>();
    for (RequestHandler requestHandler : rh) {
      HandlerType type = requestHandler.getType();
      if (type.equals(HandlerType.FILE)) {
        fileList.add(requestHandler);
      } else if (type.equals(HandlerType.ERROR)) {
        errorList.add(requestHandler);
      } else {
        othersList.add(requestHandler);
      }
    }
    rh = new ArrayList<RequestHandler>();
    rh.addAll(fileList);
    rh.addAll(errorList);
    rh.addAll(othersList);
  }

  // --------------------------------------------------------------------------
  // Implementation of the RequestHandler interface
  // --------------------------------------------------------------------------

  public final void handleRequest(Request r) throws IOException
  {
    // Ensure that the file handler is the first one.
    for (RequestHandler requestHandler : rh) {
      try {
        requestHandler.handleRequest(r);
        return;
      } catch (IOException exc) {
      }
    }
  }

  public final HandlerType getType()
  {
    return HandlerType.DELEGATING;
  }
}
