/**
 * OW2 FraSCAti Examples: Comanche
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Pessemier
 * 
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 */

package org.ow2.frascati.examples.comanche;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Property;

@Scope("COMPOSITE")
@Service(Runnable.class)
public class RequestReceiver
  implements Runnable
{
  @Reference
  private Scheduler s;

  @Reference
  private RequestHandler rh;

  @Property
  private int port = 8888;

  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------
 
  public final void run()
  {
    new Thread() {
      public void run() {
        try {
          ServerSocket ss = new ServerSocket(port);
          System.out.println("Comanche HTTP Server ready on port " + port + ".");
          System.out.println("Load http://localhost:" + port + "/gnu.jpg");
          while (true) {
            final Socket socket = ss.accept();
            s.schedule(new Runnable() {
              public final void run() {
                try {
                  rh.handleRequest(new Request(socket));
                } catch (IOException exc) {
                }
              }
            });
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }.start();
  }

}
