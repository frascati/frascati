/**
 * OW2 FraSCAti Examples: Comanche
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Pessemier
 * 
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 */

package org.ow2.frascati.examples.comanche;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.PrintStream;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class RequestAnalyzer
  implements RequestHandler
{
  private static final String GET = "GET ";

  @Reference(name = "bisrh")
  private RequestHandler rh;

  @Reference
  private Logger l;

  // --------------------------------------------------------------------------
  // Implementation of the RequestHandler interface
  // --------------------------------------------------------------------------

  public final void handleRequest(Request r) throws IOException
  {
    r.in = new InputStreamReader(r.s.getInputStream());
    r.out = new PrintStream(r.s.getOutputStream());
    String rq = new LineNumberReader(r.in).readLine();
    if(rq != null) {
      l.log(rq);
      if (rq.startsWith(GET)) {
        r.url = rq.substring(GET.length()+1, rq.indexOf(' ', GET.length()));
        rh.handleRequest(r);
      }
    }
    r.out.close();
    r.s.close();
  }

  public final HandlerType getType()
  {
    return HandlerType.DELEGATING;
  }
}
