/**
 * OW2 FraSCAti Examples: Comanche
 * Copyright (C) 2008-2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org 
 *
 * Author: Nicolas Pessemier
 * 
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 */

package org.ow2.frascati.examples.comanche;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.osoa.sca.annotations.Scope;

@Scope("COMPOSITE")
public class FileRequestHandler
  implements RequestHandler
{

  // --------------------------------------------------------------------------
  // Implementation of the RequestHandler interface
  // --------------------------------------------------------------------------

  public final void handleRequest(Request r) throws IOException
  {
    File f = new File(r.url);
    if (f.exists() && !f.isDirectory()) {
      InputStream is = new FileInputStream(f);
      byte[] data = null;
      try {
        data = new byte[is.available()];
        int len = is.read(data);
        if(len != data.length) {
          throw new IOException("File data not totally read");
        }
      } finally {
        is.close();
      }
      r.out.print("HTTP/1.0 200 OK\n\n");
      r.out.write(data);
    } else {
      throw new IOException("File not found");
    }
  }

  public final HandlerType getType()
  {
    return HandlerType.FILE;
  }
}
