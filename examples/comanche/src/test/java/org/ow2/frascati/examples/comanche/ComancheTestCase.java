/**
 * OW2 FraSCAti Examples: Comanche
 * Copyright (C) 2008-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Pessemier
 *
 * Contributor(s): Nicolas Dolet
 *                 Philippe Merle
 *
 */
package org.ow2.frascati.examples.comanche;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class ComancheTestCase
     extends FraSCAtiTestCase
{
    @Test
    public final void testImage() throws IOException, MalformedURLException, InterruptedException
    {
      Runnable service = getService(Runnable.class, "r");
      service.run();
      // Wait for 1 second to be sure that the Web server is started.
      Thread.sleep(1000);
      URL url = new URL("http://localhost:8888/gnu.jpg");
      File file = new File("gnu.jpg");
      BufferedImage remoteImage = ImageIO.read(url);
      BufferedImage localImage = ImageIO.read(file);
      int[] offsets1 = remoteImage.getData().getDataBuffer().getOffsets();
      int[] offsets2 = localImage.getData().getDataBuffer().getOffsets();
      Assert.assertTrue("The server image has not the expected size", offsets1.length == offsets2.length);
      for (int i = 0; i < offsets1.length; i++) {
        Assert.assertTrue("The server image seems to be corrupted.", offsets1[i] == offsets2[i]);
      }
    }
}
