/**
 * OW2 FraSCAti Examples: HelloWorld Script
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.examples.helloworld.script.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTextField;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;
import org.ow2.frascati.examples.helloworld.script.api.PrintService;

/**
 * Is the FraSCAti Explorer plugin to show {@link PrintService} instances.
 *
 * @author Philippe Merle
 */
@SuppressWarnings("serial")
public class PrintServicePanel
     extends AbstractSelectionPanel<PrintService>
{
  /**
   * Default constructor creates an empty white panel with a button.
   */
  public PrintServicePanel()
  {
    super();
    JButton button = new JButton("Print the following message:");
    add(button);
    final JTextField textField = new JTextField(20);
    add(textField);
    button.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
        	selected.printMessage(textField.getText());
        }
      }
    );
  }
}
