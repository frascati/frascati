============================================================================
OW2 FraSCAti Examples: HelloWorld Script
Copyright (C) 2009-2010 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s): Nicolas Dolet

============================================================================


HelloWorld Script:
------------------

This example shows how to implement SCA components with scripting languages as
BeanShell, Groovy, JavaScript, JRuby, and Jython.

Let's note that FraSCAti supports any scripting engine supporting the Java 6
Script Engine API (JSR-223). For using your favorite scripting language, just 
add in pom.xml all the Maven dependencies required by your favorite scripting 
engine.

This example provides an SCA composite composed of a main component bound
to a set of scripted client components. Each client component is bound to a 
scripted server component. See src/main/resources/helloworld-script.composite

The FraSCAti implementation type <frascati:implementation.script> has two attributes:
* 'script': to provide the location of the script, and
* 'language': to indicate the scripting language of the script.

The following table lists the scripting engine used to execute the script
according to the extension of the value of the 'script' attribute:

+-----------+-----------------------+
| Extension | Scripting Engine      |
+-----------+-----------------------+
| .bsh      | BeanShell 2.0b5       |
| .groovy   | Groovy 1.6.4          |
| .java     | BeanShell 2.0b5       |
| .js       | Mozilla Rhino 1.7     |
| .jy       | Jython 2.2.1          |
| .py       | Jython 2.2.1          |
| .rb       | JRuby 1.3.1           |
+-----------+-----------------------+

Examples:

<frascati:implementation.script script="MainClient.bsh"/>

will use BeanShell to execute the 'MainClient.bsh' script file.

<frascati:implementation.script script="Server.js"/>

will use Mozilla Rhino to execute the JavaScript 'Server.js' script file.

The following table lists the scripting engine used to execute the script
according to the value of the 'language' attribute:

+---------------+-----------------------+
| Language      | Scripting Engine      |
+---------------+-----------------------+
| beanshell     | BeanShell 2.0b5       |
| bsh           | BeanShell 2.0b5       |
| ECMAScript    | Mozilla Rhino 1.7     |
| ecmascript    | Mozilla Rhino 1.7     |
| Groovy        | Groovy 1.6.4          |
| groovy        | Groovy 1.6.4          |
| java          | BeanShell 2.0b5       |
| JavaScript    | Mozilla Rhino 1.7     |
| javascript    | Mozilla Rhino 1.7     |
| js            | Mozilla Rhino 1.7     |
| jruby         | JRuby 1.3.1           |
| jython        | Jython 2.2.1          |
| python        | Jython 2.2.1          |
| rhino         | Mozilla Rhino 1.7     |
| ruby          | JRuby 1.3.1           |
+---------------+-----------------------+

Examples:
 
<implementation.script language="xyz">
  Here the source code of the script.
</implementation.script>

Requirements:
-------------
Java 6 is required to run the example.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Compilation and execution with the FraSCAti script:
---------------------------------------------------
Not available because this example needs third party libraries not provided with the distribution.
Maven can download them for you.
