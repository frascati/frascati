<!--
  * OW2 FraSCAti Examples: Parent module
  *
  * Copyright (c) 2012-2013 Inria, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  * USA
  *
  * Contact: frascati@ow2.org
  *
  * Author: Philippe Merle
  *
  * Contributor(s):
  *
-->

This directory contains a set of FraSCAti examples.

------------------------------
How to use examples/pom.xml
------------------------------

The pom.xml file present at the root of this directory defines common 
Maven elements reused by all FraSCAti example's pom.xml files.

To reuse these common Maven definitions, your pom.xml must contain:

  <parent>
    <groupId>org.ow2.frascati.examples</groupId>
    <artifactId>parent</artifactId>
    <version>${frascati.version}</version>
  </parent>

------------------------------
examples/pom.xml properties
------------------------------

The examples/pom.xml file defines the following FraSCAti properties:

- config.directory:
  The directory where configuration files are present.
  By default, it is the examples/ directory.

- java.util.logging.config.file:
  The file to configure Java logging.
  By default, it is the ${config.directory}/logging.properties file.

- cxf.config.file:
  The file to configure Apache CXF.
  By default, it is the ${config.directory}/frascati-cxf.xml file.
  See at http://cxf.apache.org/docs/configuration.html for details on how to configure Apache CXF.

- org.ow2.frascati.class:
  The class used to instantiate FraSCAti.
  By default, it is the org.ow2.frascati.FraSCAti class.

- org.ow2.frascati.bootstrap:
  The class used to bootstrap FraSCAti.
  By default, it is the org.ow2.frascati.bootstrap.FraSCAti class.

  This property must be overloaded when the FraSCAti composite contains specific <binding> or <implementation>.
  This is the case when using FraSCAti Explorer, FScript, Remote Management, and Web Explorer.
  Current FraSCAti bootstrap values are:
  - org.ow2.frascati.bootstrap.FraSCAti:
    Basic FraSCAti bootstrap.
  - org.ow2.frascati.bootstrap.FraSCAtiJDT:
    Add support for JDT compiler.
  - org.ow2.frascati.bootstrap.FraSCAtiJDK6:
    Add support for JDK6 compiler.
  - org.ow2.frascati.bootstrap.FraSCAtiFractal:
    Add support for <frascati:implementation.fractal>.
    To use with FraSCAti Explorer, and FraSCAti FScript.
  - org.ow2.frascati.bootstrap.FraSCAtiJDTRest:
    Add support for JDT compiler and <frascati:binding.rest>.
    To use with FraSCAti Remote Management.
  - org.ow2.frascati.bootstrap.FraSCAtiJDTFractalRest:
    Add support for JDT compiler, <frascati:implementation.fractal> and <frascati:binding.rest>.
    To use with FraSCAti Remote Management + FScript.
  - org.ow2.frascati.bootstrap.FraSCAtiWebExplorer:
    To use with FraSCAti Web Explorer.

- org.ow2.frascati.composite:
  The SCA composite of FraSCAti.
  By default, it is the org.ow2.frascati.FraSCAti composite.

- org.ow2.frascati.binding.uri.base:
  The base URI to use for related bindings, i.e., where binding uri starting by /.
  This is the case for REST bindings of FraSCAti Remote Management and Web Explorer.
  By default, it is http://localhost:8765

  This property can be overloaded as following:
  $ mvn -Dorg.ow2.frascati.binding.uri.base=http://hostname:port/path ...
  Then related bindings will be exposed on http://hostname:port/path/...

- composite.file:
  The SCA composite file to compile, to execute, etc.

- service.name:
  The service name to execute.

- method.name:
  The method name to execute.

- method.params:
  The parameters for the method to execute.


All these properties can be overloaded:

- in your pom.xml as follows:

  <properties>
    <propertyName>propertyValue</propertyName>
  </properties>

- when running Maven:

  $ mvn -DpropertyName=propertyValue ...

------------------------------
examples/pom.xml plugins
------------------------------

The examples/pom.xml file configures the following Maven plugins:

- maven-compiler-plugin:
  To compile Java sources.

- maven-surefire-plugin:
  To execute Java unit tests.

- frascati-compiler-plugin:
  To compile the SCA ${composite.file} composite file.

------------------------------
examples/pom.xml profiles
------------------------------

The examples/pom.xml file provides the following Maven profiles:

- generate-SCA-diagrams:
  Generates diagrams for SCA composite files.
  Usage:
  $ mvn -Pgenerate-SCA-diagrams

  SCA diagrams are generated into the target/composite-diagrams directory.

  Let's note that this profile uses an Apache Tuscany specific Maven plugin.
  You should download latest Tuscany sources and install this plug-in locally.

- run:
  Run the SCA ${composite.file} composite.
  Usage:
  $ mvn -Prun

- explorer:
  Run the FraSCAti Explorer.
  Usage:
  $ mvn -Pexplorer

- explorer-fscript:
  Run the FraSCAti Explorer with its FScript plugin.
  Usage:
  $ mvn -Pexplorer-fscript

- fscript-console:
  Run the FraSCAti FScript console.
  Usage:
  $ mvn -Pfscript-console

- fscript-console-explorer:
  Run the FraSCAti FScript console and the FraSCAti Explorer.
  Usage:
  $ mvn -Pfscript-console-explorer

- explorer-jdk6:
  Run the FraSCAti Explorer with the JDK6 compiler.
  Usage:
  $ mvn -Pexplorer-jdk6

- jmx:
  Add support for FraSCAti JMX.
  Usage:
  $ mvn -Pjmx

- remote-fscript:
  Add support for FraSCAti Remote Management plus FScript.
  Usage:
  $ mvn -Premote-fscript

- remote:
  Add support for FraSCAti Remote Management.
  Usage:
  $ mvn -Premote

- uml:
  Add support for FraSCAti UML Sequence Diagram Aspect.
  Usage:
  $ mvn -Puml

- web:
  Add support for FraSCAti Web Explorer.
  Usage:
  $ mvn -Pweb

- one-jar:
  Generate an executable jar containing all required dependencies.
  Usage:
  $ mvn -Pone-jar

  To run the executable jar, type:
  $ java -jar target/<example-artifactid>-<frascati-version>.one-jar.jar

  To run the executable jar without One-Jar classloading information, type:
  $ java -Done-jar.silent=true -jar target/<example-artifactid>-<frascati-version>.one-jar.jar

  If the perm memory is too small then type:
  $ java -Xmx512M -XX:MaxPermSize=512M -jar target/<example-artifactid>-<frascati-version>.one-jar.jar

- all:
  Add support for all FraSCAti modules.
  Usage:
  $ mvn -Pall
