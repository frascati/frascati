/**
 * OW2 FraSCAti Examples: Fibonacci
 * Copyright (C) 2011 INRIA, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Romain Rouvoy
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.fibonacci.lib;

import org.oasisopen.sca.annotation.Scope;

import org.ow2.frascati.examples.fibonacci.api.Fibonacci;

/**
 * Naive implementation of the Fibonacci algorithm.
 * 
 * @author <a href="mailto:Romain.Rouvoy@lifl.fr">Romain Rouvoy</a>
 */
@Scope("COMPOSITE")
public class FibonacciImpl
  implements Fibonacci
{
    /**
     * @see org.ow2.frascati.examples.fibonacci.api.Fibonacci#compute(int)
     */
    public long compute(int n)
    {
      return (n <= 1) ? n : compute(n - 1) + compute(n - 2);
    }
}
