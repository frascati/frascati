============================================================================
OW2 FraSCAti Examples: Fibonacci
Copyright (C) 2011 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

Fibonacci:
----------

This example implements the Fibonacci algorithm as an SCA application and
exposes it as both a Web Service and a REST resource.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun
  
  This command launches the fibonacci SCA composite and its 'fibonacci'
  SCA service is exposed via both SOAP and REST bindings. This
  command is not giving back control until you press Ctrl+C.

  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)
  mvn -Prun,web                  (with FraSCAti Web Explorer)

Using the Fibonacci service:
----------------------------

Open your favorite Web browser and go to:
* http://localhost:8765/fibonacci-ws?wsdl to obtain the WSDL describing the
  Fibonacci Web Service. To invoke this service, use soapUI for instance.
* http://localhost:8765/fibonacci-rest?_wadl&_type=xml to obtain the WADL
  describing the Fibonacci REST resource.
* http://localhost:8765/fibonacci-rest/10 to obtain the result of Fibonacci(10).
  You should obtain 55 as reply.

This example is also deployed in the Clouds at:
* http://frascati.frascati.cloudbees.net/
* http://ow2-frascati.appspot.com/
