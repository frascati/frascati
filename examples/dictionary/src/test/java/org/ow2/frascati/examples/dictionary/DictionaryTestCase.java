/**
 * OW2 FraSCAti Examples: Dictionary
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */
package org.ow2.frascati.examples.dictionary;

import java.io.StringReader;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.frascati.examples.dictionary.api.Dictionary;
import org.ow2.frascati.test.FraSCAtiTestCase;

public class DictionaryTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testService()
  {
    // Try 3 times to reach the service before giving up
    int nbTries = 3;
    boolean success = false;
    String response = null;
    while (nbTries > 0 && !success) {
      try {
        Dictionary dict = getService(Dictionary.class, "Dictionary");
        response = dict.definition("middleware");
        System.out.println(response);
        success = true;
      } catch (RuntimeException e) {
        System.err.println("Cannot get the service response...");
        System.err.println("Number of tries left: " + (nbTries - 1));
        // Sleep 15 seconds
        try {
          Thread.currentThread().sleep(15000);
        } catch (InterruptedException ie) {
          throw new Error(ie);
        }
      }
      nbTries--;
    }
    Assert.assertNotSame("Service unavailable after 3 tries", 0, nbTries);
    StringReader sr = new StringReader(response);
    StringReader srExpected = new StringReader(expected);
    int b;
    int line = 1, col = 1;
    try {
      while ((b = sr.read()) != -1) {
        char c = (char) b;
        char e = (char) srExpected.read();
        Assert.assertEquals("Reading from server '" + c + "' while expecting '"
            + e + "', line " + line + " - col " + col, e, c);
        if (c == '\n') {
          line++;
          col = 1;
        } else {
          col++;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private final static String expected =
"Definition 1:middleware\n" +
"     \n" + 
"        Software that mediates between an {application program} and a\n" +
"        {network}.  It manages the interaction between disparate\n" +
"        applications across the heterogeneous computing {platform}s.\n" +
"        The {Object Request Broker} (ORB), software that manages\n" +
"        communication between {objects}, is an example of a middleware\n" +
"        program.\n" +
"     \n" +
"     \n" +
"\n";
}
