/**
 * OW2 FraSCAti Examples: HelloWorld Web Service Asynchronous
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.ws.async;

import java.util.concurrent.Future;
import javax.xml.ws.AsyncHandler;
import javax.xml.ws.Response;

import org.apache.hello_world_soap_http.Greeter;
import org.apache.hello_world_soap_http.PingMeFault;
import org.apache.hello_world_soap_http.types.FaultDetail;
import org.apache.hello_world_soap_http.types.GreetMeResponse;
import org.apache.hello_world_soap_http.types.PingMeResponse;
import org.apache.hello_world_soap_http.types.SayHiResponse;

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(Greeter.class)
public class GreeterImpl implements Greeter
{
    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#greetMeAsync(java.lang.String)
     */
    public Response<GreetMeResponse> greetMeAsync(
       java.lang.String me
    ) {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#greetMeAsync(java.lang.String,AsyncHandler)
     */
    public Future<?> greetMeAsync(
        java.lang.String me,
        AsyncHandler<GreetMeResponse> asyncHandler
    ) {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#greetMe(java.lang.String)
     */
    public String greetMe(String me) {
        System.out.println("GreaterImpl - Executing operation greetMe");
        System.out.println("GreaterImpl - Message received: " + me + "\n");
        return "Hello " + me;
    }
    
    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#greetMeOneWay(java.lang.String)
     */
    public void greetMeOneWay(String me) {
        System.out.println("GreaterImpl - Executing operation greetMeOneWay\n");
        System.out.println("GreaterImpl - Hello there " + me);
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#sayHiAsync()
     */
    public Response<SayHiResponse> sayHiAsync()
    {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#sayHiAsync(AsyncHandler)
     */
    public Future<?> sayHiAsync(
        AsyncHandler<SayHiResponse> asyncHandler
    ) {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#sayHi()
     */
    public String sayHi() {
        System.out.println("GreaterImpl - Executing operation sayHi\n");
        return "Bonjour";
    }
    
    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#pingMeAsync()
     */
    public Response<PingMeResponse> pingMeAsync()
    {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#pingMeAsync(AsyncHandler)
     */
    public Future<?> pingMeAsync(
        AsyncHandler<PingMeResponse> asyncHandler
    )
    {
        /* never called */
        throw new UnsupportedOperationException();
    }

    /** (non-Javadoc)
     * @see org.apache.hello_world_soap_http.Greeter#pingMe()
     */
    public void pingMe() throws PingMeFault {
        System.out.println("GreaterImpl - Executing operation pingMe, throwing PingMeFault exception\n");
        FaultDetail faultDetail = new FaultDetail();
        faultDetail.setMajor((short)2);
        faultDetail.setMinor((short)1);
        throw new PingMeFault("PingMeFault raised by server", faultDetail);
    }
}
