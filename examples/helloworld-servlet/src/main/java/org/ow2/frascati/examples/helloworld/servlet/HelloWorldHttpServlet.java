/**
 * OW2 FraSCAti Examples: HelloWorld Servlet
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.examples.helloworld.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osoa.sca.annotations.Property;

public class HelloWorldHttpServlet
     extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	/**
	 * A configurable property 'name'.
	 */
	@Property(name = "name")
	private String name;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public final void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException 
    {
      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      String query = request.getQueryString();
      out.println("<html><body>Hello " + (query==null?"World":query) + " I'm " + name + "</body></html>");
      out.close();
    }

}
