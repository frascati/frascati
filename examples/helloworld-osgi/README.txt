============================================================================
OW2 FraSCAti Examples: HelloWorld OSGi
Copyright (C) 2010-2011 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s): Christophe Munilla
 
============================================================================

HelloWorld OSGi Example:
------------------------

This example shows how to embed existing OSGi bundles into an SCA application, i.e.,
use of <frascati:implementation.osgi bundle="..."/> implemented with Apache Felix,
Eclipse Equinox and Knopflerfish.

The directory 'bundles' contains three OSGi bundles (api, server, and client).
The directory 'sca' contains an SCA composite embedding the three OSGi bundles,
see sca/src/main/resources/helloworld-osgi.composite
The directory 'osgi/felix' contains the pom.xml to run the SCA composite with Apache Felix.
The directory 'osgi/equinox' contains the pom.xml to run the SCA composite with Eclipse Equinox.
The directory 'osgi/knopflerfish' contains the pom.xml to run the SCA composite with Knopflerfish.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  With Apache Felix:
  cd osgi/felix
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

  With Eclipse Equinox:
  cd osgi/equinox
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

  With Knopflerfish:
  cd osgi/knopflerfish
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Execution with the FraSCAti script:
-----------------------------------
You have to compile this example with Maven.

TO BE COMPLETED
