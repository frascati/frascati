/**
 * OW2 FraSCAti Examples: HelloWorld OSGi
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.osgi.api;

import java.util.Dictionary;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator
  implements BundleActivator
{
    public final void start( BundleContext context )
    {
        Bundle bundle = context.getBundle();
        Dictionary dict = bundle.getHeaders();
        String name = (String) dict.get("Bundle-Name");
        System.err.println("Bundle <"+name+"> started.");
    }

    public final void stop( BundleContext context )
    {
        Bundle bundle = context.getBundle();
        Dictionary dict = bundle.getHeaders();
        String name = (String) dict.get("Bundle-Name");
        System.err.println("Bundle <"+name+"> stopped.");
    }

}
