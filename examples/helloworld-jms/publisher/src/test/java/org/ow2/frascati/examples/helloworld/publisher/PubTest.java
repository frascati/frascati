  /*
  * OW2 FraSCAti Examples: Parent module
  *
  * Copyright (c) 2009-2011 INRIA, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * Contact: frascati@ow2.org
  *
  * Author: Fernand PARAÏSO
  *
  * Contributor(s): Phillipe MERLE
  *                 
  *                 
  *                 
  */
package org.ow2.frascati.examples.helloworld.publisher;

import org.junit.BeforeClass;
import org.junit.Test;
import org.objectweb.fractal.api.Component;
import org.ow2.frascati.FraSCAti;

public class PubTest {
	public static final String COMPOSITE = "publisher.composite";

	public static FraSCAti frascati;

	public static Component composite;

	@BeforeClass
	public static void init() throws Exception {
		frascati = FraSCAti.newFraSCAti();
		composite = frascati.getComposite(COMPOSITE);
	}

	@Test
	public void EpserTest() throws Exception {
		System.out.println("Publish Message");
		Runnable publisherService = (Runnable) frascati.getService(composite,
				"publisher-run", Runnable.class);
	}
}
