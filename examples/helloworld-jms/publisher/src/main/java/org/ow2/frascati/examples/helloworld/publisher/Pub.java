package org.ow2.frascati.examples.helloworld.publisher;

import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.helloworld.jms.sender.Sender;
import org.ow2.frascati.examples.helloworld.jms.sender.Message;
import org.ow2.frascati.examples.helloworld.jms.sender.ResponseMessage;

@Service(interfaces = { Runnable.class, Sender.class})
public class Pub implements Runnable, Sender {
	@Reference(required = false)
	Sender jmsWire;
	
	@ComponentName
	private String componentName;
	
	public final void run() {
		//Os Configuration informations
		String osName = System.getProperty ("os.name");
		String osVersion = System.getProperty ("os.version");
		int nbProc = Runtime.getRuntime().availableProcessors();
		String username = System.getProperty("user.name");
		
		System.out.println("Node(name=" + componentName + ") Sent()...");
		//Jms message
		Message message = new Message();

		if(jmsWire !=null){
			message.setOsName(osName);
			message.setOsVersion(osVersion);
			message.setNbProc(nbProc);
			message.setOsUsername(username);
			//Send event via jms to the remote Client
			jmsWire.send(message);
		}
	}
	
	public final ResponseMessage send(Message msg) {
		System.out.println("Receive message from client ...\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		System.out.println("Node(name=" + componentName + ") receive "
				+ toString(msg));
		//Os Configuration informations
		String osName = System.getProperty ("os.name");
		String osVersion = System.getProperty ("os.version");
		int nbProc = Runtime.getRuntime().availableProcessors();
		String username = System.getProperty("user.name");
		
		ResponseMessage response = new ResponseMessage();
		response.setOsName(osName);
		response.setOsVersion(osVersion);
		response.setNbProc(nbProc);
		response.setOsUsername(username);
		return response;
	}
	
	private String toString(Message msg) {
		StringBuffer sb = new StringBuffer();
		sb.append("Message ");
		sb.append(msg.getOsName());
		sb.append(msg.getOsVersion());
		sb.append(msg.getNbProc());
		sb.append(msg.getOsUsername());
		sb.append(" ");
		return sb.toString();
	}
}

