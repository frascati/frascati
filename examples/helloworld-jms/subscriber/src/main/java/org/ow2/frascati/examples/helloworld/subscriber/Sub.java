package org.ow2.frascati.examples.helloworld.subscriber;
import org.osoa.sca.annotations.ComponentName;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.helloworld.jms.sender.Sender;
import org.ow2.frascati.examples.helloworld.jms.sender.Message;
import org.ow2.frascati.examples.helloworld.jms.sender.ResponseMessage;

@Service(interfaces = { Runnable.class, Sender.class})
public class Sub implements Runnable, Sender {
	public final void run() {
		System.out.println("Node(name=" + componentName + ") run()...");
	}

	public final ResponseMessage send(Message msg) {
		System.out.println("Receive message from client ...\n");
		System.out.println("Subscriber (name=" + componentName + ") receive "
				+ toString(msg));
		//Os Configuration informations
		String osName = System.getProperty ("os.name");
		String osVersion = System.getProperty ("os.version");
		int nbProc = Runtime.getRuntime().availableProcessors();
		String username = System.getProperty("user.name");
		
		ResponseMessage response = new ResponseMessage();
		response.setOsName(osName);
		response.setOsVersion(osVersion);
		response.setNbProc(nbProc);
		response.setOsUsername(username);
		return response;
	}

	@ComponentName
	private String componentName;

	private String toString(Message msg) {
		StringBuffer sb = new StringBuffer();
		String token = "\n";
		sb.append("Message ");
		sb.append(token+"OS NAME : "+msg.getOsName()+token);
		sb.append("OS VERSION : "+ msg.getOsVersion()+token);
		sb.append("NUMBER OF PROCESSOR: "+msg.getNbProc()+token);
		sb.append("USERNAME : "+msg.getOsUsername()+token);
		sb.append(" ");
		return sb.toString();
	}

}
