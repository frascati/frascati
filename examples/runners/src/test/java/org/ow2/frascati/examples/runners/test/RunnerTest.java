/**
 * OW2 FraSCAti Examples: Runners
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.runners.test;

import org.junit.Test;

import org.objectweb.fractal.api.Component;
import org.objectweb.fractal.api.Interface;
import org.objectweb.fractal.bf.BindingFactory;
import org.objectweb.fractal.util.Fractal;

import org.ow2.frascati.FraSCAti;

public class RunnerTest
{
    @Test
    public void runnerReconfiguration() throws Exception {
      // Initialize FraSCAti.
      FraSCAti frascati = FraSCAti.newFraSCAti();
      // Load the SCA composite 'runners'.
      Component composite = frascati.getComposite("runners.composite");
      // Obtain its Runnable service.
      Runnable runnable = frascati.getService(composite, "Runnable", Runnable.class);

      // RunnerMultiple, A, B, and C components will be invoked.
      runnable.run();
      
      for(int i=0; i<2; i++) { System.out.println(); }

      // Search components named 'RunnerMultiple' and 'D'.
      Component componentRunnerMultiple = null;
      Component componentD = null;
      // Iterate over all sub components of the loaded composite.
      for(Component component : Fractal.getContentController(composite).getFcSubComponents()) {
        // Get the name of the current sub component.
        String componentName = Fractal.getNameController(component).getFcName();
        if("RunnerMultiple".equals(componentName)) {
          componentRunnerMultiple = component;
        }
        if("D".equals(componentName)) {
          componentD = component;
        }
      }
      // Get the Runnable service of component D.
      Runnable runnableD = (Runnable)componentD.getFcInterface("Runnable");

      //
      // With Julia then Tinfi, the method bindFc of BindingController can be invoked on a started component.
      // However here, we stop the component before calling bindFc() in order to be sure that
      // no business activities are run inside the component during the binding reconfiguration.
      //
      
      Fractal.getLifeCycleController(componentRunnerMultiple).stopFc();
      // Connect the 'runnables' reference of the RunnerMultiple component to the 'Runnable' service of the component D.
      Fractal.getBindingController(componentRunnerMultiple).bindFc("runnables-D", runnableD);
      Fractal.getLifeCycleController(componentRunnerMultiple).startFc();

      // RunnerMultiple, A, B, C, and D components will be invoked.
      runnable.run();

      for(int i=0; i<2; i++) { System.out.println(); }

      // Get the FraSCAti Binding Factory.
      BindingFactory bf = (BindingFactory)frascati.getFrascatiComposite().getFcInterface("binding-factory");

      // Hints for creating a Web Service binding.
      java.util.Map<String, Object> hints = new java.util.HashMap<String, Object>();
      hints.put("plugin.id", "ws");
      hints.put("uri", "http://localhost:8765/D");

      Fractal.getLifeCycleController(componentRunnerMultiple).stopFc();
      // Bind the runnables reference to a Web Service.
      bf.bind(componentRunnerMultiple, "runnables-D-WS-1", hints);
      Fractal.getLifeCycleController(componentRunnerMultiple).startFc();

      for(int i=0; i<2; i++) { System.out.println(); }

      Fractal.getLifeCycleController(componentRunnerMultiple).stopFc();
      // Bind the runnables reference to a Web Service.
      bf.bind(componentRunnerMultiple, "runnables-D-WS-2", hints);
      Fractal.getLifeCycleController(componentRunnerMultiple).startFc();

      for(int i=0; i<2; i++) { System.out.println(); }

      // RunnerMultiple, A, B, C, D, D, and D will be invoked.
      // D is invoked three times as it was bound three times but with three different binding names.
      runnable.run();
      
      for(int i=0; i<2; i++) { System.out.println(); }

      Fractal.getLifeCycleController(componentRunnerMultiple).stopFc();
      // Disconnect the 'runnables' reference of the RunnerMultiple component to the 'Runnable' service of the component A, B, C, and D.
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-A");
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-B");
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-C");
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-D");
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-D-WS-1");
      Fractal.getBindingController(componentRunnerMultiple).unbindFc("runnables-D-WS-2");
      Fractal.getLifeCycleController(componentRunnerMultiple).startFc();

      // RunnerMultiple will be invoked.
      runnable.run();

      for(int i=0; i<2; i++) { System.out.println(); }

      for(Object o : componentRunnerMultiple.getFcInterfaces()) {
        Interface fractalInterface = (Interface)o;
        System.out.println("* " + fractalInterface.getFcItfName());
      }
      for(int i=0; i<2; i++) { System.out.println(); }

      for(String name : Fractal.getBindingController(componentRunnerMultiple).listFc()) {
        System.out.println("* " + name);
      }
      for(int i=0; i<2; i++) { System.out.println(); }

      frascati.close(composite);
      frascati.close();
    }

}
