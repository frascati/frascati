/**
 * OW2 FraSCAti Examples: Runners
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.runners;

import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.types.Step;

import org.oasisopen.sca.annotation.Init;
import org.oasisopen.sca.annotation.Service;
import org.oasisopen.sca.annotation.Reference;

public class RunnerMultiple extends Runner
{
	/**
	 * Method called at the end of the initialization of the SCA component.
	 */
    @Init
    public void init()
    {
      System.out.println("INIT on component " + this.componentName);
    }

	/**
	 * Method called each time Fractal LifecycleController startFc() is invoked.
	 */
    @Lifecycle(step=Step.START)
    public void start()
    {
      System.out.println("START on component " + this.componentName);
    }

    private java.util.List<Runnable> runnables;

    /**
     * Method called to set the reference 'runnables'.
     */
    @Reference(name="runnables")
    public void setRunnables(java.util.List<Runnable> runnables)
    {
      System.out.println("SET reference 'runnables' of component " + this.componentName + " with a list of " + runnables.size() + " elements.");
      this.runnables = runnables;
    }

    /**
     * Method called each time Fractal BindingController bindFc("runnablesXXX", o) is invoked.
     */
    public java.util.List<Runnable> getRunnables()
    {
      System.out.println("GET reference 'runnables' of " + this.componentName + ".");
      return this.runnables;
    }

    @Override
    public void run()
    {
      super.run();
      for(Runnable runnable : this.runnables) {
        runnable.run();
      }
    }
}
