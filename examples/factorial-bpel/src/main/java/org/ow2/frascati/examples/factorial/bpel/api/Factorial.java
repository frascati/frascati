/**
 * OW2 FraSCAti Examples: Factorial BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.factorial.bpel.api;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * This class was generated by Apache CXF 2.2.9
 * Tue Jul 06 21:32:29 CEST 2010
 * Generated source version: 2.2.9
 * 
 */
 
@WebService(targetNamespace = "http://frascati.ow2.org/examples/factorial/bpel/api", name = "Factorial")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface Factorial
{
    @WebResult(name = "FactorialResponse", targetNamespace = "http://frascati.ow2.org/examples/factorial/bpel/api", partName = "value")
    @WebMethod
    @GET
    @Path("/{value}")
    @Produces("text/plain")
    double factorial(
        @WebParam(partName = "value", name = "FactorialRequest", targetNamespace = "http://frascati.ow2.org/examples/factorial/bpel/api")
        @PathParam("value")
        double value
    );
}
