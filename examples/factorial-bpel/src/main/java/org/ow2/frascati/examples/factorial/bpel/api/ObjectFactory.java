/**
 * OW2 FraSCAti Examples: Factorial BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.factorial.bpel.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.ow2.frascati.examples.factorial.bpel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory
{
	private static final String API_NS = "http://frascati.ow2.org/examples/factorial/bpel/api";

    private static final QName FACTORIAL_RESPONSE_QNAME = new QName(API_NS, "FactorialResponse");
    private static final QName FACTORIAL_REQUEST_QNAME = new QName(API_NS, "FactorialRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.ow2.frascati.examples.factorial.bpel.api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = API_NS, name = "FactorialResponse")
    public final JAXBElement<Double> createFactorialResponse(Double value) {
        return new JAXBElement<Double>(FACTORIAL_RESPONSE_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = API_NS, name = "FactorialRequest")
    public final JAXBElement<Double> createFactorialRequest(Double value) {
        return new JAXBElement<Double>(FACTORIAL_REQUEST_QNAME, Double.class, null, value);
    }

}
