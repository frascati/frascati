/**
 * OW2 FraSCAti Examples: Factorial BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.factorial.bpel.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.ow2.frascati.examples.factorial.bpel.api.Factorial;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link Factorial} instances.
 *
 * @author Philippe Merle - INRIA
 */
@SuppressWarnings("serial")
public class FactorialPanel
     extends AbstractSelectionPanel<Factorial>
{
  /**
   * Default constructor to create the panel.
   */
  public FactorialPanel()
  {
    super();
    JButton button = new JButton("Factorial");
    add(button);
    add(new JLabel("("));
    final JTextField valueField = new JTextField(5);
    add(valueField);
    add(new JLabel(") ==> "));
    final JLabel resultLabel = new JLabel("");
    add(resultLabel);
    button.addActionListener( new ActionListener() {
      public final void actionPerformed(ActionEvent e) {
        try {
          // Get the user's input.
          double value = Double.valueOf(valueField.getText());
          // Call the factorial service.
          double result = selected.factorial(value);
          // Display the result.
          resultLabel.setText(Double.toString(result));
        } catch(NumberFormatException nfe) {
          // Error when input is not an integer.
          throw new RuntimeException(nfe);

// TODO: This will be better to display a graphical alert!

        }
      }
    });
  }
}
