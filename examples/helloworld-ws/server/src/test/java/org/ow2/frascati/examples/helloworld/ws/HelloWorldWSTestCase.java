/**
 * OW2 FraSCAti Examples: HelloWorld WS Server
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.helloworld.ws;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Test;
import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 * Test the HelloWorld Web Service server
 *
 */
public class HelloWorldWSTestCase
     extends FraSCAtiTestCase
{
  @Test
  public void getWSDL() throws Exception
  {
	String wsdlLocation = "http://localhost:9000/HelloService?wsdl";
    System.out.println("Getting WSDL at: " + wsdlLocation + "...");
    URL wsdl = new URL(wsdlLocation);
    BufferedReader in = new BufferedReader(new InputStreamReader(wsdl
        .openStream()));
    String inputLine, expectedLine;
    BufferedReader reader = new BufferedReader(new FileReader(
        "../client/src/main/wsdl/HelloService.wsdl"));
    while ((expectedLine = reader.readLine()) != null
        && (inputLine = in.readLine()) != null) {
      Assert.assertEquals("Error in WSDL file exposed on the server ",
          expectedLine, inputLine);
    }
    in.close();
    reader.close();
    System.out.println("The WSDL is correctly exposed.");
  }
}
