/**
 * OW2 FraSCAti Examples: ISBN test
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 */

package org.ow2.frascati.examples.isbntest.lib;

import org.osoa.sca.annotations.Reference;
import org.ow2.frascati.examples.isbntest.api.Verifier;

import com.daehosting.webservices.isbn.ISBNServiceSoapType;

/**
 * SCA Java Component implementation using an ISBN test web service
 */
public class Client
  implements Verifier
{
  @Reference
  private ISBNServiceSoapType isbn;
  
  // --------------------------------------------------------------------------
  // Implementation of the Verifier interface
  // --------------------------------------------------------------------------

  public final boolean verify(String type, String isbnNumber)
  {
    System.out.println("Checking " + type + ": " + isbnNumber + "...");
    boolean isValid;
    if (type.equals(Verifier.ISBN_10_TYPE)) {
      isValid = isbn.isValidISBN10(isbnNumber);
    } else if (type.equals(Verifier.ISBN_13_TYPE)) {
      isValid = isbn.isValidISBN13(isbnNumber);
    } else {
      System.err.println("Invalid ISBN format.");
      return false;
    }
    System.out.println("ISBN " + isbnNumber + " is " + (isValid ? "" : "not ")
        + "valid.");
    return isValid;
  }
}
