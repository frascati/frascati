/**
 * OW2 FraSCAti Examples: ISBN test
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.examples.isbntest.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.ow2.frascati.examples.isbntest.api.Verifier;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link Verifier} instances.
 *
 * @author Christophe Demarey
 */
@SuppressWarnings("serial")
public class VerifierPanel
     extends AbstractSelectionPanel<Verifier>
{
  /**
   * Default constructor creates the panel.
   */
  public VerifierPanel()
  {
    super();

    final JComboBox list = new JComboBox();
    list.setModel(new javax.swing.DefaultComboBoxModel(new String[] { Verifier.ISBN_10_TYPE, Verifier.ISBN_13_TYPE }));
    list.setName("isbnType");

    
    final JTextField textField = new JTextField(20);    
    JButton button = new JButton("Check ISBN");    
    final JLabel result = new JLabel("");
    
    button.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
        	result.setText(selected.verify((String) list.getSelectedItem(), textField.getText()) ? "true" : "false");
        }
      }
    );

    // layout
    org.jdesktop.layout.GroupLayout mainPanelLayout = new org.jdesktop.layout.GroupLayout(this);
    this.setLayout(mainPanelLayout);
    mainPanelLayout.setHorizontalGroup(
        mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
        .add(mainPanelLayout.createSequentialGroup()
            .add(26, 26, 26)
            .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                .add(button)
                .add(mainPanelLayout.createSequentialGroup()
                    .add(list, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 80, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                    .add(textField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(result))
            .addContainerGap(21, Short.MAX_VALUE))
    );
    mainPanelLayout.setVerticalGroup(
        mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
        .add(mainPanelLayout.createSequentialGroup()
            .add(32, 32, 32)
            .add(mainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                .add(textField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(list, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(18, 18, 18)
            .add(button)
            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
            .add(result)
            .addContainerGap(32, Short.MAX_VALUE))
    );    
  }
}
