/**
 * OW2 FraSCAti Examples: ISBN test
 * Copyright (C) 2009-2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Nicolas Dolet
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.isbntest;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.frascati.examples.isbntest.api.Verifier;
import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 * Test the ISBN test webservice
 *
 */
public class IsbnTestTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testService()
  {
    // Try 3 times to reach the service before giving up
    int nbTries = 3;
    boolean success = false;
    Verifier verifier = getService(Verifier.class, "Verifier");
    while (nbTries > 0 && !success) {
      try {
        String[] request;
        Boolean response;
        for (String s : validRequests) {
          request = s.split(" ");
          response = verifier.verify(request[0], request[1]);
          Assert.assertNotNull("Server response is null for " + request[1],
              response);
          Assert.assertTrue(request[1]
              + " should be detected as a valid number!", response
              .booleanValue());
        }
        for (String s : invalidRequests) {
          request = s.split(" ");
          response = verifier.verify(request[0], request[1]);
          Assert.assertNotNull("Server response is null for " + request[1],
              response);
          Assert.assertFalse(request[1]
              + " should be detected as an unvalid number", response
              .booleanValue());
        }
        success = true;
      } catch (RuntimeException e) {
        System.err.println("Cannot get the service response...");
        System.err.println("Number of tries left: " + (nbTries - 1));
        // Sleep 15 seconds
        try {
          Thread.currentThread().sleep(15000);
        } catch (InterruptedException ie) {
          throw new Error(ie);
        }
      }
      nbTries--;
    }
    Assert.assertNotSame("Service unavailable after 3 tries", 0, nbTries);
  }
  
  private static final String[] validRequests =
    new String[] {
        "ISBN-10 0131465759",
        "ISBN-10 0321180860",
        "ISBN-10 0470054352",
        "ISBN-13 978-0131465756",
        "ISBN-13 978-0321180865",
        "ISBN-13 978-0470054352",
    };

  private static final String[] invalidRequests =
    new String[] {
        "ISBN-10 2-2222-1111-1",
        "ISBN-13 999-9-9999-9999-9",
    };
}
