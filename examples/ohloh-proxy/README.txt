============================================================================
OW2 FraSCAti Examples: Ohloh Proxy
Copyright (C) 2009-2010 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

FraSCAti Ohloh Proxy:
---------------------

This example shows how to consume the Ohloh RESTful services with FraSCAti.
This proxy client is then exposed as a new RESTful service.
See at http://www.ohloh.net/api/getting_started for more details on the
Ohloh RESTful API.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  1) Standalone execution
     cd standalone
     mvn -Prun                      (standalone execution)
     mvn -Pexplorer                 (with FraSCAti Explorer)
     mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
     mvn -Pfscript-console          (with FraSCAti FScript Console)
     mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
     mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

  2) Execution as a Web Application Archive
     cd war
     mvn jetty:run

  3) Execution as a Web Application
     cd web
     mvn -Prun                      (standalone execution)
     mvn -Pexplorer                 (with FraSCAti Explorer)
     mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
     mvn -Pfscript-console          (with FraSCAti FScript Console)
     mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
     mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Compilation and execution with the FraSCAti script:
---------------------------------------------------
  Follow instructions in the 'standalone' directory.
