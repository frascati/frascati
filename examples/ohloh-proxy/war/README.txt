============================================================================
OW2 FraSCAti Examples: Ohloh Proxy Web Application
Copyright (C) 2009-2010 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):
 
============================================================================

FraSCAti RESTful Ohloh Proxy Web Application:
---------------------------------------------

This example shows how to package a RESTful service (ohloh-proxy) into a WAR
and then to deploy it on a Web Application Server like Jetty, Tomcat, JBoss,
Geronimo, JOnAS, etc.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn jetty:run
  
This command starts a standalone Jetty server on port 8080. This server loads
the FraSCAti RESTful Ohloh Proxy Web Application on the context path '/ohloh-proxy'.
This WAR deploys the OW2 FraSCAti Servlet where the SCA composite
'ohloh-proxy' is launched.

Deploying the WAR on a Web Application server:
----------------------------------------------
Install target/ohloh-proxy-web-application.war on your favorite
Web Application server, e.g., Jetty, Tomcat, JBoss, Geronimo, etc. 

For instance, for Apache Tomcat:
* copy the WAR to the Tomcat webapps directory:
  cp target/ohloh-proxy-web-application.war $CATALINA_HOME/webapps/ohloh-proxy.war
* Use the Tomcat Manager page to start the war.

For other Web servers, please refer to the documentation of your favorite server.

Using the RESTful Ohloh Proxy from a client Web browser:
--------------------------------------------------------
Open your favorite Web browser and go to:
* http://localhost:8080/ohloh-proxy/ to load the HTML page allowing to interact 
  with the SCA RESTful Ohloh Proxy service.
* http://localhost:8080/ohloh-proxy/services to see the list of exported RESTful resources.
* http://localhost:8080/ohloh-proxy/services/ohloh?_wadl to obtain the
  WADL describing the RESTful Ohloh Proxy service.

Interesting files to read:
--------------------------
* pom.xml contains the Maven process to build the WAR and start the Jetty server.
* src/main/webapp/WEB-INF/web.xml contains the configuration of the RESTful Ohloh
  Proxy Web Application.
* src/main/webapp/index.html is the HTML page accessible by http://localhost:8080/ohloh-proxy/

Let's note that the implementation of the RESTful Ohloh Proxy is available in the directory ../standalone/
