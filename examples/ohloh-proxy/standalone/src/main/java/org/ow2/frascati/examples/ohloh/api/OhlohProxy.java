/**
 * OW2 FraSCAti Examples: Ohloh Proxy
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.examples.ohloh.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Another simpler RESTful-based API for Ohloh.
 * Let's note that in comparison to {@link Ohloh},
 * each method doesn't require an 'api_key' parameter.
 *
 * TODO: Must be extended with other Ohloh methods.
 *
 * @author Philippe Merle
 */
public interface OhlohProxy
{
  /**
   * Returns information of a given Ohloh project in XML format.
   */
  @GET
  @Path("/projects/{project_id}.xml")
  @Produces("application/xml")
  String getProjectInXml(@PathParam("project_id") String projectId);
}
