/**
 * OW2 FraSCAti Examples: Ohloh Proxy
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 * 
 */

package org.ow2.frascati.examples.ohloh.explorer;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;

import org.ow2.frascati.examples.ohloh.api.OhlohProxy;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to interact with {@link OhlohProxy} instances.
 *
 * @author Philippe Merle
 */
@SuppressWarnings("serial")
public class OhlohProxyPanel
     extends AbstractSelectionPanel<OhlohProxy>
{
  /**
   * The default constructor creates the panel.
   */
  public OhlohProxyPanel()
  {
    super();
    this.setLayout(new GridLayout(2, 1));
    JPanel panel1 = new JPanel();
    this.add(panel1);
    panel1.setLayout(new GridLayout(2,1));
    JPanel panel2 = new JPanel();
    panel1.add(panel2);
    panel2.add(new JLabel("Enter an Ohloh project name: "));
    final JTextField projectNameField = new JTextField(20);
    panel2.add(projectNameField);
    JButton button = new JButton("Get project in XML");
    panel1.add(button);
    final JTextArea projectArea = new JTextArea(10, 20);
    this.add(new JScrollPane(projectArea));
    button.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
        	projectArea.setText(
              getSelection().getProjectInXml(projectNameField.getText())
            );
        }
      }
    );
  }
}
