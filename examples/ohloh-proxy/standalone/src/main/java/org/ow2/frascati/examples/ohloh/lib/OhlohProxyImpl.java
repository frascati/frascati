/**
 * OW2 FraSCAti Examples: Ohloh Proxy
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.examples.ohloh.lib;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.ohloh.api.Ohloh;
import org.ow2.frascati.examples.ohloh.api.OhlohProxy;

/**
 * Implements a proxy to Ohloh which injects the 'api_key' parameter on each method invocation automatically.
 * 
 * @author Philippe Merle
 */
@Service(OhlohProxy.class)
public class OhlohProxyImpl
  implements OhlohProxy
{

  /** Reference to the real Ohloh service. */
  @Reference(name="real-ohloh")
  private Ohloh ohloh;

  /** Property to configure the 'api_key' to use. */
  @Property(name="api-key")
  private String apiKey;

  /**
   * @see org.ow2.frascati.examples.ohloh.api.OhlohProxy#getProjectInXml(String)
   */
  public final String getProjectInXml(String projectId)
  {
    // Add the 'api_key' parameter.
    return ohloh.getProjectInXml(projectId, this.apiKey);
  }
}
