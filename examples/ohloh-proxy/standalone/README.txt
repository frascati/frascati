============================================================================
OW2 FraSCAti Examples: Ohloh Proxy
Copyright (C) 2009-2010 INRIA, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

FraSCAti Ohloh Proxy Standalone:
--------------------------------

This example shows how to build an SCA proxy client consuming Ohloh RESTful services
(http://www.ohloh.net). This proxy client is then exposed as a RESTful service.


Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------
  mvn -Prun                      (standalone execution)
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)

Compilation and execution with the FraSCAti script:
---------------------------------------------------
  frascati compile src ohloh
  frascati run ohloh-proxy -libpath ohloh.jar -s ohloh -m getProjectInXml -p frascati

Using the Ohloh Proxy Service from a client Web browser:
--------------------------------------------------------
Open your favorite Web browser and go to:
* http://localhost:8090/ohloh/projects/frascati.xml to see Ohloh information
  about the FraSCAti project.
* http://localhost:8090/ohloh/projects/{project_name}.xml to see Ohloh information
  about a given project.
* http://localhost:8090/ohloh?_wadl to obtain the WADL of the RESTful Ohloh Proxy service.
