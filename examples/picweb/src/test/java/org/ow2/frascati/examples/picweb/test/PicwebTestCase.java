/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URL;

import junit.framework.Assert;
import org.junit.Test;

import org.ow2.frascati.test.FraSCAtiTestCase;

/**
 * Test the PicWeb service.
 * 
 */
public class PicwebTestCase extends FraSCAtiTestCase
{
    @Test
    public final void testService() {
      // assert(true);
      // Try 3 times to reach the service before giving up
      int nbTries = 3;
      boolean success = false;
      while (nbTries > 0 && !success) {
        try {
          Runnable r = getService(Runnable.class, "r");
          System.out.println("run " + r.toString());
          r.run();
          success = true;

          /*org.ow2.frascati.examples.bpel.picasa.PicasaFcInItf picasa = getService(
						org.ow2.frascati.examples.bpel.picasa.PicasaFcInItf.class,
						"getPictures");

				ArrayOfURL response = null;
				response = picasa.getPictures("clown");// getAlbumContent(
														// "72157626659017125","b86b62c8d70789ae086489cccd2d1635");
				// response = flickr.getAlbumContent( "","");
				for (String resp : response.getURL())
					System.out.println(resp);
				success = true;*/
        } catch (RuntimeException e) {
          e.printStackTrace();
          System.err.println("Cannot get the service response...");
          System.err.println("Number of tries left: " + (nbTries - 1));
          // Sleep 15 seconds
          try {
            Thread.currentThread().sleep(15000);
          } catch (InterruptedException ie) {
            throw new Error(ie);
          }
        }
        nbTries--;
      }
      Assert.assertNotSame("Service unavailable after 3 tries", 0, nbTries);
    }
}
