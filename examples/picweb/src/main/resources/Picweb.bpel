<?xml version="1.0" encoding="UTF-8"?>
<!--
  * OW2 FraSCAti Examples: PicWeb
  *
  * Copyright (c) 2012 Inria, University of Lille 1
  *
  * This library is free software; you can redistribute it and/or
  * modify it under the terms of the GNU Lesser General Public
  * License as published by the Free Software Foundation; either
  * version 2 of the License, or (at your option) any later version.
  *
  * This library is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  * Lesser General Public License for more details.
  *
  * You should have received a copy of the GNU Lesser General Public
  * License along with this library; if not, write to the Free Software
  * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  *
  * Contact: frascati@ow2.org
  *
  * Author: Alexandre Feugas
  *
  * Contributor(s): Philippe Merle
-->
<bpws:process exitOnStandardFault="yes" name="Picweb"
	targetNamespace="http://frascati.ow2.org/examples/picweb/bpel"
	suppressJoinFailure="yes"
	xmlns:bpws="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
	xmlns:data="http://frascati.ow2.org/examples/picweb/bpel/data"
	xmlns:picweb="http://frascati.ow2.org/examples/picweb/bpel/picweb"
	xmlns:picasa="http://frascati.ow2.org/examples/picweb/bpel/picasa"
	xmlns:flickr="http://frascati.ow2.org/examples/picweb/bpel/flickr" 
	xmlns:helper="http://frascati.ow2.org/examples/picweb/bpel/helper"
	queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath2.0"
	expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath2.0"
>
	<bpws:import importType="http://schemas.xmlsoap.org/wsdl/"
		location="Picweb.wsdl" namespace="http://frascati.ow2.org/examples/picweb/bpel/picweb" />

	<bpws:import namespace="http://frascati.ow2.org/examples/picweb/bpel/picasa"
		location="Picasa.wsdl" importType="http://schemas.xmlsoap.org/wsdl/" />

	<bpws:import namespace="http://frascati.ow2.org/examples/picweb/bpel/flickr"
		location="Flickr.wsdl" importType="http://schemas.xmlsoap.org/wsdl/" />

	<bpws:import namespace="http://frascati.ow2.org/examples/picweb/bpel/helper"
		location="Helper.wsdl" importType="http://schemas.xmlsoap.org/wsdl/" />

	<bpws:partnerLinks>
		<bpws:partnerLink name="picweb"
		    partnerLinkType="picweb:Picweb" myRole="PicwebProvider" />
		<bpws:partnerLink name="PicasaPartner"
			partnerLinkType="picasa:Picasa" partnerRole="PicasaProvider" />
		<bpws:partnerLink name="FlickrPartner"
			partnerLinkType="flickr:Flickr" partnerRole="FlickrProvider" />
		<bpws:partnerLink name="HelperPartner"
			partnerLinkType="helper:Helper" partnerRole="HelperProvider" />
	</bpws:partnerLinks>

	<bpws:variables>
		<bpws:variable name="ReceiveInput" element="data:Tag"></bpws:variable>
		<bpws:variable name="PicasaResponse" element="data:PicwebResponse"></bpws:variable>
		<bpws:variable name="Response" element="data:PicwebResponse"></bpws:variable>
		<bpws:variable name="FlickrResponse" element="data:PicwebResponse"></bpws:variable>
        <bpws:variable name="ConcElemts" element="data:Concatenate"></bpws:variable>
    </bpws:variables>
	<bpws:sequence name="main">
		<bpws:receive createInstance="yes" name="receiveInput"
			partnerLink="picweb" operation="getPictures" portType="picweb:Picweb"
			variable="ReceiveInput">
		</bpws:receive>

        <bpws:flow name="Flow">
			<bpws:invoke name="PicasaInvoke" partnerLink="PicasaPartner"
				operation="getPics" portType="picasa:Picasa" inputVariable="ReceiveInput"
				outputVariable="PicasaResponse">
			</bpws:invoke>
			<bpws:invoke name="FlickrInvoke" partnerLink="FlickrPartner"
				operation="getPics" portType="flickr:Flickr" inputVariable="ReceiveInput"
				outputVariable="FlickrResponse"></bpws:invoke>
        </bpws:flow>
        <bpws:assign validate="no" name="Assign">
            <bpws:copy>
                <bpws:from><bpws:literal>
<data:Concatenate>
  <data:elmt1>
    <data:return>data:return</data:return>
  </data:elmt1>
  <data:elmt2>
    <data:return>data:return</data:return>
  </data:elmt2>
</data:Concatenate>
</bpws:literal></bpws:from>
                <bpws:to variable="ConcElemts"></bpws:to>
            </bpws:copy>
            <bpws:copy>
                <bpws:from variable="FlickrResponse">
                </bpws:from>
                <bpws:to variable="ConcElemts">
                    <bpws:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                        <![CDATA[data:elmt1]]>
                    </bpws:query>
                </bpws:to>
            </bpws:copy>
            <bpws:copy>
                <bpws:from variable="PicasaResponse">
                </bpws:from>
                <bpws:to variable="ConcElemts">
                    <bpws:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                        <![CDATA[data:elmt2]]>
                    </bpws:query>
                </bpws:to>
            </bpws:copy>
        </bpws:assign>
        <bpws:invoke name="Invoke" partnerLink="HelperPartner" operation="concat" portType="helper:Helper" inputVariable="ConcElemts" outputVariable="Response"></bpws:invoke>
        <bpws:reply name="replyOutput" operation="getPictures"
			partnerLink="picweb" portType="picweb:Picweb" variable="Response" />
	</bpws:sequence>
</bpws:process>
