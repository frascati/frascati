/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb.lib;

import java.io.File;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.tinfi.api.IntentHandler;
import org.ow2.frascati.tinfi.api.IntentJoinPoint;

@Scope("Composite")
@Service(IntentHandler.class)
public class TimeIntent implements IntentHandler
{
    public Object invoke(IntentJoinPoint ijp) throws Throwable
    {
      Object ret;
      long begin = System.currentTimeMillis();
      ret = ijp.proceed();
      long end = System.currentTimeMillis();
      System.out.println("	>>" + ijp.getMethod().getName() + ": time="
				+ (end - begin));

      BufferedWriter output = null;
      String text = ijp.getMethod().getName() + "," + end +","+(end - begin)+"\n";
      File file = new File("traces/"+ ijp.getInterface().getFcItfName()+"-" + ijp.getMethod().getName() +".csv");
      output = new BufferedWriter(new FileWriter(file,true));
      output.write(text);
      output.close();
      System.out.println("Your file has been written");
      return ret;
	}
}
