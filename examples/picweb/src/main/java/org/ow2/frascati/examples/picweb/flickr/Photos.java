/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.picweb.flickr;

import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author Philippe Merle
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Photos")
public class Photos
{
	@XmlAttribute(name="page")
    private int page;

	@XmlAttribute(name="pages")
    private int pages;

	@XmlAttribute(name="perpage")
    private int perPage;

	@XmlAttribute(name="total")
    private int total;

	@XmlElement(name="photo", nillable=true)
    private Collection<Photo> photos;

    public final Collection<Photo> getPhotos()
    {
      return this.photos;
    }

    public final void setPhotos(Collection<Photo> photos)
    {
        this.photos = photos;
    }

    public final int getPage()
    {
      return this.page;
    }

    public final int getPages()
    {
      return this.pages;
    }

    public final int getPerPage()
    {
      return this.perPage;
    }

    public final int getTotal()
    {
      return this.total;
    }
}
