/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb.flickr;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.examples.picweb.bpel.data.GetFolksonomyContentResponse;

/**
 * @author Alexandre Feugas - Inria
 */
@Scope("COMPOSITE")
@Service(org.ow2.frascati.examples.picweb.bpel.flickr.Flickr.class)
public class FlickrImpl implements org.ow2.frascati.examples.picweb.bpel.flickr.Flickr
{
    // --------------------------------------------------------------------------
    // Internal state.
    // --------------------------------------------------------------------------

    @Property
    private int count = 100;

    @Property
    String apiKey = "b86b62c8d70789ae086489cccd2d1635";

    @Reference(name="Flickr-REST")
    protected Flickr flickr;
    
    // --------------------------------------------------------------------------
    // Default constructor.
    // --------------------------------------------------------------------------

    public FlickrImpl()
    {
      System.out.println("Flickr created");
    }

    // --------------------------------------------------------------------------
    // Implementation of the Flickr interface.
    // --------------------------------------------------------------------------

    public GetFolksonomyContentResponse getPics(String tags)
    {
      System.out.println("Flickr::getPics(" + tags + ")...");

      try {
        // Invoke Flickr.
        Rsp response = flickr.invoke("flickr.photos.search", this.apiKey, tags, this.count);

        // Create the response for the BPEL process.
        GetFolksonomyContentResponse gfcr = new GetFolksonomyContentResponse();
        for(Photo photo : response.getPhotos().getPhotos()) {
          gfcr.getReturn().add(photo.getURL());
        }
        return gfcr;
      } catch (Exception e) {
        // throw new FlickrWrapperException(e.getMessage());
        System.err.println("Flickr::getPics : exception " + e.getMessage());
        e.printStackTrace(System.err);
        return new GetFolksonomyContentResponse();
      }
    }
}
