/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.picweb.picasa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author Philippe Merle - Inria
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="entry", namespace="http://www.w3.org/2005/Atom")
public class Entry
{
	@XmlElement(name="id")
	private String id;
			
	@XmlElement(name="updated")
	private String updated; // TODO Date

	@XmlElement(name="published")
	private String published; // TODO Date

	// TODO <category> element

	@XmlElement(name="title")
	private String title;

	@XmlElement(name="content")
	private Content content;

	@XmlElement(name="summary")
	private String summary;

	// TODO collection of <link> elements
	// TODO <author> element
	// TODO <gphoto:*> elements
	// TODO <media:group> element

    public final String getId()
    {
      return this.id;
    }

    public final String getPublished()
    {
      return this.published;
    }

    public final String getUpdated()
    {
      return this.updated;
    }

    public final String getTitle()
    {
      return this.title;
    }

    public final String getSummary()
    {
      return this.summary;
    }

    public final Content getContent()
    {
      return this.content;
    }
}
