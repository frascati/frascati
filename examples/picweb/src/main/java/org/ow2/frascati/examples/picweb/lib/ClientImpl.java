/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb.lib;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.osoa.sca.annotations.Reference;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.picweb.bpel.data.GetFolksonomyContentResponse;
import org.ow2.frascati.examples.picweb.bpel.picweb.Picweb;

/**
 * @author Alexandre Feugas - Inria
 */
@Scope("COMPOSITE")
@Service(interfaces = { Runnable.class })
public class ClientImpl implements Runnable
{
    @Reference(name = "process")
    private Picweb process;

    public final void run()
    {
      try {
        // Open the file that is the first
        // command line parameter
        FileInputStream fstream = new FileInputStream("words630.txt");
        // Get the object of DataInputStream
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        // Read File Line By Line
        while ((strLine = br.readLine()) != null) {
          GetFolksonomyContentResponse res = process.getPictures(strLine);
          System.out.println("call done");
          if (res.getReturn().size() > 0) { // we have pictures!
            System.out.println("Il y a " + res.getReturn().size()
                             + " pictures related to " + strLine);
            Iterator<String> it = res.getReturn().iterator();
            if (it == null) {
              System.out.println("[E] ClientImpl : error on word" + strLine);
            }
            while (it.hasNext()) {
              System.out.println(it.next());
            }
          }
        }
        // Close the input stream
        in.close();
      } catch (Exception e) { // Catch exception if any
        System.err.println("[E] ClientImpl exception " + e.getMessage());
      }
    }
}
