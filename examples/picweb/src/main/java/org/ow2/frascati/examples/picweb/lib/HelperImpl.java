/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb.lib;

import java.util.List;

import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;

import org.ow2.frascati.examples.picweb.bpel.data.ConcatenateType;
import org.ow2.frascati.examples.picweb.bpel.data.GetFolksonomyContentResponse;
import org.ow2.frascati.examples.picweb.bpel.helper.Helper;

/**
 * @author Alexandre Feugas - Inria
 */
@Scope("COMPOSITE")
@Service(Helper.class)
public class HelperImpl implements Helper
{
    public HelperImpl()
    {
      System.out.println("Helper created");
    }

    public GetFolksonomyContentResponse concat(ConcatenateType conc) {
      GetFolksonomyContentResponse rep = new GetFolksonomyContentResponse();
      List<String> elmts = rep.getReturn();
      elmts.addAll(conc.getElmt1().getReturn());
      elmts.addAll(conc.getElmt2().getReturn());
      return rep;
    }
}
