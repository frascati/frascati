/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.picweb.flickr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author Philippe Merle
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Photo")
public class Photo
{
	@XmlAttribute(name="id")
    private String id;

	@XmlAttribute(name="owner")
    private String owner;

	@XmlAttribute(name="secret")
    private String secret;

	@XmlAttribute(name="server")
    private String server;

	@XmlAttribute(name="farm")
    private String farm;

	@XmlAttribute(name="title")
    private String title;

	@XmlAttribute(name="ispublic")
    private String isPublic;

	@XmlAttribute(name="isfriend")
    private String isFriend;

	@XmlAttribute(name="isfamily")
    private String isFamily;

    public final String getId()
    {
      return this.id;
    }

    public final String getOwner()
    {
      return this.owner;
    }

    public final String getSecret()
    {
      return this.secret;
    }

    public final String getServer()
    {
      return this.server;
    }

    public final String getFarm()
    {
      return this.farm;
    }

    public final String getTitle()
    {
      return this.title;
    }

    public final String getIsPublic()
    {
      return this.isPublic;
    }

    public final String getIsFriend()
    {
      return this.isFriend;
    }

    public final String getIsFamily()
    {
      return this.isFamily;
    }

    public final String getURL()
    {
      return "http://farm" + this.farm + ".static.flickr.com/" + this.server + "/" + this.id + "_" + this.secret + ".jpg";
    }
}
