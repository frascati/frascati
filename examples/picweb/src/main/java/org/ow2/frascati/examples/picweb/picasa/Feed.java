/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.picweb.picasa;

import java.util.Collection;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Philippe Merle - Inria
 */
@XmlRootElement(name="feed", namespace="http://www.w3.org/2005/Atom")
@XmlAccessorType(XmlAccessType.FIELD)
public class Feed
{
	@XmlElement(name="id")
	private String id;
			
	@XmlElement(name="updated")
	private String updated; // TODO type is Date

	@XmlElement(name="title")
	private String title;

	// TODO collection of <link> elements
	// TODO <generator> element

	@XmlElement(name="totalResults")
	private int totalResults;

	@XmlElement(name="startIndex")
	private int startIndex;

	@XmlElement(name="itemsPerPage")
	private int itemsPerPage;

	@XmlElement(name="entry", nillable=true)
    private Collection<Entry> entry;

    public final String getId()
    {
      return this.id;
    }

    public final String getUpdated()
    {
      return this.updated;
    }

    public final String getTitle()
    {
      return this.title;
    }

    public final int getTotalResults()
    {
      return this.totalResults;
    }

    public final int getStartIndex()
    {
      return this.startIndex;
    }

    public final int getItemsPerPage()
    {
      return this.itemsPerPage;
    }

    public final Collection<Entry> getEntry()
    {
      return this.entry;
    }
}
