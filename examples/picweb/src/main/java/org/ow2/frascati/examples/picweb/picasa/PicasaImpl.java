/**
 * OW2 FraSCAti: PicWeb
 * Copyright (C) 2012 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Alexandre Feugas
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.picweb.picasa;

import org.osoa.sca.annotations.Property;
import org.osoa.sca.annotations.Scope;
import org.osoa.sca.annotations.Service;
import org.osoa.sca.annotations.Reference;

import org.ow2.frascati.examples.picweb.bpel.data.GetFolksonomyContentResponse;

/**
 * @author Alexandre Feugas - Inria
 */
@Scope("COMPOSITE")
@Service(org.ow2.frascati.examples.picweb.bpel.picasa.Picasa.class)
public class PicasaImpl implements org.ow2.frascati.examples.picweb.bpel.picasa.Picasa
{
    // --------------------------------------------------------------------------
    // Internal state
    // --------------------------------------------------------------------------

    @Property
    private int count = 100;

    @Reference(name="Picasa-REST")
    protected Picasa picasa;
    
    // --------------------------------------------------------------------------
    // Default constructor
    // --------------------------------------------------------------------------

    public PicasaImpl()
    {
      System.out.println("Picasa created");
    }

    // --------------------------------------------------------------------------
    // Implementation of the Picassa interface
    // --------------------------------------------------------------------------

    public final GetFolksonomyContentResponse getPics(final String tags)
    {
      System.out.println("Picasa::getPics(" + tags + ")...");

      try {
        // Invoke Picasa.
        Feed response = picasa.invoke("photo", this.count, tags);

        // Create the response for the BPEL process.
        GetFolksonomyContentResponse gfcr = new GetFolksonomyContentResponse();
        for(Entry entry : response.getEntry()) {
          gfcr.getReturn().add(entry.getContent().getSrc());
        }
        return gfcr;
      } catch (Exception e) {
        // throw new FlickrWrapperException(e.getMessage());
        System.err.println("Picasa::getPics : exception " + e.getMessage());
        e.printStackTrace(System.err);
        return new GetFolksonomyContentResponse();
      }
    }
}
