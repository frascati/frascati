/**
 * OW2 FraSCAti Examples: Fibonacci with WS-Discovery-based Load Balancing
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.fibonacci;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 * Interface of the Fibonacci algorithm annotated to be REST ready.
 * 
 * @author Philippe Merle at Inria
 */
public interface Fibonacci
{
  /**
   * Computes the value of Fibonnaci(n)
   * 
   * @param n input parameter (must be strictly positive).
   * @return Fibonnaci(n).
   */
  @GET
  @Path("/{n}")
  @Produces("text/plain")
  long compute(@PathParam("n") int n);
}
