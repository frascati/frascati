/**
 * OW2 FraSCAti Examples: Fibonacci with WS-Discovery-based Load Balancing
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.fibonacci;

/**
 * Implementation of the Fibonacci algorithm by delegation to another Fibonacci computer.
 *
 * @author Philippe Merle at Inria
 */
@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(Fibonacci.class)
public class FibonacciProxy
  implements Fibonacci
{
	@org.oasisopen.sca.annotation.ComponentName
	private String myComponentName;

	@org.oasisopen.sca.annotation.Reference
	private Fibonacci fibonacci;

    /**
     * @see org.ow2.frascati.examples.fibonacci.api.Fibonacci#compute(int)
     */
    public long compute(int n)
    {
      System.out.println(this.myComponentName + ": Compute fibonacci(" + n + ")...");
      long result = this.fibonacci.compute(n);
      System.out.println(this.myComponentName + ": fibonacci(" + n + ")=" + result);
      return result;
    }
}
