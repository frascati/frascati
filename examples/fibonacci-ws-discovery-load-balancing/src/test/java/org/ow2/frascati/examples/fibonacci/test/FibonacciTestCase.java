/**
 * OW2 FraSCAti Examples: Fibonacci with WS-Discovery-based Load Balancing
 * Copyright (C) 2013 Inria, University Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 */

package org.ow2.frascati.examples.fibonacci.test;

import org.junit.Assert;
import org.junit.Test;

import org.ow2.frascati.test.FraSCAtiTestCase;
import org.ow2.frascati.examples.fibonacci.Fibonacci;

public class FibonacciTestCase
     extends FraSCAtiTestCase
{
  @Test
  public final void testService()
  {
    Fibonacci fibonacci = getService(Fibonacci.class, "Fibonacci");
    for(int i=0; i<10; i++) {
      Assert.assertEquals("Fibonacci(0)", 0, fibonacci.compute(0));
      Assert.assertEquals("Fibonacci(1)", 1, fibonacci.compute(1));
      Assert.assertEquals("Fibonacci(2)", 1, fibonacci.compute(2));
      Assert.assertEquals("Fibonacci(3)", 2, fibonacci.compute(3));
      Assert.assertEquals("Fibonacci(10)", 55, fibonacci.compute(10));
      Assert.assertEquals("Fibonacci(20)", 6765, fibonacci.compute(20));
    }
  }
}
