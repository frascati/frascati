============================================================================
OW2 FraSCAti Examples: Fibonacci with WS-Discovery-based Load Balancing 
Copyright (C) 2013 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

Fibonacci with WS-Discovery-based Load Balancing:
-------------------------------------------------

This example illustrates the usage of the WS-DiscoveryLoadBalancing intent.
This intent allows to load balance requests to a set of Web services
discovered dynamically via the WS-Discovery protocol.

This example includes a set of workers implementing the Fibonacci algorithm as
an SCA component exposed as a Web Service. A master component is bound to this
set of workers via the DiscoveryLoadBalancing intent.
Each time the master is invoked then it invokes one of the workers.
Workers are discovered dynamically with WS-Discovery.
The invoked worker is selected by a load balancer.


Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------

  mvn -Prun 
  mvn -Pexplorer                 (with FraSCAti Explorer)
  mvn -Pexplorer-fscript         (with FraSCAti Explorer and FScript plugin)
  mvn -Pfscript-console          (with FraSCAti FScript Console)
  mvn -Pfscript-console-explorer (with FraSCAti Explorer and FScript Console)
  mvn -Pexplorer-jdk6            (with FraSCAti Explorer and JDK6)
  mvn -Prun,web                  (with FraSCAti Web Explorer)

By default, the load balancing algorithm is sequential.
To use the random load balancing algorithm, type:

  mvn -Prandom

To start a set of workers, open another shell and type:

  mvn -Prun,workers

Optionally add '-Dport=xyz' to configure the IP port where workers are exposed, e.g.:

  mvn -Prun,workers -Dport=9876

Then type 'mvn test' and you will see that requests are load balanced
between workers running into different processes.
