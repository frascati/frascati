/**
 * OW2 FraSCAti Examples: Soap Calculator
 * Copyright (C) 2009-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Christophe Demarey
 *
 * Contributor(s): Philippe Merle
 * 
 */

package org.ow2.frascati.examples.soapcalc.explorer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.ow2.frascati.examples.soapcalc.api.CalcService;
import org.ow2.frascati.explorer.gui.AbstractSelectionPanel;

/**
 * Is the FraSCAti Explorer plugin to show {@link CalcService} instances.
 *
 * @author Christophe Demarey
 */
@SuppressWarnings("serial")
public class CalculatorPanel
     extends AbstractSelectionPanel<CalcService>
{
  private JButton jButtonAdd;
  private JButton jButtonDiv;
  private JButton jButtonMult;
  private JButton jButtonSub;
  private JLabel jLabel1;
  private JLabel jLabel3;
  private JLabel jLabel5;
  private JLabel jLabel7;
  private JLabel jLabelAddResult;
  private JLabel jLabelDivResult;
  private JLabel jLabelMultResult;
  private JLabel jLabelSubResult;
  private JTextField jTextField1;
  private JTextField jTextField2;
  private JTextField jTextField3;
  private JTextField jTextField4;
  private JTextField jTextField5;
  private JTextField jTextField6;
  private JTextField jTextField7;
  private JTextField jTextField8;

  /**
   * Default constructor creates the panel.
   */
  public CalculatorPanel()
  {
    super();

    jButtonAdd = new JButton("=");
    jTextField1 = new JTextField();
    jLabel1 = new JLabel("+");
    jLabelAddResult = new JLabel();
    jTextField2 = new JTextField();
    jLabel3 = new JLabel("-");
    jTextField3 = new JTextField();
    jLabelSubResult = new JLabel();
    jTextField4 = new JTextField();
    jButtonSub = new JButton("=");
    jLabel5 = new JLabel("*");
    jTextField5 = new JTextField();
    jLabelMultResult = new JLabel();
    jTextField6 = new JTextField();
    jButtonMult = new JButton("=");
    jLabel7 = new JLabel("/");
    jTextField7 = new JTextField();
    jLabelDivResult = new JLabel();
    jTextField8 = new JTextField();
    jButtonDiv = new JButton("=");

    // layout
    GroupLayout mainPanelLayout = new GroupLayout(this);
    this.setLayout(mainPanelLayout);
    mainPanelLayout.setHorizontalGroup(
        mainPanelLayout.createParallelGroup(GroupLayout.LEADING)
        .add(mainPanelLayout.createSequentialGroup()
            .addContainerGap()
            .add(mainPanelLayout.createParallelGroup(GroupLayout.LEADING)
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jTextField4, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.RELATED)
                    .add(jLabel3))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jTextField8, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.RELATED)
                    .add(jLabel7))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jTextField6, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.RELATED)
                    .add(jLabel5))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jTextField1, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.RELATED)
                    .add(jLabel1)))
            .addPreferredGap(LayoutStyle.RELATED)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.CENTER)
                .add(jTextField7, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .add(jTextField5, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .add(jTextField3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .add(jTextField2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
            .add(12, 12, 12)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.LEADING)
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jButtonSub)
                    .addPreferredGap(LayoutStyle.UNRELATED)
                    .add(jLabelSubResult, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jButtonDiv)
                    .add(18, 18, 18)
                    .add(jLabelDivResult, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jButtonMult)
                    .add(18, 18, 18)
                    .add(jLabelMultResult, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE))
                .add(mainPanelLayout.createSequentialGroup()
                    .add(jButtonAdd)
                    .add(18, 18, 18)
                    .add(jLabelAddResult, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(178, Short.MAX_VALUE))
    );
    mainPanelLayout.setVerticalGroup(
        mainPanelLayout.createParallelGroup(GroupLayout.LEADING)
        .add(mainPanelLayout.createSequentialGroup()
            .add(14, 14, 14)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.BASELINE)
                .add(jTextField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .add(jLabel1)
                .add(jButtonAdd)
                .add(jLabelAddResult, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .add(jTextField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.RELATED)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.BASELINE)
                .add(jTextField4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .add(jLabel3)
                .add(jLabelSubResult, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .add(jButtonSub)
                .add(jTextField3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.RELATED)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.BASELINE)
                .add(jTextField6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .add(jLabel5)
                .add(jButtonMult)
                .add(jLabelMultResult, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .add(jTextField5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(LayoutStyle.RELATED)
            .add(mainPanelLayout.createParallelGroup(GroupLayout.BASELINE)
                .add(jTextField8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .add(jLabel7)
                .add(jButtonDiv)
                .add(jLabelDivResult, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .add(jTextField7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addContainerGap(259, Short.MAX_VALUE))
    );

    // actions
    jButtonAdd.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            int result = selected.add( Integer.parseInt(jTextField1.getText()),
                                       Integer.parseInt(jTextField2.getText()) );
            jLabelAddResult.setText(Integer.toString(result));
        }
      }
    );
    jButtonSub.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            int result = selected.sub( Integer.parseInt(jTextField4.getText()),
                                       Integer.parseInt(jTextField3.getText()) );
            jLabelSubResult.setText(Integer.toString(result));
        }
      }
    );
    jButtonMult.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            int result = selected.mult( Integer.parseInt(jTextField5.getText()),
                                       Integer.parseInt(jTextField6.getText()) );
            jLabelMultResult.setText(Integer.toString(result));
        }
      }
    );
    jButtonDiv.addActionListener( new ActionListener() {
        public final void actionPerformed(ActionEvent e) {
            double result = selected.div( Integer.parseInt(jTextField8.getText()),
                                       Integer.parseInt(jTextField7.getText()) );
            jLabelDivResult.setText(Double.toString(result));
        }
      }
    );
  }

}
