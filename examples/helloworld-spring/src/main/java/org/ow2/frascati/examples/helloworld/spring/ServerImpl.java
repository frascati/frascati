/**
 * OW2 FraSCAti Examples: HelloWorld Spring
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.spring;

public class ServerImpl
  implements Service
{
  private String header = "->";
  private int count = 1;

  public ServerImpl()
  {
    System.err.println("SERVER created");
  }

  public final void print(final String msg)
  {
    new Exception() {
      private static final long serialVersionUID = 2182742162070453637L;

      public String toString() {
        return "Server: print method called";
      }

    }.printStackTrace();
    System.err.println("Server: begin printing...");
    for (int i = 0; i < (count); ++i) {
      System.err.println(((header) + msg));
    }
    System.err.println("Server: print done.");
  }

  public final String getHeader()
  {
    return header;
  }

  public final void setHeader(final String header)
  {
    this.header = header;
  }

  public final int getCount()
  {
    return count;
  }

  public final void setCount(final int count)
  {
    this.count = count;
  }

}
