/**
 * OW2 FraSCAti Examples: HelloWorld Spring
 * Copyright (C) 2008-2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 *
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.spring;

import org.objectweb.fractal.api.Component;

import org.ow2.frascati.FraSCAti;
import org.ow2.frascati.util.FrascatiException;

public final class Main
{
  private Main() {}

  public static void main(String[] args) throws FrascatiException
  {
    System.out.println("\n\n\nStarting the OW2 FraSCAti Helloworld Spring Example\n\n\n");

    FraSCAti frascati = FraSCAti.newFraSCAti();

    Component scaComposite = frascati.getComposite("helloworld-spring");

    Runnable hello = frascati.getService(scaComposite, "r", Runnable.class);

    // performs some calls
    hello.run();

    // close the SCA composite.
    frascati.close(scaComposite);

    System.out.println("\n\n\n");
  }

}
