(:
  OW2 FraSCAti: SCA Implementation BPEL
  Copyright (C) 2010 INRIA, University of Lille 1
 
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
  Contact: frascati@ow2.org
 
  Author: Haderer Nicolas
 
  Contributor(s):
 
 /:)

(: --------------------- DECLARE SERVICE ----------------------------------------- :)
(: the namespace of the service declaration must be the name of the interface :)

declare namespace client="org.ow2.frascati.examples.helloworld.xquery.IXQueryClient";

(: --------------------- DECLARE REFERENCE-------------------------------------- :)
(: the namespace of the reference must begining by "java:"+ the name of  the inteface of  the reference :)
declare namespace server="java:org.ow2.frascati.examples.helloworld.xquery.IXQueryServer";

(: for service refenrece, the variable must be to declared  external :)
declare variable $server external;

declare variable $store as xs:string:="src/main/resources/scripts/bookstore.xml";










declare function client:printInteger() as xs:integer {
   server:getInteger($server)
};

declare function client:printDecimal() as xs:double{
  server:getDecimal($server)
};


declare function client:printTabString() as xs:string*{
   server:getTabString($server)
};

declare function client:printListFloat() as xs:float*{
   server:getListFloat($server)
};

declare function client:printString() as xs:string{
    server:getString($server)
};

declare function client:getBookUnderPrice($bookstore as document-node(), $price  as xs:decimal) as document-node()*{
        
   let $books  as document-node()* :=  server:getBookUnderPrice($server,$bookstore, $price)
   return $books
};

0