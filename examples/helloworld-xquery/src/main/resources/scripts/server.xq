xquery version "1.0";
(:
  OW2 FraSCAti: SCA Implementation BPEL
  Copyright (C) 2010 INRIA, University of Lille 1
 
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
 
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 
  Contact: frascati@ow2.org
 
  Author: Haderer Nicolas
 
  Contributor(s):
 
 /:)
declare namespace server="org.ow2.frascati.examples.helloworld.xquery.IXQueryServer";
                                                             


declare variable $store as xs:string:="src/main/resources/scripts/bookstore.xml";

declare function server:getString(   ) as xs:string{
    "hello world"
};


declare function server:getInteger() as xs:integer{
    50 + 1
};

declare function server:getDecimal() as xs:decimal{
       10.01
       
};

declare function server:getTabString() as xs:string*{

    doc("src/main/resources/scripts/bookstore.xml")/bookstore/book/title

};

declare function server:getListFloat() as xs:decimal*{

    doc("src/main/resources/scripts/bookstore.xml")/bookstore/book/price

};

declare function server:getBookUnderPrice($bookstore as element(), $price as xs:decimal) as element()*{
 
   for $book in $bookstore/bookstore/book
   where $book/price <= $price
   return $book
};



0
