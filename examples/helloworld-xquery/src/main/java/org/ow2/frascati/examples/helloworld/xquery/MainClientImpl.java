/**
 * OW2 FraSCAti: SCA Implementation BPEL
 * Copyright (C) 2010 INRIA, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Haderer Nicolas
 *
 * Contributor(s):
 *
 */
package org.ow2.frascati.examples.helloworld.xquery;

import java.util.List;

import javax.script.ScriptEngineManager;

import org.oasisopen.sca.annotation.Reference;
import org.ow2.frascati.examples.helloworld.xquery.pojo.Book;
import org.ow2.frascati.examples.helloworld.xquery.pojo.Bookstore;
import org.ow2.frascati.examples.helloworld.xquery.pojo.ObjectFactory;


public class MainClientImpl implements Runnable{

	@Reference
	private IXQueryClient client;
	
	public void run() {
		
		this.testTypeSimple();
		
		this.testJaxbType();
	
	}

	public void testTypeSimple(){
		
		System.out.println("=====================");
		System.out.println("== Basic type ");
		System.out.println("=====================");
		
		
		/*
		 * Print String
		 */
		final String str = client.printString();
		System.out.println( str.getClass()+" = "+str.toString());
		
		/*
		 * Print Integer
		 */
		final Integer integer = client.printInteger();
		System.out.println( integer.getClass()+" = "+integer);
		
		/*
		 * Print Array
		 */
		final String[] strTab = client.printTabString();
		System.out.print(strTab.getClass()+" => ");
		for (int i=0;i<strTab.length;i++){
			System.out.print("str["+i+"] = "+strTab[i]+" ");
		}
		System.out.println();
		
		
		/*
		 * Print List
		 */
		final List<Float> floatList = client.printListFloat();
		System.out.println(floatList.getClass()+" = "+floatList.toString());
	}
	
	public void testJaxbType(){
		
		System.out.println("=====================");
		System.out.println("== JaxB type");
		System.out.println("=====================");
		
		
		//create jaxb bookstore 
		final ObjectFactory factory = new ObjectFactory();
		Bookstore bookstore = factory.createBookstore();
		for (int i=0;i<10;i++){
			Book book = factory.createBook();
			book.setTitle("title "+i);
			book.setAuthor("author"+i);
			bookstore.getBook().add(book);
		}
		
		
		
		final float price = 30;
		System.out.println("search book under the price "+price);
		
		/*
		 * call the client function with jaxb object
		 * engine convert automatically jaxb object to xpath value for the xquery process
		 */
		
		final Book[] books =  this.client.getBookUnderPrice(bookstore, price);
		
		
		for (Book book: books){
			System.out.println("Title : \""+book.getTitle()+"\" Price : \""+book.getPrice()+"\"");
		}
		
	}
	
	public static void main(String[] args){
		
		ScriptEngineManager manager = new ScriptEngineManager();
		//System.out.println(manager.getEngineFactories().toString());
	}

}
