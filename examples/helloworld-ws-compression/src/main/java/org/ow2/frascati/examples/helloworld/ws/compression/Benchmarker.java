/**
 * OW2 FraSCAti Examples: HelloWorld Web Service with FastInfoset and GZIP 
 * Copyright (C) 2013 Inria, University of Lille 1
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Philippe Merle
 * 
 * Contributor(s):
 *
 */

package org.ow2.frascati.examples.helloworld.ws.compression;

import org.apache.hello_world_soap_http.Greeter;

@org.oasisopen.sca.annotation.Scope("COMPOSITE")
@org.oasisopen.sca.annotation.Service(Runnable.class)
public class Benchmarker
  implements Runnable
{
  // --------------------------------------------------------------------------
  // SCA
  // --------------------------------------------------------------------------

  @org.oasisopen.sca.annotation.Property
  private String description = "UNKNOWN";

  @org.oasisopen.sca.annotation.Property
  private int warnupCalls = 10;

  @org.oasisopen.sca.annotation.Property
  private int benchmarkCalls = 100;

  @org.oasisopen.sca.annotation.Reference
  private Greeter greeter;
  
  // --------------------------------------------------------------------------
  // Implementation of the Runnable interface
  // --------------------------------------------------------------------------

  public final void run()
  {
    //
	// Warnup phase.
	//
	for(int i=0; i<this.warnupCalls; i++) {
      String greetMe = this.greeter.greetMe("FraSCAti");
	}

    //
	// Benchmark phase.
	//
	long beginTime = System.currentTimeMillis();
	for(int i=0; i<this.benchmarkCalls; i++) {
	   String greetMe = this.greeter.greetMe("FraSCAti");
	}
	long endTime = System.currentTimeMillis();

    //
	// Report phase.
	//
	long totalTime = endTime - beginTime;
	System.out.println("----------------------------------------");
	System.out.println(this.description + ":");
	System.out.println("* " + warnupCalls + " warnup calls.");
	System.out.println("* " + benchmarkCalls + " benchmark calls.");
	System.out.println("* total time in ms = " + totalTime);
	System.out.println("* invocation time in ms = " + ((double)totalTime/this.benchmarkCalls));
	System.out.println("----------------------------------------");
  }
}
