============================================================================
OW2 FraSCAti Examples: HelloWorld Web Service Reliable Messaging (WS-RM)
Copyright (C) 2013 Inria, University of Lille 1

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
USA

Contact: frascati@ow2.org

Author: Philippe Merle

Contributor(s):

============================================================================

HelloWorld Web Service Reliable Messaging (WS-RM):
--------------------------------------------------

This example shows how to expose an SCA service as a Web Service supporting
WS-Reliable Messaging and how to consume it from an SCA reference.

Compilation with Maven:
-----------------------
  mvn install

Execution with Maven:
---------------------

     mvn -Prun,server (in one shell)
     mvn -Prun,client (in another shell)
