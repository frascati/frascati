/**
 * OW2 FraSCAti Examples: HelloWorld multiple reference
 * Copyright (C) 2013 Inria, University of Lille 1
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Contact: frascati@ow2.org
 *
 * Author: Gwenael Cattez
 *
 * Contributor(s): Philippe Merle
 *
 */

package org.ow2.frascati.examples.helloworld.multiple;

import java.util.List;

import org.oasisopen.sca.annotation.Reference;
import org.oasisopen.sca.annotation.Scope;
import org.objectweb.fractal.fraclet.annotations.Lifecycle;
import org.objectweb.fractal.fraclet.types.Step;

@Scope("COMPOSITE")
public class RunnerMultiple implements Runnable
{
    
    private List<Runnable> runnables;
    
    private Runnable runner;

    public RunnerMultiple()
    {
        System.out.println("RunnerMultiple Constructor");
    }
    
    @Lifecycle(step=Step.CREATE)
    public void create()
    {
      System.out.println("RunnerMultiple CREATE");
    }

    @Lifecycle(step=Step.START)
    public void start()
    {
      System.out.println("RunnerMultiple START");
    }
    
    public void run()
    {
      System.out.println();
      for(Runnable runnable : this.runnables)
      {
          if(runnable!=null)
          {
              runnable.run();
          }
      }
    }

    public java.util.List<Runnable> getRunnables()
    {
        System.out.println("Get Runnables");
        return runnables;
    }

    @Reference(name="runnables")
    public void setRunnables(List<Runnable> runnables)
    {
        System.out.println("set Runnables with "+runnables.size()+" elements");
        this.runnables = runnables;
    }

    public Runnable getRunner()
    {
        System.out.println("Get Runner");
        return runner;
    }

    @Reference(name="runner")
    public void setRunner(Runnable runner)
    {
        this.runner = runner;
    }
    
    @Lifecycle(step=Step.STOP)
    public void stop()
    {
      System.out.println("RunnerMultiple STOP");
    }
    
    @Lifecycle(step=Step.DESTROY)
    public void destry()
    {
      System.out.println("RunnerMultiple DESTROY");
    }
   
}
